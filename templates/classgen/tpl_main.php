<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * <p>{label}</p>
 *
 * @name  {name}
 * @author {author}
 * @licence Cecill v2 (http://www.cecill.info) {link}
 * @copyright {copyright}
 * @version {version}
 * @package {package}
 */

class {name} {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietes    */
	/*~*~*~*~*~*~*~*~*~*~*/

	{attrib}

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. methodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>creation de l'instance de la classe</p>
	 *
	 * @name {name}::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		{init}
		
		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}
		
	}

	/**
	 * Accesseurs en lecture
	 */

	{readfonc}

	/**
	 * Accesseurs en ecriture
	 */

	{writefonc}

	/**
	 * Compte du nombre d'occurence de l'objet {name} dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name {name}::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count({idName}) AS nbr ';
		$sql .= 'FROM {tableName} ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

		/**
	 * renvoit l'id d'un stamp existant
	 *
	 * <p>_existsRef</p>
	 *
	 * @name {name}::_existsRef()
	 * @param $lib string
	 * @return int
	 */
	public function _existsRef($lib)
	{
		$idRef = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT {idName} ';
		$sql .= ' FROM {tableName} ';
		$sql .= ' WHERE UPPER(lib) = \''.strtoupper($lib).'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$idRef  = $row['{idName}'] ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		return $idRef;
	}
	
	/**
	 * Creation d'un {name} dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name {name}::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX({idName}) AS idmax FROM {tableName} ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->{idName} = $row['idmax']+1 ;
			}else{
				$this->{idName} = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		 
		$sql  = 'INSERT INTO {tableName} VALUES( ';
		{createfonc}
		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise e jour d'un {name} dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name {name}::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE {tableName} SET ';
		{updatefonc}
		$sql .= 'WHERE {idName}=\''.$this->{idName}.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un {name} dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name {name}::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'DELETE FROM {tableName} ';
		$sql .= 'WHERE {idName} = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}
	/**
	 * Recuperation de la liste des {name}s
	 *
	 * <p>Liste des {name}</p>
	 *
	 * @name {name}::_getList()
	 * @return array
	 */
	public function _getList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM {tableName} ' ;
		if ($this->{idName}>0)
		$sql.= ' WHERE id = \''.$this->{idName}.'\' ';
		$sql .= 'ORDER BY LIB ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				{listfonc}				
			}
		}
		return $listArray ;
	}

	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name {name}::_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->{idName}==0) return false;

		$listArray=$this->_getList();
		{fillfonc}
	}
	
	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name {name}::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>