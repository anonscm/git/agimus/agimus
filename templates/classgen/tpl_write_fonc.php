/**
 * @name {Cname}::_set{Uname}()
 * @param ${name} ({var})
 * @return void
*/
public function _set{Uname}(${name}) {
	$this->{name}  = ${name} ;
}
