/***********************************
Popup de suppression d'un element
***********************************/
function delete_item(target, lg)
{
	if(lg=='fr')
	{
		iAns = confirm("Etes-vous sur de vouloir supprimer cet élément ?");
	}else{
		iAns = confirm("Do you really want to delete this element ?");
	}

  if (iAns == true)
   {
     self.location.href = target;
   }
  else
   {
      return false;
   }
}

/**********************************************
Popup destine a  l'affichage des informations
***********************************************/
function popup_infos(page,largeur,hauteur,options,name)
{
  var newWindow = null;
  
  if ( options == "")
  {
    options = "menubar=no,scrollbars=no,statusbar=no, location=no"; 
  }
  var top=(screen.height-hauteur)/2;
  var left=(screen.width-largeur)/2;
  var options = "top="+top+",left="+left+",width="+largeur+",height="+hauteur+","+options;
  window.open(page,name,options);

}

/* Script cree par KevBrok ;-) */
	/*
	* Montre / Cache un div
	************************************************
	 Pour info : A ajouter dans les styles css (sinon �a ne fonctionne pas...)
	.cachediv {
	visibility: hidden;
	overflow: hidden;
	height: 1px;
	margin-top: -1px;
	position: absolute;
	}
	**************************************************
	*/
	function DivStatus( nom, numero )
		{
			var divID = nom + numero;
			if ( document.getElementById && document.getElementById( divID ) ) // Pour les navigateurs r�cents
				{
					Pdiv = document.getElementById( divID );
					PcH = true;
		 		}
			else if ( document.all && document.all[ divID ] ) // Pour les veilles versions
				{
					Pdiv = document.all[ divID ];
					PcH = true;
				}
			else if ( document.layers && document.layers[ divID ] ) // Pour les tr�s veilles versions
				{
					Pdiv = document.layers[ divID ];
					PcH = true;
				}
			else
				{
					
					PcH = false;
				}
			if ( PcH )
				{
					Pdiv.className = ( Pdiv.className == 'cachediv' ) ? '' : 'cachediv';
					Session.set("tdbselclass",Pdiv.className);
				}
		}
		
	/*
	* Cache tous les divs ayant le m�me pr�fixe
	*/
	function CacheTout( nom )
		{	
			var NumDiv = 1;
			if ( document.getElementById ) // Pour les navigateurs r�cents
				{
					while ( document.getElementById( nom + NumDiv) )
						{
							SetDiv = document.getElementById( nom + NumDiv );
							if ( SetDiv && SetDiv.className != 'cachediv' )
								{
									DivStatus( nom, NumDiv );
								}
							NumDiv++;
						}
				}
			else if ( document.all ) // Pour les veilles versions
				{
					while ( document.all[ nom + NumDiv ] )
						{
							SetDiv = document.all[ nom + NumDiv ];
							if ( SetDiv && SetDiv.className != 'cachediv' )
								{
									DivStatus( nom, NumDiv );
								}
							NumDiv++;
						}
				}
			else if ( document.layers ) // Pour les tr�s veilles versions
				{
					while ( document.layers[ nom + NumDiv ] )
						{
							SetDiv = document.layers[ nom + NumDiv ];
							if ( SetDiv && SetDiv.className != 'cachediv' )
								{
									DivStatus( nom, NumDiv );
								}
							NumDiv++;
						}
				}
		}
	
	/*
	* Montre tous les divs ayant le m�me pr�fixe
	*/
	function MontreTout( nom )
		{	
			var NumDiv = 1;
			if ( document.getElementById ) // Pour les navigateurs r�cents
				{
					while ( document.getElementById( nom + NumDiv) )
						{
							SetDiv = document.getElementById( nom + NumDiv );
							if ( SetDiv && SetDiv.className != '' )
								{
									DivStatus( nom, NumDiv );
								}
							NumDiv++;
						}
				}
			else if ( document.all ) // Pour les veilles versions
				{
					while ( document.all[ nom + NumDiv ] )
						{
							SetDiv = document.all[ nom + NumDiv ];
							if ( SetDiv && SetDiv.className != '' )
								{
									DivStatus( nom, NumDiv );
								}
							NumDiv++;
						}
				}
			else if ( document.layers ) // Pour les tr�s veilles versions
				{
					while ( document.layers[ nom + NumDiv ] )
						{
							SetDiv = document.layers[ nom + NumDiv ];
							if ( SetDiv && SetDiv.className != '' )
								{
									DivStatus( nom, NumDiv );
								}
							NumDiv++;
						}
				}
		}
		
	/*
	* Inverse les divs: Cache les divs visible et montre le divs cach�s :)
	*/
	function InverseTout( nom )
		{	
			var NumDiv = 1;
			if ( document.getElementById ) // Pour les navigateurs r�cents
				{
					while ( document.getElementById( nom + NumDiv ) )
						{
							SetDiv = document.getElementById( nom + NumDiv );
							DivStatus( nom, NumDiv );
							NumDiv++;
						}
				}
			else if ( document.all ) // Pour les veilles versions
				{
					while ( document.all[ nom + NumDiv ] )
						{
							SetDiv = document.all[ nom + NumDiv ];
							DivStatus( nom, NumDiv );
							NumDiv++;
						}
				}
			else if ( document.layers ) // Pour les tr�s veilles versions
				{
					while ( document.layers[ nom + NumDiv ] )
						{
							SetDiv = document.layers[ nom + NumDiv ];
							DivStatus( nom, NumDiv );
							NumDiv++;
						}
				}
		}

/*
Coche d'un ensemble de plusieurs checkboxes*/

function cocheTous(formname, nameCheckBoxes, valeur)
{
	var liste = document.forms[formname].elements[nameCheckBoxes];
	for (var i = 0; i < liste.length; i++) {
		
		liste[i].checked = valeur;
	}
}

function checkAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = true ;
}

function uncheckAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = false ;
}


		
/*
Script calendar
*/

moisX=["","Janvier","Fevrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Decembre"];
JourM=["Di","Lu","Ma","Me","Je","Ve","Sa"];

var fermable_microcal=true;
var select_old= null;

var startWeek=0;//debut de la semaine 0=dim,1=lun,...
var jourPause={0:true,6:true}; //jour de pause de la semaine
var jourFeriee={"1-1":"jour an","1-5":"fete du travail","8-5":"armistice","14-7":"fete nationale","15-8":"ascencion","1-11":"armistice","11-11":"toussain","25-12":"noel"};

//structure la date
function strucDate(dateX)
{
	return {"pos":dateX.getDay(),"jour":dateX.getDate(),"mois":dateX.getMonth()+1,"annee":dateX.getFullYear()};
}

var dateS= strucDate(new Date());//date Selectionn�
var dnow= strucDate(new Date());//date actuelle

//retourne le i�me jour du 1er du mois
function premJourMois(mois,annee)
{
	return (new Date(annee,mois-1,1).getDay());
}
//retourne le jour max du mois
function JmaxMois(mois,annee)
{
	return (new Date(annee,mois,0).getDate());
}


/* Test si une date  est correct*/
function testTypeDate(dateEntree)
{
	tst=false;
	try
	{
		rc=dateEntree.split("/");nd=new Date(rc[2],(rc[1]-1),rc[0]);
		tst=(rc[2]>1800&&rc[2]<2200&&rc[2]==nd.getFullYear()&&rc[1]==(nd.getMonth()+1)&&rc[0]==nd.getDate());
	} 
	catch(e) {}
	return tst;
}

//selection de la zone avec la souris
function choix(koi,code)
{
	if (code)
	{ 
		select_old= koi.style.background;
		koi.style.background ='orange';
	}else{
		koi.style.background =select_old;
	}
}


function testTravail(oldX,xx,jj,mm,aa)
{
	styleX="font-family:Arial;font-size:10pt;text-align:center;";
	styleX+=(oldX)?"":"color:#e0e0e0;";
	styleX+="cursor:hand;border-right:1px #e0e0e0 solid;border-bottom:1px #e0e0e0 solid;";
	if (jourPause[xx]!=null) styleX+="background:#f0f0f0;";
	if (jj==dnow.jour&&mm==dnow.mois&&aa==dnow.annee) styleX+="border:1px red solid;";
	return styleX;
}

//test si ann�e bissextile
function bissextile(annee) 
{
	return (annee%4==0 && annee %100!=0 || annee%400==0);
}

//Retourne le nombre de jour depuis le 1er janvier (num de semaine)
function nbJAnnee(dateX)
{
	var nb_mois=[,0,31,59,90,120,151,181,212,243,273,304,334];
	j=dateX.jour ; m=dateX.mois ; a=dateX.annee;
	nb=nb_mois[m]+j-1 ;
	if (bissextile(a) && m>2) nb++;
	return nb;
}

//affiche le calendrier
function view_microcal(actif,ki,source,mxS,axS)
{
	if (actif)
	{
		//decalage du mois su on clique sur -/+
		if (mxS!=-1)
		{
			clearTimeout(cc);
			document.getElementById(ki).focus();
			fermable_microcal=true;
			dateS.mois=mxS;
			dateS.annee=axS;
			if (dateS.mois<1){
				dateS.annee--;dateS.mois+=12;
			}
			if (dateS.mois>12) {
				dateS.annee++;dateS.mois-=12;
			}
		}
		//init
		Dstart=(premJourMois(dateS.mois,dateS.annee)+7-startWeek)%7;
		jmaxi=JmaxMois(dateS.mois,dateS.annee);
		jmaxiAvant=JmaxMois((dateS.mois-1),dateS.annee);
		//si on veux ajouter le numero de la semaine ...
		idxWeek=parseInt(nbJAnnee(strucDate(new Date(dateS.mois+'-01-'+dateS.annee)))/7,10)+1;

		ymaxi=parseInt((jmaxi+Dstart+1)/7,10);

		//generation du tableau
		//--ent�te
		htm="<table><tr style='font-size:10pt;font-family:Arial;text-align:center;'>";
		htm+="<td style='cursor:hand;' onclick=\"view_microcal(true,'"+ki+"','"+source+"',"+(dateS.mois-1)+","+dateS.annee+");\"><img src=\"../images/gif/arrow_left.gif\"></td>";
		htm+="<td colspan='5'> <b> "+moisX[dateS.mois]+"</b>&nbsp;"+dateS.annee+"</td>";
		htm+="<td style='cursor:hand;' onclick=\"view_microcal(true,'"+ki+"','"+source+"',"+(dateS.mois+1)+","+dateS.annee+")\">&nbsp;>>&nbsp;</td></tr>";
		//--corps
		htm+="<tr>";
//affichage des jours DLMMJVS
for (x=0;x<7;x++)
htm+="<td style='font-size:10px;font-family:Arial;'><b>"+JourM[(x+startWeek)%7]+"</b></td>";
htm+="</tr>"


//------------------------
for (y=0;y<=ymaxi;y++)
{
htm+="<tr>";
for (x=0;x<7;x++)
{
idxP=y*7+x-Dstart+1; //numero du jour
aa=dateS.annee;
xx=(x+startWeek)%7;
//jour du mois pr�cedent
if (idxP<=0)
{
jj=idxP+jmaxiAvant;mm=dateS.mois-1;
if (mm==0)
{mm=12;aa--;}
htm+="<td style='"+testTravail(false,xx,jj,mm,aa)+"' onmouseover='choix(this,true)' onmouseout='choix(this,false)' onclick=\""+ki+".value='"+((jj<10)?"0":"")+jj+"/"+((mm<10)?"0":"")+mm+"/"+aa+"';"+ki+".style.color='black';\">"+jj+"</td>";
}
else if (idxP>jmaxi) //jour du mois suivant
{
jj=idxP-jmaxi;mm=dateS.mois+1;
if (mm==13)
{mm=1;aa++;}

htm+="<td style='"+testTravail(false,xx,jj,mm,aa)+"' onmouseover='choix(this,true)' onmouseout='choix(this,false)' onclick=\"document.getElementById('"+ki+"').value='"+((jj<10)?"0":"")+jj+"/"+((mm<10)?"0":"")+mm+"/"+aa+"';document.getElementById('"+ki+"').style.color='black';\">"+jj+"</td>";}
else //jour du mois en cours
{
jj=idxP;mm=dateS.mois;
htm+="<td style='"+testTravail(true,xx,jj,mm,aa)+"' onmouseover='choix(this,true)' onmouseout='choix(this,false)' onclick=\"document.getElementById('"+ki+"').value='"+((jj<10)?"0":"")+jj+"/"+((mm<10)?"0":"")+mm+"/"+aa+"';document.getElementById('"+ki+"').style.color='black';\">"+jj+"</td>";}
}
htm+="</tr>"
}//-------------------------
htm+="</table>"
//affiche le tableau
document.getElementById(source).innerHTML=htm;
document.getElementById(source).style.visibility="";
} else
{
//ferme le calendrier
if (fermable_microcal)
   cc=setTimeout("document.getElementById('"+source+"').style.visibility='hidden'",500);
}
}
