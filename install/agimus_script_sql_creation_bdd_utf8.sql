-- phpMyAdmin SQL Dump
-- version 2.8.2.4
-- http://www.phpmyadmin.net
-- 
-- Serveur: cribd.univ-pau.fr
-- Généré le : Mercredi 17 Novembre 2010 à 16:30
-- Version du serveur: 5.0.51
-- Version de PHP: 4.4.1
-- 
-- Base de données: `agimus_maquette`
-- 

-- --------------------------------------------------------

-- 
-- Structure de la table `admin_right`
-- 

DROP TABLE IF EXISTS `admin_right`;
CREATE TABLE `admin_right` (
  `RIGHT_ID` int(11) NOT NULL auto_increment,
  `RIGHT_CODE` varchar(32) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`RIGHT_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

-- 
-- Contenu de la table `admin_right`
-- 

INSERT INTO `admin_right` VALUES (1, '_USER_LIST_ACCESS_');
INSERT INTO `admin_right` VALUES (2, '_USER_CREATE_');
INSERT INTO `admin_right` VALUES (3, '_USER_UPDATE_');
INSERT INTO `admin_right` VALUES (4, '_USER_DELETE_');
INSERT INTO `admin_right` VALUES (5, '_USER_RESTORE_');
INSERT INTO `admin_right` VALUES (6, '_GROUP_LIST_ACCESS_');
INSERT INTO `admin_right` VALUES (7, '_GROUP_CREATE_');
INSERT INTO `admin_right` VALUES (8, '_GROUP_UPDATE_');
INSERT INTO `admin_right` VALUES (9, '_GROUP_DELETE_');
INSERT INTO `admin_right` VALUES (10, '_USER_DEACTIVATE_');
INSERT INTO `admin_right` VALUES (11, '_LOGIN_PWD_UPDATE_');
INSERT INTO `admin_right` VALUES (12, '_RIGHT_LIST_ACCESS_');
INSERT INTO `admin_right` VALUES (13, '_RIGHT_UPDATE_');

-- --------------------------------------------------------

-- 
-- Structure de la table `admin_right_acl`
-- 

DROP TABLE IF EXISTS `admin_right_acl`;
CREATE TABLE `admin_right_acl` (
  `RIGHT_ID` int(11) NOT NULL default '0',
  `INT_COD` int(11) NOT NULL default '0',
  `GID` int(11) NOT NULL default '0',
  `ACL` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`RIGHT_ID`,`INT_COD`,`GID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `admin_right_acl`
-- 

INSERT INTO `admin_right_acl` VALUES (1, 1, 1, 1);
INSERT INTO `admin_right_acl` VALUES (2, 1, 1, 1);
INSERT INTO `admin_right_acl` VALUES (3, 1, 1, 1);
INSERT INTO `admin_right_acl` VALUES (4, 1, 1, 1);
INSERT INTO `admin_right_acl` VALUES (5, 1, 1, 1);
INSERT INTO `admin_right_acl` VALUES (6, 1, 1, 1);
INSERT INTO `admin_right_acl` VALUES (7, 1, 1, 1);
INSERT INTO `admin_right_acl` VALUES (8, 1, 1, 1);
INSERT INTO `admin_right_acl` VALUES (9, 1, 1, 1);
INSERT INTO `admin_right_acl` VALUES (10, 1, 1, 1);
INSERT INTO `admin_right_acl` VALUES (11, 1, 1, 1);
INSERT INTO `admin_right_acl` VALUES (12, 1, 1, 1);
INSERT INTO `admin_right_acl` VALUES (13, 1, 1, 1);
INSERT INTO `admin_right_acl` VALUES (7, 5, 12, 1);
INSERT INTO `admin_right_acl` VALUES (3, 4, 12, 1);
INSERT INTO `admin_right_acl` VALUES (7, 3, 12, 1);
INSERT INTO `admin_right_acl` VALUES (3, 0, 12, 1);
INSERT INTO `admin_right_acl` VALUES (9, 5, 12, 1);
INSERT INTO `admin_right_acl` VALUES (5, 4, 12, 1);
INSERT INTO `admin_right_acl` VALUES (9, 3, 12, 1);
INSERT INTO `admin_right_acl` VALUES (5, 0, 12, 1);
INSERT INTO `admin_right_acl` VALUES (6, 5, 12, 1);
INSERT INTO `admin_right_acl` VALUES (1, 4, 12, 1);
INSERT INTO `admin_right_acl` VALUES (6, 3, 12, 1);
INSERT INTO `admin_right_acl` VALUES (1, 0, 12, 1);
INSERT INTO `admin_right_acl` VALUES (8, 5, 12, 1);
INSERT INTO `admin_right_acl` VALUES (4, 4, 12, 1);
INSERT INTO `admin_right_acl` VALUES (8, 3, 12, 1);
INSERT INTO `admin_right_acl` VALUES (4, 0, 12, 1);
INSERT INTO `admin_right_acl` VALUES (11, 5, 12, 1);
INSERT INTO `admin_right_acl` VALUES (10, 4, 12, 1);
INSERT INTO `admin_right_acl` VALUES (11, 3, 12, 1);
INSERT INTO `admin_right_acl` VALUES (10, 0, 12, 1);
INSERT INTO `admin_right_acl` VALUES (12, 5, 12, 1);
INSERT INTO `admin_right_acl` VALUES (2, 4, 12, 1);
INSERT INTO `admin_right_acl` VALUES (12, 3, 12, 1);
INSERT INTO `admin_right_acl` VALUES (2, 0, 12, 1);
INSERT INTO `admin_right_acl` VALUES (13, 5, 12, 1);
INSERT INTO `admin_right_acl` VALUES (13, 4, 12, 1);
INSERT INTO `admin_right_acl` VALUES (13, 3, 12, 1);
INSERT INTO `admin_right_acl` VALUES (13, 0, 12, 1);
INSERT INTO `admin_right_acl` VALUES (2, 5, 12, 1);
INSERT INTO `admin_right_acl` VALUES (12, 4, 12, 1);
INSERT INTO `admin_right_acl` VALUES (2, 3, 12, 1);
INSERT INTO `admin_right_acl` VALUES (12, 0, 12, 1);
INSERT INTO `admin_right_acl` VALUES (10, 5, 12, 1);
INSERT INTO `admin_right_acl` VALUES (11, 4, 12, 1);
INSERT INTO `admin_right_acl` VALUES (10, 3, 12, 1);
INSERT INTO `admin_right_acl` VALUES (11, 0, 12, 1);
INSERT INTO `admin_right_acl` VALUES (4, 5, 12, 1);
INSERT INTO `admin_right_acl` VALUES (8, 4, 12, 1);
INSERT INTO `admin_right_acl` VALUES (4, 3, 12, 1);
INSERT INTO `admin_right_acl` VALUES (8, 0, 12, 1);
INSERT INTO `admin_right_acl` VALUES (1, 5, 12, 1);
INSERT INTO `admin_right_acl` VALUES (6, 4, 12, 1);
INSERT INTO `admin_right_acl` VALUES (1, 3, 12, 1);
INSERT INTO `admin_right_acl` VALUES (6, 0, 12, 1);
INSERT INTO `admin_right_acl` VALUES (5, 5, 12, 1);
INSERT INTO `admin_right_acl` VALUES (9, 4, 12, 1);
INSERT INTO `admin_right_acl` VALUES (5, 3, 12, 1);
INSERT INTO `admin_right_acl` VALUES (9, 0, 12, 1);
INSERT INTO `admin_right_acl` VALUES (3, 5, 12, 1);
INSERT INTO `admin_right_acl` VALUES (7, 4, 12, 1);
INSERT INTO `admin_right_acl` VALUES (3, 3, 12, 1);
INSERT INTO `admin_right_acl` VALUES (7, 0, 12, 1);

-- --------------------------------------------------------

-- 
-- Structure de la table `admin_right_libelle`
-- 

DROP TABLE IF EXISTS `admin_right_libelle`;
CREATE TABLE `admin_right_libelle` (
  `RIGHT_ID` int(11) NOT NULL default '0',
  `LANG` varchar(16) collate utf8_unicode_ci NOT NULL default '',
  `LIBELLE` varchar(64) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`RIGHT_ID`,`LANG`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `admin_right_libelle`
-- 

INSERT INTO `admin_right_libelle` VALUES (1, 'fr', 'Accéder à la liste des utilisateurs');
INSERT INTO `admin_right_libelle` VALUES (1, 'en', 'Access to the list of users');
INSERT INTO `admin_right_libelle` VALUES (2, 'fr', 'Créer un utilisateur');
INSERT INTO `admin_right_libelle` VALUES (2, 'en', 'Create an user');
INSERT INTO `admin_right_libelle` VALUES (3, 'fr', 'Modifier un utilisateur');
INSERT INTO `admin_right_libelle` VALUES (3, 'en', 'Update an user');
INSERT INTO `admin_right_libelle` VALUES (4, 'fr', 'Supprimer un utilisateur');
INSERT INTO `admin_right_libelle` VALUES (4, 'en', 'Delete an user');
INSERT INTO `admin_right_libelle` VALUES (5, 'fr', 'Réactiver un utilisateur');
INSERT INTO `admin_right_libelle` VALUES (5, 'en', 'Restore an user');
INSERT INTO `admin_right_libelle` VALUES (6, 'fr', 'Accéder à la liste des groupes d''utilisateurs');
INSERT INTO `admin_right_libelle` VALUES (6, 'en', 'Access to the list of groups of users');
INSERT INTO `admin_right_libelle` VALUES (7, 'fr', 'Créer un groupe d''utilisateurs');
INSERT INTO `admin_right_libelle` VALUES (7, 'en', 'Create a group of users');
INSERT INTO `admin_right_libelle` VALUES (8, 'fr', 'Modifier un groupe d''utilisateurs');
INSERT INTO `admin_right_libelle` VALUES (8, 'en', 'Update a group of users');
INSERT INTO `admin_right_libelle` VALUES (9, 'fr', 'Supprimer un groupe d''utilisateurs');
INSERT INTO `admin_right_libelle` VALUES (9, 'en', 'Delete a group of users');
INSERT INTO `admin_right_libelle` VALUES (10, 'fr', 'Désactiver un utilisateur');
INSERT INTO `admin_right_libelle` VALUES (10, 'en', 'Deactivate an user');
INSERT INTO `admin_right_libelle` VALUES (11, 'fr', 'Mettre à jour son login et son mot de passe.');
INSERT INTO `admin_right_libelle` VALUES (11, 'en', 'Update login and password.');
INSERT INTO `admin_right_libelle` VALUES (12, 'fr', 'Accès à la liste des droits.');
INSERT INTO `admin_right_libelle` VALUES (12, 'en', 'Access to the list of rights.');
INSERT INTO `admin_right_libelle` VALUES (13, 'fr', 'Mettre à jour les droits.');
INSERT INTO `admin_right_libelle` VALUES (13, 'en', 'Update the rights.');

-- --------------------------------------------------------

-- 
-- Structure de la table `appl_group`
-- 

DROP TABLE IF EXISTS `appl_group`;
CREATE TABLE `appl_group` (
  `GID` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`GID`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

-- 
-- Contenu de la table `appl_group`
-- 

INSERT INTO `appl_group` VALUES (1);
INSERT INTO `appl_group` VALUES (2);
INSERT INTO `appl_group` VALUES (8);
INSERT INTO `appl_group` VALUES (11);
INSERT INTO `appl_group` VALUES (12);
INSERT INTO `appl_group` VALUES (13);

-- --------------------------------------------------------

-- 
-- Structure de la table `appl_group_libelle`
-- 

DROP TABLE IF EXISTS `appl_group_libelle`;
CREATE TABLE `appl_group_libelle` (
  `GID` int(11) NOT NULL default '0',
  `LANG` varchar(16) collate utf8_unicode_ci NOT NULL default '',
  `LIBELLE` varchar(32) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`GID`,`LANG`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `appl_group_libelle`
-- 

INSERT INTO `appl_group_libelle` VALUES (1, 'fr', 'Administrateur');
INSERT INTO `appl_group_libelle` VALUES (1, 'en', 'Application Manager');
INSERT INTO `appl_group_libelle` VALUES (2, 'fr', 'Tous les utilisateurs');
INSERT INTO `appl_group_libelle` VALUES (2, 'en', 'All users');
INSERT INTO `appl_group_libelle` VALUES (8, 'en', 'Administration');
INSERT INTO `appl_group_libelle` VALUES (8, 'fr', 'Administration');
INSERT INTO `appl_group_libelle` VALUES (11, 'en', 'Lecteur AGIMUS');
INSERT INTO `appl_group_libelle` VALUES (11, 'fr', 'Lecteur AGIMUS');
INSERT INTO `appl_group_libelle` VALUES (12, 'en', 'AGIMUS administrator');
INSERT INTO `appl_group_libelle` VALUES (12, 'fr', 'Administrateur AGIMUS');
INSERT INTO `appl_group_libelle` VALUES (13, 'en', 'Pilote');
INSERT INTO `appl_group_libelle` VALUES (13, 'fr', 'Pilote AGIMUS');

-- --------------------------------------------------------

-- 
-- Structure de la table `appl_languages`
-- 

DROP TABLE IF EXISTS `appl_languages`;
CREATE TABLE `appl_languages` (
  `LANG` varchar(16) collate utf8_unicode_ci NOT NULL default '',
  `LIBELLE` varchar(32) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`LANG`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `appl_languages`
-- 

INSERT INTO `appl_languages` VALUES ('fr', 'français');
INSERT INTO `appl_languages` VALUES ('en', 'anglais');

-- --------------------------------------------------------

-- 
-- Structure de la table `appl_log_error`
-- 

DROP TABLE IF EXISTS `appl_log_error`;
CREATE TABLE `appl_log_error` (
  `MSG_ID` int(11) NOT NULL default '0',
  `MOD_ID` int(11) NOT NULL default '0',
  `INT_COD` int(11) NOT NULL default '0',
  `ERROR_DATE` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`MSG_ID`,`MOD_ID`,`INT_COD`,`ERROR_DATE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `appl_log_error`
-- 

-- --------------------------------------------------------

-- 
-- Structure de la table `appl_modules`
-- 

DROP TABLE IF EXISTS `appl_modules`;
CREATE TABLE `appl_modules` (
  `MOD_ID` int(11) NOT NULL auto_increment,
  `MOD_NAME` varchar(32) collate utf8_unicode_ci NOT NULL default '',
  `MOD_TITLE` varchar(64) collate utf8_unicode_ci NOT NULL default '',
  `MOD_ORDER` int(11) NOT NULL default '0',
  `MOD_ENABLED` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`MOD_ID`),
  UNIQUE KEY `MOD_NAME` (`MOD_NAME`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- 
-- Contenu de la table `appl_modules`
-- 

INSERT INTO `appl_modules` VALUES (1, 'admin', 'Administration', 1, 1);
INSERT INTO `appl_modules` VALUES (2, 'gestflux', 'Gestion des Flux', 2, 1);
INSERT INTO `appl_modules` VALUES (3, 'ref', 'Référentiels', 3, 1);
INSERT INTO `appl_modules` VALUES (4, 'restit', 'Tableaux de bord', 4, 1);

-- --------------------------------------------------------

-- 
-- Structure de la table `appl_modules_acl`
-- 

DROP TABLE IF EXISTS `appl_modules_acl`;
CREATE TABLE `appl_modules_acl` (
  `MOD_ID` int(11) NOT NULL default '0',
  `RIGHT_ID` int(11) NOT NULL default '0',
  `INT_COD` int(11) NOT NULL default '0',
  `GID` int(11) NOT NULL default '0',
  `ACL` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`MOD_ID`,`RIGHT_ID`,`INT_COD`,`GID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `appl_modules_acl`
-- 

INSERT INTO `appl_modules_acl` VALUES (1, 1, 1, 1, 1);
INSERT INTO `appl_modules_acl` VALUES (2, 1, 1, 1, 1);
INSERT INTO `appl_modules_acl` VALUES (3, 1, 1, 1, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 1, 1, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 4, 2, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 0, 2, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 6, 13, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 3, 13, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 0, 13, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 7, 11, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 0, 11, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 2, 8, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 0, 8, 1);
INSERT INTO `appl_modules_acl` VALUES (1, 1, 5, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 4, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (1, 1, 3, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 0, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (3, 1, 2, 8, 1);
INSERT INTO `appl_modules_acl` VALUES (3, 1, 0, 8, 1);
INSERT INTO `appl_modules_acl` VALUES (2, 1, 5, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (3, 1, 4, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (2, 1, 3, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (2, 1, 6, 0, 1);
INSERT INTO `appl_modules_acl` VALUES (3, 1, 6, 0, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 6, 0, 1);
INSERT INTO `appl_modules_acl` VALUES (3, 1, 0, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (3, 1, 5, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (2, 1, 4, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (3, 1, 3, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (2, 1, 0, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 5, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (1, 1, 4, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 3, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (1, 1, 0, 12, 1);
INSERT INTO `appl_modules_acl` VALUES (4, 1, 8, 13, 1);

-- --------------------------------------------------------

-- 
-- Structure de la table `appl_modules_right`
-- 

DROP TABLE IF EXISTS `appl_modules_right`;
CREATE TABLE `appl_modules_right` (
  `RIGHT_ID` int(11) NOT NULL auto_increment,
  `RIGHT_CODE` varchar(32) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`RIGHT_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- 
-- Contenu de la table `appl_modules_right`
-- 

INSERT INTO `appl_modules_right` VALUES (1, '_ACCESS_');

-- --------------------------------------------------------

-- 
-- Structure de la table `appl_modules_right_libelle`
-- 

DROP TABLE IF EXISTS `appl_modules_right_libelle`;
CREATE TABLE `appl_modules_right_libelle` (
  `RIGHT_ID` int(11) NOT NULL default '0',
  `LANG` varchar(16) collate utf8_unicode_ci NOT NULL default '',
  `LIBELLE` varchar(64) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`RIGHT_ID`,`LANG`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `appl_modules_right_libelle`
-- 

INSERT INTO `appl_modules_right_libelle` VALUES (1, 'fr', 'Accès au module');
INSERT INTO `appl_modules_right_libelle` VALUES (1, 'en', 'Module access');

-- --------------------------------------------------------

-- 
-- Structure de la table `appl_msg`
-- 

DROP TABLE IF EXISTS `appl_msg`;
CREATE TABLE `appl_msg` (
  `MSG_ID` int(11) NOT NULL auto_increment,
  `MOD_ID` int(11) NOT NULL default '0',
  `MSG_CODE` varchar(64) collate utf8_unicode_ci NOT NULL default '',
  `MSG_TYPE` varchar(16) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`MSG_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=91 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=91 ;

-- 
-- Contenu de la table `appl_msg`
-- 

INSERT INTO `appl_msg` VALUES (7, 0, '_ERROR_NAME_MISSING_', 'error');
INSERT INTO `appl_msg` VALUES (8, 0, '_ERROR_FIRSTNAME_MISSING_', 'error');
INSERT INTO `appl_msg` VALUES (9, 0, '_ERROR_SAME_NAME_ALREADY_EXISTS_', 'error');
INSERT INTO `appl_msg` VALUES (16, 0, '_ERROR_DELETE_SUPERADMIN_', 'error');
INSERT INTO `appl_msg` VALUES (17, 0, '_PERSON_DELETE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (18, 0, '_PERSON_UPDATE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (19, 0, '_PERSON_RESTORE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (20, 0, '_PERSON_CREATE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (26, 0, '_DATABASE_ERROR_', 'error');
INSERT INTO `appl_msg` VALUES (27, 0, '_STRUCT_DELETE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (28, 0, '_STRUCT_UPDATE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (29, 0, '_STRUCT_RESTORE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (30, 0, '_STRUCT_CREATE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (31, 0, '_STRUCT_DEACTIVATE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (32, 0, '_PERSON_DEACTIVATE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (33, 0, '_ERROR_CORPORATE_NAME_MISSING_', 'error');
INSERT INTO `appl_msg` VALUES (34, 0, '_ERROR_TOWN_MISSING_', 'error');
INSERT INTO `appl_msg` VALUES (35, 0, '_FILE_DELETE_FAILED_', 'error');
INSERT INTO `appl_msg` VALUES (36, 0, '_FILE_TO_UPDATE_DELETE_FAILED_', 'error');
INSERT INTO `appl_msg` VALUES (37, 0, '_PERSON_DEACTIVATE_NOT_ALLOWED_', 'error');
INSERT INTO `appl_msg` VALUES (38, 0, '_ERROR_LOGIN_MISSING_', 'error');
INSERT INTO `appl_msg` VALUES (39, 0, '_ERROR_PWD1_MISSING_', 'error');
INSERT INTO `appl_msg` VALUES (40, 0, '_ERROR_PWD2_MISSING_', 'error');
INSERT INTO `appl_msg` VALUES (41, 0, '_ERROR_PWD2_PWD1_NO_MATCH_', 'error');
INSERT INTO `appl_msg` VALUES (42, 0, '_ERROR_PWD_ALREADY_USED_', 'error');
INSERT INTO `appl_msg` VALUES (43, 0, '_ERROR_LOGIN_ALREADY_USED_', 'error');
INSERT INTO `appl_msg` VALUES (44, 0, '_ERROR_GROUP_LABEL_MISSING_', 'error');
INSERT INTO `appl_msg` VALUES (45, 0, '_ERROR_SAME_GROUP_NAME_ALREADY_EXISTS_', 'error');
INSERT INTO `appl_msg` VALUES (46, 0, '_ERROR_USER_IN_MAIN_OTHER_GROUP_', 'error');
INSERT INTO `appl_msg` VALUES (47, 0, '_GROUP_DELETE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (48, 0, '_GROUP_UPDATE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (49, 0, '_GROUP_CREATE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (50, 0, '_MODULE_ACCESS_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (51, 0, '_FONCTION_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (52, 0, '_FILE_TO_UPDATE_SAVED_MOVE_FAILED_', '');
INSERT INTO `appl_msg` VALUES (53, 0, '_FILE_UPLOAD_TOO_BIG_', '');
INSERT INTO `appl_msg` VALUES (57, 0, '_ERROR_LIBELLE_MISSING_', '');
INSERT INTO `appl_msg` VALUES (58, 0, '_ERROR_LIBELLE_ALREADY_EXISTS_', '');
INSERT INTO `appl_msg` VALUES (60, 0, '_GROUPING_DELETE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (61, 0, '_GROUPING_UPDATE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (62, 0, '_GROUPING_CREATE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (63, 0, '_ERROR_CODE_MISSING_', '');
INSERT INTO `appl_msg` VALUES (64, 0, '_ERROR_CODE_ALREADY_EXISTS_', '');
INSERT INTO `appl_msg` VALUES (65, 0, '_PROFILE_ACCESS_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (66, 0, '_PROFILE_DELETE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (67, 0, '_PROFILE_UPDATE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (68, 0, '_PROFILE_CREATE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (69, 0, '_ERROR_NUMBER_WRONG_FORMAT_', '');
INSERT INTO `appl_msg` VALUES (90, 0, '_ERROR_STATUS_MISSING_', '');
INSERT INTO `appl_msg` VALUES (70, 0, '_DENOMBR_DELETE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (71, 0, '_DENOMBR_UPDATE_NOT_ALLOWED_', '');
INSERT INTO `appl_msg` VALUES (72, 0, '_DENOMBR_CREATE_NOT_ALLOWED_', '');

-- --------------------------------------------------------

-- 
-- Structure de la table `appl_msg_libelle`
-- 

DROP TABLE IF EXISTS `appl_msg_libelle`;
CREATE TABLE `appl_msg_libelle` (
  `MSG_ID` int(11) NOT NULL default '0',
  `LANG` varchar(16) collate utf8_unicode_ci NOT NULL default '',
  `LIBELLE` varchar(256) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`MSG_ID`,`LANG`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `appl_msg_libelle`
-- 

INSERT INTO `appl_msg_libelle` VALUES (7, 'fr', 'Le nom n''est pas renseigné. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (8, 'fr', 'Le prénom n''est pas renseigné. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (9, 'fr', 'Ces nom et prénom sont déjà utilisés pour une autre personne. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (7, 'en', 'The last name is missing. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (8, 'en', 'The first name is missing. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (9, 'en', 'The first and last names are already used by another person. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (16, 'en', 'The administrator can''t be deleted. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (16, 'fr', 'L''administrateur ne peut être supprimé. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (17, 'en', 'You''re not allowed to delete this person. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (17, 'fr', 'Vous n''êtes pas autorisé à supprimer cette personne. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (18, 'en', 'You''re not allowed to modify this person. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (18, 'fr', 'Vous n''êtes pas autorisé à modifier cette personne. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (19, 'en', 'You''re not allowed to restore this person. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (19, 'fr', 'Vous n''êtes pas autorisé à réactiver cette personne. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (20, 'en', 'You''re not allowed to create a person. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (20, 'fr', 'Vous n''êtes pas autorisé à créer une personne. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (26, 'en', 'An error occured whith the database. Please, try again.');
INSERT INTO `appl_msg_libelle` VALUES (26, 'fr', 'Un problème est survenu dans la base de données. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (27, 'en', 'You''re not allowed to delete this firm. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (27, 'fr', 'Vous n''êtes pas autorisé à supprimer cette société. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (28, 'en', 'You''re not allowed to modify this firm. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (28, 'fr', 'Vous n''êtes pas autorisé à modifier cette société. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (29, 'en', 'You''re not allowed to restore this firm. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (29, 'fr', 'Vous n''êtes pas autorisé à réactiver cette société. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (30, 'en', 'You''re not allowed to create a firm. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (30, 'fr', 'Vous n''êtes pas autorisé à créer une société. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (31, 'en', 'You''re not allowed to deactivate a firm. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (31, 'fr', 'Vous n''êtes pas autorisé à désactiver une société. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (32, 'en', 'You''re not allowed to deactivate a person. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (32, 'fr', 'Vous n''êtes pas autorisé à désactiver une personne. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (33, 'fr', 'La raison sociale n''est pas renseignée. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (33, 'en', 'The corporate name is missing. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (34, 'fr', 'La commune n''est pas renseignée. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (34, 'en', 'The town is missing. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (35, 'fr', 'Une erreur s''est produite : le fichier n''a pas été supprimé.');
INSERT INTO `appl_msg_libelle` VALUES (35, 'en', 'An error occured : the file has not been deleted.');
INSERT INTO `appl_msg_libelle` VALUES (36, 'fr', 'Une erreur s''est produite : le fichier à mettre à jour n''a pas été supprimé. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (36, 'en', 'An error occured : the file to update has not been deleted. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (37, 'en', 'You''re not allowed to deactivate this person. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (37, 'fr', 'Vous n''êtes pas autorisé à désactiver cette personne. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (38, 'en', 'The login is missing. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (38, 'fr', 'Il manque le login. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (39, 'en', 'The password is missing. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (39, 'fr', 'Il manque le mot de passe. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (40, 'en', 'The second password is missing. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (40, 'fr', 'Il manque le second mot de passe. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (41, 'en', 'The first and second password mismatch. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (41, 'fr', 'les premier et second mots de passe sont différents. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (42, 'en', 'You have already used this password. Please, choose an other one.');
INSERT INTO `appl_msg_libelle` VALUES (42, 'fr', 'Vous avez déjà utilisé ce mot de passe. Choisissez-en un autre, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (43, 'en', 'This login is already used. Please, choose an other one.');
INSERT INTO `appl_msg_libelle` VALUES (43, 'fr', 'Ce login est déjà utilisé. Choisissez-en un autre, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (44, 'en', 'A label of the group is missing. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (44, 'fr', 'Il manque un libellé pour le groupe. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (45, 'en', 'A label for the group already exists. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (45, 'fr', 'Il existe un libellé identique pour le groupe. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (46, 'en', 'An user can''t be in main and other group. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (46, 'fr', 'Un utilisateur ne peut être à la fois dans le groupe principal et dans le groupe secondaire. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (47, 'en', 'You''re not allowed to delete this group. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (47, 'fr', 'Vous n''êtes pas autorisé à supprimer ce groupe. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (48, 'en', 'You''re not allowed to update this group. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (48, 'fr', 'Vous n''êtes pas autorisé à mettre à jour ce groupe. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (49, 'en', 'You''re not allowed to create a group. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (49, 'fr', 'Vous n''êtes pas autorisé à créer un groupe. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (50, 'en', 'You''re not allowed to access this module. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (50, 'fr', 'L''accès à ce module ne vous est pas autorisé. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (51, 'en', 'You''re not allowed to use this functionality. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (51, 'fr', 'Vous n''êtes pas autorisé à utiliser cette fonctionnalité. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (52, 'fr', 'Le fichier de sauvegarde pour la mise à jour na pas été récupéré. Veuillez contacter l''administrateur.');
INSERT INTO `appl_msg_libelle` VALUES (52, 'en', 'The file of maintenance was not recovered. Please, contact the administrator.');
INSERT INTO `appl_msg_libelle` VALUES (53, 'fr', 'Le fichier est trop gros. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (53, 'en', 'The file is too big. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (57, 'fr', 'Il manque le libellé. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (57, 'en', 'The name is missing. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (58, 'fr', 'Ce libellé est déjà utilisé. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (58, 'en', 'This name is already used. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (60, 'en', 'You''re not allowed to delete a grouping. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (60, 'fr', 'Vous n''êtes pas autorisé à supprimer un regroupement. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (61, 'en', 'You''re not allowed to update a grouping. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (61, 'fr', 'Vous n''êtes pas autorisé à modifier un regroupement. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (62, 'en', 'You''re not allowed to create a grouping. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (62, 'fr', 'Vous n''êtes pas autorisé à créer un regroupement. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (63, 'en', 'The code is missing. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (63, 'fr', 'Il manque le code. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (64, 'fr', 'Ce code est déjà utilisé. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (64, 'en', 'This code is already used. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (65, 'en', 'You''re not allowed to access the list of roles. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (65, 'fr', 'Vous n''êtes pas autorisé à accéder à la liste des rôles. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (66, 'en', 'You''re not allowed to delete a role. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (66, 'fr', 'Vous n''êtes pas autorisé à supprimer un rôle. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (67, 'en', 'You''re not allowed to update a role. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (67, 'fr', 'Vous n''êtes pas autorisé à modifier un rôle. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (68, 'en', 'You''re not allowed to create a role. Sorry.');
INSERT INTO `appl_msg_libelle` VALUES (68, 'fr', 'Vous n''êtes pas autorisé à créer un rôle. Désolé.');
INSERT INTO `appl_msg_libelle` VALUES (69, 'en', 'Ce nombre n''est pas correct. Recommencez, s''il vous plait.');
INSERT INTO `appl_msg_libelle` VALUES (69, 'fr', 'This number is not correct. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (90, 'en', 'The status is missing. Please, do it again.');
INSERT INTO `appl_msg_libelle` VALUES (90, 'fr', 'Il manque le statut. Recommencez, s''il vous plait.');

-- --------------------------------------------------------

-- 
-- Structure de la table `appl_session_log`
-- 

DROP TABLE IF EXISTS `appl_session_log`;
CREATE TABLE `appl_session_log` (
  `SESSLOG_ID` int(11) NOT NULL auto_increment,
  `SESSID` varchar(16) collate utf8_unicode_ci NOT NULL default '',
  `INT_COD` int(11) NOT NULL default '0',
  `LANG` varchar(16) collate utf8_unicode_ci NOT NULL default '',
  `DATE_START` datetime NOT NULL default '0000-00-00 00:00:00',
  `ADDRESS_IP` varchar(16) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`SESSLOG_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

-- 
-- Contenu de la table `appl_session_log`
-- 

-- --------------------------------------------------------

-- 
-- Structure de la table `appl_session_log_action`
-- 

DROP TABLE IF EXISTS `appl_session_log_action`;
CREATE TABLE `appl_session_log_action` (
  `SESSLOG_ID` int(11) NOT NULL default '0',
  `INT_COD` int(11) NOT NULL default '0',
  `MOD_ID` int(11) NOT NULL default '0',
  `PAGE_ORDER` int(11) NOT NULL default '0',
  `TIME_PAGE` int(11) NOT NULL default '0',
  `URL_PAGE` varchar(128) collate utf8_unicode_ci NOT NULL default '',
  `ACTION` varchar(32) collate utf8_unicode_ci NOT NULL default 'afficher',
  PRIMARY KEY  (`SESSLOG_ID`,`PAGE_ORDER`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `appl_session_log_action`
-- 

-- --------------------------------------------------------

-- 
-- Structure de la table `appl_user_group`
-- 

DROP TABLE IF EXISTS `appl_user_group`;
CREATE TABLE `appl_user_group` (
  `GID` int(11) NOT NULL default '2',
  `INT_COD` int(11) NOT NULL default '0',
  `MAIN_GROUP_YN` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`GID`,`INT_COD`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `appl_user_group`
-- 

INSERT INTO `appl_user_group` VALUES (1, 1, 1);
INSERT INTO `appl_user_group` VALUES (8, 2, 1);
INSERT INTO `appl_user_group` VALUES (13, 3, 0);
INSERT INTO `appl_user_group` VALUES (2, 4, 1);
INSERT INTO `appl_user_group` VALUES (12, 3, 1);
INSERT INTO `appl_user_group` VALUES (12, 5, 1);
INSERT INTO `appl_user_group` VALUES (12, 4, 1);
INSERT INTO `appl_user_group` VALUES (13, 6, 1);
INSERT INTO `appl_user_group` VALUES (11, 7, 1);
INSERT INTO `appl_user_group` VALUES (13, 8, 1);

-- --------------------------------------------------------

-- 
-- Structure de la table `gestflux_right`
-- 

DROP TABLE IF EXISTS `gestflux_right`;
CREATE TABLE `gestflux_right` (
  `RIGHT_ID` int(11) NOT NULL auto_increment,
  `RIGHT_CODE` varchar(32) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`RIGHT_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

-- 
-- Contenu de la table `gestflux_right`
-- 

INSERT INTO `gestflux_right` VALUES (20, '_ANALYSE_CNX_LOGS');
INSERT INTO `gestflux_right` VALUES (21, 'IMPORT_SQL');
INSERT INTO `gestflux_right` VALUES (22, 'EXPORT_SQL');
INSERT INTO `gestflux_right` VALUES (23, 'LIST_JOBS');
INSERT INTO `gestflux_right` VALUES (24, 'DELETE_JOB');
INSERT INTO `gestflux_right` VALUES (25, 'UPDATE_JOB');
INSERT INTO `gestflux_right` VALUES (26, 'CREATE_JOB');
INSERT INTO `gestflux_right` VALUES (27, 'RESTART_JOB');

-- --------------------------------------------------------

-- 
-- Structure de la table `gestflux_right_acl`
-- 

DROP TABLE IF EXISTS `gestflux_right_acl`;
CREATE TABLE `gestflux_right_acl` (
  `RIGHT_ID` int(11) NOT NULL default '0',
  `INT_COD` int(11) NOT NULL default '0',
  `GID` int(11) NOT NULL default '0',
  `ACL` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`RIGHT_ID`,`INT_COD`,`GID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `gestflux_right_acl`
-- 

INSERT INTO `gestflux_right_acl` VALUES (20, 1, 1, 1);
INSERT INTO `gestflux_right_acl` VALUES (21, 1, 1, 1);
INSERT INTO `gestflux_right_acl` VALUES (22, 1, 1, 1);
INSERT INTO `gestflux_right_acl` VALUES (23, 1, 1, 1);
INSERT INTO `gestflux_right_acl` VALUES (24, 1, 1, 1);
INSERT INTO `gestflux_right_acl` VALUES (25, 1, 1, 1);
INSERT INTO `gestflux_right_acl` VALUES (26, 1, 1, 1);
INSERT INTO `gestflux_right_acl` VALUES (27, 1, 1, 1);
INSERT INTO `gestflux_right_acl` VALUES (26, 6, 0, 1);
INSERT INTO `gestflux_right_acl` VALUES (24, 6, 0, 1);
INSERT INTO `gestflux_right_acl` VALUES (22, 6, 0, 1);
INSERT INTO `gestflux_right_acl` VALUES (21, 6, 0, 1);
INSERT INTO `gestflux_right_acl` VALUES (23, 6, 0, 1);
INSERT INTO `gestflux_right_acl` VALUES (27, 6, 0, 1);
INSERT INTO `gestflux_right_acl` VALUES (25, 6, 0, 1);
INSERT INTO `gestflux_right_acl` VALUES (20, 6, 0, 1);
INSERT INTO `gestflux_right_acl` VALUES (26, 5, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (20, 4, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (26, 3, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (20, 0, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (24, 5, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (25, 4, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (24, 3, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (25, 0, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (22, 5, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (27, 4, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (22, 3, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (27, 0, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (21, 5, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (23, 4, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (21, 3, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (23, 0, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (23, 5, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (21, 4, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (23, 3, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (21, 0, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (27, 5, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (22, 4, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (27, 3, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (22, 0, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (25, 5, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (24, 4, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (25, 3, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (24, 0, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (20, 5, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (26, 4, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (20, 3, 12, 1);
INSERT INTO `gestflux_right_acl` VALUES (26, 0, 12, 1);

-- --------------------------------------------------------

-- 
-- Structure de la table `gestflux_right_libelle`
-- 

DROP TABLE IF EXISTS `gestflux_right_libelle`;
CREATE TABLE `gestflux_right_libelle` (
  `RIGHT_ID` int(11) NOT NULL default '0',
  `LANG` varchar(16) collate utf8_unicode_ci NOT NULL default '',
  `LIBELLE` varchar(64) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`RIGHT_ID`,`LANG`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `gestflux_right_libelle`
-- 

INSERT INTO `gestflux_right_libelle` VALUES (20, 'fr', 'Analyser les logs de connexion');
INSERT INTO `gestflux_right_libelle` VALUES (20, 'en', 'Analyse connection logs');
INSERT INTO `gestflux_right_libelle` VALUES (21, 'fr', 'Importer les flux SQL');
INSERT INTO `gestflux_right_libelle` VALUES (21, 'en', 'Import SQL Files');
INSERT INTO `gestflux_right_libelle` VALUES (22, 'fr', 'Exporter les flux SQL');
INSERT INTO `gestflux_right_libelle` VALUES (22, 'en', 'Export SQL Files');
INSERT INTO `gestflux_right_libelle` VALUES (23, 'fr', 'Lister les traitements');
INSERT INTO `gestflux_right_libelle` VALUES (23, 'en', 'List jobs');
INSERT INTO `gestflux_right_libelle` VALUES (24, 'fr', 'Supprimer un traitement');
INSERT INTO `gestflux_right_libelle` VALUES (24, 'en', 'delete a job');
INSERT INTO `gestflux_right_libelle` VALUES (25, 'fr', 'Mettre a jour un traitement');
INSERT INTO `gestflux_right_libelle` VALUES (25, 'en', 'update a job');
INSERT INTO `gestflux_right_libelle` VALUES (26, 'fr', 'Créer un traitement');
INSERT INTO `gestflux_right_libelle` VALUES (26, 'en', 'create a job');
INSERT INTO `gestflux_right_libelle` VALUES (27, 'fr', 'Relancer un traitement');
INSERT INTO `gestflux_right_libelle` VALUES (27, 'en', 'restart a job');

-- --------------------------------------------------------

-- 
-- Structure de la table `r_denombrement`
-- 

DROP TABLE IF EXISTS `r_denombrement`;
CREATE TABLE `r_denombrement` (
  `ID` int(11) NOT NULL default '0',
  `REF_DIM` varchar(30) collate utf8_unicode_ci default NULL,
  `ID_DIM` int(11) NOT NULL default '0',
  `NBR_OCC` int(11) NOT NULL default '0',
  `DATE_OCC` date NOT NULL default '0000-00-00',
  `ID_STRUCT` varchar(50) collate utf8_unicode_ci NOT NULL default '0',
  `ID_STRUCT_PERE` varchar(50) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`ID`,`ID_STRUCT`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `r_denombrement`
-- 

-- --------------------------------------------------------

-- 
-- Structure de la table `ref_right`
-- 

DROP TABLE IF EXISTS `ref_right`;
CREATE TABLE `ref_right` (
  `RIGHT_ID` int(11) NOT NULL auto_increment,
  `RIGHT_CODE` varchar(32) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`RIGHT_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=77 ;

-- 
-- Contenu de la table `ref_right`
-- 

INSERT INTO `ref_right` VALUES (20, '_STRUCT_LIST_ACCESS_');
INSERT INTO `ref_right` VALUES (21, '_STRUCT_CREATE_');
INSERT INTO `ref_right` VALUES (22, '_STRUCT_DELETE_');
INSERT INTO `ref_right` VALUES (23, '_STRUCT_UPDATE_');
INSERT INTO `ref_right` VALUES (24, '_DENOMBR_LIST_ACCESS_');
INSERT INTO `ref_right` VALUES (25, '_DENOMBR_CREATE_');
INSERT INTO `ref_right` VALUES (26, '_DENOMBR_DELETE_');
INSERT INTO `ref_right` VALUES (27, '_DENOMBR_UPDATE_');
INSERT INTO `ref_right` VALUES (28, '_INDIC_LIST_ACCESS_');
INSERT INTO `ref_right` VALUES (29, '_INDIC_CREATE_');
INSERT INTO `ref_right` VALUES (30, '_INDIC_DELETE_');
INSERT INTO `ref_right` VALUES (31, '_INDIC_UPDATE_');
INSERT INTO `ref_right` VALUES (32, '_APPLI_LIST_ACCESS_');
INSERT INTO `ref_right` VALUES (33, '_APPLI_CREATE_');
INSERT INTO `ref_right` VALUES (34, '_APPLI_DELETE_');
INSERT INTO `ref_right` VALUES (35, '_APPLI_UPDATE_');
INSERT INTO `ref_right` VALUES (36, '_SRV_INTRA_LIST_ACCESS_');
INSERT INTO `ref_right` VALUES (37, '_SRV_INTRA_CREATE_');
INSERT INTO `ref_right` VALUES (38, '_SRV_INTRA_DELETE_');
INSERT INTO `ref_right` VALUES (39, '_SRV_INTRA_UPDATE_');
INSERT INTO `ref_right` VALUES (40, '_DCPL_INTRA_LIST_ACCESS_');
INSERT INTO `ref_right` VALUES (41, '_DCPL_INTRA_CREATE_');
INSERT INTO `ref_right` VALUES (42, '_DCPL_INTRA_DELETE_');
INSERT INTO `ref_right` VALUES (43, '_DCPL_INTRA_UPDATE_');
INSERT INTO `ref_right` VALUES (44, '_DIPLOME_INTRA_ACCESS_');
INSERT INTO `ref_right` VALUES (45, '_DIPLOME_INTRA_CREATE_');
INSERT INTO `ref_right` VALUES (46, '_DIPLOME_INTRA_DELETE_');
INSERT INTO `ref_right` VALUES (47, '_DIPLOME_INTRA_UPDATE_');
INSERT INTO `ref_right` VALUES (48, '_DOMETU_INTRA_LIST_ACCESS_');
INSERT INTO `ref_right` VALUES (49, '_DOMETU_INTRA_CREATE_');
INSERT INTO `ref_right` VALUES (50, '_DOMETU_INTRA_DELETE_');
INSERT INTO `ref_right` VALUES (51, '_DOMETU_INTRA_UPDATE_');
INSERT INTO `ref_right` VALUES (52, '_CAT_SRV_LIST_ACCESS_');
INSERT INTO `ref_right` VALUES (53, '_CAT_SRV_CREATE_');
INSERT INTO `ref_right` VALUES (54, '_CAT_SRV_DELETE_');
INSERT INTO `ref_right` VALUES (55, '_CAT_SRV_UPDATE_');
INSERT INTO `ref_right` VALUES (56, '_DISCIPLINE_LIST_ACCESS_');
INSERT INTO `ref_right` VALUES (57, '_DISCIPLINE_INTRA_CREATE_');
INSERT INTO `ref_right` VALUES (58, '_DISCIPLINE_INTRA_DELETE_');
INSERT INTO `ref_right` VALUES (59, '_DISCIPLINE_INTRA_UPDATE_');
INSERT INTO `ref_right` VALUES (60, '_DIPLOME_ACCESS_');
INSERT INTO `ref_right` VALUES (61, '_DIPLOME_CREATE_');
INSERT INTO `ref_right` VALUES (62, '_DIPLOME_DELETE_');
INSERT INTO `ref_right` VALUES (63, '_DIPLOME_UPDATE_');
INSERT INTO `ref_right` VALUES (64, '_DOMETU_LIST_ACCESS_');
INSERT INTO `ref_right` VALUES (65, '_DOMETU_CREATE_');
INSERT INTO `ref_right` VALUES (66, '_DOMETU_DELETE_');
INSERT INTO `ref_right` VALUES (67, '_DOMETU_UPDATE_');
INSERT INTO `ref_right` VALUES (68, '_AFFILIATION_LIST_ACCESS_');
INSERT INTO `ref_right` VALUES (69, '_AFFILIATION_CREATE_');
INSERT INTO `ref_right` VALUES (70, '_AFFILIATION_DELETE_');
INSERT INTO `ref_right` VALUES (71, '_AFFILIATION_UPDATE_');
INSERT INTO `ref_right` VALUES (72, '_AFFIL_INTRA_LIST_ACCESS_');
INSERT INTO `ref_right` VALUES (73, '_AFFIL_INTRA_CREATE_');
INSERT INTO `ref_right` VALUES (74, '_AFFIL_INTRA_DELETE_');
INSERT INTO `ref_right` VALUES (75, '_AFFIL_INTRA_UPDATE_');
INSERT INTO `ref_right` VALUES (76, '_INDIC_IMPORT_');

-- --------------------------------------------------------

-- 
-- Structure de la table `ref_right_acl`
-- 

DROP TABLE IF EXISTS `ref_right_acl`;
CREATE TABLE `ref_right_acl` (
  `RIGHT_ID` int(11) NOT NULL default '0',
  `INT_COD` int(11) NOT NULL default '0',
  `GID` int(11) NOT NULL default '0',
  `ACL` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`RIGHT_ID`,`INT_COD`,`GID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `ref_right_acl`
-- 

INSERT INTO `ref_right_acl` VALUES (20, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (21, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (22, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (23, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (22, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (21, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (39, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (36, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (38, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (37, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (31, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (28, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (76, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (30, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (29, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (67, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (64, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (51, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (48, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (50, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (24, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (25, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (26, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (27, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (49, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (66, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (65, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (56, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (59, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (58, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (57, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (63, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (47, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (46, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (45, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (44, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (62, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (61, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (60, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (27, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (28, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (29, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (30, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (31, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (24, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (26, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (25, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (43, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (40, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (42, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (41, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (55, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (52, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (54, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (53, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (35, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (32, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (34, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (33, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (75, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (32, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (33, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (34, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (35, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (36, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (37, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (38, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (39, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (40, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (41, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (42, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (43, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (44, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (45, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (46, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (47, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (48, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (49, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (50, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (51, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (52, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (53, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (54, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (55, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (56, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (57, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (58, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (59, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (60, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (61, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (62, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (63, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (64, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (65, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (66, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (67, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (72, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (74, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (73, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (71, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (68, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (70, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (69, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (76, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (21, 2, 8, 1);
INSERT INTO `ref_right_acl` VALUES (21, 0, 8, 1);
INSERT INTO `ref_right_acl` VALUES (23, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (21, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (23, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (21, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (20, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (39, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (20, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (39, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (22, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (36, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (22, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (36, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (69, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (38, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (69, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (38, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (70, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (37, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (70, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (37, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (31, 2, 8, 1);
INSERT INTO `ref_right_acl` VALUES (31, 0, 8, 1);
INSERT INTO `ref_right_acl` VALUES (68, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (31, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (68, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (31, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (28, 7, 11, 1);
INSERT INTO `ref_right_acl` VALUES (28, 0, 11, 1);
INSERT INTO `ref_right_acl` VALUES (28, 2, 8, 1);
INSERT INTO `ref_right_acl` VALUES (28, 0, 8, 1);
INSERT INTO `ref_right_acl` VALUES (71, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (28, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (71, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (28, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (73, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (76, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (73, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (76, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (30, 2, 8, 1);
INSERT INTO `ref_right_acl` VALUES (30, 0, 8, 1);
INSERT INTO `ref_right_acl` VALUES (74, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (30, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (74, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (30, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (29, 2, 8, 1);
INSERT INTO `ref_right_acl` VALUES (29, 0, 8, 1);
INSERT INTO `ref_right_acl` VALUES (72, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (29, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (72, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (29, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (75, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (67, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (75, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (67, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (33, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (64, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (33, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (64, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (34, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (51, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (34, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (51, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (32, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (48, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (32, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (48, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (35, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (50, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (35, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (50, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (53, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (49, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (53, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (49, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (54, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (66, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (54, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (66, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (52, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (65, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (52, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (65, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (55, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (56, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (55, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (56, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (41, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (57, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (41, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (57, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (42, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (63, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (42, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (63, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (40, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (47, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (40, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (47, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (43, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (46, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (43, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (46, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (25, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (45, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (25, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (45, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (26, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (44, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (26, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (44, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (24, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (62, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (24, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (62, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (27, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (61, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (27, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (61, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (60, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (60, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (60, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (60, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (27, 2, 8, 1);
INSERT INTO `ref_right_acl` VALUES (27, 0, 8, 1);
INSERT INTO `ref_right_acl` VALUES (61, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (27, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (61, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (27, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (24, 2, 8, 1);
INSERT INTO `ref_right_acl` VALUES (24, 0, 8, 1);
INSERT INTO `ref_right_acl` VALUES (62, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (24, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (62, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (24, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (26, 2, 8, 1);
INSERT INTO `ref_right_acl` VALUES (26, 0, 8, 1);
INSERT INTO `ref_right_acl` VALUES (44, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (26, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (44, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (26, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (25, 2, 8, 1);
INSERT INTO `ref_right_acl` VALUES (25, 0, 8, 1);
INSERT INTO `ref_right_acl` VALUES (45, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (25, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (45, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (25, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (46, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (43, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (46, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (43, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (47, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (40, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (47, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (40, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (63, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (42, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (63, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (42, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (57, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (41, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (57, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (41, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (56, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (55, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (56, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (55, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (65, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (52, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (65, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (52, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (66, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (54, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (66, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (54, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (49, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (53, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (49, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (53, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (50, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (35, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (50, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (35, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (68, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (69, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (70, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (71, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (72, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (73, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (74, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (75, 1, 1, 1);
INSERT INTO `ref_right_acl` VALUES (48, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (32, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (48, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (32, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (51, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (34, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (51, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (34, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (64, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (33, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (64, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (33, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (67, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (75, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (67, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (75, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (29, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (72, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (29, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (72, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (30, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (74, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (30, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (74, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (76, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (73, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (76, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (73, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (28, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (71, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (28, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (71, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (20, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (23, 6, 0, 1);
INSERT INTO `ref_right_acl` VALUES (31, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (68, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (31, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (68, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (37, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (70, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (37, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (70, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (38, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (69, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (38, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (69, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (22, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (36, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (22, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (36, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (22, 0, 8, 1);
INSERT INTO `ref_right_acl` VALUES (22, 2, 8, 1);
INSERT INTO `ref_right_acl` VALUES (20, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (39, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (20, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (39, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (20, 0, 8, 1);
INSERT INTO `ref_right_acl` VALUES (20, 2, 8, 1);
INSERT INTO `ref_right_acl` VALUES (23, 0, 12, 1);
INSERT INTO `ref_right_acl` VALUES (21, 3, 12, 1);
INSERT INTO `ref_right_acl` VALUES (23, 4, 12, 1);
INSERT INTO `ref_right_acl` VALUES (21, 5, 12, 1);
INSERT INTO `ref_right_acl` VALUES (23, 0, 8, 1);
INSERT INTO `ref_right_acl` VALUES (23, 2, 8, 1);

-- --------------------------------------------------------

-- 
-- Structure de la table `ref_right_libelle`
-- 

DROP TABLE IF EXISTS `ref_right_libelle`;
CREATE TABLE `ref_right_libelle` (
  `RIGHT_ID` int(11) NOT NULL default '0',
  `LANG` varchar(16) collate utf8_unicode_ci NOT NULL default '',
  `LIBELLE` varchar(64) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`RIGHT_ID`,`LANG`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `ref_right_libelle`
-- 

INSERT INTO `ref_right_libelle` VALUES (20, 'fr', 'Accès à la liste des structures');
INSERT INTO `ref_right_libelle` VALUES (20, 'en', 'Access to the list of structures');
INSERT INTO `ref_right_libelle` VALUES (21, 'fr', 'Créer une structure');
INSERT INTO `ref_right_libelle` VALUES (21, 'en', 'Create a structure');
INSERT INTO `ref_right_libelle` VALUES (22, 'fr', 'Supprimer une structure');
INSERT INTO `ref_right_libelle` VALUES (22, 'en', 'Delete a structure');
INSERT INTO `ref_right_libelle` VALUES (23, 'fr', 'Modifier une structure');
INSERT INTO `ref_right_libelle` VALUES (23, 'en', 'Update a structure');
INSERT INTO `ref_right_libelle` VALUES (24, 'fr', 'Accès à la liste des dénombrements');
INSERT INTO `ref_right_libelle` VALUES (24, 'en', 'Access to the list of enumerations');
INSERT INTO `ref_right_libelle` VALUES (25, 'fr', 'Créer un dénombrement');
INSERT INTO `ref_right_libelle` VALUES (25, 'en', 'Create a enumeration');
INSERT INTO `ref_right_libelle` VALUES (26, 'fr', 'Supprimer un dénombrement');
INSERT INTO `ref_right_libelle` VALUES (26, 'en', 'Delete a enumeration');
INSERT INTO `ref_right_libelle` VALUES (27, 'fr', 'Modifier un dénombrement');
INSERT INTO `ref_right_libelle` VALUES (27, 'en', 'Update a enumeration');
INSERT INTO `ref_right_libelle` VALUES (28, 'fr', 'Accès à la liste des indicateurs');
INSERT INTO `ref_right_libelle` VALUES (28, 'en', 'Access to the list of indicators');
INSERT INTO `ref_right_libelle` VALUES (29, 'fr', 'Créer un indicateur');
INSERT INTO `ref_right_libelle` VALUES (29, 'en', 'Create a indicator');
INSERT INTO `ref_right_libelle` VALUES (30, 'fr', 'Supprimer un indicateur');
INSERT INTO `ref_right_libelle` VALUES (30, 'en', 'Delete a indicator');
INSERT INTO `ref_right_libelle` VALUES (31, 'fr', 'Modifier un indicateur');
INSERT INTO `ref_right_libelle` VALUES (31, 'en', 'Update a indicator');
INSERT INTO `ref_right_libelle` VALUES (32, 'fr', 'Accès à la liste des applications');
INSERT INTO `ref_right_libelle` VALUES (32, 'en', 'Access to the list ofapplications');
INSERT INTO `ref_right_libelle` VALUES (33, 'fr', 'Créer une application');
INSERT INTO `ref_right_libelle` VALUES (33, 'en', 'Create a application');
INSERT INTO `ref_right_libelle` VALUES (34, 'fr', 'Supprimer une application');
INSERT INTO `ref_right_libelle` VALUES (34, 'en', 'Delete a application');
INSERT INTO `ref_right_libelle` VALUES (35, 'fr', 'Modifier une application');
INSERT INTO `ref_right_libelle` VALUES (35, 'en', 'Update a application');
INSERT INTO `ref_right_libelle` VALUES (36, 'fr', 'Accès à la liste des services intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (36, 'en', 'Access to the list of internal services');
INSERT INTO `ref_right_libelle` VALUES (37, 'fr', 'Créer un service intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (37, 'en', 'Create an internal service');
INSERT INTO `ref_right_libelle` VALUES (38, 'fr', 'Supprimer un service intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (38, 'en', 'Delete an internal service');
INSERT INTO `ref_right_libelle` VALUES (39, 'fr', 'Modifier un service intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (39, 'en', 'Update an internal service');
INSERT INTO `ref_right_libelle` VALUES (40, 'fr', 'Accès à la liste des disciplines intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (40, 'en', 'Access to the list of internal disciplines');
INSERT INTO `ref_right_libelle` VALUES (41, 'fr', 'Créer une discipline intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (41, 'en', 'Create an internal discipline ');
INSERT INTO `ref_right_libelle` VALUES (42, 'fr', 'Supprimer une discipline intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (42, 'en', 'Delete an internal discipline ');
INSERT INTO `ref_right_libelle` VALUES (43, 'fr', 'Modifier une discipline intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (43, 'en', 'Update an internal discipline ');
INSERT INTO `ref_right_libelle` VALUES (44, 'fr', 'Accès à la liste des diplomes intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (44, 'en', 'Access to the list of internal diplomes');
INSERT INTO `ref_right_libelle` VALUES (45, 'fr', 'Créer un diplome intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (45, 'en', 'Create an internal diplome ');
INSERT INTO `ref_right_libelle` VALUES (46, 'fr', 'Supprimer un diplome intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (46, 'en', 'Delete an internal diplome ');
INSERT INTO `ref_right_libelle` VALUES (47, 'fr', 'Modifier un diplome intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (47, 'en', 'Update an internal diplome ');
INSERT INTO `ref_right_libelle` VALUES (48, 'fr', 'Accès à la liste des domaines intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (48, 'en', 'Access to the list of internal domains');
INSERT INTO `ref_right_libelle` VALUES (49, 'fr', 'Créer un domaine intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (49, 'en', 'Create an internal domain ');
INSERT INTO `ref_right_libelle` VALUES (50, 'fr', 'Supprimer un domaine intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (50, 'en', 'Delete an internal domain ');
INSERT INTO `ref_right_libelle` VALUES (51, 'fr', 'Modifier un domaine intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (51, 'en', 'Update an internal domain ');
INSERT INTO `ref_right_libelle` VALUES (52, 'fr', 'Accès à la liste des catégories de service');
INSERT INTO `ref_right_libelle` VALUES (52, 'en', 'Access to the list of service categories');
INSERT INTO `ref_right_libelle` VALUES (53, 'fr', 'Créer une catégorie de service');
INSERT INTO `ref_right_libelle` VALUES (53, 'en', 'Create a service category ');
INSERT INTO `ref_right_libelle` VALUES (54, 'fr', 'Supprimer une catégorie de service');
INSERT INTO `ref_right_libelle` VALUES (54, 'en', 'Delete a service category ');
INSERT INTO `ref_right_libelle` VALUES (55, 'fr', 'Modifier une catégorie de service');
INSERT INTO `ref_right_libelle` VALUES (55, 'en', 'Update a service category ');
INSERT INTO `ref_right_libelle` VALUES (56, 'fr', 'Accès à la liste des disciplines');
INSERT INTO `ref_right_libelle` VALUES (56, 'en', 'Access to the list of disciplinees');
INSERT INTO `ref_right_libelle` VALUES (57, 'fr', 'Créer une discipline');
INSERT INTO `ref_right_libelle` VALUES (57, 'en', 'Create a discipline ');
INSERT INTO `ref_right_libelle` VALUES (58, 'fr', 'Supprimer une discipline');
INSERT INTO `ref_right_libelle` VALUES (58, 'en', 'Delete a discipline ');
INSERT INTO `ref_right_libelle` VALUES (59, 'fr', 'Modifier une discipline');
INSERT INTO `ref_right_libelle` VALUES (59, 'en', 'Update a discipline ');
INSERT INTO `ref_right_libelle` VALUES (60, 'fr', 'Accès à la liste des diplomes');
INSERT INTO `ref_right_libelle` VALUES (60, 'en', 'Access to the list of diplomes');
INSERT INTO `ref_right_libelle` VALUES (61, 'fr', 'Créer un diplome');
INSERT INTO `ref_right_libelle` VALUES (61, 'en', 'Create a diplome ');
INSERT INTO `ref_right_libelle` VALUES (62, 'fr', 'Supprimer un diplome');
INSERT INTO `ref_right_libelle` VALUES (62, 'en', 'Delete a diplome ');
INSERT INTO `ref_right_libelle` VALUES (63, 'fr', 'Modifier un diplome');
INSERT INTO `ref_right_libelle` VALUES (63, 'en', 'Update a diplome ');
INSERT INTO `ref_right_libelle` VALUES (64, 'fr', 'Accès à la liste des domaines');
INSERT INTO `ref_right_libelle` VALUES (64, 'en', 'Access to the list of domains');
INSERT INTO `ref_right_libelle` VALUES (65, 'fr', 'Créer un domaine');
INSERT INTO `ref_right_libelle` VALUES (65, 'en', 'Create a domain ');
INSERT INTO `ref_right_libelle` VALUES (66, 'fr', 'Supprimer un domaine');
INSERT INTO `ref_right_libelle` VALUES (66, 'en', 'Delete a domain ');
INSERT INTO `ref_right_libelle` VALUES (67, 'fr', 'Modifier un domaine');
INSERT INTO `ref_right_libelle` VALUES (67, 'en', 'Update a domain ');
INSERT INTO `ref_right_libelle` VALUES (68, 'fr', 'Accès à la liste des profils');
INSERT INTO `ref_right_libelle` VALUES (68, 'en', 'Access to the list of affiliations');
INSERT INTO `ref_right_libelle` VALUES (69, 'fr', 'Créer un profil');
INSERT INTO `ref_right_libelle` VALUES (69, 'en', 'Create an affiliation ');
INSERT INTO `ref_right_libelle` VALUES (70, 'fr', 'Supprimer un profil');
INSERT INTO `ref_right_libelle` VALUES (70, 'en', 'Delete an affiliation ');
INSERT INTO `ref_right_libelle` VALUES (71, 'fr', 'Modifier un profil');
INSERT INTO `ref_right_libelle` VALUES (71, 'en', 'Update an affiliation ');
INSERT INTO `ref_right_libelle` VALUES (72, 'fr', 'Accès à la liste des profils intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (72, 'en', 'Access to the list of internal affiliations');
INSERT INTO `ref_right_libelle` VALUES (73, 'fr', 'Créer un profil intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (73, 'en', 'Create an internal affiliation ');
INSERT INTO `ref_right_libelle` VALUES (74, 'fr', 'Supprimer un profil  intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (74, 'en', 'Delete an internal affiliation ');
INSERT INTO `ref_right_libelle` VALUES (75, 'fr', 'Modifier un profil  intra-etablissement');
INSERT INTO `ref_right_libelle` VALUES (75, 'en', 'Update an internal affiliation ');
INSERT INTO `ref_right_libelle` VALUES (76, 'fr', 'Importer un indicateur');
INSERT INTO `ref_right_libelle` VALUES (76, 'en', 'import an indicator');

-- --------------------------------------------------------

-- 
-- Structure de la table `restit_right`
-- 

DROP TABLE IF EXISTS `restit_right`;
CREATE TABLE `restit_right` (
  `RIGHT_ID` int(11) NOT NULL auto_increment,
  `RIGHT_CODE` varchar(32) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`RIGHT_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

-- 
-- Contenu de la table `restit_right`
-- 

INSERT INTO `restit_right` VALUES (20, '_STRUCT_LIST_ACCESS_');

-- --------------------------------------------------------

-- 
-- Structure de la table `restit_right_acl`
-- 

DROP TABLE IF EXISTS `restit_right_acl`;
CREATE TABLE `restit_right_acl` (
  `RIGHT_ID` int(11) NOT NULL default '0',
  `INT_COD` int(11) NOT NULL default '0',
  `GID` int(11) NOT NULL default '0',
  `ACL` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`RIGHT_ID`,`INT_COD`,`GID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `restit_right_acl`
-- 

INSERT INTO `restit_right_acl` VALUES (20, 1, 1, 1);
INSERT INTO `restit_right_acl` VALUES (20, 4, 2, 1);
INSERT INTO `restit_right_acl` VALUES (20, 0, 2, 1);
INSERT INTO `restit_right_acl` VALUES (20, 6, 13, 1);
INSERT INTO `restit_right_acl` VALUES (20, 3, 13, 1);
INSERT INTO `restit_right_acl` VALUES (20, 0, 13, 1);
INSERT INTO `restit_right_acl` VALUES (20, 7, 11, 1);
INSERT INTO `restit_right_acl` VALUES (20, 0, 11, 1);
INSERT INTO `restit_right_acl` VALUES (20, 2, 8, 1);
INSERT INTO `restit_right_acl` VALUES (20, 0, 8, 1);
INSERT INTO `restit_right_acl` VALUES (20, 5, 12, 1);
INSERT INTO `restit_right_acl` VALUES (20, 6, 0, 1);
INSERT INTO `restit_right_acl` VALUES (20, 4, 12, 1);
INSERT INTO `restit_right_acl` VALUES (20, 3, 12, 1);
INSERT INTO `restit_right_acl` VALUES (20, 0, 12, 1);
INSERT INTO `restit_right_acl` VALUES (20, 8, 13, 1);

-- --------------------------------------------------------

-- 
-- Structure de la table `restit_right_libelle`
-- 

DROP TABLE IF EXISTS `restit_right_libelle`;
CREATE TABLE `restit_right_libelle` (
  `RIGHT_ID` int(11) NOT NULL default '0',
  `LANG` varchar(16) collate utf8_unicode_ci NOT NULL default '',
  `LIBELLE` varchar(64) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`RIGHT_ID`,`LANG`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `restit_right_libelle`
-- 

INSERT INTO `restit_right_libelle` VALUES (20, 'fr', 'Accès à la liste des tableaux de bord');
INSERT INTO `restit_right_libelle` VALUES (20, 'en', 'Access to the list of dashboards');

-- --------------------------------------------------------

-- 
-- Structure de la table `t_affil_intra`
-- 

DROP TABLE IF EXISTS `t_affil_intra`;
CREATE TABLE `t_affil_intra` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `CHAINE_TYPE` varchar(255) collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  `ID_STRUCT` smallint(6) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_affil_intra`
-- 

INSERT INTO `t_affil_intra` VALUES (1, '', 'Autres', 'Autres', '', 1, 0);
INSERT INTO `t_affil_intra` VALUES (2, 'STU', 'Student', 'Student', '#^student$#', 2, 0);
INSERT INTO `t_affil_intra` VALUES (3, 'F', 'faculty', 'faculty', '#^faculty$#', 3, 0);
INSERT INTO `t_affil_intra` VALUES (4, 'STA', 'Staff', 'Staff', '#^STA$#', 4, 0);
INSERT INTO `t_affil_intra` VALUES (5, 'E', 'Employee', 'Employee', '#^employee$#', 4, 0);
INSERT INTO `t_affil_intra` VALUES (6, 'M', 'member', 'member', '#^M$#', 1, 0);
INSERT INTO `t_affil_intra` VALUES (7, 'A', 'affiliate', 'affiliate', '#^affiliate$#', 1, 0);
INSERT INTO `t_affil_intra` VALUES (8, 'AL', 'alum', 'alum', '#^AL$#', 1, 0);
INSERT INTO `t_affil_intra` VALUES (9, '?', 'library-walk-in', 'library-walk-in', '#^LWI$#', 1, 0);
INSERT INTO `t_affil_intra` VALUES (10, 'R', 'researcher', 'researcher', '#^R$#', 5, 0);
INSERT INTO `t_affil_intra` VALUES (11, 'RD', 'retired', 'retired', '#^RD$#', 1, 0);
INSERT INTO `t_affil_intra` VALUES (12, 'EUS', 'emeritus', 'emeritus', '#^EUS$#', 3, 0);
INSERT INTO `t_affil_intra` VALUES (13, 'T', 'teacher', 'teacher', '#^T$#', 3, 0);
INSERT INTO `t_affil_intra` VALUES (14, 'RR', 'registered-reader', 'registered-reader', '#^RR$#', 1, 0);

-- --------------------------------------------------------

-- 
-- Structure de la table `t_affiliation`
-- 

DROP TABLE IF EXISTS `t_affiliation`;
CREATE TABLE `t_affiliation` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_affiliation`
-- 

INSERT INTO `t_affiliation` VALUES (1, '', 'Autres', 'Autres', 1);
INSERT INTO `t_affiliation` VALUES (2, '', 'Etudiant', 'Etudiant', 2);
INSERT INTO `t_affiliation` VALUES (3, '', 'Enseignant', 'Enseignant', 3);
INSERT INTO `t_affiliation` VALUES (4, '', 'IATOS', 'IATOS', 4);
INSERT INTO `t_affiliation` VALUES (5, '', 'Chercheurs', 'Chercheurs', 5);

-- --------------------------------------------------------

-- 
-- Structure de la table `t_agregat`
-- 

DROP TABLE IF EXISTS `t_agregat`;
CREATE TABLE `t_agregat` (
  `ID_TEMPS` int(11) NOT NULL,
  `NB_UID` mediumint(11) NOT NULL,
  `NB_CNX` mediumint(11) NOT NULL,
  `DUREE` float NOT NULL,
  `VOLUME` float NOT NULL,
  `ID_CAT_STRUCT` tinyint(4) NOT NULL,
  `ID_STRUCT` varchar(50) collate utf8_unicode_ci NOT NULL,
  `ID_STRUCT_PERE` varchar(50) collate utf8_unicode_ci NOT NULL,
  `ID_SRV_INTRA` smallint(6) NOT NULL,
  `ID_CAT_SRV` smallint(6) NOT NULL,
  `ID_DCPL_INTRA` smallint(6) NOT NULL,
  `ID_DCPL` smallint(6) NOT NULL,
  `ID_AFFIL_INTRA` smallint(6) NOT NULL,
  `ID_AFFIL` smallint(6) NOT NULL,
  `ID_DOM_ETU_INTRA` smallint(6) NOT NULL,
  `ID_DOM_ETU` smallint(6) NOT NULL,
  `ID_DIPLOME_INTRA` smallint(6) NOT NULL,
  `ID_DIPLOME` smallint(6) NOT NULL,
  `DATE_CRE` datetime NOT NULL default '0000-00-00 00:00:00',
  `DATE_EXPORT` datetime NOT NULL default '0000-00-00 00:00:00',
  `USER_AGENT` varchar(10) collate utf8_unicode_ci NOT NULL,
  `USER_DIM_1` smallint(6) NOT NULL,
  `USER_DIM_2` smallint(6) NOT NULL,
  `USER_DIM_3` smallint(6) NOT NULL,
  PRIMARY KEY  (`ID_TEMPS`,`ID_CAT_STRUCT`,`ID_STRUCT`,`ID_STRUCT_PERE`,`ID_SRV_INTRA`,`ID_CAT_SRV`,`ID_DCPL_INTRA`,`ID_DCPL`,`ID_AFFIL_INTRA`,`ID_AFFIL`,`ID_DOM_ETU_INTRA`,`ID_DOM_ETU`,`ID_DIPLOME_INTRA`,`ID_DIPLOME`,`USER_AGENT`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_agregat`
-- 



-- --------------------------------------------------------

-- 
-- Structure de la table `t_agregat_calc`
-- 

DROP TABLE IF EXISTS `t_agregat_calc`;
CREATE TABLE `t_agregat_calc` (
  `ID_TEMPS` int(11) NOT NULL,
  `UID` varchar(100) collate utf8_unicode_ci NOT NULL,
  `NB_UID` mediumint(11) NOT NULL,
  `NB_CNX` mediumint(11) NOT NULL,
  `DUREE` float NOT NULL,
  `VOLUME` float NOT NULL,
  `ID_CAT_STRUCT` tinyint(4) NOT NULL,
  `ID_STRUCT` varchar(50) character set utf8 NOT NULL,
  `ID_STRUCT_PERE` varchar(50) character set utf8 NOT NULL,
  `ID_SRV_INTRA` smallint(6) NOT NULL,
  `ID_CAT_SRV` smallint(6) NOT NULL,
  `ID_DCPL_INTRA` smallint(6) NOT NULL,
  `ID_DCPL` smallint(6) NOT NULL,
  `ID_AFFIL_INTRA` smallint(6) NOT NULL,
  `ID_AFFIL` smallint(6) NOT NULL,
  `ID_DOM_ETU_INTRA` smallint(6) NOT NULL,
  `ID_DOM_ETU` smallint(6) NOT NULL,
  `ID_DIPLOME_INTRA` smallint(6) NOT NULL,
  `ID_DIPLOME` smallint(6) NOT NULL,
  `DATE_CRE` datetime NOT NULL default '0000-00-00 00:00:00',
  `DATE_EXPORT` datetime NOT NULL default '0000-00-00 00:00:00',
  `USER_AGENT` varchar(10) character set utf8 NOT NULL,
  `USER_DIM_1` smallint(6) NOT NULL,
  `USER_DIM_2` smallint(6) NOT NULL,
  `USER_DIM_3` smallint(6) NOT NULL,
  PRIMARY KEY  (`ID_TEMPS`,`UID`,`ID_CAT_STRUCT`,`ID_STRUCT`,`ID_STRUCT_PERE`,`ID_SRV_INTRA`,`ID_CAT_SRV`,`ID_DCPL_INTRA`,`ID_DCPL`,`ID_AFFIL_INTRA`,`ID_AFFIL`,`ID_DOM_ETU_INTRA`,`ID_DOM_ETU`,`ID_DIPLOME_INTRA`,`ID_DIPLOME`,`USER_AGENT`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_agregat_calc`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `t_agregat_export`
-- 

DROP TABLE IF EXISTS `t_agregat_export`;
CREATE TABLE `t_agregat_export` (
  `ID_TEMPS` int(11) NOT NULL,
  `NB_UID` mediumint(11) NOT NULL,
  `NB_CNX` mediumint(11) NOT NULL,
  `DUREE` float NOT NULL,
  `VOLUME` float NOT NULL,
  `ID_CAT_STRUCT` tinyint(4) NOT NULL,
  `ID_STRUCT` varchar(50) NOT NULL,
  `ID_STRUCT_PERE` varchar(50) NOT NULL,
  `ID_SRV_INTRA` smallint(6) NOT NULL,
  `ID_CAT_SRV` smallint(6) NOT NULL,
  `ID_DCPL_INTRA` smallint(6) NOT NULL,
  `ID_DCPL` smallint(6) NOT NULL,
  `ID_AFFIL_INTRA` smallint(6) NOT NULL,
  `ID_AFFIL` smallint(6) NOT NULL,
  `ID_DOM_ETU_INTRA` smallint(6) NOT NULL,
  `ID_DOM_ETU` smallint(6) NOT NULL,
  `ID_DIPLOME_INTRA` smallint(6) NOT NULL,
  `ID_DIPLOME` smallint(6) NOT NULL,
  `DATE_CRE` datetime NOT NULL default '0000-00-00 00:00:00',
  `DATE_EXPORT` datetime NOT NULL default '0000-00-00 00:00:00',
  `USER_AGENT` varchar(10) NOT NULL,
  `USER_DIM_1` smallint(6) NOT NULL,
  `USER_DIM_2` smallint(6) NOT NULL,
  `USER_DIM_3` smallint(6) NOT NULL,
  PRIMARY KEY  (`ID_TEMPS`,`ID_CAT_STRUCT`,`ID_STRUCT`,`ID_STRUCT_PERE`,`ID_CAT_SRV`,`ID_DCPL`,`ID_AFFIL`,`ID_DOM_ETU`,`ID_DIPLOME`,`USER_AGENT`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Contenu de la table `t_agregat_export`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `t_application`
-- 

DROP TABLE IF EXISTS `t_application`;
CREATE TABLE `t_application` (
  `ID` smallint(6) NOT NULL default '0',
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `URL_TYPE` varchar(255) collate utf8_unicode_ci NOT NULL,
  `ID_SRV_INTRA` smallint(6) NOT NULL,
  `ID_STRUCT` smallint(6) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_application`
-- 

INSERT INTO `t_application` VALUES (7, 'Annuaire', 'Annuaire Membres', '#annu_etu#', 4, 3);
INSERT INTO `t_application` VALUES (26, 'MyAgenda', 'Agenda professionnel', '#^https://webmail.univ.fr/horde/kronolith$#', 23, 3);
INSERT INTO `t_application` VALUES (1, 'Autre', '', '', 1, 3);
INSERT INTO `t_application` VALUES (33, 'AGIMUS', 'Mesure des usages', '#agimus#', 1, 3);
INSERT INTO `t_application` VALUES (34, 'Webmail dynamique', '', '#^https://webmail.univ.fr/horde/dimp#', 28, 0);
INSERT INTO `t_application` VALUES (35, 'Portail eSup', 'Portail eSup', '#^https://portail.univ.fr/#', 12, 0);

-- --------------------------------------------------------

-- 
-- Structure de la table `t_aut`
-- 

DROP TABLE IF EXISTS `t_aut`;
CREATE TABLE `t_aut` (
  `INT_COD` int(11) NOT NULL default '0',
  `AUT_PWD` varchar(32) collate utf8_unicode_ci default NULL,
  `DATE_PWD` date NOT NULL default '0000-00-00',
  `AUT_PRF` char(3) collate utf8_unicode_ci NOT NULL default '',
  `AUT_STA` char(1) collate utf8_unicode_ci NOT NULL default '',
  `AUT_DER_DAT` datetime default NULL,
  `AUT_STA_LOG` char(1) collate utf8_unicode_ci NOT NULL default '',
  `AUT_USER_LOGIN` char(64) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`INT_COD`),
  KEY `INT_COD` (`INT_COD`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_aut`
-- 

INSERT INTO `t_aut` VALUES (1, '0ea58701b84295bdd11c5b05426c6c3f', '2010-06-03', '1', '1', '2009-10-01 08:24:54', '1', 'agimusadmin');
INSERT INTO `t_aut` VALUES (6, '0b67b19b706ae02d97f62021e6d8d63a', '0000-00-00', '', '1', NULL, '', 'Pilote');
INSERT INTO `t_aut` VALUES (7, 'f4636db288a54da3e6b55b4aa8d37d6f', '0000-00-00', '', '1', NULL, '', 'Lecteur');


-- --------------------------------------------------------

-- 
-- Structure de la table `t_cat_srv`
-- 

DROP TABLE IF EXISTS `t_cat_srv`;
CREATE TABLE `t_cat_srv` (
  `ID` smallint(6) NOT NULL default '0',
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_cat_srv`
-- 

INSERT INTO `t_cat_srv` VALUES (9, 'Information et Communication', 'Webmail, Forum, Chat, Annuaire des membres, Diffusion selective deinformation des outils de gestion de contenu');
INSERT INTO `t_cat_srv` VALUES (2, 'Scolarité', 'Inscriptions ; Réinscription ; Dossier administratif ; Consultation des notes ; Calendrier des examens ; Emploi du temps ;\r\n');
INSERT INTO `t_cat_srv` VALUES (3, 'Plateforme d''enseignement', 'Services à définir. Quelquefois redondant avec des services inscrits dans d''autres catégories.');
INSERT INTO `t_cat_srv` VALUES (4, 'Documentation', 'Dossier lecteur ; Réservation en ligne ; Accès aux catalogues ; Accès aux dictionnaires, encyclopédies et autres ressources génériques ; Accès aux ressources spécifiques; Moteur de recherche fédéré;');
INSERT INTO `t_cat_srv` VALUES (5, 'Gestion financière', 'Services à définir');
INSERT INTO `t_cat_srv` VALUES (6, 'Gestion des personnels', 'Suivi des missions ; Dossier Personnel ; Congés et absences');
INSERT INTO `t_cat_srv` VALUES (7, 'Bureau numérique', 'Service de consultation d''agenda (personnel ou de groupe), Service d''accès au stockage en ligne');
INSERT INTO `t_cat_srv` VALUES (8, 'Relation entreprise ', 'Gestion des stages ');
INSERT INTO `t_cat_srv` VALUES (1, 'Autre', 'Autre');

-- --------------------------------------------------------

-- 
-- Structure de la table `t_cat_struct`
-- 

DROP TABLE IF EXISTS `t_cat_struct`;
CREATE TABLE `t_cat_struct` (
  `ID` smallint(6) NOT NULL default '0',
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_cat_struct`
-- 

INSERT INTO `t_cat_struct` VALUES (1, 'Ministère', 'Ministère');
INSERT INTO `t_cat_struct` VALUES (2, 'U.N.R.', 'Université Numérique en Région');
INSERT INTO `t_cat_struct` VALUES (3, 'Université', 'Université');
INSERT INTO `t_cat_struct` VALUES (4, 'IUT', 'Institut Universitaire de Technologie');
INSERT INTO `t_cat_struct` VALUES (5, 'UFR', 'Unité de formation et de recherche');

-- --------------------------------------------------------

-- 
-- Structure de la table `t_cnx`
-- 

DROP TABLE IF EXISTS `t_cnx`;
CREATE TABLE `t_cnx` (
  `ID` int(11) NOT NULL default '0',
  `UID` varchar(100) collate utf8_unicode_ci NOT NULL,
  `STAMP_START` varchar(15) collate utf8_unicode_ci NOT NULL,
  `FLOW_SIZE` int(11) NOT NULL default '0',
  `RESP_TIME` int(11) NOT NULL default '0',
  `ID_APPLI` int(11) NOT NULL default '0',
  `ID_DCPL` int(11) NOT NULL default '0',
  `ID_AFFIL` int(11) NOT NULL default '0',
  `ID_DIPLOME` int(11) NOT NULL default '0',
  `ID_DOM_ETU` int(11) NOT NULL default '0',
  `ID_STRUCT` varchar(50) collate utf8_unicode_ci NOT NULL default '0',
  `STAMP_PROCESSED` datetime NOT NULL default '0000-00-00 00:00:00',
  `USER_AGENT` varchar(50) collate utf8_unicode_ci NOT NULL default '',
  `USER_DIM_1` varchar(50) collate utf8_unicode_ci NOT NULL default '',
  `USER_DIM_2` varchar(50) collate utf8_unicode_ci NOT NULL default '',
  `USER_DIM_3` varchar(50) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`ID`),
  KEY `STAMP_START` (`STAMP_START`),
  KEY `UID` (`UID`,`STAMP_START`),
  KEY `processed_id` (`STAMP_PROCESSED`,`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_cnx`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `t_cnxlog`
-- 

DROP TABLE IF EXISTS `t_cnxlog`;
CREATE TABLE `t_cnxlog` (
  `ID` int(11) NOT NULL default '0',
  `UID` varchar(100) collate utf8_unicode_ci NOT NULL default '',
  `STAMP_START` varchar(30) collate utf8_unicode_ci NOT NULL default '',
  `FLOW_SIZE` int(11) NOT NULL default '0',
  `RESP_TIME` int(11) NOT NULL default '0',
  `TARGET_URL` varchar(255) collate utf8_unicode_ci NOT NULL default '',
  `MIME_TYPE` varchar(100) collate utf8_unicode_ci NOT NULL default '',
  `STAMP_PROCESSED` datetime NOT NULL default '0000-00-00 00:00:00',
  `USER_AGENT` varchar(50) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`ID`),
  KEY `STAMP_START` (`STAMP_START`),
  KEY `processed_id` (`STAMP_PROCESSED`,`ID`),
  KEY `uid_stamp_start` (`UID`,`STAMP_START`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_cnxlog`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `t_dcpl_intra`
-- 

DROP TABLE IF EXISTS `t_dcpl_intra`;
CREATE TABLE `t_dcpl_intra` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `CHAINE_TYPE` varchar(255) collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  `ID_STRUCT` smallint(6) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_dcpl_intra`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `t_dim`
-- 

DROP TABLE IF EXISTS `t_dim`;
CREATE TABLE `t_dim` (
  `ID` int(11) NOT NULL default '0',
  `LIB` varchar(100) collate utf8_unicode_ci default NULL,
  `REF_DIM` varchar(30) collate utf8_unicode_ci default NULL,
  `DIM_ID_LIB` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_dim`
-- 

INSERT INTO `t_dim` VALUES (1, 'Profil', 'Affiliation', 'id_affil');
INSERT INTO `t_dim` VALUES (2, 'Filière', 'Discipline', 'id_dcpl');
INSERT INTO `t_dim` VALUES (3, 'Niveau de Diplome', 'Diplome', 'id_diplome');
INSERT INTO `t_dim` VALUES (4, 'Domaine d''étude', 'DomEtu', 'id_dom_etu');
INSERT INTO `t_dim` VALUES (5, 'Catégorie de service', 'CatSrv', 'id_cat_srv');
INSERT INTO `t_dim` VALUES (7, 'Service interne', 'SrvIntra', 'id_srv_intra');
INSERT INTO `t_dim` VALUES (8, 'Profil interne', 'AffilIntra', 'id_affil_intra');
INSERT INTO `t_dim` VALUES (9, 'Filière interne', 'DcplIntra', 'id_dcpl_intra');
INSERT INTO `t_dim` VALUES (10, 'Diplome interne', 'DiplomeIntra', 'id_diplome_intra');
INSERT INTO `t_dim` VALUES (11, 'Domaine d''Etude interne', 'DomEtuIntra', 'id_dom_etu_intra');

-- --------------------------------------------------------

-- 
-- Structure de la table `t_diplome`
-- 

DROP TABLE IF EXISTS `t_diplome`;
CREATE TABLE `t_diplome` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  `NBR_AN_FORM` smallint(6) NOT NULL default '0',
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_diplome`
-- 

INSERT INTO `t_diplome` VALUES (2, '', 'Baccalauréat', 'Baccalauréat', 2, 0);
INSERT INTO `t_diplome` VALUES (3, '', 'Licence 1', 'Licence 1', 3, 1);
INSERT INTO `t_diplome` VALUES (4, '', 'Licence 2', 'Licence 2', 4, 2);
INSERT INTO `t_diplome` VALUES (5, '', 'Licence 3', 'Licence 3', 5, 3);
INSERT INTO `t_diplome` VALUES (6, '', 'Master 1', 'Master 1', 6, 4);
INSERT INTO `t_diplome` VALUES (7, '', 'Master 2', 'Master 2', 7, 5);
INSERT INTO `t_diplome` VALUES (8, '', 'Doctorat', 'Doctorat', 8, 6);
INSERT INTO `t_diplome` VALUES (9, '', 'BTS', 'BTS', 9, 2);
INSERT INTO `t_diplome` VALUES (10, '', 'DUT', 'DUT', 10, 2);
INSERT INTO `t_diplome` VALUES (1, '', 'Autre', 'Autre', 1, 0);

-- --------------------------------------------------------

-- 
-- Structure de la table `t_diplome_intra`
-- 

DROP TABLE IF EXISTS `t_diplome_intra`;
CREATE TABLE `t_diplome_intra` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `CHAINE_TYPE` varchar(255) collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  `ID_STRUCT` smallint(6) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_diplome_intra`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `t_discipline`
-- 

DROP TABLE IF EXISTS `t_discipline`;
CREATE TABLE `t_discipline` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_discipline`
-- 

INSERT INTO `t_discipline` VALUES (94, '', 'DU Français Langue Etrang', 'DU Français Langue Etrang', 0);
INSERT INTO `t_discipline` VALUES (93, '', 'Master Droit entreprise', 'Master Droit entreprise', 0);
INSERT INTO `t_discipline` VALUES (92, '', 'DUT Statistique et informatique décisionnelle', 'DUT Statistique et informatique décisionnelle', 0);
INSERT INTO `t_discipline` VALUES (1, '', 'Autres filières', '', 0);
INSERT INTO `t_discipline` VALUES (91, '', 'Licence professionnelle  espaces naturels', 'Licence professionnelle  espaces naturels', 0);
INSERT INTO `t_discipline` VALUES (90, '', 'LP Production industrielle', 'LP Production industrielle', 0);
INSERT INTO `t_discipline` VALUES (89, '', 'Doctorat Informatique', 'Doctorat Informatique', 0);
INSERT INTO `t_discipline` VALUES (88, '', 'Master Évaluation, Gestion et Traitements des Pollutions', 'Master Évaluation, Gestion et Traitements des Pollutions', 0);
INSERT INTO `t_discipline` VALUES (87, '', 'Direction administrative et financière', 'Direction administrative et financière', 0);
INSERT INTO `t_discipline` VALUES (86, '', 'Management et administation des entreprises', 'Management et administation des entreprises', 0);
INSERT INTO `t_discipline` VALUES (85, '', 'DUT Gestion des Entreprises et des Administrations', 'DUT Gestion des Entreprises et des Administrations', 0);
INSERT INTO `t_discipline` VALUES (84, '', 'Licence Pro Adjoint Direc', 'Licence Pro Adjoint Direc', 0);
INSERT INTO `t_discipline` VALUES (83, '', 'Management du Sport, des Loisirs et du Tourisme', 'Management du Sport, des Loisirs et du Tourisme', 0);
INSERT INTO `t_discipline` VALUES (82, '', 'Doctorat de Sciences juridiques', 'Doctorat de Sciences juridiques', 0);
INSERT INTO `t_discipline` VALUES (81, '', 'Licence Histoire de l''Art et Archéologie', 'Licence Histoire de l''Art et Archéologie', 0);
INSERT INTO `t_discipline` VALUES (80, '', 'Master Méthodes stochastiqu. & informatiqu. pour la décision', 'Master Méthodes stochastiqu. & informatiqu. pour la décision', 0);
INSERT INTO `t_discipline` VALUES (79, '', 'Master Enseignement  en Education Physique et Sportive', 'Master Enseignement  en Education Physique et Sportive', 0);
INSERT INTO `t_discipline` VALUES (77, '', 'Droit Pénal et Sc Crimin.', 'Droit Pénal et Sc Crimin.', 0);
INSERT INTO `t_discipline` VALUES (78, '', 'Master Dynamique des Écosystèmes Aquatiques', 'Master Dynamique des Écosystèmes Aquatiques', 0);
INSERT INTO `t_discipline` VALUES (75, '', 'DUT Techniques de commercialisation', 'DUT Techniques de commercialisation', 0);
INSERT INTO `t_discipline` VALUES (76, '', 'Année Préparatoire à l''Insertion en Licence Sc. de la Vie', 'Année Préparatoire à l''Insertion en Licence Sc. de la Vie', 0);
INSERT INTO `t_discipline` VALUES (74, '', 'Diplôme Compta Gestion', 'Diplôme Compta Gestion', 0);
INSERT INTO `t_discipline` VALUES (72, '', 'Juriste européen', 'Juriste européen', 0);
INSERT INTO `t_discipline` VALUES (73, '', 'DUT Informatique', 'DUT Informatique', 0);
INSERT INTO `t_discipline` VALUES (71, '', 'Master Environnement et Matériaux, Concepts fondamentaux PC', 'Master Environnement et Matériaux, Concepts fondamentaux PC', 0);
INSERT INTO `t_discipline` VALUES (70, '', 'Doct Sciences Economiques', 'Doct Sciences Economiques', 0);
INSERT INTO `t_discipline` VALUES (69, '', 'Management international', 'Management international', 0);
INSERT INTO `t_discipline` VALUES (68, '', 'Doctorat Génie des procédés', 'Doctorat Génie des procédés', 0);
INSERT INTO `t_discipline` VALUES (67, '', 'DUT Génie Biologique - industries alimentaires & biologiques', 'DUT Génie Biologique - industries alimentaires & biologiques', 0);
INSERT INTO `t_discipline` VALUES (66, '', 'Doctorat de Géographie', 'Doctorat de Géographie', 0);
INSERT INTO `t_discipline` VALUES (65, '', 'Master  Enseignement en  Histoire-Géographie', 'Master  Enseignement en  Histoire-Géographie', 0);
INSERT INTO `t_discipline` VALUES (64, '', 'Master Logistique,act. opérationnelles et syst. d''info', 'Master Logistique,act. opérationnelles et syst. d''info', 0);
INSERT INTO `t_discipline` VALUES (63, '', 'Langue Littérature et culture étrangère', 'Langue Littérature et culture étrangère', 0);
INSERT INTO `t_discipline` VALUES (62, '', 'Doctorat Energétique', 'Doctorat Energétique', 0);
INSERT INTO `t_discipline` VALUES (61, '', 'DUT GIM', 'DUT GIM', 0);
INSERT INTO `t_discipline` VALUES (60, '', 'Métiers ens. Phys chimie', 'Métiers ens. Phys chimie', 0);
INSERT INTO `t_discipline` VALUES (59, '', 'Master Bioprotection, Valorisation non alimentaires des agro', 'Master Bioprotection, Valorisation non alimentaires des agro', 0);
INSERT INTO `t_discipline` VALUES (57, '', 'Doctorat de Physique', 'Doctorat de Physique', 0);
INSERT INTO `t_discipline` VALUES (58, '', 'Aménagement Touristique', 'Aménagement Touristique', 0);
INSERT INTO `t_discipline` VALUES (55, '', 'Master Droit Public', 'Master Droit Public', 0);
INSERT INTO `t_discipline` VALUES (56, '', 'Licence Professionnelle Management des organisations SIDMQ', 'Licence Professionnelle Management des organisations SIDMQ', 0);
INSERT INTO `t_discipline` VALUES (54, '', 'Master Droit notarial', 'Master Droit notarial', 0);
INSERT INTO `t_discipline` VALUES (53, '', 'Métiers des langues', 'Métiers des langues', 0);
INSERT INTO `t_discipline` VALUES (51, '', 'Doctorat Génie civil', 'Doctorat Génie civil', 0);
INSERT INTO `t_discipline` VALUES (52, '', 'Economie de l''intégration européenne et internationale', 'Economie de l''intégration européenne et internationale', 0);
INSERT INTO `t_discipline` VALUES (49, '', 'Licence Histoire', 'Licence Histoire', 0);
INSERT INTO `t_discipline` VALUES (50, '', 'Licence Espagnol', 'Licence Espagnol', 0);
INSERT INTO `t_discipline` VALUES (47, '', 'Master Managment Public', 'Master Managment Public', 0);
INSERT INTO `t_discipline` VALUES (48, '', 'Master Enseignement en  Lettres & Langues', 'Master Enseignement en  Lettres & Langues', 0);
INSERT INTO `t_discipline` VALUES (46, '', 'Master Microbiologie et Biotechnologies', 'Master Microbiologie et Biotechnologies', 0);
INSERT INTO `t_discipline` VALUES (45, '', 'Lic Sciences de la Terre', 'Lic Sciences de la Terre', 0);
INSERT INTO `t_discipline` VALUES (44, '', 'Doctorat de Mathématiques', 'Doctorat de Mathématiques', 0);
INSERT INTO `t_discipline` VALUES (43, '', 'Management et Systèmes d''Information', 'Management et Systèmes d''Information', 0);
INSERT INTO `t_discipline` VALUES (42, '', 'Préparation à l''examen d''entrée au CRFPA', 'Préparation à l''examen d''entrée au CRFPA', 0);
INSERT INTO `t_discipline` VALUES (41, '', 'Licence Géographie', 'Licence Géographie', 0);
INSERT INTO `t_discipline` VALUES (40, '', 'Certificat de Sciences criminelles', 'Certificat de Sciences criminelles', 0);
INSERT INTO `t_discipline` VALUES (39, '', 'Licence Economie-Gestion', 'Licence Economie-Gestion', 0);
INSERT INTO `t_discipline` VALUES (38, '', 'Master ingénierie des systémes industriels', 'Master ingénierie des systémes industriels', 0);
INSERT INTO `t_discipline` VALUES (37, '', 'DU ERASMUS', 'DU ERASMUS', 0);
INSERT INTO `t_discipline` VALUES (35, '', 'Doctorat de chimie', 'Doctorat de chimie', 0);
INSERT INTO `t_discipline` VALUES (36, '', 'Master Technologie de l''internet', 'Master Technologie de l''internet', 0);
INSERT INTO `t_discipline` VALUES (34, '', 'Master Poét & hist litté', 'Master Poét & hist litté', 0);
INSERT INTO `t_discipline` VALUES (33, '', 'Licence Droit', 'Licence Droit', 0);
INSERT INTO `t_discipline` VALUES (32, '', 'Histoire, Histoire de l'' Art, Archéologie, Anthropologie', 'Histoire, Histoire de l'' Art, Archéologie, Anthropologie', 0);
INSERT INTO `t_discipline` VALUES (31, '', 'Année Préparatoire à l''Insertion en Licence Sc. Phys-Chimie', 'Année Préparatoire à l''Insertion en Licence Sc. Phys-Chimie', 0);
INSERT INTO `t_discipline` VALUES (30, '', 'Valorisation des Patrimoines & Politiques Culturelles Territ', 'Valorisation des Patrimoines & Politiques Culturelles Territ', 0);
INSERT INTO `t_discipline` VALUES (29, '', 'Diplôme d''ingénieur de l''ENSGTI', 'Diplôme d''ingénieur de l''ENSGTI', 0);
INSERT INTO `t_discipline` VALUES (28, '', 'Licence Mathématiques', 'Licence Mathématiques', 0);
INSERT INTO `t_discipline` VALUES (27, '', 'Diplôme d''ingénieur de l''ENSGTI spécialié Energétique', 'Diplôme d''ingénieur de l''ENSGTI spécialié Energétique', 0);
INSERT INTO `t_discipline` VALUES (26, '', 'Doctorat Sciences de l''univers', 'Doctorat Sciences de l''univers', 0);
INSERT INTO `t_discipline` VALUES (25, '', 'Master génie pétrolier', 'Master génie pétrolier', 0);
INSERT INTO `t_discipline` VALUES (24, '', 'Licence Langue et Culture Régional', 'Licence Langue et Culture Régional', 0);
INSERT INTO `t_discipline` VALUES (23, '', 'Licence Mathématiques Appliquées et Sciences Sociales', 'Licence Mathématiques Appliquées et Sciences Sociales', 0);
INSERT INTO `t_discipline` VALUES (22, '', 'Diplôme d''ingénieur ENSGTI spécialité génie des procédés', 'Diplôme d''ingénieur ENSGTI spécialité génie des procédés', 0);
INSERT INTO `t_discipline` VALUES (21, '', 'Licence professionnelles biotechnologies', 'Licence professionnelles biotechnologies', 0);
INSERT INTO `t_discipline` VALUES (20, '', 'Licence STAPS', 'Licence STAPS', 0);
INSERT INTO `t_discipline` VALUES (19, '', 'Diplôme d''ingénieur de l''ISA BTP', 'Diplôme d''ingénieur de l''ISA BTP', 0);
INSERT INTO `t_discipline` VALUES (17, '', 'Droit public & adm publiq', 'Droit public & adm publiq', 0);
INSERT INTO `t_discipline` VALUES (18, '', 'Master Méthodes Analytiques pour l''Environnement /matériaux', 'Master Méthodes Analytiques pour l''Environnement /matériaux', 0);
INSERT INTO `t_discipline` VALUES (16, '', 'Entrain., prépa phys & mentale & optimisa. de la performance', 'Entrain., prépa phys & mentale & optimisa. de la performance', 0);
INSERT INTO `t_discipline` VALUES (15, '', 'DUT Réseaux et Télécommunications', 'DUT Réseaux et Télécommunications', 0);
INSERT INTO `t_discipline` VALUES (13, '', 'Licence Biologie', 'Licence Biologie', 0);
INSERT INTO `t_discipline` VALUES (14, '', 'Licence Lettres', 'Licence Lettres', 0);
INSERT INTO `t_discipline` VALUES (11, '', 'Licence LEA', 'Licence LEA', 0);
INSERT INTO `t_discipline` VALUES (12, '', 'Licence Informatique', 'Licence Informatique', 0);
INSERT INTO `t_discipline` VALUES (10, '', 'Master Ingénierie et Sciences des Matériaux', 'Master Ingénierie et Sciences des Matériaux', 0);
INSERT INTO `t_discipline` VALUES (9, '', 'Chargé d''études économiques', 'Chargé d''études économiques', 0);
INSERT INTO `t_discipline` VALUES (8, '', 'Comptabilité, Contrôle, Audit', 'Comptabilité, Contrôle, Audit', 0);
INSERT INTO `t_discipline` VALUES (7, '', 'DUT Génie Thermique et Énergie', 'DUT Génie Thermique et Énergie', 0);
INSERT INTO `t_discipline` VALUES (6, '', 'DUT Science et Génie des Matériaux', 'DUT Science et Génie des Matériaux', 0);
INSERT INTO `t_discipline` VALUES (5, '', 'Licence Anglais', 'Licence Anglais', 0);
INSERT INTO `t_discipline` VALUES (3, '', 'Licence Sciences Physiques et Chimiques', 'Licence Sciences Physiques et Chimiques', 0);
INSERT INTO `t_discipline` VALUES (106, '', 'Gestion de la production industrielle', 'Gestion de la production industrielle', 0);
INSERT INTO `t_discipline` VALUES (105, '', 'Master Mathématiques et leurs applic. - Maths Modèl. Simul.', 'Master Mathématiques et leurs applic. - Maths Modèl. Simul.', 0);
INSERT INTO `t_discipline` VALUES (104, '', 'LP Assurance, Banque, Finance', 'LP Assurance, Banque, Finance', 0);
INSERT INTO `t_discipline` VALUES (103, '', 'Réseaux Télécommunications Intégration Systèmes Voix Données', 'Réseaux Télécommunications Intégration Systèmes Voix Données', 0);
INSERT INTO `t_discipline` VALUES (102, '', 'LP Protection de l'' Environnement', 'LP Protection de l'' Environnement', 0);
INSERT INTO `t_discipline` VALUES (101, '', 'Doct Sciences de gestion', 'Doct Sciences de gestion', 0);
INSERT INTO `t_discipline` VALUES (100, '', 'Licence professionnelle Énergie et Génie climatique', 'Licence professionnelle Énergie et Génie climatique', 0);
INSERT INTO `t_discipline` VALUES (99, '', 'Ecologie Hist  & Comparée', 'Ecologie Hist  & Comparée', 0);
INSERT INTO `t_discipline` VALUES (98, '', 'Master Sociétés, Aménagement, Territoires', 'Master Sociétés, Aménagement, Territoires', 0);
INSERT INTO `t_discipline` VALUES (97, '', 'Master Droit Privé', 'Master Droit Privé', 0);
INSERT INTO `t_discipline` VALUES (96, '', 'Doctorat Histoire art', 'Doctorat Histoire art', 0);
INSERT INTO `t_discipline` VALUES (95, '', 'Licence Administration Economique et Sociale', 'Licence Administration Economique et Sociale', 0);
INSERT INTO `t_discipline` VALUES (107, '', 'Préparation à l'' agrégation d''anglais', 'Préparation à l'' agrégation d''anglais', 0);
INSERT INTO `t_discipline` VALUES (108, '', 'Traduction et Documentation  Scientifique et Technique', 'Traduction et Documentation  Scientifique et Technique', 0);
INSERT INTO `t_discipline` VALUES (109, '', 'Année Préparatoire à l''Insertion en Licence Sc. Math-Info', 'Année Préparatoire à l''Insertion en Licence Sc. Math-Info', 0);
INSERT INTO `t_discipline` VALUES (110, '', 'Année Préparatoire à l''Insertion en Lic. Sanitaire et Social', 'Année Préparatoire à l''Insertion en Lic. Sanitaire et Social', 0);
INSERT INTO `t_discipline` VALUES (111, '', 'Métiers de l''enseignement en mathématiques', 'Métiers de l''enseignement en mathématiques', 0);
INSERT INTO `t_discipline` VALUES (112, '', 'DU ADM TERRITORIALE', 'DU ADM TERRITORIALE', 0);
INSERT INTO `t_discipline` VALUES (113, '', 'DUT SID', 'DUT SID', 0);
INSERT INTO `t_discipline` VALUES (114, '', 'LP Systémes Informatiques et Logiciels', 'LP Systémes Informatiques et Logiciels', 0);
INSERT INTO `t_discipline` VALUES (115, '', 'Coopération transfrontalière et interrégionale', 'Coopération transfrontalière et interrégionale', 0);
INSERT INTO `t_discipline` VALUES (116, '', 'Préparation à l'' agrégation d''espagnol', 'Préparation à l'' agrégation d''espagnol', 0);
INSERT INTO `t_discipline` VALUES (117, '', 'Sécurité des biens et des personnes', 'Sécurité des biens et des personnes', 0);
INSERT INTO `t_discipline` VALUES (118, '', 'DELF/DALF', 'DELF/DALF', 0);
INSERT INTO `t_discipline` VALUES (119, '', 'DU Responsable en logistique et transports', 'DU Responsable en logistique et transports', 0);
INSERT INTO `t_discipline` VALUES (120, '', 'Doctorat Mécanique des fluides', 'Doctorat Mécanique des fluides', 0);
INSERT INTO `t_discipline` VALUES (121, '', 'Doct Physio et biologie des organismes-populations-interacti', 'Doct Physio et biologie des organismes-populations-interacti', 0);
INSERT INTO `t_discipline` VALUES (122, '', 'Master Ingenierie de projet, politiques locales et Tic', 'Master Ingenierie de projet, politiques locales et Tic', 0);
INSERT INTO `t_discipline` VALUES (123, '', 'Réseaux Télécommunications, Administration Sécurité Réseaux', 'Réseaux Télécommunications, Administration Sécurité Réseaux', 0);
INSERT INTO `t_discipline` VALUES (124, '', 'Doctorat STAPS', 'Doctorat STAPS', 0);
INSERT INTO `t_discipline` VALUES (125, '', 'Préparation au concurs d\\''entrée de l\\''ENM', 'Préparation au concurs d\\''entrée de l\\''ENM', 0);
INSERT INTO `t_discipline` VALUES (126, '', 'Année Préparatoire à l\\''Insertion en Licence Sc. de la Terre', 'Année Préparatoire à l\\''Insertion en Licence Sc. de la Terre', 0);
INSERT INTO `t_discipline` VALUES (127, '', 'Année préparatoire à l\\''insertion en licence', 'Année préparatoire à l\\''insertion en licence', 0);
INSERT INTO `t_discipline` VALUES (128, '', 'Licence Pro GRH', 'Licence Pro GRH', 0);
INSERT INTO `t_discipline` VALUES (129, '', 'Doctorat Langues et littératures françaises', 'Doctorat Langues et littératures françaises', 0);
INSERT INTO `t_discipline` VALUES (130, '', 'Doctorat d''Ethnologie', 'Doctorat d''Ethnologie', 0);
INSERT INTO `t_discipline` VALUES (131, '', 'Doct Génie Electrique', 'Doct Génie Electrique', 0);
INSERT INTO `t_discipline` VALUES (132, '', 'Diplôme d''Accès aux Études Universitaires option A', 'Diplôme d''Accès aux Études Universitaires option A', 0);
INSERT INTO `t_discipline` VALUES (133, '', 'DU Techniques multimédias', 'DU Techniques multimédias', 0);
INSERT INTO `t_discipline` VALUES (134, '', 'DU  Préparation au  concours Greffier en chef & Greffier', 'DU  Préparation au  concours Greffier en chef & Greffier', 0);
INSERT INTO `t_discipline` VALUES (135, '', 'Capacité en droit', 'Capacité en droit', 0);
INSERT INTO `t_discipline` VALUES (136, '', 'C2i - niveau 1', 'C2i - niveau 1', 0);
INSERT INTO `t_discipline` VALUES (137, '', 'DU Sciences de l''inadaptation et de la délinquance juvénile', 'DU Sciences de l''inadaptation et de la délinquance juvénile', 0);
INSERT INTO `t_discipline` VALUES (138, '', 'Doctorat Histoire', 'Doctorat Histoire', 0);
INSERT INTO `t_discipline` VALUES (139, '', 'Année Préparatoire à l''Insertion en Licence Sc.  MASS', 'Année Préparatoire à l''Insertion en Licence Sc.  MASS', 0);
INSERT INTO `t_discipline` VALUES (140, '', 'DU Technologies de l''Information et de la Communication', 'DU Technologies de l''Information et de la Communication', 0);
INSERT INTO `t_discipline` VALUES (141, '', 'CAPES HISTOIRE GEOGRAPHIE', 'CAPES HISTOIRE GEOGRAPHIE', 0);
INSERT INTO `t_discipline` VALUES (142, '', 'CAPES ANGLAIS', 'CAPES ANGLAIS', 0);
INSERT INTO `t_discipline` VALUES (143, '', 'Master Langue et Cult Rég', 'Master Langue et Cult Rég', 0);
INSERT INTO `t_discipline` VALUES (144, '', 'Préparation à l'' agrégation de lettres', 'Préparation à l'' agrégation de lettres', 0);
INSERT INTO `t_discipline` VALUES (145, '', 'DU METIERS DE L'' ADMINISTRATION GENERALE TERRITORIALE', 'DU METIERS DE L'' ADMINISTRATION GENERALE TERRITORIALE', 0);

-- --------------------------------------------------------

-- 
-- Structure de la table `t_dometu`
-- 

DROP TABLE IF EXISTS `t_dometu`;
CREATE TABLE `t_dometu` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_dometu`
-- 

INSERT INTO `t_dometu` VALUES (2, '71', 'PLURI SANTE', 'PLURI SANTE', 2);
INSERT INTO `t_dometu` VALUES (3, '80', 'VETERINAIRE', 'VETERINAIRE', 3);
INSERT INTO `t_dometu` VALUES (4, '1', 'MATHEMATIQUES', 'MATHEMATIQUES', 4);
INSERT INTO `t_dometu` VALUES (5, '2', 'PHYSIQUE', 'PHYSIQUE', 5);
INSERT INTO `t_dometu` VALUES (6, '3', 'CHIMIE', 'CHIMIE', 6);
INSERT INTO `t_dometu` VALUES (7, '4', 'MATHEMATIQUES APPLIQUEES ET SCIENCES SOCIALES (M.A.S.S.)', 'MATHEMATIQUES APPLIQUEES ET SCIENCES SOCIALES (M.A.S.S.)', 7);
INSERT INTO `t_dometu` VALUES (8, '5', 'SCIENCES DE L''UNIVERS', 'SCIENCES DE L''UNIVERS', 8);
INSERT INTO `t_dometu` VALUES (9, '6', 'SCIENCES DE LA VIE', 'SCIENCES DE LA VIE', 9);
INSERT INTO `t_dometu` VALUES (10, '7', 'MEDECINE', 'MEDECINE', 10);
INSERT INTO `t_dometu` VALUES (11, '8', 'ODONTOLOGIE', 'ODONTOLOGIE', 11);
INSERT INTO `t_dometu` VALUES (12, '9', 'PHARMACIE', 'PHARMACIE', 12);
INSERT INTO `t_dometu` VALUES (13, '10', 'S.T.A.P.S.', 'S.T.A.P.S.', 13);
INSERT INTO `t_dometu` VALUES (14, '11', 'MECANIQUE, GENIE MECANIQUE', 'MECANIQUE, GENIE MECANIQUE', 14);
INSERT INTO `t_dometu` VALUES (15, '12', 'GENIE CIVIL', 'GENIE CIVIL', 15);
INSERT INTO `t_dometu` VALUES (16, '13', 'GENIE DES PROCEDES', 'GENIE DES PROCEDES', 16);
INSERT INTO `t_dometu` VALUES (17, '14', 'INFORMATIQUE', 'INFORMATIQUE', 17);
INSERT INTO `t_dometu` VALUES (18, '15', 'ELECTRONIQUE, GENIE ELECTRIQUE', 'ELECTRONIQUE, GENIE ELECTRIQUE', 18);
INSERT INTO `t_dometu` VALUES (19, '16', 'SCIENCES ET TECHNOLOGIE INDUSTRIELLES', 'SCIENCES ET TECHNOLOGIE INDUSTRIELLES', 19);
INSERT INTO `t_dometu` VALUES (20, '17', 'SCIENCES DU LANGAGE - LINGUISTIQUE', 'SCIENCES DU LANGAGE - LINGUISTIQUE', 20);
INSERT INTO `t_dometu` VALUES (21, '18', 'LANGUES ET LITTERATURES ANCIENNES', 'LANGUES ET LITTERATURES ANCIENNES', 21);
INSERT INTO `t_dometu` VALUES (22, '19', 'LANGUES ET LITTERATURES FRANCAISES', 'LANGUES ET LITTERATURES FRANCAISES', 22);
INSERT INTO `t_dometu` VALUES (1, '', 'Autres', 'Autres', 1);

-- --------------------------------------------------------

-- 
-- Structure de la table `t_dometu_intra`
-- 

DROP TABLE IF EXISTS `t_dometu_intra`;
CREATE TABLE `t_dometu_intra` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `CHAINE_TYPE` varchar(255) collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  `ID_STRUCT` smallint(6) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_dometu_intra`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `t_indic`
-- 

DROP TABLE IF EXISTS `t_indic`;
CREATE TABLE `t_indic` (
  `ID` int(11) NOT NULL default '0',
  `OWNER` int(11) NOT NULL default '0',
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL default '',
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `TYPE` varchar(100) collate utf8_unicode_ci NOT NULL,
  `UNITE` varchar(100) collate utf8_unicode_ci NOT NULL default '',
  `DEFAULT_OUTPUT` smallint(6) NOT NULL default '0',
  `UPDATE_FREQ` smallint(6) NOT NULL default '0',
  `TRIG` varchar(100) collate utf8_unicode_ci NOT NULL,
  `SQL_STRING` text collate utf8_unicode_ci NOT NULL,
  `COMPLETION_VALUES` varchar(10) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_indic`
-- 

INSERT INTO `t_indic` VALUES (1, 1, 'Taux de connexion brut par profil par etb.', 'Évaluer le taux d’usage d’un service suivant les différents paramètres du profil de connexion\r\nRenvoi le rapport du nombre d\\''uid sur la population (par profil)\r\n(Validé le 23/09/10 avec HKR)', '2', '3', 2, 1, '', 'SELECT \r\nt_agregat.id_affil as pivotval, t_affiliation.lib as pivotlib, concat(lpad(t_ut.annee,4,\\''0000\\''),\\''/\\'',lpad(t_ut.mois,2,\\''00\\''),\\''/\\'',lpad(t_ut.jour,2,\\''00\\'')) as lib,\r\n ((sum(nb_uid)/r_denombrement.nbr_occ)*100) as val, sum(nb_uid) as nbuid, r_denombrement.nbr_occ as denombr, \r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax  \r\nFROM `t_agregat`, t_ut,t_affiliation, r_denombrement \r\nWHERE \r\n(t_agregat.id_struct=r_denombrement.id_struct) \r\nand t_ut.id=id_temps\r\nand t_affiliation.id = t_agregat.id_affil\r\nand r_denombrement.ref_dim=1 and r_denombrement.id_dim=t_agregat.id_affil\r\n%STRUCTSEL% %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by  pivotlib,t_ut.annee, t_ut.mois,t_ut.jour\r\norder by t_ut.annee, t_ut.mois,t_ut.jour,pivotlib', '0');
INSERT INTO `t_indic` VALUES (18, 1, 'Liste des URLS détectées', 'Renvoie un tableau fournissant la liste des URLs détectées par AGIMUS', '3', '4', 1, 1, '', 'select target_url as lib, count(stamp_start) as val from t_cnxlog\r\ngroup by lib\r\norder by val desc, lib asc', '0');
INSERT INTO `t_indic` VALUES (4, 1, 'Nombre de services opérationnels par etb.', 'L’établissement déploie les services afin d’assurer une couverture large des besoins des usagers\r\n\r\n  (validé le 23/09/2010 avec HKR)', '1', '4', 5, 4, '', 'SELECT t_cat_srv.lib as lib , count(distinct(t_agregat.id_srv_intra)) as val,\r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax\r\nfrom t_srv_intra left join t_agregat on t_srv_intra.id =t_agregat.id_srv_intra, t_cat_srv \r\nwhere 1  \r\nand t_cat_srv.id=t_srv_intra.id_regr  \r\n\r\n %DIMSEL% %DATEDEB% %DATEFIN%\r\n\r\ngroup by t_cat_srv.id   ORDER BY lib\r\n\r\n', '0');
INSERT INTO `t_indic` VALUES (5, 1, 'Fréquence moyenne d’usage du service', 'Fournit le nombre moyen d\\''utilisations d\\''une catégorie de service par jour et par profil\r\n(validation interne le 23/09/2010)', '2', '4', 4, 4, '', 'SELECT\r\nt_agregat.id_affil as pivotval, t_affiliation.lib as pivotlib, t_cat_srv.lib as lib, \r\nsum(nb_cnx), \r\ntotcnx, (sum(nb_cnx)/totcnx) as val \r\n\r\nFROM `t_agregat`, t_affiliation, t_cat_srv,\r\n(select id_affil as agr1affil, sum(nb_cnx) as totcnx from t_agregat as t_agr1  group by id_affil) as t_totcnx\r\n\r\nwhere 1\r\nand t_agregat.id_affil = t_affiliation.id \r\nand t_agregat.id_cat_srv = t_cat_srv.id \r\nand t_totcnx.agr1affil=t_agregat.id_affil\r\n\r\n%STRUCTSEL% %DIMSEL% %DATEDEB% %DATEFIN% \r\n\r\nGROUP BY ID_AFFIL, ID_CAT_SRV ORDER BY ID_AFFIL \r\n', '0');
INSERT INTO `t_indic` VALUES (6, 1, 'Utilisation des Catégories de Service', 'dénombrement des connexions par catégorie de service', '0', '4', 4, 3, '', 'select t_ut.annee as pivotlib, t_ut.annee as pivotval, t_cat_srv.lib as lib, sum(t_agregat.nb_cnx) as val,\r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax\r\n from t_agregat, t_cat_srv,t_ut \r\nwhere 1\r\nand t_agregat.id_temps = t_ut.id\r\nand t_agregat.id_cat_srv = t_cat_srv.id\r\n%STRUCTSEL% %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by t_ut.annee,t_agregat.id_cat_srv \r\norder by t_ut.annee,val desc', '0');
INSERT INTO `t_indic` VALUES (7, 1, 'Liste des applications détectées', 'Renvoie un tableau fournissant la liste des applications détectées par AGIMUS', '3', '2', 1, 1, '', 'SELECT t_application.lib as lib, count(t_cnx.id_appli) as val from t_application,t_cnx\r\nwhere t_cnx.id_appli = t_application.id\r\ngroup by t_cnx.id_appli\r\norder by val desc', '0');
INSERT INTO `t_indic` VALUES (10, 1, 'Utilisation des Services intra-etablissement', '', '0', '2', 4, 2, '', 'SELECT t_agregat.id_affil as pivotval, t_affiliation.lib as pivotlib, t_srv_intra.lib as lib,\r\n (sum(t_agregat.volume)/1024000) as val,\r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax,\r\n1 as pivotid  \r\nFROM `t_agregat`, t_ut, t_srv_intra,t_affiliation \r\nWHERE t_ut.id=id_temps\r\nand t_srv_intra.id = t_agregat.id_srv_intra\r\nand t_affiliation.id = t_agregat.id_affil\r\n %STRUCTSEL% %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by  t_agregat.id_affil,t_agregat.id_srv_intra\r\norder by t_affiliation.lib\r\n', '0');
INSERT INTO `t_indic` VALUES (11, 1, 'Couverture applicative', 'Représentation (stable) du nombre de services déployés par catégorie de services\r\n- Les services ne sont pas forcément opérationnels  \r\n(validation interne le 23/09/2010)', '2', '4', 5, 3, '', 'SELECT t_cat_srv.lib as lib , count(t_srv_intra.id ) as val \r\nfrom t_cat_srv left join t_srv_intra on t_cat_srv.id =t_srv_intra.id_regr  where 1 \r\n group by t_cat_srv.id   ORDER BY lib\r\n', '0');
INSERT INTO `t_indic` VALUES (12, 1, 'Utilisateurs quotidiens', 'Nombre d\\''utilisateurs quotidiens', '1', '4', 4, 1, '', 'select t_agregat.id_affil as pilotval, t_affiliation.lib as pivotlib, concat(t_ut.jour,\\"/\\",t_ut.mois,\\"/\\",t_ut.annee) as lib, sum(t_agregat.nb_uid) as val from t_agregat, t_ut, t_affiliation \r\nwhere 1 \r\nand t_agregat.id_temps = t_ut.id \r\nand t_agregat.id_affil = t_affiliation.id \r\n%STRUCTSEL%  %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by t_agregat.id_affil, t_ut.annee, t_ut.mois, t_ut.jour\r\norder by t_ut.annee, t_ut.mois, t_ut.jour,  t_agregat.id_affil', '0');
INSERT INTO `t_indic` VALUES (13, 1, 'Analyse par type de navigateur ', 'Renvoie les % d\\''utilisateurs pour les différents navigateurs repérés par le dispositif.\r\n (validation interne le 23/09/2010)', '2', '4', 3, 4, '', 'select t_agregat.user_agent  as lib, sum(t_agregat.nb_uid) as val,\r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax\r\n from t_agregat, t_ut \r\nwhere 1\r\n%STRUCTSEL% %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by lib \r\norder by val desc', '0');
INSERT INTO `t_indic` VALUES (14, 1, 'Nombre de connexions par Filière', ' (validé le 23/09/2010 avec HKR)', '1', '4', 1, 1, '', 'SELECT t_discipline.id as pivotval, t_discipline.lib as pivotlib, concat(lpad(t_ut.annee,4,\\''0000\\''),\\''-\\'',lpad(t_ut.mois,2,\\''00\\'')) as lib,\r\nsum(t_agregat.nb_uid) as val,\r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax  \r\nFROM `t_agregat`, t_ut, t_discipline \r\nWHERE 1 \r\nand t_ut.id=id_temps \r\nand t_discipline.id = t_agregat.id_dcpl \r\n %STRUCTSEL% %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by  t_agregat.id_dcpl, lib \r\norder by val desc, t_discipline.lib', '0');
INSERT INTO `t_indic` VALUES (15, 1, 'Nombre moyen de services opérationnels par UNR', ' (validé le 23/09/2010 avec HKR)', '2', '4', 5, 4, '', 'SELECT  t_cat_srv.lib as lib , count(distinct(t_agregat.id_srv_intra)) as val,\r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax\r\nfrom t_srv_intra left join t_agregat on t_srv_intra.id =t_agregat.id_srv_intra, t_cat_srv,\r\n(select count(t_struct.id) as totstruct from t_struct where parent=\\''%IDSTRUCTPERE%\\'') as t_totstruct \r\nwhere 1  \r\nand t_cat_srv.id=t_srv_intra.id_regr  \r\n\r\n%DIMSEL% %DATEDEB% %DATEFIN%\r\n\r\ngroup by t_cat_srv.id   ORDER BY lib', '0');
INSERT INTO `t_indic` VALUES (16, 1, 'Taux de connexion brut par catégorie de service (UNR)', ' Renvoit le rapport du nombre d\\''uid par unité de temps sur la population totale\r\n(validé le 23/09/2010 avec HKR)', '2', '3', 4, 4, '', 'SELECT \r\nt_agregat.id_cat_srv as pivotval, t_cat_srv.lib as pivotlib, concat(t_ut.jour,\\"/\\",t_ut.mois,\\"/\\",t_ut.annee) as lib,\r\n(sum(nb_uid)/count(distinct(id_temps)) / totdenombr*100\r\n) as val, (sum(nb_uid)/count(distinct(id_temps))) as nbuid, totdenombr ,\r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax  \r\nFROM `t_agregat`, t_ut,t_affiliation, (select sum(nbr_occ) as totdenombr from r_denombrement where ref_dim=1) as t_denombr, t_cat_srv \r\nWHERE 1\r\nand t_ut.id=id_temps\r\nand t_affiliation.id = t_agregat.id_affil\r\nand t_agregat.id_cat_srv = t_cat_srv.id \r\n%STRUCTSEL% %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by  t_cat_srv.id, t_ut.annee, t_ut.mois, t_ut.jour\r\norder by t_ut.annee, t_ut.mois, t_ut.jour, t_cat_srv.id\r\n', '0');
INSERT INTO `t_indic` VALUES (17, 1, 'Maintenance des dénombrements par profil', 'si résultat mauvais, mettre à jour les dénombrements', '3', '4', 1, 1, '', 'select id_temps, t_agregat.id_affil, sum(nb_uid)\r\n from t_agregat\r\ngroup by t_agregat.id_affil, id_temps\r\norder by id_temps', '0');
INSERT INTO `t_indic` VALUES (19, 1, 'Durée moyenne de session', 'Graphe retournant le nombre de minutes passées, en moyenne, par un profil, sur un service de l\\''ENT.', '1', '1', 2, 2, '', 'SELECT \r\nt_agregat.id_affil as pivotval, t_affiliation.lib as pivotlib,\r\n concat(t_ut.jour,\\"/\\",t_ut.mois,\\"/\\",t_ut.annee) as lib, \r\n (sum(duree)/sum(nb_cnx)/60) as val,\r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax \r\nFROM `t_agregat`, t_ut, t_srv_intra,t_affiliation \r\nWHERE t_ut.id=id_temps\r\nand t_srv_intra.id = t_agregat.id_srv_intra\r\nand t_affiliation.id = t_agregat.id_affil\r\n%STRUCTSEL% %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by t_agregat.id_affil, t_ut.annee, t_ut.mois, t_ut.jour\r\norder by t_agregat.id_affil, t_ut.annee, t_ut.mois, t_ut.jour', '0');

-- --------------------------------------------------------

-- 
-- Structure de la table `t_int`
-- 

DROP TABLE IF EXISTS `t_int`;
CREATE TABLE `t_int` (
  `INT_COD` int(11) NOT NULL default '0',
  `INT_TRG` varchar(5) collate utf8_unicode_ci NOT NULL default '',
  `INT_NOM` varchar(100) collate utf8_unicode_ci NOT NULL default '',
  `INT_PRE` varchar(50) collate utf8_unicode_ci NOT NULL default '',
  `STRUCT_REF` tinyint(4) NOT NULL default '0',
  `INT_EMA` varchar(100) collate utf8_unicode_ci default NULL,
  `INT_PERE` smallint(6) NOT NULL default '0',
  `IND_SUPP` char(1) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`INT_COD`),
  KEY `INT_TRG` (`INT_TRG`),
  KEY `T_INT_PK` (`INT_COD`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_int`
-- 

INSERT INTO `t_int` VALUES (1, 'ADM', 'ADMINISTRATEUR', '-', 0, '', 0, '');
INSERT INTO `t_int` VALUES (6, 'PiA', 'AGIMUS', 'Pilote', 0, 'hkromm@acthan-expertises.com', 0, '');
INSERT INTO `t_int` VALUES (7, 'LeG', 'AGIMUS', 'Lecteur', 0, 'contact@acthan-expertises.com', 0, '');
INSERT INTO `t_int` VALUES (8, 'PPP', 'pilote PPR', 'pilote', 0, '', 0, '');

-- --------------------------------------------------------

-- 
-- Structure de la table `t_srv_intra`
-- 

DROP TABLE IF EXISTS `t_srv_intra`;
CREATE TABLE `t_srv_intra` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `CHAINE_TYPE` varchar(255) collate utf8_unicode_ci NOT NULL,
  `ID_REGR` smallint(6) NOT NULL,
  `ID_STRUCT` smallint(6) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_srv_intra`
-- 

INSERT INTO `t_srv_intra` VALUES (28, '', 'Webmail', 'Webmail', '', 9, 0);
INSERT INTO `t_srv_intra` VALUES (4, '', 'Annuaire Membres', 'Annuaire Membres', '', 6, 0);
INSERT INTO `t_srv_intra` VALUES (12, '', 'Plateforme Enseignement', 'Plateforme Enseignement', '', 3, 0);
INSERT INTO `t_srv_intra` VALUES (23, 'Agenda professionnel', 'Agenda professionnel', 'Agenda professionnel', '#agenda#', 7, 0);
INSERT INTO `t_srv_intra` VALUES (1, '', 'Autre', '', '', 1, 0);

-- --------------------------------------------------------

-- 
-- Structure de la table `t_struct`
-- 

DROP TABLE IF EXISTS `t_struct`;
CREATE TABLE `t_struct` (
  `ID` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL default '',
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `PARENT` varchar(50) collate utf8_unicode_ci default NULL,
  `ID_CAT_STRUCT` tinyint(4) NOT NULL default '0',
  `URL_EXPORT` varchar(255) collate utf8_unicode_ci NOT NULL,
  `USER` varchar(255) collate utf8_unicode_ci NOT NULL,
  `PWD` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_struct`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `t_tracelog`
-- 

DROP TABLE IF EXISTS `t_tracelog`;
CREATE TABLE `t_tracelog` (
  `ID` int(11) NOT NULL default '0',
  `COOKIE_ID` varchar(100) collate utf8_unicode_ci NOT NULL,
  `UID` varchar(255) collate utf8_unicode_ci NOT NULL default '',
  `STAMP_PROCESSED` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`ID`),
  KEY `COOKIE_ID` (`COOKIE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_tracelog`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `t_traitement`
-- 

DROP TABLE IF EXISTS `t_traitement`;
CREATE TABLE `t_traitement` (
  `ID` int(11) NOT NULL,
  `TRT_COD` varchar(30) collate utf8_unicode_ci NOT NULL,
  `TRT_DATE_DEB` datetime NOT NULL,
  `TRT_DATE_FIN` datetime NOT NULL,
  `TRT_PARAMS` varchar(255) collate utf8_unicode_ci NOT NULL,
  `TRT_STATUS` tinyint(4) NOT NULL,
  `TRT_LOG` text collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_traitement`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `t_ut`
-- 

DROP TABLE IF EXISTS `t_ut`;
CREATE TABLE `t_ut` (
  `ID` int(11) NOT NULL default '0',
  `ANNEE` smallint(6) NOT NULL default '0',
  `MOIS` tinyint(4) NOT NULL default '0',
  `SEMAINE` tinyint(4) NOT NULL default '0',
  `JOUR` tinyint(4) NOT NULL default '0',
  `HEURE` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_ut`
-- 

-- --------------------------------------------------------

-- 
-- Structure de la table `user_pwd_history`
-- 

DROP TABLE IF EXISTS `user_pwd_history`;
CREATE TABLE `user_pwd_history` (
  `INT_COD` int(11) NOT NULL default '0',
  `AUT_PWD` varchar(32) collate utf8_unicode_ci NOT NULL default '',
  `DATE_PWD` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`INT_COD`,`AUT_PWD`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `user_pwd_history`
-- 

INSERT INTO `user_pwd_history` VALUES (1, 'e0a30ac283dff16147fd7dfb7ee4cec4', '2010-02-17');
INSERT INTO `user_pwd_history` VALUES (3, '2cb0985f22e099d18e0bd1a876d48019', '2010-05-06');
INSERT INTO `user_pwd_history` VALUES (1, 'f7c70ab179b88e9640c73f271cddec57', '2010-06-03');
INSERT INTO `user_pwd_history` VALUES (3, '719430328e11f79a55f4c95b2faccfec', '2010-09-24');
INSERT INTO `user_pwd_history` VALUES (3, '0ea58701b84295bdd11c5b05426c6c3f', '2010-11-02');
INSERT INTO `user_pwd_history` VALUES (5, '0183459a6cb7a31c13f0f1f52ec011d2', '2010-11-04');
