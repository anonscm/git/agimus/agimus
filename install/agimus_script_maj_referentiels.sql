-- phpMyAdmin SQL Dump
-- version 2.8.2.4
-- http://www.phpmyadmin.net
-- 
-- Serveur: cribd.univ-pau.fr
-- Généré le : Jeudi 23 Septembre 2010 à 11:53
-- Version du serveur: 5.0.51
-- Version de PHP: 4.4.1
-- 
-- Base de données: `agimus_maquette`
-- 

-- --------------------------------------------------------

-- 
-- Structure de la table `t_affiliation`
-- 

DROP TABLE IF EXISTS `t_affiliation`;
CREATE TABLE `t_affiliation` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_affiliation`
-- 

INSERT INTO `t_affiliation` VALUES (1, '', 'Autres', 'Autres', 1);
INSERT INTO `t_affiliation` VALUES (2, '', 'Etudiant', 'Etudiant', 2);
INSERT INTO `t_affiliation` VALUES (3, '', 'Enseignant', 'Enseignant', 3);
INSERT INTO `t_affiliation` VALUES (4, '', 'IATOS', 'IATOS', 4);
INSERT INTO `t_affiliation` VALUES (5, '', 'Chercheurs', 'Chercheurs', 5);

-- --------------------------------------------------------

-- 
-- Structure de la table `t_cat_srv`
-- 

DROP TABLE IF EXISTS `t_cat_srv`;
CREATE TABLE `t_cat_srv` (
  `ID` smallint(6) NOT NULL default '0',
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_cat_srv`
-- 

INSERT INTO `t_cat_srv` VALUES (9, 'Information et Communication', 'Webmail, Forum, Chat, Annuaire des membres, Diffusion s?lective d?information des outils de gestion de contenu');
INSERT INTO `t_cat_srv` VALUES (2, 'Scolarité', 'Inscriptions ; Réinscription ; Dossier administratif ; Consultation des notes ; Calendrier des examens ; Emploi du temps ;\r\n');
INSERT INTO `t_cat_srv` VALUES (3, 'Plateforme d''enseignement', 'Services à définir. Quelquefois redondant avec des services inscrits dans d''autres catégories.');
INSERT INTO `t_cat_srv` VALUES (4, 'Documentation', 'Dossier lecteur ; Réservation en ligne ; Accès aux catalogues ; Accès aux dictionnaires, encyclopédies et autres ressources génériques ; Accès aux ressources spécifiques; Moteur de recherche fédéré;');
INSERT INTO `t_cat_srv` VALUES (5, 'Gestion financière', 'Services à définir');
INSERT INTO `t_cat_srv` VALUES (6, 'Gestion des personnels', 'Suivi des missions ; Dossier Personnel ; Congés et absences');
INSERT INTO `t_cat_srv` VALUES (7, 'Bureau numérique', 'Service de consultation d''agenda (personnel ou de groupe), Service d''accès au stockage en ligne');
INSERT INTO `t_cat_srv` VALUES (8, 'Relation entreprise ', 'Gestion des stages ');
INSERT INTO `t_cat_srv` VALUES (1, 'Autre', 'Autre');

-- --------------------------------------------------------

-- 
-- Structure de la table `t_cat_struct`
-- 

DROP TABLE IF EXISTS `t_cat_struct`;
CREATE TABLE `t_cat_struct` (
  `ID` smallint(6) NOT NULL default '0',
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_cat_struct`
-- 

INSERT INTO `t_cat_struct` VALUES (1, 'Ministère', 'Ministère');
INSERT INTO `t_cat_struct` VALUES (2, 'U.N.R.', 'Université Numérique en Région');
INSERT INTO `t_cat_struct` VALUES (3, 'Université', 'Université');
INSERT INTO `t_cat_struct` VALUES (4, 'IUT', 'Institut Universitaire de Technologie');
INSERT INTO `t_cat_struct` VALUES (5, 'UFR', 'Unité de formation et de recherche');

-- --------------------------------------------------------

-- 
-- Structure de la table `t_dim`
-- 

DROP TABLE IF EXISTS `t_dim`;
CREATE TABLE `t_dim` (
  `ID` int(11) NOT NULL default '0',
  `LIB` varchar(100) collate utf8_unicode_ci default NULL,
  `REF_DIM` varchar(30) collate utf8_unicode_ci default NULL,
  `DIM_ID_LIB` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_dim`
-- 

INSERT INTO `t_dim` VALUES (1, 'Profil', 'Affiliation', 'id_affil');
INSERT INTO `t_dim` VALUES (2, 'Filière', 'Discipline', 'id_dcpl');
INSERT INTO `t_dim` VALUES (3, 'Niveau de Diplome', 'Diplome', 'id_diplome');
INSERT INTO `t_dim` VALUES (4, 'Domaine d''étude', 'DomEtu', 'id_dom_etu');
INSERT INTO `t_dim` VALUES (5, 'Catégorie de service', 'CatSrv', 'id_cat_srv');
INSERT INTO `t_dim` VALUES (7, 'Service interne', 'SrvIntra', 'id_srv_intra');
INSERT INTO `t_dim` VALUES (8, 'Profil interne', 'AffilIntra', 'id_affil_intra');
INSERT INTO `t_dim` VALUES (9, 'Filière interne', 'DcplIntra', 'id_dcpl_intra');
INSERT INTO `t_dim` VALUES (10, 'Diplome interne', 'DiplomeIntra', 'id_diplome_intra');
INSERT INTO `t_dim` VALUES (11, 'Domaine d''Etude interne', 'DomEtuIntra', 'id_dom_etu_intra');

-- --------------------------------------------------------

-- 
-- Structure de la table `t_diplome`
-- 

DROP TABLE IF EXISTS `t_diplome`;
CREATE TABLE `t_diplome` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  `NBR_AN_FORM` smallint(6) NOT NULL default '0',
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_diplome`
-- 

INSERT INTO `t_diplome` VALUES (2, '', 'Baccalauréat', 'Baccalauréat', 2, 0);
INSERT INTO `t_diplome` VALUES (3, '', 'Licence 1', 'Licence 1', 3, 1);
INSERT INTO `t_diplome` VALUES (4, '', 'Licence 2', 'Licence 2', 4, 2);
INSERT INTO `t_diplome` VALUES (5, '', 'Licence 3', 'Licence 3', 5, 3);
INSERT INTO `t_diplome` VALUES (6, '', 'Master 1', 'Master 1', 6, 4);
INSERT INTO `t_diplome` VALUES (7, '', 'Master 2', 'Master 2', 7, 5);
INSERT INTO `t_diplome` VALUES (8, '', 'Doctorat', 'Doctorat', 8, 6);
INSERT INTO `t_diplome` VALUES (9, '', 'BTS', 'BTS', 9, 2);
INSERT INTO `t_diplome` VALUES (10, '', 'DUT', 'DUT', 10, 2);
INSERT INTO `t_diplome` VALUES (1, '', 'Autre', 'Autre', 1, 0);

-- --------------------------------------------------------

-- 
-- Structure de la table `t_discipline`
-- 

DROP TABLE IF EXISTS `t_discipline`;
CREATE TABLE `t_discipline` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_discipline`
-- 

INSERT INTO `t_discipline` VALUES (94, '', 'DU Français Langue Etrang', 'DU Français Langue Etrang', 0);
INSERT INTO `t_discipline` VALUES (93, '', 'Master Droit entreprise', 'Master Droit entreprise', 0);
INSERT INTO `t_discipline` VALUES (92, '', 'DUT Statistique et informatique décisionnelle', 'DUT Statistique et informatique décisionnelle', 0);
INSERT INTO `t_discipline` VALUES (1, '', 'Autres filières', '', 0);
INSERT INTO `t_discipline` VALUES (91, '', 'Licence professionnelle  espaces naturels', 'Licence professionnelle  espaces naturels', 0);
INSERT INTO `t_discipline` VALUES (90, '', 'LP Production industrielle', 'LP Production industrielle', 0);
INSERT INTO `t_discipline` VALUES (89, '', 'Doctorat Informatique', 'Doctorat Informatique', 0);
INSERT INTO `t_discipline` VALUES (88, '', 'Master Évaluation, Gestion et Traitements des Pollutions', 'Master Évaluation, Gestion et Traitements des Pollutions', 0);
INSERT INTO `t_discipline` VALUES (87, '', 'Direction administrative et financière', 'Direction administrative et financière', 0);
INSERT INTO `t_discipline` VALUES (86, '', 'Management et administation des entreprises', 'Management et administation des entreprises', 0);
INSERT INTO `t_discipline` VALUES (85, '', 'DUT Gestion des Entreprises et des Administrations', 'DUT Gestion des Entreprises et des Administrations', 0);
INSERT INTO `t_discipline` VALUES (84, '', 'Licence Pro Adjoint Direc', 'Licence Pro Adjoint Direc', 0);
INSERT INTO `t_discipline` VALUES (83, '', 'Management du Sport, des Loisirs et du Tourisme', 'Management du Sport, des Loisirs et du Tourisme', 0);
INSERT INTO `t_discipline` VALUES (82, '', 'Doctorat de Sciences juridiques', 'Doctorat de Sciences juridiques', 0);
INSERT INTO `t_discipline` VALUES (81, '', 'Licence Histoire de l''Art et Archéologie', 'Licence Histoire de l''Art et Archéologie', 0);
INSERT INTO `t_discipline` VALUES (80, '', 'Master Méthodes stochastiqu. & informatiqu. pour la décision', 'Master Méthodes stochastiqu. & informatiqu. pour la décision', 0);
INSERT INTO `t_discipline` VALUES (79, '', 'Master Enseignement  en Education Physique et Sportive', 'Master Enseignement  en Education Physique et Sportive', 0);
INSERT INTO `t_discipline` VALUES (77, '', 'Droit Pénal et Sc Crimin.', 'Droit Pénal et Sc Crimin.', 0);
INSERT INTO `t_discipline` VALUES (78, '', 'Master Dynamique des Écosystèmes Aquatiques', 'Master Dynamique des Écosystèmes Aquatiques', 0);
INSERT INTO `t_discipline` VALUES (75, '', 'DUT Techniques de commercialisation', 'DUT Techniques de commercialisation', 0);
INSERT INTO `t_discipline` VALUES (76, '', 'Année Préparatoire à l''Insertion en Licence Sc. de la Vie', 'Année Préparatoire à l''Insertion en Licence Sc. de la Vie', 0);
INSERT INTO `t_discipline` VALUES (74, '', 'Diplôme Compta Gestion', 'Diplôme Compta Gestion', 0);
INSERT INTO `t_discipline` VALUES (72, '', 'Juriste européen', 'Juriste européen', 0);
INSERT INTO `t_discipline` VALUES (73, '', 'DUT Informatique', 'DUT Informatique', 0);
INSERT INTO `t_discipline` VALUES (71, '', 'Master Environnement et Matériaux, Concepts fondamentaux PC', 'Master Environnement et Matériaux, Concepts fondamentaux PC', 0);
INSERT INTO `t_discipline` VALUES (70, '', 'Doct Sciences Economiques', 'Doct Sciences Economiques', 0);
INSERT INTO `t_discipline` VALUES (69, '', 'Management international', 'Management international', 0);
INSERT INTO `t_discipline` VALUES (68, '', 'Doctorat Génie des procédés', 'Doctorat Génie des procédés', 0);
INSERT INTO `t_discipline` VALUES (67, '', 'DUT Génie Biologique - industries alimentaires & biologiques', 'DUT Génie Biologique - industries alimentaires & biologiques', 0);
INSERT INTO `t_discipline` VALUES (66, '', 'Doctorat de Géographie', 'Doctorat de Géographie', 0);
INSERT INTO `t_discipline` VALUES (65, '', 'Master  Enseignement en  Histoire-Géographie', 'Master  Enseignement en  Histoire-Géographie', 0);
INSERT INTO `t_discipline` VALUES (64, '', 'Master Logistique,act. opérationnelles et syst. d''info', 'Master Logistique,act. opérationnelles et syst. d''info', 0);
INSERT INTO `t_discipline` VALUES (63, '', 'Langue Littérature et culture étrangère', 'Langue Littérature et culture étrangère', 0);
INSERT INTO `t_discipline` VALUES (62, '', 'Doctorat Energétique', 'Doctorat Energétique', 0);
INSERT INTO `t_discipline` VALUES (61, '', 'DUT GIM', 'DUT GIM', 0);
INSERT INTO `t_discipline` VALUES (60, '', 'Métiers ens. Phys chimie', 'Métiers ens. Phys chimie', 0);
INSERT INTO `t_discipline` VALUES (59, '', 'Master Bioprotection, Valorisation non alimentaires des agro', 'Master Bioprotection, Valorisation non alimentaires des agro', 0);
INSERT INTO `t_discipline` VALUES (57, '', 'Doctorat de Physique', 'Doctorat de Physique', 0);
INSERT INTO `t_discipline` VALUES (58, '', 'Aménagement Touristique', 'Aménagement Touristique', 0);
INSERT INTO `t_discipline` VALUES (55, '', 'Master Droit Public', 'Master Droit Public', 0);
INSERT INTO `t_discipline` VALUES (56, '', 'Licence Professionnelle Management des organisations SIDMQ', 'Licence Professionnelle Management des organisations SIDMQ', 0);
INSERT INTO `t_discipline` VALUES (54, '', 'Master Droit notarial', 'Master Droit notarial', 0);
INSERT INTO `t_discipline` VALUES (53, '', 'Métiers des langues', 'Métiers des langues', 0);
INSERT INTO `t_discipline` VALUES (51, '', 'Doctorat Génie civil', 'Doctorat Génie civil', 0);
INSERT INTO `t_discipline` VALUES (52, '', 'Economie de l''intégration européenne et internationale', 'Economie de l''intégration européenne et internationale', 0);
INSERT INTO `t_discipline` VALUES (49, '', 'Licence Histoire', 'Licence Histoire', 0);
INSERT INTO `t_discipline` VALUES (50, '', 'Licence Espagnol', 'Licence Espagnol', 0);
INSERT INTO `t_discipline` VALUES (47, '', 'Master Managment Public', 'Master Managment Public', 0);
INSERT INTO `t_discipline` VALUES (48, '', 'Master Enseignement en  Lettres & Langues', 'Master Enseignement en  Lettres & Langues', 0);
INSERT INTO `t_discipline` VALUES (46, '', 'Master Microbiologie et Biotechnologies', 'Master Microbiologie et Biotechnologies', 0);
INSERT INTO `t_discipline` VALUES (45, '', 'Lic Sciences de la Terre', 'Lic Sciences de la Terre', 0);
INSERT INTO `t_discipline` VALUES (44, '', 'Doctorat de Mathématiques', 'Doctorat de Mathématiques', 0);
INSERT INTO `t_discipline` VALUES (43, '', 'Management et Systèmes d''Information', 'Management et Systèmes d''Information', 0);
INSERT INTO `t_discipline` VALUES (42, '', 'Préparation à l''examen d''entrée au CRFPA', 'Préparation à l''examen d''entrée au CRFPA', 0);
INSERT INTO `t_discipline` VALUES (41, '', 'Licence Géographie', 'Licence Géographie', 0);
INSERT INTO `t_discipline` VALUES (40, '', 'Certificat de Sciences criminelles', 'Certificat de Sciences criminelles', 0);
INSERT INTO `t_discipline` VALUES (39, '', 'Licence Economie-Gestion', 'Licence Economie-Gestion', 0);
INSERT INTO `t_discipline` VALUES (38, '', 'Master ingénierie des systémes industriels', 'Master ingénierie des systémes industriels', 0);
INSERT INTO `t_discipline` VALUES (37, '', 'DU ERASMUS', 'DU ERASMUS', 0);
INSERT INTO `t_discipline` VALUES (35, '', 'Doctorat de chimie', 'Doctorat de chimie', 0);
INSERT INTO `t_discipline` VALUES (36, '', 'Master Technologie de l''internet', 'Master Technologie de l''internet', 0);
INSERT INTO `t_discipline` VALUES (34, '', 'Master Poét & hist litté', 'Master Poét & hist litté', 0);
INSERT INTO `t_discipline` VALUES (33, '', 'Licence Droit', 'Licence Droit', 0);
INSERT INTO `t_discipline` VALUES (32, '', 'Histoire, Histoire de l'' Art, Archéologie, Anthropologie', 'Histoire, Histoire de l'' Art, Archéologie, Anthropologie', 0);
INSERT INTO `t_discipline` VALUES (31, '', 'Année Préparatoire à l''Insertion en Licence Sc. Phys-Chimie', 'Année Préparatoire à l''Insertion en Licence Sc. Phys-Chimie', 0);
INSERT INTO `t_discipline` VALUES (30, '', 'Valorisation des Patrimoines & Politiques Culturelles Territ', 'Valorisation des Patrimoines & Politiques Culturelles Territ', 0);
INSERT INTO `t_discipline` VALUES (29, '', 'Diplôme d''ingénieur de l''ENSGTI', 'Diplôme d''ingénieur de l''ENSGTI', 0);
INSERT INTO `t_discipline` VALUES (28, '', 'Licence Mathématiques', 'Licence Mathématiques', 0);
INSERT INTO `t_discipline` VALUES (27, '', 'Diplôme d''ingénieur de l''ENSGTI spécialié Energétique', 'Diplôme d''ingénieur de l''ENSGTI spécialié Energétique', 0);
INSERT INTO `t_discipline` VALUES (26, '', 'Doctorat Sciences de l''univers', 'Doctorat Sciences de l''univers', 0);
INSERT INTO `t_discipline` VALUES (25, '', 'Master génie pétrolier', 'Master génie pétrolier', 0);
INSERT INTO `t_discipline` VALUES (24, '', 'Licence Langue et Culture Régional', 'Licence Langue et Culture Régional', 0);
INSERT INTO `t_discipline` VALUES (23, '', 'Licence Mathématiques Appliquées et Sciences Sociales', 'Licence Mathématiques Appliquées et Sciences Sociales', 0);
INSERT INTO `t_discipline` VALUES (22, '', 'Diplôme d''ingénieur ENSGTI spécialité génie des procédés', 'Diplôme d''ingénieur ENSGTI spécialité génie des procédés', 0);
INSERT INTO `t_discipline` VALUES (21, '', 'Licence professionnelles biotechnologies', 'Licence professionnelles biotechnologies', 0);
INSERT INTO `t_discipline` VALUES (20, '', 'Licence STAPS', 'Licence STAPS', 0);
INSERT INTO `t_discipline` VALUES (19, '', 'Diplôme d''ingénieur de l''ISA BTP', 'Diplôme d''ingénieur de l''ISA BTP', 0);
INSERT INTO `t_discipline` VALUES (17, '', 'Droit public & adm publiq', 'Droit public & adm publiq', 0);
INSERT INTO `t_discipline` VALUES (18, '', 'Master Méthodes Analytiques pour l''Environnement /matériaux', 'Master Méthodes Analytiques pour l''Environnement /matériaux', 0);
INSERT INTO `t_discipline` VALUES (16, '', 'Entrain., prépa phys & mentale & optimisa. de la performance', 'Entrain., prépa phys & mentale & optimisa. de la performance', 0);
INSERT INTO `t_discipline` VALUES (15, '', 'DUT Réseaux et Télécommunications', 'DUT Réseaux et Télécommunications', 0);
INSERT INTO `t_discipline` VALUES (13, '', 'Licence Biologie', 'Licence Biologie', 0);
INSERT INTO `t_discipline` VALUES (14, '', 'Licence Lettres', 'Licence Lettres', 0);
INSERT INTO `t_discipline` VALUES (11, '', 'Licence LEA', 'Licence LEA', 0);
INSERT INTO `t_discipline` VALUES (12, '', 'Licence Informatique', 'Licence Informatique', 0);
INSERT INTO `t_discipline` VALUES (10, '', 'Master Ingénierie et Sciences des Matériaux', 'Master Ingénierie et Sciences des Matériaux', 0);
INSERT INTO `t_discipline` VALUES (9, '', 'Chargé d''études économiques', 'Chargé d''études économiques', 0);
INSERT INTO `t_discipline` VALUES (8, '', 'Comptabilité, Contrôle, Audit', 'Comptabilité, Contrôle, Audit', 0);
INSERT INTO `t_discipline` VALUES (7, '', 'DUT Génie Thermique et Énergie', 'DUT Génie Thermique et Énergie', 0);
INSERT INTO `t_discipline` VALUES (6, '', 'DUT Science et Génie des Matériaux', 'DUT Science et Génie des Matériaux', 0);
INSERT INTO `t_discipline` VALUES (5, '', 'Licence Anglais', 'Licence Anglais', 0);
INSERT INTO `t_discipline` VALUES (2, '', '', '', 0);
INSERT INTO `t_discipline` VALUES (3, '', 'Licence Sciences Physiques et Chimiques', 'Licence Sciences Physiques et Chimiques', 0);
INSERT INTO `t_discipline` VALUES (106, '', 'Gestion de la production industrielle', 'Gestion de la production industrielle', 0);
INSERT INTO `t_discipline` VALUES (105, '', 'Master Mathématiques et leurs applic. - Maths Modèl. Simul.', 'Master Mathématiques et leurs applic. - Maths Modèl. Simul.', 0);
INSERT INTO `t_discipline` VALUES (104, '', 'LP Assurance, Banque, Finance', 'LP Assurance, Banque, Finance', 0);
INSERT INTO `t_discipline` VALUES (103, '', 'Réseaux Télécommunications Intégration Systèmes Voix Données', 'Réseaux Télécommunications Intégration Systèmes Voix Données', 0);
INSERT INTO `t_discipline` VALUES (102, '', 'LP Protection de l'' Environnement', 'LP Protection de l'' Environnement', 0);
INSERT INTO `t_discipline` VALUES (101, '', 'Doct Sciences de gestion', 'Doct Sciences de gestion', 0);
INSERT INTO `t_discipline` VALUES (100, '', 'Licence professionnelle Énergie et Génie climatique', 'Licence professionnelle Énergie et Génie climatique', 0);
INSERT INTO `t_discipline` VALUES (99, '', 'Ecologie Hist  & Comparée', 'Ecologie Hist  & Comparée', 0);
INSERT INTO `t_discipline` VALUES (98, '', 'Master Sociétés, Aménagement, Territoires', 'Master Sociétés, Aménagement, Territoires', 0);
INSERT INTO `t_discipline` VALUES (97, '', 'Master Droit Privé', 'Master Droit Privé', 0);
INSERT INTO `t_discipline` VALUES (96, '', 'Doctorat Histoire art', 'Doctorat Histoire art', 0);
INSERT INTO `t_discipline` VALUES (95, '', 'Licence Administration Economique et Sociale', 'Licence Administration Economique et Sociale', 0);
INSERT INTO `t_discipline` VALUES (107, '', 'Préparation à l'' agrégation d''anglais', 'Préparation à l'' agrégation d''anglais', 0);
INSERT INTO `t_discipline` VALUES (108, '', 'Traduction et Documentation  Scientifique et Technique', 'Traduction et Documentation  Scientifique et Technique', 0);
INSERT INTO `t_discipline` VALUES (109, '', 'Année Préparatoire à l''Insertion en Licence Sc. Math-Info', 'Année Préparatoire à l''Insertion en Licence Sc. Math-Info', 0);
INSERT INTO `t_discipline` VALUES (110, '', 'Année Préparatoire à l''Insertion en Lic. Sanitaire et Social', 'Année Préparatoire à l''Insertion en Lic. Sanitaire et Social', 0);
INSERT INTO `t_discipline` VALUES (111, '', 'Métiers de l''enseignement en mathématiques', 'Métiers de l''enseignement en mathématiques', 0);
INSERT INTO `t_discipline` VALUES (112, '', 'DU ADM TERRITORIALE', 'DU ADM TERRITORIALE', 0);
INSERT INTO `t_discipline` VALUES (113, '', 'DUT SID', 'DUT SID', 0);
INSERT INTO `t_discipline` VALUES (114, '', 'LP Systémes Informatiques et Logiciels', 'LP Systémes Informatiques et Logiciels', 0);
INSERT INTO `t_discipline` VALUES (115, '', 'Coopération transfrontalière et interrégionale', 'Coopération transfrontalière et interrégionale', 0);
INSERT INTO `t_discipline` VALUES (116, '', 'Préparation à l'' agrégation d''espagnol', 'Préparation à l'' agrégation d''espagnol', 0);
INSERT INTO `t_discipline` VALUES (117, '', 'Sécurité des biens et des personnes', 'Sécurité des biens et des personnes', 0);
INSERT INTO `t_discipline` VALUES (118, '', 'DELF/DALF', 'DELF/DALF', 0);
INSERT INTO `t_discipline` VALUES (119, '', 'DU Responsable en logistique et transports', 'DU Responsable en logistique et transports', 0);
INSERT INTO `t_discipline` VALUES (120, '', 'Doctorat Mécanique des fluides', 'Doctorat Mécanique des fluides', 0);
INSERT INTO `t_discipline` VALUES (121, '', 'Doct Physio et biologie des organismes-populations-interacti', 'Doct Physio et biologie des organismes-populations-interacti', 0);
INSERT INTO `t_discipline` VALUES (122, '', 'Master Ingenierie de projet, politiques locales et Tic', 'Master Ingenierie de projet, politiques locales et Tic', 0);
INSERT INTO `t_discipline` VALUES (123, '', 'Réseaux Télécommunications, Administration Sécurité Réseaux', 'Réseaux Télécommunications, Administration Sécurité Réseaux', 0);
INSERT INTO `t_discipline` VALUES (124, '', 'Doctorat STAPS', 'Doctorat STAPS', 0);
INSERT INTO `t_discipline` VALUES (125, '', 'Préparation au concurs d\\''entrée de l\\''ENM', 'Préparation au concurs d\\''entrée de l\\''ENM', 0);
INSERT INTO `t_discipline` VALUES (126, '', 'Année Préparatoire à l\\''Insertion en Licence Sc. de la Terre', 'Année Préparatoire à l\\''Insertion en Licence Sc. de la Terre', 0);
INSERT INTO `t_discipline` VALUES (127, '', 'Année préparatoire à l\\''insertion en licence', 'Année préparatoire à l\\''insertion en licence', 0);
INSERT INTO `t_discipline` VALUES (128, '', 'Licence Pro GRH', 'Licence Pro GRH', 0);
INSERT INTO `t_discipline` VALUES (129, '', 'Doctorat Langues et littératures françaises', 'Doctorat Langues et littératures françaises', 0);
INSERT INTO `t_discipline` VALUES (130, '', 'Doctorat d''Ethnologie', 'Doctorat d''Ethnologie', 0);
INSERT INTO `t_discipline` VALUES (131, '', 'Doct Génie Electrique', 'Doct Génie Electrique', 0);
INSERT INTO `t_discipline` VALUES (132, '', 'Diplôme d''Accès aux Études Universitaires option A', 'Diplôme d''Accès aux Études Universitaires option A', 0);
INSERT INTO `t_discipline` VALUES (133, '', 'DU Techniques multimédias', 'DU Techniques multimédias', 0);
INSERT INTO `t_discipline` VALUES (134, '', 'DU  Préparation au  concours Greffier en chef & Greffier', 'DU  Préparation au  concours Greffier en chef & Greffier', 0);
INSERT INTO `t_discipline` VALUES (135, '', 'Capacité en droit', 'Capacité en droit', 0);
INSERT INTO `t_discipline` VALUES (136, '', 'C2i - niveau 1', 'C2i - niveau 1', 0);
INSERT INTO `t_discipline` VALUES (137, '', 'DU Sciences de l''inadaptation et de la délinquance juvénile', 'DU Sciences de l''inadaptation et de la délinquance juvénile', 0);
INSERT INTO `t_discipline` VALUES (138, '', 'Doctorat Histoire', 'Doctorat Histoire', 0);
INSERT INTO `t_discipline` VALUES (139, '', 'Année Préparatoire à l''Insertion en Licence Sc.  MASS', 'Année Préparatoire à l''Insertion en Licence Sc.  MASS', 0);
INSERT INTO `t_discipline` VALUES (140, '', 'DU Technologies de l''Information et de la Communication', 'DU Technologies de l''Information et de la Communication', 0);
INSERT INTO `t_discipline` VALUES (141, '', 'CAPES HISTOIRE GEOGRAPHIE', 'CAPES HISTOIRE GEOGRAPHIE', 0);
INSERT INTO `t_discipline` VALUES (142, '', 'CAPES ANGLAIS', 'CAPES ANGLAIS', 0);
INSERT INTO `t_discipline` VALUES (143, '', 'Master Langue et Cult Rég', 'Master Langue et Cult Rég', 0);
INSERT INTO `t_discipline` VALUES (144, '', 'Préparation à l'' agrégation de lettres', 'Préparation à l'' agrégation de lettres', 0);
INSERT INTO `t_discipline` VALUES (145, '', 'DU METIERS DE L'' ADMINISTRATION GENERALE TERRITORIALE', 'DU METIERS DE L'' ADMINISTRATION GENERALE TERRITORIALE', 0);

-- --------------------------------------------------------

-- 
-- Structure de la table `t_dometu`
-- 

DROP TABLE IF EXISTS `t_dometu`;
CREATE TABLE `t_dometu` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_dometu`
-- 

INSERT INTO `t_dometu` VALUES (2, '71', 'PLURI SANTE', 'PLURI SANTE', 2);
INSERT INTO `t_dometu` VALUES (3, '80', 'VETERINAIRE', 'VETERINAIRE', 3);
INSERT INTO `t_dometu` VALUES (4, '1', 'MATHEMATIQUES', 'MATHEMATIQUES', 4);
INSERT INTO `t_dometu` VALUES (5, '2', 'PHYSIQUE', 'PHYSIQUE', 5);
INSERT INTO `t_dometu` VALUES (6, '3', 'CHIMIE', 'CHIMIE', 6);
INSERT INTO `t_dometu` VALUES (7, '4', 'MATHEMATIQUES APPLIQUEES ET SCIENCES SOCIALES (M.A.S.S.)', 'MATHEMATIQUES APPLIQUEES ET SCIENCES SOCIALES (M.A.S.S.)', 7);
INSERT INTO `t_dometu` VALUES (8, '5', 'SCIENCES DE L''UNIVERS', 'SCIENCES DE L''UNIVERS', 8);
INSERT INTO `t_dometu` VALUES (9, '6', 'SCIENCES DE LA VIE', 'SCIENCES DE LA VIE', 9);
INSERT INTO `t_dometu` VALUES (10, '7', 'MEDECINE', 'MEDECINE', 10);
INSERT INTO `t_dometu` VALUES (11, '8', 'ODONTOLOGIE', 'ODONTOLOGIE', 11);
INSERT INTO `t_dometu` VALUES (12, '9', 'PHARMACIE', 'PHARMACIE', 12);
INSERT INTO `t_dometu` VALUES (13, '10', 'S.T.A.P.S.', 'S.T.A.P.S.', 13);
INSERT INTO `t_dometu` VALUES (14, '11', 'MECANIQUE, GENIE MECANIQUE', 'MECANIQUE, GENIE MECANIQUE', 14);
INSERT INTO `t_dometu` VALUES (15, '12', 'GENIE CIVIL', 'GENIE CIVIL', 15);
INSERT INTO `t_dometu` VALUES (16, '13', 'GENIE DES PROCEDES', 'GENIE DES PROCEDES', 16);
INSERT INTO `t_dometu` VALUES (17, '14', 'INFORMATIQUE', 'INFORMATIQUE', 17);
INSERT INTO `t_dometu` VALUES (18, '15', 'ELECTRONIQUE, GENIE ELECTRIQUE', 'ELECTRONIQUE, GENIE ELECTRIQUE', 18);
INSERT INTO `t_dometu` VALUES (19, '16', 'SCIENCES ET TECHNOLOGIE INDUSTRIELLES', 'SCIENCES ET TECHNOLOGIE INDUSTRIELLES', 19);
INSERT INTO `t_dometu` VALUES (20, '17', 'SCIENCES DU LANGAGE - LINGUISTIQUE', 'SCIENCES DU LANGAGE - LINGUISTIQUE', 20);
INSERT INTO `t_dometu` VALUES (21, '18', 'LANGUES ET LITTERATURES ANCIENNES', 'LANGUES ET LITTERATURES ANCIENNES', 21);
INSERT INTO `t_dometu` VALUES (22, '19', 'LANGUES ET LITTERATURES FRANCAISES', 'LANGUES ET LITTERATURES FRANCAISES', 22);
INSERT INTO `t_dometu` VALUES (1, '', 'Autres', 'Autres', 1);

-- --------------------------------------------------------

-- 
-- Structure de la table `t_indic`
-- 

DROP TABLE IF EXISTS `t_indic`;
CREATE TABLE `t_indic` (
  `ID` int(11) NOT NULL default '0',
  `OWNER` int(11) NOT NULL default '0',
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL default '',
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `TYPE` varchar(100) collate utf8_unicode_ci NOT NULL,
  `UNITE` varchar(100) collate utf8_unicode_ci NOT NULL default '',
  `DEFAULT_OUTPUT` smallint(6) NOT NULL default '0',
  `UPDATE_FREQ` smallint(6) NOT NULL default '0',
  `TRIG` varchar(100) collate utf8_unicode_ci NOT NULL,
  `SQL_STRING` text collate utf8_unicode_ci NOT NULL,
  `COMPLETION_VALUES` varchar(10) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_indic`
-- 

INSERT INTO `t_indic` VALUES (1, 1, 'Taux de connexion brut par profil', 'Evaluer le taux d’usage d’un service suivant les différents paramètres du profil de connexion', '0', '3', 2, 1, '', 'SELECT t_agregat.id_affil as pivotval, t_affiliation.lib as pivotlib, concat(lpad(t_ut.annee,4,\\''0000\\''),\\''/\\'',lpad(t_ut.mois,2,\\''00\\''),\\''/\\'',lpad(t_ut.jour,2,\\''00\\'')) as lib,\r\n ((sum(nb_uid)/r_denombrement.nbr_occ)*100) as val, sum(nb_uid) as nbuid, \r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax  \r\nFROM `t_agregat`, t_ut,t_affiliation, r_denombrement \r\nWHERE \r\n((t_agregat.id_struct=\\''%IDSTRUCT%\\'' and r_denombrement.id_struct = \\''%IDSTRUCT%\\'') or \r\n(t_agregat.id_struct_pere=\\''%IDSTRUCTPERE%\\'' and r_denombrement.id_struct_pere = \\''%IDSTRUCTPERE%\\'' )) \r\nand t_ut.id=id_temps\r\nand t_affiliation.id = t_agregat.id_affil\r\nand r_denombrement.ref_dim=1 and r_denombrement.id_dim=t_agregat.id_affil\r\n %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by  pivotlib,t_ut.annee, t_ut.mois,t_ut.jour\r\norder by pivotlib,t_ut.annee, t_ut.mois,t_ut.jour', '0');
INSERT INTO `t_indic` VALUES (2, 1, 'Taux de pénétration du service', 'Evaluer le niveau d’appropriation d’un service par ses utilisateurs potentiel', '0', '3', 4, 2, '', 'SELECT t_agregat.id_affil as pivotval, t_affiliation.lib as pivotlib, concat(lpad(t_ut.annee,4,\\''0000\\''),\\''-\\'',lpad(t_ut.mois,2,\\''00\\'')) as lib,\r\n((sum(t_agregat.nb_uid))/(r_denombrement.nbr_occ)*100) as val,\r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax  \r\nFROM `t_agregat`, t_ut, t_affiliation, r_denombrement  \r\nWHERE \r\nt_agregat.id_struct=\\''%IDSTRUCT%\\'' and r_denombrement.id_struct = \\''%IDSTRUCT%\\''\r\nand t_ut.id=id_temps\r\nand t_affiliation.id = t_agregat.id_affil \r\nand r_denombrement.ref_dim=1 and r_denombrement.id_dim=t_agregat.id_affil\r\n %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by  t_agregat.id_affil, lib\r\norder by t_affiliation.lib, val desc', '0');
INSERT INTO `t_indic` VALUES (3, 1, 'Durée moyenne de session', 'évaluer la durée moyenne des sessions des usagers de l’ENT', '2', '1', 4, 2, '', 'SELECT t_agregat.id_affil as pivotval, t_affiliation.lib as pivotlib, t_srv_intra.lib as lib,\r\n ((sum(duree)/sum(nb_cnx))/60) as val,\r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax,\r\n1 as pivotid   \r\nFROM `t_agregat`, t_ut, t_srv_intra,t_affiliation \r\nWHERE t_ut.id=id_temps\r\nand t_srv_intra.id = t_agregat.id_srv_intra\r\nand t_affiliation.id = t_agregat.id_affil\r\n %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by  t_agregat.id_affil,t_agregat.id_srv_intra\r\norder by t_affiliation.lib, val desc', '0');
INSERT INTO `t_indic` VALUES (4, 1, 'Nombre de services opérationnels', 'L’établissement déploie les services afin d’assurer une couverture large des besoins des usagers', '0', '2', 4, 4, '', 'SELECT \\"Tous\\" as pivotlib, 0 as pivotval, t_cat_srv.lib as lib , sum(t_agregat.nb_cnx)as val,\r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax,\r\n0 as pivotid  \r\nfrom t_cat_srv left join t_agregat on t_cat_srv.id =t_agregat.id_cat_srv  where 1  \r\n %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by t_cat_srv.id   ORDER BY lib\r\n', '0');
INSERT INTO `t_indic` VALUES (5, 1, 'Fréquence moyenne d’usage du service', 'Objectifs liés : L’établissement s’assure de la fidélisation des usagers en développant la fréquence d’usage', '0', '3', 4, 4, '', 'SELECT t_agregat.id_affil as pivotval, t_affiliation.lib as pivotlib, t_cat_srv.lib as lib, sum(nb_cnx), totcnx, (sum(nb_cnx)/totcnx*100) as val FROM `t_agregat`, (select sum(nb_cnx) as totcnx from t_agregat) as t_totcnx, t_affiliation, t_cat_srv\r\nwhere 1\r\nand t_agregat.id_affil = t_affiliation.id \r\nand t_agregat.id_cat_srv = t_cat_srv.id \r\n%DIMSEL% %DATEDEB% %DATEFIN% \r\nGROUP BY ID_AFFIL, ID_CAT_SRV ORDER BY ID_AFFIL \r\n', '0');
INSERT INTO `t_indic` VALUES (8, 1, 'Liste des URLS détectées', '', '0', '2', 1, 1, '', 'select target_url as lib, count(stamp_start) as val from t_cnxlog\r\ngroup by lib\r\norder by val desc, lib asc', '0');
INSERT INTO `t_indic` VALUES (6, 1, 'Utilisation des Catégories de Service', 'dénombrement des connexions par catégorie de service', '0', '2', 4, 3, '', 'select t_ut.annee as pivotlib, t_ut.annee as pivotval, t_cat_srv.lib as lib, sum(t_agregat.nb_cnx) as val,\r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax\r\n from t_agregat, t_cat_srv,t_ut \r\nwhere 1\r\nand t_agregat.id_temps = t_ut.id\r\nand t_agregat.id_cat_srv = t_cat_srv.id\r\n %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by t_ut.annee,t_agregat.id_cat_srv \r\norder by t_ut.annee,val desc', '0');
INSERT INTO `t_indic` VALUES (7, 1, 'Liste des applications détectées', '', '0', '2', 1, 1, '', 'SELECT t_application.lib as lib, count(t_cnx.id_appli) as val from t_application,t_cnx\r\nwhere t_cnx.id_appli = t_application.id\r\ngroup by t_cnx.id_appli\r\norder by val desc', '0');
INSERT INTO `t_indic` VALUES (10, 1, 'Utilisation des Services intra-etablissement', '', '0', '2', 4, 2, '', 'SELECT t_agregat.id_affil as pivotval, t_affiliation.lib as pivotlib, t_srv_intra.lib as lib,\r\n (sum(t_agregat.volume)/1024000) as val,\r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax,\r\n1 as pivotid  \r\nFROM `t_agregat`, t_ut, t_srv_intra,t_affiliation \r\nWHERE t_ut.id=id_temps\r\nand t_srv_intra.id = t_agregat.id_srv_intra\r\nand t_affiliation.id = t_agregat.id_affil\r\n %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by  t_agregat.id_affil,t_agregat.id_srv_intra\r\norder by t_affiliation.lib\r\n', '0');
INSERT INTO `t_indic` VALUES (11, 1, 'Couverture applicative', '', '1', '3', 5, 3, '', 'SELECT t_cat_srv.lib as lib , count(t_srv_intra.id ) as val \r\nfrom t_cat_srv left join t_srv_intra on t_cat_srv.id =t_srv_intra.id_regr  where 1 \r\n group by t_cat_srv.id   ORDER BY lib\r\n', '0');
INSERT INTO `t_indic` VALUES (12, 1, 'Utilisateurs quotidiens', 'Nombre d\\''utilisateurs quotidiens', '1', '4', 4, 1, '', 'select t_agregat.id_affil as pilotval, t_affiliation.lib as pivotlib, concat(t_ut.jour,\\"/\\",t_ut.mois,\\"/\\",t_ut.annee) as lib, sum(t_agregat.nb_uid) as val from t_agregat, t_ut, t_affiliation \r\nwhere 1 and \r\n((t_agregat.id_struct=\\''%IDSTRUCT%\\'') or \r\n(t_agregat.id_struct_pere=\\''%IDSTRUCTPERE%\\'' )) \r\nand t_agregat.id_temps = t_ut.id \r\nand t_agregat.id_affil = t_affiliation.id \r\n %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by t_agregat.id_affil, t_ut.annee, t_ut.mois, t_ut.jour\r\norder by t_agregat.id_affil, t_ut.annee, t_ut.mois, t_ut.jour', '0');
INSERT INTO `t_indic` VALUES (13, 1, 'Analyse par type de navigateur', '', '1', '4', 3, 1, '', 'select t_agregat.user_agent  as lib, sum(t_agregat.nb_uid) as val,\r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax\r\n from t_agregat, t_ut \r\nwhere 1\r\n %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by lib \r\norder by val desc', '0');
INSERT INTO `t_indic` VALUES (14, 1, 'analyse par flière', '', '2', '4', 1, 1, '', 'SELECT t_discipline.id as pivotval, t_discipline.lib as pivotlib, concat(lpad(t_ut.annee,4,\\''0000\\''),\\''-\\'',lpad(t_ut.mois,2,\\''00\\'')) as lib,\r\nsum(t_agregat.nb_uid) as val,\r\nmin(t_agregat.id_temps) as datemin, max(t_agregat.id_temps) as datemax  \r\nFROM `t_agregat`, t_ut, t_discipline \r\nWHERE 1 \r\nand t_ut.id=id_temps \r\nand t_discipline.id = t_agregat.id_dcpl \r\n %DIMSEL% %DATEDEB% %DATEFIN%\r\ngroup by  t_agregat.id_dcpl, lib \r\norder by val desc, t_discipline.lib', '0');
