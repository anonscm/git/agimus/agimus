/* PROFIL */

truncate t_affiliation;
insert into t_affiliation values("1","","Autres","Autres","1");
insert into t_affiliation values("2","","Etudiant","Etudiant","2");
insert into t_affiliation values("3","","Enseignant","Enseignant","3");
insert into t_affiliation values("4","","IATOS","IATOS","4");
insert into t_affiliation values("5","","Chercheurs","Chercheurs","5");

truncate t_affil_intra;
insert into t_affil_intra values("1","","Autres","Autres","","1","IUT0331");
insert into t_affil_intra values("2","STU","Student","Student","#STU#","2","IUT0331");
insert into t_affil_intra values("3","F","faculty","faculty","#F#","3","IUT0331");
insert into t_affil_intra values("4","STA","Staff","Staff","#STA#","4","IUT0331");
insert into t_affil_intra values("5","E","Employee","Employee","#E#","1","IUT0331");
insert into t_affil_intra values("6","M","member","member","#M#","1","IUT0331");
insert into t_affil_intra values("7","A","affiliate","affiliate","#A#","1","IUT0331");
insert into t_affil_intra values("8","AL","alum","alum","#AL#","1","IUT0331");
insert into t_affil_intra values("9","?","library-walk-in","library-walk-in","#?#","1","IUT0331");
insert into t_affil_intra values("10","R","researcher","researcher","#R#","5","IUT0331");
insert into t_affil_intra values("11","RD","retired","retired","#RD#","1","IUT0331");
insert into t_affil_intra values("12","EUS","emeritus","emeritus","#EUS#","3","IUT0331");
insert into t_affil_intra values("13","T","teacher","teacher","#T#","3","IUT0331");
insert into t_affil_intra values("14","RR","registered-reader","registered-reader","#RR#","1","IUT0331");

/* Diplome */

truncate t_diplome;
insert into t_diplome values('2','','Baccalauréat','Baccalauréat','2','0');
insert into t_diplome values('3','','Licence 1','Licence 1','3','1');
insert into t_diplome values('4','','Licence 2','Licence 2','4','2');
insert into t_diplome values('5','','Licence 3','Licence 3','5','3');
insert into t_diplome values('6','','Master 1','Master 1','6','4');
insert into t_diplome values('7','','Master 2','Master 2','7','5');
insert into t_diplome values('8','','Doctorat','Doctorat','8','6');
insert into t_diplome values('9','','BTS','BTS','9','2');
insert into t_diplome values('10','','DUT','DUT','10','2');
insert into t_diplome values('1','','Autre','Autre','1','0');

truncate t_diplome_intra;
INSERT INTO t_diplome_intra (ID, EXTN_REF,LIB,DSC,CHAINE_TYPE,ID_REGR,ID_STRUCT) SELECT ID, EXTN_REF,LIB,DSC, concat("#",LIB,"#") as CHAINE_TYPE, ID_REGR, "IUT0331" FROM t_diplome;



/* Domaine */

truncate t_dometu;
insert into t_dometu values('2','71','PLURI SANTE','PLURI SANTE','2');
insert into t_dometu values('3','80','VETERINAIRE','VETERINAIRE','3');
insert into t_dometu values('4','1','MATHEMATIQUES','MATHEMATIQUES','4');
insert into t_dometu values('5','2','PHYSIQUE','PHYSIQUE','5');
insert into t_dometu values('6','3','CHIMIE','CHIMIE','6');
insert into t_dometu values('7','4','MATHEMATIQUES APPLIQUEES ET SCIENCES SOCIALES (M.A.S.S.)','MATHEMATIQUES APPLIQUEES ET SCIENCES SOCIALES (M.A.S.S.)','7');
insert into t_dometu values('8','5',"SCIENCES DE L'UNIVERS","SCIENCES DE L'UNIVERS",'8');
insert into t_dometu values('9','6','SCIENCES DE LA VIE','SCIENCES DE LA VIE','9');
insert into t_dometu values('10','7','MEDECINE','MEDECINE','10');
insert into t_dometu values('11','8','ODONTOLOGIE','ODONTOLOGIE','11');
insert into t_dometu values('12','9','PHARMACIE','PHARMACIE','12');
insert into t_dometu values('13','10','S.T.A.P.S.','S.T.A.P.S.','13');
insert into t_dometu values('14','11','MECANIQUE, GENIE MECANIQUE','MECANIQUE, GENIE MECANIQUE','14');
insert into t_dometu values('15','12','GENIE CIVIL','GENIE CIVIL','15');
insert into t_dometu values('16','13','GENIE DES PROCEDES','GENIE DES PROCEDES','16');
insert into t_dometu values('17','14','INFORMATIQUE','INFORMATIQUE','17');
insert into t_dometu values('18','15','ELECTRONIQUE, GENIE ELECTRIQUE','ELECTRONIQUE, GENIE ELECTRIQUE','18');
insert into t_dometu values('19','16','SCIENCES ET TECHNOLOGIE INDUSTRIELLES','SCIENCES ET TECHNOLOGIE INDUSTRIELLES','19');
insert into t_dometu values('20','17','SCIENCES DU LANGAGE - LINGUISTIQUE','SCIENCES DU LANGAGE - LINGUISTIQUE','20');
insert into t_dometu values('21','18','LANGUES ET LITTERATURES ANCIENNES','LANGUES ET LITTERATURES ANCIENNES','21');
insert into t_dometu values('22','19','LANGUES ET LITTERATURES FRANCAISES','LANGUES ET LITTERATURES FRANCAISES','22');
insert into t_dometu values('1','','Autres','Autres','1');

truncate t_dometu_intra;
INSERT INTO t_dometu_intra (ID, EXTN_REF,LIB,DSC,CHAINE_TYPE,ID_REGR,ID_STRUCT) SELECT ID, EXTN_REF,LIB,DSC, concat("#",EXTN_REF,"#") as CHAINE_TYPE, ID_REGR, "IUT0331" FROM t_dometu;

/* composantes */

truncate t_cat_struct;
insert into t_cat_struct values('2','IUT','INSTITUT UNIVERSITAIRE DE TECHNOLOGIE');
insert into t_cat_struct values('3','UFR','UNITE DE FORMATION ET DE RECHERCHE ET AS');
insert into t_cat_struct values('4','IUP','INSTITUT UNIVERSITAIRE PROFESSIONNEL');
insert into t_cat_struct values('5','INGI','ECOLE INGENIEUR INTERNE A L"UNIVERSITE');
insert into t_cat_struct values('6','INGR','ECOLE INGENIEUR RATTACHEE');
insert into t_cat_struct values('7','IEPC','INST. ETUDES POLITIQUES COMPOSANTE');
insert into t_cat_struct values('8','NFI','NOUVELLE FORMATION INGENIEUR');
insert into t_cat_struct values('9','IAE','INSTITUT D"ADMINISTRATION ET D"ENTREPRIS');
insert into t_cat_struct values('10','IPAG','INSTITUT DE PREPA. ADMINISTRATION GENE.');
insert into t_cat_struct values('11','IREM','INSTITUT DE RECH. ET ETUDES MATH.');
insert into t_cat_struct values('12','LABO','LABORATOIRE');
insert into t_cat_struct values('13','SERV','SERVICE COMMUN');
insert into t_cat_struct values('14','AUTR','AUTRE TYPE COMPOSANTE');
insert into t_cat_struct values('15','INGC','ECOLE INGENIEUR CONVENTIONNE');
insert into t_cat_struct values('16','IEPR','IEP RATTACHE - ARTICLE 43');
insert into t_cat_struct values('1','Autres','Autres');


/* Filieres */

truncate t_discipline;
/* cf .filieres.csv */

truncate t_dcpl_intra;
INSERT INTO t_dcpl_intra (ID, EXTN_REF,LIB,DSC,CHAINE_TYPE,ID_REGR,ID_STRUCT) SELECT ID, EXTN_REF,LIB,DSC, concat("#",EXTN_REF,"#") as CHAINE_TYPE, ID_REGR, "IUT0331" FROM t_discipline;

