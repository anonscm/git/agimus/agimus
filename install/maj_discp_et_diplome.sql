-- phpMyAdmin SQL Dump
-- version 2.8.2.4
-- http://www.phpmyadmin.net
-- 
-- Serveur: cribd.univ-pau.fr
-- Généré le : Jeudi 23 Septembre 2010 à 10:25
-- Version du serveur: 5.0.51
-- Version de PHP: 4.4.1
-- 
-- Base de données: `agimus_maquette`
-- 

-- --------------------------------------------------------

-- 
-- Structure de la table `t_dcpl_intra`
-- 

DROP TABLE IF EXISTS `t_dcpl_intra`;
CREATE TABLE `t_dcpl_intra` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `CHAINE_TYPE` varchar(255) collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  `ID_STRUCT` smallint(6) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_dcpl_intra`
-- 

INSERT INTO `t_dcpl_intra` VALUES (1, '', 'Autres filieres', 'Autres filieres', '#^0$#', 1, 0);
INSERT INTO `t_dcpl_intra` VALUES (2, '', 'Licence Sciences Physiques et Chimiques', 'Licence Sciences Physiques et Chimiques', '#^Licence Sciences Physiques et Chimiques$#', 3, 0);
INSERT INTO `t_dcpl_intra` VALUES (4, '', 'Licence Anglais', 'Licence Anglais', '#^Licence Anglais$#', 5, 0);
INSERT INTO `t_dcpl_intra` VALUES (5, '', 'DUT Science et Génie des Matériaux', 'DUT Science et Génie des Matériaux', '#^DUT Science et Génie des Matériaux$#', 6, 0);
INSERT INTO `t_dcpl_intra` VALUES (6, '', 'DUT Génie Thermique et Énergie', 'DUT Génie Thermique et Énergie', '#^DUT Génie Thermique et Énergie$#', 7, 0);
INSERT INTO `t_dcpl_intra` VALUES (7, '', 'Comptabilité, Contrôle, Audit', 'Comptabilité, Contrôle, Audit', '#^Comptabilité, Contrôle, Audit$#', 8, 0);
INSERT INTO `t_dcpl_intra` VALUES (8, '', 'Chargé d''études économiques', 'Chargé d''études économiques', '#^Chargé d''études économiques$#', 9, 0);
INSERT INTO `t_dcpl_intra` VALUES (9, '', 'Master Ingénierie et Sciences des Matériaux', 'Master Ingénierie et Sciences des Matériaux', '#^Master Ingénierie et Sciences des Matériaux$#', 10, 0);
INSERT INTO `t_dcpl_intra` VALUES (10, '', 'Licence LEA', 'Licence LEA', '#^Licence LEA$#', 11, 0);
INSERT INTO `t_dcpl_intra` VALUES (11, '', 'Licence Informatique', 'Licence Informatique', '#^Licence Informatique$#', 12, 0);
INSERT INTO `t_dcpl_intra` VALUES (12, '', 'Licence Biologie', 'Licence Biologie', '#^Licence Biologie$#', 13, 0);
INSERT INTO `t_dcpl_intra` VALUES (13, '', 'Licence Lettres', 'Licence Lettres', '#^Licence Lettres$#', 14, 0);
INSERT INTO `t_dcpl_intra` VALUES (14, '', 'DUT Réseaux et Télécommunications', 'DUT Réseaux et Télécommunications', '#^DUT Réseaux et Télécommunications$#', 15, 0);
INSERT INTO `t_dcpl_intra` VALUES (15, '', 'Entrain., prépa phys & mentale & optimisa. de la performance', 'Entrain., prépa phys & mentale & optimisa. de la performance', '#^Entrain., prépa phys & mentale & optimisa. de la performance$#', 16, 0);
INSERT INTO `t_dcpl_intra` VALUES (16, '', 'Droit public & adm publiq', 'Droit public & adm publiq', '#^Droit public & adm publiq$#', 17, 0);
INSERT INTO `t_dcpl_intra` VALUES (17, '', 'Master Méthodes Analytiques pour l''Environnement /matériaux', 'Master Méthodes Analytiques pour l''Environnement /matériaux', '#^Master Méthodes Analytiques pour l''Environnement /matériaux$#', 18, 0);
INSERT INTO `t_dcpl_intra` VALUES (18, '', 'Diplôme d''ingénieur de l''ISA BTP', 'Diplôme d''ingénieur de l''ISA BTP', '#^Diplôme d''ingénieur de l''ISA BTP$#', 19, 0);
INSERT INTO `t_dcpl_intra` VALUES (19, '', 'Licence STAPS', 'Licence STAPS', '#^Licence STAPS$#', 20, 0);
INSERT INTO `t_dcpl_intra` VALUES (20, '', 'Licence professionnelles biotechnologies', 'Licence professionnelles biotechnologies', '#^Licence professionnelles biotechnologies$#', 21, 0);
INSERT INTO `t_dcpl_intra` VALUES (21, '', 'Diplôme d''ingénieur ENSGTI spécialité génie des procédés', 'Diplôme d''ingénieur ENSGTI spécialité génie des procédés', '#^Diplôme d''ingénieur ENSGTI spécialité génie des procédés$#', 22, 0);
INSERT INTO `t_dcpl_intra` VALUES (22, '', 'Licence Mathématiques Appliquées et Sciences Sociales', 'Licence Mathématiques Appliquées et Sciences Sociales', '#^Licence Mathématiques Appliquées et Sciences Sociales$#', 23, 0);
INSERT INTO `t_dcpl_intra` VALUES (23, '', 'Licence Langue et Culture Régional', 'Licence Langue et Culture Régional', '#^Licence Langue et Culture Régional$#', 24, 0);
INSERT INTO `t_dcpl_intra` VALUES (24, '', 'Master génie pétrolier', 'Master génie pétrolier', '#^Master génie pétrolier$#', 25, 0);
INSERT INTO `t_dcpl_intra` VALUES (25, '', 'Doctorat Sciences de l''univers', 'Doctorat Sciences de l''univers', '#^Doctorat Sciences de l''univers$#', 26, 0);
INSERT INTO `t_dcpl_intra` VALUES (26, '', 'Diplôme d''ingénieur de l''ENSGTI spécialié Energétique', 'Diplôme d''ingénieur de l''ENSGTI spécialié Energétique', '#^Diplôme d''ingénieur de l''ENSGTI spécialié Energétique$#', 27, 0);
INSERT INTO `t_dcpl_intra` VALUES (27, '', 'Licence Mathématiques', 'Licence Mathématiques', '#^Licence Mathématiques$#', 28, 0);
INSERT INTO `t_dcpl_intra` VALUES (28, '', 'Diplôme d''ingénieur de l''ENSGTI', 'Diplôme d''ingénieur de l''ENSGTI', '#^Diplôme d''ingénieur de l''ENSGTI$#', 29, 0);
INSERT INTO `t_dcpl_intra` VALUES (29, '', 'Valorisation des Patrimoines & Politiques Culturelles Territ', 'Valorisation des Patrimoines & Politiques Culturelles Territ', '#^Valorisation des Patrimoines & Politiques Culturelles Territ$#', 30, 0);
INSERT INTO `t_dcpl_intra` VALUES (30, '', 'Année Préparatoire à l''Insertion en Licence Sc. Phys-Chimie', 'Année Préparatoire à l''Insertion en Licence Sc. Phys-Chimie', '#^Année Préparatoire à l''Insertion en Licence Sc. Phys-Chimie$#', 31, 0);
INSERT INTO `t_dcpl_intra` VALUES (31, '', 'Histoire, Histoire de l'' Art, Archéologie, Anthropologie', 'Histoire, Histoire de l'' Art, Archéologie, Anthropologie', '#^Histoire, Histoire de l'' Art, Archéologie, Anthropologie$#', 32, 0);
INSERT INTO `t_dcpl_intra` VALUES (32, '', 'Licence Droit', 'Licence Droit', '#^Licence Droit$#', 33, 0);
INSERT INTO `t_dcpl_intra` VALUES (33, '', 'Master Poét & hist litté', 'Master Poét & hist litté', '#^Master Poét & hist litté$#', 34, 0);
INSERT INTO `t_dcpl_intra` VALUES (34, '', 'Doctorat de chimie', 'Doctorat de chimie', '#^Doctorat de chimie$#', 35, 0);
INSERT INTO `t_dcpl_intra` VALUES (35, '', 'Master Technologie de l''internet', 'Master Technologie de l''internet', '#^Master Technologie de l''internet$#', 36, 0);
INSERT INTO `t_dcpl_intra` VALUES (36, '', 'DU ERASMUS', 'DU ERASMUS', '#^DU ERASMUS$#', 37, 0);
INSERT INTO `t_dcpl_intra` VALUES (37, '', 'Master ingénierie des systémes industriels', 'Master ingénierie des systémes industriels', '#^Master ingénierie des systémes industriels$#', 38, 0);
INSERT INTO `t_dcpl_intra` VALUES (38, '', 'Licence Economie-Gestion', 'Licence Economie-Gestion', '#^Licence Economie-Gestion$#', 39, 0);
INSERT INTO `t_dcpl_intra` VALUES (39, '', 'Certificat de Sciences criminelles', 'Certificat de Sciences criminelles', '#^Certificat de Sciences criminelles$#', 40, 0);
INSERT INTO `t_dcpl_intra` VALUES (40, '', 'Licence Géographie', 'Licence Géographie', '#^Licence Géographie$#', 41, 0);
INSERT INTO `t_dcpl_intra` VALUES (41, '', 'Préparation à l''examen d''entrée au CRFPA', 'Préparation à l''examen d''entrée au CRFPA', '#^Préparation à l''examen d''entrée au CRFPA$#', 42, 0);
INSERT INTO `t_dcpl_intra` VALUES (42, '', 'Management et Systèmes d''Information', 'Management et Systèmes d''Information', '#^Management et Systèmes d''Information$#', 43, 0);
INSERT INTO `t_dcpl_intra` VALUES (43, '', 'Doctorat de Mathématiques', 'Doctorat de Mathématiques', '#^Doctorat de Mathématiques$#', 44, 0);
INSERT INTO `t_dcpl_intra` VALUES (44, '', 'Lic Sciences de la Terre', 'Lic Sciences de la Terre', '#^Lic Sciences de la Terre$#', 45, 0);
INSERT INTO `t_dcpl_intra` VALUES (45, '', 'Master Microbiologie et Biotechnologies', 'Master Microbiologie et Biotechnologies', '#^Master Microbiologie et Biotechnologies$#', 46, 0);
INSERT INTO `t_dcpl_intra` VALUES (46, '', 'Master Managment Public', 'Master Managment Public', '#^Master Managment Public$#', 47, 0);
INSERT INTO `t_dcpl_intra` VALUES (47, '', 'Master Enseignement en  Lettres & Langues', 'Master Enseignement en  Lettres & Langues', '#^Master Enseignement en  Lettres & Langues$#', 48, 0);
INSERT INTO `t_dcpl_intra` VALUES (48, '', 'Licence Histoire', 'Licence Histoire', '#^Licence Histoire$#', 49, 0);
INSERT INTO `t_dcpl_intra` VALUES (49, '', 'Licence Espagnol', 'Licence Espagnol', '#^Licence Espagnol$#', 50, 0);
INSERT INTO `t_dcpl_intra` VALUES (50, '', 'Doctorat Génie civil', 'Doctorat Génie civil', '#^Doctorat Génie civil$#', 51, 0);
INSERT INTO `t_dcpl_intra` VALUES (51, '', 'Economie de l''intégration européenne et internationale', 'Economie de l''intégration européenne et internationale', '#^Economie de l''intégration européenne et internationale$#', 52, 0);
INSERT INTO `t_dcpl_intra` VALUES (52, '', 'Métiers des langues', 'Métiers des langues', '#^Métiers des langues$#', 53, 0);
INSERT INTO `t_dcpl_intra` VALUES (53, '', 'Master Droit notarial', 'Master Droit notarial', '#^Master Droit notarial$#', 54, 0);
INSERT INTO `t_dcpl_intra` VALUES (54, '', 'Master Droit Public', 'Master Droit Public', '#^Master Droit Public$#', 55, 0);
INSERT INTO `t_dcpl_intra` VALUES (55, '', 'Licence Professionnelle Management des organisations SIDMQ', 'Licence Professionnelle Management des organisations SIDMQ', '#^Licence Professionnelle Management des organisations SIDMQ$#', 56, 0);
INSERT INTO `t_dcpl_intra` VALUES (56, '', 'Doctorat de Physique', 'Doctorat de Physique', '#^Doctorat de Physique$#', 57, 0);
INSERT INTO `t_dcpl_intra` VALUES (57, '', 'Aménagement Touristique', 'Aménagement Touristique', '#^Aménagement Touristique$#', 58, 0);
INSERT INTO `t_dcpl_intra` VALUES (58, '', 'Master Bioprotection, Valorisation non alimentaires des agro', 'Master Bioprotection, Valorisation non alimentaires des agro', '#^Master Bioprotection, Valorisation non alimentaires des agro$#', 59, 0);
INSERT INTO `t_dcpl_intra` VALUES (59, '', 'Métiers ens. Phys chimie', 'Métiers ens. Phys chimie', '#^Métiers ens. Phys chimie$#', 60, 0);
INSERT INTO `t_dcpl_intra` VALUES (60, '', 'DUT GIM', 'DUT GIM', '#^DUT GIM$#', 61, 0);
INSERT INTO `t_dcpl_intra` VALUES (61, '', 'Doctorat Energétique', 'Doctorat Energétique', '#^Doctorat Energétique$#', 62, 0);
INSERT INTO `t_dcpl_intra` VALUES (62, '', 'Langue Littérature et culture étrangère', 'Langue Littérature et culture étrangère', '#^Langue Littérature et culture étrangère$#', 63, 0);
INSERT INTO `t_dcpl_intra` VALUES (63, '', 'Master Logistique,act. opérationnelles et syst. d''info', 'Master Logistique,act. opérationnelles et syst. d''info', '#^Master Logistique,act. opérationnelles et syst. d''info$#', 64, 0);
INSERT INTO `t_dcpl_intra` VALUES (64, '', 'Master  Enseignement en  Histoire-Géographie', 'Master  Enseignement en  Histoire-Géographie', '#^Master  Enseignement en  Histoire-Géographie$#', 65, 0);
INSERT INTO `t_dcpl_intra` VALUES (65, '', 'Doctorat de Géographie', 'Doctorat de Géographie', '#^Doctorat de Géographie$#', 66, 0);
INSERT INTO `t_dcpl_intra` VALUES (66, '', 'DUT Génie Biologique - industries alimentaires & biologiques', 'DUT Génie Biologique - industries alimentaires & biologiques', '#^DUT Génie Biologique - industries alimentaires & biologiques$#', 67, 0);
INSERT INTO `t_dcpl_intra` VALUES (67, '', 'Doctorat Génie des procédés', 'Doctorat Génie des procédés', '#^Doctorat Génie des procédés$#', 68, 0);
INSERT INTO `t_dcpl_intra` VALUES (68, '', 'Management international', 'Management international', '#^Management international$#', 69, 0);
INSERT INTO `t_dcpl_intra` VALUES (69, '', 'Doct Sciences Economiques', 'Doct Sciences Economiques', '#^Doct Sciences Economiques$#', 70, 0);
INSERT INTO `t_dcpl_intra` VALUES (70, '', 'Master Environnement et Matériaux, Concepts fondamentaux PC', 'Master Environnement et Matériaux, Concepts fondamentaux PC', '#^Master Environnement et Matériaux, Concepts fondamentaux PC$#', 71, 0);
INSERT INTO `t_dcpl_intra` VALUES (71, '', 'Juriste européen', 'Juriste européen', '#^Juriste européen$#', 72, 0);
INSERT INTO `t_dcpl_intra` VALUES (72, '', 'DUT Informatique', 'DUT Informatique', '#^DUT Informatique$#', 73, 0);
INSERT INTO `t_dcpl_intra` VALUES (73, '', 'Diplôme Compta Gestion', 'Diplôme Compta Gestion', '#^Diplôme Compta Gestion$#', 74, 0);
INSERT INTO `t_dcpl_intra` VALUES (74, '', 'DUT Techniques de commercialisation', 'DUT Techniques de commercialisation', '#^DUT Techniques de commercialisation$#', 75, 0);
INSERT INTO `t_dcpl_intra` VALUES (75, '', 'Année Préparatoire à l''Insertion en Licence Sc. de la Vie', 'Année Préparatoire à l''Insertion en Licence Sc. de la Vie', '#^Année Préparatoire à l''Insertion en Licence Sc. de la Vie$#', 76, 0);
INSERT INTO `t_dcpl_intra` VALUES (76, '', 'Droit Pénal et Sc Crimin.', 'Droit Pénal et Sc Crimin.', '#^Droit Pénal et Sc Crimin.$#', 77, 0);
INSERT INTO `t_dcpl_intra` VALUES (77, '', 'Master Dynamique des Écosystèmes Aquatiques', 'Master Dynamique des Écosystèmes Aquatiques', '#^Master Dynamique des Écosystèmes Aquatiques$#', 78, 0);
INSERT INTO `t_dcpl_intra` VALUES (78, '', 'Master Enseignement  en Education Physique et Sportive', 'Master Enseignement  en Education Physique et Sportive', '#^Master Enseignement  en Education Physique et Sportive$#', 79, 0);
INSERT INTO `t_dcpl_intra` VALUES (79, '', 'Master Méthodes stochastiqu. & informatiqu. pour la décision', 'Master Méthodes stochastiqu. & informatiqu. pour la décision', '#^Master Méthodes stochastiqu. & informatiqu. pour la décision$#', 80, 0);
INSERT INTO `t_dcpl_intra` VALUES (80, '', 'Licence Histoire de l''Art et Archéologie', 'Licence Histoire de l''Art et Archéologie', '#^Licence Histoire de l''Art et Archéologie$#', 81, 0);
INSERT INTO `t_dcpl_intra` VALUES (81, '', 'Doctorat de Sciences juridiques', 'Doctorat de Sciences juridiques', '#^Doctorat de Sciences juridiques$#', 82, 0);
INSERT INTO `t_dcpl_intra` VALUES (82, '', 'Management du Sport, des Loisirs et du Tourisme', 'Management du Sport, des Loisirs et du Tourisme', '#^Management du Sport, des Loisirs et du Tourisme$#', 83, 0);
INSERT INTO `t_dcpl_intra` VALUES (83, '', 'Licence Pro Adjoint Direc', 'Licence Pro Adjoint Direc', '#^Licence Pro Adjoint Direc$#', 84, 0);
INSERT INTO `t_dcpl_intra` VALUES (84, '', 'DUT Gestion des Entreprises et des Administrations', 'DUT Gestion des Entreprises et des Administrations', '#^DUT Gestion des Entreprises et des Administrations$#', 85, 0);
INSERT INTO `t_dcpl_intra` VALUES (85, '', 'Management et administation des entreprises', 'Management et administation des entreprises', '#^Management et administation des entreprises$#', 86, 0);
INSERT INTO `t_dcpl_intra` VALUES (86, '', 'Direction administrative et financière', 'Direction administrative et financière', '#^Direction administrative et financière$#', 87, 0);
INSERT INTO `t_dcpl_intra` VALUES (87, '', 'Master Évaluation, Gestion et Traitements des Pollutions', 'Master Évaluation, Gestion et Traitements des Pollutions', '#^Master Évaluation, Gestion et Traitements des Pollutions$#', 88, 0);
INSERT INTO `t_dcpl_intra` VALUES (88, '', 'Doctorat Informatique', 'Doctorat Informatique', '#^Doctorat Informatique$#', 89, 0);
INSERT INTO `t_dcpl_intra` VALUES (89, '', 'LP Production industrielle', 'LP Production industrielle', '#^LP Production industrielle$#', 90, 0);
INSERT INTO `t_dcpl_intra` VALUES (90, '', 'Licence professionnelle  espaces naturels', 'Licence professionnelle  espaces naturels', '#^Licence professionnelle  espaces naturels$#', 91, 0);
INSERT INTO `t_dcpl_intra` VALUES (91, '', 'DUT Statistique et informatique décisionnelle', 'DUT Statistique et informatique décisionnelle', '#^DUT Statistique et informatique décisionnelle$#', 92, 0);
INSERT INTO `t_dcpl_intra` VALUES (92, '', 'Master Droit entreprise', 'Master Droit entreprise', '#^Master Droit entreprise$#', 93, 0);
INSERT INTO `t_dcpl_intra` VALUES (93, '', 'DU Français Langue Etrang', 'DU Français Langue Etrang', '#^DU Français Langue Etrang$#', 94, 0);
INSERT INTO `t_dcpl_intra` VALUES (94, '', 'Licence Administration Economique et Sociale', 'Licence Administration Economique et Sociale', '#^Licence Administration Economique et Sociale$#', 95, 0);
INSERT INTO `t_dcpl_intra` VALUES (95, '', 'Doctorat Histoire art', 'Doctorat Histoire art', '#^Doctorat Histoire art$#', 96, 0);
INSERT INTO `t_dcpl_intra` VALUES (96, '', 'Master Droit Privé', 'Master Droit Privé', '#^Master Droit Privé$#', 97, 0);
INSERT INTO `t_dcpl_intra` VALUES (97, '', 'Master Sociétés, Aménagement, Territoires', 'Master Sociétés, Aménagement, Territoires', '#^Master Sociétés, Aménagement, Territoires$#', 98, 0);
INSERT INTO `t_dcpl_intra` VALUES (98, '', 'Ecologie Hist  & Comparée', 'Ecologie Hist  & Comparée', '#^Ecologie Hist  & Comparée$#', 99, 0);
INSERT INTO `t_dcpl_intra` VALUES (99, '', 'Licence professionnelle Énergie et Génie climatique', 'Licence professionnelle Énergie et Génie climatique', '#^Licence professionnelle Énergie et Génie climatique$#', 100, 0);
INSERT INTO `t_dcpl_intra` VALUES (100, '', 'Doct Sciences de gestion', 'Doct Sciences de gestion', '#^Doct Sciences de gestion$#', 101, 0);
INSERT INTO `t_dcpl_intra` VALUES (101, '', 'LP Protection de l'' Environnement', 'LP Protection de l'' Environnement', '#^LP Protection de l'' Environnement$#', 102, 0);
INSERT INTO `t_dcpl_intra` VALUES (102, '', 'Réseaux Télécommunications Intégration Systèmes Voix Données', 'Réseaux Télécommunications Intégration Systèmes Voix Données', '#^Réseaux Télécommunications Intégration Systèmes Voix Données$#', 103, 0);
INSERT INTO `t_dcpl_intra` VALUES (103, '', 'LP Assurance, Banque, Finance', 'LP Assurance, Banque, Finance', '#^LP Assurance, Banque, Finance$#', 104, 0);
INSERT INTO `t_dcpl_intra` VALUES (104, '', 'Master Mathématiques et leurs applic. - Maths Modèl. Simul.', 'Master Mathématiques et leurs applic. - Maths Modèl. Simul.', '#^Master Mathématiques et leurs applic. - Maths Modèl. Simul.$#', 105, 0);
INSERT INTO `t_dcpl_intra` VALUES (105, '', 'Gestion de la production industrielle', 'Gestion de la production industrielle', '#^Gestion de la production industrielle$#', 106, 0);
INSERT INTO `t_dcpl_intra` VALUES (106, '', 'Préparation à l'' agrégation d''anglais', 'Préparation à l'' agrégation d''anglais', '#^Préparation à l'' agrégation d''anglais$#', 107, 0);
INSERT INTO `t_dcpl_intra` VALUES (107, '', 'Traduction et Documentation  Scientifique et Technique', 'Traduction et Documentation  Scientifique et Technique', '#^Traduction et Documentation  Scientifique et Technique$#', 108, 0);
INSERT INTO `t_dcpl_intra` VALUES (108, '', 'Année Préparatoire à l''Insertion en Licence Sc. Math-Info', 'Année Préparatoire à l''Insertion en Licence Sc. Math-Info', '#^Année Préparatoire à l''Insertion en Licence Sc. Math-Info$#', 109, 0);
INSERT INTO `t_dcpl_intra` VALUES (109, '', 'Année Préparatoire à l''Insertion en Lic. Sanitaire et Social', 'Année Préparatoire à l''Insertion en Lic. Sanitaire et Social', '#^Année Préparatoire à l''Insertion en Lic. Sanitaire et Social$#', 110, 0);
INSERT INTO `t_dcpl_intra` VALUES (110, '', 'Métiers de l''enseignement en mathématiques', 'Métiers de l''enseignement en mathématiques', '#^Métiers de l''enseignement en mathématiques$#', 111, 0);
INSERT INTO `t_dcpl_intra` VALUES (111, '', 'DU ADM TERRITORIALE', 'DU ADM TERRITORIALE', '#^DU ADM TERRITORIALE$#', 112, 0);
INSERT INTO `t_dcpl_intra` VALUES (112, '', 'DUT SID', 'DUT SID', '#^DUT SID$#', 113, 0);
INSERT INTO `t_dcpl_intra` VALUES (113, '', 'LP Systémes Informatiques et Logiciels', 'LP Systémes Informatiques et Logiciels', '#^LP Systémes Informatiques et Logiciels$#', 114, 0);
INSERT INTO `t_dcpl_intra` VALUES (114, '', 'Coopération transfrontalière et interrégionale', 'Coopération transfrontalière et interrégionale', '#^Coopération transfrontalière et interrégionale$#', 115, 0);
INSERT INTO `t_dcpl_intra` VALUES (115, '', 'Préparation à l'' agrégation d''espagnol', 'Préparation à l'' agrégation d''espagnol', '#^Préparation à l'' agrégation d''espagnol$#', 116, 0);
INSERT INTO `t_dcpl_intra` VALUES (116, '', 'Sécurité des biens et des personnes', 'Sécurité des biens et des personnes', '#^Sécurité des biens et des personnes$#', 117, 0);
INSERT INTO `t_dcpl_intra` VALUES (117, '', 'DELF/DALF', 'DELF/DALF', '#^DELF/DALF$#', 118, 0);
INSERT INTO `t_dcpl_intra` VALUES (118, '', 'DU Responsable en logistique et transports', 'DU Responsable en logistique et transports', '#^DU Responsable en logistique et transports$#', 119, 0);
INSERT INTO `t_dcpl_intra` VALUES (119, '', 'Doctorat Mécanique des fluides', 'Doctorat Mécanique des fluides', '#^Doctorat Mécanique des fluides$#', 120, 0);
INSERT INTO `t_dcpl_intra` VALUES (120, '', 'Doct Physio et biologie des organismes-populations-interacti', 'Doct Physio et biologie des organismes-populations-interacti', '#^Doct Physio et biologie des organismes-populations-interacti$#', 121, 0);
INSERT INTO `t_dcpl_intra` VALUES (121, '', 'Master Ingenierie de projet, politiques locales et Tic', 'Master Ingenierie de projet, politiques locales et Tic', '#^Master Ingenierie de projet, politiques locales et Tic$#', 122, 0);
INSERT INTO `t_dcpl_intra` VALUES (122, '', 'Réseaux Télécommunications, Administration Sécurité Réseaux', 'Réseaux Télécommunications, Administration Sécurité Réseaux', '#^Réseaux Télécommunications, Administration Sécurité Réseaux$#', 123, 0);
INSERT INTO `t_dcpl_intra` VALUES (123, '', 'Doctorat STAPS', 'Doctorat STAPS', '#^Doctorat STAPS$#', 124, 0);
INSERT INTO `t_dcpl_intra` VALUES (124, '', 'Préparation au concurs d''entrée de l''ENM', 'Préparation au concurs d''entrée de l''ENM', '#^Préparation au concurs d''entrée de l''ENM$#', 125, 0);
INSERT INTO `t_dcpl_intra` VALUES (125, '', 'Année Préparatoire à l''Insertion en Licence Sc. de la Terre', 'Année Préparatoire à l''Insertion en Licence Sc. de la Terre', '#^Année Préparatoire à l''Insertion en Licence Sc. de la Terre$#', 126, 0);
INSERT INTO `t_dcpl_intra` VALUES (126, '', 'Année préparatoire à l''insertion en licence', 'Année préparatoire à l''insertion en licence', '#^Année préparatoire à l''insertion en licence$#', 127, 0);
INSERT INTO `t_dcpl_intra` VALUES (127, '', 'Licence Pro GRH', 'Licence Pro GRH', '#^Licence Pro GRH$#', 128, 0);
INSERT INTO `t_dcpl_intra` VALUES (128, '', 'Doctorat Langues et littératures françaises', 'Doctorat Langues et littératures françaises', '#^Doctorat Langues et littératures françaises$#', 129, 0);
INSERT INTO `t_dcpl_intra` VALUES (129, '', 'Doctorat d''Ethnologie', 'Doctorat d''Ethnologie', '#^Doctorat d''Ethnologie$#', 130, 0);
INSERT INTO `t_dcpl_intra` VALUES (130, '', 'Doct Génie Electrique', 'Doct Génie Electrique', '#^Doct Génie Electrique$#', 131, 0);
INSERT INTO `t_dcpl_intra` VALUES (131, '', 'Diplôme d''Accès aux Études Universitaires option A', 'Diplôme d''Accès aux Études Universitaires option A', '#^Diplôme d''Accès aux Études Universitaires option A$#', 132, 0);
INSERT INTO `t_dcpl_intra` VALUES (132, '', 'DU Techniques multimédias', 'DU Techniques multimédias', '#^DU Techniques multimédias$#', 133, 0);
INSERT INTO `t_dcpl_intra` VALUES (133, '', 'DU  Préparation au  concours Greffier en chef & Greffier', 'DU  Préparation au  concours Greffier en chef & Greffier', '#^DU  Préparation au  concours Greffier en chef & Greffier$#', 134, 0);
INSERT INTO `t_dcpl_intra` VALUES (134, '', 'Capacité en droit', 'Capacité en droit', '#^Capacité en droit$#', 135, 0);
INSERT INTO `t_dcpl_intra` VALUES (135, '', 'C2i - niveau 1', 'C2i - niveau 1', '#^C2i - niveau 1$#', 136, 0);
INSERT INTO `t_dcpl_intra` VALUES (136, '', 'DU Sciences de l''inadaptation et de la délinquance juvénile', 'DU Sciences de l''inadaptation et de la délinquance juvénile', '#^DU Sciences de l''inadaptation et de la délinquance juvénile$#', 137, 0);
INSERT INTO `t_dcpl_intra` VALUES (137, '', 'Doctorat Histoire', 'Doctorat Histoire', '#^Doctorat Histoire$#', 138, 0);
INSERT INTO `t_dcpl_intra` VALUES (138, '', 'Année Préparatoire à l''Insertion en Licence Sc.  MASS', 'Année Préparatoire à l''Insertion en Licence Sc.  MASS', '#^Année Préparatoire à l''Insertion en Licence Sc.  MASS$#', 139, 0);
INSERT INTO `t_dcpl_intra` VALUES (139, '', 'DU Technologies de l''Information et de la Communication', 'DU Technologies de l''Information et de la Communication', '#^DU Technologies de l''Information et de la Communication$#', 140, 0);
INSERT INTO `t_dcpl_intra` VALUES (140, '', 'CAPES HISTOIRE GEOGRAPHIE', 'CAPES HISTOIRE GEOGRAPHIE', '#^CAPES HISTOIRE GEOGRAPHIE$#', 141, 0);
INSERT INTO `t_dcpl_intra` VALUES (141, '', 'CAPES ANGLAIS', 'CAPES ANGLAIS', '#^CAPES ANGLAIS$#', 142, 0);
INSERT INTO `t_dcpl_intra` VALUES (142, '', 'Master Langue et Cult Rég', 'Master Langue et Cult Rég', '#^Master Langue et Cult Rég$#', 143, 0);
INSERT INTO `t_dcpl_intra` VALUES (143, '', 'Préparation à l'' agrégation de lettres', 'Préparation à l'' agrégation de lettres', '#^Préparation à l'' agrégation de lettres$#', 144, 0);
INSERT INTO `t_dcpl_intra` VALUES (144, '', 'DU METIERS DE L'' ADMINISTRATION GENERALE TERRITORIALE', 'DU METIERS DE L'' ADMINISTRATION GENERALE TERRITORIALE', '#^DU METIERS DE L'' ADMINISTRATION GENERALE TERRITORIALE$#', 145, 0);

-- --------------------------------------------------------

-- 
-- Structure de la table `t_diplome`
-- 

DROP TABLE IF EXISTS `t_diplome`;
CREATE TABLE `t_diplome` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  `NBR_AN_FORM` smallint(6) NOT NULL default '0',
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_diplome`
-- 

INSERT INTO `t_diplome` VALUES (2, '', 'Baccalauréat', 'Baccalauréat', 2, 0);
INSERT INTO `t_diplome` VALUES (3, '', 'Licence 1', 'Licence 1', 3, 1);
INSERT INTO `t_diplome` VALUES (4, '', 'Licence 2', 'Licence 2', 4, 2);
INSERT INTO `t_diplome` VALUES (5, '', 'Licence 3', 'Licence 3', 5, 3);
INSERT INTO `t_diplome` VALUES (6, '', 'Master 1', 'Master 1', 6, 4);
INSERT INTO `t_diplome` VALUES (7, '', 'Master 2', 'Master 2', 7, 5);
INSERT INTO `t_diplome` VALUES (8, '', 'Doctorat', 'Doctorat', 8, 6);
INSERT INTO `t_diplome` VALUES (9, '', 'BTS', 'BTS', 9, 2);
INSERT INTO `t_diplome` VALUES (10, '', 'DUT', 'DUT', 10, 2);
INSERT INTO `t_diplome` VALUES (1, '', 'Autre', 'Autre', 1, 0);

-- --------------------------------------------------------

-- 
-- Structure de la table `t_diplome_intra`
-- 

DROP TABLE IF EXISTS `t_diplome_intra`;
CREATE TABLE `t_diplome_intra` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `CHAINE_TYPE` varchar(255) collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  `ID_STRUCT` smallint(6) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_diplome_intra`
-- 

INSERT INTO `t_diplome_intra` VALUES (1, '', 'Diplome ', '', '#^$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (2, '', 'Licence 3 II', 'TLDII3', '#^TLDII3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (3, '', 'Diplome 0', '0', '#^0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (4, '', 'Licence 2 AN', 'LLDAN2', '#^LLDAN2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (5, '', 'DUT 1 MB', 'UTDMB1', '#^UTDMB1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (6, '', 'Diplome UTTE2', 'UTTE2', '#^UTTE2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (7, '', 'Master 1 CO', 'AZDCO1', '#^AZDCO1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (8, '', 'Master 1 CE', 'DZDCE1', '#^DZDCE1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (9, '', 'Master 1 SM', 'TZDSM1', '#^TZDSM1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (10, '', 'Licence 2 TE', 'LLDTE2', '#^LLDTE2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (11, '', 'Licence 3 IN', 'TLDIN3', '#^TLDIN3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (12, '', 'Licence 3 TE', 'LLDTE3', '#^LLDTE3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (13, '', 'Licence 3 SV', 'TLDSV3', '#^TLDSV3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (14, '', 'Licence 1 LE', 'LLDLE1', '#^LLDLE1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (15, '', 'Diplome UTTR2', 'UTTR2', '#^UTTR2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (16, '', 'Master 1 PY', 'LZDPY1', '#^LZDPY1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (17, '', 'Master 2 SS', 'DZDSS2', '#^DZDSS2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (18, '', 'Master 2 ME', 'TZDME2', '#^TZDME2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (19, '', 'Licence 1 IN', 'TLDIN1', '#^TLDIN1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (20, '', 'Diplome RISA4', 'RISA4', '#^RISA4$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (21, '', 'Licence 3 EN', 'LLDEN3', '#^LLDEN3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (22, '', 'Licence 0 PB', 'ULDPB0', '#^ULDPB0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (23, '', ' 2 IP', 'EGTIP2', '#^EGTIP2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (24, '', 'Licence 1 SV', 'TLDSV1', '#^TLDSV1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (25, '', 'Licence 3 MA', 'TLDMA3', '#^TLDMA3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (26, '', 'Licence 1 ST', 'LLDST1', '#^LLDST1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (27, '', 'Licence 2 BA', 'PLDBA2', '#^PLDBA2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (28, '', 'Licence 2 SV', 'TLDSV2', '#^TLDSV2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (29, '', 'Master 1 GP', 'TZDGP1', '#^TZDGP1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (30, '', 'Diplome CWSU1', 'CWSU1', '#^CWSU1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (31, '', 'Master 2 SM', 'TZDSM2', '#^TZDSM2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (32, '', 'Licence 2 MA', 'TLDMA2', '#^TLDMA2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (33, '', ' 2 IE', 'EGTIE2', '#^EGTIE2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (34, '', 'Licence 2 MT', 'TLDMT2', '#^TLDMT2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (35, '', 'Diplome RISA3', 'RISA3', '#^RISA3$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (36, '', 'Licence 1 TA', 'LLDTA1', '#^LLDTA1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (37, '', 'Diplome ERGP3', 'ERGP3', '#^ERGP3$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (38, '', 'Master 2 VP', 'LZDVP2', '#^LZDVP2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (39, '', 'Licence 3 EM', 'LLDEM3', '#^LLDEM3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (40, '', 'Diplome TILSP', 'TILSP', '#^TILSP$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (41, '', 'Master 1 AA', 'LZDAA1', '#^LZDAA1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (42, '', 'Licence 1 DR', 'DLDDR1', '#^DLDDR1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (43, '', 'Licence 1 PY', 'TLDPY1', '#^TLDPY1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (44, '', ' 3 IP', 'EGTIP3', '#^EGTIP3$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (45, '', 'Master 1 HL', 'LZDHL1', '#^LZDHL1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (46, '', 'Licence 3 PG', 'TLDPG3', '#^TLDPG3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (47, '', 'Doctorat 2 CH', 'CWSCH2', '#^CWSCH2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (48, '', 'Master 2 GP', 'TZDGP2', '#^TZDGP2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (49, '', 'Master 1 TI', 'TZDTI1', '#^TZDTI1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (50, '', 'Diplome LUDER', 'LUDER', '#^LUDER$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (51, '', 'Licence 2 EM', 'LLDEM2', '#^LLDEM2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (52, '', 'Master 2 IS', 'TZDIS2', '#^TZDIS2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (53, '', 'Licence 3 EE', 'DLDEE3', '#^DLDEE3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (54, '', 'Licence 3 AN', 'LLDAN3', '#^LLDAN3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (55, '', 'Diplome DUSCC', 'DUSCC', '#^DUSCC$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (56, '', 'Licence 1 GE', 'LLDGE1', '#^LLDGE1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (57, '', 'Diplome DBDR', 'DBDR', '#^DBDR$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (58, '', 'Master 1 SI', 'AZDSI1', '#^AZDSI1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (59, '', 'Master 2 SI', 'AZDSI2', '#^AZDSI2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (60, '', 'Diplome CWSMA', 'CWSMA', '#^CWSMA$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (61, '', 'Master 2 TI', 'TZDTI2', '#^TZDTI2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (62, '', 'Licence 3 SR', 'TLDSR3', '#^TLDSR3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (63, '', 'Licence 1 DR', 'PLDDR1', '#^PLDDR1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (64, '', 'Diplome UTTE1', 'UTTE1', '#^UTTE1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (65, '', 'Licence 1 AN', 'LLDAN1', '#^LLDAN1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (66, '', 'Licence 1 MM', 'TLDMM1', '#^TLDMM1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (67, '', 'Master 1 BT', 'TZDBT1', '#^TZDBT1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (68, '', 'Master 2 SS', 'AZDSS2', '#^AZDSS2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (69, '', 'Master 2 DC', 'LZEDC2', '#^LZEDC2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (70, '', 'Diplome RISA5', 'RISA5', '#^RISA5$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (71, '', 'Master 2 CO', 'AZDCO2', '#^AZDCO2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (72, '', 'Licence 2 HI', 'LLDHI2', '#^LLDHI2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (73, '', 'Licence 3 CA', 'LLDCA3', '#^LLDCA3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (74, '', 'Diplome CWSGC', 'CWSGC', '#^CWSGC$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (75, '', 'Master 1 IE', 'DZDIE1', '#^DZDIE1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (76, '', 'Master 2 FL', 'LZDFL2', '#^LZDFL2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (77, '', ' 3 IE', 'EGTIE3', '#^EGTIE3$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (78, '', 'Licence 2 SR', 'TLDSR2', '#^TLDSR2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (79, '', 'Master 1 NO', 'DZDNO1', '#^DZDNO1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (80, '', 'Master 2 PU', 'DZDPU2', '#^DZDPU2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (81, '', 'Licence 0 PI', 'ULDPI0', '#^ULDPI0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (82, '', 'Doctorat 0 PH', 'CWSPH0', '#^CWSPH0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (83, '', 'Licence 2 CA', 'LLDCA2', '#^LLDCA2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (84, '', 'Master 2 AT', 'LZDAT2', '#^LZDAT2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (85, '', 'Licence 1 CA', 'LLDCA1', '#^LLDCA1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (86, '', 'Master 2 AB', 'TZDAB2', '#^TZDAB2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (87, '', 'Master 2 AA', 'LZDAA2', '#^LZDAA2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (88, '', 'Licence 2 DR', 'PLDDR2', '#^PLDDR2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (89, '', 'Doctorat 1 CH', 'CWSCH1', '#^CWSCH1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (90, '', 'Master 1 DP', 'TZEDP1', '#^TZEDP1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (91, '', 'Diplome BTMI1', 'BTMI1', '#^BTMI1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (92, '', 'Diplome CWSEN', 'CWSEN', '#^CWSEN$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (93, '', 'Master 1 PS', 'LZDPS1', '#^LZDPS1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (94, '', 'Master 2 SI', 'RZDSI2', '#^RZDSI2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (95, '', 'Master 2 DH', 'LZEDH2', '#^LZEDH2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (96, '', 'Diplome CWSU2', 'CWSU2', '#^CWSU2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (97, '', 'Doctorat 2 EO', 'LWGEO2', '#^LWGEO2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (98, '', 'Diplome UTBI2', 'UTBI2', '#^UTBI2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (99, '', 'Diplome CWSGP', 'CWSGP', '#^CWSGP$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (100, '', 'Master 1 CC', 'TZDCC1', '#^TZDCC1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (101, '', 'Master 2 AL', 'AZDAL2', '#^AZDAL2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (102, '', 'Diplome DWDSE', 'DWDSE', '#^DWDSE$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (103, '', 'Master 1 PU', 'DZDPU1', '#^DZDPU1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (104, '', 'Licence 3 NP', 'TLDNP3', '#^TLDNP3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (105, '', 'Master 3 AL', 'AZDAL3', '#^AZDAL3$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (106, '', 'Master 2 CH', 'TZDCH2', '#^TZDCH2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (107, '', 'Master 1 FL', 'LZDFL1', '#^LZDFL1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (108, '', 'Diplome UTTR1', 'UTTR1', '#^UTTR1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (109, '', ' 1 IP', 'EGTIP1', '#^EGTIP1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (110, '', 'Master 1 EJ', 'PZDEJ1', '#^PZDEJ1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (111, '', 'Master 1 AT', 'LZDAT1', '#^LZDAT1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (112, '', 'Licence 2 EG', 'PLDEG2', '#^PLDEG2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (113, '', 'Diplome BTIN4', 'BTIN4', '#^BTIN4$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (114, '', 'Diplome PFCG2', 'PFCG2', '#^PFCG2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (115, '', 'Diplome BTTC2', 'BTTC2', '#^BTTC2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (116, '', 'Licence 3 SP', 'TLDSP3', '#^TLDSP3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (117, '', 'Diplome BTIN1', 'BTIN1', '#^BTIN1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (118, '', 'Licence 3 CH', 'TLDCH3', '#^TLDCH3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (119, '', 'Diplome TILSV', 'TILSV', '#^TILSV$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (120, '', 'Master 2 SC', 'DZDSC2', '#^DZDSC2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (121, '', ' 1 IE', 'EGTIE1', '#^EGTIE1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (122, '', 'Licence 3 CS', 'TLDCS3', '#^TLDCS3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (123, '', 'Licence 1 SR', 'TLDSR1', '#^TLDSR1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (124, '', 'Licence 1 PY', 'RLDPY1', '#^RLDPY1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (125, '', 'Licence 2 IN', 'TLDIN2', '#^TLDIN2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (126, '', 'Master 1 EA', 'RZDEA1', '#^RZDEA1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (127, '', 'Doctorat 1 PH', 'CWSPH1', '#^CWSPH1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (128, '', 'Master 1 TD', 'LZDTD1', '#^LZDTD1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (129, '', 'Master 2 IE', 'DZDIE2', '#^DZDIE2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (130, '', 'Master 1 VP', 'LZDVP1', '#^LZDVP1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (131, '', 'Master 1 DS', 'LZEDS1', '#^LZEDS1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (132, '', 'Licence 2 CH', 'TLDCH2', '#^TLDCH2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (133, '', 'Master 2 CE', 'DZDCE2', '#^DZDCE2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (134, '', 'Master 2 MD', 'TZDMD2', '#^TZDMD2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (135, '', 'Master 1 MD', 'TZDMD1', '#^TZDMD1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (136, '', 'Licence 3 DR', 'DLDDR3', '#^DLDDR3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (137, '', 'Licence 1 AR', 'LLDAR1', '#^LLDAR1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (138, '', 'Licence 3 MT', 'TLDMT3', '#^TLDMT3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (139, '', 'Diplome DWDPR', 'DWDPR', '#^DWDPR$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (140, '', 'Master 2 MS', 'LZDMS2', '#^LZDMS2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (141, '', 'Licence 0 PA', 'PLDPA0', '#^PLDPA0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (142, '', 'Master 1 AT', 'DZDAT1', '#^DZDAT1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (143, '', 'Licence 1 MA', 'TLDMA1', '#^TLDMA1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (144, '', 'Diplome BTEA1', 'BTEA1', '#^BTEA1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (145, '', 'Master 2 EP', 'DZDEP2', '#^DZDEP2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (146, '', 'Licence 3 TA', 'LLDTA3', '#^LLDTA3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (147, '', 'Licence 1 BO', 'RLDBO1', '#^RLDBO1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (148, '', 'Master 3 AE', 'AZDAE3', '#^AZDAE3$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (149, '', 'Doctorat 2 PH', 'CWSPH2', '#^CWSPH2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (150, '', 'Licence 3 HI', 'LLDHI3', '#^LLDHI3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (151, '', 'Licence 3 LE', 'LLDLE3', '#^LLDLE3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (152, '', 'Master 1 DA', 'AZDDA1', '#^AZDDA1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (153, '', 'Master 1 NP', 'TZDNP1', '#^TZDNP1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (154, '', 'Diplome CWSIN', 'CWSIN', '#^CWSIN$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (155, '', 'Diplome RUER', 'RUER', '#^RUER$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (156, '', 'Master 1 LL', 'LZDLL1', '#^LZDLL1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (157, '', 'Master 1 AR', 'LZDAR1', '#^LZDAR1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (158, '', 'Licence 3 EC', 'DLDEC3', '#^DLDEC3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (159, '', 'Licence 2 EG', 'DLDEG2', '#^DLDEG2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (160, '', 'Licence 2 EN', 'LLDEN2', '#^LLDEN2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (161, '', 'Licence 2 DR', 'DLDDR2', '#^DLDDR2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (162, '', 'Master 2 AE', 'AZDAE2', '#^AZDAE2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (163, '', 'Master 2 AT', 'DZDAT2', '#^DZDAT2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (164, '', 'Licence 0 PM', 'ULDPM0', '#^ULDPM0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (165, '', 'Licence 1 HI', 'LLDHI1', '#^LLDHI1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (166, '', 'Master 1 IM', 'AZDIM1', '#^AZDIM1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (167, '', 'Master 1 EP', 'DZDEP1', '#^DZDEP1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (168, '', 'Licence 1 TE', 'LLDTE1', '#^LLDTE1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (169, '', 'Master 1 SI', 'RZDSI1', '#^RZDSI1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (170, '', 'Licence 0 PV', 'RLDPV0', '#^RLDPV0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (171, '', 'Licence 1 LE', 'PLDLE1', '#^PLDLE1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (172, '', 'Master 1 SC', 'DZDSC1', '#^DZDSC1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (173, '', 'DUT 2 ID', 'UTSID2', '#^UTSID2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (174, '', 'Licence 2 GE', 'LLDGE2', '#^LLDGE2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (175, '', 'Master 2 IE', 'PZDIE2', '#^PZDIE2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (176, '', 'Master 1 JA', 'DZDJA1', '#^DZDJA1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (177, '', 'Licence 3 EG', 'PLDEG3', '#^PLDEG3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (178, '', 'Licence 3 EF', 'DLDEF3', '#^DLDEF3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (179, '', 'Master 2 RA', 'LZDRA2', '#^LZDRA2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (180, '', 'Master 2 DA', 'AZDDA2', '#^AZDDA2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (181, '', 'Diplome d''Université 1 FL', 'LUDFL1', '#^LUDFL1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (182, '', 'Master 1 DA', 'LZEDA1', '#^LZEDA1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (183, '', 'Licence 3 AG', 'DLDAG3', '#^DLDAG3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (184, '', 'Master 1 SS', 'DZDSS1', '#^DZDSS1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (185, '', 'Licence 2 BO', 'RLDBO2', '#^RLDBO2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (186, '', 'Master 2 HL', 'LZDHL2', '#^LZDHL2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (187, '', 'Master 1 DL', 'LZEDL1', '#^LZEDL1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (188, '', 'Diplome LWHA1', 'LWHA1', '#^LWHA1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (189, '', 'Licence 3 AR', 'LLDAR3', '#^LLDAR3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (190, '', 'Licence 3 BO', 'RLDBO3', '#^RLDBO3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (191, '', 'Master 2 PR', 'DZDPR2', '#^DZDPR2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (192, '', 'Master 1 IS', 'TZDIS1', '#^TZDIS1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (193, '', 'Licence 3 AE', 'DLDAE3', '#^DLDAE3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (194, '', 'Master 1 SA', 'LZDSA1', '#^LZDSA1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (195, '', 'Licence 2 TA', 'LLDTA2', '#^LLDTA2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (196, '', 'Diplome PFCG1', 'PFCG1', '#^PFCG1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (197, '', 'Master 2 CO', 'DZDCO2', '#^DZDCO2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (198, '', 'Master 2 PY', 'LZDPY2', '#^LZDPY2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (199, '', 'Master 1 EH', 'LZDEH1', '#^LZDEH1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (200, '', 'Licence 0 PT', 'ULDPT0', '#^ULDPT0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (201, '', 'Doctorat 3 CH', 'CWSCH3', '#^CWSCH3$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (202, '', 'Licence 1 CP', 'TLDCP1', '#^TLDCP1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (203, '', 'Master 2 AR', 'LZDAR2', '#^LZDAR2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (204, '', 'Diplome DWDSG', 'DWDSG', '#^DWDSG$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (205, '', 'Diplome EUER', 'EUER', '#^EUER$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (206, '', 'Licence 1 AS', 'DLDAS1', '#^DLDAS1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (207, '', 'Licence 0 PE', 'RLDPE0', '#^RLDPE0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (208, '', 'Licence 2 LE', 'LLDLE2', '#^LLDLE2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (209, '', 'Master 1 DH', 'LZEDH1', '#^LZEDH1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (210, '', 'Master 2 BT', 'TZDBT2', '#^TZDBT2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (211, '', 'Diplome RISA2', 'RISA2', '#^RISA2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (212, '', 'Licence 3 DE', 'DLDDE3', '#^DLDDE3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (213, '', 'Licence 0 PV', 'ULDPV0', '#^ULDPV0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (214, '', 'Licence 0 PD', 'BLDPD0', '#^BLDPD0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (215, '', 'DUT 1 ID', 'UTSID1', '#^UTSID1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (216, '', 'Licence 2 AR', 'LLDAR2', '#^LLDAR2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (217, '', 'Master 2 MQ', 'TZDMQ2', '#^TZDMQ2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (218, '', 'Licence 0 PP', 'TLDPP0', '#^TLDPP0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (219, '', 'Master 2 DS', 'LZEDS2', '#^LZEDS2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (220, '', 'Licence 3 GE', 'LLDGE3', '#^LLDGE3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (221, '', 'Licence 3 EI', 'PLDEI3', '#^PLDEI3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (222, '', 'Master 2 MD', 'PZDMD2', '#^PZDMD2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (223, '', 'Licence 3 SI', 'RLDSI3', '#^RLDSI3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (224, '', 'Master 2 CI', 'AZDCI2', '#^AZDCI2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (225, '', 'Master 2 LL', 'LZDLL2', '#^LZDLL2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (226, '', 'Licence 2 PG', 'TLDPG2', '#^TLDPG2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (227, '', 'Diplome RISA1', 'RISA1', '#^RISA1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (228, '', 'Diplome LPAN', 'LPAN', '#^LPAN$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (229, '', 'Licence 1 EG', 'DLDEG1', '#^DLDEG1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (230, '', 'Master 2 TD', 'LZDTD2', '#^LZDTD2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (231, '', 'Diplome TILSM', 'TILSM', '#^TILSM$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (232, '', 'Licence 2 CS', 'TLDCS2', '#^TLDCS2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (233, '', 'Diplome UTBI1', 'UTBI1', '#^UTBI1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (234, '', 'Licence 1 EG', 'PLDEG1', '#^PLDEG1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (235, '', 'Diplome TILSS', 'TILSS', '#^TILSS$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (236, '', 'Master 1 CL', 'AZDCL1', '#^AZDCL1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (237, '', 'Master 2 NO', 'DZDNO2', '#^DZDNO2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (238, '', 'Master 2 DM', 'TZEDM2', '#^TZEDM2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (239, '', 'Licence 3 MX', 'TLDMX3', '#^TLDMX3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (240, '', 'Diplome d''Université 1 DT', 'DUADT1', '#^DUADT1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (241, '', 'Master 1 MS', 'LZDMS1', '#^LZDMS1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (242, '', 'Master 1 PR', 'DZDPR1', '#^DZDPR1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (243, '', 'Licence 2 SP', 'RLDSP2', '#^RLDSP2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (244, '', 'Master 2 EA', 'RZDEA2', '#^RZDEA2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (245, '', 'Diplome DWDPU', 'DWDPU', '#^DWDPU$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (246, '', 'Master 2 CL', 'AZDCL2', '#^AZDCL2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (247, '', 'Master 2 PD', 'TZDPD2', '#^TZDPD2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (248, '', 'Licence 0 PM', 'BLDPM0', '#^BLDPM0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (249, '', 'Licence 1 CS', 'TLDCS1', '#^TLDCS1$#', 3, 0);
INSERT INTO `t_diplome_intra` VALUES (250, '', 'Master 2 PS', 'LZDPS2', '#^LZDPS2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (251, '', 'Licence 2 SP', 'TLDSP2', '#^TLDSP2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (252, '', 'Master 1 SS', 'AZDSS1', '#^AZDSS1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (253, '', 'Licence 2 LE', 'PLDLE2', '#^PLDLE2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (254, '', 'Master 2 DA', 'LZEDA2', '#^LZEDA2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (255, '', 'Diplome PFCG3', 'PFCG3', '#^PFCG3$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (256, '', 'Master 2 JA', 'DZDJA2', '#^DZDJA2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (257, '', 'Master 2 EJ', 'PZDEJ2', '#^PZDEJ2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (258, '', 'Master 1 DM', 'TZEDM1', '#^TZEDM1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (259, '', 'Master 1 AB', 'TZDAB1', '#^TZDAB1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (260, '', 'Master 2 DL', 'LZEDL2', '#^LZEDL2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (261, '', 'Master 2 CT', 'PZDCT2', '#^PZDCT2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (262, '', 'Master 1 MQ', 'TZDMQ1', '#^TZDMQ1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (263, '', 'Licence 3 AD', 'DLDAD3', '#^DLDAD3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (264, '', 'Licence 3 DR', 'PLDDR3', '#^PLDDR3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (265, '', 'Diplome BTEA2', 'BTEA2', '#^BTEA2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (266, '', 'Master 1 AP', 'LZDAP1', '#^LZDAP1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (267, '', 'Licence 3 MS', 'LLDMS3', '#^LLDMS3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (268, '', 'Diplome PWDPU', 'PWDPU', '#^PWDPU$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (269, '', 'Preparation Concours Administratifs 0 S0', 'LPES00', '#^LPES00$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (270, '', 'Licence 0 PS', 'TLDPS0', '#^TLDPS0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (271, '', 'DALF/DELF  1 LF', 'LADLF1', '#^LADLF1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (272, '', 'Master 1 DC', 'LZEDC1', '#^LZEDC1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (273, '', 'Diplome FULT0', 'FULT0', '#^FULT0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (274, '', 'DUT 2 MB', 'UTDMB2', '#^UTDMB2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (275, '', 'Licence 2 AD', 'DLDAD2', '#^DLDAD2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (276, '', 'Diplome CWSMF', 'CWSMF', '#^CWSMF$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (277, '', 'Diplome CWSPB', 'CWSPB', '#^CWSPB$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (278, '', 'Diplome BTEA3', 'BTEA3', '#^BTEA3$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (279, '', 'Diplome BTTC1', 'BTTC1', '#^BTTC1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (280, '', 'Master 2 IP', 'LZDIP2', '#^LZDIP2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (281, '', 'Licence 0 PA', 'ULDPA0', '#^ULDPA0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (282, '', 'Doctorat 1 EO', 'LWGEO1', '#^LWGEO1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (283, '', 'Licence 3 MQ', 'TLDMQ3', '#^TLDMQ3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (284, '', 'Diplome CWSST', 'CWSST', '#^CWSST$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (285, '', 'Licence 2 AG', 'DLDAG2', '#^DLDAG2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (286, '', 'Diplome DPDR', 'DPDR', '#^DPDR$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (287, '', 'Diplome TILSR', 'TILSR', '#^TILSR$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (288, '', 'Licence 2 AA', 'DLDAA2', '#^DLDAA2$#', 4, 0);
INSERT INTO `t_diplome_intra` VALUES (289, '', ' L PI', 'DKAPIL', '#^DKAPIL$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (290, '', 'Master 2 SA', 'LZDSA2', '#^LZDSA2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (291, '', 'Licence 0 PH', 'ALDPH0', '#^ALDPH0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (292, '', 'Licence 3 AA', 'DLDAA3', '#^DLDAA3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (293, '', 'Diplome LWLF1', 'LWLF1', '#^LWLF1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (294, '', 'Doctorat 1 TH', 'LWETH1', '#^LWETH1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (295, '', 'Master 2 AA', 'AZDAA2', '#^AZDAA2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (296, '', 'Master 2 EH', 'LZDEH2', '#^LZDEH2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (297, '', 'Diplome BTTC3', 'BTTC3', '#^BTTC3$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (298, '', 'Diplome CWSGE', 'CWSGE', '#^CWSGE$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (299, '', 'Licence 3 LE', 'PLDLE3', '#^PLDLE3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (300, '', 'Master 2 AN', 'AZDAN2', '#^AZDAN2$#', 7, 0);
INSERT INTO `t_diplome_intra` VALUES (301, '', 'DAEU 0 DP', 'FEODP0', '#^FEODP0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (302, '', 'DAEU 0 PP', 'FEOPP0', '#^FEOPP0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (303, '', 'Doctorat 0 CH', 'CWSCH0', '#^CWSCH0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (304, '', 'Diplome d''Université 0 MT', 'FUMMT0', '#^FUMMT0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (305, '', 'Licence 3 EC', 'PLDEC3', '#^PLDEC3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (306, '', 'Diplome DUGCG', 'DUGCG', '#^DUGCG$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (307, '', 'Diplome TUDER', 'TUDER', '#^TUDER$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (308, '', 'Capacité  2 RP', 'DCDRP2', '#^DCDRP2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (309, '', 'Diplome FC2I1', 'FC2I1', '#^FC2I1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (310, '', 'Master 1 CP', 'TZDCP1', '#^TZDCP1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (311, '', 'Diplome PUDJ0', 'PUDJ0', '#^PUDJ0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (312, '', 'Diplome LWHI1', 'LWHI1', '#^LWHI1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (313, '', 'Master 1 CO', 'DZDCO1', '#^DZDCO1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (314, '', 'Diplome d''Université 2 DT', 'DUADT2', '#^DUADT2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (315, '', 'Diplome TILSA', 'TILSA', '#^TILSA$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (316, '', 'Diplome BUTI1', 'BUTI1', '#^BUTI1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (317, '', 'Diplome DUER', 'DUER', '#^DUER$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (318, '', 'Diplome FEOM0', 'FEOM0', '#^FEOM0$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (319, '', 'Licence 3 EP', 'DLDEP3', '#^DLDEP3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (320, '', ' 2 GP', 'I1HGP2', '#^I1HGP2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (321, '', ' 2 NP', 'I1ANP2', '#^I1ANP2$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (322, '', 'Master 1 UQ', 'AZDUQ1', '#^AZDUQ1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (323, '', 'Master 1 BA', 'PZDBA1', '#^PZDBA1$#', 6, 0);
INSERT INTO `t_diplome_intra` VALUES (324, '', 'Diplome ERGP1', 'ERGP1', '#^ERGP1$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (325, '', 'Licence 3 SS', 'TLDSS3', '#^TLDSS3$#', 5, 0);
INSERT INTO `t_diplome_intra` VALUES (326, '', 'Preparation Concours Administratifs 0 M0', 'LPLM00', '#^LPLM00$#', 1, 0);
INSERT INTO `t_diplome_intra` VALUES (327, '', 'Diplome d''Université T AG', 'DUMAGT', '#^DUMAGT$#', 1, 0);

-- --------------------------------------------------------

-- 
-- Structure de la table `t_discipline`
-- 

DROP TABLE IF EXISTS `t_discipline`;
CREATE TABLE `t_discipline` (
  `ID` smallint(6) NOT NULL default '0',
  `EXTN_REF` varchar(50) collate utf8_unicode_ci NOT NULL,
  `LIB` varchar(100) collate utf8_unicode_ci NOT NULL,
  `DSC` text collate utf8_unicode_ci NOT NULL,
  `ID_REGR` int(11) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `t_discipline`
-- 

INSERT INTO `t_discipline` VALUES (94, '', 'DU Français Langue Etrang', 'DU Français Langue Etrang', 0);
INSERT INTO `t_discipline` VALUES (93, '', 'Master Droit entreprise', 'Master Droit entreprise', 0);
INSERT INTO `t_discipline` VALUES (92, '', 'DUT Statistique et informatique décisionnelle', 'DUT Statistique et informatique décisionnelle', 0);
INSERT INTO `t_discipline` VALUES (1, '', 'Autres filières', '', 0);
INSERT INTO `t_discipline` VALUES (91, '', 'Licence professionnelle  espaces naturels', 'Licence professionnelle  espaces naturels', 0);
INSERT INTO `t_discipline` VALUES (90, '', 'LP Production industrielle', 'LP Production industrielle', 0);
INSERT INTO `t_discipline` VALUES (89, '', 'Doctorat Informatique', 'Doctorat Informatique', 0);
INSERT INTO `t_discipline` VALUES (88, '', 'Master Évaluation, Gestion et Traitements des Pollutions', 'Master Évaluation, Gestion et Traitements des Pollutions', 0);
INSERT INTO `t_discipline` VALUES (87, '', 'Direction administrative et financière', 'Direction administrative et financière', 0);
INSERT INTO `t_discipline` VALUES (86, '', 'Management et administation des entreprises', 'Management et administation des entreprises', 0);
INSERT INTO `t_discipline` VALUES (85, '', 'DUT Gestion des Entreprises et des Administrations', 'DUT Gestion des Entreprises et des Administrations', 0);
INSERT INTO `t_discipline` VALUES (84, '', 'Licence Pro Adjoint Direc', 'Licence Pro Adjoint Direc', 0);
INSERT INTO `t_discipline` VALUES (83, '', 'Management du Sport, des Loisirs et du Tourisme', 'Management du Sport, des Loisirs et du Tourisme', 0);
INSERT INTO `t_discipline` VALUES (82, '', 'Doctorat de Sciences juridiques', 'Doctorat de Sciences juridiques', 0);
INSERT INTO `t_discipline` VALUES (81, '', 'Licence Histoire de l''Art et Archéologie', 'Licence Histoire de l''Art et Archéologie', 0);
INSERT INTO `t_discipline` VALUES (80, '', 'Master Méthodes stochastiqu. & informatiqu. pour la décision', 'Master Méthodes stochastiqu. & informatiqu. pour la décision', 0);
INSERT INTO `t_discipline` VALUES (79, '', 'Master Enseignement  en Education Physique et Sportive', 'Master Enseignement  en Education Physique et Sportive', 0);
INSERT INTO `t_discipline` VALUES (77, '', 'Droit Pénal et Sc Crimin.', 'Droit Pénal et Sc Crimin.', 0);
INSERT INTO `t_discipline` VALUES (78, '', 'Master Dynamique des Écosystèmes Aquatiques', 'Master Dynamique des Écosystèmes Aquatiques', 0);
INSERT INTO `t_discipline` VALUES (75, '', 'DUT Techniques de commercialisation', 'DUT Techniques de commercialisation', 0);
INSERT INTO `t_discipline` VALUES (76, '', 'Année Préparatoire à l''Insertion en Licence Sc. de la Vie', 'Année Préparatoire à l''Insertion en Licence Sc. de la Vie', 0);
INSERT INTO `t_discipline` VALUES (74, '', 'Diplôme Compta Gestion', 'Diplôme Compta Gestion', 0);
INSERT INTO `t_discipline` VALUES (72, '', 'Juriste européen', 'Juriste européen', 0);
INSERT INTO `t_discipline` VALUES (73, '', 'DUT Informatique', 'DUT Informatique', 0);
INSERT INTO `t_discipline` VALUES (71, '', 'Master Environnement et Matériaux, Concepts fondamentaux PC', 'Master Environnement et Matériaux, Concepts fondamentaux PC', 0);
INSERT INTO `t_discipline` VALUES (70, '', 'Doct Sciences Economiques', 'Doct Sciences Economiques', 0);
INSERT INTO `t_discipline` VALUES (69, '', 'Management international', 'Management international', 0);
INSERT INTO `t_discipline` VALUES (68, '', 'Doctorat Génie des procédés', 'Doctorat Génie des procédés', 0);
INSERT INTO `t_discipline` VALUES (67, '', 'DUT Génie Biologique - industries alimentaires & biologiques', 'DUT Génie Biologique - industries alimentaires & biologiques', 0);
INSERT INTO `t_discipline` VALUES (66, '', 'Doctorat de Géographie', 'Doctorat de Géographie', 0);
INSERT INTO `t_discipline` VALUES (65, '', 'Master  Enseignement en  Histoire-Géographie', 'Master  Enseignement en  Histoire-Géographie', 0);
INSERT INTO `t_discipline` VALUES (64, '', 'Master Logistique,act. opérationnelles et syst. d''info', 'Master Logistique,act. opérationnelles et syst. d''info', 0);
INSERT INTO `t_discipline` VALUES (63, '', 'Langue Littérature et culture étrangère', 'Langue Littérature et culture étrangère', 0);
INSERT INTO `t_discipline` VALUES (62, '', 'Doctorat Energétique', 'Doctorat Energétique', 0);
INSERT INTO `t_discipline` VALUES (61, '', 'DUT GIM', 'DUT GIM', 0);
INSERT INTO `t_discipline` VALUES (60, '', 'Métiers ens. Phys chimie', 'Métiers ens. Phys chimie', 0);
INSERT INTO `t_discipline` VALUES (59, '', 'Master Bioprotection, Valorisation non alimentaires des agro', 'Master Bioprotection, Valorisation non alimentaires des agro', 0);
INSERT INTO `t_discipline` VALUES (57, '', 'Doctorat de Physique', 'Doctorat de Physique', 0);
INSERT INTO `t_discipline` VALUES (58, '', 'Aménagement Touristique', 'Aménagement Touristique', 0);
INSERT INTO `t_discipline` VALUES (55, '', 'Master Droit Public', 'Master Droit Public', 0);
INSERT INTO `t_discipline` VALUES (56, '', 'Licence Professionnelle Management des organisations SIDMQ', 'Licence Professionnelle Management des organisations SIDMQ', 0);
INSERT INTO `t_discipline` VALUES (54, '', 'Master Droit notarial', 'Master Droit notarial', 0);
INSERT INTO `t_discipline` VALUES (53, '', 'Métiers des langues', 'Métiers des langues', 0);
INSERT INTO `t_discipline` VALUES (51, '', 'Doctorat Génie civil', 'Doctorat Génie civil', 0);
INSERT INTO `t_discipline` VALUES (52, '', 'Economie de l''intégration européenne et internationale', 'Economie de l''intégration européenne et internationale', 0);
INSERT INTO `t_discipline` VALUES (49, '', 'Licence Histoire', 'Licence Histoire', 0);
INSERT INTO `t_discipline` VALUES (50, '', 'Licence Espagnol', 'Licence Espagnol', 0);
INSERT INTO `t_discipline` VALUES (47, '', 'Master Managment Public', 'Master Managment Public', 0);
INSERT INTO `t_discipline` VALUES (48, '', 'Master Enseignement en  Lettres & Langues', 'Master Enseignement en  Lettres & Langues', 0);
INSERT INTO `t_discipline` VALUES (46, '', 'Master Microbiologie et Biotechnologies', 'Master Microbiologie et Biotechnologies', 0);
INSERT INTO `t_discipline` VALUES (45, '', 'Lic Sciences de la Terre', 'Lic Sciences de la Terre', 0);
INSERT INTO `t_discipline` VALUES (44, '', 'Doctorat de Mathématiques', 'Doctorat de Mathématiques', 0);
INSERT INTO `t_discipline` VALUES (43, '', 'Management et Systèmes d''Information', 'Management et Systèmes d''Information', 0);
INSERT INTO `t_discipline` VALUES (42, '', 'Préparation à l''examen d''entrée au CRFPA', 'Préparation à l''examen d''entrée au CRFPA', 0);
INSERT INTO `t_discipline` VALUES (41, '', 'Licence Géographie', 'Licence Géographie', 0);
INSERT INTO `t_discipline` VALUES (40, '', 'Certificat de Sciences criminelles', 'Certificat de Sciences criminelles', 0);
INSERT INTO `t_discipline` VALUES (39, '', 'Licence Economie-Gestion', 'Licence Economie-Gestion', 0);
INSERT INTO `t_discipline` VALUES (38, '', 'Master ingénierie des systémes industriels', 'Master ingénierie des systémes industriels', 0);
INSERT INTO `t_discipline` VALUES (37, '', 'DU ERASMUS', 'DU ERASMUS', 0);
INSERT INTO `t_discipline` VALUES (35, '', 'Doctorat de chimie', 'Doctorat de chimie', 0);
INSERT INTO `t_discipline` VALUES (36, '', 'Master Technologie de l''internet', 'Master Technologie de l''internet', 0);
INSERT INTO `t_discipline` VALUES (34, '', 'Master Poét & hist litté', 'Master Poét & hist litté', 0);
INSERT INTO `t_discipline` VALUES (33, '', 'Licence Droit', 'Licence Droit', 0);
INSERT INTO `t_discipline` VALUES (32, '', 'Histoire, Histoire de l'' Art, Archéologie, Anthropologie', 'Histoire, Histoire de l'' Art, Archéologie, Anthropologie', 0);
INSERT INTO `t_discipline` VALUES (31, '', 'Année Préparatoire à l''Insertion en Licence Sc. Phys-Chimie', 'Année Préparatoire à l''Insertion en Licence Sc. Phys-Chimie', 0);
INSERT INTO `t_discipline` VALUES (30, '', 'Valorisation des Patrimoines & Politiques Culturelles Territ', 'Valorisation des Patrimoines & Politiques Culturelles Territ', 0);
INSERT INTO `t_discipline` VALUES (29, '', 'Diplôme d''ingénieur de l''ENSGTI', 'Diplôme d''ingénieur de l''ENSGTI', 0);
INSERT INTO `t_discipline` VALUES (28, '', 'Licence Mathématiques', 'Licence Mathématiques', 0);
INSERT INTO `t_discipline` VALUES (27, '', 'Diplôme d''ingénieur de l''ENSGTI spécialié Energétique', 'Diplôme d''ingénieur de l''ENSGTI spécialié Energétique', 0);
INSERT INTO `t_discipline` VALUES (26, '', 'Doctorat Sciences de l''univers', 'Doctorat Sciences de l''univers', 0);
INSERT INTO `t_discipline` VALUES (25, '', 'Master génie pétrolier', 'Master génie pétrolier', 0);
INSERT INTO `t_discipline` VALUES (24, '', 'Licence Langue et Culture Régional', 'Licence Langue et Culture Régional', 0);
INSERT INTO `t_discipline` VALUES (23, '', 'Licence Mathématiques Appliquées et Sciences Sociales', 'Licence Mathématiques Appliquées et Sciences Sociales', 0);
INSERT INTO `t_discipline` VALUES (22, '', 'Diplôme d''ingénieur ENSGTI spécialité génie des procédés', 'Diplôme d''ingénieur ENSGTI spécialité génie des procédés', 0);
INSERT INTO `t_discipline` VALUES (21, '', 'Licence professionnelles biotechnologies', 'Licence professionnelles biotechnologies', 0);
INSERT INTO `t_discipline` VALUES (20, '', 'Licence STAPS', 'Licence STAPS', 0);
INSERT INTO `t_discipline` VALUES (19, '', 'Diplôme d''ingénieur de l''ISA BTP', 'Diplôme d''ingénieur de l''ISA BTP', 0);
INSERT INTO `t_discipline` VALUES (17, '', 'Droit public & adm publiq', 'Droit public & adm publiq', 0);
INSERT INTO `t_discipline` VALUES (18, '', 'Master Méthodes Analytiques pour l''Environnement /matériaux', 'Master Méthodes Analytiques pour l''Environnement /matériaux', 0);
INSERT INTO `t_discipline` VALUES (16, '', 'Entrain., prépa phys & mentale & optimisa. de la performance', 'Entrain., prépa phys & mentale & optimisa. de la performance', 0);
INSERT INTO `t_discipline` VALUES (15, '', 'DUT Réseaux et Télécommunications', 'DUT Réseaux et Télécommunications', 0);
INSERT INTO `t_discipline` VALUES (13, '', 'Licence Biologie', 'Licence Biologie', 0);
INSERT INTO `t_discipline` VALUES (14, '', 'Licence Lettres', 'Licence Lettres', 0);
INSERT INTO `t_discipline` VALUES (11, '', 'Licence LEA', 'Licence LEA', 0);
INSERT INTO `t_discipline` VALUES (12, '', 'Licence Informatique', 'Licence Informatique', 0);
INSERT INTO `t_discipline` VALUES (10, '', 'Master Ingénierie et Sciences des Matériaux', 'Master Ingénierie et Sciences des Matériaux', 0);
INSERT INTO `t_discipline` VALUES (9, '', 'Chargé d''études économiques', 'Chargé d''études économiques', 0);
INSERT INTO `t_discipline` VALUES (8, '', 'Comptabilité, Contrôle, Audit', 'Comptabilité, Contrôle, Audit', 0);
INSERT INTO `t_discipline` VALUES (7, '', 'DUT Génie Thermique et Énergie', 'DUT Génie Thermique et Énergie', 0);
INSERT INTO `t_discipline` VALUES (6, '', 'DUT Science et Génie des Matériaux', 'DUT Science et Génie des Matériaux', 0);
INSERT INTO `t_discipline` VALUES (5, '', 'Licence Anglais', 'Licence Anglais', 0);
INSERT INTO `t_discipline` VALUES (2, '', '', '', 0);
INSERT INTO `t_discipline` VALUES (3, '', 'Licence Sciences Physiques et Chimiques', 'Licence Sciences Physiques et Chimiques', 0);
INSERT INTO `t_discipline` VALUES (106, '', 'Gestion de la production industrielle', 'Gestion de la production industrielle', 0);
INSERT INTO `t_discipline` VALUES (105, '', 'Master Mathématiques et leurs applic. - Maths Modèl. Simul.', 'Master Mathématiques et leurs applic. - Maths Modèl. Simul.', 0);
INSERT INTO `t_discipline` VALUES (104, '', 'LP Assurance, Banque, Finance', 'LP Assurance, Banque, Finance', 0);
INSERT INTO `t_discipline` VALUES (103, '', 'Réseaux Télécommunications Intégration Systèmes Voix Données', 'Réseaux Télécommunications Intégration Systèmes Voix Données', 0);
INSERT INTO `t_discipline` VALUES (102, '', 'LP Protection de l'' Environnement', 'LP Protection de l'' Environnement', 0);
INSERT INTO `t_discipline` VALUES (101, '', 'Doct Sciences de gestion', 'Doct Sciences de gestion', 0);
INSERT INTO `t_discipline` VALUES (100, '', 'Licence professionnelle Énergie et Génie climatique', 'Licence professionnelle Énergie et Génie climatique', 0);
INSERT INTO `t_discipline` VALUES (99, '', 'Ecologie Hist  & Comparée', 'Ecologie Hist  & Comparée', 0);
INSERT INTO `t_discipline` VALUES (98, '', 'Master Sociétés, Aménagement, Territoires', 'Master Sociétés, Aménagement, Territoires', 0);
INSERT INTO `t_discipline` VALUES (97, '', 'Master Droit Privé', 'Master Droit Privé', 0);
INSERT INTO `t_discipline` VALUES (96, '', 'Doctorat Histoire art', 'Doctorat Histoire art', 0);
INSERT INTO `t_discipline` VALUES (95, '', 'Licence Administration Economique et Sociale', 'Licence Administration Economique et Sociale', 0);
INSERT INTO `t_discipline` VALUES (107, '', 'Préparation à l'' agrégation d''anglais', 'Préparation à l'' agrégation d''anglais', 0);
INSERT INTO `t_discipline` VALUES (108, '', 'Traduction et Documentation  Scientifique et Technique', 'Traduction et Documentation  Scientifique et Technique', 0);
INSERT INTO `t_discipline` VALUES (109, '', 'Année Préparatoire à l''Insertion en Licence Sc. Math-Info', 'Année Préparatoire à l''Insertion en Licence Sc. Math-Info', 0);
INSERT INTO `t_discipline` VALUES (110, '', 'Année Préparatoire à l''Insertion en Lic. Sanitaire et Social', 'Année Préparatoire à l''Insertion en Lic. Sanitaire et Social', 0);
INSERT INTO `t_discipline` VALUES (111, '', 'Métiers de l''enseignement en mathématiques', 'Métiers de l''enseignement en mathématiques', 0);
INSERT INTO `t_discipline` VALUES (112, '', 'DU ADM TERRITORIALE', 'DU ADM TERRITORIALE', 0);
INSERT INTO `t_discipline` VALUES (113, '', 'DUT SID', 'DUT SID', 0);
INSERT INTO `t_discipline` VALUES (114, '', 'LP Systémes Informatiques et Logiciels', 'LP Systémes Informatiques et Logiciels', 0);
INSERT INTO `t_discipline` VALUES (115, '', 'Coopération transfrontalière et interrégionale', 'Coopération transfrontalière et interrégionale', 0);
INSERT INTO `t_discipline` VALUES (116, '', 'Préparation à l'' agrégation d''espagnol', 'Préparation à l'' agrégation d''espagnol', 0);
INSERT INTO `t_discipline` VALUES (117, '', 'Sécurité des biens et des personnes', 'Sécurité des biens et des personnes', 0);
INSERT INTO `t_discipline` VALUES (118, '', 'DELF/DALF', 'DELF/DALF', 0);
INSERT INTO `t_discipline` VALUES (119, '', 'DU Responsable en logistique et transports', 'DU Responsable en logistique et transports', 0);
INSERT INTO `t_discipline` VALUES (120, '', 'Doctorat Mécanique des fluides', 'Doctorat Mécanique des fluides', 0);
INSERT INTO `t_discipline` VALUES (121, '', 'Doct Physio et biologie des organismes-populations-interacti', 'Doct Physio et biologie des organismes-populations-interacti', 0);
INSERT INTO `t_discipline` VALUES (122, '', 'Master Ingenierie de projet, politiques locales et Tic', 'Master Ingenierie de projet, politiques locales et Tic', 0);
INSERT INTO `t_discipline` VALUES (123, '', 'Réseaux Télécommunications, Administration Sécurité Réseaux', 'Réseaux Télécommunications, Administration Sécurité Réseaux', 0);
INSERT INTO `t_discipline` VALUES (124, '', 'Doctorat STAPS', 'Doctorat STAPS', 0);
INSERT INTO `t_discipline` VALUES (125, '', 'Préparation au concurs d\\''entrée de l\\''ENM', 'Préparation au concurs d\\''entrée de l\\''ENM', 0);
INSERT INTO `t_discipline` VALUES (126, '', 'Année Préparatoire à l\\''Insertion en Licence Sc. de la Terre', 'Année Préparatoire à l\\''Insertion en Licence Sc. de la Terre', 0);
INSERT INTO `t_discipline` VALUES (127, '', 'Année préparatoire à l\\''insertion en licence', 'Année préparatoire à l\\''insertion en licence', 0);
INSERT INTO `t_discipline` VALUES (128, '', 'Licence Pro GRH', 'Licence Pro GRH', 0);
INSERT INTO `t_discipline` VALUES (129, '', 'Doctorat Langues et littératures françaises', 'Doctorat Langues et littératures françaises', 0);
INSERT INTO `t_discipline` VALUES (130, '', 'Doctorat d''Ethnologie', 'Doctorat d''Ethnologie', 0);
INSERT INTO `t_discipline` VALUES (131, '', 'Doct Génie Electrique', 'Doct Génie Electrique', 0);
INSERT INTO `t_discipline` VALUES (132, '', 'Diplôme d''Accès aux Études Universitaires option A', 'Diplôme d''Accès aux Études Universitaires option A', 0);
INSERT INTO `t_discipline` VALUES (133, '', 'DU Techniques multimédias', 'DU Techniques multimédias', 0);
INSERT INTO `t_discipline` VALUES (134, '', 'DU  Préparation au  concours Greffier en chef & Greffier', 'DU  Préparation au  concours Greffier en chef & Greffier', 0);
INSERT INTO `t_discipline` VALUES (135, '', 'Capacité en droit', 'Capacité en droit', 0);
INSERT INTO `t_discipline` VALUES (136, '', 'C2i - niveau 1', 'C2i - niveau 1', 0);
INSERT INTO `t_discipline` VALUES (137, '', 'DU Sciences de l''inadaptation et de la délinquance juvénile', 'DU Sciences de l''inadaptation et de la délinquance juvénile', 0);
INSERT INTO `t_discipline` VALUES (138, '', 'Doctorat Histoire', 'Doctorat Histoire', 0);
INSERT INTO `t_discipline` VALUES (139, '', 'Année Préparatoire à l''Insertion en Licence Sc.  MASS', 'Année Préparatoire à l''Insertion en Licence Sc.  MASS', 0);
INSERT INTO `t_discipline` VALUES (140, '', 'DU Technologies de l''Information et de la Communication', 'DU Technologies de l''Information et de la Communication', 0);
INSERT INTO `t_discipline` VALUES (141, '', 'CAPES HISTOIRE GEOGRAPHIE', 'CAPES HISTOIRE GEOGRAPHIE', 0);
INSERT INTO `t_discipline` VALUES (142, '', 'CAPES ANGLAIS', 'CAPES ANGLAIS', 0);
INSERT INTO `t_discipline` VALUES (143, '', 'Master Langue et Cult Rég', 'Master Langue et Cult Rég', 0);
INSERT INTO `t_discipline` VALUES (144, '', 'Préparation à l'' agrégation de lettres', 'Préparation à l'' agrégation de lettres', 0);
INSERT INTO `t_discipline` VALUES (145, '', 'DU METIERS DE L'' ADMINISTRATION GENERALE TERRITORIALE', 'DU METIERS DE L'' ADMINISTRATION GENERALE TERRITORIALE', 0);
