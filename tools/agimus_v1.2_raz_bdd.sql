/* REINITIALISATION AGIMUS */
/* ======================= */

/* décommenter le paragraphe désiré pour activer la réinitialisation */

/* Droits */
/* ------ */

/*
UPDATE `t_aut` SET `AUT_PWD` = 'bd7f7186e445e5b69beb1aaaa66a6310', `AUT_USER_LOGIN` = 'admin' WHERE `t_aut`.`INT_COD` = 1; /* [admin/agimus]
delete from `t_aut` WHERE int_cod > 1;
delete from `admin_right_acl` WHERE int_cod <>1;
TRUNCATE TABLE `appl_log_error`;
delete from `appl_modules_acl` WHERE int_cod <>1;
delete from `appl_user_group` WHERE int_cod <>1;
delete from `t_int` WHERE int_cod <>1;
delete from `gestflux_right_acl` WHERE int_cod <>1;
delete from `ref_right_acl` WHERE int_cod <>1;
delete from `restit_right_acl` WHERE int_cod <>1;
*/

/* Traces applicatives */
/* ------------------- */

/*
TRUNCATE TABLE `appl_session_log`;
TRUNCATE TABLE `appl_session_log_action`;
TRUNCATE `user_pwd_history`;
*/

/* Indicateurs, dénombrements  et structures */
/* ----------------------------------------- */

/*
TRUNCATE `r_denombrement`;
TRUNCATE `t_indic`;
TRUNCATE `t_struct` ;
*/

/* Référentiels nationaux (les identifiants 'autres' sont conservés) */
/* ---------------------------------------------------------------- */

/*
delete from `t_affiliation` WHERE id > 1;
delete from `t_diplome` WHERE id > 1;
delete from `t_discipline` WHERE id > 1;
delete from `t_dometu` WHERE id > 1;
*/

/* Référentiels internes (les identifiants 'autres' sont conservés) */
/* ---------------------------------------------------------------- */

delete from `t_affil_intra` WHERE id > 1;
delete from `t_application` WHERE id > 1;
delete from `t_dcpl_intra` WHERE id > 1;
delete from `t_diplome_intra` WHERE id > 1;
delete from `t_dometu_intra` WHERE id > 1;


/* Données de collecte et d'agregation */
/* ----------------------------------- */

TRUNCATE `t_agregat`;
TRUNCATE `t_agregat_calc`;
TRUNCATE `t_agregat_export`;
TRUNCATE `t_cnx`;
TRUNCATE `t_cnxlog`;
TRUNCATE `t_tracelog`;
TRUNCATE `t_traitement`;
TRUNCATE `t_usage`;
TRUNCATE `t_ut`;


