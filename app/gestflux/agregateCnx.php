<?php

/* traitement batch d'agregation dans le DWH */


if (isset($_SERVER['APPLDIR']))
{
	$applDir=trim($_SERVER['APPLDIR']);

	require_once( $applDir.'/app/common/required.php') ;
	require_once($applDir.'/app/common/library.php') ;
	require_once($applDir.'/app/main/access.php');
	require_once($applDir.'/app/common/traitement.php');

	$debug=false;
	// initialisation des variables et totaux de controle
	$msgString = '' ;
	$valueArray = array() ;
	$nbligred=0;
	$nbligwrited=0;
	$template="";
	$separator="";
	$no_arg=true;
	$param_a_utiliser="";

	// recuperation des parametres de batch

	if (isset($_SERVER["argc"]))
	{
		foreach($_SERVER["argv"] as $prm_value)
		{
			if ($param_a_utiliser!="")
			{
				switch ($param_a_utiliser) {
					case "-u" : $userId=$prm_value;break;
					case "-p" : $userPwd=$prm_value;break;
				}
				$param_a_utiliser="";
				$no_arg=false;
			}

			if (
			(strstr($prm_value,"-u"))
			or (strstr($prm_value,"-p"))
			)
			$param_a_utiliser=trim($prm_value);
		}
	}
	else
	{
		echo "\r\n Traitement impossible : paramètres non définis";
		exit(20);
	}

	if ($debug)
	echo "\r\nparametres lus : [$userId] | [".md5($userPwd)."] " ;

	$access = new Access($userId,$userPwd,DEFAULT_LANG_APPL) ;

	if($access->_verifAccess()==TRUE)
	{
		$username = $access->_getUsername() ;
		$uid = $access->_getUid() ;
		$lang = $access->_getLang() ;
	}else{
		die("\r\nAcces interdit !");
	}

	$rights = ModuleRights::_GetInstance() ;
	$codeRet=0;

	try{
		// creation des r�pertoires si inexistants
		if (!is_dir(ROOT_OUT))
		{if (!mkdir(ROOT_OUT)) die("ARRET DU TRAITEMENT : impossible de creer le repertoire ".ROOT_OUT);}
		chmod(ROOT_OUT,511); // equivalent 777 octal
		if (!is_dir(ROOT_LOGS))
		{if (!mkdir(ROOT_LOGS)) die("ARRET DU TRAITEMENT : impossible de creer le repertoire ".ROOT_LOGS);}
		chmod(ROOT_LOGS,511); // equivalent 777 octal

		if ($debug)
		echo "\r\nRepertoires crees";


		if ($debug)
		echo "\r\nvariables initialisees";


		// affichage de l'aide
		if ($no_arg)
		{
			$info_help="\r\n".NOM_APPL." - Agregation des lignes de collecte";
			$info_help.="\r\n=====================================";
			$info_help.="\r\n\r\nOptions :\r\n-u : UserId | -p : Password ";
			die ($info_help);
		}

		$date=strftime("%y/%m/%d");
		$datestamp=strftime("%y%m%d");
		$heurestamp=strftime("%H%M%S");
		$heure=strftime("%H:%M:%S");
		$stamp=$datestamp."_".$heurestamp;
		$logFile="agregate_cnx_$stamp.log";
		$logname=ROOT_LOGS."/".$logFile;
		$flog=fopen($logname,"w");

		fwrite($flog,NOM_APPL." - Agregation des lignes de collecte\r\n ");
		fwrite($flog,"=====================================\r\n ");
		fwrite($flog,"\r\nTraitement : du $date a $heure\r\n\r\n");

		//memorisation du traitement
		$trtDateDeb=strftime("%y-%m-%d %H:%M:%S");
		$newTrt=new Traitement();
		$newTrt->_setTrtCod("CALC_AGREGATE");
		$newTrt->_setTrtDateDeb($trtDateDeb);
		$newTrt->_setTrtStatus(10);
		$newTrt->_setTrtParams(implode(" -",$_SERVER["argv"]));
		$newTrt->_setTrtLog($logFile);
		$newTrt->_create();

		$erreur=false;

		// validation des param�tres
		if ((!no_args))
		{
			fwrite($flog,"\r\n---->>> TRAITEMENT INTERROMPU ! parametres incorrects !<<<----\r\n");
			if ($debug)
			echo "\r\nparametres incorrects " ;
			$erreur=20;
		}
		else
		{
			// lancement du traitement

			if ($debug)
			echo "\r\nLancement du traitement " ;


			if ($debug)
			echo "\r\nFichier de log ouvert " ;

			// ------------------------------------------------
			// TRAITEMENT D'agregation

			// lecture des lignes de connexion non traitees
			require_once( ROOT_CLASS."/cnx.php") ;
			
			//v1.2-PPR-28032011 ajout reduction volume de traitement
			
			$config=$_SESSION['config'];
			$seuil=10000;
			if (isset($config['seuil_decoupe_batch']))
				$seuil=$config['seuil_decoupe_batch'];

			$cnxToProcess=Cnx::_getNbToProcess();
			
			//v1.3-PPR 23052011 correctif
			if ($seuil>$cnxToProcess)
				$seuil=$cnxToProcess;

			fwrite($flog,"\r\nNombre de lignes à agréger : ".$cnxToProcess."\r\n");
			fwrite($flog,"\r\nSeuil de traitement par lot : ".$seuil."\r\n");

			$from=0;
			$to=$seuil;
			$lot=0;

			if ($debug)	echo "\r\nLancement de l'agregation " ;

			// nettoyage de la table t_Agregat_calc
			require_once( ROOT_CLASS."/agregat.php") ;
			Agregat::_purgeTempTable();

			while ($cnxToProcess>0) {
				$arCnxLines=Cnx::_getList(true," UID ASC, STAMP_START ASC, ID_APPLI ASC",0,$from,$to); // true : recuperation des lignes non trait�es uniquement
	
				fwrite($flog,"\r\n...traitement du lot n° $lot (".count($arCnxLines)." enreg.)");
				if (count($arCnxLines)>0)
				{
					//analyse les lignes de connexion
					$newAgregat=new Agregat();
					
					list($messAna,$erreur)=$newAgregat->_agregateCnx($arCnxLines);
					$mess.=$messAna;					
				}
				else
				{
					fwrite($flog,"\r\nAucune ligne de collecte a traiter !\r\n");
				}
				
				$lot+=1;
				$cnxToProcess=Cnx::_getNbToProcess();
			}

			if ($debug)
			echo "\r\nAgregation terminée ";
			fwrite($flog,"\r\nAnalyse et integration effectuees : ".$mess."\r\n");

		}

		if ($debug)
		echo "\r\nFin du traitement d'agregation " ;

		//	------------------------------------------------
		// totaux de controle
		fwrite($flog,"\r\n".str_repeat("-",40)."\r\n");

		$date=strftime("%y/%m/%d");
		$heure=strftime("%H:%M:%S");
		fwrite($flog,"\r\nFin de traitement : le $date a $heure.\r\n");

		if ($debug)
		echo "\r\nFin de traitement le $date a $heure. Lues : $nbligred - Ecrites : $nbligwrited";
		fclose($flog);

		// cloture du traitement
		$trtDateFin=strftime("%y-%m-%d %H:%M:%S");
		$newTrt->_setTrtDateFin($trtDateFin);
		if ($erreur>0)
		$newTrt->_setTrtStatus($erreur);
		else
		$newTrt->_setTrtStatus(30);
		$newTrt->_update();

		// envoi d'un mail de suivi d'exploitation
		if (SEND_MAIL_ACTIF)
		{
			//TODO : mail � envoyer
		}
		exit($erreur) ;

	}
	catch(MsgException $e){
		$msgString = $e->_getError($session) ;
		echo "\r\n Traitement impossible : erreur $msgString";
		exit(20);
	}
}
else
{
	echo "\r\n Traitement impossible : la variable APPLDIR n'est pas définie";
	exit(20);
}

