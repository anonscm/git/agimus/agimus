<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/gestflux/gestfluxSession.php') ;

//R�cup�ration de la session
$session = GestfluxSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(2, 1, $uid))&&($rights->_isActionAllowed('gestflux', 21, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		
		if(isset($_GET['action'])){
			$action = $_GET['action'] ;
		}

		require_once( "gestfluxSqlIo.php") ;
		$sqlIo = new GestfluxSqlIo($session, $rights,"import") ;

		try{
			if($action == 'import') {
	
				$arFiles=_analyseDir(ROOT_IN,$templateString=IMPORT_TEMPLATE_FILE);
				$importMess=$sqlIo->_importFiles(ROOT_IN,$arFiles);
				$msgString.=$importMess;

				$session->_sessionLogAction(1, '_IMPORT_TABLE_COMPLETED_') ;
				header('Location: '.ROOT_APPL.'/app/gestflux/sqlImport.php');
				exit() ;

			} elseif ($action == 'random') {
				require_once( DIR_WWW.ROOT_APPL.'/app/common/agregat.php') ;						 				
				$newAgregat=new Agregat();
				$newAgregat->_randomFill();
			}else{
				$sqlIo->_makeForm() ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'import'){
				$sqlIo->_makeForm() ;
			}
		}
		 
		$sqlIo->_makePage(ROOT_IN,IMPORT_TEMPLATE_FILE,$msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
