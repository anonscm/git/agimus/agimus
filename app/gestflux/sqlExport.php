<?php
/* traitement batch export agregat vers n+1 */
/* APPLDIR est defini dans le fichier batch */
$applDir=trim($_SERVER['APPLDIR']);
require_once( '../../app/common/required.php') ;
require_once( '../../app/gestflux/gestfluxSession.php') ;

//Recuperation de la session
$session = GestfluxSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(2, 1, $uid))&&($rights->_isActionAllowed('gestflux', 22, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;

		if(isset($_GET['action'])){
			$action = $_GET['action'] ;
		}

		require_once( "gestfluxSqlIo.php") ;
		$sqlIo = new GestfluxSqlIo($session, $rights) ;

		try{
			if($action == 'export') {


				/* suppression des fichiers plats présetns dans /out */
				filesDirDelete(ROOT_OUT,"sql");
				
				//TODO : a placer en form
				$dateStartSelect="0000-00-00";

				/* mise en forme agregat pour export (suppression des dimensions intra) */
				
				require_once( "../../app/common/agregat.php") ;
				$arCpt=Agregat::_agregateForSupLevel();
				
				/* export des agregats */
				$sqlString=" WHERE DATE_CRE >= '".$dateStartSelect."' ";
				$config=$_SESSION['config'];
				$sqlIo->outputFileName=ROOT_OUT."/agregat_".$config["id_struct"]."_".date('Ymd_His').".sql";
				$sqlIo->_exportTable('t_agregat_export','t_agregat',false,true,'insert','','REPLACE',false,false,$sqlString,$arDimsToExport,$arDimsToRaz);
				// TODO : mettre a jour la date export

				/* export des unités de temps */
				$dateTab=explode("-",$dateStartSelect);
				$stampStartSelect=mktime(0,0,0,$dateTab[1], $dateTab[2], $dateTab[0]);
				$sqlString=" WHERE ID >= '".$stampStartSelect."' ";
				$config=$_SESSION['config'];
				$sqlIo->outputFileName=ROOT_OUT."/ut_".$config["id_struct"]."_".date('Ymd_His').".sql";
				$sqlIo->_exportTable('t_ut','t_ut',false,true,'insert','','REPLACE',false,false,$sqlString);

				/* export des référentiels associés */
				$sqlString="";
				$config=$_SESSION['config'];

				$sqlIo->outputFileName=ROOT_OUT."/struct_".$config["id_struct"]."_".date('Ymd_His').".sql";
				$sqlIo->_exportTable('t_struct','t_struct',false,true,'insert','','REPLACE',false,false,$sqlString);

				$sqlIo->outputFileName=ROOT_OUT."/denombr_".$config["id_struct"]."_".date('Ymd_His').".sql";
				$sqlIo->_exportTable('r_denombrement','r_denombrement',false,true,'insert','','REPLACE',false,false,$sqlString,$arDimsToExport,$arDimsToRaz);

				$session->_sessionLogAction(1, '_EXPORT_TABLE_COMPLETED_') ;
				header('Location: '.ROOT_APPL.'/app/gestflux/sqlExport.php');
				exit() ;

			}elseif($action == 'post') {

				// transmission des données
				$config=$_SESSION['config'];
				require("../../app/common/structure.php");
				$newStruct= new Structure($config["id_struct"]);
				$structUrlExport=$newStruct->_getUrlExport();

				if ($structUrlExport!="")
				{
					// file list
					$arFiles=_analyseDir(ROOT_OUT,"");
					if (count($arFiles)>0)
					{
						foreach($arFiles as $curFile)
						{

							//$fileContents="agregat_PAU461.sql\r\n";
							$fileHandle = fopen(ROOT_OUT.'/'.$curFile, "rb");
							$fileContents.= stream_get_contents($fileHandle);
							fclose($fileHandle);

							$params = array(
      				'http' => array
							('method' => 'POST',
         		 'header'=>"Content-Type: multipart/form-data\r\n",
          		'content' => $fileContents
							)
							);

							$url = $structUrlExport."/app/gestflux/importFiles.php";
							$ctx = stream_context_create($params);
							$fp = fopen($url, 'rb', false, $ctx);
							$response=stream_get_contents($fp);
							if (!$response)
							echo "ERREUR DANS L'ENVOI DES DONNEES : $response";
							fclose($fp);
							// suppression du fichier d'export
							unlink(ROOT_OUT.'/'.$curFile);
						}
					}
				}
				else
				{
					echo "Url export inconnue ! Transfert impossible ";
				}
			}

			else{
				$sqlIo->_makeForm() ;
			}
		}
		catch(MsgException $e)
		{
			$msgString = $e->_getError($session) ;

			if($action == 'export')
			{
				$sqlIo->_makeForm() ;
			}
		}
		$sqlIo->_makePage(ROOT_OUT,"agregat",$msgString) ;
	}
}

catch(MsgException $e)
{
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
