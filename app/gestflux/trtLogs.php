<?php
/* traitement batch integration des logs de connexion */
/* APPLDIR est defini dans le fichier batch */

/* v1.2-PPR-28032011 amélioration de la gestion du code retour */

$applDir=trim($_SERVER['APPLDIR']);

require_once( $applDir.'/app/common/required.php') ;
require_once($applDir.'/app/common/library.php') ;
require_once($applDir.'/app/main/access.php');
require_once($applDir.'/app/common/traitement.php');

$debug=true;
// initialisation des variables et totaux de controle
$msgString = '' ;
$valueArray = array() ;
$nbligred=0;
$nbligwrited=0;
$template="";
$separator="";
$path="";
$no_arg=true;
$param_a_utiliser="";

// recuperation des parametres de batch

if (isset($_SERVER["argc"]))
{
	foreach($_SERVER["argv"] as $prm_value)
	{
		if ($param_a_utiliser!="")
		{
			switch ($param_a_utiliser) {
				case "-u" : $userId=$prm_value;break;
				case "-p" : $userPwd=$prm_value;break;
				case "-tpl" : $template=$prm_value;break;
				case "-sep" : $separator=$prm_value;break;
				case "-path": $path=$prm_value;break;
			}
			$param_a_utiliser="";
			$no_arg=false;
		}

		if (
		(strstr($prm_value,"-u"))
		or (strstr($prm_value,"-p"))
		or (strstr($prm_value,"-tpl"))
		or (strstr($prm_value,"-sep"))
		or (strstr($prm_value,"-path"))
		)
		$param_a_utiliser=trim($prm_value);
	}
}

if ($debug)
echo "\r\nparametres lus : [$template] | [$separator] | [$userId] | [".md5($userPwd)."] | [".$path."] " ;

$access = new Access($userId,$userPwd,DEFAULT_LANG_APPL) ;

if($access->_verifAccess()==TRUE)
{
	$username = $access->_getUsername() ;
	$uid = $access->_getUid() ;
	$lang = $access->_getLang() ;
}else{
	die("\r\nAcces interdit !");
}

$rights = ModuleRights::_GetInstance() ;
$codeRet=0;

try{
	//if(($rights->_isModuleAllowed(2, 1, $uid))&&($rights->_isActionAllowed('gestflux', 20, $uid, '_FONCTION_NOT_ALLOWED_')))
	{

		try{
			// creation des repertoires si inexistants
			if (!is_dir(ROOT_OUT))
			{if (!mkdir(ROOT_OUT)) die("ARRET DU TRAITEMENT : impossible de creer le repertoire ".ROOT_OUT);}
			chmod(ROOT_OUT,511); // equivalent 777 octal
			if (!is_dir(ROOT_LOGS))
			{if (!mkdir(ROOT_LOGS)) die("ARRET DU TRAITEMENT : impossible de creer le repertoire ".ROOT_LOGS);}
			chmod(ROOT_LOGS,511); // equivalent 777 octal

			if ($debug)
			echo "\r\nRepertoires crees";


			if ($debug)
			echo "\r\nvariables initialisees";


			// affichage de l'aide
			if ($no_arg)
			{
				$info_help="\r\n".NOM_APPL." - Import des logs de connexion";
				$info_help.="\r\n=====================================";
				$info_help.="\r\n\r\nOptions :\r\n-tpl : Nom du fichier de log ex : -obj access ";
				$info_help.="\r\n-u : UserId | -p : Password ";
				$info_help.="\r\n-sep : Separateur ex : -sep \" \" ";
				$info_help.="\r\n-sep : Path ex : '/2010/06/21' \":\" ";
				die ($info_help);
			}

			$date=strftime("%y/%m/%d");
			$datestamp=strftime("%y%m%d");
			$heurestamp=strftime("%H%M%S");
			$heure=strftime("%H:%M:%S");
			$stamp=$datestamp."_".$heurestamp;
			$logFile="import_cnx_log_$stamp.log";
			$logname=ROOT_LOGS."/".$logFile;
			$flog=fopen($logname,"w");

			fwrite($flog,NOM_APPL." - Import des logs de connexion\r\n ");
			fwrite($flog,"=====================================\r\n ");
			fwrite($flog,"\r\nTraitement : du $date a $heure\r\n");
			fwrite($flog,"\r\nTemplate : [".$template."] ");
			fwrite($flog,"\r\nSeparator : [".$separator."] ");
			fwrite($flog,"\r\nPath : [".$path."] \r\n");

			//memorisation du traitement
			$trtDateDeb=strftime("%y-%m-%d %H:%M:%S");
			$newTrt=new Traitement();
			$newTrt->_setTrtCod("IMPORT_CNX_LOGS");
			$newTrt->_setTrtDateDeb($trtDateDeb);
			$newTrt->_setTrtStatus(10);
			$newTrt->_setTrtParams(implode(" -",$_SERVER["argv"]));
			$newTrt->_setTrtLog($logFile);
			$newTrt->_create();

			// validation des parametres
			if ((!$no_arg) and ((trim($template)=="") or trim($separator=="")))
			{
				fwrite($flog,"\r\n---->>> TRAITEMENT INTERROMPU ! parametres incorrects !<<<----\r\n");
				if ($debug)
				echo "\r\nparametres incorrects " ;
				$codeRet=20;

			}
			else
			{
				// lancement du traitement

				if ($debug)
				echo "\r\nLancement du traitement " ;



				if ($debug)
				echo "\r\nFichier de log ouvert " ;

				// ------------------------------------------------
				// TRAITEMENT D'ANALYSE ET D'INTEGRATION

				if ((ROOT_IN.$path=="") or (!is_dir(ROOT_IN.$path)))
				{
					fwrite($flog,"\r\nTraitement stoppé : répertoire [$path] incorrect !\r\n");
					$codeRet=20;
				}
				else
				{
					//v1.1-PPR-03012011 intégration gestion multilogs et fusion traitements analyse et collecte

						//Alimente la table de collecte (cnx)
						require_once( ROOT_CLASS."/cnx.php") ;
						$cnx=new Cnx();
						
						list($messAlim,$codeRet)=$cnx->_analyzeAndGenCnx($template,$separator,$path);

						fwrite($flog,"\r\nAlimentation terminee :".$messAlim."\r\n");
				}
			}

			if ($debug)
			echo "\r\nFin du traitement d'analyse et d'import " ;

			//	------------------------------------------------
			// totaux de controle
			fwrite($flog,"\r\n".str_repeat("-",40)."\r\n");
			//fwrite($flog,"Lignes lues : $nbligred\r\n");
			//fwrite($flog,"Lignes rejetees : $nbligrej\r\n");
			//fwrite($flog,"Lignes ecrites : $nbligwrited\r\n");

			$date=strftime("%y/%m/%d");
			$heure=strftime("%H:%M:%S");
			fwrite($flog,"\r\nFin de traitement : le $date a $heure.\r\n");

			if ($debug)
			echo "\r\nFin de traitement le $date a $heure. Lues : $nbligred - Ecrites : $nbligwrited";
			fclose($flog);

			// cloture du traitement
			$trtDateFin=strftime("%y-%m-%d %H:%M:%S");
			$newTrt->_setTrtDateFin($trtDateFin);
			$newTrt->_setTrtStatus($codeRet);
			$newTrt->_update();

			// envoi d'un mail de suivi d'exploitation
			//v1.1 inactif pour l'instant
			//if (SEND_MAIL_ACTIF)
			//{
			//}
			//$session->_sessionLogAction(1, '_LOG_ANALYSE_COMPLETED_') ;
			exit($codeRet) ;

		}
		catch(MsgException $e){
			exit(20);
		}
	}
}
catch(MsgException $e){
	exit(20);
}
