<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/traitement.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/hdate.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/indicXmlIO.php') ;

/**
 *
 * <p>Traitement</p>
 *
 * @name Traitement
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class GestfluxTraitement extends Traitement {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des Traitements
	 */
	private $TraitementArray ;

	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>creation de l'instance de la classe</p>
	 *
	 * @name Traitement::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'traitementlist.xtpl' ;
		$this->form_file = 'traitementform.xtpl' ;
		$this->result_file = 'traitementresult.xtpl' ;
		$this->xtpl_path = $this->lang.'/gestflux/'  ;
		$this->TraitementArray = $this->_getList() ;
	}

	/**
	 * Verification des variables recues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refTraitement::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;

	}

	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refTraitement::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
		Traitement::_setId($valueArray['traitement_id']) ;
		Traitement::_setTrtCod($valueArray['trt_cod']) ;
		Traitement::_setTrtDateDeb($valueArray['trt_date_deb']) ;
		Traitement::_setTrtDateFin($valueArray['trt_date_fin']) ;
		Traitement::_setTrtParams($valueArray['trt_params']) ;
		Traitement::_setTrtStatus($valueArray['trt_status']) ;
		Traitement::_setTrtLog($valueArray['trt_log']) ;
	}

	/**
	 * Insertion des donn�es dans le mod�le de pag
	 *
	 * <p>_makePage</p>
	 *
	 * @name Traitement::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($jobId=0, $msg='')
	{
		global $arTrtStatus;

		$config=$_SESSION['config'];

		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
	 	$xtpl->assign('MSG', $msg) ;
	 	$xtpl->parse('main.msg');
		}

		// collecte des dénombrements par table
		require_once( DIR_WWW.ROOT_APPL.'/app/common/cnxLog.php');
		$nbCnxLogToProcess=CnxLog::_getNbToProcess();
	 	$xtpl->assign('NB_CNXLOG_TO_PROCESS', $nbCnxLogToProcess) ;
	 	
		require_once( DIR_WWW.ROOT_APPL.'/app/common/cnx.php');
		$nbCnxToProcess=Cnx::_getNbToProcess();
	 	$xtpl->assign('NB_CNX_TO_PROCESS', $nbCnxToProcess) ;
	 	
		require_once( DIR_WWW.ROOT_APPL.'/app/common/agregat.php');
		$nbAgregatToExport=Agregat::_getNbToExport();
	 	$xtpl->assign('NB_AGREGAT_TO_EXPORT', $nbAgregatToExport) ;

	 	$xtpl->parse('main.numbers');
		

		foreach($this->TraitementArray as $key=>$value)
		{
			$xtpl->assign('TRAITEMENT_ID', $key);
			$xtpl->assign('TRT_COD', $value['TRT_COD']);
			$xtpl->assign('TRT_DATE_DEB', $value['TRT_DATE_DEB']);
			$xtpl->assign('TRT_DATE_FIN', $value['TRT_DATE_FIN']);
			$xtpl->assign('TRT_STATUS', $arTrtStatus[$value['TRT_STATUS']]);
			
			if (file_exists(DIR_WWW."/".PATH_LOGS."/".$value['TRT_LOG']))
			{
			$xtpl->assign('TRT_LOG', PATH_LOGS."/".$value['TRT_LOG']);
				$xtpl->parse('main.list.row.logexists');
			}	
			if(($key != $jobId)&&($this->rights->_isActionAllowed('gestflux', 25, $this->uid)))
			{
				$xtpl->parse('main.list.row.modif');
			}

			if(($key != $jobId)&&($this->rights->_isActionAllowed('gestflux', 24, $this->uid)))
			{
				$xtpl->parse('main.list.row.delete');
			}
			$xtpl->parse('main.list.row');
		}
		$xtpl->parse('main.list');

		//Condenombrion du menu en fonction des droits de l'utilisateur

		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}
		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}


	/**
	 * G�n�ration du formulaire de saisie
	 *
	 * <p>_makeForm</p>
	 *
	 * @name Traitement::_makeForm()
	 * @param $job_id (int)
	 * @param $valueArray (array)
	 * @param $doc_ref (Int)
	 * @param $docVal (array)
	 * @return string
	 */
	public function _makeForm($job_id=0, $valueArray=array(), $doc_ref=0, $docVal = array())
	{
		global $arTrtStatus;

		$form = new Form('../lst_traitement.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);

		if(($valueArray == NULL)&&($job_id==0))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}

		if($valueArray != NULL)
		$valueArray=keyToUpper($valueArray);

		if($valueArray != NULL)
		{
			$job_id = $valueArray['ID'] ;
			$trt_cod = $valueArray['TRT_COD'] ;
			$trt_date_deb = $valueArray['TRT_DATE_DEB'] ;
			$trt_date_fin = $valueArray['TRT_DATE_FIN'] ;
			$trt_params = $valueArray['TRT_PARAMS'] ;
			$trt_status = $valueArray['TRT_STATUS'] ;
			$trt_log = $valueArray['TRT_LOG'] ;

		}else{
			if($job_id!=0)
			{
				$job_id = $this->TraitementArray[$job_id]['ID'] ;
				$trt_cod = $this->TraitementArray[$job_id]['TRT_COD'] ;
				$trt_date_deb = $this->TraitementArray[$job_id]['TRT_DATE_DEB'] ;
				$trt_date_fin = $this->TraitementArray[$job_id]['TRT_DATE_FIN'] ;
				$trt_params = $this->TraitementArray[$job_id]['TRT_PARAMS'] ;
				$trt_status = $this->TraitementArray[$job_id]['TRT_STATUS'] ;
				$trt_log = $this->TraitementArray[$job_id]['TRT_LOG'] ;

			}else{
				$job_id ='' ;
				$trt_cod = '' ;
				$trt_date_deb = '' ;
				$trt_date_fin = '' ;
				$trt_params = '' ;
				$trt_status = '' ;
				$trt_log = '' ;
			}
		}

		$date=new HDate();
		$xtplform->assign('TRAITEMENT_ID', $form->_mkInput('hidden', 'job_id', $job_id)) ;
		//TODO A terminer

		$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('validate', 'Enregistrer' ) );

		$xtplform->parse('main.link_emptyform');

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Traitement::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>