<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/cnxLog.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;

/**
 *
 * <p>Societe</p>
 *
 * @name Societe
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class GestfluxCnxLog extends CnxLog {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des logs
	 */
	private $cnxLogArray ;
	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $resultString(String)
	 * @desc resultat du traitement d'analyse
	 */
	private $resultString ;

	/**
	 * @var $filesString(String)
	 * @desc liste des fichiers de log dispo pour traitement
	 */
	private $filesString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Structure::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'cnxLoglist.xtpl' ;
		$this->form_file = 'cnxLogform.xtpl' ;
		$this->xtpl_path = $this->lang.'/gestflux/'  ;
		$this->cnxLogArray = array() ;
	}


	/**
	 * V�rification des variables re�ues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refStruct::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray)
	{
	}

	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refStruct::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
	}

	/**
	 * Insertion des donn�es dans le mod�le de pag
	 *
	 * <p>_makePage</p>
	 *
	 * @name Structure::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($cnxLogId=0, $msg='')
	{
		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
	 	$xtpl->assign('MSG', $msg) ;
	 	$xtpl->parse('main.msg');
		}
		$xtpl->parse('main.list');
		//Construction du menu en fonction des droits de l'utilisateur

		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}

		// test exp. rationelle
		$string ='TRACE-81283-M62pcjbMa2ZQ2aIjqWCU5DrHe0uEsbemMg1kxcjEEpx3LrvPfJ-sso.univ-pau.fr ';
		$string .='82.243.195.101 - - 757630 [09/Sep/2010:00:00:00 +0200] ';
		$string .='"POST /horde/dimp/imp.php/PollFolders HTTP/1.1" 200 307 ';
		$string .='"https://webmail.univ-pau.fr/horde/dimp/?frameset_loaded=1" "Mozilla/5.0 ';
		$string .='(Windows; U; Windows NT 6.0; fr; rv:1.9.2.6) Gecko/20100625 Firefox/3.6.6 (.NET CLR 3.5.30729)" ';

		//$logEntryPattern = "^(\S+) ([\d.]+) (\S+) (\S+) \[([\w:/]+\s[+\-]\d{4})\] \"(.+?)\" (\d{3}) (\d+) \"([^\"]+)\" \"([^\"]+)\"";
		$logEntryPattern = "#[\"*]#";
		$arSplit=preg_split($logEntryPattern,$string);
		//print_r($arSplit);
		//else echo "impossible de parser la ligne ! ";
		
		if($this->resultString != '')
		{
			$xtpl->assign('ANA_RESULT', $this->resultString);
			$xtpl->parse('main.result');
		}

		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}

	public function _makeForm($cnxLog_id=0, $valueArray=array(), $doc_ref=0, $docVal = array())
	{
		$form = new Form('../trtLogs.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	public function _makeResult()
	{

		$resStr="";
		foreach($this->cnxLogArray as $curFile=>$curLogFile)
		{
			$xtplresult = new XTemplate($this->result_file, $this->xtpl_path);
				
		}
		$xtplresult->parse('main');
		$this->resultString =$xtplresult->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Structure::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>