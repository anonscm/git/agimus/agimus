<?php

/* traitement batch d'agregation dans le DWH */


if (isset($_SERVER['APPLDIR']))
{
	$applDir=trim($_SERVER['APPLDIR']);

	require_once( $applDir.'/app/common/required.php') ;
	require_once($applDir.'/app/common/library.php') ;
	require_once($applDir.'/app/main/access.php');
	require_once($applDir.'/app/common/traitement.php');

	$debug=false;
	// initialisation des variables et totaux de controle
	$msgString = '' ;
	$valueArray = array() ;
	$nbligred=0;
	$nbligwrited=0;
	$template="";
	$separator="";
	$no_arg=true;
	$param_a_utiliser="";

	// recuperation des parametres de batch

	if (isset($_SERVER["argc"]))
	{
		foreach($_SERVER["argv"] as $prm_value)
		{
			if ($param_a_utiliser!="")
			{
				switch ($param_a_utiliser) {
					case "-u" : $userId=$prm_value;break;
					case "-p" : $userPwd=$prm_value;break;
				}
				$param_a_utiliser="";
				$no_arg=false;
			}

			if (
			(strstr($prm_value,"-u"))
			or (strstr($prm_value,"-p"))
			)
			$param_a_utiliser=trim($prm_value);
		}
	}
	else
	{

	}

	if ($debug)
	echo "\r\nparametres lus : [$userId] | [".md5($userPwd)."] " ;

	$access = new Access($userId,$userPwd,DEFAULT_LANG_APPL) ;

	if($access->_verifAccess()==TRUE)
	{
		$username = $access->_getUsername() ;
		$uid = $access->_getUid() ;
		$lang = $access->_getLang() ;
	}else{
		die("\r\nAcces interdit !");
	}

	$rights = ModuleRights::_GetInstance() ;
	$codeRet=0;

	try{
		//if(($rights->_isModuleAllowed(2, 1, $uid))&&($rights->_isActionAllowed('gestflux', 20, $uid, '_FONCTION_NOT_ALLOWED_')))
		{

			try{
				// creation des repertoires si inexistants
				if (!is_dir(ROOT_OUT))
				{if (!mkdir(ROOT_OUT)) die("ARRET DU TRAITEMENT : impossible de creer le repertoire ".ROOT_OUT);}
				chmod(ROOT_OUT,511); // equivalent 777 octal
				if (!is_dir(ROOT_LOGS))
				{if (!mkdir(ROOT_LOGS)) die("ARRET DU TRAITEMENT : impossible de creer le repertoire ".ROOT_LOGS);}
				chmod(ROOT_LOGS,511); // equivalent 777 octal

				if ($debug)
				echo "\r\nRepertoires crees";


				if ($debug)
				echo "\r\nvariables initialisees";


				// affichage de l'aide
				if ($no_arg)
				{
					$info_help="\r\n".NOM_APPL." - Agregation finale ";
					$info_help.="\r\n=====================================";
					$info_help.="\r\n\r\nOptions :\r\n-u : UserId | -p : Password ";
					die ($info_help);
				}

				$date=strftime("%y/%m/%d");
				$datestamp=strftime("%y%m%d");
				$heurestamp=strftime("%H%M%S");
				$heure=strftime("%H:%M:%S");
				$stamp=$datestamp."_".$heurestamp;
				$logFile="final_agregate_$stamp.log";
				$logname=ROOT_LOGS."/".$logFile;
				$flog=fopen($logname,"w");

				fwrite($flog,NOM_APPL." - Agregation finale\r\n ");
				fwrite($flog,"=====================================\r\n ");
				fwrite($flog,"\r\nTraitement : du $date a $heure\r\n\r\n");

				//memorisation du traitement
				$trtDateDeb=strftime("%y-%m-%d %H:%M:%S");
				$newTrt=new Traitement();
				$newTrt->_setTrtCod("FINAL_AGREGATE");
				$newTrt->_setTrtDateDeb($trtDateDeb);
				$newTrt->_setTrtStatus(10);
				$newTrt->_setTrtParams(implode(" -",$_SERVER["argv"]));
				$newTrt->_setTrtLog($logFile);
				$newTrt->_create();

				$erreur=false;

				// validation des parametres
				if ((!no_args))
				{
					fwrite($flog,"\r\n---->>> TRAITEMENT INTERROMPU ! parametres incorrects !<<<----\r\n");
					if ($debug)
					echo "\r\nparametres incorrects " ;
					$erreur=20;
				}
				else
				{
					// lancement du traitement

					if ($debug)
					echo "\r\nLancement du traitement " ;


					if ($debug)
					echo "\r\nFichier de log ouvert " ;

					// ------------------------------------------------
					// TRAITEMENT D'agregation

					require_once( ROOT_CLASS."/agregat.php") ;
					$newAgregat=new Agregat();

					list($messAna,$erreur)=$newAgregat->_finalAgregation();
					fwrite($flog,"\r\nAgregation finale effectuee : ".$messAna."\r\n");
				}

				if ($debug)
				echo "\r\nFin du traitement d'agregation " ;

				//	------------------------------------------------
				// totaux de controle
				fwrite($flog,"\r\n".str_repeat("-",40)."\r\n");

				$date=strftime("%y/%m/%d");
				$heure=strftime("%H:%M:%S");
				fwrite($flog,"\r\nFin de traitement : le $date a $heure.\r\n");
				fclose($flog);

				// cloture du traitement
				$trtDateFin=strftime("%y-%m-%d %H:%M:%S");
				$newTrt->_setTrtDateFin($trtDateFin);
				if ($erreur>0)
				$newTrt->_setTrtStatus($erreur);
				else
				$newTrt->_setTrtStatus(30);
				$newTrt->_update();

				// envoi d'un mail de suivi d'exploitation
				if (SEND_MAIL_ACTIF)
				{
					//TODO : mail a envoyer
				}
				//$session->_sessionLogAction(1, '_LOG_ANALYSE_COMPLETED_') ;
				exit($codeRet) ;

			}
			catch(MsgException $e){
				exit(20);
			}
		}
	}
	catch(MsgException $e){
		exit(20);
	}

}


else


{
	// version Online
	require_once( '../../app/common/required.php') ;
	require_once( DIR_WWW.ROOT_APPL.'/app/gestflux/gestfluxSession.php') ;

	//Recuperation de la session
	$session = GestfluxSession::_GetInstance() ;
	$uid = $session->_getUid() ;
	$rights = ModuleRights::_GetInstance() ;
	$codeRet=0;
	$result="";

	try{
		if(($rights->_isModuleAllowed(2, 1, $uid))&&($rights->_isActionAllowed('gestflux', 23, $uid, '_FONCTION_NOT_ALLOWED_')))
		{

			try{
				// creation des repertoires si inexistants
				if (!is_dir(ROOT_OUT))
				{if (!mkdir(ROOT_OUT)) die("ARRET DU TRAITEMENT : impossible de creer le repertoire ".ROOT_OUT);}
				chmod(ROOT_OUT,511); // equivalent 777 octal
				if (!is_dir(ROOT_LOGS))
				{if (!mkdir(ROOT_LOGS)) die("ARRET DU TRAITEMENT : impossible de creer le repertoire ".ROOT_LOGS);}
				chmod(ROOT_LOGS,511); // equivalent 777 octal

				if ($debug)
				$result.="<br>Repertoires crees";


				if ($debug)
				$result.="<br>variables initialisees";

				// lancement du traitement

				if ($debug)
				$result.="<br>Lancement du traitement " ;

				$date=strftime("%y/%m/%d");
				$datestamp=strftime("%y%m%d");
				$heurestamp=strftime("%H%M%S");
				$heure=strftime("%H:%M:%S");
				$stamp=$datestamp."_".$heurestamp;
				$logname=ROOT_LOGS."/agregate_cnx_$stamp.log";
				$flog=fopen($logname,"w");

				// ------------------------------------------------
				// TRAITEMENT D'agregation


				require_once( ROOT_CLASS."/agregat.php") ;
				$newAgregat=new Agregat();

				list($messAna,$codeRet)=$newAgregat->_finalAgregation();

				if ($debug)
				$result.="<br>Fin du traitement d'agregation " ;

				//	------------------------------------------------
				// totaux de controle
				$result.="<br>Lignes lues : $nbligred";
				$result.="<br>Lignes ecrites : $nbligwrited";

				// envoi d'un mail de suivi d'exploitation
				if (SEND_MAIL_ACTIF)
				{
					//TODO : mail a envoyer
				}
				$session->_sessionLogAction(1, '_LOG_ANALYSE_COMPLETED_') ;

				echo str_replace("\r\n","<br/>",$result) ;

			}
			catch(MsgException $e){
				$msgString = $e ->_getError($session) ;
				$session->_makeErrorPage($msgString) ;
			}
		}
	}
	catch(MsgException $e){
		$msgString = $e ->_getError($session) ;
		$session->_makeErrorPage($msgString) ;
	}
}
