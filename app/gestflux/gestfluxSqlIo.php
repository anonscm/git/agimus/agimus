<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/mysqlIO.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;

/**
 *
 * <p>GestfluxSqlIo</p>
 *
 * @name GestfluxSqlIo
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class GestfluxSqlIo extends MysqlIO {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $resultString(String)
	 * @desc resultat du traitement d'analyse
	 */
	private $resultString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Structure::__construct()
	 * @return void
	 */
	public function __construct($session, $rights,$typeIo="export") {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		if ($typeIo=="import")
		{
			$this->xtpl_file = 'sqlImport.xtpl' ;
			$this->form_file = 'sqlImportForm.xtpl' ;
		}
		else
		{
			$this->xtpl_file = 'sqlExport.xtpl' ;
			$this->form_file = 'sqlExportForm.xtpl' ;
		}
		$this->xtpl_path = $this->lang.'/gestflux/'  ;
	}

	/**
	 * Analyse du r�pertoire sas
	 *
	 * <p>Analyse du r�pertoire sas</p>
	 *
	 * @name cnxLog::_analyseLogDir()
	 * @param $templateString (string)
	 * @param $separator (string)
	 * @return array
	 */
	public function _analyseDir($fileDir,$templateString="") {

		$arFiles=array();

		if ($handle = opendir($fileDir)) {

			/* parcours du sas et lecture fichier par fichier */
			while (false !== ($file= readdir($handle))) {

				if ($file!=="." and $file !== ".." ) {

					// filtrage sur les fichiers sql uniquement
					if (strrchr($file,'.')=='.sql')
					{
						// contr�le que le nom du fichier correspond � la recherche
						if (($templateString=="") or (strtoupper(substr($file,0,strlen($templateString)))==strtoupper($templateString))) {
							$arFiles[]=$file;
						}
					}
				}
			}
		}
		return $arFiles;
	}

	/**
	 * V�rification des variables re�ues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refStruct::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray)
	{
	}

	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refStruct::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
	}

	/**
	 * Insertion des donn�es dans le mod�le de page
	 *
	 * <p>_makePage</p>
	 *
	 * @name Structure::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($fileDir,$templateString,$msg='')
	{
		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
	 	$xtpl->assign('MSG', $msg) ;
	 	$xtpl->parse('main.msg');
		}
		$xtpl->parse('main.list');
		//Construction du menu en fonction des droits de l'utilisateur

		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}

		// file list
		$arFiles=_analyseDir($fileDir,$templateString);
		if (count($arFiles)>0)
		{
			foreach($arFiles as $curFile)
			{
				$xtpl->assign('FILE_LINK',$curFile);
				$xtpl->assign('FILE_NAME',$curFile);
				$xtpl->parse('main.filelist.fileitem');
			}
			$xtpl->parse('main.filelist');
		}

		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}

	public function _makeForm($cnxLog_id=0, $valueArray=array(), $doc_ref=0, $docVal = array())
	{
		$form = new Form('../sqlExport.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}


	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Structure::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>