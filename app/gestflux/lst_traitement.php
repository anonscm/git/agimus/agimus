<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../gestflux/gestfluxSession.php') ;

//Recuperation de la session
$session = GestfluxSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(2, 1, $uid))&&($rights->_isActionAllowed('gestflux', 23, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['traitement_id'])){
			$traitementId = $_GET['traitement_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['traitement_id'])){
			$traitementId = $_POST['traitement_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$traitementId = 0 ;
			$action = 'create' ;
		}



		require_once( 'gestfluxTraitement.php') ;
		$traitement = new gestfluxTraitement($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('gestflux', 24, $uid, '_JOB_DELETE_NOT_ALLOWED_'))
				{
					$traitement->_delete($traitementId) ;
					$session->_sessionLogAction(1, '_JOB_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/gestflux/lst_traitement.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('gestflux', 25, $uid, '_JOB_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donn�es dans le formulaire
					$traitement->_makeForm($traitementId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($traitementId == 0)&&($rights->_isActionAllowed('gestflux', 26, $uid, '_JOB_CREATE_NOT_ALLOWED_')))
				{
					$traitement->_checkFormValues($_POST) ; 
					$traitement->_setFormValues($_POST) ;
						$traitement->_create() ;
						$session->_sessionLogAction(1, '_JOB_CREATE_') ;
				}
				if(($traitementId != 0)&&($rights->_isActionAllowed('gestflux', 25, $uid, '_JOB_UPDATE_NOT_ALLOWED_')))
				{
					$traitement->_checkFormValues($_POST) ;
					$traitement->_setFormValues($_POST) ;
					$traitement->_update() ;
					$session->_sessionLogAction(1, '_JOB_UPDATE_') ;
					header('Location: '.ROOT_APPL.'/app/gestflux/lst_traitement.php?traitement_id='.$traitementId);
					exit() ;
				}
				header('Location: '.ROOT_APPL.'/app/gestflux/lst_traitement.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('gestflux', 25, $uid))
				{
					$traitement->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$traitementId=0 ;
				if($rights->_isActionAllowed('gestflux', 26, $uid))
				{
					$traitement->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$traitement->_makeForm($traitementId, $valueArray, $docref, $docVal) ;
			}
		}

		$traitement->_makePage($traitementId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
