<?php
require_once( '../../app/common/required.php') ;

/* Suppression des fichiers import présents */
filesDirDelete(ROOT_IN,"sql");

/* Récupération des données transmises par "POST" */
$_print=file_get_contents('php://input');

/* OUverture et ecriture du buffer dans un nouveau fichier d'import */
$fp=fopen(ROOT_IN."/importData_".date('Ymd_His').".sql","w");
fwrite($fp,$_print);
fclose($fp);

echo true;

?>