<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/session.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/cnxLog.php') ;

/**
 * Gestion des sessions dans le module gestflux
 * <p>GestfluxSession</p>
 * 
 * @name GestfluxSession
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright Capella Conseil 2009
 * @version 1.0.0
 * @package gestflux
 */
 
 class GestfluxSession extends Session {
 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. attributs     */
    /*~*~*~*~*~*~*~*~*~*~*/
 	/**
    * @var (Singleton)
    * @desc variable pour le singleton
    */
 	private static $instance;
 	    
    /**
    * @var $menu_file (String)
    * @desc fichier template du menu vertical
    */
    private $menu_file ;
    
    /**
    * @var $xtpl_path (String)
    * @desc chemin des templates
    */   
    private $xtpl_path ;

    /**
    * @var $module (String)
    * @desc module
    */  
    private $module ;
    
    /**
    * @var (Array)
    * @desc tableau des objets du module gestflux
    */
    private $gestfluxArray  ;
        
    
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name GestfluxSession::__construct()
    * @return void 
    */
    public function __construct() {
    	parent::__construct();
    	$this->module = 'gestflux' ;
    	$this->menu_file = 'gestfluxmenuleft.xtpl' ; 
    	$this->xtpl_path = Session::_getLang().'/gestflux/'  ;
    	$this->uid = Session::_getUid() ;
    	$this->_countReferences() ;
    }
    /**
    * Singleton
    * 
    * <p>cr�ation de l'instance de la classe si n'existe pas</p>
    * 
    * @name GestfluxSession::_GetInstance()
    * @return Session
    */
	public static function _GetInstance()
	{
		if(!isset(self::$instance))
		{
			self::$instance = new GestfluxSession();
		}
		return self::$instance;
	}  

	public function _getXtplPath()
	{
		return $this->xtpl_path ;
	}
	
	/**
    * Initialisation des variables de session pour l'affichage
    * 
    * @name GestfluxSession::_initSessionValue()
    * @param $gestfluxArray (Array)
    * 			Tableau des identifiants avec $key=>identifiant (int) et $value=>identifiant (int)
    * @return void 
    */ 
    public function _initSessionValue($classCi=1)
    {
    	$_SESSION['ci_tab'] = 'cont' ;
    	$_SESSION['ci_page'] = 'list' ;
    	$_SESSION['ci_view'] = 'smtree';
    	$_SESSION['father_ref'] = 0;
    }
	    
    /**
    * @name GestfluxSession::_setDocTypeArray()
    * @param $typeArray (Array)
    * @return void 
    */ 
    public function _setDocTypeArray($typeArray)
    {
    	$_SESSION['doc_type'] = $typeArray ;
    }

    /**
    * @name GestfluxSession::_setDocObjArray()
    * @param $ObjArray (Array)
    * @return void 
    */ 
    public function _setDocObjArray($ObjArray)
    {
    	$_SESSION['doc_objet'] = $ObjArray ;
    }

    /**
    * @name GestfluxSession::_setDocintArray()
    * @param $IntArray (Array)
    * @return void 
    */ 
    public function _setDocIntArray($intArray)
    {
    	$_SESSION['doc_user'] = $intArray ;
    }
    
     /**
    * @name GestfluxSession::_setDocTriArray()
    * @param $TriArray (Array)
    * @return void 
    */ 
    public function _setDocTriArray($triArray)
    {
    	$_SESSION['doc_tri'] = $triArray ;
    }
    
    /**
    * @name GestfluxSession::_getDocTypeArray()
    * @return Array 
    */ 
    public function _getDocTypeArray()
    {
    	if($_SESSION['doc_type'] != NULL)
    	{
    		return $_SESSION['doc_type'];
    	}else{
    		$typeArray = array() ;
    		return $typeArray;
    	}
    }
    /**
    * @name GestfluxSession::_getDocObjArray()
    * @return Array 
    */ 
    public function _getDocObjArray()
    {
     	if($_SESSION['doc_objet'] != NULL)
    	{
    		return $_SESSION['doc_objet'];
    	}else{
    		$ObjArray = array() ;
    		return $ObjArray ;
    	}
    }    

    /**
    * @name GestfluxSession::_getDocIntArray()
    * @return Array 
    */ 
    public function _getDocIntArray()
    {
     	if($_SESSION['doc_user'] != NULL)
    	{
    		return $_SESSION['doc_user'];
    	}else{
    		$intArray = array() ;
    		return $intArray ;
    	}
    }
    
     /**
    * @name GestfluxSession::_getDocTriArray()
    * @return Array 
    */ 
    public function _getDocTriArray()
    {
     	if($_SESSION['doc_tri'] != NULL)
    	{
    		return $_SESSION['doc_tri'];
    	}else{
    		$triArray = array() ;
    		return $triArray ;
    	}
    }
    
     private function _countReferences()
    {
    }

    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /*  2.1 m�thodes publiques */
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /**
    * <p>Construction de la page principale</p>
    * 
    * @name GestfluxSession::_makeMainPage()
    * @param $content (String)
    * @return void 
    */   

     public function _makeMenuLeft($rights)
    {
		$xtplm = new XTemplate($this->menu_file, $this->xtpl_path);
		$xtplm->assign('IMAGES_PATH', ROOT_IMAGES ) ;
      			
//       if($rights->_isActionAllowed($this->module, 20, $this->uid))
//      	{
//            $xtplm->parse('main.analyseopen'); 
//      	}else{
//      		$xtplm->parse('main.analyseclose');
//      	}
        if($rights->_isActionAllowed($this->module, 21, $this->uid))
      	{
            $xtplm->parse('main.importopen'); 
      	}else{
      		$xtplm->parse('main.importclose');
      	}
        if($rights->_isActionAllowed($this->module, 23, $this->uid))
      	{
            $xtplm->parse('main.agregateopen'); 
      	}else{
      		$xtplm->parse('main.agregateclose');
      	}
      	if($rights->_isActionAllowed($this->module, 22, $this->uid))
      	{
            $xtplm->parse('main.exportopen'); 
      	}else{
      		$xtplm->parse('main.exportclose');
      	}
      	if($rights->_isActionAllowed($this->module, 23, $this->uid))
      	{
            $xtplm->parse('main.jobopen'); 
      	}else{
      		$xtplm->parse('main.jobclose');
      	}
      	
      	$xtplm->parse('main');
      	$menuString = $xtplm->text('main') ;
      	return $menuString;
    }
 } 
?>