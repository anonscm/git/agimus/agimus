<?php
	require_once( '../../app/common/required.php') ;
	require_once( DIR_WWW.ROOT_APPL.'/app/gestflux/gestfluxSession.php') ;

 	//R�cup�ration de la session
 	$session = GestfluxSession::_GetInstance() ;
 	$uid = $session->_getUid() ;
	$rights = ModuleRights::_GetInstance() ;
	
	try{
		if(($rights->_isModuleAllowed(2, 1, $uid))&&($rights->_isActionAllowed('gestflux', 20, $uid, '_FONCTION_NOT_ALLOWED_')))
 		{
 			$msgString = '' ;
 			$valueArray = array() ;

 		if(isset($_GET['action'])){
		      	$action = $_GET['action'] ;
		    }

		    require_once( 'gestfluxCnxLog.php') ;
		    $cnxLog = new GestfluxCnxLog($session, $rights) ;
		    
		    try{
				if($action == 'analyse') {

					require_once( ROOT_CLASS."/cnxLog.php") ;
		    		$cnxLog->_analyseLogDir();
		    		if (count($cnxLog->cnxLogArray)>0)
						$cnxLog->_analyseLog();		    			

						$cnxLog->_makeResult();
		    		
						$session->_sessionLogAction(1, '_LOG_ANALYSE_COMPLETED_') ;	
	      				header('Location: '.ROOT_APPL.'/app/gestflux/trtLogs.php');
	        			exit() ;

				}else{
        				$cnxLog->_makeForm() ;
    			}
		    }
 			catch(MsgException $e){
				$msgString = $e ->_getError($session) ;
				if($action == 'analyse'){
       				$cnxLog->_makeForm() ;
				}
 	    	}
 	    	
 	    	$cnxLog->_makePage($cnxLogId, $msgString) ;
 		}
	}
	catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
