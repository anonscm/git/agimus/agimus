<?php

/* traitement batch d'export vers le niveau n+1 */

if (isset($_SERVER['APPLDIR']))
{
	$applDir=trim($_SERVER['APPLDIR']);

	require_once( $applDir.'/app/common/required.php') ;
	require_once($applDir.'/app/common/library.php') ;
	require_once($applDir.'/app/main/access.php');
	require_once($applDir.'/app/common/traitement.php');

	$debug=false;
	// initialisation des variables et totaux de controle
	$msgString = '' ;
	$valueArray = array() ;
	$nbligred=0;
	$nbligwrited=0;
	$template="";
	$separator="";
	$no_arg=true;
	$param_a_utiliser="";

	// recuperation des parametres de batch

	if (isset($_SERVER["argc"]))
	{
		foreach($_SERVER["argv"] as $prm_value)
		{
			if ($param_a_utiliser!="")
			{
				switch ($param_a_utiliser) {
					case "-u" : $userId=$prm_value;break;
					case "-p" : $userPwd=$prm_value;break;
				}
				$param_a_utiliser="";
				$no_arg=false;
			}

			if (
			(strstr($prm_value,"-u"))
			or (strstr($prm_value,"-p"))
			)
			$param_a_utiliser=trim($prm_value);
		}
	}
	else
	{

	}

	if ($debug)
	echo "\r\nparametres lus : [$userId] | [".md5($userPwd)."] " ;

	$access = new Access($userId,$userPwd,DEFAULT_LANG_APPL) ;

	if($access->_verifAccess()==TRUE)
	{
		$username = $access->_getUsername() ;
		$uid = $access->_getUid() ;
		$lang = $access->_getLang() ;
	}else{
		die("\r\nAcces interdit !");
	}

	$rights = ModuleRights::_GetInstance() ;
	$codeRet=0;

	try{
		//if(($rights->_isModuleAllowed(2, 1, $uid))&&($rights->_isActionAllowed('gestflux', 20, $uid, '_FONCTION_NOT_ALLOWED_')))
		{

			try{
				// creation des repertoires si inexistants
				if (!is_dir(ROOT_OUT))
				{if (!mkdir(ROOT_OUT)) die("ARRET DU TRAITEMENT : impossible de creer le repertoire ".ROOT_OUT);}
				chmod(ROOT_OUT,511); // equivalent 777 octal
				if (!is_dir(ROOT_LOGS))
				{if (!mkdir(ROOT_LOGS)) die("ARRET DU TRAITEMENT : impossible de creer le repertoire ".ROOT_LOGS);}
				chmod(ROOT_LOGS,511); // equivalent 777 octal

				if ($debug)
				echo "\r\nRepertoires crees";


				if ($debug)
				echo "\r\nvariables initialisees";


				// affichage de l'aide
				if ($no_arg)
				{
					$info_help="\r\n".NOM_APPL." - Envoi agregats vers le niveau superieur ";
					$info_help.="\r\n=======================================================";
					$info_help.="\r\n\r\nOptions :\r\n-u : UserId | -p : Password  ";
					die ($info_help);
				}

				$date=strftime("%y/%m/%d");
				$datestamp=strftime("%y%m%d");
				$heurestamp=strftime("%H%M%S");
				$heure=strftime("%H:%M:%S");
				$stamp=$datestamp."_".$heurestamp;
				$logFile="post_to_sup_level_$stamp.log";
				$logname=ROOT_LOGS."/".$logFile;
				$flog=fopen($logname,"w");

				fwrite($flog,NOM_APPL." - Envoi agregats  vers le niveau superieur\r\n ");
				fwrite($flog,"============================================\r\n ");
				fwrite($flog,"\r\nTraitement : du $date a $heure\r\n\r\n");

				//memorisation du traitement
				$trtDateDeb=strftime("%y-%m-%d %H:%M:%S");
				$newTrt=new Traitement();
				$newTrt->_setTrtCod("POST_TO_SUP_LEVEL");
				$newTrt->_setTrtDateDeb($trtDateDeb);
				$newTrt->_setTrtStatus(10);
				$newTrt->_setTrtParams(implode(" -",$_SERVER["argv"]));
				$newTrt->_setTrtLog($logFile);
				$newTrt->_create();

				$erreur=0;

				// validation des parametres
				if ((!no_args))
				{
					fwrite($flog,"\r\n---->>> TRAITEMENT INTERROMPU ! parametres incorrects !<<<----\r\n");
					if ($debug)
					echo "\r\nparametres incorrects " ;
					$erreur=20;
				}
				else
				{
					// lancement du traitement

					if ($debug)
					echo "\r\nLancement du traitement " ;


					if ($debug)
					echo "\r\nFichier de log ouvert " ;

					// ------------------------------------------------
					// TRAITEMENT de POSTAGE VERS LE NIVEAU SUPERIEUR

						
					// transmission des données
					$config=$_SESSION['config'];
					require("../../app/common/structure.php");
					$newStruct= new Structure($config["id_struct"]);
					$structUrlExport=$newStruct->_getUrlExport();

					if ($structUrlExport!="")
					{
						// file list
						$arFiles=_analyseDir(ROOT_OUT,"");
						if (count($arFiles)>0)
						{
							foreach($arFiles as $curFile)
							{

								//$fileContents="agregat_PAU461.sql\r\n";
								$fileHandle = fopen(ROOT_OUT.'/'.$curFile, "rb");
								$fileContents.= stream_get_contents($fileHandle);
								fclose($fileHandle);

								$params = array(
      				'http' => array
								('method' => 'POST',
         		 'header'=>"Content-Type: multipart/form-data\r\n",
          		'content' => $fileContents
								)
								);

								$url = $structUrlExport."/app/gestflux/importFiles.php";
								$ctx = stream_context_create($params);
								$fp = fopen($url, 'rb', false, $ctx);
								$response=stream_get_contents($fp);
								if (!$response)
								fwrite($flog,"\r\n ERREUR DANS L'ENVOI DES DONNEES : $response");
								fclose($fp);
								// suppression du fichier d'export
								unlink(ROOT_OUT.'/'.$curFile);
							}
						}
					}
					else
					{
						fwrite($flog,"\r\n Url export inconnue ! Transfert impossible ");
						$erreur=20;
					}
					fwrite($flog,"\r\nExport vers niveau supérieur effectué: ".$messAna."\r\n");
				}

				if ($debug)
				echo "\r\nFin du traitement d'envoi " ;

				//	------------------------------------------------
				// totaux de controle
				fwrite($flog,"\r\n".str_repeat("-",40)."\r\n");

				$date=strftime("%y/%m/%d");
				$heure=strftime("%H:%M:%S");
				fwrite($flog,"\r\nFin de traitement : le $date a $heure.\r\n");

				if ($debug)
				echo "\r\nFin de traitement le $date a $heure. Lues : $nbligred - Ecrites : $nbligwrited";
				fclose($flog);

				// cloture du traitement
				$trtDateFin=strftime("%y-%m-%d %H:%M:%S");
				$newTrt->_setTrtDateFin($trtDateFin);
				if ($erreur>0)
				$newTrt->_setTrtStatus($erreur);
				else
				$newTrt->_setTrtStatus(30);
				$newTrt->_update();

				// envoi d'un mail de suivi d'exploitation
				if (SEND_MAIL_ACTIF)
				{
					//TODO : mail a envoyer
				}
				//$session->_sessionLogAction(1, '_LOG_ANALYSE_COMPLETED_') ;
				exit($codeRet) ;

			}
			catch(MsgException $e){
				exit(20);
			}
		}
	}
	catch(MsgException $e){
		exit(20);
	}

}
