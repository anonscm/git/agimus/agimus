<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../restit/restitSession.php') ;

//Recuperation de la session
$session = RestitSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

$tdbId=0;

try{
	if(($rights->_isModuleAllowed(4, 1, $uid))&&($rights->_isActionAllowed('restit', 20, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;
		$type=0;
		if (isset($_GET['type']))
			$type=$_GET['type'];

		require_once( 'restitTdb.php') ;
		$tdb = new RestitTdb($session, $rights) ;
		$tdb->_makePage($tdbId, $type, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
