<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../restit/restitSession.php') ;

//R�cup�ration de la session
$session = RestitSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(4, 1, $uid))&&($rights->_isActionAllowed('restit', 20, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$dateArray=array();
		$docVal = array() ;
		$pivotVal=0;
		$config=$_SESSION['config'];
		$idStruct=$config['id_struct'];

		// pré-alimentation des critères déjà saisis et mémorisés
		if (isset($_SESSION['seldates']))
		$dateArray=$_SESSION['seldates'];
		if (isset($_SESSION['selvalues']))
		$valueArray=$_SESSION['selvalues'];
		if (isset($_SESSION['pivotVal']))
		$pivotVal=$_SESSION['pivotVal'];
		if (isset($_SESSION['idStruct']))
		$idStruct=$_SESSION['idStruct'];
			
			
		if(isset($_GET['tdb_id'])){
			$tdbId = $_GET['tdb_id'] ;

			// réinitialisation du pilot mémorisé si changement d'indicateur
			if ($tdbId!=$_SESSION['tdb_id']=$tdbId)
			{	$pivotLib="";$pivotVal=0; }

			if (isset($_GET['pivot']))
			$pivotVal=$_GET['pivot'];
			$action = 'view' ;
		}
		elseif(isset($_POST['tdb_id']))
		{
			$tdbId = $_POST['tdb_id'] ;
			if (isset($_POST['pivot']))
			$pivotVal=$_POST['pivot'];
			$idStruct=$_POST['idStruct'];
			$action = 'view' ;
		}
		else
		{
			header('Location: '.ROOT_APPL.'/app/restit/lst_tdb.php');
			exit() ;
		}

		if (!$idStruct>0)
		$idStruct=$config['id_struct'];

		require_once( 'restitTdb.php') ;
		$tdb = new RestitTdb($session, $rights) ;
		$tdb->_setId($tdbId);
		$tdb->_fill();

		// TODO : a mettre en place
		// génération d'un mail à destination d'utilisateurs
		if ($action=="send") {
			$message .=$newSlrIo->getMessAlert();

			$message .= "";

			$adrMailMag="";

			$mail = new MIME_Mail(EMAIL_APPLI, $adrMailMag, $objet, $message);
			$ret=$mail->send_mail();
		}

		// alimentation de critères
		foreach($_POST as $curPost=>$curValue)
		{
			if (substr($curPost,0,7)=='dimsel_') {
				$tabval=explode("_",$curPost);
				$valueArray[$tabval[1]]=$curValue;
			}
		}
		if (isset($_POST['date_deb']))
		$dateArray['date_deb']=$_POST['date_deb'];
		if (isset($_POST['date_fin']))
		$dateArray['date_fin']=$_POST['date_fin'];
			
		list($arHeaders,$arSelect,$arGraph,$arDates,$pivotId)=$tdb->_getValues($pivotVal,$idStruct,$dateArray,$valueArray);

		// mémorisation des critères fournis
		$_SESSION['seldates']=$dateArray;
		$_SESSION['selvalues']=$valueArray;
		$_SESSION['pivotVal']=$pivotVal;
		$_SESSION['idStruct']=$idStruct;
		$_SESSION['tdb_id']=$tdbId;

		$tdb->_makeform($tdbId, $arSelect, $valueArray, $dateArray, $pivotVal,$pivotId,$idStruct,$msgString) ;
		$tdb->_makePageDetail($tdbId, $arHeaders,$arSelect,$arGraph, $arDates, $pivotVal, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
