<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/indicateur.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/message.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/hdate.php') ;
//v1.3-PPR-24052011 upgrade jpgraph 3.0
require_once( DIR_WWW.ROOT_APPL.'/app/common/dynChartJPGraph.php');
require_once( DIR_WWW.ROOT_APPL.'/app/common/dimension.php');
require_once( DIR_WWW.ROOT_APPL.'/app/common/structure.php');

/**
 *
 * <p>RestitTdb</p>
 *
 * @name RestitTdb
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class RestitTdb extends Indicateur {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés 
	 /*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des indicateurs
	 */
	private $IndicateurArray ;

	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des groupes utilisateurs
	 */
	private $OwnerArray ;

	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $detail file(String)
	 * @desc fichier detail
	 */
	private $detail_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/**
	 * @var $filterDesc (array)
	 * @desc Liste des filtres
	 */
	private $filterDesc ;

	//v1.3-PPR-24052011 ajout maxValues
	private $maxValues;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name RestitTdb::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'tdblist.xtpl' ;
		$this->detail_file = 'afftdb.xtpl' ;
		$this->form_file = 'afftdbform.xtpl' ;
		$this->xtpl_path = $this->lang.'/restit/'  ;
		$this->IndicateurArray = $this->_getList() ;

		//TODO : remplacer par acc�s classe
		$this->OwnerArray=array("1"=>"Administrateurs");

		//v1.3-PPR-24052011 ajout maxValues
		$this->maxValues=7;
	}

	/**
	 * V�rification des variables re�ues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name RestitTdb::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray)
	{
		//TODO : check dates
	}

	
	/**
	 * Collecte des données à afficher
	 *
	 * <p>_getValues</p>
	 *
	 * @name RestitTdb::_getValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _getValues($pivotValRef=0,$idStruct='', $dateArray="",$dimValues=array())
	{
		$config=$_SESSION['config'];
		$arValues=array();
		$arSelect=array();
		$arHeaders=array();
		$arCat=array();
		$pivotId=0;
		//v1.5-PPR-#000 suppression message notice
		$arGraph=array();

		$hDate=new hDate();

		$debug=false;

		// recherche d'une requete SQL existante
		$reqString=trim($this->_getSqlString());

		if ($reqString!="")
		{
			$newSqlString='';

			//personnalisation de la requete

			if (count($dimValues)>0)
			{
				foreach($dimValues as $refDim=>$dimValue)
				{
					if ($dimValue>0)
					{
						$newDim=new Dimension($refDim);
						$dimIdLib=$newDim->_getDimIdLib();
						if ($dimIdLib!='')
						$newSqlString.=" AND t_agregat.$dimIdLib = '$dimValue' ";
					}
				}
			}
			$reqString=str_replace('%DIMSEL%',$newSqlString,$reqString);

			if ($idStruct=='') // structure selectionnée
			$idStruct=$config['id_struct'];
			
			// recherche d'un pere eventuel
			$newStruct=new Structure();
			$arStructFils=$newStruct->_getTree($idStruct);
			//v1.3-PPR-24052011 correctif notice
			if ((isset($arStructFils[$idStruct])) and (count($arStructFils[$idStruct])>0))
			{
				//$reqString=str_replace('%IDSTRUCTPERE%',$newSqlString,$reqString);
				$newSqlString=" AND (" . $this->_formatStructSql($arStructFils[$idStruct]) . " ) ";
			}
			else
			{
				//$reqString=str_replace('%IDSTRUCTPERE%',$newStruct->_getParent(),$reqString);
				//$reqString=str_replace('%IDSTRUCT%',$newSqlString,$reqString);
				$newSqlString =" AND t_agregat.ID_STRUCT = \"$idStruct\" ";
			}
			$reqString=str_replace('%STRUCTSEL%',$newSqlString,$reqString);
						
			
			// fourchettes de date

			$newSqlString='';
			if ((isset($dateArray["date_deb"])) and ($dateArray["date_deb"]>0))
			{
				$stamp=strtotime($hDate->_dateToEnFormat($dateArray["date_deb"]));
				$newSqlString=" AND t_agregat.id_temps >= '$stamp' ";
			}
			$reqString=str_replace('%DATEDEB%',$newSqlString,$reqString);

			$newSqlString='';
			if ((isset($dateArray["date_fin"])) and ($dateArray["date_fin"]>0))
			{
				$stamp=strtotime($hDate->_dateToEnFormat($dateArray["date_fin"]));
				$newSqlString=" AND t_agregat.id_temps <= '$stamp' ";
			}
			$reqString=str_replace('%DATEFIN%',$newSqlString,$reqString);

			// requetage final

			$this->_setSqlString($reqString);
			$arResult=$this->_getResult();

			$datemin=mktime();
			$datemax="";

			if (count($arResult)>0)
			{
				$row=0;
				//TRAITEMENT DES LIGNES DE RESULTAT DE REQUETE
				foreach($arResult as $ligne=>$values)
				{
					if (count($values)>0)
					{

						// filtrage sur le pivot

						$pivotOk=true;
						if (($pivotValRef>0) and ($values['pivotval']!=$pivotValRef))
						$pivotOk=false;

						//traitement des entetes

						if ($row==0)
						{
							foreach(array_keys($values) as $header)
							{
								if ($debug) echo "$header - ";

								if ($header!="pivotval")
								{
									$arHeaders[]=$header;
								}
								else
								{
									$pivotFound=true;
								}
							}
						}

						$arValeurs=array();
						//if (isset($values['pivotval']))
						$arSelect[$values['pivotval']]=$values['pivotlib'];

						// reperage et enrichissement des tableaux de valeur

						foreach($values as $key=>$valeur)
						{
							if ($pivotOk)
							{
								//detection de l'id pivot pour non-affichage dans le form
								if ($key=="pivotid")
								$pivotId=$valeur;

								if ($key!="pivotval")
								{
									$arValeurs[$key]=$valeur;
								}

								if ($key=='pivotval')
								$curPivotVal=$valeur;


								// Alimentation du tableau pour graphique

								if ($key=='lib')
								{
									$curVal=str_replace("'"," ",$valeur);
									if (!in_array($curVal,$arCat))
									$arCat[]=$curVal;
								}

								if ($key=='val')
								$arrData[$arSelect[$curPivotVal]][$curVal]=$valeur;

								if ($key=="datemin")
								$datemin=min($datemin,$valeur);
								if ($key=="datemax")
								$datemax=max($datemax,$valeur);
							}
						}

						$row+=1;
					}
				}

				// ------------------ Completion des valeurs
					
				$arCompletion=array();
				if ($this->_getCompletionValues()!="")
				{
					switch($this->_getCompletionValues()) {
						case '1' : $arCompletion=$_arHours;break;
						default : break;
					}

					if (count($arCompletion)>0)
					{
						$newData=array();
						foreach($arrData as $pivot=>$values)
						{
							// if ($this->_getCompletingFlag())
							{
								$values=$values+$arCompletion;
							}
							ksort($values);
							$newData[$pivot]=$values;
						}
						$arrData=$newData;

						foreach($arCompletion as $key=>$val)
						{
							if (!in_array($key,$arCat))
							$arCat[]=$key;
						}

						sort($arCat);
					}
				}

				// ------------------ Remplissage des zones vides

				$arGraph=array();
				foreach($arCat as $curCat)
				{
					foreach($arrData as $pivot=>$values)
					{
						// traitement des r�sultats sans pivots
						if ($pivot=='')
						{
							$arGraph=$values;
							break;
						}

						$catFound=false;
						foreach($values as $catLib=>$catVal)
						{
							//echo "<br>$pivot : $catLib => $catVal";
							if ($catLib==$curCat)
							{
								$arGraph[$pivot][$curCat]=$catVal;
								$catFound=true;
								break;
							}
						}
						if (!$catFound)
						$arGraph[$pivot][$curCat]=0;
					}
				}
			}
		}
		$arValues=array($arHeaders,$arSelect,$arGraph,array("date_min"=>$datemin,"date_max"=>$datemax),$pivotId);
		return $arValues;
	}

	/**
	 * Contrôle requete SQL
	 *
	 * <p>Contrôle requete SQL</p>
	 *
	 * @name Indicateur::_ctrSqlQuery($reqString)
	 * @return boolean
	 */
	public function _ctrSqlQuery($reqString)
	{
		if ($reqString=="")
		return false;

		$config=$_SESSION['config'];
		//remplacement des paramètres de requete
		$reqString=str_replace('%IDSTRUCT%',$config['id_struct'],$reqString);
		$reqString=str_replace('%STRUCTSEL%','',$reqString);
		$reqString=str_replace('%DIMSEL%','',$reqString);
		$reqString=str_replace('%DATEDEB%','',$reqString);
		$reqString=str_replace('%DATEFIN%','',$reqString);

		$maconnexion = MysqlDatabase::GetInstance() ;
		$res = $maconnexion->_queryTest($reqString);
		return $res;
	}

	/**
	 * Insertion des donnees dans le modele de page
	 *
	 * <p>_makePage</p>
	 *
	 * @name Indicateur::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($tdbId=0, $type=0, $msg='')
	{
		global $_indicUnits,$_defaultOutputs,$_updateFreqs,$_arGraphIcons;

		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
			$xtpl->assign('MSG', $msg) ;
			$xtpl->parse('main.msg');
		}

		$idxRow=0;
		$nbCols=NBR_COL_MENU_TDB;

		foreach($this->IndicateurArray as $key=>$value)
		{
			// filtrage sur le type
			if (($type!=0) && ($type!=$value['TYPE'])) continue;

			$xtpl->assign('TDB_ID', $key);
			$xtpl->assign('LIB', $value['LIB']);

			if (isset($value['DEFAULT_OUTPUT']))
			$icon=$_arGraphIcons[$value['DEFAULT_OUTPUT']];
			else
			$icon=$_arGraphIcons[0];

			$xtpl->assign('ICON', $icon);

			if(($key != $tdbId)&&($this->rights->_isActionAllowed('restit', 20, $this->uid)))
			{
				if ($value["SQL_STRING"]!="")
				{
					// Annulé cause perfs
					
					//if (!($this->_ctrSqlQuery($value['SQL_STRING'])))
					//{
					//	$xtpl->parse('main.list.row.select.sqlfailure');
					//}
					//else
					{
						$xtpl->parse('main.list.row.select.sqlok');

					}
					$xtpl->parse('main.list.row.select');

					if ($idxRow==$nbCols)
					{
						$xtpl->parse('main.list.row');
						$idxRow=0;
					}
					else
					{$idxRow+=1;}
				}
			}
		}

		if ($idxRow!=0)
		{
			$xtpl->parse('main.list.row');
		}

		$xtpl->parse('main.list');

		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}

	/**
	 * Affichage detaille du tableau de bord
	 *
	 * <p>_makePageDetail</p>
	 *
	 * @name Indicateur::_makePageDetail()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePageDetail($tdbId=0, $arHeaders=array(),$arSelect=array(),$arGraph=array(), $arDates=array(), $pivotValRef=0, $msg='')
	{
		global $_indicUnits,$_defaultOutputs,$_updateFreqs,
		$_arJPGColors,$_arJPGCumColors,$_arHours,$_arReqColLibs;

		$debug=false;

		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->detail_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;

		if($msg != '')
		{
			$xtpl->assign('MSG', $msg) ;
			$xtpl->parse('main.msg');
		}

		$xtpl->assign('TDB_LIB', $this->_getLib());

		// Initialisation du tableau pour le graphique
		$arrData=array();
		$pivotFound=false;
		$arCat=array();

		// Contrôle des résultats pour message "Panel Insuffisant"
		$messPanelInsuff="";

		if (count($arGraph)>0)
		{
			foreach($arGraph as $pivotLib=>$arValues)
			{
				if (is_array($arValues)) // c'est un tableau -> il y a un pivot
				foreach($arValues as $val=>$lib)
				{

					if ((SEUIL_PANEL_INSUFFISANT>0) and ($val<SEUIL_PANEL_INSUFFISANT))
					{
						$messPanelInsuff="<br>Panel insuffisant pour afficher le graphe ";
						break;
					}
				}

				else
				{
					if ((SEUIL_PANEL_INSUFFISANT>0) and ($arValues<SEUIL_PANEL_INSUFFISANT))
					{
						$messPanelInsuff="<br>Panel insuffisant pour afficher le graphe ";
						break;
					}
				}
			}
			$xtpl->assign('MESS_NO_RESULT', $messPanelInsuff);
			$xtpl->parse('main.noresult');
		}

		if ($messPanelInsuff=="")
		{
			// affichage en mode table
			if ($this->_getDefaultOutput()==1)
			{
				if (count($arHeaders)>0)
				foreach($arHeaders as $curHeader)
				{
					if (($curHeader!="datemin") and ($curHeader!="datemax"))
					{
						$lib=(isset($_arReqColLibs[$curHeader])?$_arReqColLibs[$curHeader]:$curHeader);

						$xtpl->assign('HEADER',$lib);
						$xtpl->parse('main.result.detail.header');
					}
				}

				if (count($arGraph)>0)
				{
					foreach($arGraph as $pivotLib=>$arValues)
					{
						if (is_array($arValues)) // c'est un tableau -> il y a un pivot
						foreach($arValues as $val=>$lib)
						{
							$xtpl->assign('VALEUR',$pivotLib);
							$xtpl->parse('main.result.detail.row.cell');
							$xtpl->assign('VALEUR',$val);
							$xtpl->parse('main.result.detail.row.cell');
							$xtpl->assign('VALEUR',$lib);
							$xtpl->parse('main.result.detail.row.cell');

							$xtpl->parse('main.result.detail.row');
						}
						else
						{
							$xtpl->assign('VALEUR',$pivotLib);
							$xtpl->parse('main.result.detail.row.cell');
							$xtpl->assign('VALEUR',$arValues);
							$xtpl->parse('main.result.detail.row.cell');

							$xtpl->parse('main.result.detail.row');
						}
					}
					$xtpl->parse('main.result.detail');
				}
			}

			if (count($arGraph)>0)
			{
				$graphHeight=600;
				$graphWidth=700;

				$newChart=new dynChartJPGraph(1);
				$date=strftime("%y%m%d-%H%M%S");

				$newChart->colors=$_arJPGColors;
				$newChart->cumColors=$_arJPGCumColors;

				// suppression des graphiques stampés (traitement bug ie de non rafraichissement)
				filesDirDelete(ROOT_MEDIA,"png","restitution_".$this->session->_getUid());
				$fileName=ROOT_MEDIA."restitution_".$this->session->_getUid()."_".time().".png";
				$newChart->_setDynChartOutFile($fileName);
				$newChart->_setDynChartValues($arGraph);
				//v1.3-PPR-24052011 ajout maxValues
				$newChart->_setMaxValuesBeforeAgregate($this->maxValues);

				$newChart->_setDynChartLib($this->_getLib());
				$newChart->_setDynChartWidth($graphWidth);
				$newChart->_setDynChartHeight($graphHeight);

				// alimentation du libellé d'ordonnée		
				$newChart->_setAxisLibs(array('xaxis'=>'','yaxis'=>$_indicUnits[$this->_getUnite()]));

				switch($this->_getDefaultOutput())
				{
					case "1" : break;
					case "2" : $fileName=$newChart->_drawCumLineChart();break;
					case "3" : $fileName=$newChart->_drawPieChart();break;
					case "4" : $fileName=$newChart->_drawCumHistoChart();break;
					case "5" : $fileName=$newChart->_drawRadarChart();break;
					default : break;
				}

				if (!$fileName)
				{
					$xtpl->assign('MESS_NO_GRAPH', "Impossible de g&eacute;n&eacute;rer le graphe !
					<br>Pas assez de données.");
					//v1.4.1 <hr>Requete : ".$this->_getSqlString()."<br>");
					$xtpl->parse('main.result.nograph');
				}

				if (file_exists($fileName))
				{
					$xtpl->assign('GRAPH_NAME', $fileName);
					$xtpl->parse('main.result.graph');
				}

				// assignation de la p�riode de mesure
				$dateMin=(int)$arDates['date_min'];
				$dateMax=(int)$arDates['date_max'];

				if ($arDates['date_min']>0)
				$xtpl->assign('DATE_MIN', date("d/m/y",$dateMin));
				if ($arDates['date_max']>0)
				$xtpl->assign('DATE_MAX', date("d/m/y",$dateMax));

				if (($dateMin>0) and ($dateMax>0))
				$xtpl->parse('main.result.fromto');
				if (($dateMin>0) and ($dateMax==0))
				$xtpl->parse('main.result.from');
				if (($dateMin==0) and ($dateMax>0))
				$xtpl->parse('main.result.to');

				$xtpl->assign('TDB_DSC', nl2br($this->_getDsc())) ;

				// génération du fichier PDF associé à l'indicateur affiché
				require_once("../../fpdf/fpdf.php");
				$pdf=new XPDF();

				if (!$pdf) {
					$msgString = "Impossible de créer le fichier PDF ";
					$session->_makeErrorPage($msgString) ;
				}
				else
				{
					//Titres des colonnes
					$pdf->Header();
					$imgPos=30;

					$pdf->SetFillColor(255,255,255);
					$pdf->SetDrawColor(128,0,0);
					$pdf->SetTextColor(0);

					$pdf->AddPage();
					$pdf->AliasNbPages();

					//Lecture des donnees
					filesDirDelete(ROOT_MEDIA,"pdf","indic_".$tdbId."_".$this->session->_getUid());
					$pdfFile=ROOT_MEDIA."/indic_".$tdbId."_".$this->session->_getUid().".pdf";

					// affichage des filtres

					$pdf->SetLeftMargin(45);
					if (file_exists($fileName))
						$pdf->Image($fileName,10,$imgPos,150,0,'');
					$pdf->SetLeftMargin(10);
						
					$pdf->SetFontSize(10);
					$pdf->Ln(105);
					$arDesc=explode("<br />",nl2br($this->_getDsc()));
					foreach($arDesc as $lineDesc)
					{
						$pdf->Cell(80,0,str_replace("?","'",utf8_decode($lineDesc)));
						$pdf->Ln(5);
					}
					$pdf->Ln(10);
					$dateResult=strftime("%m/%m%y - %H%M");
					$pdf->Cell(80,0,"Date du graphe : ".$dateResult);
					$pdf->Ln(10);
						
					if (count($this->filterDesc)>0)
					{
						foreach($this->filterDesc as $curFilter)
						{
							$pdf->SetFontSize(8);
							$pdf->Cell(80,0,utf8_decode($curFilter));
							//Saut de ligne
							$pdf->Ln(5);
							$imgPos+=5;
						}
					}
					$pdf->Output($pdfFile);

					if (file_exists($pdfFile))
					$xtpl->assign('TDB_PDF',$pdfFile);
				}

				$xtpl->parse('main.result');

			}
			else
			{
				$msgLib='Pas de r&eacute;sultat';
				$xtpl->assign('MESS_NO_RESULT',$msgLib);
				$xtpl->parse('main.noresult');
			}

			if($this->formString != '')
			{
				$xtpl->assign('FORM', $this->formString);
				$xtpl->parse('main.select');
			}
		} // panel non insuffisant

		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}

	public function _makeForm($tdbId=0,  $arPivot=array(),$valueArray=array(),$arDates=array(),$pivotVal=0,$pivotId=0, $idStruct='', $msgString="")
	{
		$form = new Form('../aff_tdb.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);
		$config=$_SESSION['config'];
		$xtplform->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtplform->assign('LANG', $this->lang ) ;

		$debug=false;

		$this->filterDesc=array();
		$dim_sel=array();
		$arDims = Dimension::_getComboList() ;

		$valuesExist=false;
		foreach($valueArray as $id=>$val)
		if ($val>0) $valuesExist=true;

		//v1.3-PPR ajout d'un icone d'information de selection
		//if((($valueArray == NULL) or (!$valuesExist)) &&($pivotVal==0) &&($idStruct==''))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}

		$noSel=true;
		if($valueArray!=NULL)
		{
			foreach($valueArray as $key=>$value) {
				if ($value!=0) $noSel=false;
			}
		}

		//recherche des paramètres de la requete
		$dimSelOk=false;
		$dateDebOk=false;
		$dateFinOk=false;

		if ($tdbId>0)
		{
			$this->_setId($tdbId);
			$arIndic=$this->_getList();
			$sqlString=$arIndic[$tdbId]["SQL_STRING"];
			if (strstr($sqlString,"%DIMSEL%"))
			$dimSelOk=true;
			if (strstr($sqlString,"%DATEDEB%"))
			$dateDebOk=true;
			if (strstr($sqlString,"%DATEFIN%"))
			$dateFinOk=true;
		}

		if(($valueArray != NULL) or ($arDates != NULL))
		{
			$date_deb = $arDates['date_deb'] ;
			$date_fin = $arDates['date_fin'] ;
			$dim_sel=$valueArray;

		}else{
			$date_deb = '' ;
			$date_fin = '' ;
			$pivot_val = $pivotVal;
		}

		// recherche libellé du pivot
		$libDim="???";
		if (!$pivotId==0)
		{
			$curDim=new Dimension($pivotId);
			if ($curDim->_getLib()!='')
			$libDim=$curDim->_getLib();
			else
			$libDim=$pivotId;
		}

		if (($pivotVal!=0) or (!$noSel)
		//v1.5-PPR-#005 correctif sur selection active
		or ($date_deb>0) or ($date_fin>0) )
			$xtplform->parse('main.selok.selactive') ;


		$xtplform->assign('TDB_ID', $form->_mkInput('hidden', 'tdb_id', $tdbId)) ;

		if ($debug) echo "<br>pivotVal:[$pivotVal] pivotId:[$pivotId] dateDebOk:[$dateDebOk] dateFinOk:[$dateFinOk] dimSelOk:[$dimSelOk] ";

		// chargement des structures disponibles
		$newStruct=new Structure();
		$dummyCat=$newStruct->_getList();
		$arStruct = $newStruct->_getComboList() ;
		if (count($arStruct)>0)
		{
			$xtplform->assign('STRUCT_LIB', $form->_mkSelect('idStruct', $arStruct, $idStruct,"onchange='submit()'")) ;
			$xtplform->parse('main.struct');
			if ($idStruct!="")
			$this->filterDesc[].="Structure : ".$arStruct[$idStruct];
		}


		if ($dateDebOk)
		{
			$xtplform->assign('DATE_DEB', $form->_mkDateInput('date_deb', $date_deb)) ;
			$xtplform->parse('main.datedeb');
			if ($date_deb!="")
			$this->filterDesc[].="Date de début : ".$date_deb;
		}
		if ($dateFinOk)
		{
			$xtplform->assign('DATE_FIN', $form->_mkDateInput('date_fin', $date_fin)) ;
			$xtplform->parse('main.datefin');
			if ($date_fin!="")
			$this->filterDesc[].="Date de fin : ".$date_fin;
		}
		if (!$pivotId==0)
		{
			$xtplform->assign('PIVOT_LIB', $form->_mkSelect('pivot', $arPivot, $pivotVal,'')) ;
			$xtplform->assign('PIVOT_ID', $libDim) ;
			$xtplform->parse('main.pivot');
			if ($pivotVal>0)
			$this->filterDesc[].="Pivot ($libDim) : ".$arPivot[$pivotVal];
		}

		// on affiche les selection par dimension que si DIMSEL a ete prévue dans la requete
		if ($dimSelOk)
		{
			foreach($arDims as $idDim=>$libDim)
			{
				// filtrage selon type de structure (pas d'affichage des intra pour UNR et MNS)
				if ((SITE_TYPE=="ETB") or 
				((SITE_TYPE!="ETB") and ($idDim<=5) // (dimensions nationales uniquement, les autres sont à zero)
				))

				//v1.5-PPR-#005 if (($pivotId==0) or (($pivotId>0) and ($idDim!=$pivotId)))
				{
					//v1.5-PPR-#000 suppression message notice sur $idDim
					if (!(isset($dim_sel[$idDim])))
						$dim_sel[$idDim]=""; 

					$arDimValComboList=Dimension::_getDimensionValues($idDim);
					$xtplform->assign('DIM_LIB', $libDim) ;
					$xtplform->assign('DIM_SEL', $form->_mkSelect('dimsel_'.$idDim, $arDimValComboList, $dim_sel[$idDim],'')) ;
					$xtplform->parse('main.dim');
					if ($dim_sel[$idDim])
					$this->filterDesc[].="$libDim : ".$arDimValComboList[$dim_sel[$idDim]];
				}
			}
		}

		if (($pivotId>0) or ($dateDebOk) or ($dateFinOk) or ($dimSelOk))
		{
			$xtplform->parse('main.selok');
			$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('sub', 'Afficher' ) );
		}
		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Indicateur::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>