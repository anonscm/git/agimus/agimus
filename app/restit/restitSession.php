<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/session.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/indicateur.php') ;

/**
 * Gestion des sessions dans le module restit
 * <p>RestitSession</p>
 *
 * @name RestitSession
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package restit
 */

class RestitSession extends Session {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. attributs     */
	/*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var (Singleton)
	 * @desc variable pour le singleton
	 */
	private static $instance;

	/**
	 * @var $menu_file (String)
	 * @desc fichier template du menu vertical
	 */
	private $menu_file ;

	/**
	 * @var $xtpl_path (String)
	 * @desc chemin des templates
	 */
	private $xtpl_path ;

	/**
	 * @var $module (String)
	 * @desc module
	 */
	private $module ;

	/**
	 * @var (Array)
	 * @desc tableau des objets du module restit
	 */
	private $restitArray  ;


	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name RestitSession::__construct()
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->module = 'restit' ;
		$this->menu_file = 'restitmenuleft.xtpl' ;
		$this->xtpl_path = Session::_getLang().'/restit/'  ;
		$this->uid = Session::_getUid() ;
		$this->_countReferences() ;
	}
	/**
	 * Singleton
	 *
	 * <p>cr�ation de l'instance de la classe si n'existe pas</p>
	 *
	 * @name Session::_GetInstance()
	 * @return Session
	 */
	public static function _GetInstance()
	{
		if(!isset(self::$instance))
		{
			self::$instance = new RestitSession();
		}
		return self::$instance;
	}

	public function _getXtplPath()
	{
		return $this->xtpl_path ;
	}

	/**
	 * Initialisation des variables de session pour l'affichage
	 *
	 * @name RestitSession::_initSessionValue()
	 * @param $restitArray (Array)
	 * 			Tableau des identifiants avec $key=>identifiant (int) et $value=>identifiant (int)
	 * @return void
	 */
	public function _initSessionValue()
	{
	}

	/**
	 * @name RestitSession::_setDocTypeArray()
	 * @param $typeArray (Array)
	 * @return void
	 */
	public function _setDocTypeArray($typeArray)
	{
		$_SESSION['doc_type'] = $typeArray ;
	}

	/**
	 * @name RestitSession::_setDocObjArray()
	 * @param $ObjArray (Array)
	 * @return void
	 */
	public function _setDocObjArray($ObjArray)
	{
		$_SESSION['doc_objet'] = $ObjArray ;
	}

	/**
	 * @name RestitSession::_setDocintArray()
	 * @param $IntArray (Array)
	 * @return void
	 */
	public function _setDocIntArray($intArray)
	{
		$_SESSION['doc_user'] = $intArray ;
	}

	/**
	 * @name RestitSession::_setDocTriArray()
	 * @param $TriArray (Array)
	 * @return void
	 */
	public function _setDocTriArray($triArray)
	{
		$_SESSION['doc_tri'] = $triArray ;
	}

	/**
	 * @name RestitSession::_getDocTypeArray()
	 * @return Array
	 */
	public function _getDocTypeArray()
	{
		if($_SESSION['doc_type'] != NULL)
		{
			return $_SESSION['doc_type'];
		}else{
			$typeArray = array() ;
			return $typeArray;
		}
	}
	/**
	 * @name Session::_getDocObjArray()
	 * @return Array
	 */
	public function _getDocObjArray()
	{
		if($_SESSION['doc_objet'] != NULL)
		{
			return $_SESSION['doc_objet'];
		}else{
			$ObjArray = array() ;
			return $ObjArray ;
		}
	}

	/**
	 * @name Session::_getDocIntArray()
	 * @return Array
	 */
	public function _getDocIntArray()
	{
		if($_SESSION['doc_user'] != NULL)
		{
			return $_SESSION['doc_user'];
		}else{
			$intArray = array() ;
			return $intArray ;
		}
	}

	/**
	 * @name Session::_getDocTriArray()
	 * @return Array
	 */
	public function _getDocTriArray()
	{
		if($_SESSION['doc_tri'] != NULL)
		{
			return $_SESSION['doc_tri'];
		}else{
			$triArray = array() ;
			return $triArray ;
		}
	}

	private function _countReferences()
	{
		$this->restitArray = array() ;
		$indic = new Indicateur();
		$this->restitArray['indic'] = $indic->_countRef() ;
	}
	/*~*~*~*~*~*~*~*~*~*~*~*~*~*/
	/*  2.1 m�thodes publiques *
	 /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * <p>Construction de la page principale</p>
	 *
	 * @name Session::_makeMainPage()
	 * @param $content (String)
	 * @return void
	 */

	public function _makeMenuLeft($rights)
	{
		$xtplm = new XTemplate($this->menu_file, $this->xtpl_path);
		$xtplm->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtplm->assign('NBR_TDB', $this->restitArray['indic']);

		if($rights->_isActionAllowed($this->module, 20, $this->uid))
		{

			//recup�re la liste des indicateurs disponibles
			//v1.3-PPR-24052011 correctif
			$newIndic=new Indicateur();
			$arTdb=$newIndic->_getList();
			foreach($arTdb as $key=>$value)
			{
				if ($value["SQL_STRING"]!="")
				{
					// suppression 'cos performances
					//require_once( DIR_WWW.ROOT_APPL.'/app/restit/restitTdb.php') ;
					//if (!(RestitTdb::_ctrSqlQuery($value['SQL_STRING'])))
					//{
					//	$xtplm->assign('TDB_TITLE', $value['LIB']);
					//	$xtplm->parse('main.tdbclose.tdbitem');
					//}
					//else
					{

						$xtplm->assign('TDB_LINK', ROOT_APPL."/app/restit/aff_tdb.php?tdb_id=".$value['ID']);
						$xtplm->assign('TDB_TITLE', $value['LIB']);
						$xtplm->parse('main.tdbopen.tdbitem');
					}
				}
			}


			$xtplm->parse('main.tdbopen');
			$xtplm->parse('main.tdbclose');
		}else{
			$xtplm->parse('main.tdbclose');
		}
			
		$xtplm->parse('main');
		$menuString = $xtplm->text('main') ;
		return $menuString;
	}
}

?>