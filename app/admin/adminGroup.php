<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/userGroup.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/langue.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/hdate.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
/**
 * Cette classe permet l'affichage des differentes vues possibles des intervenants
 * <p>Gestion des intervenants dans le module Admin</p>
 *
 * @name AdminGroup
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package admin
 */

class AdminGroup extends UserGroup {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (String)
	 * @desc fichier template de la page
	 */
	protected $xtpl_file ;
	/**
	 * @var (String)
	 * @desc fichier template du formulaire
	 */
	protected $form_file ;

	/**
	 * @var (String)
	 * @desc fichier template du formulaire
	 */
	protected $info_file ;

	/**
	 * @var formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	protected $formString ;

	/**
	 * @var lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;
	/**
	 * @var groupArray (Array)
	 * @desc Tableau des groupes avec l'identifiant comme cl�
	 */
	private $groupArray;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name AdminGroup::__construct()
	 * @return void
	 */
	public function __construct($session, $rights, $gid=0) {
		$this->xtpl_file = 'grouplist.xtpl' ;
		$this->form_file = 'groupform.xtpl' ;
		$this->info_file = 'groupinfos.xtpl' ;
		$this->lang = $session->lang;
		$this->xtpl_path = $this->lang.'/admin/'  ;
		$this->groupArray = UserGroup::_getGroupList() ;
	}


	/**
	 * <p>V�rification des variables re�ues via le formulaire</p>
	 *
	 * @name AdminGroup::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return String
	 */
	public function _checkFormValues($gid, $langArray, $valueArray)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		foreach($langArray as $key =>$value)
		{
			if($valueArray[$key] == '')
			{
				throw new msgException('_ERROR_GROUP_LABEL_MISSING_');
			}
				
			$sql = 'SELECT * FROM appl_group_libelle ';
			$sql .= 'WHERE LIBELLE = \''.AddSlashes($valueArray[$key]).'\' ';
			$sql .= 'AND GID !=  \''.$gid.'\' ';
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			if($maconnexion->_bddNumRows($res) >0)
			{
				throw new msgException('_ERROR_SAME_GROUP_NAME_ALREADY_EXISTS_')  ;
			}

			if(($valueArray['mainuser'] != NULL)&&($valueArray['otheruser'] != NULL))
			{
				$main = $valueArray['mainuser'] ;
				$other = $valueArray['otheruser'] ;
				foreach($main as $key=>$value)
				{
					if(in_array($value, $other))
					{
						throw new msgException('_ERROR_USER_IN_MAIN_OTHER_GROUP_')  ;
					}
				}
			}
		}
	}

	/**
	 * Insertion des donn�es dans le mod�le de page
	 *
	 * <p>_makePage</p>
	 *
	 * @name AdminGroup::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return string
	 */
	public function _makePage($session, $rights,$gid=0, $msg='')
	{
		$cols = 2;
		$menuleft = $session->_makeMenuLeft($rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$lang = $session->lang ;
		$xtpl->assign('LANG', $lang) ;

		if($msg != '')
		{
			$xtpl->assign('MSG', $msg);
			$xtpl->parse('main.msg');
		}
		$uid=$session->_getUid() ;
		$xtpl->assign('LIEN_USER', 'class="active"') ;
		if($this->groupArray != NULL)
		{
			if($rights->_isActionAllowed('admin', 3, $uid))
			{
				$xtpl->parse('main.list.colmodif');
				$cols++;
			}
			if($rights->_isActionAllowed('admin', 4, $uid))
			{
				$xtpl->parse('main.list.colcheck');
			}
			$hdate = new HDate() ;
			foreach($this->groupArray as $key=>$value)
			{
				if($key != $gid)
				{
					$xtpl->assign('GID', $key);
					$xtpl->assign('GROUPE', $value[$lang]);
					$xtpl->assign('NB_USERS',$value['nb_users']);

					if($rights->_isActionAllowed('admin', 3, $uid))
					{
						$xtpl->parse('main.list.row.modif');
					}
					if($rights->_isActionAllowed('admin', 4, $uid))
					{
						$xtpl->parse('main.list.row.checkbox');
					}
					$xtpl->parse('main.list.row');
				}
			}
			if($rights->_isActionAllowed('admin', 4, $uid))
			{
				$xtpl->assign('COLSPAN', $cols);
				$xtpl->parse('main.list.deactivate');
			}
			$xtpl->parse('main.list');
		}else{
			$xtpl->parse('main.emptylist');
		}

		//Construction du menu en fonction des droits de l'utilisateur
		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}

		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$session->_makeMainPage($content, $menuleft) ;
	}



	public function _makeForm($gid=0, $valueArray=array())
	{
		$lang = new Langue() ;
		$langArray = $lang->_getLang() ;
		$gidArray = array() ;
		$mainuser = array() ;
		$otheruser = array() ;
		$form = new Form('./lst_group.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);
		if(($gid == 0)&&($valueArray==NULL))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}else{
			$xtplform->assign('CACHEDIV', 'opendiv') ;
		}
		$xtplform->assign('IMAGES_PATH', ROOT_IMAGES) ;
		$xtplform->assign('GID', $gid) ;
		if($valueArray != NULL)
		{
			foreach($langArray as $key=>$value)
			{
				$gidArray[$key] = $valueArray[$key] ;
			}
			if($valueArray['mainuser'] != NULL)
			{
				$mainuser=$valueArray['mainuser'] ;
			}
			if($valueArray['otheruser'] != NULL)
			{
				$otheruser=$valueArray['otheruser'] ;
			}
			$xtplform ->parse('main.link_emptyform');
		}else{
			if($gid != 0)
			{
				$gidArray = $this->groupArray[$gid];
				/*foreach($langArray as $key=>$value)
				 {
				 $libelle[$key] = $gidArray[$key] ;
				 }*/

				$user=UserGroup::_getUserInGroupList($gid) ;
				if($user != NULL)
				{
					foreach($user as $key=>$value)
					{
						if($user[$key]['MAIN_YN']==1)
						{
							$mainuser[]=$key ;
						}else{
							$otheruser[]=$key ;
						}
					}
				}

				$xtplform ->parse('main.link_emptyform');
			}
		}
		$hiddenfield = $form->_mkInput('hidden', 'gid', $gid) ;
		$xtplform->assign('HIDDEN_FIELD',$hiddenfield);

		foreach($langArray as $key=>$value)
		{
			$xtplform->assign('LANG', $key);
			$xtplform->assign('NAME', $form->_mkInput('text', $key, $gidArray[$key],'')) ;
			$xtplform ->parse('main.inpgroup');
		}
		$userFreeArray = UserGroup::_getUserGroupFreeList($gid) ;
		if($userFreeArray != NULL)
		{
			foreach($userFreeArray as $key=>$value)
			{
				$xtplform->assign('USERNAME', $value['USER']);
				if(in_array($key, $mainuser))
				{
					$maindflt = 'checked="checked"' ;
				}else{
					$maindflt = '' ;
				}
				$xtplform->assign('USER_ID', $form->_mkInput('checkbox', 'mainuser[]', $key, $maindflt)) ;

				$xtplform ->parse('main.maingroup.user');
			}
			$xtplform ->parse('main.maingroup');
		}
		$allUserArray = UserGroup::_getAllUser();
		if($allUserArray != NULL)
		{
			foreach($allUserArray as $key=>$value)
			{
				$xtplform->assign('USERNAME', $value['USER']);
				if(in_array($key, $otheruser))
				{
					$otherdflt = 'checked="checked"' ;
				}else{
					$otherdflt = '' ;
				}
				$xtplform->assign('USER_ID', $form->_mkInput('checkbox', 'otheruser[]', $key, $otherdflt)) ;

				$xtplform ->parse('main.othergroup.user');
			}
			$xtplform ->parse('main.othergroup');
		}

		$xtplform ->parse('main');
		$this->formString = $xtplform->text('main') ;
	}



	/**
	 * Construction de la page d'informations sur l'intervenant
	 *
	 * <p>Page d'informations</p>
	 *
	 * @name AdminGroup::_makePageInfos()
	 * @param $session(class)
	 * @param $intcod(Int)
	 * @return String
	 */
	public function _makePageInfos($session, $intcod)
	{
		$xtpl = new XTemplate($this->info_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('STRUCT_LIB', $this->structArray[$this->personArray[$intcod]['ID_STRUCT']]['STRUCT_LIB']) ;
		$xtpl->assign('RESP_NAME', $this->personArray[$this->personArray[$intcod]['INT_PERE']]['INT_NOM'].' '.$this->personArray [$this->personArray[$intcod]['INT_PERE']]['INT_PRE']) ;
		$xtpl->assign('INT_TRG', $this->personArray[$intcod]['INT_TRG']);
		$xtpl->assign('INT_NOM', $this->personArray[$intcod]['INT_NOM']);
		$xtpl->assign('INT_PRE', $this->personArray[$intcod]['INT_PRE']);
		$xtpl->assign('INT_EMA', $this->personArray[$intcod]['INT_EMA']) ;

		$xtpl->assign('APP_NAME', NOM_APPL) ;
		$xtpl->assign('VERSION', VERSION_APPL);
		$xtpl->assign('YEAR', YEAR_APPL) ;
		$xtpl->parse('main');
		$xtpl->out('main');
	}
	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Societe::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>