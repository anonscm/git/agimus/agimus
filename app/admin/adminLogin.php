<?php
  require_once( DIR_WWW.ROOT_APPL.'/app/common/user.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/userGroup.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/hdate.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
/**
 * Cette classe permet l'affichage des diff�rentes vues possibles des intervenants
 * <p>Gestion des intervenants dans le module Admin</p>
 * 
 * @name AdminUser
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package admin
 */
 
 class AdminLogin extends User {
 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/

    /**
    * @var $xtpl_path (String)
    * @desc chemin du template de la page
    */
    protected $xtpl_path ;    
    /**
    * @var (String)
    * @desc fichier template du formulaire
    */
    protected $form_file ;

        
    /**
    * @var formString(String)
    * @desc fichier du formulaire apr�s traitement
    */
    protected $formString ;
    

    
    /**
     * @var lang (String)
     * @desc Langue de l'utilisateur
     */
    private $lang;
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name AdminUser::__construct()
    * @return void 
    */
    public function __construct($session, $rights) {
      $this->form_file = 'loginform.xtpl' ;
      $this->lang = $session->_getLang() ;
      $this->uid = $session->_getUid() ;
      $this->session = $session ;
      $this->rights = $rights ;
      $this->xtpl_path = $this->lang.'/admin/'  ;
      $this->_getUserLogin() ;
    } 


    /**
    * Mise � jour des variables re�ues via le formulaire
    * 
    * <p>libelle</p>
    * 
    * @name AdminUser::_setFormValues(()
    * @param $valuArray(Array)
    * @return void
    */    
     public function _setFormValues($valueArray)
    {		
		Person::_setLogin(trim($valueArray['aut_login'])) ;
    	Person::_setPwd(trim($valueArray['aut_pwd1'])) ;
    }
    
    private function _getUserLogin()
    {
    	$maconnexion = MysqlDatabase::GetInstance() ;    	
        $sql = 'SELECT AUT_USER_LOGIN FROM t_aut ' ; 
		$sql .= 'WHERE INT_COD = \''.$this->uid.'\' ';	

    	try{
	        $res = $maconnexion->_bddQuery($sql) ;
	        $row = $maconnexion->_bddFetchAssoc($res) ;
	        Person::_setLogin(StripSlashes($row['AUT_USER_LOGIN']));
	    }
	    catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
    }

    /**
    * <p>V�rification des variables re�ues via le formulaire</p>
    * 
    * @name AdminLogin::_checkFormValues()
    * @param $valueArray(Array)
    * @return String
    */ 
    public function _checkFormValues($valueArray)
    {
    	$maconnexion = MysqlDatabase::GetInstance() ;
        if($valueArray['aut_login'] == '')
    	{
			throw new msgException('_ERROR_LOGIN_MISSING_');	
    	}    	//transformation d'un intervenant si pas d�j� dans t_aut
    	
    	$sql = 'SELECT * FROM t_aut ' ; 
		$sql .= 'WHERE AUT_USER_LOGIN = \''.AddSlashes($valueArray['aut_login']).'\' ';
    	$sql .= 'AND INT_COD != \''.$this->uid.'\' ';	

    	try{
	        $res = $maconnexion->_bddQuery($sql) ;
	    }
	    catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
	    if($maconnexion->_bddNumRows($res)> 0)
	    {
	    	throw new msgException('_ERROR_LOGIN_ALREADY_USED_')  ;
    	}
    	
        if(($valueArray['aut_pwd2'] == '')&&($valueArray['aut_pwd1'] != ''))
    	{
			throw new msgException('_ERROR_PWD2_MISSING_');	
    	}
    	if($valueArray['aut_pwd1'] != $valueArray['aut_pwd2'])
    	{
			throw new msgException('_ERROR_PWD2_PWD1_NO_MATCH_');
    	}
    	//V�rification sur le mot de passe en cas de mise � jour
    	if($valueArray['aut_pwd1']!='')
	    {
    		//historique des mots de passe
	    	$sql = 'SELECT * FROM user_pwd_history ' ; 
	    	$sql .= 'WHERE  AUT_PWD = \''.md5($valueArray['aut_pwd1']).'\' ';
	    	$sql .= 'AND INT_COD = \''.$this->uid.'\' ';
	    	try{
	        	$res = $maconnexion->_bddQuery($sql) ;
	    	}
	        catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      		}
    		if($maconnexion->_bddNumRows($res) >0)
    		{
    			throw new msgException('_ERROR_PWD_ALREADY_USED_')  ;
    		}	    	
	    }
    }

    /* Mise � jour login et mot de passe d'un utilisateur dans la bdd
    * 
    * <p>_update</p>
    * 
    * @name AdminLogin::_update()
    * @return void 
    */ 
    public function _update()
    {
      $maconnexion = MysqlDatabase::GetInstance() ; 
      try{     
	      	//maj t_aut
	      	$sql = 'UPDATE t_aut SET ';
			$sql .= 'AUT_USER_LOGIN = \''.AddSlashes($this->login).'\' ';
			if($this->pwd != '')
			{
				$sql .= ', AUT_PWD = \''.md5($this->pwd).'\', '; 
				$sql .= 'DATE_PWD = \''.date('Y-m-d').'\' '; 
			}
			$sql .= 'WHERE INT_COD = \''.$this->uid.'\' ';
	    	$res = $maconnexion->_bddQuery($sql) ;
	    	
	    	if($this->pwd != '')
			{
				$sql = 'INSERT INTO user_pwd_history SET ' ; 
				$sql .= 'INT_COD = \''.$this->uid.'\', ';
	    		$sql .= 'AUT_PWD = \''.md5($this->pwd).'\', ';
	    		$sql .= 'DATE_PWD = \''.date('Y-m-d').'\' ';
	    		$res = $maconnexion->_bddQuery($sql) ;
			}
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
    }
    
    public function _makeForm($valueArray=array(), $msg='')
    {
    	$menuleft = $this->session->_makeMenuLeft($this->rights) ;      	
    	$form = new Form('./lst_user.php') ;
	    $xtplform = new XTemplate($this->form_file, $this->xtpl_path);
        if($msg != '')
      	{
         	$xtplform->assign('MSG', $msg);
         	$xtplform->parse('main.msg');
      	}

		if($valueArray != NULL)
      	{
			$aut_login = $valueArray['aut_login'];
			$aut_pwd1 = '';
			$aut_pwd2 = '';	       
      	     	
      	}else{
	      	$aut_login = $this->login ;
			$aut_pwd1 = '';
			$aut_pwd2 = '';
      	}
 
      //Connexion
      $xtplform->assign('LOGIN', $form->_mkInput('text','aut_login', $aut_login)) ;
      $xtplform->assign('PWD_1', $form->_mkInput('password', 'aut_pwd1', $aut_pwd1)) ;
      $xtplform->assign('PWD_2', $form->_mkInput('password', 'aut_pwd2', $aut_pwd2)) ;
      
		$xtplform->parse('main') ;
      
      $content = $xtplform->text('main') ;
      $this->session->_makeMainPage($content, $menuleft) ;
    }

    /**
    * Destructeur
    * 
    * <p>Destruction de l'instance de classe</p>
    * 
    * @name Societe::__destruct()
    * @return void
    */
    public function __destruct() {
    
    }
 } 
?>