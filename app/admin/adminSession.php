<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/session.php') ;
/**
 * Gestion des sessions dans le module Admin
 * <p>AdminSession</p>
 * 
 * @name AdminSession
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright Capella Conseil 2009
 * @version 1.0.0
 * @package admin
 */
 
 class AdminSession extends Session {
 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. attributs     */
    /*~*~*~*~*~*~*~*~*~*~*/
 	/**
    * @var (Singleton)
    * @desc variable pour le singleton
    */
 	private static $instance;
 	    
    /**
    * @var $menu_file (String)
    * @desc fichier template du menu vertical
    */
    private $menu_file ;
    
    /**
    * @var $xtpl_path (String)
    * @desc chemin des templates
    */   
    private $xtpl_path ;

    /**
    * @var $module (String)
    * @desc module
    */  
    private $module ;
    
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name Session::__construct()
    * @return void 
    */
    public function __construct() {
    	parent::__construct();
    	$this->module = 'admin' ;
    	$this->menu_file = 'adminmenuleft.xtpl' ; 
    	$this->xtpl_path = Session::_getLang().'/'.$this->module.'/'  ;
    	$this->uid = Session::_getUid() ;
    }
    /**
    * Singleton
    * 
    * <p>cr�ation de l'instance de la classe si n'existe pas</p>
    * 
    * @name Session::_GetInstance()
    * @return Session
    */
	public static function _GetInstance()
	{
		if(!isset(self::$instance))
		{
			self::$instance = new AdminSession();
		}
		return self::$instance;
	}  

	public function _getXtplPath()
	{
		return $this->xtpl_path ;
	}
	
 	public function _getModule()
	{
		return $this->module ;
	}
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /*  2.1 m�thodes publiques */
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /**
    * <p>Construction de la page principale</p>
    * 
    * @name Session::_makeMainPage()
    * @param $content (String)
    * @return void 
    */   

     public function _makeMenuLeft($rights)
    {
		$xtplm = new XTemplate($this->menu_file, $this->xtpl_path);
		$xtplm->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		
      	if($rights->_isActionAllowed($this->module, 1, $this->uid))
      	{
            $xtplm->parse('main.personopen');
      	}else{
      		$xtplm->parse('main.personclose');
      	}
        if($rights->_isActionAllowed($this->module, 6, $this->uid))
      	{
            $xtplm->parse('main.groupopen'); 
      	}else{
      		$xtplm->parse('main.groupclose');
      	}
        if($rights->_isActionAllowed($this->module, 12, $this->uid))
      	{
            $xtplm->parse('main.rightopen'); 
      	}else{
      		$xtplm->parse('main.rightclose');
      	}

      	$xtplm->parse('main');
      	$menuString = $xtplm->text('main') ;
      	return $menuString;
    }
 } 
?>