<?php
  require_once( DIR_WWW.ROOT_APPL.'/app/common/userGroup.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/langue.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
/**
 * Cette classe permet l'affichage des diff�rentes vues possibles des intervenants
 * <p>Gestion des intervenants dans le module Admin</p>
 * 
 * @name AdminRight
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package admin
 */
 
 class AdminRight extends ModuleRights {
 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/

    /**
    * @var (String)
    * @desc fichier template de la page
    */
    protected $xtpl_file ;    
    /**
    * @var (String)
    * @desc fichier template du formulaire
    */
    protected $form_file ;
    
    /**
    * @var (String)
    * @desc fichier template du formulaire
    */
    protected $info_file ;
        
    /**
    * @var formString(String)
    * @desc fichier du formulaire apr�s traitement
    */
    protected $formString ;
    
    /**
    * @var lang(String)
    * @desc langue pour la session
    */
    private $lang ;
    /**
     * @var groupArray (Array)
     * @desc Tableau des groupes avec l'identifiant comme cl�
     */
    private $groupArray;
    
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name AdminRight::__construct()
    * @return void 
    */
    public function __construct($session, $rights) {
      $this->xtpl_file = 'rightlist.xtpl' ;
     // $this->form_file = 'groupform.xtpl' ;
      //$this->info_file = 'groupinfos.xtpl' ;
      $this->lang = $session->lang;
      $this->xtpl_path = $this->lang.'/admin/';
      $group = new UserGroup() ;
      $this->groupArray = $group->_getGroupList($this->lang) ;  
    } 

    /**
    * Insertion des donn�es dans le mod�le de page
    * 
    * <p>_makePage</p>
    * 
    * @name AdminRight::_makePage()
    * @param session (class)
    * @param rights (class)
    * @return string
    */ 
    public function _makePage($session, $rights, $view='mod', $msg='') 
    {
    	$uid=$session->_getUid() ;
    	$menuleft = $session->_makeMenuLeft($rights) ;
      	$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
      	$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
      	$lang = $session->lang ;
        if($msg != '')
      	{
         	$xtpl->assign('MSG', $msg);
         	$xtpl->parse('main.msg');
      	}
      	$xtpl->assign('VIEW', $view ) ;
      	if($view == 'mod')
      	{
      		$moduleArray = ModuleRights::_getModuleRights($lang);
      		$group = sizeof($this->groupArray) ;
      	    $i=0 ;
      		foreach($this->groupArray as $keygrp=>$valgrp)
      		{
				$groupList[$i]['gid'] = $keygrp ;
      			$groupList[$i]['name'] = $valgrp[$lang];
      			$i++;
      		}
      		foreach($moduleArray as $key=>$value)
      		{
      			$xtpl->assign('MOD_ID', $key) ;
      			$rightArray = $moduleArray[$key]['right'] ; 
      			$actionArray = $moduleArray[$key]['action_right'] ;
      			
      			
      			$xtpl->assign('MOD_NAME', $moduleArray[$key]['title']);
      			$indice = 0 ;
				$i=0;
      			$j=8;
      			$k = 0;
      			$l = 0;
      			if($group%8 == 0)
      			{
      				$cols =$group ;
      			}else{
      				$cols =$group + (8-($group%8));
      			}
      			while($i<$cols)
      			{
					while($i<$j)
      				{
						$xtpl->assign('GROUP_NAME', $groupList[$i]['name']);
      					$xtpl->parse('main.modlist.table.group');
      					$i++ ;
      				}
      				
      			    foreach ($rightArray as $key1=>$value1)
      				{
      					$k=$indice;
      					$xtpl->assign('RIGHT_NAME', $rightArray[$key1]['name']);
      					$groupRight = $rightArray[$key1]['group'] ;
      					while($k<$j)
      					{	
      						if($groupList[$k]['gid'] != NULL)
      						{			
	      						$xtpl->assign('MOD_RIGHT', $key.'|'.$key1.'|'.$groupList[$k]['gid']);
	      						if(in_array($groupList[$k]['gid'], $groupRight))
	      						{
	      							$xtpl->assign('CHECKED', 'checked="checked"');
	      						}else{
	      							$xtpl->assign('CHECKED', '');
	      						}
	      						$xtpl->parse('main.modlist.table.row.checkbox');
      						}
      						$k++ ;
      					}
      					$xtpl->parse('main.modlist.table.row');
      				}

      			    foreach ($actionArray as $key2=>$value2)
      				{
      					$l=$indice;
      					$groupRight = $actionArray[$key2]['group'] ;
      					$xtpl->assign('ACTION_NAME', $actionArray[$key2]['name']);
      					while($l<$j)
      					{	
      						if($groupList[$l]['gid'] != NULL)
      						{			
	      						$xtpl->assign('ACT_RIGHT', $key.'|'.$key2.'|'.$groupList[$l]['gid']);
      							if(in_array($groupList[$l]['gid'], $groupRight))
	      						{
	      							$xtpl->assign('CHECKED', 'checked="checked"');
	      						}else{
	      							$xtpl->assign('CHECKED', '');
	      						}
	      						$xtpl->parse('main.modlist.table.actionrow.checkbox');
      						}
      						$l++ ;      						
      					}
      					$xtpl->parse('main.modlist.table.actionrow');
      				}
					$indice +=8 ; 
					$j+=8 ;
      				$xtpl->parse('main.modlist.table');
      			}
      			$xtpl->parse('main.modlist');
      		}
      	}

      $xtpl->parse('main');
      $content = $xtpl->text('main') ;
      $session->_makeMainPage($content, $menuleft) ;
    }

 
    /**
    * Destructeur
    * 
    * <p>Destruction de l'instance de classe</p>
    * 
    * @name AdminRight::__destruct()
    * @return void
    */
    public function __destruct() {
    
    }
 } 
?>