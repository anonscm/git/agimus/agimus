<?php

require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/admin/adminSession.php') ;

try{
	$session = AdminSession::_GetInstance() ;
	$uid = $session->_getUid();
 	$rights = ModuleRights::_GetInstance() ;

	if(($rights->_isModuleAllowed(1, 1, $uid))&&($rights->_isActionAllowed('admin', 12, $uid, '_FONCTION_NOT_ALLOWED_')))
 	{
 		$msgString = '' ;
 		$modright = array();
 		$actright = array() ;
 		$action = 'create';	
 	 	if(isset($_GET['view'])){
 			$view= $_GET['view'] ;
 	 	}elseif(isset($_POST['view'])){
 	 		$view= $_POST['view'] ;
 	 		if(isset($_POST['submit'])){
	 	 		if($_POST['mod_right'] != NULL)
	 	 		{
	 	 			$modright = $_POST['mod_right'] ;
	 	 		}
	 	 	 	if($_POST['act_right'] != NULL)
	 	 		{
	 	 			$actright = $_POST['act_right'] ;
	 	 		}
	 	 		$action = 'submit';	
 	 		}
 	 	}else{
 	 		$view = 'mod' ;
 	 	}
 	 	
 	 	if($view == 'mod' )
	    { 		
	 		require_once( 'adminRight.php') ;
		    $right = new AdminRight($session, $rights) ;
		    if($action == 'submit')
		    {
		    	$modArray = array() ;
		    	$actArray = array() ;
		    	if($modright != NULL)
		    	{
		    		foreach($modright as $key=>$value)
		    		{
		    			$data = explode('|', $value) ;
		    			$modArray[$data[0]][$data[1]][] = $data[2] ;
		    		}
		    	}
		    	if($actright != NULL)
		    	{
		    		foreach($actright as $key=>$value)
		    		{
		    			$data = explode('|', $value) ;
		    			$actArray[$data[0]][$data[1]][] = $data[2] ;
		    		}
		    	}
		    	$right->_updateGroupRight($modArray, $actArray) ;
		    	$session->_sessionLogAction(5, '_RIGHT_UPDATE_') ;	
	      		header('Location: '.ROOT_APPL.'/app/admin/lst_right.php');
	        	exit() ;
	    	}

	    }else{
	    	require_once( 'adminUserRight.php') ;
		    $right = new AdminUserRight($session, $rights) ;
		    $userid = 0;
		    if(isset($_POST['user_id']))
 	 		{
 	 			$userid = $_POST['user_id'] ;
 	 		}
	     	if(isset($_GET['usrid'])){
 	 			$userid = $_GET['usrid'] ;	 			
 	 		} 	 		
 	 		if(isset($_POST['usrid'])){
 	 			$userid = $_POST['usrid'] ;	 			
 	 		}
 	 		$right->_setUserID($userid) ;
 	 		if($action == 'submit')
		    {
		    	$modArray = array() ;
		    	$actArray = array() ;
		    	if($modright != NULL)
		    	{
		    		foreach($modright as $key=>$value)
		    		{
		    			$data = explode('|', $value) ;
		    			$modArray[$data[0]][] = $data[1] ;
		    		}
		    	}
		    	if($actright != NULL)
		    	{
		    		foreach($actright as $key=>$value)
		    		{
		    			$data = explode('|', $value) ;
		    			$actArray[$data[0]][] = $data[1] ;
		    		}
		    	}
		    	$right->_updateUserRight($userid, $modArray, $actArray) ;
		    	$session->_sessionLogAction(5, '_RIGHT_UPDATE_') ;	
	      		header('Location: '.ROOT_APPL.'/app/admin/lst_right.php?usrid='.$userid.'&view='.$view);
	        	exit() ;
		    }
	    }
	    $right->_makePage($session, $rights, $view, $msgString) ;
 	}			
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
 	
?>