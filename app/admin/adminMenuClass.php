<?php
  require_once( DIR_WWW.ROOT_APPL.'/app/main/mainMenu.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/person.php') ;
  
/**
 * 
 * <p>Acc�s au menu du module admin</p>
 * 
 * @name AdminMenuClass
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */
 
 
 class AdminMenuClass extends MainMenu
 { 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/
    /**
    * @var (String)
    * @desc fichier du template
    */
    private $xtpl_file ;
    
    /**
    * @var (String)
    * @desc chemin du template
    */
    private $xtpl_path ;
    
        
    /**
    * @var (String)
    * @desc module
    */
    private $module ;
    
        /**
    * @var (Array)
    * @desc tableau des objets du module Admin
    */
    private $refArray  ;
    
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name MysqlDatabase::__construct()
    * @return void 
    */    
    public function __construct($session, $rights) {

      parent::__construct($session, $rights) ;
      $this->module = $session->_getModule() ;
      $this->xtpl_file = 'adminmenu.xtpl' ;
      $this->xtpl_path = $session->_getXtplPath()  ;
    }

    
    public function _makePage($session, $rights) 
    {
		$menuleft = $session->_makeMenuLeft($rights) ;
      $xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
      $xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;

      //Construction du menu en fonction des droits de l'utilisateur
      
      $xtpl->parse('main.table');
      if($rights->_isActionAllowed('admin', 1, $session->_getUid()))
      {
		$xtpl->parse('main.user');
      }
      if($rights->_isActionAllowed('admin', 6, $session->_getUid()))
      {
		$xtpl->parse('main.group');
      }
      $xtpl->parse('main');
      $content = $xtpl->text('main') ;
      $session->_makeMainPage($content, $menuleft) ;
    }
 }
 
?>