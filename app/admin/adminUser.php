<?php
  require_once( DIR_WWW.ROOT_APPL.'/app/common/user.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/userGroup.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/hdate.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
/**
 * Cette classe permet l'affichage des diff�rentes vues possibles des intervenants
 * <p>Gestion des intervenants dans le module admin</p>
 * 
 * @name AdminUser
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package admin
 */
 
 class AdminUser extends User {
 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/

    /**
    * @var (String)
    * @desc fichier template de la page
    */
    protected $xtpl_file ;    
    /**
    * @var (String)
    * @desc fichier template du formulaire
    */
    protected $form_file ;
    
    /**
    * @var (String)
    * @desc fichier template du formulaire
    */
    protected $info_file ;
        
    /**
    * @var formString(String)
    * @desc fichier du formulaire apr�s traitement
    */
    protected $formString ;
    
    /**
     * @var userArray (Array)
     * @desc Tableau des utilisateurs avec l'identifiant comme cl�
     */
    private $userArray;
    
    /**
     * @var lang (String)
     * @desc Langue de l'utilisateur
     */
    private $lang;
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name AdminUser::__construct()
    * @return void 
    */
    public function __construct($session, $rights, $int_cod=0, $list='1') {
      $this->xtpl_file = 'userlist.xtpl' ;
      $this->form_file = 'userform.xtpl' ;
      $this->info_file = 'userinfos.xtpl' ;
      $this->lang = $session->_getLang() ;
      $this->xtpl_path = $this->lang.'/admin/'  ;
      $this->userArray = User::_getUserList($list, $this->lang) ;    
    } 


    /**
    * Mise � jour des variables re�ues via le formulaire
    * 
    * <p>libelle</p>
    * 
    * @name AdminUser::_setFormValues(()
    * @param $valuArray(Array)
    * @return void
    */    
     public function _setFormValues($valueArray)
    {
    	Person::_setIntCod($valueArray['intcod']) ;
		Person::_setIntTrg(trim($valueArray['int_trg'])) ;
		Person::_setNom(trim($valueArray['int_nom']));
		Person::_setPrenom(trim($valueArray['int_pre']));
		Person::_setEmail(trim($valueArray['int_ema']));		
		Person::_setLogin(trim($valueArray['aut_login'])) ;
    	Person::_setPwd(trim($valueArray['aut_pwd1'])) ;
		User::_setStatut(1) ;
		User::_setMainGroup($valueArray['maingroup']);
		User::_setOtherGroupArray($valueArray['othergroup']);
    }

    /**
    * <p>V�rification des variables re�ues via le formulaire</p>
    * 
    * @name AdminUser::_checkFormValues()
    * @param $valueArray(Array)
    * @return String
    */ 
    public function _checkFormValues($valueArray)
    {
    	//V�rification des champs obligatoires
        if($valueArray['int_nom'] == '')
    	{
			throw new msgException('_ERROR_NAME_MISSING_');	
    	}
    	if($valueArray['int_pre'] == '')
    	{
			throw new msgException('_ERROR_FIRSTNAME_MISSING_');		
    	}
    	if($valueArray['int_trg'] == '')
    	{
			throw new msgException('_ERROR_TRG_MISSING_');			  		
    	}

    	//v�rifications dans la base de donn�es
    	$maconnexion = MysqlDatabase::GetInstance() ;
    	//V�rification que le trigramme n'est pas d�j� utilis�   	
	    $sql = 'SELECT * FROM t_int WHERE INT_TRG=  \''.AddSlashes(trim($valueArray['int_trg'])).'\' ';
    	if($valueArray['intcod']> 0)
	    {
			$sql .= 'AND INT_COD != \''.$valueArray['intcod'].'\' ';	
	    }
        try{
	    	$res = $maconnexion->_bddQuery($sql) ;
	    }
	    catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
    	if($maconnexion->_bddNumRows($res) >0)
    	{
    		throw new msgException('_ERROR_TRG_ALREADY_EXISTS_')  ;
    	}
    	//V�rification que le nom et le pr�nom ne sont pas d�j� utilis�s   	
	    $sql = 'SELECT * FROM t_int ' ; 
	    $sql .= 'WHERE INT_NOM= \''.AddSlashes(trim($valueArray['int_nom'])).'\' ';
	    $sql .= 'AND INT_PRE= \''.AddSlashes(trim($valueArray['int_pre'])).'\' ';
	    if($valueArray['intcod']> 0)
	    {
			$sql .= 'AND INT_COD != \''.$valueArray['intcod'].'\' ';	
	    }
        try{
	    	$res = $maconnexion->_bddQuery($sql) ;
	    }
	    catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
    	if($maconnexion->_bddNumRows($res) >0)
    	{
    		throw new msgException('_ERROR_SAME_NAME_ALREADY_EXISTS_')  ;
    	}
    	
    	//V�rification login et mot de passe
        if($valueArray['aut_login'] == '')
    	{
			throw new msgException('_ERROR_LOGIN_MISSING_');	
    	}    	//transformation d'un intervenant si pas d�j� dans t_aut
    	
    	$sql = 'SELECT * FROM t_aut ' ; 
		$sql .= 'WHERE AUT_USER_LOGIN = \''.AddSlashes($valueArray['aut_login']).'\' ';
    	if($valueArray['intcod']> 0)
	    {
			$sql .= 'AND INT_COD != \''.$valueArray['intcod'].'\' ';	
	    }
    	try{
	        $res = $maconnexion->_bddQuery($sql) ;
	    }
	    catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
	    if($maconnexion->_bddNumRows($res)> 0)
	    {
	    		throw new msgException('_ERROR_LOGIN_ALREADY_USED_')  ;
    	}
    	
        if(($valueArray['aut_pwd1'] == '')&&($valueArray['intcod']== 0 ))
    	{
			throw new msgException('_ERROR_PWD1_MISSING_');	
    	}
    	//transformation d'un intervenant si pas d�j� dans t_aut
    	if(($valueArray['intcod']> 0)&&($valueArray['aut_pwd1'] == ''))
    	{
		    $sql = 'SELECT * FROM t_aut ' ; 
		    $sql .= 'WHERE INT_COD != \''.$valueArray['intcod'].'\' ';	
    		try{
	        	$res = $maconnexion->_bddQuery($sql) ;
	    	}
	        catch(MsgException $e){
      			$msgString = $e ->_getError();
      			throw new MsgException($msgString, 'database') ;
      		}
	    	if($maconnexion->_bddNumRows($res)== 0)
	    	{
	    		throw new msgException('_ERROR_PWD1_MISSING_')  ;
	    	}
    	}
    	
        if(($valueArray['aut_pwd2'] == '')&&($valueArray['aut_pwd1'] != ''))
    	{
			throw new msgException('_ERROR_PWD2_MISSING_');	
    	}
    	if($valueArray['aut_pwd1'] != $valueArray['aut_pwd2'])
    	{
			throw new msgException('_ERROR_PWD2_PWD1_NO_MATCH_');
    	}
    	//V�rification sur le mot de passe en cas de mise � jour
    	if(($valueArray['intcod']> 0)&&($valueArray['aut_pwd1']!=''))
	    {
    		//historique des mots de passe
	    	$sql = 'SELECT * FROM user_pwd_history ' ; 
	    	$sql .= 'WHERE  AUT_PWD = \''.md5($valueArray['aut_pwd1']).'\' ';
	    	$sql .= 'AND INT_COD = \''.$valueArray['intcod'].'\' ';
	    	try{
	        	$res = $maconnexion->_bddQuery($sql) ;
	    	}
	        catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      		}
    		if($maconnexion->_bddNumRows($res) >0)
    		{
    			throw new msgException('_ERROR_PWD_ALREADY_USED_')  ;
    		}	    	
	    }
	    
    	if(($valueArray['maingroup'] != 0)&&($valueArray['othergroup'] != NULL))
	    {
	    	$main = $valueArray['maingroup'] ;
	      	$other = $valueArray['othergroup'] ;
	      	if(in_array($main, $other))
	      	{
	      		throw new msgException('_ERROR_USER_IN_MAIN_OTHER_GROUP_')  ;
	      	}
	    }
    }
    
    /**
    * Insertion des donn�es dans le mod�le de page
    * 
    * <p>_makePage</p>
    * 
    * @name AdminUser::_makePage()
    * @param session (class)
    * @param rights (class)
    * @return string
    */ 
    public function _makePage($session, $rights, $intcod = 0, $msg='', $list='1') 
    {
    	$cols = 5;
    	$menuleft = $session->_makeMenuLeft($rights) ;
      	$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
      	$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
      	$xtpl->assign('LANG', $session->lang ) ;
      	$xtpl->assign('LIST', $list);
      	if($msg != '')
      	{
         	$xtpl->assign('MSG', $msg);
         	$xtpl->parse('main.msg');
      	}
		$uid=$session->_getUid() ;

      	if($list=='1')
	  	{
	  		$xtpl->assign('LIEN_USER', 'class="active"') ;
	  	  	if($this->userArray != NULL)
	  		{
	  			if($rights->_isActionAllowed('admin', 3, $uid))
		        {
		        	$xtpl->parse('main.list.colmodif');
		        	$cols++;
		        }      
		        if($rights->_isActionAllowed('admin', 4, $uid))
		        {
		        	$xtpl->parse('main.list.colcheck');
		        }
				$hdate = new HDate() ;
	      		foreach($this->userArray as $key=>$value)
	      		{
	      			if($key != $intcod)
	      			{
		        		$xtpl->assign('INT_COD', $key);
		        		$xtpl->assign('INT_TRG', $value['INT_TRG']);
		       	 		$xtpl->assign('FULLNAME', $value['INT_NOM'].' '.$value['INT_PRE']);
		       	 		$xtpl->assign('GROUPE', $value['GROUPE']);
		        		$xtpl->assign('INT_EMA', $value['INT_EMA']);
		        		if(strtotime($value['DATE_PWD']) < mktime(0,0,0,date('m')-3, date('d'), date('Y')))
		        		{
		        			$xtpl->assign('ALERT', 'class="alert"');
		        		}
	      				if($value['AUT_NEW']== TRUE)
		        		{
		        			$xtpl->assign('ALERT', 'class="alert"');
		        			$xtpl->assign('DATE_PWD', '') ;			        			
		        			$xtpl->parse('main.list.row.new');
		        		}else{
		        			$xtpl->assign('ALERT', '');
		        			$xtpl->assign('DATE_PWD', $hdate->_dateStampLgFormat(strtotime($value['DATE_PWD']), $session->lang));	
		        		}
	      				if($rights->_isActionAllowed('admin', 3, $uid))
		        		{
		        			$xtpl->parse('main.list.row.modif');
		        		}      
		        		if($rights->_isActionAllowed('admin', 4, $uid))
		        		{
		        			$xtpl->parse('main.list.row.checkbox');
		        		}
		        		$xtpl->parse('main.list.row');
	      			}
	      		}
	      		if($rights->_isActionAllowed('admin', 4, $uid))
		       	{
		       		$xtpl->assign('COLSPAN', $cols);
		        	$xtpl->parse('main.list.deactivate');
		        }
      			$xtpl->parse('main.list');
	  		}else{
	  			$xtpl->parse('main.emptylist');
	  		}
	   	 	$xtpl->parse('main.list.modif');
	    
	  	}elseif($list=='inter'){
	  		$xtpl->assign('LIEN_INTER', 'class="active"') ;
		  	if($this->userArray != NULL)
		  	{
		  		if($rights->_isActionAllowed('admin', 3, $uid))
		      	{
		        	$xtpl->parse('main.list.colmodif');
		        	$cols++;
		      	}
		      foreach($this->userArray as $key=>$value)
		      {
		      	if($key != $intcod)
		      	{
			        $xtpl->assign('INT_COD', $key);
			        $xtpl->assign('INT_TRG', $value['INT_TRG']);
			        $xtpl->assign('FULLNAME', $value['INT_NOM'].' '.$value['INT_PRE']);
			        $xtpl->assign('INT_EMA', $value['INT_EMA']);
			        $xtpl->assign('DATE_PWD', '-');
		      		if($rights->_isActionAllowed('admin', 3, $uid))
		        	{
		        		$xtpl->parse('main.list.row.modif');
		        	}
			        $xtpl->parse('main.list.row');
		      	}
		      }
	      	$xtpl->parse('main.list');
		  }else{
		  	$xtpl->parse('main.emptylist');
		  }
	  }else{
	  		$xtpl->assign('LIEN_INACTIF', 'class="active"') ;
	  		if($this->userArray != NULL)
	  		{
	  			if($rights->_isActionAllowed('admin', 3, $uid))
		        {
		        	$xtpl->parse('main.list.colcheck');
		        }
	  			$hdate = new HDate() ;
	      		foreach($this->userArray as $key=>$value)
	      		{
	      			if($key != $intcod)
	      			{
		        		$xtpl->assign('INT_COD', $key);
		        		$xtpl->assign('INT_TRG', $value['INT_TRG']);
		       	 		$xtpl->assign('FULLNAME', $value['INT_NOM'].' '.$value['INT_PRE']);
		       	 		$xtpl->assign('GROUPE', $value['GROUPE']);
		        		$xtpl->assign('INT_EMA', $value['INT_EMA']);
		        		if($value['DATE_PWD'] != '0000-00-00')
		        		{
		        			$xtpl->assign('DATE_PWD', $hdate->_dateStampLgFormat(strtotime($value['DATE_PWD']), $session->lang));	
		        		}else{
		        			$xtpl->assign('DATE_PWD', '-') ;
		        		}
		        		if($rights->_isActionAllowed('admin', 3, $uid))
		        		{
		        			$xtpl->parse('main.list.row.checkbox');
		        		}      
		        		$xtpl->parse('main.list.row');
	      			}
	      		}
	      		if($rights->_isActionAllowed('admin', 3, $uid))
		        {
		        	$xtpl->parse('main.list.restore');
		        }  
	  		}
      		$xtpl->parse('main.list');
	      //$xtpl->parse('main.list.restore');
	  }

      //Construction du menu en fonction des droits de l'utilisateur     
      if($this->formString != '')
      {
         $xtpl->assign('FORM', $this->formString);
         $xtpl->parse('main.form');
      }

      $xtpl->parse('main');
      $content = $xtpl->text('main') ;
      $session->_makeMainPage($content, $menuleft) ;
    }
    

    
    public function _makeForm($intcod=0, $valueArray=array(), $list='1')
    {
    	$form = new Form('./lst_user.php') ;
	    $xtplform = new XTemplate($this->form_file, $this->xtpl_path);
	    $othergrp = array() ;
	    if(($intcod == 0)&&($valueArray==NULL))
	    {
	      	$xtplform->assign('CACHEDIV', 'cachediv') ;
	    }else{
	    	$xtplform->assign('CACHEDIV', 'opendiv') ;      	
	    }
	    $xtplform->assign('IMAGES_PATH', ROOT_IMAGES) ;      
	    $xtplform->assign('INT_COD', $intcod) ;
	    $xtplform->assign('LIST', $list) ; 
		if($valueArray != NULL)
      	{
			$int_nom = $valueArray['int_nom'] ;
			$int_pre = $valueArray['int_pre'] ;
			$int_trg = $valueArray['int_trg'] ;
			$int_ema = $valueArray['int_ema'];
			$aut_login = $valueArray['aut_login'];
			$maingrp = $valueArray['maingroup'];
			$othergrp = $valueArray['othergroup'];
			$aut_pwd1 = '';
			$aut_pwd2 = '';	       
			$xtplform ->parse('main.link_emptyform');      	     	
      	}else{
	      	if($intcod != 0)
	      	{
				$xtplform->assign('CACHEDIV', 'opendiv') ;
		      	$int_nom = $this->userArray[$intcod]['INT_NOM'];
		        $int_pre = $this->userArray[$intcod]['INT_PRE'];
		        $int_trg = $this->userArray[$intcod]['INT_TRG'];
				$int_ema = $this->userArray[$intcod]['INT_EMA'];
				$aut_login = $this->userArray[$intcod]['AUT_USER_LOGIN'];
				$aut_pwd1 = '';
				$aut_pwd2 = '';
				$maingrp = $this->userArray[$intcod]['MAIN_GROUP'];
				$othergrp = $this->userArray[$intcod]['OTHER_GROUP'];
				$xtplform ->parse('main.link_emptyform');      
	      	}else{
			    $int_nom = '' ;
			    $int_pre ='' ;
			    $int_trg = '' ;
			  	$int_ema = '';
				$aut_login = '';
				$aut_pwd1 = '';
				$aut_pwd2 = '';
				$maingrp = array();
				$othergrp = array();
	      	}
      	}
      $hiddenfield = $form->_mkInput('hidden', 'intcod', $intcod) ;
      $hiddenfield .= $form->_mkInput('hidden', 'list', $list) ;
      $xtplform->assign('HIDDEN_FIELD',$hiddenfield);
      $xtplform->assign('NAME', $form->_mkInput('text', 'int_nom',$int_nom,'')) ;
      $xtplform->assign('FIRST_NAME', $form->_mkInput('text', 'int_pre',$int_pre,'')) ;
      $trgAtt='onFocus=\'this.value=(this.form.int_pre.value.substring(0,1).toUpperCase()) +( this.form.int_nom.value.substring(0,2).toUpperCase())\'' ;
      $xtplform->assign('TRIGRAMME', $form->_mkInput('text', 'int_trg',$int_trg, $trgAtt)) ;
      $xtplform->assign('EMAIL', $form->_mkInput('text', 'int_ema',$int_ema, '')) ;    
      //Connexion
      $xtplform->assign('LOGIN', $form->_mkInput('text','aut_login',$aut_login)) ;
      $xtplform->assign('PWD_1', $form->_mkInput('password', 'aut_pwd1', $aut_pwd1)) ;
      $xtplform->assign('PWD_2', $form->_mkInput('password', 'aut_pwd2', $aut_pwd2)) ;
      
      $group = new UserGroup() ;
      $groupArray = $group->_getGroupList($this->lang) ;
      foreach ($groupArray as $key=>$value)
      {
      	$selectGroup[$key] = $groupArray[$key][$this->lang] ;
      	$xtplform->assign('GROUPNAME', $groupArray[$key][$this->lang]);
      	if ((is_array($othergrp)) and (in_array($key, $othergrp)))
      	{
			$otherchk = 'checked="checked"' ;
	    }else{
	      	$otherchk = '' ;
	    }
      	$xtplform->assign('GROUP_ID', $form->_mkInput('checkbox', 'othergroup[]', $key, $otherchk));
      	$xtplform->parse('main.group');
      }
      $xtplform->assign('MAIN_GROUP', $form->_mkSelect('maingroup',$selectGroup, $maingrp,'')) ;
      
      $xtplform ->parse('main');
      $this->formString = $xtplform->text('main') ;
    }


    
    /**
    * Construction de la page d'informations sur l'intervenant
    * 
    * <p>Page d'informations</p>
    * 
    * @name AdminUser::_makePageInfos()
    * @param $session(class)
    * @param $intcod(Int)
    * @return String
    */ 
     public function _makePageInfos($session, $intcod)
    {
      	$xtpl = new XTemplate($this->info_file, $this->xtpl_path);
      	$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;      
      	$xtpl->assign('STRUCT_LIB', $this->structArray[$this->personArray[$intcod]['ID_STRUCT']]['STRUCT_LIB']) ;
      	$xtpl->assign('RESP_NAME', $this->personArray[$this->personArray[$intcod]['INT_PERE']]['INT_NOM'].' '.$this->personArray [$this->personArray[$intcod]['INT_PERE']]['INT_PRE']) ;
	    $xtpl->assign('INT_TRG', $this->personArray[$intcod]['INT_TRG']);
	    $xtpl->assign('INT_NOM', $this->personArray[$intcod]['INT_NOM']);
	    $xtpl->assign('INT_PRE', $this->personArray[$intcod]['INT_PRE']);
         $xtpl->assign('INT_EMA', $this->personArray[$intcod]['INT_EMA']) ;
        
		$xtpl->assign('APP_NAME', NOM_APPL) ;
		$xtpl->assign('VERSION', VERSION_APPL);
		$xtpl->assign('YEAR', YEAR_APPL) ;     
      	$xtpl->parse('main');
      	$xtpl->out('main');
    }
    /**
    * Destructeur
    * 
    * <p>Destruction de l'instance de classe</p>
    * 
    * @name Societe::__destruct()
    * @return void
    */
    public function __destruct() {
    
    }
 } 
?>