<?php

require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/admin/adminSession.php') ;

try{
	$session = AdminSession::_GetInstance() ;
	$uid = $session->_getUid();
 	$rights = ModuleRights::_GetInstance() ;
	if(($rights->_isModuleAllowed(1, 1, $uid, '_MODULE_ACCESS_NOT_ALLOWED_'))&&($rights->_isActionAllowed('admin', 1, $uid, '_FONCTION_NOT_ALLOWED_')))
 	{
		$msgString = '' ;
 		//R�cup�ration des donn�es du GET ou du POST pour le formulaire
 	 	if(isset($_GET['list'])){
 			$list= $_GET['list'] ;
 		}elseif(isset($_POST['list'])){
 			$list= $_POST['list'] ;
 		}else{
 			$list='1' ;
 		}
 		
 		if(isset($_GET['intcod']))
	    {
	      $intcod = $_GET['intcod'] ;
	      $action = 'update' ;

	    }elseif(isset($_POST['intcod']))
	    {
	      $intcod = $_POST['intcod'] ;
	      $action = 'submit' ;
	    }elseif(isset($_POST['user_id'])){
	    	if($_POST['submit']== 'deactive')
	    	{
	    		$action = 'deactivate' ;
	    	}else{
	    		$action = 'restore' ;
	    	}
	    	$userArray = $_POST['user_id'] ;
	    }else{
	      $intcod = 0 ;
	      $action = 'create' ;
	    }

	    require_once( 'adminUser.php') ;
	    $person = new AdminUser($session, $rights, $intcod, $list) ;
	    
	    try{
			if($action == 'deactivate'){
				if($rights->_isActionAllowed('admin', 10, $uid,'_PERSON_DEACTIVATE_NOT_ALLOWED_' ))
	    		{
					foreach($userArray as $key=>$value)
	    			{
	    				$person->_deactivate($value) ;
	    			}
	      			$session->_sessionLogAction(5, '_USER_DEACTIVATE_') ;	
	      			header('Location: '.ROOT_APPL.'/app/admin/lst_user.php');
	        		exit() ;
	    		}
			 }elseif($action == 'restore'){
    			if($rights->_isActionAllowed('admin', 5, $uid, '_PERSON_RESTORE_NOT_ALLOWED_'))
    			{
    				foreach($userArray as $key=>$value)
	    			{
	    				$person->_restore($value) ;
	    			}
		    		$session->_sessionLogAction(5, '_USER_RESTORE_') ;          
	    			header('Location: '.ROOT_APPL.'/app/admin/lst_user.php');
	        		exit() ;	
    			}
			}elseif($action == 'update')
    		{
    			if($rights->_isActionAllowed('admin', 3, $uid, '_PERSON_UPDATE_NOT_ALLOWED_'))
    			{
        			//affichage des donn�es dans le formulaire
        			$person->_makeForm($intcod, '', $list) ;
    			}
	
    		}elseif($action == 'submit'){
    			
				if(($intcod == 0)&&($rights->_isActionAllowed('admin', 2, $uid, '_PERSON_CREATE_NOT_ALLOWED_')))
				{
					$person->_checkFormValues($_POST) ;
	      			$person->_setFormValues($_POST) ;
	      			$person->_create() ;
			        $session->_sessionLogAction(5, '_USER_CREATE_') ;
				}
				if(($intcod != 0)&&($rights->_isActionAllowed('admin', 3, $uid, '_PERSON_UPDATE_NOT_ALLOWED_')))
    			{
    				$person->_checkFormValues($_POST) ;
	      			$person->_setFormValues($_POST) ;
	      			$person->_update() ;
			        $session->_sessionLogAction(5, '_USER_UPDATE_') ;
    			}
    			header('Location: '.ROOT_APPL.'/app/admin/lst_user.php');
		        exit() ;
      		}else{
        		//instructions
        		if(($list=='1')&&($rights->_isActionAllowed('admin', 2, $uid)))
        		{
        			$person->_makeForm() ;  
        		}  		
		    }
		}
		
	    catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'deactivate'){
				$intcod=0 ;
				if($rights->_isActionAllowed('admin', 2, $uid))
        		{
        			$person->_makeForm() ;
        		}
			}
			if($action == 'submit')
			{
        			$person->_makeForm($intcod, $_POST, $list) ;
			}
 	    }
 	    $person->_makePage($session, $rights, $intcod, $msgString, $list) ;
 	}	
 		
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
 	
?>