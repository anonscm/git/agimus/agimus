<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/structure.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/person.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/message.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/hdate.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/jpgraph/src/jpgraph.php');
require_once( DIR_WWW.ROOT_APPL.'/jpgraph/src/jpgraph_bar.php');
/**
 * Cette classe permet l'affichage des diff�rentes vues possibles des intervenants
 * <p>Gestion des intervenants dans le module Admin</p>
 *
 * @name AdminPerson
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package admin
 */

class AdminPerson extends Person {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/


	/**
	 * @var (String)
	 * @desc nom du module o� l'on se trouve
	 */
	protected $module;
	/**
	 * @var (Array)
	 * @desc Tableau des modules disponibles pour l'utilisateur
	 */
	// protected $moduleArray ;
	/**
	 * @var (Session)
	 * @desc Objet session
	 */
	protected $runSession ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des ntervenants
	 */
	protected $personArray ;
	/**
	 * @var (Array)
	 * @desc Tableau des structures
	 */
	protected $structArray ;
	/**
	 * @var  (String)
	 * @desc fichier template de la page
	 */
	protected $xtpl_file ;
	/**
	 * @var $gdt_file (String)
	 * @desc fichier template des regroupements (workgroup)
	 */
	protected $gdt_file ;
	/**
	 * @var (String)
	 * @desc fichier template du formulaire
	 */
	protected $form_file ;

	/**
	 * @var (String)
	 * @desc fichier template du formulaire
	 */
	protected $info_file ;

	/**
	 * @var (String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	protected $formString ;


	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name AdminPerson::__construct()
	 * @return void
	 */
	public function __construct($session, $rights, $int_cod=0, $supp='') {
		parent::__construct($int_cod);
		$this->rights = $rights ;
		$this->session = $session ;
		$this->uid=$session->_getUid();
		$this->lang=$session->_getLang() ;
		$this->xtpl_file = 'personlist.xtpl' ;
		$this->form_file = 'personform.xtpl' ;
		$this->info_file = 'personinfos.xtpl' ;
		$this->xtpl_path = $this->session->lang.'/admin/'  ;
		$this->personArray = Person::_getList($supp) ;
		$this->personComboArray = Person::_getComboList() ;
		$this->structArray = $this->_getStructList() ;
		$this->list = $supp ;
	}


	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>libelle</p>
	 *
	 * @name AdminPerson::_setFormValues(()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
		Person::_setIntCod($valueArray['intcod']) ;
		Person::_setIntTrg(trim($valueArray['int_trg'])) ;
		Person::_setNom(trim($valueArray['int_nom']));
		Person::_setPrenom(trim($valueArray['int_pre']));
		Person::_setStructId(trim($valueArray['struct_id']));
		$hdate = new HDate() ;
		Person::_setEmail(trim($valueArray['int_ema']));

		if($valueArray['int_pere'] == '')
		{
			$intpere = 0 ;
		}else{
			$intpere = $valueArray['int_pere'] ;
		}
		Person::_setIntPere($intpere);
		Person::_setUserYN($valueArray['user_yn']) ;
		if($valueArray['user_yn'] == 1)
		{
			$login = strtolower(trim($valueArray['int_pre'].$valueArray['int_nom']));
			Person::_setLogin($login);
			Person::_setPwd(strtolower(trim($valueArray['int_trg'])));
		}
	}

	/**
	 * <p>V�rification des variables re�ues via le formulaire</p>
	 *
	 * @name AdminPerson::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return String
	 */
	public function _checkFormValues($valueArray)
	{
		//V�rification des champs obligatoires
		if(trim($valueArray['int_nom']) == '')
		{
			throw new msgException('_ERROR_NAME_MISSING_');
		}
		if(trim($valueArray['int_pre']) == '')
		{
			throw new msgException('_ERROR_FIRSTNAME_MISSING_');
		}
		if(trim($valueArray['int_trg']) == '')
		{
			throw new msgException('_ERROR_TRG_MISSING_');
		}

		//v�rifications dans la base de donn�es
		$maconnexion = MysqlDatabase::GetInstance() ;
		//V�rification que le trigramme n'est pas d�j� utilis�
		$sql = 'SELECT * FROM t_int WHERE INT_TRG=  \''.AddSlashes($valueArray['int_trg']).'\' ';
		if($valueArray['intcod']> 0)
		{
			$sql .= 'AND INT_COD != \''.$valueArray['intcod'].'\' ';
		}
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res) >0)
		{
			throw new msgException('_ERROR_TRG_ALREADY_EXISTS_')  ;
		}
		//V�rification que le nom et le pr�nom ne sont pas d�j� utilis�s
		$sql = 'SELECT * FROM t_int ' ;
		$sql .= 'WHERE INT_NOM= \''.AddSlashes($valueArray['int_nom']).'\' ';
		$sql .= 'AND INT_PRE= \''.AddSlashes($valueArray['int_pre']).'\' ';
		if($valueArray['intcod']> 0)
		{
			$sql .= 'AND INT_COD != \''.$valueArray['intcod'].'\' ';
		}
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res) >0)
		{
			throw new msgException('_ERROR_SAME_NAME_ALREADY_EXISTS_ ')  ;
		}
	}

	/**
	 * <p>V�rification des variables pour les workgroups re�ues via le formulaire</p>
	 *
	 * @name AdminPerson::_checkAllocFormValues()
	 * @param $valueArray(Array)
	 * @return String
	 */

	public function _checkAllocFormValues($valueArray)
	{
		 
	}


	/**
	 * Insertion des donn�es dans le mod�le de page
	 *
	 * <p>_makePage</p>
	 *
	 * @name AdminPerson::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return string
	 */
	public function _makePage($msg='')
	{
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
			$xtpl->assign('MSG', $msg);
			$xtpl->parse('main.msg');
		}

		if($this->list=='')
		{
			$xtpl->assign('LIEN_ACTIF', 'class="active"') ;
			$xtpl->parse('main.list.modif');
			if($this->rights->_isActionAllowed('admin', 8, $this->uid))
			{
				$xtpl->parse('main.linkform');
			}
		}else{
			$xtpl->assign('LIEN_INACTIF', 'class="active"') ;
			$xtpl->parse('main.list.restore');
		}
		if($this->personArray != NULL)
		{
			foreach($this->personArray as $key=>$value)
			{
				if($key != $this->intCod)
				{
					$xtpl->assign('MODIF_COLOR', '#cccccc') ;
					$xtpl->assign('INT_COD', $key);
					$xtpl->assign('INT_TRG', $value['INT_TRG']);
					$xtpl->assign('FULLNAME', $value['INT_NOM'].' '.$value['INT_PRE']);
					$xtpl->assign('STRUCTURE', $this->structArray[$value['ID_STRUCT']]);
					$xtpl->assign('INT_RESP', $value['INT_PERE']);
					$xtpl->assign('RESP_NAME', $this->personArray[$value['INT_PERE']]['INT_NOM'].' '.$this->personArray[$value['INT_PERE']]['INT_PRE']) ;
					if(($this->rights->_isActionAllowed('admin', 8, $this->uid))&&($supp==''))
					{
						$xtpl->parse('main.list.row.modif');
					}
					if(($this->rights->_isActionAllowed('admin', 8, $this->uid))&&($supp=='S'))
					{
						$xtpl->parse('main.list.row.restore');
					}
					if(($this->rights->_isActionAllowed('admin', 7, $this->uid))&&($supp==''))
					{
						$xtpl->parse('main.list.row.delete');
					}
					$xtpl->parse('main.list.row');
				}
			}
			$xtpl->parse('main.list');
		}else{
			$xtpl->parse('main.emptylist');
		}

		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		return $content ;
	}

	/**
	 * <p>R�cup�ration de la liste des structures</p>
	 *
	 * @name AdminPerson::_getStructList(()
	 * @return array
	 */
	private function _getStructList()
	{
		$struct = new Structure() ;
		$this->structArray = $struct->_getComboList() ;
		return $this->structArray ;
	}

	public function _makeForm($valueArray=array(), $aff_gdt_ref=0, $affVaLArray=array(), $msg )
	{
		$form = new Form('./lst_int.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);
		if($msg != '')
		{
			$xtplform->assign('MSG', $msg) ;
			$xtplform->parse('main.msg');
		}
		$xtplform->assign('IMAGES_PATH', ROOT_IMAGES) ;
		$xtplform->assign('INT_COD', $this->intCod) ;
		 
		if($valueArray != NULL)
		{
			$int_nom = $valueArray['int_nom'] ;
			$int_pre = $valueArray['int_pre'] ;
			$int_trg = $valueArray['int_trg'] ;
			$int_pere = $valueArray['int_pere'] ;
			$int_ema = $valueArray['int_ema'];

			if($valueArray['user_yn'] == 1)
			{
				$user_y ='checked="checked"' ;
				$user_n ='' ;
			}else{
				$user_y ='' ;
				$user_n ='checked="checked"' ;
			}
			$xtplform ->parse('main.link_emptyform');
		}else{
			if($this->intCod != 0)
			{
				$xtplform->assign('CACHEDIV', 'opendiv') ;
				$int_nom = $this->personArray[$this->intCod]['INT_NOM'];
				$int_pre = $this->personArray[$this->intCod]['INT_PRE'];
				$int_trg = $this->personArray[$this->intCod]['INT_TRG'];
				$int_pere = $this->personArray[$this->intCod]['INT_PERE'] ;
				$int_ema = $this->personArray[$this->intCod]['INT_EMA'];
	    
				if($this->personArray[$this->intCod]['USER_YN'] == 1)
				{
					$user_y ='checked="checked"' ;
					$user_n ='' ;
				}else{
					$user_y ='' ;
					$user_n ='checked="checked"' ;
				}
				$xtplform ->parse('main.link_emptyform');
			}else{
				$int_nom = '' ;
				$int_pre ='' ;
				$int_trg = '' ;
				$int_pere = 0 ;
				$int_ema = '';
				$user_y ='' ;
				$user_n ='checked="checked"' ;
			}
		}

		$xtplform->assign('HIDDEN_FIELD' ,$form->_mkInput('hidden', 'intcod',$this->intCod));
		$xtplform->assign('NAME', $form->_mkInput('text', 'int_nom',$int_nom,'')) ;
		$xtplform->assign('FIRST_NAME', $form->_mkInput('text', 'int_pre',$int_pre,'')) ;
		$trgAtt='onFocus=\'this.value=(this.form.int_pre.value.substring(0,1).toUpperCase()) +( this.form.int_nom.value.substring(0,2).toUpperCase())\'' ;
		$xtplform->assign('TRIGRAMME', $form->_mkInput('text', 'int_trg',$int_trg, $trgAtt)) ;
		//Infos soci�t�
		$xtplform->assign('STRUCTURE', $form->_mkSelect('struct_id',$this->structArray, $this->personArray[$this->intCod]['ID_STRUCT'],'')) ;
		$xtplform->assign('RESPONSABLE', $form->_mkSelect('int_pere',$this->personComboArray, $int_pere,'')) ;
		//coordonn�es
		$xtplform->assign('EMAIL', $form->_mkInput('text', 'int_ema',$int_ema, '')) ;
		//Infos
		$xtplform->assign('ITM_USER_N', $form->_mkInput('radio', 'user_yn','0', $user_n)) ;
		$xtplform->assign('ITM_USER_Y', $form->_mkInput('radio', 'user_yn','1', $user_y)) ;
		//
		$xtplform ->parse('main');
		return $xtplform->text('main') ;
	}

	/**
	 * Construction de la page d'informations sur l'intervenant
	 *
	 * <p>Page d'informations</p>
	 *
	 * @name AdminPerson::_makePageInfos()
	 * @param $intcod(Int)
	 * @return String
	 */
	public function _makePageInfos()
	{
		$xtpl = new XTemplate($this->info_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('STRUCT_LIB', $this->structArray[$this->personArray[$this->intCod]['ID_STRUCT']]['STRUCT_LIB']) ;
		$xtpl->assign('RESP_NAME', $this->personArray[$this->personArray[$this->intCod]['INT_PERE']]['INT_NOM'].' '.$this->personArray [$this->personArray[$this->intCod]['INT_PERE']]['INT_PRE']) ;
		$xtpl->assign('INT_TRG', $this->personArray[$this->intCod]['INT_TRG']);
		$xtpl->assign('INT_NOM', $this->personArray[$this->intCod]['INT_NOM']);
		$xtpl->assign('INT_PRE', $this->personArray[$this->intCod]['INT_PRE']);
		$xtpl->assign('INT_EMA', $this->personArray[$this->intCod]['INT_EMA']) ;

		$xtpl->assign('APP_NAME', NOM_APPL) ;
		$xtpl->assign('VERSION', VERSION_APPL);
		$xtpl->assign('YEAR', YEAR_APPL) ;
		$xtpl->parse('main');
		$xtpl->out('main');
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Societe::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>