<?php

require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/admin/adminSession.php') ;

try{
	$session = AdminSession::_GetInstance() ;
	$uid = $session->_getUid();
 	$rights = ModuleRights::_GetInstance() ;
	if(($rights->_isModuleAllowed(1, 1, $uid, '_MODULE_ACCESS_NOT_ALLOWED_'))&&($rights->_isActionAllowed('admin', 6, $uid, '_FONCTION_NOT_ALLOWED_')))
 	{
		$msgString = '' ;
 		//R�cup�ration des donn�es du GET ou du POST pour le formulaire
 	 	if(isset($_GET['gid'])){
 			$gid= $_GET['gid'] ;
 			$action = 'update' ;
 		}elseif(isset($_POST['gid'])){
 			$gid= $_POST['gid'] ;
 			$action = 'submit' ;
 		}elseif(isset($_POST['group_id'])){
 			$groupArray = $_POST['group_id'];
 			$action = 'delete' ;
 		}else{
 			$gid=0 ;
 			$action = 'create' ;
 		}

	    require_once( 'adminGroup.php') ;
	    $group = new AdminGroup($session, $rights, $gid) ;
	    
	    try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('admin', 9, $uid, '_GROUPE_DELETE_NOT_ALLOWED_' ))
	    		{
					foreach($groupArray as $key=>$value)
	    			{
	    				$group->_delete($value) ;
	    			}
	      			$session->_sessionLogAction(5, '_GROUP_DELETE_') ;	
	      			header('Location: '.ROOT_APPL.'/app/admin/lst_group.php');
	        		exit() ;
	    		}
			}elseif($action == 'update')
    		{
    			if($rights->_isActionAllowed('admin', 8, $uid, '_GROUP_UPDATE_NOT_ALLOWED_'))
    			{
        			//affichage des donn�es dans le formulaire
        			$group->_makeForm($gid, $_POST) ;
    			}
	
    		}elseif($action == 'submit'){
    			
				if(($gid == 0)&&($rights->_isActionAllowed('admin', 7, $uid, '_GROUP_CREATE_NOT_ALLOWED_')))
				{
					$lang = new Langue() ;
    				$langArray = $lang->_getLang() ;
    				$group->_checkFormValues($gid, $langArray, $_POST) ;
    				foreach($langArray as $key=>$value)
    				{
    					$group->_setLibelleArray($key, $_POST[$key]);
    				}
    				if($_POST['mainuser'] != NULL)
    				{
	    				foreach($_POST['mainuser'] as $key=>$value)
	    				{
	    					$group->_setMainUserArray($value);
	    				}
    				}
    			    if($_POST['otheruser'] != NULL)
    				{
	    				foreach($_POST['otheruser'] as $key=>$value)
	    				{
	    					$group->_setOtherUserArray($value);
	    				}
    				}
	      			$group->_create() ;
			        $session->_sessionLogAction(5, '_GROUP_CREATE_') ;
				}
				
				
				
				
				if(($gid != 0)&&($rights->_isActionAllowed('admin', 8, $uid, '_GROUP_UPDATE_NOT_ALLOWED_')))
    			{  				
    				$lang = new Langue() ;
    				$langArray = $lang->_getLang() ;

    				$group->_checkFormValues($gid, $langArray, $_POST) ;
    				foreach($langArray as $key=>$value)
    				{
    					$group->_setLibelleArray($key, $_POST[$key]);
    				}
    				if($_POST['mainuser'] != NULL)
    				{
	    				foreach($_POST['mainuser'] as $key=>$value)
	    				{
	    					$group->_setMainUserArray($value);
	    				}
    				}
    			    if($_POST['otheruser'] != NULL)
    				{
	    				foreach($_POST['otheruser'] as $key=>$value)
	    				{
	    					$group->_setOtherUserArray($value);
	    				}
    				}
			        $group->_setGid($gid) ;
			        $group->_update() ;
			        $session->_sessionLogAction(5, '_GROUP_UPDATE_') ;          
    			}
		      	header('Location: '.ROOT_APPL.'/app/admin/lst_group.php');
		        exit() ;

      		}else{
        		if($rights->_isActionAllowed('admin', 7, $uid))
        		{
        			$group->_makeForm() ;  
        		}  		
		    }
		}
		
	    catch(MsgException $e){

			$msgString = $e ->_getError($session) ;
					
			if($action == 'delete'){
				if($rights->_isActionAllowed('admin', 7, $uid))
        		{
        			$group->_makeForm() ;
        		}
			}
			if($action == 'submit')
			{
        			$group->_makeForm($gid, $_POST) ;
			}
 	    }
 	    $group->_makePage($session, $rights, $gid, $msgString) ;
 	}	
 		
}
catch(MsgException $e){

	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
 	
?>