<?php
require_once( '../../app/common/required.php') ;

 $session = Session::_GetInstance() ;
 $rights = ModuleRights::_GetInstance() ;

 try{
	 if(($rights->_isModuleAllowed(1, 1, $session->_getUid()))&&($rights->_isActionAllowed('admin', 5, $session->_getUid(), '_FONCTION_NOT_ALLOWED_')))
	 
	 {
	    if(isset($_GET['intcod']))
	    {
	      $intcod = $_GET['intcod'] ;
	    }else{
	      $intcod = 0 ;
	    }
	            
	    require_once( 'adminPerson.php') ; 
	    
	    $person = new AdminPerson($session, $rights, $intcod) ;
	    if($rights->_isActionAllowed('admin', 2, $session->_getUid()))
	    {
	    	$person->_makePageInfos() ;
	    }
	 }
 }
 catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	echo $msgString ;
 }
