<?php

require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/admin/adminSession.php') ;
require_once( 'adminLogin.php') ;
try{
	$session = AdminSession::_GetInstance() ;
	$uid = $session->_getUid();
 	$rights = ModuleRights::_GetInstance() ;
	$msgString = '' ;
	if(isset($_POST['aut_login']))
	{
		
		$action = 'submit' ;
	}else{
	   $action = 'create' ;
	}
	
	$person = new Adminlogin($session, $rights) ;
	    
	try{
		if($action == 'submit'){
			$person->_checkFormValues($_POST) ;
	      	$person->_setFormValues($_POST) ;
	      	$person->_update() ;
			$session->_sessionLogAction(5, '_LOGIN_PWD_UPDATE_') ;

    		header('Location: '.ROOT_APPL.'/app/admin/lst_login.php');
		    exit() ;
      	}else{
        	$person->_makeForm() ;    		
		}
	}
	catch(MsgException $e){
		$msgString = $e ->_getError($session) ;
		if($action == 'submit')
		{
			$person->_makeForm($_POST, $msgString) ;
 	    }
 	}	
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	echo $msgString ;
}
 	
?>