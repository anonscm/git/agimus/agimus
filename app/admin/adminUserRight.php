<?php
  require_once( DIR_WWW.ROOT_APPL.'/app/common/moduleRights.php') ;
  require_once( 'adminRight.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/langue.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/user.php') ;
/**
 * Cette classe permet l'affichage des diff�rentes vues possibles des intervenants
 * <p>Gestion des intervenants dans le module admin</p>
 * 
 * @name AdminUserRight
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright Capella Conseil 2009
 * @version 1.0.0
 * @package admin
 */
 
 class AdminUserRight extends AdminRight {
 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/

    /**
    * @var (String)
    * @desc fichier template de la page
    */
    protected $xtpl_file ;    
    /**
    * @var (String)
    * @desc fichier template du formulaire
    */
    protected $form_file ;
    
    /**
    * @var (String)
    * @desc fichier template du formulaire
    */
    protected $info_file ;
        
    /**
    * @var formString(String)
    * @desc fichier du formulaire apr�s traitement
    */
    protected $formString ;
    
    /**
    * @var lang(String)
    * @desc langue pour la session
    */
    private $lang ;
    /**
     * @var userArray (Array)
     * @desc Tableau des utilisateurs avec l'identifiant comme cl�
     */
    private $userArray;
    
    /**
     * @var userID (Int)
     * @desc Identifiant de l'utilisateur
     */    
    private $userID ;
    
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name AdminserRight::__construct()
    * @return void 
    */
    public function __construct($session, $rights) {
      $this->xtpl_file = 'rightuserlist.xtpl' ;
		$this->userID = 0 ;
      $this->lang = $session->lang;
      $this->xtpl_path = $this->lang.'/admin/';
      $user = new User() ;
      $this->userArray = $user->_getUserRightList($this->lang) ;  
    } 
    
    public function _setUserID($userID)
    {
    	$this->userID = $userID ;
    }

    /**
    * Insertion des donn�es dans le mod�le de page
    * 
    * <p>_makePage</p>
    * 
    * @name AdminRight::_makePage()
    * @param session (class)
    * @param rights (class)
    * @return string
    */ 
    public function _makePage($session, $rights, $view='user', $msg='') 
    {
    	$uid=$session->_getUid() ;
    	$menuleft = $session->_makeMenuLeft($rights) ;
      	$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
      	$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
      	$lang = $session->lang ;
        if($msg != '')
      	{
         	$xtpl->assign('MSG', $msg);
         	$xtpl->parse('main.msg');
      	}
      	$xtpl->assign('VIEW', $view ) ;

        $form = new Form('./lst_right.php') ;
      	foreach($this->userArray as $key=>$value)
      	{
      		$userlist[$key] = $this->userArray[$key]['user'] ;
      	}
      	$onSelect='OnChange="document.seluser.submit()"' ;
      	$xtpl->assign('USER_ID', $form->_mkSelect('user_id',$userlist, $this->userID, $onSelect)) ;
      	$xtpl->assign('USRID', $form->_mkInput('hidden', 'usrid', $this->userID)) ;
      	if($this->userID != 0)
      	{
      		$moduleArray = ModuleRights::_getModuleRights($lang, 0, $this->userID);
      		foreach($moduleArray as $key=>$value)
      		{
      			$xtpl->assign('MOD_ID', $key) ;
      			$rightArray = $moduleArray[$key]['right'] ; 
      			$actionArray = $moduleArray[$key]['action_right'] ;
      			$xtpl->assign('MOD_NAME', $moduleArray[$key]['title']);
      			
      			foreach ($rightArray as $key1=>$value1)
      			{
      				$user = $rightArray[$key1]['user'] ;
      				$gid =$user[$this->userID]['group'] ;
      				$userid = $user[$this->userID]['userid'] ;
      				      				
      				$modright[$row1['RIGHT_ID']]['user'] = $user ; 
      				
      				$xtpl->assign('RIGHT_NAME', $rightArray[$key1]['name']);
      				$xtpl->assign('MOD_RIGHT', $key.'|'.$key1);
      				$checked = '' ;
      				if($userid == $this->userID)
      				{
      					$checked = 'checked="checked"';	
      					if($gid != 0)
      					{
      						$checked .= 'disabled="disabled"' ;
      					}
      				}
      				$xtpl->assign('CHECKED', $checked) ;
      				$xtpl->parse('main.right.modlist.row');
      			}
      		    foreach ($actionArray as $key2=>$value2)
      			{
      				$user = $actionArray[$key2]['user'] ;
      				$gid =$user[$this->userID]['group'] ;
      				$userid = $user[$this->userID]['userid'] ;
      				$xtpl->assign('ACTION_NAME', $actionArray[$key2]['name']);
      				$xtpl->assign('ACT_RIGHT', $key.'|'.$key2);
      				
      				$checked = '' ;
      				if($userid == $this->userID)
      				{
      					$checked = 'checked="checked"';	
      					if($gid != 0)
      					{
      						$checked .= 'disabled="disabled"' ;
      					}
      				}
      				$xtpl->assign('CHECKED', $checked) ;
      				$xtpl->parse('main.right.modlist.actionrow');
      			}     			
      			$xtpl->parse('main.right.modlist');
      		}
      		$xtpl->parse('main.right');   			
      	}
      	$xtpl->parse('main');
      	$content = $xtpl->text('main') ;
      	$session->_makeMainPage($content, $menuleft) ;
    }

 
    /**
    * Destructeur
    * 
    * <p>Destruction de l'instance de classe</p>
    * 
    * @name AdminRight::__destruct()
    * @return void
    */
    public function __destruct() {
    
    }
 } 
?>