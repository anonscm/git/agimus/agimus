<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * Structure
 * <p>Structure</p>
 *
 * @name Structure
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright Capella Conseil 2009
 * @version 1.0.0
 * @package name
 * v1.2-PPR-28032011 ajout ID intra pour meilleure gestion
 * des mises à jour
 */

class Structure {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var $id(String)
	 * @desc $id
	 */
	private $id;

	/**
	 * @var $idIntra(String)
	 * @desc $idIntra
	 */
	private $idIntra;

	/**
	 * @var (String)
	 * @desc lib
	 */
	private $lib;
	/**
	 * @var (String)
	 * @desc dsc
	 */
	private $dsc;
	/**
	 * @var (Int)
	 * @desc identifiant de la structure m�re
	 * */
	private $parent;

	/**
	 * @var (Int)
	 * @desc identifiant de la categorie de structure
	 * */
	private $id_cat_struct;

	/**
	 * @var (string)
	 * @desc url d'export de l'agregat */
	private $urlExport;

	/**
	 * @var (string)
	 * @desc user pour export */
	private $user;

	/**
	 * @var (string)
	 * @desc pwd d'export de l'agregat */
	private $pwd;


	/**
	 * @var (Int)
	 * @desc niveau de l'arborescence */
	private $level;

	/**
	 * @var (Array)
	 * @desc tableau identifiant=>libell� */
	private $comboList;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Structure::__construct()
	 * @return void
	 */
	public function __construct($idRef='') {
		$this->id = '' ;
		$this->idIntra = '' ;
		$this->lib = '';
		$this->dsc = '';
		$this->parent = '' ;
		$this->id_cat_struct = 0;
		$this->urlExport='';
		$this->user='';
		$this->pwd='';
		$this->level = 0 ;
		$this->comboList = array() ;

		if ($idRef>'')
		{
			$this->id=$idRef;
			$this->_fill();
		}

	}

	/**
	 * Accesseurs en lecture
	 */
	/**
	 * @name Structure::_getId()
	 * @return String
	 */
	public function _getId()
	{
		return $this->id ;
	}

	/**
	 * @name Structure::_getIdIntra()
	 * @return String
	 */
	public function _getIdIntra()
	{
		return $this->idIntra ;
	}

	/**
	 * @name Structure::_getLib()
	 * @return string
	 */
	public function _getLib()
	{
		return $this->lib ;
	}
	/**
	 * @name Structure::_getDsc()
	 * @return string
	 */
	public function _getDsc()
	{
		return $this->dsc ;
	}
	/**
	 * @name Structure::_getParent()
	 * @return string
	 */
	public function _getParent()
	{
		return $this->parent ;
	}
	/**
	 * @name Structure::_getCatStruct()
	 * @return string
	 */
	public function _getCatStruct()
	{
		return $this->id_cat_struct ;
	}

	/**
	 * @name Structure::_getUrlExport()
	 * @return string
	 */
	public function _getUrlExport()
	{
		return $this->urlExport ;
	}

	/**
	 * @name Structure::_getUser()
	 * @return string
	 */
	public function _getUser()
	{
		return $this->user ;
	}

	/**
	 * @name Structure::_getPwd()
	 * @return string
	 */
	public function _getPwd()
	{
		return $this->pwd ;
	}

	/**
	 * @name Structure::_getLevel()
	 * @return int
	 */
	public function _getLevel()
	{
		return $this->level ;
	}
	/**
	 * @name Structure::_getComboList()
	 * @return array
	 */
	public function _getComboList()
	{
		return $this->comboList ;
	}
	/**
	 * Accesseurs en �criture
	 */
	/**
	 * @name Structure::_setId()
	 * @param $id (String)
	 * @return void
	 */
	public function _setId($id)
	{
		$this->id = $id ;
	}

	/**
	 * @name Structure::_setIdIntra()
	 * @param $idIntra (String)
	 * @return void
	 */
	public function _setIdIntra($idIntra)
	{
		$this->idIntra = $idIntra ;
	}

	/**
	 * @name Structure::_setLib()
	 * @param $lib (string)
	 * @return void
	 */
	public function _setLib($lib)
	{
		$this->lib = $lib ;
	}
	/**
	 * @name Structure::_setDsc()
	 * @param $dsc (string)
	 * @return void
	 */
	public function _setDsc($dsc)
	{
		$this->dsc = $dsc ;
	}
	/**
	 * @name Structure::_setParent()
	 * @param $parent (string)
	 * @return void
	 */
	public function _setParent($parent)
	{
		$this->parent = $parent ;
	}
	/**
	 * @name Structure::_setCatStruct()
	 * @param $catStruct (int)
	 * @return void
	 */
	public function _setCatStruct($catStruct)
	{
		$this->id_cat_struct = $catStruct ;
	}

	/**
	 * @name Structure::_setUrlExport()
	 * @param $urlExport (string)
	 * @return void
	 */
	public function _setUrlExport($urlExport)
	{
		$this->urlExport = $urlExport ;
	}

	/**
	 * @name Structure::_setUser()
	 * @param $user (string)
	 * @return void
	 */
	public function _setUser($user)
	{
		$this->user = $user ;
	}

	/**
	 * @name Structure::_setPwd()
	 * @param $pwd (string)
	 * @return void
	 */
	public function _setPwd($pwd)
	{
		$this->pwd = $pwd ;
	}

	/**
	 * Compte du nombre d'occurence de l'objet Structure dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name Structure::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_struct ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Cr�ation d'un Structure dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name Structure::_create()
	 * @return void
	 */
	public function _create()
	{
		// contr�les pr�alables
		if ($this->id==$this->parent)
		{
			throw new MsgException('_ERROR_ID_EQUAL_TO_PARENT_')  ;
		}

		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT ID FROM t_struct ';
		$sql .= 'WHERE ID=\''.AddSlashes($this->id).'\' ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				throw new MsgException('_ERROR_ID_ALREADY_EXISTS_')  ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
			
		$sql  = 'INSERT INTO t_struct VALUES( ';
		$sql .= '\''.AddSlashes($this->id).'\', ';
		$sql .= '\''.AddSlashes($this->lib).'\', ';
		$sql .= '\''.AddSlashes($this->dsc).'\', ';
		$sql .= '\''.AddSlashes($this->parent).'\', ';
		$sql .= '\''.AddSlashes($this->id_cat_struct).'\', ';
		$sql .= '\''.AddSlashes($this->urlExport).'\', ';
		$sql .= '\''.AddSlashes($this->user).'\', ';
		$sql .= '\''.AddSlashes($this->pwd).'\', ';
		$sql .= '\''.AddSlashes($this->idIntra).'\') ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise � jour d'un Structure dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name Structure::_update()
	 * @return void
	 */
	public function _update()
	{
		// contr�les pr�alables
		if ($this->id==$this->parent)
		{
			throw new MsgException('_ERROR_ID_EQUAL_TO_PARENT_')  ;
		}

	//v1.2-PPR-28032011 ajout de l'id intra pour mise à jour facilitée de l'ID
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_struct SET ';
		$sql .= 'ID = \''.AddSlashes($this->id).'\', ';
		$sql .= 'LIB = \''.AddSlashes($this->lib).'\', ';
		$sql .= 'DSC = \''.AddSlashes($this->dsc).'\', ';
		$sql .= 'PARENT = \''.AddSlashes($this->parent).'\', ';
		$sql .= 'ID_CAT_STRUCT = \''.AddSlashes(AddSlashes($this->id_cat_struct)).'\', ';
		$sql .= 'URL_EXPORT = \''.AddSlashes(AddSlashes($this->urlExport)).'\', ';
		$sql .= 'USER = \''.AddSlashes(AddSlashes($this->user)).'\', ';
		$sql .= 'PWD = \''.AddSlashes(AddSlashes($this->pwd)).'\', ';
		$sql .= 'ID_INTRA = \''.AddSlashes(AddSlashes($this->id)).'\' ';
		$sql .= 'WHERE ID=\''.AddSlashes($this->idIntra).'\' ';

//		echo $sql;exit();
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un Structure dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name Structure::_delete()
	 * @param $structref(int)
	 * @return void
	 */
	public function _delete($structId)
	{
		// Controle si la structure n'a pas de fils
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_struct ';
		$sql .= 'WHERE PARENT = \''.$structId.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			throw new MsgException('_USED_OBJECT_CANNOT_BE_DELETED_') ;
		}else{
			$sql  = 'DELETE FROM t_struct ';
			$sql .= 'WHERE ID = \''.$structId.'\' ' ;
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}
	}
	/**
	 * R�cup�ration de la liste des Structures
	 *
	 * <p>Liste des Structures</p>
	 *
	 * @name Structure::_getList()
	 * @return array
	 */
	public function _getList($idStruct='')
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_struct ' ;

		if ($idStruct!='')
		$sql .= ' WHERE ID = \''.$idStruct.'\' ';
		$sql .= 'ORDER BY LIB ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['LIB'] = StripSlashes($row['LIB']) ;
				$listArray[$row['ID']]['DSC'] = StripSlashes($row['DSC']) ;
				$listArray[$row['ID']]['PARENT'] = StripSlashes($row['PARENT']) ;
				$listArray[$row['ID']]['ID_CAT_STRUCT'] = $row['ID_CAT_STRUCT'] ;
				$listArray[$row['ID']]['URL_EXPORT'] = StripSlashes($row['URL_EXPORT']) ;
				$listArray[$row['ID']]['USER'] = StripSlashes($row['USER']) ;
				$listArray[$row['ID']]['PWD'] = StripSlashes($row['PWD']) ;
				$listArray[$row['ID']]['ID_INTRA'] = StripSlashes($row['ID_INTRA']) ;
				$this->comboList[$row['ID']] = StripSlashes($row['LIB']) ;
			}
		}
		return $listArray ;
	}

	/**
	 * R�cup�ration de l'arborescence de la liste des Structures
	 *
	 * <p>Liste arborecente des Structures</p>
	 *
	 * @name Structure::_getTreeList()
	 * @return array
	 */
	public function _getTreeList($supp='')
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_struct ' ;
		$sql .= 'WHERE PARENT = \'\' ' ;
		$sql .= 'ORDER BY LIB ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{

			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['libelle'] = StripSlashes($row['LIB']) ;
				$listArray[$row['ID']]['tree'] = $this->_getTree($row['ID']) ;
				$listArray[$row['ID']]['cat_struct'] = StripSlashes($row['ID_CAT_STRUCT']) ;
			}
		}
		return $listArray ;
	}

	public function _getTree($structId, $sizetree=1)
	{
		$treesize = $sizetree ;
		$treeArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sqlt  = 'SELECT * FROM t_struct ' ;
		$sqlt .= 'WHERE PARENT = \''.$structId.'\' ' ;
		$sqlt .= 'ORDER BY LIB ' ;
		try{
			$rest = $maconnexion->_bddQuery($sqlt) ;
			if($maconnexion->_bddNumRows($rest))
			{
				$treesize++ ;
				while($rowt = $maconnexion->_bddFetchAssoc($rest))
				{
					$treeArray[$rowt['PARENT']][$rowt['ID']]['libelle'] = StripSlashes($rowt['LIB']) ;
					$treeArray[$rowt['PARENT']][$rowt['ID']]['cat_struct'] = StripSlashes($rowt['ID_CAT_STRUCT']) ;
					$subtree = $this->_getTree($rowt['ID'], $treesize) ;
					if($subtree != NULL)
					{
						$treesize++ ;
						$treeArray[$rowt['PARENT']][$rowt['ID']]['tree'] = $subtree ;
					}
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($treesize>$this->level)
		{
			$this->level = $treesize ;
		}
		return $treeArray ;
			
	}

	/**
	 * R�cup�ration de la liste des Structures racines dans un tableau index=>nom
	 * pour alimenter des listes de s�lection
	 *
	 * <p>getComboRootList</p>
	 *
	 * @name Structure::_getComboRootList()
	 * @return array
	 */
	public function _getComboRootList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT ID, LIB FROM t_struct ' ;
		$sql  .= 'WHERE PARENT = \'\' ';
		$sql  .= 'ORDER BY LIB ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		while($row = $maconnexion->_bddFetchAssoc($res))
		{
			$listArray[$row['ID']]= StripSlashes($row['LIB']) ;
		}
		return $listArray ;
	}

	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name Dimension::_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id=='') return false;

		$listArray=$this->_getList($this->id);
		$this->lib = $listArray[$this->id]['LIB'];
		$this->dsc = $listArray[$this->id]['DSC'];
		$this->parent = $listArray[$this->id]['PARENT'];
		$this->id_cat_struct = $listArray[$this->id]['ID_CAT_STRUCT'];
		$this->urlExport = $listArray[$this->id]['URL_EXPORT'];
		$this->user = $listArray[$this->id]['USER'];
		$this->pwd = $listArray[$this->id]['PWD'];
		$this->idIntra = $listArray[$this->id]['ID_INTRA'];
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Structure::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>