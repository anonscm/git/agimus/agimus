<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * <p>Application</p>
 *
 * @name  Application
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

class Application {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc Identifiant
	 */
	private $id;

	/**
	 * @var (String)
	 * @desc Libelle
	 */
	private $lib;

	/**
	 * @var (String)
	 * @desc Decription
	 */
	private $dsc;

	/**
	 * @var (String)
	 * @desc Url Type (reperage)
	 */
	private $urlType;

	/**
	 * @var (Int)
	 * @desc Identifiant Service interne
	 */
	private $idSrvIntra;

	/**
	 * @var (Int)
	 * @desc ID Structure
	 */
	private $idStruct;



	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. methodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>creation de l'instance de la classe</p>
	 *
	 * @name Application::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		$this->id=0;
		$this->lib='';
		$this->dsc='';
		$this->urlType='';
		$this->idSrvIntra=0;
		$this->idStruct=0;

		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}

	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name Application::_getId()
	 * @return Int
	 */
	public function _getId() {
		return $this->id ;
	}

	/**
	 * @name Application::_getLib()
	 * @return String
	 */
	public function _getLib() {
		return $this->lib ;
	}

	/**
	 * @name Application::_getDsc()
	 * @return String
	 */
	public function _getDsc() {
		return $this->dsc ;
	}

	/**
	 * @name Application::_getUrlType()
	 * @return String
	 */
	public function _getUrlType() {
		return $this->urlType ;
	}

	/**
	 * @name Application::_getIdSrvIntra()
	 * @return Int
	 */
	public function _getIdSrvIntra() {
		return $this->idSrvIntra ;
	}

	/**
	 * @name Application::_getIdStruct()
	 * @return Int
	 */
	public function _getIdStruct() {
		return $this->idStruct ;
	}



	/**
	 * Accesseurs en ecriture
	 */

	/**
	 * @name Application::_setId()
	 * @param $id (Int)
	 * @return void
	 */
	public function _setId($id) {
		$this->id  = $id ;
	}

	/**
	 * @name Application::_setLib()
	 * @param $lib (String)
	 * @return void
	 */
	public function _setLib($lib) {
		$this->lib  = $lib ;
	}

	/**
	 * @name Application::_setDsc()
	 * @param $dsc (String)
	 * @return void
	 */
	public function _setDsc($dsc) {
		$this->dsc  = $dsc ;
	}

	/**
	 * @name Application::_setUrlType()
	 * @param $urlType (String)
	 * @return void
	 */
	public function _setUrlType($urlType) {
		$this->urlType  = $urlType ;
	}

	/**
	 * @name Application::_setIdSrvIntra()
	 * @param $idSrvIntra (Int)
	 * @return void
	 */
	public function _setIdSrvIntra($idSrvIntra) {
		$this->idSrvIntra  = $idSrvIntra ;
	}

	/**
	 * @name Application::_setIdStruct()
	 * @param $idStruct (Int)
	 * @return void
	 */
	public function _setIdStruct($idStruct) {
		$this->idStruct  = $idStruct ;
	}



	/**
	 * Compte du nombre d'occurence de l'objet Application dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name Application::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_application ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * renvoit l'id d'une application sur le libelle
	 *
	 * <p>_existsRef</p>
	 *
	 * @name Application::_existsRef()
	 * @param $stampStart string
	 * @return int
	 */
	public function _existsRef($lib,$strict=0,$noDouble=0)
	{
		$idRef = 0 ;
		$listArray=array();
		if (trim($lib)=="") return $listArray;
		$debug=false;

		$maconnexion = MysqlDatabase::GetInstance() ;

		if ($strict)
		{
			$sql = 'SELECT id ';
			$sql .= ' FROM t_application ';
			$sql .= ' WHERE UPPER(lib) = \''.strtoupper($lib).'\' ';

			try{
				$res = $maconnexion->_bddQuery($sql) ;
				$row = $maconnexion->_bddFetchAssoc($res);
				$idRef  = $row['id'] ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			return $idRef;
		}
		else
		{
			// recherche de concordance entre libell� et url_type
			$sql = 'SELECT t_application.*, LENGTH(URL_TYPE) as url_length  ';
			$sql .= ' FROM t_application ';
			$sql .= ' WHERE URL_TYPE <>\'\' ';
			//v1.2-PPR-28032011 tri par longueur décroissante de l'url type
			$sql .= ' ORDER BY url_length DESC ';
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

			if($maconnexion->_bddNumRows($res))
			{
				while($row = $maconnexion->_bddFetchAssoc($res))
				{
					$row=keyToUpper($row);
					if ($debug) echo "\r\ncomparatif [$lib] et ['".$row['URL_TYPE']."']";
					//if (strstr($lib,$row['URL_TYPE']))
					if (preg_match($row['URL_TYPE'],$lib))
					{
					$listArray[]=$row['ID'];
					if ($debug) echo "----> Trouvé : [".$row['ID']."]";
					//v1.2-PPR-28032011 retour direct dés la première trouvée
					if ($noDouble)
						return $listArray;
					}
				}
			}
			return $listArray;
		}
	}


	/**
	 * regroupe les applications répondant à une expression régulière
	 *
	 * <p>_regroupe</p>
	 *
	 * @name Application::_regroupe()
	 * @param $idAppli id
	 * @return int
	 */
	public function _regroupe()
	{

		require_once (DIR_WWW.ROOT_APPL.'/app/common/cnx.php') ;
		if (!$this->id>0) return false;
		if ($this->urlType=='') return true;
		$debug=false;

		$maconnexion = MysqlDatabase::GetInstance() ;

		// recherche de concordance entre libellé et url_type
		$sql = 'SELECT *  ';
		$sql .= ' FROM t_application ';
		$sql .= ' WHERE URL_TYPE <>\'\' ';
		$sql .= ' AND ID <> \''.$this->id.'\' ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}

		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$row=keyToUpper($row);
				if ($debug) echo "<br>comparatif [".$this->urlType."] et ['".$row['DSC']."']";
				if (preg_match($this->urlType,$row['DSC']))
				{
					if ($debug) echo "----> Trouvé : [".$row['LIB']."(".$row['ID']."]";
					
					// mise à jour des lignes de connexion en conséquence
					$nbMaj=Cnx::_updateAppli($row['ID'],$this->id);
										
					// suppression de l'application correspondante
					$newAppl=new Application($row['ID']);
					$newAppl->_delete($row['ID']);
				}
			}
		}
		return true;
	}

	/**
	 * Cr�ation d'un Application dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name Application::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM t_application ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}

		$sql  = 'INSERT INTO t_application VALUES( ';
		$sql .= '\''.AddSlashes($this->id).'\', ';
		$sql .= '\''.AddSlashes($this->lib).'\', ';
		$sql .= '\''.AddSlashes($this->dsc).'\', ';
		$sql .= '\''.AddSlashes($this->urlType).'\', ';
		$sql .= '\''.AddSlashes($this->idSrvIntra).'\', ';
		$sql .= '\''.AddSlashes($this->idStruct).'\' ';

		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise � jour d'un Application dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name Application::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_application SET ';
		$sql .= 'ID = \''.AddSlashes($this->id).'\', ';
		$sql .= 'LIB = \''.AddSlashes($this->lib).'\', ';
		$sql .= 'DSC = \''.AddSlashes($this->dsc).'\', ';
		$sql .= 'URL_TYPE = \''.AddSlashes($this->urlType).'\', ';
		$sql .= 'ID_SRV_INTRA = \''.AddSlashes($this->idSrvIntra).'\', ';
		$sql .= 'ID_STRUCT = \''.AddSlashes($this->idStruct).'\' ';

		$sql .= 'WHERE id=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un Application dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name Application::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		require_once (DIR_WWW.ROOT_APPL.'/app/common/cnx.php') ;

		//contrôle préalable appli non rattachée
		$nbCnx=Cnx::_countRef($idRef);

		if ($nbCnx==0)
		{
			$maconnexion = MysqlDatabase::GetInstance() ;
			$sql  = 'DELETE FROM t_application ';
			$sql .= 'WHERE id = \''.$idRef.'\' ' ;
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}
	}
	/**
	 * R�cup�ration de la liste des Applications
	 *
	 * <p>Liste des Application</p>
	 *
	 * @name Application::_getList()
	 * @return array
	 */
	public function _getList($idRegr=0)
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_application WHERE 1 ' ;
		//v1.5-PPr-#000 suppression message notice
		if ((isset($this->id)) and ($this->id>0))
			$sql.= ' AND id = \''.$this->id.'\' ';
		if ($idRegr>0)
		$sql.= ' AND id_srv_intra = \''.$idRegr.'\' ';
		$sql .= 'ORDER BY LIB ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['ID'] = StripSlashes($row['ID']) ;
				$listArray[$row['ID']]['LIB'] = StripSlashes($row['LIB']) ;
				$listArray[$row['ID']]['DSC'] = StripSlashes($row['DSC']) ;
				$listArray[$row['ID']]['URL_TYPE'] = StripSlashes($row['URL_TYPE']) ;
				$listArray[$row['ID']]['ID_SRV_INTRA'] = StripSlashes($row['ID_SRV_INTRA']) ;
				$listArray[$row['ID']]['ID_STRUCT'] = StripSlashes($row['ID_STRUCT']) ;

			}
		}
		return $listArray ;
	}

	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name Application:_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id==0) return false;

		$listArray=$this->_getList();
		$this->_setLib($listArray[$this->id]['LIB']);
		$this->_setDsc($listArray[$this->id]['DSC']);
		$this->_setUrlType($listArray[$this->id]["URL_TYPE"]);
		$this->_setIdSrvIntra($listArray[$this->id]["ID_SRV_INTRA"]);
		$this->_setIdStruct($listArray[$this->id]["ID_STRUCT"]);
	}

	/**
	 * Récupération de la liste des Alertes
	 *
	 * <p>Liste des Alertes</p>
	 *
	 * @name Application::_getAlerts()
	 * @return array
	 */
	public function _getAlerts($typeAlert=1)
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_application WHERE 1 ' ;
		if ($typeAlert==1)
			$sql.= ' AND id_srv_intra = 0 ';
		if ($typeAlert==2)
			$sql.= ' AND lib like  \'%~%\' ';
		$sql .= 'ORDER BY id desc ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]=$row['LIB'];				
			}
		}
		return $listArray ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Application::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>