<?php
/**
 * 
 * <p>Person</p>
 * 
 * @name Person
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package common
 */
 
 class Person {
 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * @var (Int)
    * @desc identifiant de l'intervenant
    */
    protected $intCod;
    /**
    * @var (String)
    * @desc trigramme de l'intervenant
    */
    protected $intTrg;
    /**
    * @var (String)
    * @desc nom
    */
    protected $nom;
    /**
    * @var (String)
    * @desc pr�nom */
    protected $prenom;
    /**
    * @var (Int)
    * @desc identifiant de la structure auquel il est rattach� */
    protected $structId;
    /**
    * @var (String)
    * @desc adresse mail */
    protected $email;
    /**
    * @var (Int)
    * @desc intervenant dont d�pend celui que l'on regarde */
    protected $intPere;
        /**
    * @var (String)
    * @desc �tat : supprim� ou non */
    protected $indSupp;
    /**
    * @var (Boolean)
    * @desc utilisateur
    */
    protected $userYN;
    
    /**
    * @var $login (String)
    * @desc nom pour la connexion
    */    
    protected $login;
    
    /**
    * @var pwd(String)
    * @desc mot de passe pour la connexion
    */
    protected $pwd;
    
    
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name Person::__construct()
    * @return void 
    */
    public function __construct($intcod=0) {
    	$this->intCod = $intcod ;
		$this->intTrg = '' ;      
		$this->nom = '' ; 
		$this->prenom = '' ; 
		$this->structId = 0 ; 
		$this->email = '' ; 
		$this->intPere = 0 ; 
		$this->indSupp = '' ; 
		$this->userYN = 0 ; 
		$this->login = '' ;	
		$this->pwd ='' ;
    } 

    /**
    * Accesseurs en lecture
    */ 
    /** 
    * @name Person::_getIntCod()
    * @return int 
    */
    public function _getIntCod()
    {
      return $this->intCod ;
    }
    /** 
    * @name Person::_getIntTrg()
    * @return string 
    */
    public function _getIntTrg()
    {
      return $this->intTrg ;
    }
      
    /** 
    * @name Person::_getNom()
    * @return string 
    */
    public function _getNom()
    {
      return $this->nom  ; 
    }
    /** 
    * @name Person::_getPrenom()
    * @return string 
    */
    public function _getPrenom()
    {
      return $this->prenom ; 
    }
    /** 
    * @name Person::_getStructId()
    * @return int
    */
    public function _getStructId()
    {
      return $this->structId ; 
    }

    /** 
    * @name Person::_getEmail()
    * @return string 
    */
    public function _getEmail()
    {
      return $this->email ; 
    }
    /** 
    * @name Person::_getIntPere()
    * @return int 
    */
    public function _getIntPere()
    {
      return $this->intPere; 
    }
    /** 
    * @name Person::_getIndSupp()
    * @return string 
    */
    public function _getIndSupp()
    {
      return $this->indSupp ; 
    }
     /** 
    * @name Person::_getUserYN()
    * @return int 
    */
    public function _getUserYN()
    {
      return $this->userYN ; 
    }    
    /**
    * Accesseurs en �criture
    */ 
    /** 
    * @name Person::_setIntcod()
    * @param $intcod (int)
    * @return void 
    */
     public function _setIntCod($itcd)
    {
      $this->intCod  = $itcd ;
    }
    /** 
    * @name Person::_setIntTrg()
    * @param $str (string)
    * @return void 
    */
    public function _setIntTrg($str)
    {
      $this->intTrg = $str ;
    }
      
    /** 
    * @name Person::_setNom()
    * @param $str (string)
    * @return void 
    */
    public function _setNom($str)
    {
      $this->nom = $str  ; 
    }
    /** 
    * @name Person::_setPrenom()
    * @param $str (string)
    * @return void 
    */
    public function _setPrenom($str)
    {
      $this->prenom = $str ; 
    }
    /** 
    * @name Person::_setStructId()
    * @param $structid (int)
    * @return void 
    */
    public function _setStructId($structid)
    {
      $this->structId = $structid ; 
    }
    /** 
    * @name Person::_setEmail()
    * @param $str (string)
    * @return void 
    */
    public function _setEmail($str)
    {
      $this->email = $str ; 
    }
    /** 
    * @name Person::_setIntPere()
    * @param $perid (int)
    * @return void  
    */
    public function _setIntPere($perid)
    {
      $this->intPere = $perid; 
    }
    /** 
    * @name Person::_setIndSupp()
    * @param $str (string)
    * @return void 
    */
    public function _setIndSupp($str)
    {
      $this->indSupp = $str ; 
    }
    /** 
    * @name Person::_setUserYN()
    * @param $yn (int)
    * @return void 
    */
    public function _setUserYN($yn)
    {
      $this->userYN = $yn ; 
    }
    
     public function _setLogin($login)
    {
      $this->login = $login ; 
    }
    
     /** 
    * @name Person::_setPwd()
    * @param $pwd (string)
    * @return void 
    */
    public function _setPwd($pwd)
    {
      $this->pwd = $pwd ;
    }
    
    /**
    * Compte du nombre d'occurence de personne dans la bdd
    * 
    * <p>_countRef</p>
    * 
    * @name Person::_countRef()
    * @return int 
    */
    public function _countRef()
    {
      $nbr = 0 ;
      $maconnexion = MysqlDatabase::GetInstance() ;
      $sql = 'SELECT count(INT_COD) AS nbr ';
      $sql .= 'FROM t_int ';
      $sql .= 'WHERE IND_SUPP=\'\' ';
      $sql .= 'AND INT_COD > 1 ';
    	try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
      $row = $maconnexion->_bddFetchAssoc($res);  // Recuperation de la requete SQL
      $nbr  = $row['nbr'] ;
      return $nbr;
    }
   
    /**
    * Cr�ation d'un intervenant dans la bdd
    * 
    * <p>_create</p>
    * 
    * @name Person::_create()
    * @return void 
    */
    public function _create()
    {
      $maconnexion = MysqlDatabase::GetInstance() ;
      $sql = 'SELECT MAX(int_cod) as intcod FROM t_int';
    	try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
      if($maconnexion->_bddNumRows($res) >0)
      {
        $row = $maconnexion->_bddFetchAssoc($res) ;
        $this->intCod= $row['intcod']+1 ;
      }else{
        $this->intCod = 1 ;
      }     
     
      $sql  = 'INSERT INTO t_int SET ';
      $sql .= 'INT_COD = \''.$this->intCod.'\', ';      
      $sql .= 'INT_TRG = \''.AddSlashes($this->intTrg).'\', ';       
      $sql .= 'INT_NOM = \''.AddSlashes($this->nom).'\', ';
      $sql .= 'INT_PRE = \''.AddSlashes($this->prenom).'\', ';
      $sql .= 'INT_EMA = \''.$this->email.'\', ';
      $sql .= 'INT_PERE = \''.$this->intPere.'\', ';
      $sql .= 'IND_SUPP = \''.$this->indSupp.'\' ';
    	try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
      	//Entr�e dans la table t_aut
      	if($this->userYN == 1)
      	{
			$sql = 'INSERT INTO t_aut SET ';
			$sql .= 'INT_COD = \''.$this->intCod.'\', '; 
			$sql .= 'AUT_USER_LOGIN = \''.AddSlashes($this->login).'\', '; 
			$sql .= 'AUT_PWD = \''.md5($this->pwd).'\', '; 
			$sql .= 'AUT_STA=\'1\' ' ; 
      	    try{
				$res = $maconnexion->_bddQuery($sql) ;
      		}
      		catch(MsgException $e){
      			$msgString = $e ->_getError();
      			throw new MsgException($msgString, 'database') ;
      		}
      	}
    }
 
    /**
    * Mise � jour d'un intervenant dans la bdd
    * 
    * <p>_update</p>
    * 
    * @name Person::_update()
    * @return void 
    */ 
    public function _update()
    {
		$maconnexion = MysqlDatabase::GetInstance() ; 
               
      	$sql  = 'UPDATE t_int SET ';
      	$sql .= 'INT_TRG = \''.$this->intTrg.'\', ';       
      	$sql .= 'INT_NOM = \''.AddSlashes($this->nom).'\', ';
      	$sql .= 'INT_PRE = \''.AddSlashes($this->prenom).'\', ';
      	$sql .= 'ID_STRUCT = \''.$this->structId.'\', ';
      	$sql .= 'INT_EMA = \''.$this->email.'\', ';
      	$sql .= 'INT_PERE = \''.$this->intPere.'\', ';
      	$sql .= 'IND_SUPP = \''.$this->indSupp.'\' ';
      	$sql .= 'WHERE INT_COD = \''.$this->intCod.'\' ';
    	try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
      	
      	//On v�rifie que l'intervenant est un utilisateur
        $sql  = 'SELECT * FROM t_aut ' ;
	   	$sql  .= 'WHERE INT_COD = \''.$this->intCod.'\' ' ;
      	try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
      	try{
		   	if($maconnexion->_bddNumRows($res) >0)
	      	{
		        $sql = 'UPDATE t_aut SET ';
				$sql .= 'AUT_STA=\''.$this->userYN.'\' ' ;
				$sql .= 'WHERE INT_COD = \''.$this->intCod.'\' ';
				$res = $maconnexion->_bddQuery($sql) ;
	
	      	}else{
				$sql = 'INSERT INTO t_aut SET ';
				$sql .= 'INT_COD = \''.$this->intCod.'\', '; 
				$sql .= 'AUT_USER_LOGIN = \''.AddSlashes($this->prenom.$this->nom).'\', '; 
				$sql .= 'AUT_PWD = \''.md5($this->intTrg).'\', ';
				$sql .= 'DATE_PWD = \''.date('Y-m-d').'\', ';
				$sql .= 'AUT_STA=\'1\' ' ;
				$res = $maconnexion->_bddQuery($sql) ;
				//historique des mots de passe
				$sql = 'INSERT INTO user_pwd_history SET ' ; 
				$sql .= 'INT_COD = \''.$this->intCod.'\', ';
		    	$sql .= 'AUT_PWD = \''.md5($this->pwd).'\', ';
		    	$sql .= 'DATE_PWD = \''.date('Y-m-d').'\' ';
		    	$res = $maconnexion->_bddQuery($sql) ;
	      	}

      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
    }
    
    /**
    * R�cup�ration de la liste des intervenants activ�s
    * 
    * <p>libelle</p>
    * 
    * @name Person::_getList()
    * @param int_cod (Int)
    * @return array
    */    
    public function _getList($supp='')
    {
      $listArray = array() ;
      $maconnexion = MysqlDatabase::GetInstance() ;
      $sql  = 'SELECT * FROM t_int ' ;
      $sql  .= 'WHERE INT_COD > 1 ' ;     
      $sql  .= 'AND IND_SUPP = \''.$supp.'\' ' ;
      $sql  .= 'ORDER BY INT_NOM ' ;
    	try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}

      while($row = $maconnexion->_bddFetchAssoc($res))
      {
        $listArray[$row['INT_COD']]['INT_NOM'] = StripSlashes($row['INT_NOM']) ; 
        $listArray[$row['INT_COD']]['INT_PRE'] = $row['INT_PRE'] ; 
        $listArray[$row['INT_COD']]['INT_TRG'] = $row['INT_TRG'] ; 
        $listArray[$row['INT_COD']]['ID_STRUCT'] = $row['ID_STRUCT'] ; 
        $listArray[$row['INT_COD']]['INT_EMA'] = $row['INT_EMA'] ;
        $listArray[$row['INT_COD']]['INT_PERE'] = $row['INT_PERE'] ;
        $listArray[$row['INT_COD']]['IND_SUPP'] = $row['IND_SUPP'] ;
        
        //On v�rifie que l'intervenant est un utilisateur
        $sql2  = 'SELECT * FROM t_aut ' ;
	   	$sql2  .= 'WHERE INT_COD = \''.$row['INT_COD'].'\' ' ;
      	try{
			$res2 = $maconnexion->_bddQuery($sql2) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
	   	if($maconnexion->_bddNumRows($res2) >0)
	   	{
	   		$row2 = $maconnexion->_bddFetchAssoc($res2)  ;
	   		$listArray[$row['INT_COD']]['USER_YN'] = 1 ;
	   		$listArray[$row['INT_COD']]['LOGIN'] = StripSlashes($row2['AUT_USER_LOGIN']) ;
	   		
	   	}else{
	   		$listArray[$row['INT_COD']]['USER_YN'] = 0 ;
	   	}
      }
      return $listArray ;
    }

        /**
    * R�cup�ration de la liste des intervenants activ�s
    * 
    * <p>libelle</p>
    * 
    * @name Person::_getList()
    * @param int_cod (Int)
    * @return array
    */    
    public function _getComboList()
    {
		$listArray = array() ;
	    $maconnexion = MysqlDatabase::GetInstance() ;
	    $sql  = 'SELECT INT_COD, INT_NOM, INT_PRE FROM t_int ' ;
	    $sql  .= 'WHERE INT_COD > 1 ' ;     
	    $sql  .= 'AND IND_SUPP = \'\' ' ;
	    $sql  .= 'ORDER BY INT_NOM ' ;      
    	try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}

      while($row = $maconnexion->_bddFetchAssoc($res))
      {
        $listArray[$row['INT_COD']]= StripSlashes($row['INT_NOM'].' '.$row['INT_PRE']) ; 
      }
      return $listArray ;
    }
    
    /**
    * Mise � jour d'un intervenant dans la bdd : d�sactivation
    * 
    * <p>_delete</p>
    * 
    * @name Person::_delete()
    * @param $int_cod (int)
    * @return String
    */ 
    public function _deactivate($int_cod)
    {
    	if($int_cod == 1)
    	{
    	 	throw new MsgException('_ERROR_DELETE_SUPERADMIN_') ;	
    	}
		$maconnexion = MysqlDatabase::GetInstance() ;
		 // contr�le si l'intervenant est utilis� dans un workgroup
 		$sql='select count(r_aff_gdt.gdt_ref) AS num FROM r_aff_gdt ';
 		$sql.=' where r_aff_gdt.ind_supp=\'\' ';
 		$sql.=' and r_aff_gdt.int_cod = \''.$int_cod.'\' ';
        try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
 		$row = $maconnexion->_bddFetchAssoc($res)  ;
	   	if($row['num'] >0)
	   	{
 			throw new MsgException('_ERROR_DELETE_PERSON_IN_WORKGROUP_') ;
  		}

 		// contr�le si l'intervenant est utilis� dans une ou plusieurs taches
 		$sql='select count(r_tac_int.tac_ref) AS num FROM r_tac_int ';
 		$sql.=' where r_tac_int.int_cod = \''.$int_cod.'\' ';
     	$res = $maconnexion->_bddQuery($sql) ;
    	try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
	   	if($row['num'] >0)
	   	{
 			throw new MsgException('_ERROR_DELETE_PERSON_IN_TASK_') ;
  		}
		$sql  = 'UPDATE t_int SET ';
		$sql .= 'IND_SUPP =\'D\',  ';
		$sql .= 'INT_DAT_SOR =\''.date('Y-m-d').'\'  ';  
		$sql .= 'WHERE int_cod = \''.$int_cod.'\' ' ;
        try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
		// Mise � jour dans la table T_AUT
		$sql  = 'UPDATE t_aut SET ';
		$sql .= 'AUT_STA =\'0\' ';     
		$sql .= 'WHERE int_cod = \''.$int_cod.'\' ' ;
        try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
    }
    
     /**
    * Mise � jour d'un intervenant dans la bdd : r�activation
    * 
    * <p>_restore</p>
    * 
    * @name Person::_restore()
    * @param $int_cod (int)
    * @return void
    */ 
    public function _restore($int_cod)
    {
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_int SET ';
		$sql .= 'IND_SUPP =\'\',  ';
		$sql .= 'INT_DAT_SOR =\'0000-00-00\'  ';  
		$sql .= 'WHERE int_cod = \''.$int_cod.'\' ' ;
    	try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
		 	
		// Mise � jour dans la table T_AUT
		$sql  = 'UPDATE t_aut SET ';
		$sql .= 'AUT_STA =\'1\' ';     
		$sql .= 'WHERE int_cod = \''.$int_cod.'\' ' ;
    	try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
    }
    
 	/**
    * R�cup�re les absences d'un intervenant entre deux dates (pour un role donn�)
    * 
    * <p>_GetIntAbs</p>
    * 
    * @name Person::_GetIntAbs()
    * @param $int_cod (int)
    * 	Identifiant de l'intervenant
    * @param $mindate (int)
    * 	Date borne inf�rieure, au format datetime
    * @param $maxdate (int)
    * 	Date borne sup�rieure, au format datetime
    * @param $type (string)
    * @param $role (int)
    * @return int
    */ 
	function _GetIntAbs($int_cod,$mindate,$maxdate,$type='',$role=0)
	{
		$sum_etp_aff=0;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT SUM(AFF_ETP_VAL) as sumetp ';
		$sql .= 'FROM t_aff WHERE UNIX_TIMESTAMP(CAL_DAT) >=\''.$mindate.'\' '; 
		$sql .= 'AND UNIX_TIMESTAMP(CAL_DAT)<= \''.$maxdate.'\' and INT_COD=\''.$int_cod.'\' ';
		if ($type!='')
		{
			$sql .= 'AND TYP_AFF_COD=\''.$type.'\' ';
		}
		if ($role>0)
		{
			$sql .='AND ROLE_REF=\''.$role.'\' ';
		}
	    try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
      		$sum_etp_aff= $row['sumetp'];
      	}
	    catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
		return $sum_etp_aff;
	}

    /**
    * Destructeur
    * 
    * <p>Destruction de l'instance de classe</p>
    * 
    * @name Person::__destruct()
    * @return void
    */
    public function __destruct() {
    }
 } 
?>