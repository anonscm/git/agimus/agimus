<?php
require_once(DIR_WWW.ROOT_APPL.'/app/common/xmlIO.php');
require_once(DIR_WWW.ROOT_APPL.'/app/common/indicateur.php');
/**
 *
 * <p>IndicXmlIO</p>
 *
 * @name Indicateur au format XML
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright Capella Conseil 2009
 * @version 1.0.0
 * @package name
 */
class IndicXmlIO extends XmlIO {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. propriétés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc nom du fichier de sortie
	 */
	public $outFile;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. méthodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>création de l'instance de la classe</p>
	 *
	 * @name IndicXmlIO::__construct()
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	private function _initPackage($id,$name) {
		$this->outString.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		$this->outString.="<indicateur xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ";
		$this->outString.="xsi:noNamespaceSchemaLocation=\"Agimus_Indicateur.xsd\">";
	}

	private function _closePackage() {
		$this->outString.="</indicateur>";
	}

	public function _indicToXml($indicId)
	{
		if (!$indicId>0) return false;

		$curIndic=new Indicateur($indicId);
		$this->_initPackage($indicId,$curIndic->_getLib());

		$this->outString.="<ID>".$curIndic->_getId()."</ID>";
		$this->outString.="<OWNER>".$curIndic->_getOwner()."</OWNER>";
		$this->outString.="<LIB>".$curIndic->_getLib()."</LIB>";
		$this->outString.="<DSC>".$curIndic->_getDsc()."</DSC>";
		$this->outString.="<TYPE>".$curIndic->_getType()."</TYPE>";
		$this->outString.="<UNITE>".$curIndic->_getUnite()."</UNITE>";
		$this->outString.="<DEFAULT_OUTPUT>".$curIndic->_getDefaultOutput()."</DEFAULT_OUTPUT>";
		$this->outString.="<UPDATE_FREQ>".$curIndic->_getUpdateFreq()."</UPDATE_FREQ>";
		$this->outString.="<TRIG>".$curIndic->_getTrig()."</TRIG>";
		$this->outString.="<SQL_STRING>".$curIndic->_getSqlString()."</SQL_STRING>";
		$this->outString.="<COMPLETION_VALUES>".$curIndic->_getCompletionValues()."</COMPLETION_VALUES>";
		
		$this->_closePackage();

		/* formattage utf-8 */
		$this->outString=utf8_encode($this->outString);

		//v1.9.7-PPR-#000
		$this->_exportXml($this->outFile);
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name IndicXmlIO::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}

?>