<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;

require_once (DIR_WWW.ROOT_APPL.'/app/common/hdate.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/cnx.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/application.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/srvIntra.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/catSrv.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/dcplIntra.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/discipline.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/affilIntra.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/affiliation.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/diplomeIntra.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/diplome.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/domEtuIntra.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/domEtu.php') ;

//v1.5-PPR-#016 ajout nouvelles dimensions
require_once (DIR_WWW.ROOT_APPL.'/app/common/compIntra.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/composante.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/regInscIntra.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/regInsc.php') ;

require_once (DIR_WWW.ROOT_APPL.'/app/common/structure.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/catStruct.php') ;

require_once (DIR_WWW.ROOT_APPL.'/app/common/uniteTemps.php') ;

//TODO : mettre en place les classes
//require_once (DIR_WWW.ROOT_APPL.'/app/common/catsrv.php') ;
//require_once (DIR_WWW.ROOT_APPL.'/app/common/srvintra.php') ;

/**
 *
 * <p>Agregat</p>
 *
 * @name  Agregat
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

class Agregat {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc Identifiant temporalite
	 */
	private $idTemps;

	/**
	 * @var (string)
	 * @desc UID (temporaire pour agregation finale)
	 */
	private $uid;

	/**
	 * @var (int)
	 * @desc Nombre UID
	 */
	private $nbUid;

	/**
	 * @var (int)
	 * @desc Nombre Connexions
	 */
	private $nbCnx;

	/**
	 * @var (int)
	 * @desc Duree de connexion
	 */
	private $duree;

	/**
	 * @var (int)
	 * @desc volume de connexion
	 */
	private $volume;

	/**
	 * @var (int)
	 * @desc Identifiant Categorie de structure
	 */
	private $idCatStruct;

	/**
	 * @var (String)
	 * @desc Identifiant Structure
	 */
	private $idStruct;

	/**
	 * @var (String)
	 * @desc Identifiant Structure Mere
	 */
	private $idStructPere;

	/**
	 * @var (int)
	 * @desc Identifiant Service Intra
	 */
	private $idSrvIntra;

	/**
	 * @var (int)
	 * @desc Identifiant Categorie de service
	 */
	private $idCatServ;

	/**
	 * @var (int)
	 * @desc Identifiant Discipline Intra
	 */
	private $idDcplIntra;

	/**
	 * @var (int)
	 * @desc Identifiant Discipline
	 */
	private $idDcpl;

	/**
	 * @var (int)
	 * @desc Identifiant Affiliation Intra
	 */
	private $idAffilIntra;

	/**
	 * @var (int)
	 * @desc Identifiant Affiliation
	 */
	private $idAffil;

	/**
	 * @var (int)
	 * @desc Identifiant Domaine Etude Intra
	 */
	private $idDomEtuIntra;

	/**
	 * @var (int)
	 * @desc Identifiant Domaine Etude
	 */
	private $idDomEtu;

	/**
	 * @var (int)
	 * @desc Identifiant Diplome Intra
	 */
	private $idDiplomeIntra;

	/**
	 * @var (int)
	 * @desc Identifiant Diplome
	 */
	private $idDiplome;

//v1.5-PPR-#016
	/**
	 * @var (int)
	 * @desc Identifiant Composante Intra
	 */
	private $idCompIntra;

	/**
	 * @var (int)
	 * @desc Identifiant Composante
	 */
	private $idComposante;

	/**
	 * @var (int)
	 * @desc Identifiant Regime Inscription Intra
	 */
	private $idRegInscIntra;

	/**
	 * @var (int)
	 * @desc Identifiant Regime Inscription
	 */
	private $idRegInsc;

	/**
	 * @var (datetime)
	 * @desc Date de creation de l'agregat
	 */
	private $dateCre;

	/**
	 * @var (datetime)
	 * @desc Date d'export de l'agregat
	 */
	private $dateExport;

	/**
	 * @var (array)
	 * @desc tableau agregat en memoire
	 */
	private $arAgregats;

	/**
	 * @var (String)
	 * @desc User Agent
	 */
	private $userAgent;

	/**
	 * @var (String)
	 * @desc Identifiant Dimension Utilisateur 1
	 */
	private $userDim1;

	/**
	 * @var (String)
	 * @desc Identifiant Dimension Utilisateur 2
	 */
	private $userDim2;

	/**
	 * @var (String)
	 * @desc Identifiant Dimension Utilisateur 3
	 */
	private $userDim3;

	/**
	 * @var (String)
	 * @desc Identifiant Dimension Utilisateur 4
	 */
	private $userDim4;

	/**
	 * @var (String)
	 * @desc Identifiant Dimension Utilisateur 5
	 */
	private $userDim5;

	/**
	 * @var (String)
	 * @desc Identifiant Dimension Utilisateur 6
	 */
	private $userDim6;


	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. methodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>creation de l'instance de la classe</p>
	 *
	 * @name Agregat::__construct()
	 * @return void
	 */
	public function __construct() {
		$this->idTemps=0;
		$this->uid='';
		$this->nbUid=0;
		$this->nbCnx=0;
		$this->duree=0;
		$this->volume=0;
		$this->idCatStruct=0;
		$this->idStruct=0;
		$this->idStructPere=0;
		$this->idSrvIntra=0;
		$this->idCatServ=0;
		$this->idDcpl=0;
		$this->idAffil=0;
		$this->idDomEtu=0;
		$this->idDiplome=0;
//v1.5-PPR-#016
		$this->idComposante=0;
		$this->idCompIntra=0;
		$this->idRegInsc=0;
		$this->idRegInscIntra=0;

		$this->dateCre='';
		$this->arAgregats=array();
		$this->userAgent='';
		$this->userDim1='';
		$this->userDim2='';
		$this->userDim3='';
//v1.5-PPR-#016
		$this->userDim4='';
		$this->userDim5='';
		$this->userDim6='';
	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name Agregat::_getIdTemps()
	 * @return Int
	 */
	public function _getIdTemps() {
		return $this->idTemps ;
	}

	/**
	 * @name Agregat::_getUid()
	 * @return string
	 */
	public function _getUid() {
		return $this->uid ;
	}

	/**
	 * @name Agregat::_getNbUid()
	 * @return int
	 */
	public function _getNbUid() {
		return $this->nbUid ;
	}

	/**
	 * @name Agregat::_getNbCnx()
	 * @return int
	 */
	public function _getNbCnx() {
		return $this->nbCnx ;
	}

	/**
	 * @name Agregat::_getDuree()
	 * @return int
	 */
	public function _getDuree() {
		return $this->duree ;
	}

	/**
	 * @name Agregat::_getVolume()
	 * @return int
	 */
	public function _getVolume() {
		return $this->volume ;
	}

	/**
	 * @name Agregat::_getIdCatStruct()
	 * @return int
	 */
	public function _getIdCatStruct() {
		return $this->idCatStruct ;
	}

	/**
	 * @name Agregat::_getIdStruct()
	 * @return int
	 */
	public function _getIdStruct() {
		return $this->idStruct ;
	}

	/**
	 * @name Agregat::_getIdStructPere()
	 * @return int
	 */
	public function _getIdStructPere() {
		return $this->idStructPere ;
	}

	/**
	 * @name Agregat::_getIdSrvIntra()
	 * @return int
	 */
	public function _getIdSrvIntra() {
		return $this->idSrvIntra ;
	}

	/**
	 * @name Agregat::_getIdCatServ()
	 * @return int
	 */
	public function _getIdCatServ() {
		return $this->idCatServ ;
	}

	/**
	 * @name Agregat::_getIdDcplIntra()
	 * @return int
	 */
	public function _getIdDcplIntra() {
		return $this->idDcplIntra ;
	}

	/**
	 * @name Agregat::_getIdDcpl()
	 * @return int
	 */
	public function _getIdDcpl() {
		return $this->idDcpl ;
	}

	/**
	 * @name Agregat::_getIdAffilIntra()
	 * @return int
	 */
	public function _getIdAffilIntra() {
		return $this->idAffilIntra ;
	}

	/**
	 * @name Agregat::_getIdAffil()
	 * @return int
	 */
	public function _getIdAffil() {
		return $this->idAffil ;
	}

	/**
	 * @name Agregat::_getIdDomEtuIntra()
	 * @return int
	 */
	public function _getIdDomEtuIntra() {
		return $this->idDomEtuIntra ;
	}

	/**
	 * @name Agregat::_getIdDomEtu()
	 * @return int
	 */
	public function _getIdDomEtu() {
		return $this->idDomEtu ;
	}

	/**
	 * @name Agregat::_getIdDiplomeIntra()
	 * @return int
	 */
	public function _getIdDiplomeIntra() {
		return $this->idDiplomeIntra ;
	}

	/**
	 * @name Agregat::_getIdDiplome()
	 * @return int
	 */
	public function _getIdDiplome() {
		return $this->idDiplome ;
	}
//v1.5-PPR-#016 ajout dimensions
	/**
	 * @name Agregat::_getIdCompIntra()
	 * @return int
	 */
	public function _getIdCompIntra() {
		return $this->idCompIntra ;
	}

	/**
	 * @name Agregat::_getIdComposante()
	 * @return int
	 */
	public function _getIdComposante() {
		return $this->idComposante ;
	}
	/**
	 * @name Agregat::_getIdRegInscIntra()
	 * @return int
	 */
	public function _getIdRegInscIntra() {
		return $this->idRegInscIntra ;
	}

	/**
	 * @name Agregat::_getIdRegInsc()
	 * @return int
	 */
	public function _getIdRegInsc() {
		return $this->idRegInsc ;
	}

	/**
	 * @name Agregat::_getDateCre()
	 * @return datetime
	 */
	public function _getDateCre() {
		return $this->dateCre ;
	}

	/**
	 * @name Agregat::_getDateExport()
	 * @return datetime
	 */
	public function _getDateExport() {
		return $this->dateExport ;
	}

	/**
	 * @name Agregat::_getUserAgent()
	 * @return string
	 */
	public function _getUserAgent() {
		return $this->userAgent ;
	}

	/**
	 * @name Agregat::_getUserDim1()
	 * @return string
	 */
	public function _getUserDim1() {
		return $this->userDim1 ;
	}

	/**
	 * @name Agregat::_getUserDim2()
	 * @return string
	 */
	public function _getUserDim2() {
		return $this->userDim2 ;
	}

	/**
	 * @name Agregat::_getUserDim3()
	 * @return string
	 */
	public function _getUserDim3() {
		return $this->userDim3 ;
	}
//v1.5-PPR-#016
	/**
	 * @name Agregat::_getUserDim4()
	 * @return string
	 */
	public function _getUserDim4() {
		return $this->userDim4 ;
	}
	/**
	 * @name Agregat::_getUserDim5()
	 * @return string
	 */
	public function _getUserDim5() {
		return $this->userDim5 ;
	}
	/**
	 * @name Agregat::_getUserDim6()
	 * @return string
	 */
	public function _getUserDim6() {
		return $this->userDim6 ;
	}


	/**
	 * Accesseurs en ecriture
	 */

	/**
	 * @name Agregat::_setIdTemps()
	 * @param $idTemps (Int)
	 * @return void
	 */
	public function _setIdTemps($idTemps) {
		$this->idTemps  = $idTemps ;
	}

	/**
	 * @name Agregat::_setUid()
	 * @param $uid (string)
	 * @return void
	 */
	public function _setUid($uid) {
		$this->uid  = $uid ;
	}

	/**
	 * @name Agregat::_setNbUid()
	 * @param $nbUid (int)
	 * @return void
	 */
	public function _setNbUid($nbUid) {
		$this->nbUid  = $nbUid ;
	}

	/**
	 * @name Agregat::_setNbCnx()
	 * @param $nbCnx (int)
	 * @return void
	 */
	public function _setNbCnx($nbCnx) {
		$this->nbCnx  = $nbCnx ;
	}

	/**
	 * @name Agregat::_setDuree()
	 * @param $duree (int)
	 * @return void
	 */
	public function _setDuree($duree) {
		$this->duree  = $duree ;
	}

	/**
	 * @name Agregat::_setVolume()
	 * @param $volume (int)
	 * @return void
	 */
	public function _setVolume($volume) {
		$this->volume  = $volume ;
	}

	/**
	 * @name Agregat::_setIdCatStruct()
	 * @param $idCatStruct (int)
	 * @return void
	 */
	public function _setIdCatStruct($idCatStruct) {
		$this->idCatStruct  = $idCatStruct ;
	}

	/**
	 * @name Agregat::_setIdStruct()
	 * @param $idStruct (int)
	 * @return void
	 */
	public function _setIdStruct($idStruct) {
		$this->idStruct  = $idStruct ;
	}

	/**
	 * @name Agregat::_setIdStructPere()
	 * @param $idStructPere (int)
	 * @return void
	 */
	public function _setIdStructPere($idStructPere) {
		$this->idStructPere  = $idStructPere ;
	}

	/**
	 * @name Agregat::_setIdSrvIntra()
	 * @param $idSrvIntra (int)
	 * @return void
	 */
	public function _setIdSrvIntra($idSrvIntra) {
		$this->idSrvIntra  = $idSrvIntra ;
	}

	/**
	 * @name Agregat::_setIdCatServ()
	 * @param $idCatServ (int)
	 * @return void
	 */
	public function _setIdCatServ($idCatServ) {
		$this->idCatServ  = $idCatServ ;
	}

	/**
	 * @name Agregat::_setIdDcplIntra()
	 * @param $idDcplIntra (int)
	 * @return void
	 */
	public function _setIdDcplIntra($idDcplIntra) {
		$this->idDcplIntra  = $idDcplIntra ;
	}

	/**
	 * @name Agregat::_setIdDcpl()
	 * @param $idDcpl (int)
	 * @return void
	 */
	public function _setIdDcpl($idDcpl) {
		$this->idDcpl  = $idDcpl ;
	}

	/**
	 * @name Agregat::_setIdAffilIntra()
	 * @param $idAffilIntra (int)
	 * @return void
	 */
	public function _setIdAffilIntra($idAffilIntra) {
		$this->idAffilIntra  = $idAffilIntra ;
	}

	/**
	 * @name Agregat::_setIdAffil()
	 * @param $idAffil (int)
	 * @return void
	 */
	public function _setIdAffil($idAffil) {
		$this->idAffil  = $idAffil ;
	}

	/**
	 * @name Agregat::_setIdDomEtuIntra()
	 * @param $idDomEtuIntra (int)
	 * @return void
	 */
	public function _setIdDomEtuIntra($idDomEtuIntra) {
		$this->idDomEtuIntra  = $idDomEtuIntra ;
	}

	/**
	 * @name Agregat::_setIdDomEtu()
	 * @param $idDomEtu (int)
	 * @return void
	 */
	public function _setIdDomEtu($idDomEtu) {
		$this->idDomEtu  = $idDomEtu ;
	}

	/**
	 * @name Agregat::_setIdDiplomeIntra()
	 * @param $idDiplomeIntra (int)
	 * @return void
	 */
	public function _setIdDiplomeIntra($idDiplomeIntra) {
		$this->idDiplomeIntra  = $idDiplomeIntra ;
	}

	/**
	 * @name Agregat::_setIdDiplome()
	 * @param $idDiplome (int)
	 * @return void
	 */
	public function _setIdDiplome($idDiplome) {
		$this->idDiplome  = $idDiplome ;
	}

//v1.5-PPR-#016
	/**
	 * @name Agregat::_setIdCompIntra()
	 * @param $idCompIntra (int)
	 * @return void
	 */
	public function _setIdCompIntra($idCompIntra) {
		$this->idCompIntra  = $idCompIntra ;
	}

	/**
	 * @name Agregat::_setIdComposante()
	 * @param $idComposante (int)
	 * @return void
	 */
	public function _setIdComposante($idComposante) {
		$this->idComposante  = $idComposante ;
	}

	/**
	 * @name Agregat::_setIdRegInscIntra()
	 * @param $idRegInscIntra (int)
	 * @return void
	 */
	public function _setRegInscIntra($idRegInscIntra) {
		$this->idRegInscIntra  = $idRegInscIntra ;
	}

	/**
	 * @name Agregat::_setIdRegInsc()
	 * @param $idRegInsc (int)
	 * @return void
	 */
	public function _setIdRegInsc($idRegInsc) {
		$this->idRegInsc  = $idRegInsc ;
	}

	/**
	 * @name Agregat::_setDateCre()
	 * @param $dateCre (datetime)
	 * @return void
	 */
	public function _setDateCre($dateCre) {
		$this->dateCre  = $dateCre ;
	}

	/**
	 * @name Agregat::_setDateExport()
	 * @param $dateExport (datetime)
	 * @return void
	 */
	public function _setDateExport($dateExport) {
		$this->dateExport  = $dateExport ;
	}

	/**
	 * @name Agregat::_setUserAgent()
	 * @param $userAgent (string)
	 * @return void
	 */
	public function _setUserAgent($userAgent) {
		$this->userAgent  = $userAgent ;
	}

	/**
	 * @name Agregat::_setUserDim1()
	 * @param $userDim1 (string)
	 * @return void
	 */
	public function _setUserDim1($userDim1) {
		$this->userDim1  = $userDim1 ;
	}

	/**
	 * @name Agregat::_setUserDim2()
	 * @param $userDim2 (string)
	 * @return void
	 */
	public function _setUserDim2($userDim2) {
		$this->userDim2  = $userDim2 ;
	}

	/**
	 * @name Agregat::_setUserDim3()
	 * @param $userDim3 (string)
	 * @return void
	 */
	public function _setUserDim3($userDim3) {
		$this->userDim3  = $userDim3 ;
	}
//v1.5-PPR-#016
	/**
	 * @name Agregat::_setUserDim4()
	 * @param $userDim4 (string)
	 * @return void
	 */
	public function _setUserDim4($userDim4) {
		$this->userDim4  = $userDim4 ;
	}
	/**
	 * @name Agregat::_setUserDim5()
	 * @param $userDim5 (string)
	 * @return void
	 */
	public function _setUserDim5($userDim5) {
		$this->userDim5  = $userDim5 ;
	}
	/**
	 * @name Agregat::_setUserDim6()
	 * @param $userDim6 (string)
	 * @return void
	 */
	public function _setUserDim6($userDim6) {
		$this->userDim6  = $userDim6 ;
	}


	/**
	 * Compte du nombre d'occurence de l'objet Agregat dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name Agregat::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id_temps) AS nbr ';
		$sql .= 'FROM t_agregat ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Compte du nombre d'occurence restant a traiter
	 *
	 * <p>getNbToExport</p>
	 *
	 * @name Agregat::_getNbToExport()
	 * @return int
	 */
	public static function _getNbToExport()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id_temps) AS nbr ';
		$sql .= 'FROM t_agregat ';
		$sql .= ' WHERE DATE_EXPORT = \'0000-00-00 00:00:00\' ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * recupere la ligne d'agregat
	 *
	 * <p>agregatExists</p>
	 *
	 * @name Agregat::_agregatExists()
	 * @return int
	 */
	public function _agregatExists()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$arAgr=array();

//V1.5-PPR-#016 ajout nouvelle dimensions

		$sql = 'SELECT * from t_agregat_calc ';
		$sql .= 'WHERE ';
		$sql .= 'ID_TEMPS = \''.$this->idTemps.'\' AND  ';
		$sql .= 'UID = \''.$this->uid.'\' AND  ';
		$sql .= 'ID_CAT_STRUCT = \''.$this->idCatStruct.'\' AND  ';
		$sql .= 'ID_STRUCT = \''.$this->idStruct.'\' AND  ';
		$sql .= 'ID_STRUCT_PERE = \''.$this->idStructPere.'\' AND  ';
		$sql .= 'ID_SRV_INTRA = \''.$this->idSrvIntra.'\' AND  ';
		$sql .= 'ID_CAT_SRV = \''.$this->idCatServ.'\' AND  ';
		$sql .= 'ID_DCPL_INTRA = \''.$this->idDcplIntra.'\' AND  ';
		$sql .= 'ID_DCPL = \''.$this->idDcpl.'\' AND  ';
		$sql .= 'ID_AFFIL_INTRA = \''.$this->idAffilIntra.'\' AND  ';
		$sql .= 'ID_AFFIL = \''.$this->idAffil.'\' AND  ';
		$sql .= 'ID_DOM_ETU_INTRA = \''.$this->idDomEtuIntra.'\' AND  ';
		$sql .= 'ID_DOM_ETU = \''.$this->idDomEtu.'\' AND  ';
		$sql .= 'ID_DIPLOME_INTRA = \''.$this->idDiplomeIntra.'\' AND  ';
		$sql .= 'ID_DIPLOME = \''.$this->idDiplome.'\' AND  ';
		$sql .= 'ID_COMP_INTRA = \''.$this->idCompIntra.'\' AND  ';
		$sql .= 'ID_COMPOSANTE = \''.$this->idComposante.'\' AND  ';
		$sql .= 'ID_REG_INSC_INTRA = \''.$this->idRegInscIntra.'\' AND  ';
		$sql .= 'ID_REG_INSC = \''.$this->idRegInsc.'\' AND  ';
		$sql .= 'USER_AGENT = \''.$this->userAgent.'\' AND  ';
		$sql .= 'USER_DIM_1 = \''.$this->userDim1.'\' AND  ';
		$sql .= 'USER_DIM_2 = \''.$this->userDim2.'\' AND  ';
		$sql .= 'USER_DIM_3 = \''.$this->userDim3.'\' AND  ';
		$sql .= 'USER_DIM_4 = \''.$this->userDim4.'\' AND  ';
		$sql .= 'USER_DIM_5 = \''.$this->userDim5.'\' AND  ';
		$sql .= 'USER_DIM_6 = \''.$this->userDim6.'\'  ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$arAgr=$row;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		return $arAgr;
	}



	/**
	 * Creation d'un Agregat dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name Agregat::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;

//v1.5-PPR-#016 ajout nouvelles dimensions

		$sql  = 'INSERT INTO t_agregat_calc VALUES( ';
		$sql .= '\''.AddSlashes($this->idTemps).'\', ';
		$sql .= '\''.AddSlashes($this->uid).'\', ';
		$sql .= '\''.AddSlashes($this->nbUid).'\', ';
		$sql .= '\''.AddSlashes($this->nbCnx).'\', ';
		$sql .= '\''.AddSlashes($this->duree).'\', ';
		$sql .= '\''.AddSlashes($this->volume).'\', ';
		$sql .= '\''.AddSlashes($this->idCatStruct).'\', ';
		$sql .= '\''.AddSlashes($this->idStruct).'\', ';
		$sql .= '\''.AddSlashes($this->idStructPere).'\', ';
		$sql .= '\''.AddSlashes($this->idSrvIntra).'\', ';
		$sql .= '\''.AddSlashes($this->idCatServ).'\', ';
		$sql .= '\''.AddSlashes($this->idDcplIntra).'\', ';
		$sql .= '\''.AddSlashes($this->idDcpl).'\', ';
		$sql .= '\''.AddSlashes($this->idAffilIntra).'\', ';
		$sql .= '\''.AddSlashes($this->idAffil).'\', ';
		$sql .= '\''.AddSlashes($this->idDomEtuIntra).'\', ';
		$sql .= '\''.AddSlashes($this->idDomEtu).'\', ';
		$sql .= '\''.AddSlashes($this->idDiplomeIntra).'\', ';
		$sql .= '\''.AddSlashes($this->idDiplome).'\', ';
		$sql .= '\''.AddSlashes($this->idCompIntra).'\', ';
		$sql .= '\''.AddSlashes($this->idComposante).'\', ';
		$sql .= '\''.AddSlashes($this->idRegInscIntra).'\', ';
		$sql .= '\''.AddSlashes($this->idRegInsc).'\', ';
		$sql .= '\''.AddSlashes($this->dateCre).'\', ';
		$sql .= '\''.AddSlashes($this->dateExport).'\', ';
		$sql .= '\''.AddSlashes($this->userAgent).'\', ';
		$sql .= '\''.AddSlashes($this->userDim1).'\', ';
		$sql .= '\''.AddSlashes($this->userDim2).'\', ';
		$sql .= '\''.AddSlashes($this->userDim3).'\', ';
		$sql .= '\''.AddSlashes($this->userDim4).'\', ';
		$sql .= '\''.AddSlashes($this->userDim5).'\', ';
		$sql .= '\''.AddSlashes($this->userDim6).'\' ';

		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * mise e jour d'une ligne d'agregat
	 *
	 * <p>_update</p>
	 *
	 * @name Agregat::_update()
	 * @return int
	 */
	public function _update()	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

//v1.5-PPR-#016 ajout nouvelle dimensions

		$sql = 'UPDATE t_agregat_calc ';
		$sql .= 'SET ';
		$sql .= 'ID_TEMPS = \''.AddSlashes($this->idTemps).'\',   ';
		$sql .= 'UID = \''.AddSlashes($this->uid).'\',   ';
		$sql .= 'NB_UID = \''.AddSlashes($this->nbUid).'\',   ';
		$sql .= 'NB_CNX = \''.AddSlashes($this->nbCnx).'\',   ';
		$sql .= 'DUREE = \''.AddSlashes($this->duree).'\',   ';
		$sql .= 'VOLUME = \''.AddSlashes($this->volume).'\',   ';
		$sql .= 'ID_CAT_STRUCT = \''.AddSlashes($this->idCatStruct).'\',   ';
		$sql .= 'ID_STRUCT = \''.AddSlashes($this->idStruct).'\',   ';
		$sql .= 'ID_STRUCT_PERE = \''.AddSlashes($this->idStructPere).'\',   ';
		$sql .= 'ID_SRV_INTRA = \''.AddSlashes($this->idSrvIntra).'\',   ';
		$sql .= 'ID_CAT_SRV = \''.AddSlashes($this->idCatServ).'\',   ';
		$sql .= 'ID_DCPL_INTRA = \''.AddSlashes($this->idDcplIntra).'\',   ';
		$sql .= 'ID_DCPL = \''.AddSlashes($this->idDcpl).'\',   ';
		$sql .= 'ID_AFFIL_INTRA = \''.AddSlashes($this->idAffilIntra).'\',   ';
		$sql .= 'ID_AFFIL = \''.AddSlashes($this->idAffil).'\',   ';
		$sql .= 'ID_DOM_ETU_INTRA = \''.AddSlashes($this->idDomEtuIntra).'\',   ';
		$sql .= 'ID_DOM_ETU = \''.AddSlashes($this->idDomEtu).'\',   ';
		$sql .= 'ID_DIPLOME_INTRA = \''.AddSlashes($this->idDiplomeIntra).'\',   ';
		$sql .= 'ID_DIPLOME = \''.AddSlashes($this->idDiplome).'\', ';
		$sql .= 'ID_COMP_INTRA = \''.AddSlashes($this->idCompIntra).'\',   ';
		$sql .= 'ID_COMPOSANTE = \''.AddSlashes($this->idComposante).'\', ';
		$sql .= 'ID_REG_INSC_INTRA = \''.AddSlashes($this->idRegInscIntra).'\',   ';
		$sql .= 'ID_REG_INSC = \''.AddSlashes($this->idRegInsc).'\', ';
		$sql .= 'DATE_CRE = \''.AddSlashes($this->dateCre).'\', ';
		$sql .= 'USER_AGENT = \''.AddSlashes($this->userAgent).'\', ';
		$sql .= 'USER_DIM_1 = \''.AddSlashes($this->userDim1).'\', ';
		$sql .= 'USER_DIM_2 = \''.AddSlashes($this->userDim2).'\', ';
		$sql .= 'USER_DIM_3 = \''.AddSlashes($this->userDim3).'\', ';
		$sql .= 'USER_DIM_4 = \''.AddSlashes($this->userDim3).'\', ';
		$sql .= 'USER_DIM_5 = \''.AddSlashes($this->userDim3).'\', ';
		$sql .= 'USER_DIM_6 = \''.AddSlashes($this->userDim3).'\' ';

		$sql .= 'WHERE ';
		$sql .= 'ID_TEMPS = \''.$this->idTemps.'\' AND  ';
		$sql .= 'UID = \''.$this->uid.'\' AND  ';
		$sql .= 'ID_CAT_STRUCT = \''.$this->idCatStruct.'\' AND  ';
		$sql .= 'ID_STRUCT = \''.$this->idStruct.'\' AND  ';
		$sql .= 'ID_STRUCT_PERE = \''.$this->idStructPere.'\' AND  ';
		$sql .= 'ID_SRV_INTRA = \''.$this->idSrvIntra.'\' AND  ';
		$sql .= 'ID_CAT_SRV = \''.$this->idCatServ.'\' AND  ';
		$sql .= 'ID_DCPL_INTRA = \''.$this->idDcplIntra.'\' AND  ';
		$sql .= 'ID_DCPL = \''.$this->idDcpl.'\' AND  ';
		$sql .= 'ID_AFFIL_INTRA = \''.$this->idAffilIntra.'\' AND  ';
		$sql .= 'ID_AFFIL = \''.$this->idAffil.'\' AND  ';
		$sql .= 'ID_DOM_ETU_INTRA = \''.$this->idDomEtuIntra.'\' AND  ';
		$sql .= 'ID_DOM_ETU = \''.$this->idDomEtu.'\' AND  ';
		$sql .= 'ID_DIPLOME_INTRA = \''.$this->idDiplomeIntra.'\' AND  ';
		$sql .= 'ID_DIPLOME = \''.$this->idDiplome.'\' AND ';
		$sql .= 'ID_COMP_INTRA = \''.$this->idCompIntra.'\' AND  ';
		$sql .= 'ID_COMPOSANTE = \''.$this->idComposante.'\' AND ';
		$sql .= 'ID_REG_INSC_INTRA = \''.$this->idRegInscIntra.'\' AND  ';
		$sql .= 'ID_REG_INSC = \''.$this->idRegInsc.'\' AND ';
		$sql .= 'USER_AGENT = \''.$this->userAgent.'\' AND  ';
		$sql .= 'USER_DIM_1 = \''.$this->userDim1.'\' AND  ';
		$sql .= 'USER_DIM_2 = \''.$this->userDim2.'\' AND  ';
		$sql .= 'USER_DIM_3 = \''.$this->userDim3.'\' AND  ';
		$sql .= 'USER_DIM_4 = \''.$this->userDim3.'\' AND ';
		$sql .= 'USER_DIM_5 = \''.$this->userDim3.'\' AND ';
		$sql .= 'USER_DIM_6 = \''.$this->userDim3.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * purge table temporaire
	 *
	 * <p>_purgeTempTable</p>
	 *
	 * @name Agregat::_purgeTempTable()
	 * @return int
	 */
	public function _purgeTempTable()	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = ' TRUNCATE t_agregat_calc ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Agregation pour export n+1 (suppression des dimensions 'intra'
	 *
	 * <p>_agregateForSupLevel</p>
	 *
	 * @name Agregat::_agregateForSupLevel()
	 * @return int
	 */
	public function _agregateForSupLevel()	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = "DROP TABLE IF EXISTS t_agregat_export";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			return array("Agregation pour export : en erreur : $msgString",20);
		}

		// creation table temporaire d'export
		//v1.5-PPR-#016 ajout nouvelles dimensions
		$sql = "CREATE TABLE `t_agregat_export` (
		  `ID_TEMPS` int(11) NOT NULL,
		  `NB_UID` mediumint(11) NOT NULL,
		  `NB_CNX` mediumint(11) NOT NULL,
		  `DUREE` float NOT NULL,
		  `VOLUME` float NOT NULL,
		  `ID_CAT_STRUCT` tinyint(4) NOT NULL,
		  `ID_STRUCT` varchar(50) NOT NULL,
		  `ID_STRUCT_PERE` varchar(50) NOT NULL,
		  `ID_SRV_INTRA` smallint(6) NOT NULL,
		  `ID_CAT_SRV` smallint(6) NOT NULL,
		  `ID_DCPL_INTRA` smallint(6) NOT NULL,
		  `ID_DCPL` smallint(6) NOT NULL,
		  `ID_AFFIL_INTRA` smallint(6) NOT NULL,
		  `ID_AFFIL` smallint(6) NOT NULL,
		  `ID_DOM_ETU_INTRA` smallint(6) NOT NULL,
		  `ID_DOM_ETU` smallint(6) NOT NULL,
		  `ID_DIPLOME_INTRA` smallint(6) NOT NULL,
		  `ID_DIPLOME` smallint(6) NOT NULL,
		  `ID_COMP_INTRA` smallint(6) NOT NULL,
		  `ID_COMPOSANTE` smallint(6) NOT NULL,
		  `ID_REG_INSC_INTRA` smallint(6) NOT NULL,
		  `ID_REG_INSC` smallint(6) NOT NULL,
		  `DATE_CRE` datetime NOT NULL default '0000-00-00 00:00:00',
		  `DATE_EXPORT` datetime NOT NULL default '0000-00-00 00:00:00',
		  `USER_AGENT` varchar(10) NOT NULL,
		  `USER_DIM_1` smallint(6) NOT NULL,
		  `USER_DIM_2` smallint(6) NOT NULL,
		  `USER_DIM_3` smallint(6) NOT NULL,
		  `USER_DIM_4` smallint(6) NOT NULL,
		  `USER_DIM_5` smallint(6) NOT NULL,
		  `USER_DIM_6` smallint(6) NOT NULL,
		  PRIMARY KEY  (`ID_TEMPS`,`ID_CAT_STRUCT`,`ID_STRUCT`,`ID_STRUCT_PERE`,`ID_CAT_SRV`,`ID_DCPL`,`ID_AFFIL`,`ID_DOM_ETU`,
		  `ID_DIPLOME`,`USER_AGENT`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;
		 ";

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			return array("Agregation pour export : en erreur : $msgString",20);
		}

		//v1.5-PPR-#016
		$sql = ' INSERT INTO t_agregat_export (
		`ID_TEMPS`,`NB_UID`,`NB_CNX`,`DUREE`,`VOLUME`,
		`ID_CAT_STRUCT`,`ID_STRUCT`,`ID_STRUCT_PERE`,`ID_SRV_INTRA`,`ID_CAT_SRV`,
		`ID_DCPL_INTRA`,`ID_DCPL`,`ID_AFFIL_INTRA`,`ID_AFFIL`,`ID_DOM_ETU_INTRA`,
		`ID_DOM_ETU`,`ID_DIPLOME_INTRA`,`ID_DIPLOME`,
		`ID_COMP_INTRA`,`ID_COMPOSANTE`,`ID_REG_INSC_INTRA`,`ID_REG_INSC`,
		`DATE_CRE`,`DATE_EXPORT`,
		`USER_AGENT`,`USER_DIM_1`,`USER_DIM_2`,`USER_DIM_3`,
		`USER_DIM_4`,`USER_DIM_5`,`USER_DIM_6`) 
		SELECT `ID_TEMPS`,
		sum(`NB_UID`) as NB_UID,
		sum(`NB_CNX`) as NB_CNX,
		sum(`DUREE`) as DUREE,
		sum(`VOLUME`) as VOLUME,
		`ID_CAT_STRUCT`,`ID_STRUCT`,`ID_STRUCT_PERE`,`ID_SRV_INTRA`,`ID_CAT_SRV`,
		`ID_DCPL_INTRA`,`ID_DCPL`,`ID_AFFIL_INTRA`,`ID_AFFIL`,`ID_DOM_ETU_INTRA`,
		`ID_DOM_ETU`,`ID_DIPLOME_INTRA`,`ID_DIPLOME`,
		`ID_COMP_INTRA`,`ID_COMPOSANTE`,`ID_REG_INSC_INTRA`,`ID_REG_INSC`,
		`DATE_CRE`,`DATE_EXPORT`,
		`USER_AGENT`,`USER_DIM_1`,`USER_DIM_2`,`USER_DIM_3`,
		`USER_DIM_4`,`USER_DIM_5`,`USER_DIM_6` 
		FROM `t_agregat` group by 
		`ID_TEMPS`,`ID_CAT_STRUCT`,`ID_STRUCT`,`ID_STRUCT_PERE`,
		`ID_CAT_SRV`,`ID_DCPL`,`ID_AFFIL`,`ID_DOM_ETU`,`ID_DIPLOME`,
		`ID_COMP_INTRA`,`ID_COMPOSANTE`,`ID_REG_INSC_INTRA`,`ID_REG_INSC`,
		`USER_AGENT` ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;

			// vidage de la table temporaire d'agregat
			$sql = "TRUNCATE t_agregat_calc";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				return array("Agregation finale en erreur : $msgString",20);
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			return array("Agregation finale en erreur : $msgString",20);
		}
		return array("Agregation finale OK",30);
	}

	/**
	 * agregation finale
	 *
	 * <p>_finalAgregation</p>
	 *
	 * @name Agregat::_finalAgregation()
	 * @return int
	 */
	public function _finalAgregation()	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		//v1.5-PPR-#016 ajout nouvelles dimensions
		
		$sql = ' REPLACE INTO t_agregat (
		`ID_TEMPS`,`NB_UID`,`NB_CNX`,`DUREE`,`VOLUME`,
		`ID_CAT_STRUCT`,`ID_STRUCT`,`ID_STRUCT_PERE`,`ID_SRV_INTRA`,`ID_CAT_SRV`,
		`ID_DCPL_INTRA`,`ID_DCPL`,`ID_AFFIL_INTRA`,`ID_AFFIL`,`ID_DOM_ETU_INTRA`,
		`ID_DOM_ETU`,`ID_DIPLOME_INTRA`,`ID_DIPLOME`,
		`ID_COMP_INTRA`,`ID_COMPOSANTE`,`ID_REG_INSC_INTRA`,`ID_REG_INSC`,
		`DATE_CRE`,`DATE_EXPORT`,
		`USER_AGENT`,`USER_DIM_1`,`USER_DIM_2`,`USER_DIM_3`,
		`USER_DIM_4`,`USER_DIM_5`,`USER_DIM_6`) 
		SELECT `ID_TEMPS`,
		sum(`NB_UID`) as NB_UID,
		sum(`NB_CNX`) as NB_CNX,
		sum(`DUREE`) as DUREE,
		sum(`VOLUME`) as VOLUME,
		`ID_CAT_STRUCT`,`ID_STRUCT`,`ID_STRUCT_PERE`,`ID_SRV_INTRA`,`ID_CAT_SRV`,
		`ID_DCPL_INTRA`,`ID_DCPL`,`ID_AFFIL_INTRA`,`ID_AFFIL`,`ID_DOM_ETU_INTRA`,
		`ID_DOM_ETU`,`ID_DIPLOME_INTRA`,`ID_DIPLOME`,
		`ID_COMP_INTRA`,`ID_COMPOSANTE`,`ID_REG_INSC_INTRA`,`ID_REG_INSC`,
		`DATE_CRE`,`DATE_EXPORT`,
		`USER_AGENT`,`USER_DIM_1`,`USER_DIM_2`,`USER_DIM_3`,
		`USER_DIM_4`,`USER_DIM_5`,`USER_DIM_6`  
		FROM `t_agregat_calc` group by 
		`ID_TEMPS`,`ID_CAT_STRUCT`,`ID_STRUCT`,`ID_STRUCT_PERE`,`ID_SRV_INTRA`,
		`ID_CAT_SRV`,`ID_DCPL_INTRA`,`ID_DCPL`,`ID_AFFIL_INTRA`,`ID_AFFIL`,
		`ID_DOM_ETU_INTRA`,`ID_DOM_ETU`,`ID_DIPLOME_INTRA`,`ID_DIPLOME`,
		`ID_COMP_INTRA`,`ID_COMPOSANTE`,`ID_REG_INSC_INTRA`,`ID_REG_INSC`,
		`USER_AGENT` ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;

			// vidage de la table temporaire d'agregat
			$sql = "TRUNCATE t_agregat_calc";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				return array("Agregation finale en erreur : $msgString",20);
			}
				
			// Nettoyage des tables cnx et cnxlog si paramètre défini
			if ((defined("ERASE_CNXLOG_PROCESSED")) and (ERASE_CNX_PROCESSED))
			$sql = "DELETE FROM t_cnxlog WHERE STAMP_PROCESSED <> '0000-00-00 00:00:00' ";
				
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				return array("Agregation finale  - nettoyage t_cnxlog en erreur : $msgString",20);
			}

			// Nettoyage des tables cnx et cnxlog si paramètre défini
			if ((defined("ERASE_CNX_PROCESSED")) and (ERASE_CNX_PROCESSED))
			$sql = "DELETE FROM t_cnx WHERE STAMP_PROCESSED <> '0000-00-00 00:00:00' ";
				
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				return array("Agregation finale  - nettoyage t_cnx en erreur : $msgString",20);
			}
			
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			return array("Agregation finale en erreur : $msgString",20);
		}
		return array("Agregation finale OK",30);
	}

	/**
	 * Suppression d'un Agregat dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name Agregat::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($date,$nbrCar=10)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'DELETE FROM t_agregat ';
		$sql .= 'WHERE LEFT(DATE_CRE,'.$nbrCar.') = \''.$date.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}
	/**
	 * Recuperation de la liste des Agregats
	 *
	 * <p>Liste des Agregat</p>
	 *
	 * @name Agregat::_getList()
	 * @return array
	 */
	public function _getList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_agregat ' ;
		$sql.= ' WHERE 1 ' ;
		$sql .= 'ORDER BY DATE_CRE ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			$idx=0;
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				//v1.5-PPR-#016 ajout nouvelles dimensions
			
				$row['ID']=$idx;
				$listArray[$row['ID']]['ID_TEMPS'] = StripSlashes($row['ID_TEMPS']) ;
				$listArray[$row['ID']]['UID'] = StripSlashes($row['UID']) ;
				$listArray[$row['ID']]['NB_UID'] = StripSlashes($row['NB_UID']) ;
				$listArray[$row['ID']]['NB_CNX'] = StripSlashes($row['NB_CNX']) ;
				$listArray[$row['ID']]['DUREE'] = StripSlashes($row['DUREE']) ;
				$listArray[$row['ID']]['VOLUME'] = StripSlashes($row['VOLUME']) ;
				$listArray[$row['ID']]['ID_CAT_STRUCT'] = StripSlashes($row['ID_CAT_STRUCT']) ;
				$listArray[$row['ID']]['ID_STRUCT'] = StripSlashes($row['ID_STRUCT']) ;
				$listArray[$row['ID']]['ID_STRUCT_PERE'] = StripSlashes($row['ID_STRUCT_PERE']) ;
				$listArray[$row['ID']]['ID_SRV_INTRA'] = StripSlashes($row['ID_SRV_INTRA']) ;
				$listArray[$row['ID']]['ID_CAT_SERV'] = StripSlashes($row['ID_CAT_SERV']) ;
				$listArray[$row['ID']]['ID_DCPL_INTRA'] = StripSlashes($row['ID_DCPL_INTRA']) ;
				$listArray[$row['ID']]['ID_DCPL'] = StripSlashes($row['ID_DCPL']) ;
				$listArray[$row['ID']]['ID_AFFIL_INTRA'] = StripSlashes($row['ID_AFFIL_INTRA']) ;
				$listArray[$row['ID']]['ID_AFFIL'] = StripSlashes($row['ID_AFFIL']) ;
				$listArray[$row['ID']]['ID_DOM_ETU_INTRA'] = StripSlashes($row['ID_DOM_ETU_INTRA']) ;
				$listArray[$row['ID']]['ID_DOM_ETU'] = StripSlashes($row['ID_DOM_ETU']) ;
				$listArray[$row['ID']]['ID_DIPLOME_INTRA'] = StripSlashes($row['ID_DIPLOME_INTRA']) ;
				$listArray[$row['ID']]['ID_DIPLOME'] = StripSlashes($row['ID_DIPLOME']) ;
				$listArray[$row['ID']]['ID_COMP_INTRA'] = StripSlashes($row['ID_COMP_INTRA']) ;
				$listArray[$row['ID']]['ID_COMPOSANTE'] = StripSlashes($row['ID_COMPOSANTE']) ;
				$listArray[$row['ID']]['ID_REG_INSC_INTRA'] = StripSlashes($row['ID_REG_INSC_INTRA']) ;
				$listArray[$row['ID']]['ID_REG_INSC'] = StripSlashes($row['ID_REG_INSC']) ;
				$listArray[$row['ID']]['DATE_CRE'] = StripSlashes($row['DATE_CRE']) ;
				$listArray[$row['ID']]['DATE_EXPORT'] = StripSlashes($row['DATE_EXPORT']) ;
				$listArray[$row['ID']]['USER_AGENT'] = StripSlashes($row['USER_AGENT']) ;
				$listArray[$row['ID']]['USER_DIM_1'] = StripSlashes($row['USER_DIM_1']) ;
				$listArray[$row['ID']]['USER_DIM_2'] = StripSlashes($row['USER_DIM_2']) ;
				$listArray[$row['ID']]['USER_DIM_3'] = StripSlashes($row['USER_DIM_3']) ;
				$listArray[$row['ID']]['USER_DIM_4'] = StripSlashes($row['USER_DIM_4']) ;
				$listArray[$row['ID']]['USER_DIM_5'] = StripSlashes($row['USER_DIM_5']) ;
				$listArray[$row['ID']]['USER_DIM_6'] = StripSlashes($row['USER_DIM_6']) ;
				$idx+=1;
			}
		}
		return $listArray ;
	}

	/**
	 * Alimentation de la table des agregats
	 *
	 * <p>Alimentation de la table des agregats</p>
	 *
	 * @name Agregat::_agregateCnx()
	 * @return array
	 * v1.5-PPR-#016 ajout traitement nouvelles dimensions
	 */

	public function _agregateCnx($arCnxLines)
	{
		global $_AppliSessionDuration, $_TimeRupt;

		$config=$_SESSION['config'];
		$mess="";
		$nbcollOk=0;
		$nbAgr=0;
		$nbRejets=0;
		$nbLus=0;
		$codeRet=0;
		$sep="|";
		$volume=0;
		$duree=0;
		$nbUid=0;
		$nbCnx=0;
		$idStruct='';
		$arUidTraites=array();
		$curRupt="";

		$debug=false;

		//v1.3 suppression et migration vers agregateCnx.php
		//$this->_purgeTempTable();

		if(count($arCnxLines)>0)
		{
			if ($debug)	echo  "\r\nDébut du traitement : ".count($arCnxLines)." lignes à traiter";

			foreach($arCnxLines as $row)
			{
				$nbLus+=1;

				if ($debug)	echo  "\r\nTraitement ligne no ".$row['ID']. " : ".$row['UID']."|".$row['STAMP_START']."|".$row["ID_APPLI"];

				if ($nbLus==1) // initialisation des chaines de rupture
				{
					$oldStamp=$row["STAMP_START"];
					$curDate=date($_TimeRupt,$row["STAMP_START"]); // formattage a l'heure pres
					$curRow=$row;
				}
					
				// analyse des donnees
				$calDate=date($_TimeRupt,$row["STAMP_START"]);
				// formattage de la duree de rupture
				// Critere 1 : changement d'UID
				// Critere 2 : changement d'application
				// Critere 3 : changement de date (cf. ci-dessus)
				$ruptString=$row["UID"].$sep.$calDate.$sep.$row["ID_APPLI"];

				if ($debug) echo  "\r\n caldate : $caldate | rupstring : [$ruptString]";

				// =========================================
				// traitement des ruptures

				if (($curRupt!="") and ($ruptString!=$curRupt))
				{
					if ($debug)	echo  "\r\n.Rupture sur ($ruptString)";

					$uid=$curRow["UID"];

					if ($idStruct=='')
					{

						//v1.2-PPR-28032011 ajout alerte bloquante sur structure mère fille inconnue
						if (trim($curRow["ID_STRUCT"])=='')
						{
							$mess.="\r\n ERREUR DE TRAITEMENT : le code Structure est inconnu : veuillez contrôler le fichier /config/config.xml ! ";
							$erreur=20;
							break;
						}	

						// recuperation des infos de la structure
						$newStruct=new Structure($curRow["ID_STRUCT"]);

						$idStruct = $curRow["ID_STRUCT"];
						$idCatStruct=$newStruct->_getCatStruct();
						$idStructPere=$newStruct->_getParent();
						if ($debug)	echo  "\r\n..Infos structure recuperees Struct[$idStruct] CatStruct[$idCatStruct] StructPere[$idStructPere] !";

						//v1.2-PPR-28032011 ajout alerte bloquante sur structure mère fille inconnue
						if (trim($idStructPere)=='')
						{
							$mess.="\r\n ERREUR DE TRAITEMENT : le code Structure Père est inconnu : veuillez contrôler le référentiel Structures ! ";
							$erreur=20;
							break;
						}	
						

					}

					// recuperation des regroupements officiels
					$idAffilIntra=$curRow["ID_AFFIL"];
					$newAffil=new AffilIntra($curRow["ID_AFFIL"]);
					$idAffil=$newAffil->_getIdRegr();
					if (!$idAffil>0)
					{
						$mess.="\r\nRejet ligne $nbLus : Profil associé au profil interne [$idAffilIntra ".$newAffil->_getLib()."] inconnu ! ";
						$nbRejets+=1;
						continue;
					}

					$idDcplIntra=$curRow["ID_DCPL"];
					$newDcpl=new DcplIntra($curRow["ID_DCPL"]);
					$idDcpl=$newDcpl->_getIdRegr();
					if (!$idDcpl>0)
					{
						$mess.="\r\nRejet ligne $nbLus : Filière associé à la filière interne [$idDcplIntra ".$newDcpl->_getLib()."] inconnue !  ";
						$nbRejets+=1;
						continue;
					}

					$idDiplomeIntra=$curRow["ID_DIPLOME"];
					$newDiplome=new DiplomeIntra($curRow["ID_DIPLOME"]);
					$idDiplome=$newDiplome->_getIdRegr();
					if (!$idDiplome>0)
					{
						$mess.="\r\nRejet ligne $nbLus : Diplome associé au Diplome interne [$idDiplomeIntra ".$newDiplome->_getLib()."] inconnu ! ";
						$nbRejets+=1;
						continue;
					}

					$idDomEtuIntra=$curRow["ID_DOM_ETU"];
					$newDomEtu=new DomEtuIntra($curRow["ID_DOM_ETU"]);
					$idDomEtu=$newDomEtu->_getIdRegr();
					if (!$idDomEtu>0)
					{
						$mess.="\r\nRejet ligne $nbLus : Domaine associé au Domaine interne [$idDomEtuIntra ".$newDomEtu->_getLib()."] inconnu ! ";
						$nbRejets+=1;
						continue;
					}
					
					//v1.5-PPR-#016
					$idCompIntra=$curRow["ID_COMPOSANTE"];
					$newCompIntra=new CompIntra($curRow["ID_COMPOSANTE"]);
					$idComposante=$newCompIntra->_getIdRegr();
					if (!$idComposante>0)
					{
						$mess.="\r\nRejet ligne $nbLus : Composante associee a la Composante interne [$idCompIntra ".$newCompIntra->_getLib()."] inconnue ! ";
						$nbRejets+=1;
						continue;
					}
					$idRegInscIntra=$curRow["ID_REG_INSC"];
					$newRegInscIntra=new CompIntra($curRow["ID_REG_INSC"]);
					$idRegInsc=$newRegInscIntra->_getIdRegr();
					if (!$idRegInsc>0)
					{
						$mess.="\r\nRejet ligne $nbLus : Regime inscription associe au Regime Inscription interne [$idRegInscIntra ".$newRegInscIntra->_getLib()."] inconnu ! ";
						$nbRejets+=1;
						continue;
					}
					

					if ($debug)	echo  "\r\n..Regroupements recuperes Affil[$idAffilIntra] Dcpl[$idDcplIntra] Diplome[$idDiplomeIntra] DomEtu[$idDomEtuIntra] 
										Composante[$idCompIntra] RegInsc[$idRegInscIntra] !";

					// ------------ recuperation des services et categories de service de l'application

					$newAppli= new Application($curRow["ID_APPLI"]);
					$idSrvIntra=$newAppli->_getIdSrvIntra();
					if (!$idSrvIntra>0)
					{
						$mess.="\r\nRejet ligne $nbLus : Service interne lié à l'appli [".$curRow["ID_APPLI"]." ".$newAppli->_getLib()."] inconnu !";
						$nbRejets+=1;
						continue;
					}
					$newSrvIntra=new SrvIntra($idSrvIntra);
					$idCatServ = $newSrvIntra->_getIdRegr();
					if (!$idCatServ>0)
					{
						$mess.="\r\nRejet ligne $nbLus : Catégorie de service lié au service [$idSrvIntra] inconnu !";
						$nbRejets+=1;
						continue;
					}
					if ($debug)	echo  "\r\n..Categories de service recuperees Appli [".$curRow["ID_APPLI"]."] ServIntra[$idSrvIntra] CatSrv[$idCatServ] !";

					$userAgent=$curRow["USER_AGENT"];

					// -> Calcul de la duree de connexion

					// si changement d'UID ou de date : il faut forcer a une duree arbitraire
					// (pas d'info dipo sur la deconnexion)
					if (($row["UID"]!=$curRow["UID"]) or
					($calDate!=$curDate))
					{
						$duree+=$_AppliSessionDuration;
					}
					else
					{
						list($newStampDec,$newStampMilli)=explode(".",$row["STAMP_START"]);
						list($oldStampDec,$oldStampMilli)=explode(".",$oldStamp);
						$duree=($newStampDec+($newStampMilli/1000))-($oldStampDec+($oldStampMilli/1000));
						if ($debug)	echo  "\r\n.Changement d'application detectee [".$row["ID_APPLI"]."]->[".$curRow["ID_APPLI"]."] : duree[$duree] = ".$row["STAMP_START"]." - ".$oldStamp;
					}
					$oldStamp=$row["STAMP_START"];

					// creation de la nouvelle ligne d'agregat

					//Formattage de la temporalite
					$arDate=explode("-",$curDate);
					$newUt=new UniteTemps();
					if ($debug) echo "\r\n..recherche $curDate dans t_ut...";
					$idTemps=$newUt->_existsRef($arDate);

					if (!$idTemps)
					{
						if ($debug)	echo  "\r\n..idTemps inconnu pour [$curDate]: creation demandee ";

						$newUt->_setAnnee($arDate[0]);
						$newUt->_setMois($arDate[1]);
						$newUt->_setSemaine($arDate[2]);
						$newUt->_setJour($arDate[3]);
						$newUt->_setHeure($arDate[4]);
						if ($debug)	echo  "\r\n..creation d'une nouvelle unité de temps... ";
						$newUt->_create();
						if ($debug)	echo  "\r\n..creation unité de temps effectuée...";
						$idTemps=$newUt->_getId();
						if ($debug)	echo  " [$idTemps] ";
					}
					else
					{
						if ($debug)	echo  "\r\n..idTemps [$idTemps] trouvé ! ";

					}

					$this->idTemps=$idTemps;
					$this->uid=$uid;
					$this->idCatStruct=$idCatStruct;
					$this->idStruct=$idStruct;
					$this->idStructPere=$idStructPere;
					$this->idSrvIntra=$idSrvIntra;
					$this->idCatServ=$idCatServ;
					$this->idDcpl=$idDcpl;
					$this->idAffil=$idAffil;
					$this->idDomEtu=$idDomEtu;
					$this->idDiplome=$idDiplome;
					$this->idDcplIntra=$idDcplIntra;
					$this->idAffilIntra=$idAffilIntra;
					$this->idDomEtuIntra=$idDomEtuIntra;
					$this->idDiplomeIntra=$idDiplomeIntra;
					
					//v1.5-PPR-#016
					$this->idCompIntra=$idCompIntra;
					$this->idComposante=$idComposante;
					$this->idRegInscIntra=$idRegInscIntra;
					$this->idRegInsc=$idRegInsc;
					
					$this->userAgent=$userAgent;

					$this->nbUid=1;
					$this->nbCnx=1; // on denombre 1 cnx par appli
					$this->$duree;
					$this->$volume;
					$this->duree=$duree;
					$this->volume=$volume;
					$newDate=date('Y-m-d H:i:s');
					$this->dateCre=$newDate;

					if ($debug) echo "\r\n.. recherche si agregat exists...";
					$arAgr=$this->_agregatExists();
					if (isset($arAgr["ID_TEMPS"]))
					{
						$this->_fill($arAgr);
						$this->_setNbCnx($this->_getNbCnx()+1);
						if ($debug) echo ".. oui -> update demandé";
						$this->_update();
						if ($debug) echo ".. effectué !";
					}
					else
					{
						if ($debug) echo ".. non -> create demandé";
						$this->_create();
						if ($debug)	echo  ".. Ligne d'agregat creee";
						$nbAgr+=1;
					}
					$volume=0;
					$duree=0;
					if ($row["ID_APPLI"]!=$curRow["ID_APPLI"])
					$nbCnx=0;
				}

				$curRupt=$ruptString;
				$curDate=date($_TimeRupt,$row["STAMP_START"]); // formattage a l'heure pres
				// cumuls
				$curRow=$row;
				$volume+=$row["FLOW_SIZE"];
				$nbClicks+=1;
				$nbCollOk+=1;

				if ($debug)
				echo  "\r\n".$row["STAMP_START"].": flowsize [".$row["FLOW_SIZE"]."] -> volume cumule : [".$volume."] | duree : [".$duree."]";

				// mise a jour du stamp_processed de cnxlog
				$newDate=date('Y-m-d H:i:s');

				$newCnx=new Cnx($row["ID"]);
				if ($debug) echo  "\r\nmise a jour du stamp sur ligne no ".$row["ID"]." : stampStart = ".$newCnx->_getStampStart();
				if ($newCnx->_getUid()!="")
				{
					$newCnx->_setStampProcessed($newDate);
					$newCnx->_update();
					if ($debug)	echo  "\r\nMise a jour cnx [".$row['ID']."] effectuee a [$newDate] ";
				}
				else
				{
					echo "\r\n Ligne ".$row["ID"]."->ERREUR DE TRAITEMENT : IMPOSSIBLE DE METTRE A JOUR LE STAMP CNX !\r\n";
					$erreur=20;
					break;
				}
			}

			// traitement derniere ligne
			if ($debug)	echo  "\r\n.Rupture sur ($ruptString)";

			if (!$erreur)
			{
				$uid=$curRow["UID"];

				if ($idStruct=='')
				{
					// recuperation des infos de la structure
					$newStruct=new Structure($curRow["ID_STRUCT"]);
					$idStruct = $curRow["ID_STRUCT"];
					$idCatStruct=$newStruct->_getCatStruct();
					$idStructPere=$newStruct->_getParent();
					if ($debug)	echo  "\r\n..Infos structure recuperees Struct[$idStruct] CatStruct[$idCatStruct] StructPere[$idStructPere] !";
				}

				// recuperation des regroupements officiels
				$idAffilIntra=$curRow["ID_AFFIL"];
				$newAffil=new AffilIntra($curRow["ID_AFFIL"]);
				$idAffil=$newAffil->_getIdRegr();

				$idDcplIntra=$curRow["ID_DCPL"];
				$newDcpl=new DcplIntra($curRow["ID_DCPL"]);
				$idDcpl=$newDcpl->_getIdRegr();

				$idDiplomeIntra=$curRow["ID_DIPLOME"];
				$newDiplome=new DiplomeIntra($curRow["ID_DIPLOME"]);
				$idDiplome=$newDiplome->_getIdRegr();

				$idDomEtuIntra=$curRow["ID_DOM_ETU"];
				$newDomEtu=new DomEtuIntra($curRow["ID_DOM_ETU"]);
				$idDomEtu=$newDomEtu->_getIdRegr();

				//v1.5-PPR-#016
				$idCompIntra=$curRow["ID_COMPOSANTE"];
				$newCompIntra=new CompIntra($curRow["ID_COMPOSANTE"]);
				$idComposante=$newCompIntra->_getIdRegr();

				$idRegInscIntra=$curRow["ID_REG_INSC"];
				$newRegInscIntra=new RegInscIntra($curRow["ID_REG_INSC"]);
				$idRegInsc=$newRegInscIntra->_getIdRegr();

				if ($debug)	echo  "\r\n..Regroupements recuperes Affil[$idAffilIntra] Dcpl[$idDcplIntra] Diplome[$idDiplomeIntra] DomEtu[$idDomEtuIntra] 
				Composante[$idCompIntra] RegInsc[$idRegInscIntra] !";

				// ------------ recuperation des services et categories de service de l'application

				$newAppli= new Application($curRow["ID_APPLI"]);
				$idSrvIntra=$newAppli->_getIdSrvIntra();
				$newSrvIntra=new SrvIntra($idSrvIntra);
				$idCatServ = $newSrvIntra->_getIdRegr();
				if ($debug)	echo  "\r\n..Categories de service recuperees Appli [".$curRow["ID_APPLI"]."] ServIntra[$idSrvIntra] CatSrv[$idCatServ] !";

				$userAgent=$curRow["USER_AGENT"];

				// -> Calcul de la duree de connexion

				// si changement d'UID ou de date : il faut forcer a une duree arbitraire
				// (pas d'info dipo sur la deconnexion)
				//if (($row["UID"]!=$curRow["UID"]) or
				//($calDate!=$curDate))
				//{
				$duree+=$_AppliSessionDuration;
				//}
				//else
				//{
				//	list($newStampDec,$newStampMilli)=explode(".",$row["STAMP_START"]);
				//	list($oldStampDec,$oldStampMilli)=explode(".",$oldStamp);
				//	$duree=($newStampDec+($newStampMilli/1000))-($oldStampDec+($oldStampMilli/1000));
				//	if ($debug)	echo  "\r\n.Changement d'application detectee [".$row["ID_APPLI"]."]->[".$curRow["ID_APPLI"]."] : duree[$duree] = ".$row["STAMP_START"]." - ".$oldStamp;
				//}
				//$oldStamp=$row["STAMP_START"];

				// creation de la nouvelle ligne d'agregat

				//Formattage de la temporalite
				$arDate=explode("-",$curDate);
				$newUt=new UniteTemps();
				$idTemps=$newUt->_existsRef($arDate);

				if ($debug)	echo  "\r\n..idTemps inconnu pour [$curDate]: creation demandee ";

				if (!$idTemps)
				{
					$newUt->_setAnnee($arDate[0]);
					$newUt->_setMois($arDate[1]);
					$newUt->_setSemaine($arDate[2]);
					$newUt->_setJour($arDate[3]);
					$newUt->_setHeure($arDate[4]);
					$newUt->_create();
					$idTemps=$newUt->_getId();
				}

				$this->idTemps=$idTemps;
				$this->uid=$uid;
				$this->idCatStruct=$idCatStruct;
				$this->idStruct=$idStruct;
				$this->idStructPere=$idStructPere;
				$this->idSrvIntra=$idSrvIntra;
				$this->idCatServ=$idCatServ;
				$this->idDcpl=$idDcpl;
				$this->idAffil=$idAffil;
				$this->idDomEtu=$idDomEtu;
				$this->idDiplome=$idDiplome;
				$this->idDcplIntra=$idDcplIntra;
				$this->idAffilIntra=$idAffilIntra;
				$this->idDomEtuIntra=$idDomEtuIntra;
				$this->idDiplomeIntra=$idDiplomeIntra;

					//v1.5-PPR-#016
					$this->idCompIntra=$idCompIntra;
					$this->idComposante=$idComposante;
					$this->idRegInscIntra=$idRegInscIntra;
					$this->idRegInsc=$idRegInsc;

				$this->userAgent=$userAgent;

				$this->nbUid=1;
				$this->nbCnx=1; // on denombre 1 cnx par appli
				$this->duree=$duree;
				$this->volume=$volume;
				$newDate=date('Y-m-d H:i:s');
				$this->dateCre=$newDate;
				$arAgr=$this->_agregatExists();
				if (isset($arAgr["ID_TEMPS"]))
				{
					$this->_fill($arAgr);
					$this->_setNbCnx($this->_getNbCnx()+1);
					$this->_update();
				}
				else
				{
					$this->_create();
					if ($debug)	echo  "\r\nLigne d'agregat creee";
					$nbAgr+=1;
				}
			}
			$codeRet=30;
		}

		// totaux de controle
		echo "\r\nNombre de lignes lues  : $nbLus";
		echo "\r\nNombre de lignes traitees  : $nbCollOk";
		echo "\r\nNombre de lignes agregees : $nbAgr";
		echo "\r\nNombre de lignes rejetees  : $nbRejets";

		if ($nbRejets>0)
		{
			echo "\r\nAnomalie dans le traitement : $nbRejets lignes rejetees !";
			$codeRet=20;
		}
		
		if ($nbLus!=($nbCollOk+$nbRejets))
		{
			echo "\r\nAnomalie dans le traitement : le nombre lu ne correspond pas a la somme 'traite + rejete' :";
			$codeRet=20;
		}

		if ($debug) echo "\r\n\r\nCR Traitement\r\n : $mess";

		return array($mess,$codeRet);
	}


	public function _fill($ar) {
		if (count($ar)==0) return false;
		$this->idTemps=$ar["ID_TEMPS"];
		$this->nbUid=$ar["NB_UID"];
		$this->nbCnx=$ar["NB_CNX"];
		$this->duree=$ar["DUREE"];
		$this->volume=$ar["VOLUME"];
		$this->idCatStruct=$ar["ID_CAT_STRUCT"];
		$this->idStruct=$ar["ID_STRUCT"];
		$this->idStructPere=$ar["ID_STRUCT_PERE"];
		$this->idSrvIntra=$ar["ID_SRV_INTRA"];
		$this->idCatServ=$ar["ID_CAT_SRV"];
		$this->idDcpl=$ar["ID_DCPL"];
		$this->idAffil=$ar["ID_AFFIL"];
		$this->idDomEtu=$ar["ID_DOM_ETU"];
		$this->idDiplome=$ar["ID_DIPLOME"];
		$this->idDcplIntra=$ar["ID_DCPL_INTRA"];
		$this->idAffilIntra=$ar["ID_AFFIL_INTRA"];
		$this->idDomEtuIntra=$ar["ID_DOM_ETU_INTRA"];
		$this->idDiplomeIntra=$ar["ID_DIPLOME_INTRA"];
//v1.5-PPR-#016
		$this->idCompIntra=$ar["ID_COMP_INTRA"];
		$this->idComposante=$ar["ID_COMPOSANTE"];
		$this->idRegInscIntra=$ar["ID_REG_INSC_INTRA"];
		$this->idRegInsc=$ar["ID_REG_INSC"];

		$this->dateCre=$ar["DATE_CRE"];
		$this->dateExport=$ar["DATE_EXPORT"];
		$this->userAgent=$ar["USER_AGENT"];
		$this->userDim1=$ar["USER_DIM_1"];
		$this->userDim2=$ar["USER_DIM_2"];
		$this->userDim3=$ar["USER_DIM_3"];
//v1.5-PPR-#016
		$this->userDim3=$ar["USER_DIM_4"];
		$this->userDim3=$ar["USER_DIM_5"];
		$this->userDim3=$ar["USER_DIM_6"];
	}


	/**
	 * Generation aleatoire pour test de charge
	 *
	 * <p>Enrichissement aleatoire pour test de charge</p>
	 *
	 * @name Agregat::_randomFill()
	 * @param nbValues (int)
	 * @return array
	 */
	public function _randomFill($nbValues=1000) {

		global $_TimeRupt, $_userAgentList;

		$idPast=strtotime("2009-09-01 00:00:00");
		$idNow=time();
		$config=$_SESSION['config'];
		$idStruct=$config["id_struct"];
		// recuperation des infos de la structure
		$newStruct=new Structure($idStruct);
		$idCatStruct=$newStruct->_getCatStruct();
		$idStructPere=$newStruct->_getParent();

		for ($i=0;$i<$nbValues;$i++) {

			// Formattage aleatoire de l'occurrence
			$rndTime=rand($idPast,$idNow);
			$curDate=date($_TimeRupt,$rndTime);
			//Formattage de la temporalite
			$arDate=explode("-",$curDate);
			$newUt=new UniteTemps();
			$idTemps=$newUt->_existsRef($arDate);

			if (!$idTemps)
			{
				$newUt->_setAnnee($arDate[0]);
				$newUt->_setMois($arDate[1]);
				$newUt->_setSemaine($arDate[2]);
				$newUt->_setJour($arDate[3]);
				$newUt->_setHeure($arDate[4]);
				$newUt->_create();
				$idTemps=$newUt->_getId();
			}

			$duree=rand(1,600);
			$volume=rand(10,100000);

			$uid=rand(1,TraceLog::_countRef());

			$nbCnx=rand(1,$uid*10);

			$idSrvIntra=rand(1,SrvIntra::_countRef());
			$newSrvIntra=new SrvIntra($idSrvIntra);
			$idCatServ = $newSrvIntra->_getIdRegr();

			$idDcplIntra=rand(1,DcplIntra::_countRef());
			$newDcplIntra=new DcplIntra($idDcplIntra);
			$idDcpl = $newDcplIntra->_getIdRegr();

			$idAffilIntra=rand(1,AffilIntra::_countRef());
			$newAffilIntra=new AffilIntra($idAffilIntra);
			$idAffil = $newAffilIntra->_getIdRegr();

			$idDometuIntra=rand(1,DomEtuIntra::_countRef());
			$newDomEtuIntra=new DomEtuIntra($idDomEtuIntra);
			$idDomEtu = $newDomEtuIntra->_getIdRegr();

			$idDiplomeIntra=rand(1,DiplomeIntra::_countRef());
			$newDiplomeIntra=new DiplomeIntra($idDiplomeIntra);
			$idDiplome = $newDiplomeIntra->_getIdRegr();

//v1.5-PPR-#016
			$idCompIntra=rand(1,CompIntra::_countRef());
			$newComp=new CompIntra($idCompIntra);
			$idComposante = $newCompIntra->_getIdRegr();

			$idRegInscIntra=rand(1,RegInscIntra::_countRef());
			$newRegInsc=new RegInscIntra($idRegInscIntra);
			$idRegInsc = $newRegInscIntra->_getIdRegr();

			$userAgent=$_userAgentList[rand(1,count($_userAgentList-1))];

			// Creation de l'occurence

			$this->idTemps=$idTemps;
			$this->uid=$uid;
			$this->nbUid=1;
			$this->nbCnx=$nbCnx;
			$this->duree=$duree;
			$this->volume=$volume;
			$this->idCatStruct=$idCatStruct;
			$this->idStruct=$idStruct;
			$this->idStructPere=$idStructPere;
			$this->idSrvIntra=$idSrvIntra;
			$this->idCatServ=$idCatServ;
			$this->idDcpl=$idDcpl;
			$this->idAffil=$idAffil;
			$this->idDomEtu=$idDomEtu;
			$this->idDiplome=$idDiplome;
			$this->idDcplIntra=$idDcplIntra;
			$this->idAffilIntra=$idAffilIntra;
			$this->idDomEtuIntra=$idDomEtuIntra;
			$this->idDiplomeIntra=$idDiplomeIntra;
//v1.5-PPR-#016
		$this->idCompIntra=$idCompIntra;
		$this->idComposante=$idComposante;
		$this->idRegInscIntra=$idRegInscIntra;
		$this->idRegInsc=$idRegInsc;

			$this->userAgent=$userAgent;
			$this->userDim1=0;
			$this->userDim2=0;
			$this->userDim3=0;
//v1.5-PPR-#016
			$this->userDim4=0;
			$this->userDim5=0;
			$this->userDim6=0;

			$newDate=date('Y-m-d H:i:s');
			$this->dateCre=$newDate;
			$this->_create();

		}


	}



	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Agregat::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>