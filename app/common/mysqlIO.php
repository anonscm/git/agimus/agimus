<?php

require_once ('mysqlDatabase.php') ;
require_once ('msgException.php') ;

/**
 *
 * <p>Import / Export donnees SQL</p>
 *
 * @name MysqlIO
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package common
 */

class MysqlIO extends MysqlDatabase {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (String)
	 * @desc Nom du fichier de sortie
	 */
	public $outputFileName;

	/**
	 * @var (Array)
	 * @desc Requete de mise a jour table source
	 */
	public $updQuery;


	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. methodes
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>creation de l'instance de la classe</p>
	 *
	 * @name MysqlDatabase::__construct()
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->outputFileName="backup_".$this->bddname."_".date("Y_m_d_H_i").".sql";
		$this->updQuery=array();
	}


	public function _updateAgregat() {
		$maconnexion = MysqlDatabase::GetInstance() ;
		if (count($this->updQuery)>0)
		{
			foreach($this->updQuery as $sqlString)
			{
				try{
					$res = $maconnexion->_bddQuery($sqlString) ;
				}
				catch(MsgException $e){
					$msgString = $e ->_getError();
					throw new MsgException($msgString, 'database') ;
				}
			}
		}
	}


	public function _importFiles($dirFiles,$arFiles) {

		$importMess="";

		if (!count($arFiles)>0) return "Aucun fichier à traiter\r\n";

		foreach($arFiles as $curFile)
		$importMess.=$this->_importTable($dirFiles."/".$curFile);

		return $importMess;
	}

	public function _importTable($fileName) {

		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql_file=fopen($fileName,"rb");

		if (!is_resource($sql_file))
		throw new MsgException("_FILE_NOT_FOUND :".$fileName, '') ;

		while (!feof($sql_file)) {// Boucle tant que non fin de fichier
			$Ligne=trim(fgets($sql_file,65535));// lecture d'une ligne, augmenter la valeur 65535 si besoin.
			if (!($Ligne=='' || $Ligne{0}=='-' || $Ligne{0}=='#')){// ligne pas vide, pas un commentaire
				$LigneSQL.=$Ligne;
				if (strlen($Ligne)>0 && $Ligne{strlen($Ligne)-1}==';')
				{// Est-ce la fin d'une commande MySQL ?
					try{
						$res = $maconnexion->_bddQuery($LigneSQL) ;
					}
					catch(MsgException $e){
						$msgString = $e ->_getError();
						throw new MsgException($msgString, 'database') ;
					}
					$LigneSQL='';// RAZ de la ligne en cours
				}
			}
		}// wend
		return "Fichier '$fileName' traité avec succès\r\n";
	}

	public function _exportTable($tablesrc, $tablecib, $structure,$donnees,$format,$insertComplet="",
	$exportMethod="INSERT",$dropOption=true,$createOption=true,$whereOption='',
	$arDimsToExport=array(),$arDimsToRaz=array())
	{
		/* Parametres :
		 *    $tablesrc : nom de la table a exporter (en entree)
		 *    $tablecib : nom de la table d'export en sortie
		 *    $structure : true => sauvegarde de la structure des tables
		 *    $donnees : true => sauvegarde des donnes des tables
		 *    $format : format des donnees ('INSERT' => des clauses SQL INSERT,
		 *                                  'CSV' => donnees separees par des virgules)
		 *    $insertComplet (optionnel) : true => clause INSERT avec nom des champs
		 *    $exportMethod (optionnel) : "INSERT" (defaut) , "UPDATE", "REPLACE"
		 *    $dropOption (optionnel) : ajout DROP TABLE IF EXISTS
		 *    $createOption (optionnel) : ajout CREATE TABLE (from schema)
		 *    $whereOption (optionnel) : ajout WHERE '[chaine SQL]'
		 */

		$maconnexion = MysqlDatabase::GetInstance() ;

		// champs a ne pas intégrer pour la mise à jour de l'export agregat
		// car il s'agit de champs calculés lors du passage de t_agregat a t_agregat_export
		$arFieldsNotToSel=array('NB_UID','NB_CNX','DUREE','VOLUME');
		
		$format = strtoupper($format);

		$filename = $this->outputFileName;
		$fp = fopen($filename,"w");
		if (!is_resource($fp))
		return false;

		if ($structure === true)
		{
			if ($dropOption)
			fwrite($fp,"DROP TABLE IF EXISTS `$tablecib`;\n");

			// requete de creation de la table
			if ($createOption)
			{
				$query = "SHOW CREATE TABLE $tablecib";
				$resCreate = mysql_query($query);
				$row = mysql_fetch_array($resCreate);
				$schema = $row[1].";";
				fwrite($fp,"$schema\n\n");
			}
		}

		if ($donnees === true)
		{
			// les donnees de la table
			$query = "SELECT * FROM $tablesrc";
			if ($whereOption!="")
			$query.= " $whereOption ";


			$resData = $maconnexion->_bddQuery($query) ;

			if (mysql_num_rows($resData) > 0)
			{
				$sFieldnames = "";
				if (($insertComplet === true))
				{

					$num_fields = mysql_num_fields($resData);
					for($j=0; $j < $num_fields; $j++)
					{
						$curField=mysql_field_name($resData, $j);
						$sFieldnames .= "`".$curField."`,";
					}
					$sFieldnames = "(".substr($sFieldnames,0,-1).")";
				}

				$sInsert = $exportMethod." INTO `$tablecib` ";
				if ($insertComplet === true)
				$sInsert.= $sFieldnames;
				$sInsert.= " values ";

				while($rowdata = mysql_fetch_assoc($resData))
				{

					// si table denombrement : non prise en compte des dimensions intra
					if (
					($tablesrc=='r_denombrement') and
					(count($arDimsToExport)>0) and
					(!in_array($rowdata["REF_DIM"],$arDimsToExport))
					)
					continue;

					$ligne=array();
					$row=0;
					// formattage requete ultérieure de mise à jour de la date d'export
					if ($tablecib=="t_agregat" )
					{
						$newDate=date('Y-m-d H:i:s');
						$updQuery = "UPDATE t_agregat SET DATE_EXPORT = '".$newDate."' ";
						$updQuery.= "WHERE 1 ";
					}

					foreach($rowdata as $field=>$curdata)
					{

						// si autres : remise à zero des dimensions indiquées en paramètre
						if (in_array($field,$arDimsToRaz))
						{
							$curdata=0;
						}
							

						$arRepl=chr(13).chr(10);
						$curdata=addslashes($curdata);
						$curdata=preg_replace("/(.)?\\n/","\\r\\n",$curdata);
						$ligne[]=$curdata;

						if ($tablecib=="t_agregat" )
						{
							if (
							(!in_array($field,$arDimsToRaz))
							and (!in_array($field,$arFieldsNotToSel))
							)
							$updQuery.= " AND ".$field." = \"".$curdata."\" ";
						}
						$row+=1;
					}

					$this->updQuery[]=$updQuery;

					$lesDonnees = "<guillemet>".implode("<guillemet>,<guillemet>",$ligne)."<guillemet>";
					$lesDonnees = str_replace("<guillemet>","\"",($lesDonnees));

					if ($format == "INSERT")
					{
						$lesDonnees = "$sInsert($lesDonnees);";
					}
					fwrite($fp,"$lesDonnees\n");
				}
			}

		}
		fclose($fp);
		return true;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name className::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>