<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/application.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/affilIntra.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/dcplIntra.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/diplomeIntra.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/domEtuIntra.php') ;
//v1.5-PPR-#016
require_once (DIR_WWW.ROOT_APPL.'/app/common/compIntra.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/regInscIntra.php') ;

require_once (DIR_WWW.ROOT_APPL.'/app/common/traceLog.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/cnxLog.php') ;
/*v1.1-PPR ajout usage */
require_once (DIR_WWW.ROOT_APPL.'/app/common/usage.php') ;

/**
 *
 * <p>Cnx</p>
 *
 * @name  Cnx
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

class Cnx {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietes
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc Identifiant
	 */
	private $id;

	/**
	 * @var (String)
	 * @desc unique user id
	 */
	private $uid;

	/**
	 * @var (String)
	 * @desc Stamp unix de debut de connexion
	 */
	private $stampStart;

	/**
	 * @var (Int)
	 * @desc Volume du flux
	 */
	private $flowSize;

	/**
	 * @var (Int)
	 * @desc Duree du flux
	 */
	private $respTime;

	/**
	 * @var (Int)
	 * @desc ID Application
	 */
	private $idAppli;

	/**
	 * @var (Int)
	 * @desc ID Affiliation (Profil)
	 */
	private $idAffil;

	/**
	 * @var (Int)
	 * @desc ID Affecation (Discipline)
	 */
	private $idDcpl;

	/**
	 * @var (Int)
	 * @desc ID Diplome
	 */
	private $idDiplome;

	/**
	 * @var (Int)
	 * @desc ID Domaine Etude
	 */
	private $idDomEtu;

	/**
	 * @var (Int)
	 * @desc ID Composante
	 */
	private $idComposante;

	/**
	 * @var (Int)
	 * @desc ID Regime Inscription
	 */
	private $idRegInsc;

	/**
	 * @var (Int)
	 * @desc ID Structure
	 */
	private $idStruct;

	/**
	 * @var (String)
	 * @desc Stamp d'integration AGIMUS
	 */
	private $stampProcessed;

	/**
	 * @var (String)
	 * @desc User Agent
	 */
	private $userAgent;

	/**
	 * @var (String)
	 * @desc Identifiant Dimension Utilisateur 1
	 */
	private $userDim1;

	/**
	 * @var (String)
	 * @desc Identifiant Dimension Utilisateur 2
	 */
	private $userDim2;

	/**
	 * @var (String)
	 * @desc Identifiant Dimension Utilisateur 3
	 */
	private $userDim3;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. methodes
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>creation de l'instance de la classe</p>
	 *
	 * @name Cnx::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		$this->id=0;
		$this->uid='';
		$this->stampStart='';
		$this->flowSize=0;
		$this->respTime=0;
		$this->idAppli=0;
		$this->idAffil=0;
		$this->idDcpl=0;
		$this->idDiplome=0;
		$this->idDomEtu=0;
//v1.5-PPR-#016
		$this->idComposante=0;
		$this->idRegInsc=0;
		
		$this->idStruct=0;
		$this->stampProcessed='';
		$this->userAgent='';
		$this->userDim1='';
		$this->userDim2='';
		$this->userDim3='';

		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}

	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name Cnx::_getId()
	 * @return Int
	 */
	public function _getId() {
		return $this->id ;
	}

	/**
	 * @name Cnx::_getUid()
	 * @return String
	 */
	public function _getUid() {
		return $this->uid ;
	}

	/**
	 * @name Cnx::_getStampStart()
	 * @return String
	 */
	public function _getStampStart() {
		return $this->stampStart ;
	}

	/**
	 * @name Cnx::_getFlowSize()
	 * @return Int
	 */
	public function _getFlowSize() {
		return $this->flowSize ;
	}

	/**
	 * @name Cnx::_getrespTime()
	 * @return Int
	 */
	public function _getRespTime() {
		return $this->respTime ;
	}

	/**
	 * @name Cnx::_getIdAppli()
	 * @return Int
	 */
	public function _getIdAppli() {
		return $this->idAppli ;
	}

	/**
	 * @name Cnx::_getIdAffil()
	 * @return Int
	 */
	public function _getIdAffil() {
		return $this->idAffil ;
	}

	/**
	 * @name Cnx::_getIdDcpl()
	 * @return Int
	 */
	public function _getIdDcpl() {
		return $this->idDcpl ;
	}

	/**
	 * @name Cnx::_getIdDiplome()
	 * @return Int
	 */
	public function _getIdDiplome() {
		return $this->idDiplome ;
	}

	/**
	 * @name Cnx::_getIdDomEtu()
	 * @return Int
	 */
	public function _getIdDomEtu() {
		return $this->idDomEtu ;
	}
//v1.5-PPR-#016

	/**
	 * @name Cnx::_getIdComposante()
	 * @return Int
	 */
	public function _getIdComposante() {
		return $this->idComposante ;
	}
	/**
	 * @name Cnx::_getIdRegInsc()
	 * @return Int
	 */
	public function _getIdRegInsc() {
		return $this->idRegInsc ;
	}

	/**
	 * @name Cnx::_getIdStruct()
	 * @return Int
	 */
	public function _getIdStruct() {
		return $this->idStruct ;
	}

	/**
	 * @name Cnx::_getStampProcessed()
	 * @return String
	 */
	public function _getStampProcessed() {
		return $this->stampProcessed ;
	}

	/**
	 * @name Cnx::_getUserAgent()
	 * @return string
	 */
	public function _getUserAgent() {
		return $this->userAgent ;
	}

	/**
	 * @name Cnx::_getUserDim1()
	 * @return string
	 */
	public function _getUserDim1() {
		return $this->userDim1 ;
	}

	/**
	 * @name Cnx::_getUserDim2()
	 * @return string
	 */
	public function _getUserDim2() {
		return $this->userDim2 ;
	}

	/**
	 * @name Cnx::_getUserDim3()
	 * @return string
	 */
	public function _getUserDim3() {
		return $this->userDim3 ;
	}

	/**
	 * Accesseurs en ecriture
	 */

	/**
	 * @name Cnx::_setId()
	 * @param $id (Int)
	 * @return void
	 */
	public function _setId($id) {
		$this->id  = $id ;
	}

	/**
	 * @name Cnx::_setUid()
	 * @param $uid (String)
	 * @return void
	 */
	public function _setUid($uid) {
		$this->uid  = $uid ;
	}

	/**
	 * @name Cnx::_setStampStart()
	 * @param $stampStart (String)
	 * @return void
	 */
	public function _setStampStart($stampStart) {
		$this->stampStart  = $stampStart ;
	}

	/**
	 * @name Cnx::_setFlowSize()
	 * @param $flowSize (Int)
	 * @return void
	 */
	public function _setFlowSize($flowSize) {
		$this->flowSize  = $flowSize ;
	}

	/**
	 * @name Cnx::_setRespTime()
	 * @param $respTime (Int)
	 * @return void
	 */
	public function _setRespTime($respTime) {
		$this->respTime  = $respTime ;
	}

	/**
	 * @name Cnx::_setIdAppli()
	 * @param $idAppli (Int)
	 * @return void
	 */
	public function _setIdAppli($idAppli) {
		$this->idAppli  = $idAppli ;
	}

	/**
	 * @name Cnx::_setIdAffil()
	 * @param $idAffil (Int)
	 * @return void
	 */
	public function _setIdAffil($idAffil) {
		$this->idAffil  = $idAffil ;
	}

	/**
	 * @name Cnx::_setIdDcpl()
	 * @param $idDcpl (Int)
	 * @return void
	 */
	public function _setIdDcpl($idDcpl) {
		$this->idDcpl  = $idDcpl ;
	}

	/**
	 * @name Cnx::_setIdDiplome()
	 * @param $idDiplome (Int)
	 * @return void
	 */
	public function _setIdDiplome($idDiplome) {
		$this->idDiplome  = $idDiplome ;
	}

	/**
	 * @name Cnx::_setIdDomEtu()
	 * @param $idDomEtu (Int)
	 * @return void
	 */
	public function _setIdDomEtu($idDomEtu) {
		$this->idDomEtu  = $idDomEtu ;
	}
//v1.5-PPR-#016
	/**
	 * @name Cnx::_setIdComposante()
	 * @param $idIdComposante (Int)
	 * @return void
	 */
	public function _setIdComposante($idComposante) {
		$this->idComposante  = $idComposante ;
	}

	/**
	 * @name Cnx::_setIdRegInsc()
	 * @param $idIdRegInsc (Int)
	 * @return void
	 */
	public function _setIdRegInsc($idRegInsc) {
		$this->idRegInsc = $idRegInsc ;
	}

	/**
	 * @name Cnx::_setIdStruct()
	 * @param $idStruct (Int)
	 * @return void
	 */
	public function _setIdStruct($idStruct) {
		$this->idStruct  = $idStruct ;
	}

	/**
	 * @name Cnx::_setStampProcessed()
	 * @param $stampProcessed (String)
	 * @return void
	 */
	public function _setStampProcessed($stampProcessed) {
		$this->stampProcessed  = $stampProcessed ;
	}

	/**
	 * @name Cnx::_setUserAgent()
	 * @param $userAgent (string)
	 * @return void
	 */
	public function _setUserAgent($userAgent) {
		$this->userAgent  = $userAgent ;
	}

	/**
	 * @name Cnx::_setUserDim1()
	 * @param $userDim1 (string)
	 * @return void
	 */
	public function _setUserDim1($userDim1) {
		$this->userDim1  = $userDim1 ;
	}

	/**
	 * @name Cnx::_setUserDim2()
	 * @param $userDim2 (string)
	 * @return void
	 */
	public function _setUserDim2($userDim2) {
		$this->userDim2  = $userDim2 ;
	}

	/**
	 * @name Cnx::_setUserDim3()
	 * @param $userDim3 (string)
	 * @return void
	 */
	public function _setUserDim3($userDim3) {
		$this->userDim3  = $userDim3 ;
	}


	/**
	 * Compte du nombre d'occurence de l'objet Cnx dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name Cnx::_countRef()
	 * @return int
	 */
	public function _countRef($idAppli=0)
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_cnx ';
		if ($idAppli>0)
		$sql .= 'WHERE id_appli = \''.$idAppli.'\' ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Compte du nombre d'occurence restant a traiter
	 *
	 * <p>getNbToProcess</p>
	 *
	 * @name Cnx::_getNbToProcess()
	 * @return int
	 */
	public static function _getNbToProcess()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_cnx ';
		$sql .= ' WHERE STAMP_PROCESSED = \'0000-00-00 00:00:00\' ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * renvoit l'id d'un stamp existant
	 * v1.2-PPR-23032011 ajout test sur uid
	 * <p>_existsRef</p>
	 *
	 * @name Cnx::_existsRef()
	 * @param $stampStart string
	 * @return int
	 */
	public function _existsRef($uid, $stampStart)
	{
		$idRef = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT id ';
		$sql .= ' FROM t_cnx ';
		$sql .= ' WHERE UID = \''.$uid.'\' ';
		$sql .= ' AND STAMP_START = \''.$stampStart.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$idRef  = $row['id'] ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		return $idRef;
	}

	/**
	 * Creation d'un Cnx dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name Cnx::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM t_cnx ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}

		$sql  = 'INSERT INTO t_cnx VALUES( ';
		$sql .= '\''.AddSlashes($this->id).'\', ';
		$sql .= '\''.AddSlashes($this->uid).'\', ';
		$sql .= '\''.AddSlashes($this->stampStart).'\', ';
		$sql .= '\''.AddSlashes($this->flowSize).'\', ';
		$sql .= '\''.AddSlashes($this->respTime).'\', ';
		$sql .= '\''.AddSlashes($this->idAppli).'\', ';
		$sql .= '\''.AddSlashes($this->idDcpl).'\', ';
		$sql .= '\''.AddSlashes($this->idAffil).'\', ';
		$sql .= '\''.AddSlashes($this->idDiplome).'\', ';
		$sql .= '\''.AddSlashes($this->idDomEtu).'\', ';
//v1.5-PPR-#016 ajout nouvelles dimensions
		$sql .= '\''.AddSlashes($this->idComposante).'\', ';
		$sql .= '\''.AddSlashes($this->idRegInsc).'\', ';

		$sql .= '\''.AddSlashes($this->idStruct).'\', ';
		$sql .= '\''.AddSlashes($this->stampProcessed).'\', ';
		$sql .= '\''.AddSlashes($this->userAgent).'\', ';
		$sql .= '\''.AddSlashes($this->userDim1).'\', ';
		$sql .= '\''.AddSlashes($this->userDim2).'\', ';
		$sql .= '\''.AddSlashes($this->userDim3).'\' ';

		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise e jour d'un Cnx dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name Cnx::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_cnx SET ';
		$sql .= 'ID = \''.AddSlashes($this->id).'\', ';
		$sql .= 'UID = \''.AddSlashes($this->uid).'\', ';
		$sql .= 'STAMP_START = \''.AddSlashes($this->stampStart).'\', ';
		$sql .= 'FLOW_SIZE = \''.AddSlashes($this->flowSize).'\', ';
		$sql .= 'RESP_TIME = \''.AddSlashes($this->respTime).'\', ';
		$sql .= 'ID_APPLI = \''.AddSlashes($this->idAppli).'\', ';
		$sql .= 'ID_AFFIL = \''.AddSlashes($this->idAffil).'\', ';
		$sql .= 'ID_DCPL = \''.AddSlashes($this->idDcpl).'\', ';
		$sql .= 'ID_DIPLOME = \''.AddSlashes($this->idDiplome).'\', ';
		$sql .= 'ID_DOM_ETU = \''.AddSlashes($this->idDomEtu).'\', ';
//v1.5-PPR-#016 ajout nouvelles dimensions
		$sql .= 'ID_COMPOSANTE = \''.AddSlashes($this->idComposante).'\', ';
		$sql .= 'ID_REG_INSC = \''.AddSlashes($this->idRegInsc).'\', ';

		$sql .= 'ID_STRUCT = \''.AddSlashes($this->idStruct).'\', ';
		$sql .= 'STAMP_PROCESSED = \''.AddSlashes($this->stampProcessed).'\', ';
		$sql .= 'USER_AGENT = \''.AddSlashes($this->userAgent).'\', ';
		$sql .= 'USER_DIM_1 = \''.AddSlashes($this->userDim1).'\', ';
		$sql .= 'USER_DIM_2 = \''.AddSlashes($this->userDim2).'\', ';
		$sql .= 'USER_DIM_3 = \''.AddSlashes($this->userDim3).'\' ';

		$sql .= 'WHERE id=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise e jour de l'appli d'un Cnx dans la bdd
	 *
	 * <p>_updateAppli</p>
	 *
	 * @name Cnx::_updateAppli()
	 * @return void
	 */
	public function _updateAppli($appliSrc, $appliCib)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_cnx SET ';
		$sql .= 'ID_APPLI = \''.AddSlashes($appliCib).'\' ';
		$sql .= 'WHERE id_appli=\''.$appliSrc.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un Cnx dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name Cnx::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'DELETE FROM t_cnx ';
		$sql .= 'WHERE id = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Recuperation de la liste des Cnx
	 *
	 * <p>Liste des Cnx</p>
	 *
	 * @name Cnx::_getList()
	 * @return array
	 */
	public function _getList($notProcessed=false,$sortString="",$idRef="",$from=0,$to=0)
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_cnx ' ;
		$sql .= 'WHERE 1 ';

		if ($notProcessed)
		$sql .= ' AND STAMP_PROCESSED =\'0000-00-00 00:00:00\' ';

		if ($idRef!="")
		$sql .= ' AND ID = \''.$idRef.'\' ';

		if ($sortString!="")
		$sql .= ' ORDER BY '.$sortString ;

		if ($to>0)
			$sql .= ' LIMIT '.$from.','.$to;

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['ID'] = StripSlashes($row['ID']) ;
				$listArray[$row['ID']]['UID'] = StripSlashes($row['UID']) ;
				$listArray[$row['ID']]['STAMP_START'] = StripSlashes($row['STAMP_START']) ;
				$listArray[$row['ID']]['FLOW_SIZE'] = StripSlashes($row['FLOW_SIZE']) ;
				$listArray[$row['ID']]['RESP_TIME'] = StripSlashes($row['RESP_TIME']) ;
				$listArray[$row['ID']]['ID_APPLI'] = StripSlashes($row['ID_APPLI']) ;
				$listArray[$row['ID']]['ID_AFFIL'] = StripSlashes($row['ID_AFFIL']) ;
				$listArray[$row['ID']]['ID_DCPL'] = StripSlashes($row['ID_DCPL']) ;
				$listArray[$row['ID']]['ID_DIPLOME'] = StripSlashes($row['ID_DIPLOME']) ;
				$listArray[$row['ID']]['ID_DOM_ETU'] = StripSlashes($row['ID_DOM_ETU']) ;
//v1.5-PPR-#016
				$listArray[$row['ID']]['ID_COMPOSANTE'] = StripSlashes($row['ID_COMPOSANTE']) ;
				$listArray[$row['ID']]['ID_REG_INSC'] = StripSlashes($row['ID_REG_INSC']) ;

				$listArray[$row['ID']]['ID_STRUCT'] = StripSlashes($row['ID_STRUCT']) ;
				$listArray[$row['ID']]['STAMP_PROCESSED'] = StripSlashes($row['STAMP_PROCESSED']) ;
				$listArray[$row['ID']]['USER_AGENT'] = StripSlashes($row['USER_AGENT']) ;
				$listArray[$row['ID']]['USER_DIM_1'] = StripSlashes($row['USER_DIM_1']) ;
				$listArray[$row['ID']]['USER_DIM_2'] = StripSlashes($row['USER_DIM_2']) ;
				$listArray[$row['ID']]['USER_DIM_3'] = StripSlashes($row['USER_DIM_3']) ;
			}
		}
		return $listArray ;
	}


	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name Cnx:_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id==0) return false;

		$listArray=$this->_getList(false,'',$this->id);
		$this->uid=$listArray[$this->id]['UID'];
		$this->stampStart=$listArray[$this->id]['STAMP_START'] ;
		$this->flowSize=$listArray[$this->id]['FLOW_SIZE'] ;
		$this->respTime=$listArray[$this->id]['RESP_TIME']  ;
		$this->idAppli=$listArray[$this->id]['ID_APPLI']  ;
		$this->idAffil=$listArray[$this->id]['ID_AFFIL'] ;
		$this->idDcpl=$listArray[$this->id]['ID_DCPL']  ;
		$this->idDiplome=$listArray[$this->id]['ID_DIPLOME']  ;
		$this->idDomEtu=$listArray[$this->id]['ID_DOM_ETU']  ;
//v1.5-PPR-#016
		$this->idComposante=$listArray[$this->id]['ID_COMPOSANTE']  ;
		$this->idRegInsc=$listArray[$this->id]['ID_REG_INSC']  ;

		$this->idStruct=$listArray[$this->id]['ID_STRUCT']  ;
		$this->stampProcessed=$listArray[$this->id]['STAMP_PROCESSED'] ;
		$this->userAgent=$listArray[$this->id]['USER_AGENT'] ;
		$this->userDim1=$listArray[$this->id]['USER_DIM_1'] ;
		$this->userDim2=$listArray[$this->id]['USER_DIM_2'] ;
		$this->userDim3=$listArray[$this->id]['USER_DIM_3'] ;
	}


	/**
	 * v1.2-PPR-23032011 Acces au LDAP transféré dans traceLog.php (pb CNIL)
	 *
	 * <p>Analyse et génération de la ligne de collecte in_flow</p>
	 *
	 * @name Cnx::_analyzeAndGenCnx()
	 * @return void
	 */
	public function _analyzeAndGenCnx($templateString="",$separator=" ",$path='') {

	// Initialisations

		global $_arMatchLogFields,$_arMatchLogFieldsDoubleQuoted, $_matchMimeTypes,$arMonthConv,
		$_matchLdapAttributes,$_attribValueMandatory,$_arPreFilter;

		$nbFicLus=0;
		$nbFicOk=0;
		$nbLigLus=0;
		$nbRejets=0;
		$nbSupp=0;
		$nbCollOk=0;
		$debug=true;
		$config=$_SESSION['config'];
		
		//v1.2-PPR-28032011 paramètrage préfixe des dimensions hors application
		$prefix_new="";
		if (isset($config['prefix_new']))
			$prefix_new=$config['prefix_new'];
		//v1.2-PPR-28032011 ajout contrôle présence id_struct
		$idStruct="";
		if (!isset($config['id_struct']) or (trim($config['id_struct'])==''))
		{
				$mess="\r\nERREUR DE TRAITEMENT : le code structure est inconnu : vérifiez le ficheir /config/config.xml";
				$codeRet=20;
				return array($mess,$codeRet);
		}		
			
		$mess="";
		$codeRet=0;
		$arLdapLus=array();
		$logDir=ROOT_IN.$path;

		// tableau des valeurs d'attribut récupérées (pour log et enrichissement)
		$arLdapValuesCatched=array();
	

	// Analyse du répertoire des logs

		$arLogFile=array();

		if (($logDir=="") or (!is_dir($logDir)))
		return false;

		if ($handle = opendir($logDir)) {


			/* parcours du sas et lecture fichier par fichier */

			// Pour chaque log détectée 
			while (false !== ($file= readdir($handle))) {

				if ($file!=="." and $file !== ".." ) {

					if ($debug) echo "\r\n Lecture du fichier $file";
					$nbFicLus+=1;

					// Selection du filtre en fonction de la log

					$arFileName=explode('-',$file);
					$templateString=$arFileName[0];

					if (in_array($templateString,array_keys($_arMatchLogFields)))
					{

					$mess.="\r\nFichier $file accepté : template '$templateString' utilisé !";
					$nbFicOk+=1;

						// ouverture du fichier de sortie des rejets
						$fileName=substr($file,0,strrpos($file,'.'));
						$fileOut=$fileName."_rej.log";
						$fs = fopen($logDir."/".$fileOut,"w");

					$_matchLogFields=$_arMatchLogFields[$templateString];
					$_matchLogFieldsDoubleQuoted=$_arMatchLogFieldsDoubleQuoted[$templateString];

					//TODO : provoque des erreurs si chemin vers linux
					chmod($logDir."/".$file,0755);

						// ouverture du fichier de sortie des lignes intégrées
						$pathString=str_replace("/","-",$path);
						$pathString=str_replace("\\","-",$pathString);
						$archFileName=$fileName.'_'.$pathString.".log";
						$pathArray=explode("/",$path);
						$pathToCreate='';
						foreach($pathArray as $value)
						{
							if (!is_dir(ROOT_IN."/archives/".$pathToCreate."/".$value)) {
								if (!mkdir(ROOT_IN."/archives/".$pathToCreate."/".$value))
									 die("ARRET DU TRAITEMENT : impossible de creer le repertoire ".ROOT_IN."/archives/".$pathToCreate."/".$value);
								chmod(ROOT_IN."/archives/".$pathToCreate."/".$value,511); // equivalent 777 octal
							}
							$pathToCreate.="/".$value;
						}
						$fi = fopen(ROOT_IN."/archives/".$path."/".$archFileName,"a+");

					// Ouverture du fichier
					$fp = fopen($logDir."/".$file, 'r');
					$i = 0;
					$arLog=array();

					// ================== lecture des lignes du fichier

					while(!feof($fp)) {

							// --------------- Nettoyage de la ligne
							
							$curLine=fgets($fp);
							$nbLigLus+=1;

							if ($debug)
							{
								echo "\r\n".str_repeat("=",60);
								echo "\r\nLecture ligne $nbLigLus : $curLine \r\n ";
							}

								//v1.2-PPR-28032011 ejection des lignes ne comportant pas
								// la chaine de caractère spécifiée dans config.xml par "cas_string"
								// si elle existe
								$filter='';
								if (isset($_arPreFilter[$templateString]))
									$filter=$_arPreFilter[$templateString];
								
								//v1.3-PPR-23052011 Déploiement Tours correctif
								//if (($filter=='') or (!strstr(trim($curLine),$filter)))
								if (($filter!='') and (!strstr(trim($curLine),$filter)))
								{
									if ($debug) echo "\r\n ---> ligne $nbLigLus : la chaine de pré-filtrage [$filter] n'a pas été trouvé : ligne supprimée !";
									$nbSupp+=1;
									continue;
								}

								$arFields=explode($separator,trim($curLine));
								$arLogLine=array();
								foreach($arFields as $curField)
								{
									if (trim($curField)!="")
									$arLogLine[]=$curField;
								}
	
								$lineRed=implode($separator,$arLogLine);

								//v1.3-PPR ajout trim Ultime
								$lineRed=Session::_trimUltime($lineRed);
	
							//v1.0 obsolete
							$v1=false;
							if ($v1)
							{
								// --------------  Traitement de la ligne
								
			
								// recup classique des élements par separateur
								$curLogLine=explode($separator,trim($lineRed));
								// recup des elements entre quotes par expression rationnelle
								$logEntryPattern = "#[\"*]#";
								$arSplit=preg_split($logEntryPattern,$lineRed);
							}

							// ====== ANALYSE ET SPLIT DE LA LIGNE PAR EXPRESSION REGULIERE
							$logEntryPattern=$_matchLogFields["PATTERN"];
							//$mess.="\r\nligne $nbLigLus :  pattern => [".$logEntryPattern."]";
							if (!preg_match($logEntryPattern, $lineRed, $curLogLine))
							{
								$mess.="\r\n ---> rejet ligne $nbLigLus : Impossible d'analyser la ligne (erreur Regex) !";
								fwrite($fs,$lineRed." \n");
								$nbRejets+=1;
								continue;
							}
							if (!count($curLogLine)>0)
							{
								$mess.="\r\n ---> rejet ligne $nbLigLus : Analyse Regex vide !";
								fwrite($fs,$lineRed." \n");
								$nbRejets+=1;
								continue;
							}

							if ($debug) echo "\r\n ligne $nbLigLus : ligne regex analysée avec succès."	;			
							// Formattage de l'ID unique
							$uid=$curLogLine[$_matchLogFields["UID"]];

							// ejection des lignes sans uid
							if ($uid=="-")
							{
								if ($debug) echo "\r\n ---> ligne $nbLigLus : l'UID n'existe pas !";
								$nbSupp+=1;
								continue;
							}
		
							//MATCHING :
							//controle que le type mime est bien selectionne dans l'application (param.php)
							$mimeType=$curLogLine[$_matchLogFields["MIME_TYPE"]];
		
							//v1.3-PPR-25042011 filtrage mimetype par template
							if ((count($_matchMimeTypes[$templateString])==0) or (in_array($mimeType,$_matchMimeTypes[$templateString])))
							{

								//v1.3-PPR 23052011 traitement spécifique logs esup
								if ($templateString=='esup') {
									$stampStart=$curLogLine[$_matchLogFields["STAMP_DATE"]];
									$stampStart.=" ".substr($curLogLine[$_matchLogFields["STAMP_HOUR"]],0,8);
									$arD=preg_split("#[- :]#",$stampStart);

									// controle si le stamp est réellement un stamp
									if (
									(myIsInt($arD[0])) and
									(myIsInt($arD[1])) and
									(myIsInt($arD[2])) and
									(myIsInt($arD[3])) and
									(myIsInt($arD[4])) and
									(myIsInt($arD[5]))
									)
									$stampStart=mktime($arD[3],$arD[4],$arD[5],$arD[1],$arD[2],$arD[0]);
									else
									{
										if ($debug) echo "\r\n ligne $nbLigLus : le Stamp est incorrect [$stampStart] !";
										$nbSupp+=1;
										continue;
									}
								}
								else {
									// formattage eventuel du stamp au format unix
									$stampStart=$curLogLine[$_matchLogFields["STAMP_START"]];
			
									$arStamp=preg_split("#^\[*#",$stampStart);
									$arD=preg_split("#[/:]#",$arStamp[1]);
			
									//conversion mois alpha en nombre
									$arD[1]=$arMonthConv[$arD[1]];
								
								// controle si le stamp est réellement un stamp
								if (
								(myIsInt($arD[0])) and
								(myIsInt($arD[1])) and
								(myIsInt($arD[2])) and
								(myIsInt($arD[3])) and
								(myIsInt($arD[4])) and
								(myIsInt($arD[5]))
								)
								$stampStart=mktime($arD[3],$arD[4],$arD[5],$arD[1],$arD[0],$arD[2]);
								else
								{
									if ($debug) echo "\r\n ligne $nbLigLus : le Stamp est incorrect [$stampStart] !";
									$nbSupp+=1;
									continue;
								}
								}
		
								if (isset($_matchLogFields["STAMP_MILLI"]))
								$stampStart.=".".$curLogLine[$_matchLogFields["STAMP_MILLI"]];
		
								//controle que le stamp n'a pas deja ete traite
								$idRef=$this->_existsRef($_matchLogFields["UID"], $stampStart);
								//si le stamp existe deja dans la base : on supprime
								if ($idRef==0)
								{
									$newCnxLog = new CnxLog();
									$newCnxLog->_setId($nbLigLus);
									$newCnxLog->_setUid($uid);
									$newCnxLog->_setStampStart($stampStart);
									$newCnxLog->_setFlowSize($curLogLine[$_matchLogFields["FLOW_SIZE"]]);
									//v1.5-PPR-#000 suppression message notice
									$newCnxLog->_setRespTime(0);
									if (isset($curLogLine[$_matchLogFields["RESP_TIME"]]))
										$newCnxLog->_setRespTime($curLogLine[$_matchLogFields["RESP_TIME"]]);
		
									// récupération de l'application	
									//$targetUrl=$arSplit[$_matchLogFieldsDoubleQuoted["TARGET_URL"]];
									$targetUrl=$curLogLine[$_matchLogFields["TARGET_URL"]];
									$esupFonc=$curLogLine[$_matchLogFields["FUNCTION"]];

									//v1.3-PPR 23052011 traitement spécifique ESUP
									if ($templateString=='esup') {
										if ($esupFonc=='SSTART')
											$targetUrl='esup-portail';

										$newCnxLog->_setTargetUrl($targetUrl);
									}
									else
									{
										if (GET_SHORT_URL)
										//v1.3-PPR-24052011 
										// $targetUrl=substr($targetUrl,0,strpos($targetUrl,"?")-1);
										$targetUrl=substr($targetUrl,0,strpos($targetUrl,"?"));
										if ($targetUrl!="")
										$newCnxLog->_setTargetUrl($targetUrl);
										else
										{
											if ($debug) echo "\r\n ligne $nbLigLus : l'URL est incorrecte [$targetUrl] !";
											$nbSupp+=1;
											continue;
										}
									}
									
									// collecte des données entre doublequotes
									//$userAgent=$newCnxLog->_getBrowser($arSplit[$_matchLogFieldsDoubleQuoted["USER_AGENT"]]);
									//v1.5-PPR-#000 suppression message notice
									$userAgent="";
									if (isset($curLogLine[$_matchLogFields["USER_AGENT"]]))
										$userAgent=$newCnxLog->_getBrowser($curLogLine[$_matchLogFields["USER_AGENT"]]);
									$newCnxLog->_setUserAgent($userAgent);
									$newCnxLog->_setMimeType($mimeType);

									// ===================   Creation de la ligne dans t_cnx
						
									if ($debug)
									{
										echo "\r\n".str_repeat("~",60);
										echo "\r\nTraitement ligne no ".$newCnxLog->_getId(). " : ".$newCnxLog->_getStampStart();
									}
												
									$uidToFind=$newCnxLog->_getUid();

									// Si l'uid est un cookie de trace, on recherche son vrai userID dans le trace log
									//v1.3-PPR 23052011 
									//if (UID_IS_COOKIE_TRACE)
									{
										if ($debug)	echo "\r\n ligne $nbLigLus : Recherche UID lie au cookie [".$newCnxLog->_getUid()."]";
				
										$idTrace=TraceLog::_existsRef($newCnxLog->_getUid());
										if ($idTrace>0)
										{
											$newTrace=new TraceLog($idTrace);
											// on remplace par le vrai uid trouve dans t_traceLog
											$uidToFind=$newTrace->_getUid();
											if ($debug)	echo " --> trouve [".$uidToFind."]";
				
											//TODO : mettre a jour le stamp pour confirmer qu'il a ete accede
										}
										else
										{
											$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : Cookie trace non trouve dans dans t_tracelog [".$newCnxLog->_getUid()."]";
											fwrite($fs,$lineRed." \n");
											$nbRejets+=1;
											continue;
										}
									}
				
									// memorisation de la correspondance entre tracelog et uid en clair
									if (!isset($arUid[$uidToFind]))
									$arUid[$uidToFind]=$newCnxLog->_getUid();
				
									if (!isset($arLdapLus[$uidToFind]))
									{
										// Alimentation du tableau lDAP à partir de t_tracelog
										$affiliation=$newTrace->_getDim1();
										$discipline=$newTrace->_getDim2();
										$diplome=$newTrace->_getDim3();
										$dometu=$newTrace->_getDim4();
//v1.5-PPR-#016
										$composante=$newTrace->_getDim5();
										$reginsc=$newTrace->_getDim6();
										
										$arLdapLus[$uidToFind]=implode("|",array($affiliation,$discipline,$diplome,$dometu,$composante,$reginsc));
										if ($debug)	echo "\r\n. Attributs LDAP detectes et memorises ";
									}
									else
									{
										list($affiliation,$discipline,$diplome,$dometu,$composante,$reginsc)=explode("|",$arLdapLus[$uidToFind]);
										if ($debug)	echo "\r\n. Attributs LDAP recuperes de arLdapLus ";
									}
				
				
									// controle des donnees qualifiantes
				
									if (!$affiliation!="")
									{
										if ($_attribValueMandatory['affiliation']==1)
										{
											$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : Affiliation -> aucune donnee '".$_matchLdapAttributes['affiliation']."' pour cet uid [".$uidToFind."]";
											fwrite($fs,$lineRed." \n");
											$nbRejets+=1;
											continue;
										}
									}
									if (!$discipline!="")
									{
										if ($_attribValueMandatory['discipline']==1)
										{
											$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : Discipline -> aucune donnee '".$_matchLdapAttributes['discipline']."' pour cet uid [".$uidToFind."]";
											fwrite($fs,$lineRed." \n");
											$nbRejets+=1;
											continue;
										}
									}
									if (!$diplome!="")
									{
										if ($_attribValueMandatory['diplome']==1)
										{
											$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : Diplome -> aucune donnee '".$_matchLdapAttributes['diplome']."' pour cet uid [".$uidToFind."]";
											fwrite($fs,$lineRed." \n");
											$nbRejets+=1;
											continue;
										}
									}
									if (!$dometu!="")
									{
										if ($_attribValueMandatory['dometu']==1)
										{
											$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : Domaine Etudes -> aucune donnee '".$_matchLdapAttributes['dometu']."' pour cet uid [".$uidToFind."]";
											fwrite($fs,$lineRed." \n");
											$nbRejets+=1;
											continue;
										}
									}
//v1.5-PPR-#016
									if (!$composante!="")
									{
										if ($_attribValueMandatory['composante']==1)
										{
											$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : Composante -> aucune donnee '".$_matchLdapAttributes['composante']."' pour cet uid [".$uidToFind."]";
											fwrite($fs,$lineRed." \n");
											$nbRejets+=1;
											continue;
										}
									}

									if (!$reginsc!="")
									{
										if ($_attribValueMandatory['reginsc']==1)
										{
											$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : Regime Inscription -> aucune donnee '".$_matchLdapAttributes['reginsc']."' pour cet uid [".$uidToFind."]";
											fwrite($fs,$lineRed." \n");
											$nbRejets+=1;
											continue;
										}
									}
				
									if ($debug)	echo "\r\n. Controle qualifiants effectue ";
				
									// creation de la nouvelle ligne de collecte
				
									$dateJour=date('Y-m-d');
				
									//$this->uid=$this->_getUid(); // on mémorise l'UID initial y compris s'il est crypté
									$this->uid=$arUid[$uidToFind];
									$this->stampStart=$newCnxLog->_getStampStart();
									$this->flowSize=$newCnxLog->_getFlowSize();
									$this->respTime=$newCnxLog->_getRespTime();
									$this->idStruct=$config['id_struct'];
									
									$this->_setUserAgent($newCnxLog->_getUserAgent());
				
									// ----------------- recherche identifiant application
				
									$arApplis=Application::_existsRef($newCnxLog->_getTargetUrl(),0,1); //0 : recherche concordance
									if (count($arApplis)==1) // une seule application trouvee
									{
										$this->idAppli=$arApplis[0];
									}
									if (count($arApplis)>1)
									{
										$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : Plusieurs applications detectees (url [".$newCnxLog->_getTargetUrl()."] ";
										fwrite($fs,$lineRed." \n");
										$nbRejets+=1;
										continue;
									}

									if (count($arApplis)==0)
									{
										//v1.2-PPR28032011 modif des règles d'enrichissement automatique
										if (CREATE_UNKNOWN_DIM)
										{
											$newAppl=new Application();
											$newAppl->_setLib("~Application detectee le ".$dateJour);
											$newAppl->_setDsc($newCnxLog->_getTargetUrl());
											//v1.2-PPR-suppression du $
											$newAppl->_setUrlType("#^".$newCnxLog->_getTargetUrl()."$#");
											//$newAppl->_setUrlType("#^".str_replace('(', "\(", str_replace(')', "\)", str_replace('\'', "\\'", $newCnxLog->_getTargetUrl())))."#");
											$newAppl->_setIdSrvIntra(!$_attribValueMandatory['application']);
											$newAppl->_setIdStruct($newCnxLog->idStruct);
											$newAppl->_create();
											$this->idAppli=$newAppl->_getId(); 
											//$mess.="\r\n---> référentiel Application mis à jour ";
										}
										else
										{
											if ($_attribValueMandatory['application']==1)
											{
												$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : application introuvable (url [".$newCnxLog->_getTargetUrl()."] ";
												fwrite($fs,$lineRed." \n");
												$nbRejets+=1;
					
												continue;
											}
											else
											$this->idAppli=1; // autre
											}
										}
				
									if ($debug)	echo "\r\n. Recherche Application ".$newCnxLog->_getTargetUrl()." effectuee : [$this->idAppli]";
				
									// ------------- recherche identifiant affiliation
									
									//v1.2-PPR-28032011 tritement des valeurs non obligatoires et inexistantes dans LDAP
									if (trim($affiliation)=="")
									$arAffil[0]=1;
									else				
									$arAffil=AffilIntra::_existsRef($affiliation);
									
									if (count($arAffil)==1) // une seule occurence trouvee
									{
										$this->idAffil=$arAffil[0];
									}
									if (count($arAffil)>1)
									{
										$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : Plusieurs affiliations detectees [$affiliation] ";
										fwrite($fs,$lineRed." \n");
										$nbRejets+=1;
										continue;
									}
									if (count($arAffil)==0)
									{
										//v1.2-PPR28032011 modif des règles d'enrichissement automatique
										if (CREATE_UNKNOWN_DIM)
										{
											$newAffil=new AffilIntra();
											$newAffil->_setLib($prefix_new.$affiliation);
											$newAffil->_setDsc($affiliation);
											//v1.2-PPR-suppression du $
											//$newAffil->_setChaineType("#^".$affiliation."$#");
											$newAffil->_setChaineType("#^".str_replace('(', "\(", str_replace(')', "\)", str_replace('\'', "\\'", $affiliation)))."$#");
											$newAffil->_setIdRegr(!$_attribValueMandatory['affiliation']);
											$newAffil->_setIdStruct($config['id_struct']);
											$newAffil->_create();
											$this->idAffil=$newAffil->_getId();
											if ($debug)	echo "\r\n. ---> référentiel AffilIntra mis à jour ";
										}
										else
										{
											if ($_attribValueMandatory['affiliation']==1)
											{
												$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : affiliation introuvable [$affiliation] ";
												fwrite($fs,$lineRed." \n");
												$nbRejets+=1;
												continue;
											}
											else
											$this->idAffil=1; // autre
										}
									}
				
									if ($debug)	echo "\r\n. Recherche Affiliation [$affiliation] effectuee : [$this->idAffil]";
				
									//-----------------  recherche identifiant discipline
				
								//v1.2-PPR-28032011 tritement des valeurs non obligatoires et inexistantes dans LDAP
									if (trim($discipline)=="")
										$arDcpl[0]=1;
									else				
										$arDcpl=DcplIntra::_existsRef($discipline);

									if (count($arDcpl)==1) // une seule occurence trouvee
									{
										$this->idDcpl=$arDcpl[0];
									}
									if (count($arDcpl)>1)
									{
										$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : Plusieurs disciplines detectees [$discipline] ";
										fwrite($fs,$lineRed." \n");
										$nbRejets+=1;
										continue;
									}
									if (count($arDcpl)==0)
									{
										//v1.2-PPR28032011 modif des règles d'enrichissement automatique
										if (CREATE_UNKNOWN_DIM)
										{
											$newDcpl=new DcplIntra();
											$newDcpl->_setLib($prefix_new.$discipline);
											$newDcpl->_setDsc($discipline);
											//v1.2-PPR-suppression du $
											//$newDcpl->_setChaineType("#^".$discipline."$#");
											$newDcpl->_setChaineType("#^".str_replace('(', "\(", str_replace(')', "\)", str_replace('\'', "\\'", $discipline)))."$#");
											$newDcpl->_setIdStruct($config['id_struct']);
											// Décision HKR du 23/09 : On force la filière nationale à 1 (autres filieres)
											$newDcpl->_setIdRegr(1);
											$newDcpl->_create();
											$this->idDcpl=$newDcpl->_getId();
											if ($debug)	echo "\r\n. ---> référentiel DcplIntra mis à jour ";
										}
										else
										{
											// Décision HKR du 23/09 : On force la filière nationale à 1 (autres filieres)
											// v1.5-PPR-#000 annulation décision suite à redéfinition des référentiels par DREY
											if ($_attribValueMandatory['discipline']==1)
											{
												$mess.="\r\nRejet ligne ".$this->_getId()." : discipline introuvable [$discipline] ";
												$nbRejets+=1;
												continue;
											}
											else
											if ($this->idDcpl==0)
											$this->idDcpl=1; // autre
										}
									}
				
									if ($debug)	echo "\r\n. Recherche Discipline [$discipline] effectuee [$idDcpl]";
				
									// -------------------- recherche identifiant Diplome
				
								//v1.2-PPR-28032011 tritement des valeurs non obligatoires et inexistantes dans LDAP
									if (trim($diplome)=="")
										$arDiplome[0]=1;
									else				
									$arDiplome=DiplomeIntra::_existsRef($diplome);

									if (count($arDiplome)==1) // une seule occurence trouvee
									{
										$this->idDiplome=$arDiplome[0];
									}
									if (count($arDiplome)>1)
									{
										$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : Plusieurs diplomes detectees [$diplome] ";
										fwrite($fs,$lineRed." \n");
										$nbRejets+=1;
										continue;
									}
									if (count($arDiplome)==0)
									{
										//v1.2-PPR28032011 modif des règles d'enrichissement automatique
										if (CREATE_UNKNOWN_DIM)
										{
											$newDip=new DiplomeIntra();
											$newDip->_setLib($prefix_new.$diplome);
											$newDip->_setDsc($diplome);
											//v1.2-PPR-suppression du $
											//$newDip->_setChaineType("#^".$diplome."$#");
											$newDip->_setChaineType("#^".str_replace('(', "\(", str_replace(')', "\)", str_replace('\'', "\\'", $diplome)))."$#");
											$newDip->_setIdRegr(!$_attribValueMandatory['diplome']);
											$newDip->_setIdStruct($config['id_struct']);
											$newDip->_create();
											$this->idDiplome=$newDip->_getId();
											if ($debug)	echo "\r\n---> référentiel DiplomeIntra mis à jour ";
										}
										if ($_attribValueMandatory['diplome']==1)
										{
											$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : diplome introuvable [$diplome] ";
											fwrite($fs,$lineRed." \n");
											$nbRejets+=1;
											continue;
										}
										else
										$this->idDiplome=1; //autre
									}
				
									if ($debug)	echo "\r\n. Recherche Diplome [$diplome] effectuee : [$this->idDiplome]";
				
									//--------------------- recherche identifiant domaine etudes
								//v1.2-PPR-28032011 tritement des valeurs non obligatoires et inexistantes dans LDAP
									if (trim($dometu)=="")
										$arDomEtu[0]=1;
									else				
										$arDomEtu=DomEtuIntra::_existsRef($dometu);

									if (count($arDomEtu)==1) // une seule occurence trouvee
									{
										$this->idDomEtu=$arDomEtu[0];
									}
									if (count($arDomEtu)>1)
									{
										$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : Plusieurs dometus detectees [$dometu] ";
										fwrite($fs,$lineRed." \n");
										$nbRejets+=1;
										continue;
									}
									if (count($arDomEtu)==0)
									{
										//v1.2-PPR28032011 modif des règles d'enrichissement automatique
										if (CREATE_UNKNOWN_DIM)
										{
											$newDom=new DomEtuIntra();
											$newDom->_setLib($prefix_new.$dometu);
											$newDom->_setDsc($dometu);
											//v1.2-PPR-suppression du $
											//$newDom->_setChaineType("#^".$dometu."$#");
											$newDom->_setChaineType("#^".str_replace('(', "\(", str_replace(')', "\)", str_replace('\'', "\\'", $dometu)))."$#");
											$newDom->_setIdRegr(!$_attribValueMandatory['dometu']);
											$newDom->_setIdStruct($config['id_struct']);
											$newDom->_create();
											$this->idDomEtu=$newDom->_getId(); 
											if ($debug)	echo "\r\n---> référentiel DomEtuIntra mis à jour ";
										}
										else
										{
											if ($_attribValueMandatory['dometu']==1)
											{
												$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : domaine etude introuvable [$dometu] ";
												fwrite($fs,$lineRed." \n");
												$nbRejets+=1;
												continue;
					
											}
											else
											$this->idDomEtu=1; // autre
										}
									}

									if ($debug)	echo "\r\n. Recherche DomEtu [$dometu] effectuee : [$this->idDometu]";

//v1.5-PPR-#016 ajout nouvelles dimensions

									//--------------------- recherche identifiant composante
									if (trim($composante)=="")
										$arComposante[0]=1;
									else				
										$arComposante=CompIntra::_existsRef($composante);
										
									echo "\r\nDEBUG";
									echo "\r\ncomposante : $composante";
									echo Session::_affPr($arComposante);	

									if (count($arComposante)==1) // une seule occurence trouvee
									{
										$this->idComposante=$arComposante[0];
									}
									if (count($arComposante)>1)
									{
										$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : Plusieurs composantes detectees [$composante] ";
										fwrite($fs,$lineRed." \n");
										$nbRejets+=1;
										continue;
									}
									if (count($arComposante)==0)
									{
										//v1.2-PPR28032011 modif des règles d'enrichissement automatique
										if (CREATE_UNKNOWN_DIM)
										{
											$newComp=new CompIntra();
											$newComp->_setLib($prefix_new.$composante);
											$newComp->_setDsc($composante);
											//v1.2-PPR-suppression du $
											//$newComp->_setChaineType("#^".$composante."$#");
											$newComp->_setChaineType("#^".str_replace('(', "\(", str_replace(')', "\)", str_replace('\'', "\\'", $composante)))."$#");
											$newComp->_setIdRegr(!$_attribValueMandatory['composante']);
											$newComp->_setIdStruct($config['id_struct']);
											$newComp->_create();
											$this->idComposante=$newComp->_getId(); 
											if ($debug)	echo "\r\n. ---> référentiel Composante Intra mis à jour ";
										}
										else
										{
											if ($_attribValueMandatory['composante']==1)
											{
												$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : composante introuvable [$composante] ";
												fwrite($fs,$lineRed." \n");
												$nbRejets+=1;
												continue;
											}
											else
											$this->idComposante=1; // autre
										}
									}

									if ($debug)	echo "\r\n. Recherche Composante [$composante] effectuee : [$this->idComposante]";

									//--------------------- recherche identifiant Regime d'inscription
									if (trim($reginsc)=="")
										$arRegInsc[0]=1;
									else				
										$arRegInsc=RegInscIntra::_existsRef($reginsc);

									if (count($arRegInsc)==1) // une seule occurence trouvee
									{
										$this->idRegInsc=$arRegInsc[0];
									}
									if (count($arRegInsc)>1)
									{
										$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : Plusieurs régimes detectes [$reginsc] ";
										fwrite($fs,$lineRed." \n");
										$nbRejets+=1;
										continue;
									}
									if (count($arRegInsc)==0)
									{
										//v1.2-PPR28032011 modif des règles d'enrichissement automatique
										if (CREATE_UNKNOWN_DIM)
										{
											$newRegInsc=new RegInscIntra();
											$newRegInsc->_setLib($prefix_new.$reginsc);
											$newRegInsc->_setDsc($reginsc);
											//v1.2-PPR-suppression du $
											//$newRegInsc->_setChaineType("#^".$reginsc."$#");
											$newRegInsc->_setChaineType("#^".str_replace('(', "\(", str_replace(')', "\)", str_replace('\'', "\\'", $reginsc)))."$#");
											$newRegInsc->_setIdRegr(!$_attribValueMandatory['reginsc']);
											$newRegInsc->_setIdStruct($config['id_struct']);
											$newRegInsc->_create();
											$this->idRegInsc=$newRegInsc->_getId(); 
											if ($debug)	echo "\r\n---> référentiel Regime Inscription Intra mis à jour ";
										}
										else
										{
											if ($_attribValueMandatory['reginsc']==1)
											{
												$mess.="\r\nRejet ligne ".$newCnxLog->_getId()." : Regime inscription introuvable [$reginsc] ";
												fwrite($fs,$lineRed." \n");
												$nbRejets+=1;
												continue;
											}
											else
											$this->idRegInsc=1; // autre
										}
									}
				
									if ($debug)	echo "\r\n. Recherche Regime inscription [$reginsc] effectuee : [$this->idRegInsc]";
				
								
									// TOUT EST OK : on cree la ligne de collecte dans t_cnx
									$this->_create();
									if ($debug)	echo "\r\nLigne de collecte no ".$this->id." creee";
									fwrite($fi,$curLine);
									if ($debug)	echo "\r\nLigne de collecte no ".$this->id." ecrite dans fichier d'archives";


									// v1.1 =========== Alimentation de la table t_usage

									$keyUidCrypt=sha1($this->uid);
									
									if ((!defined(REF_DIM_USAGE)) or (!(REF_DIM_USAGE>0)))
										$keyRefDim=7; // servIntra
									else	
									$keyRefDim=REF_DIM_USAGE;
									$newAppli= new Application($this->_getIdAppli());
									$idSrvIntra=$newAppli->_getIdSrvIntra();
									if ($idSrvIntra>0)
										$keyIdDim=$idSrvIntra;
									else
										$keyIdDim=1; // Autres
									
									$keyDate=strftime('%y-%m-%d',$this->_getStampStart());

									$newUsage=new Usage();
									$usageExists=$newUsage->_countRef($keyUidCrypt,$keyRefDim,$keyIdDim,$keyDate);

									if (!$usageExists)
									{
										$newUsage->_setUid($keyUidCrypt);
										$newUsage->_setRefDim($keyRefDim);
										$newUsage->_setIdDim($keyIdDim);
										$newUsage->_setDateAcces($keyDate);
										$newUsage->_create();
										if ($debug)	echo "\r\n. Ligne $nbLigLus : Usage ajouté pour le tuple [$keyRefDim][$keyIdDim][$keyIdDim][$keyDate]";
									}
									else
									{
										if ($debug)	echo "\r\n. Ligne $nbLigLus : Usage existe déjà le tuple [$keyRefDim][$keyIdDim][$keyIdDim][$keyDate]";
									}
									
									$nbCollOk+=1;

								} // idRef = 0
								else // DÉJÀ TRAITÉ
								{
									$mess.="\r\nRejet ligne ".$idRef." : StampStart [$stampStart] deja traite !";
									$nbSupp+=1;
									continue;
		
								}
							} // Match ok sur matchType
							else
							{
								//v1.3-PPR-23052011
								//$mess.="\r\nRejet ligne ".$nbLigLus." : la ligne ne matche pas le mimeType !";
								//v1.5-PPR-#003 $nbRejets+=1;
								$nbSupp+=1;
								continue;
							}
							
						$i++;
					} // Boucle sur chaque ligne du fichier lu
					fclose($fp);
					fclose($fs);
					fclose($fi);

					// suppression du fichier de log initial
					if (!unlink($logDir."/".$file))
					{
						echo "\r\nFichier d'entrée [$file] impossible à supprimer !";
						$codeRet=10;
					}			
					else
						$mess.="\r\nFichier d'entrée [$file] supprimé.";	

					
				  } // match sur template de nom de fichier 
				} // fichier différent de '.' et '..'


			} // boucle sur chaque fichier lu
			
		} // handle opendir

			//analyse du tableau des uid récupérés pour affichage des valeurs ldap connues
			foreach($arLdapLus as $curUid=>$arUidValues)
			{
				list($affiliation,$discipline,$diplome,$dometu)=explode("|",$arUidValues);
				$arLdapValuesCatched['affiliation'][$affiliation]+=1;
				$arLdapValuesCatched['discipline'][$discipline]+=1;
				$arLdapValuesCatched['diplome'][$diplome]+=1;
				$arLdapValuesCatched['dometu'][$dometu]+=1;
//v1.5-PPR-#016
				$arLdapValuesCatched['composante'][$composante]+=1;
				$arLdapValuesCatched['reginsc'][$reginsc]+=1;
			}

			// affichage des valeurs LDAP connues
			$mess.="\r\n\r\nDénombrements\r\n------------------\r\n ";

			$mess.="\r\nNombre d'UID detectes : ".count($arUid)." (LDAP : ".count($arLdapLus).")\r\n";

			if (is_array($arLdapValuesCatched['affiliation']))
			{$mess.="\r\nProfil  : ";foreach($arLdapValuesCatched['affiliation'] as $val=>$nb) {$mess.="\r\n...".$val."[$nb]";}}
			if (is_array($arLdapValuesCatched['discipline']))
			{$mess.="\r\nFiliere  : ";foreach($arLdapValuesCatched['discipline'] as $val=>$nb) {$mess.="\r\n...".$val."[$nb]";}}
			if (is_array($arLdapValuesCatched['diplome']))
			{$mess.="\r\nDiplome : ";foreach($arLdapValuesCatched['diplome'] as $val=>$nb) {$mess.="\r\n...".$val."[$nb]";}}
			if (is_array($arLdapValuesCatched['dometu']))
			{$mess.="\r\nDomaine Etude  : ";foreach($arLdapValuesCatched['dometu'] as $val=>$nb) {$mess.="\r\n...".$val."[$nb]";}}
//v1.5-PPR-#016
			if (is_array($arLdapValuesCatched['composante']))
			{$mess.="\r\nComposante  : ";foreach($arLdapValuesCatched['composante'] as $val=>$nb) {$mess.="\r\n...".$val."[$nb]";}}
			if (is_array($arLdapValuesCatched['reginsc']))
			{$mess.="\r\nRegime Inscription  : ";foreach($arLdapValuesCatched['reginsc'] as $val=>$nb) {$mess.="\r\n...".$val."[$nb]";}}

			// totaux de controle
			$mess.="\r\nNombre de fichiers lus                             : $nbFicLus";
			$mess.="\r\nNombre de fichiers acceptés                        : $nbFicOk";
			$mess.="\r\nNombre de lignes traitees                          : $nbLigLus";
			$mess.="\r\nNombre de lignes integrees en base (t_cnx)         : $nbCollOk";
			$mess.="\r\nNombre de lignes rejetees (dans fichier .rej)      : $nbRejets";
			$mess.="\r\nNombre de lignes supprimées (non prises en compte) : $nbSupp";
			if ($nbLigLus!=($nbCollOk+$nbRejets+$nbSupp))
			{
				$mess.="\r\nAnomalie dans le traitement : le nombre lu ne correspond pas a la somme integre + rejete + supprimees :";
				$codeRet=10;
			}
			else
			{
				$codeRet=30;
			}

		return array($mess,$codeRet);

	}


	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Cnx::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>
