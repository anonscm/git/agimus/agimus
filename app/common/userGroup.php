<?php
  require_once( 'mysqlDatabase.php') ;
  require_once( 'msgException.php') ;
  require_once( 'langue.php') ;
  require_once( 'moduleRights.php') ;
  
/**
 * 
 * <p>User</p>
 * 
 * @name User
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright Capella Conseil 2009
 * @version 1.0.0
 * @package common
 */
 
 class UserGroup{
 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/
    

    /**
    * @var $gid(Int)
    * @desc Identifiant du groupe d'utilisateur
    */
    private $gid;
    /**
    * @var libelleArray(Array)
    * @desc tableau des libelles du groupe d'utilisateurs par langue
    */
	private $libelleArray ;
	
	/**
    * @var mainUserArray(Array)
    * @desc tableau des utilisateurs dont le groupe est le principal
    */
	private $mainUserArray ;
	
	/**
    * @var otherUserArray(Array)
    * @desc tableau des utilisateurs dont le groupe est un secondaire
    */
	private $otherUserArray ;
    
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name Person::__construct()
    * @return void 
    */
    public function __construct() {
    	$this->gid = 0 ;
		$this->libelleArray = array() ;
		$this->mainUserArray = array() ;
		$this->otherUserArray = array() ;
    } 

    /**
    * Accesseurs en lecture
    */ 
    /** 
    * @name UserGroup::_getGid()
    * @return Int 
    */
    public function _getGid()
    {
      return $this->gid ;
    }      
 
    /**
    * Accesseurs en �criture
    */ 
    /** 
    * @name UserGroup::_setGid()
    * @param $gid (Int)
    * @return void 
    */
     public function _setGid($gid)
    {
      $this->gid = $gid;
    }
    /** 
    * @name UserGroup::_setLibelleArray()
    * @param $key (string)
    * @param $value (string)
    * @return void 
    */
    public function _setLibelleArray($key, $value)
    {
      $this->libelleArray[$key] = $value ;
    }
    
    /** 
    * @name UserGroup::_setMainUserArray()
    * @param $value (string)
    * @return void 
    */
    public function _setMainUserArray($value)
    {
      $this->mainUserArray[] = $value ;
    }
    /** 
    * @name UserGroup::_setOtherUserArray()
    * @param $value (string)
    * @return void 
    */
    public function _setOtherUserArray($value)
    {
      $this->otherUserArray[] = $value ;
    }
      
    /**
    * Compte du nombre d'occurence d'utilisateurs dans un groupe
    * 
    * <p>_countUserInGroup</p>
    * 
    * @name UserGroup::_countUserInGroup()
    * @return int 
    */
    public function _countUserInGroup($gid)
    {
		try{
	      	$nbr = 0 ;
	      	$maconnexion = MysqlDatabase::GetInstance() ;
	      	$sql = 'SELECT count(iug.INT_COD) AS nbr ';
	      	$sql .= 'FROM t_aut as ta, appl_user_group as iug ';
	      	$sql .= 'WHERE ta.AUT_STA=\'1\' ';
	      	$sql .= 'AND ta.INT_COD=iug.INT_COD ';
	      	$sql .= 'AND iug.GID =\''.$gid.'\' ';    	
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);  // Recuperation de la requete SQL
      		$nbr  = $row['nbr'] ;
      		return $nbr;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
    }
   
    /**
    * Cr�ation d'un groupe dans la bdd
    * 
    * <p>_create</p>
    * 
    * @name UserGroup::_create()
    * @return void 
    */
    public function _create()
    {
		try{
			$maconnexion = MysqlDatabase::GetInstance() ;
			$sql='INSERT INTO appl_group( GID ) VALUES (\'\') ';
	      	$this->gid = $maconnexion->_bddQueryLastId($sql);

    		foreach($this->libelleArray as $key =>$value)
    		{
    	    	$sql = 'INSERT INTO `appl_group_libelle` (`GID`, `LANG`, `LIBELLE`) ';
	      		$sql .= 'VALUES('.$this->gid.', \''.$key.'\', \''.AddSlashes(trim($value)).'\')';
	      		$res = $maconnexion->_bddQuery($sql) ;
	      	}	      	

	      	if($this->mainUserArray != NULL)
	      	{
	      		foreach($this->mainUserArray as $key=>$value)
	      		{
					$sql = 'INSERT INTO `appl_user_group` (`GID`, `INT_COD`, `MAIN_GROUP_YN`) ';
					$sql .= 'VALUES('.$this->gid.', \''.$value.'\', 1) ';
	      			$res = $maconnexion->_bddQuery($sql) ;
	      		}
	      	}
			if($this->otherUserArray != NULL)
	      	{
	      		foreach($this->otherUserArray as $key=>$value)
	      		{
					$sql = 'INSERT INTO `appl_user_group` (`GID`, `INT_COD`, `MAIN_GROUP_YN`) ';
					$sql .= 'VALUES('.$this->gid.', \''.$value.'\', 0) ';
	      			$res = $maconnexion->_bddQuery($sql) ;
	      		}
	      	}  	
		}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}	
    }
 
    /**
    * Mise � jour d'un groupe dans la bdd
    * 
    * <p>_update</p>
    * 
    * @name UserGroup::_update()
    * @return void 
    */ 
    public function _update()
    {
		try{  
      		$maconnexion = MysqlDatabase::GetInstance() ;   
    		foreach($this->libelleArray as $key =>$value)
    		{
    			$sql = 'SELECT * FROM appl_group_libelle ';
    			$sql .= 'WHERE GID = '.$this->gid.' ';
    	    	$sql .= 'AND LANG = \''.$key.'\' ';
    	    	$res = $maconnexion->_bddQuery($sql) ;
    	    	if($maconnexion->_bddNumRows($res)>0)
	    		{ 
	    	    	$sql = 'UPDATE appl_group_libelle SET ';
	    	    	$sql .= 'LIBELLE = \''.AddSlashes(trim($value)).'\' ';
	    	    	$sql .= 'WHERE GID = '.$this->gid.' ';
	    	    	$sql .= 'AND LANG = \''.$key.'\' ';
		      		$res = $maconnexion->_bddQuery($sql) ;
	    		}else{
		    		$sql = 'INSERT INTO `appl_group_libelle` (`GID`, `LANG`, `LIBELLE`) ';
		      		$sql .= 'VALUES('.$this->gid.', \''.$key.'\', \''.AddSlashes(trim($value)).'\')';
		      		$res = $maconnexion->_bddQuery($sql) ;	    			
	    		}
	      	}
	      		      	
			$sql = 'DELETE FROM `appl_user_group` WHERE GID = '.$this->gid.'';
	      	$res = $maconnexion->_bddQuery($sql) ;
	      	$right = new ModuleRights ;
	      	if($this->mainUserArray != NULL)
	      	{
	      		foreach($this->mainUserArray as $key=>$value)
	      		{
					$sql = 'INSERT INTO `appl_user_group` (`GID`, `INT_COD`, `MAIN_GROUP_YN`) ';
					$sql .= 'VALUES('.$this->gid.', \''.$value.'\', 1) ';
	      			$res = $maconnexion->_bddQuery($sql) ;
	      			$right->_updateUserGroupRight($this->gid, $value) ;
	      		}
	      	}
			if($this->otherUserArray != NULL)
	      	{
	      		foreach($this->otherUserArray as $key=>$value)
	      		{
					$sql = 'INSERT INTO `appl_user_group` (`GID`, `INT_COD`, `MAIN_GROUP_YN`) ';
					$sql .= 'VALUES('.$this->gid.', \''.$value.'\', 0) ';
	      			$res = $maconnexion->_bddQuery($sql) ;
	      			$right->_updateUserGroupRight($this->gid, $value) ;
	      		}
	      	}  
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
    }
    
    /**
    * R�cup�ration de la liste des groupe d'utilisateurs
    * 
    * <p>_getGroupList</p>
    * 
    * @name UserGroup::_getGroupList()
    * @param $lang (String)
    * @return array
    */    
    public function _getGroupList($lang='')
    {
		try{
			$listArray = array() ;
      		$maconnexion = MysqlDatabase::GetInstance() ;
      
      		$sql  = 'SELECT * FROM appl_group_libelle ' ;
      		$sql .= 'WHERE GID > 1 ' ;
      		if($lang != '')
      		{
      			$sql .= 'AND LANG = \''.$lang.'\' ' ;
      		}
      		$sql .= 'ORDER BY LIBELLE ' ;
			$res = $maconnexion->_bddQuery($sql) ;
			while($row = $maconnexion->_bddFetchAssoc($res))
      		{
        		$listArray[$row['GID']][$row['LANG']] = StripSlashes($row['LIBELLE']) ; 
      		}
      		foreach($listArray as $key=>$value)
      		{
      			$nb=$this->_countUserInGroup($key);
      			$listArray[$key]['nb_users']=$nb ;
      		}
      		return $listArray ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
    }

     /**
    * <p>R�cup�ration de la liste de tous les utilisateurs</p>
    * 
    * @name AdminUserGroup::_getAllUser(()
    */     
    public function _getAllUser()
    {
    	try{
      		$listArray = array() ;
      		$maconnexion = MysqlDatabase::GetInstance() ;

			$sql  = 'SELECT ti.INT_COD, ti.INT_TRG, ti.INT_NOM, ti.INT_PRE ';
			$sql .= 'FROM t_int as ti, t_aut as ta  ' ;
			$sql .= 'WHERE ti.INT_COD = ta.INT_COD ' ;
			$sql .= 'AND ti.INT_COD > 1 ';
			$sql .= 'AND ta.AUT_STA=\'1\' ' ;
			$sql .= 'ORDER BY ti.INT_NOM  ' ;

			$res = $maconnexion->_bddQuery($sql) ;
    	   	while($row = $maconnexion->_bddFetchAssoc($res))
	      	{
				$listArray[$row['INT_COD']]['USER'] = StripSlashes(StripSlashes('['.$row['INT_TRG'].'] '.$row['INT_NOM'].' '.$row['INT_PRE'])) ;  	
			}
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
      	return $listArray ;    
    }
    /**
    * <p>R�cup�ration de la liste des utilisateurs d'un groupe</p>
    * 
    * @name AdminUserGroup::_getUserInGroupList()
    * @param $gid (Int)
    * @return Array
    */     
    public function _getUserInGroupList($gid)
    {
    	try{
      		$listArray = array() ;
      		$maconnexion = MysqlDatabase::GetInstance() ;

			$sql  = 'SELECT ti.INT_COD, ti.INT_TRG, ti.INT_NOM, ti.INT_PRE, ';
			$sql .= 'iug.MAIN_GROUP_YN ';
			$sql .= 'FROM t_int as ti, t_aut as ta, appl_user_group as iug  ' ;
			$sql .= 'WHERE ti.INT_COD = ta.INT_COD ' ;
			$sql .= 'AND ti.INT_COD = iug.INT_COD ';
			$sql .= 'AND ti.INT_COD > 1 ';
			$sql .= 'AND ta.AUT_STA=\'1\' ' ;
			$sql .= 'AND iug.GID=\''.$gid.'\' ' ;
			$sql .= 'ORDER BY ti.INT_NOM  ' ;

			$res = $maconnexion->_bddQuery($sql) ;
    	   	while($row = $maconnexion->_bddFetchAssoc($res))
	      	{
				$listArray[$row['INT_COD']]['USER'] = StripSlashes(StripSlashes('['.$row['INT_TRG'].'] '.$row['INT_NOM'].' '.$row['INT_PRE'])) ;  
				$listArray[$row['INT_COD']]['MAIN_YN'] = $row['MAIN_GROUP_YN'] ;	
			}
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
      	return $listArray ;    
    }

    /**
    * <p>R�cup�ration de la liste des utilisateurs sans groupe</p>
    * 
    * @name AdminUserGroup::_getUserGroupFreeList()
    * @return Array
    */     
    public function _getUserGroupFreeList($gid=0)
    {
    	try{
      		$listArray = array() ;
      		$maconnexion = MysqlDatabase::GetInstance() ;

			$sql  = 'SELECT ti.INT_COD, ti.INT_TRG, ti.INT_NOM, ti.INT_PRE ';
			$sql .= 'FROM t_int as ti, t_aut as ta  ' ;
			$sql .= 'WHERE ti.INT_COD = ta.INT_COD ' ;
			$sql .= 'AND ti.INT_COD > 1 ';
			$sql .= 'AND ta.AUT_STA=\'1\' ' ;
			$sql .= 'AND (NOT EXISTS(SELECT DISTINCT iug.INT_COD ';
			$sql .= 'FROM appl_user_group AS iug ';
			$sql .= 'WHERE ti.INT_COD = iug.INT_COD )';
			if($gid != 0)
			{
				$sql .= ' OR EXISTS ( ' ;
				$sql .= 'SELECT DISTINCT iug.INT_COD ';
				$sql .= 'FROM appl_user_group AS iug ';
				$sql .= 'WHERE ti.INT_COD = iug.INT_COD ';
				$sql .= 'AND iug.GID = '.$gid.' )';
			}
			$sql .= ') ORDER BY ti.INT_NOM  ' ;

			$res = $maconnexion->_bddQuery($sql) ;
    	   	while($row = $maconnexion->_bddFetchAssoc($res))
	      	{
				$listArray[$row['INT_COD']]['USER'] = StripSlashes('['.$row['INT_TRG'].'] '.$row['INT_NOM'].' '.$row['INT_PRE']) ; 
			}
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
      	return $listArray ;    
    }
    
     /**
    * <p>R�cup�ration de la liste des utilisateurs par groupes</p>
    * 
    * @name AdminUserGroup::_getUserByGroupList()
    * @return Array
    */     
    public function _getUserByGroupList()
    {
    	try{
      		$listArray = array() ;
      		$maconnexion = MysqlDatabase::GetInstance() ;

			$sql  = 'SELECT ti.INT_COD, ti.INT_TRG, ti.INT_NOM, ti.INT_PRE, ';
			$sql .= 'iug.MAIN_GROUP_YN ';
			$sql .= 'FROM t_int as ti, t_aut as ta, appl_user_group as iug  ' ;
			$sql .= 'WHERE ti.INT_COD = ta.INT_COD ' ;
			$sql .= 'AND ti.INT_COD = iug.INT_COD ';
			$sql .= 'AND ti.INT_COD > 1 ';
			$sql .= 'AND ta.AUT_STA=\'1\' ' ;
			$sql .= 'ORDER BY ti.INT_NOM  ' ;

			$res = $maconnexion->_bddQuery($sql) ;
    	   	while($row = $maconnexion->_bddFetchAssoc($res))
	      	{
				$listArray[$row['GID']][$row['INT_COD']]['USER'] = StripSlashes(StripSlashes('['.$row['INT_TRG'].'] '.$row['INT_NOM'].' '.$row['INT_PRE'])) ;  
				$listArray[$row['GID']][$row['INT_COD']]['MAIN_YN'] = $row['MAIN_GROUP_YN'] ;	
			}
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
      	return $listArray ;    
    }
    
    public function _delete($gid)
    {    	
    	try{
    		$module = new ModuleRights();
      		$maconnexion = MysqlDatabase::GetInstance() ;

      		$sql='DELETE FROM appl_group WHERE GID = \''.$gid.'\' ';
      		$res = $maconnexion->_bddQuery($sql) ;
	      	$sql = 'DELETE FROM appl_group_libelle WHERE GID = \''.$gid.'\' ';
	      	$res = $maconnexion->_bddQuery($sql) ;
	      	$sql = 'DELETE FROM appl_user_group WHERE GID = \''.$gid.'\' ';
	      	$res = $maconnexion->_bddQuery($sql) ;
	      	
	      	$moduleArray =$module->_loadModules() ;
	      	if($moduleArray != NULL)
	      	{
	      		foreach($moduleArray as $key=>$value)
	      		{
					$sql  = 'DELETE FROM '.$moduleArray[$key]['name'].'_right_acl ';
					$sql .= 'WHERE GID = \''.$gid.'\' ';
      				$sql .= 'AND INT_COD = 0 ' ;
      				$res = $maconnexion->_bddQuery($sql) ;
	      		}
	      	}
    	}
    	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}  	
    }
    
    /**
    * Destructeur
    * 
    * <p>Destruction de l'instance de classe</p>
    * 
    * @name Person::__destruct()
    * @return void
    */
    public function __destruct() {
    }
 } 
?>