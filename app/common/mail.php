<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * <p>Application</p>
 *
 * @name  Application
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

class MIME_Mail {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (String)
	 * @desc envoyeur
	 */
	public $from;
	/**
	 * @var (String)
	 * @desc destinataire
	 */
	public $to;
	/**
	 * @var (String)
	 * @desc sujet
	 */
	public $subject;
	/**
	 * @var (String)
	 * @desc contenu
	 */
	public $message;
	/**
	 * @var (String)
	 * @desc attached
	 */
	public $attached;
	/**
	 * @var (String)
	 * @desc copie cochée
	 */
	public $bcc;

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Application::__construct()
	 * @return void
	 */
	public function __construct($from, $to=NULL, $subject=NULL, $message=NULL, $bcc=NULL)
	{
		$this->from = $from;
		$this->to = $to;
		$this->bcc = $bcc;
		$this->subject = $subject;
		$this->message = $message;
		$this->attached = array();
	}

	public function set_from($from)
	{
		$this->from = $from;
	}

	public function set_to($to)
	{
		$this->to = $to;
	}

	public function set_bcc($bcc)
	{
		$this->bcc = $bcc;
	}

	public function set_subject($subject)
	{
		$this->subject = $subject;
	}

	public function set_message($message)
	{
		$this->message = $message;
	}

	public function attach_file($path)
	{
		switch( strrchr( $path, '.') )
		{
			case '.gz':
				$ctype = "application/x-gzip";
				break;

			case ".tgz":
				$ctype = "application/x-gzip";
				break;

			case ".zip":
				$ctype = "application/zip";
				break;

			case ".pdf":
				$ctype = "application/pdf";
				break;

			case ".png":
				$ctype = "image/png";
				break;

			case ".gif":
				$ctype = "image/gif";
				break;

			case ".jpg":
				$ctype = "image/jpeg";
				break;

			case ".txt":
				$ctype = "text/plain";
				break;

			case ".htm":
				$ctype = "text/html";
				break;

			case ".html":
				$ctype = "text/html";
				break;

			default:
				$ctype = "application/octet-stream";
				break;
		}

		$handle = fopen($path, "r");
		$code = fread($handle, filesize($path));
		$code = chunk_split(base64_encode($code));
		fclose($handle);

		$this->attached[] = array('ctype' => $ctype, 'code' => $code, 'name' => basename($path));
	}

	public function send_mail()
	{
		$boundary = md5(uniqid(time() ));

		$entete  = "From: {$this->from}\r\n";
		$entete .= "Reply-To: {$this->from}\r\n";
		if ($this->bcc!="")
		$entete .= "Bcc: {$this->bcc}\r\n";
		$entete .= "MIME-Version: 1.0\r\n";
		$entete .= "X-Mailer: PHP\r\n";
		$entete .= "Content-Type: multipart/mixed;boundary=$boundary\r\n\r\n";

		$entete_message = "--$boundary\r\n";
		$entete_message .= "Content-type:text/plain;charset=us-ascii\r\n";
		$entete_message .= "Content-transfer-encoding:8bit\r\n\r\n";
		$entete_message .= "{$this->message}\r\n";

		$entete_attached = '';

		foreach( $this->attached as $attach )
		{
			$entete_attached .= "--$boundary\r\n";
			$entete_attached .= "Content-type:{$attach['ctype']};name={$attach['name']}\r\n";
			$entete_attached .= "Content-transfer-encoding:base64\r\n\r\n";
			$entete_attached .= "{$attach['code']}\r\n";
			$entete_attached .= "--$boundary--";
		}
		//aff_pr($this);echo "$entete . $entete_message . $entete_attached";
		return mail($this->to, $this->subject,"",$entete . $entete_message . $entete_attached);
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Application::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}