<?php
/**
 * 
 * <p>HDate</p>
 * 
 * @name className
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package common
 */

class HDate {
	 /**
    * @var $instance(Singleton)
    * @desc variable pour le singleton
    */
 	private static $instance;
 	
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name HDate::__construct()
    * @return void 
    */
    public function __construct() {
    	$this->_getDateParam();
    } 
    /**
    * Singleton
    * 
    * <p>cr�ation de l'instance de la classe si n'existe pas</p>
    * 
    * @name HDate::_GetInstance()
    * @return HDate
    */
	public static function _GetInstance()
	{
		if(!isset(self::$instance))
		{
			self::$instance = new Hdate();
		}
		return self::$instance;
	} 
    public function _getDateParam()
    {
    	$session = Session::_GetInstance() ;
    }

    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /*   m�thodes publiques    */
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Transforme un timestamp en cha�ne de caract�re en fonction du format li� � la langue
    * dd/mm/YYYY pour le format fran�ais
    * YYYY-mm-dd pour le format anglais
    * 
    * <p>_dateStampLgFormat</p>
    * 
    * @name HDate::_dateStampLgFormat()
    * @param $dateStamp (Int)
    * @param $lg (String)
    * @return string 
    */
    public function _dateStampLgFormat($dateStamp, $lg)
    {
    	if($lg == 'fr')
    	{
    		$format = 'd/m/Y' ;
    	}else{
    		$format = 'Y-m-d' ;
    	}
    	return date($format, $dateStamp) ;
    }

    /**
    * Transforme une cha�ne de caract�re au format  de date et heure support� par MySQL
    * YYYY-mm-dd h:i:s
    * 
    * <p>_dateHourToMySQLFormat</p>
    * 
    * @name HDate::_dateHourToMySQLFormat()
    * @param $dateHourStr (String)
    * @return string 
    */
    public function _dateHourToMySQLFormat($dateHourStr)
    {
        if(strpos($dateStr, '/'))
    	{
    		$dateStr = str_replace('/', '-', $dateStr) ;
    	}
        if(strpos($dateStr, ' '))
    	{
    		$dateStr = str_replace(' ', '-', $dateStr) ;
    	}
        	if(strpos($dateStr, ':'))
    	{
    		$dateStr = str_replace(':', '-', $dateStr) ;
    	}  	
    	$dateArray = explode ('-', $dateStr) ;
    	
    	if(strlen($dateArray[0]) == 4)
    	{
    		$dMySQL = $dateArray[0].'-'. $dateArray[1].'-'.$dateArray[2].' '.$dateArray[3].':'.$dateArray[4].':'.$dateArray[5] ;
    	}else{
    		$dMySQL = $dateArray[2].'-'. $dateArray[1].'-'.$dateArray[0].' '.$dateArray[3].':'.$dateArray[4].':'.$dateArray[5] ;    			
    	}    		
    	return $dMySQL ;	
    }

    /**
    * Transforme une cha�ne de caract�re au format  de date support� par MySQL
    * YYYY-mm-dd
    * 
    * <p>_dateToMySQLFormat</p>
    * 
    * @name HDate::_dateToMySQLFormat()
    * @param $dateStr (String)
    * @return string 
    */
    public function _dateToMySQLFormat($dateStr)
    {
        if(strpos($dateStr, '/'))
    	{
    		$dateStr = str_replace('/', '-', $dateStr) ;
    	}
        if(strpos($dateStr, ' '))
    	{
    		$dateStr = str_replace(' ', '-', $dateStr) ;
    	}
        	if(strpos($dateStr, ':'))
    	{
    		$dateStr = str_replace(':', '-', $dateStr) ;
    	}  	
    	$dateArray = explode ('-', $dateStr) ;
    	if(strlen($dateArray[0]) == 4)
    	{
    		$dMySQL = $dateArray[0].'-'. $dateArray[1].'-'.$dateArray[2]  ;
    	}else{
    		$dMySQL = $dateArray[2].'-'. $dateArray[1].'-'.$dateArray[0]  ;    			
    	}  		
    	return $dMySQL ;	
    }
    /**
    * V�rification que la date existe
    * 
    * <p>_verifDateStr</p>
    * 
    * @name HDate::_verifDateStr()
    * @param $dateStr (String)
    * @return boolean
    */     
    function _verifDateStr($dateStr)
    {
    	$verDate = $this->_dateToEnFormat($dateStr) ;
    	$dateArray = explode ('-', $verDate) ;    	
    	if(! checkDate($dateArray[1], $dateArray[2], $dateArray[0] ) )
      	{
      		return FALSE ;
      	}else{
      		return TRUE ;
      	}
    }
    /**
    * Transforme une cha�ne de caract�re au format  de date fran�ais
    * dd/mm/YYYY
    * 
    * <p>_dateToFrFormat</p>
    * 
    * @name HDate::_dateToFrFormat()
    * @param $dateStr (String)
    * @return string 
    */    
    public function _dateToFrFormat($dateStr)
    {
        if(strpos($dateStr, '/'))
    	{
    		$dateStr = str_replace('/', '-', $dateStr) ;
    	}
        if(strpos($dateStr, ' '))
    	{
    		$dateStr = str_replace(' ', '-', $dateStr) ;
    	}
        	if(strpos($dateStr, ':'))
    	{
    		$dateStr = str_replace(':', '-', $dateStr) ;
    	}  	
    	$dateArray = explode ('-', $dateStr) ;
    	if(strlen($dateArray[0]) == 4)
    	{
			$dfr = $dateArray[2].'/'. $dateArray[1].'/'.$dateArray[0]  ;
    	}else{
    		$dfr = $dateArray[0].'/'. $dateArray[1].'/'.$dateArray[2]  ;	
    	}  		
    	return $dfr ;	
    }
    
    /**
    * Transforme une cha�ne de caract�re au format  de date anglais
    * YYYY-mm-dd
    * 
    * <p>_dateToEnFormat</p>
    * 
    * @name HDate::_dateToEnFormat()
    * @param $dateStr (String)
    * @return string 
    */  
    public function _dateToEnFormat($dateStr)
    {
        if(strpos($dateStr, '/'))
    	{
    		$dateStr = str_replace('/', '-', $dateStr) ;
    	}
        if(strpos($dateStr, ' '))
    	{
    		$dateStr = str_replace(' ', '-', $dateStr) ;
    	}
        	if(strpos($dateStr, ':'))
    	{
    		$dateStr = str_replace(':', '-', $dateStr) ;
    	}  	
    	$dateArray = explode ('-', $dateStr) ;
    	if(strlen($dateArray[0]) == 4)
    	{
			$den = $dateArray[0].'-'. $dateArray[1].'-'.$dateArray[2]  ;		
    	}else{
    		$den = $dateArray[2].'-'. $dateArray[1].'-'.$dateArray[0]  ;
    	}  		
    	return $den ;	
    }
    /**
    * Transforme une cha�ne de caract�re en timestamp
    * Attention : les timestamp ne sont transform�s qu � partir de 01/01/1970
    * 
    * <p>_dateStrToStamp</p>
    * 
    * @name HDate::_dateStrToStamp()
    * @param $dateStr (String)
    * @return int 
    */ 
    public function _dateStrToStamp($dateStr)
    {
    	if(strpos($dateStr, '/'))
    	{
    		$dateStr = str_replace('/', '-', $dateStr) ;
    	}
        if(strpos($dateStr, ' '))
    	{
    		$dateStr = str_replace(' ', '-', $dateStr) ;
    	}
        	if(strpos($dateStr, ':'))
    	{
    		$dateStr = str_replace(':', '-', $dateStr) ;
    	}  	
    	$dateArray = explode ('-', $dateStr) ;
    	if(sizeof($dateArray) == 3)
    	{
    		if(strlen($dateArray[0]) == 4)
    		{
    			$dstamp = mktime(0, 0, 0, $dateArray[1], $dateArray[2], $dateArray[0]) ;
    		}else{
    			$dstamp = mktime(0, 0, 0, $dateArray[1], $dateArray[0], $dateArray[2]) ;    			
    		}
    	}else{
    	    if(strlen($dateArray[0]) == 4)
    		{
    			$dstamp = mktime($dateArray[3], $dateArray[4], $dateArray[5], $dateArray[1], $dateArray[2], $dateArray[0]) ;
    		}else{
    			$dstamp = mktime($dateArray[3], $dateArray[4], $dateArray[5], $dateArray[1], $dateArray[0], $dateArray[2]) ;    			
    		}    		
    	}
    	return $dstamp ;
    }

    /**
    * Compte le nombre de jours ouvr�s entre deux date
    * Attention : les timestamp ne sont transform�s qu � partir de 01/01/1970
    * 
    * <p>_countWorkedDays</p>
    * 
    * @name HDate::_countWorkedDays()
    * @param $datefrom (int)
    * @param $dateto (int)
    * @return int 
    */ 
   function _countWorkedDays($datefrom, $dateto)
   {
      $nbdays = 0 ;
      if($this->nbrJrTravAn != 0)
      {
      	$ratio = 365/$this->nbrJrTravAn ;
      }else{
      	$ratio = 1 ;
      }
      if($datefrom == $dateto)
      {
         $nbdays = 1 ;
      }else{
      	$nbyear = date("Y", $dateto) - date("Y", $datefrom) ;
      	if($nbyear <= 2)
      	{
	      	 while($datefrom <= $dateto)
	         {
	            $nbdays++ ;
	            $datefrom = mktime(0,0,0, date("m", $datefrom), date("d", $datefrom)+1, date("Y", $datefrom) ) ;
	         }
	         $nbdays = round($nbdays/$ratio) ;
      		
      	}else{
      	    $nextyear = mktime(0,0,0, 12, 31, date("Y", $datefrom));
	        while($datefrom <= $nextyear)
	        {
	           $nbdays++ ;
	           $datefrom = mktime(0,0,0, date("m", $datefrom), date("d", $datefrom)+1, date("Y", $datefrom) ) ;
	        }
	        $datefrom = mktime(0,0,0, 1, 31, date("Y", $dateto));
      		 while($datefrom <= $dateto)
	         {
	            $nbdays++ ;
	            $datefrom = mktime(0,0,0, date("m", $datefrom), date("d", $datefrom)+1, date("Y", $datefrom) ) ;
	         }
	         $nbdays = round($nbdays/$ratio) +($this->nbrJrTravAn*($nbyear-2) );
      	}
      }
      return $nbdays ;
   }
   
    /**
    * Compte le nombre d'ann�es entre deux date
    * Attention : les timestamp ne sont transform�s qu � partir de 01/01/1970
    * 
    * <p>_countYears</p>
    * 
    * @name HDate::_countYears()
    * @param $datefrom (int)
    * @param $dateto (int)
    * @return int 
    */ 
   function _countYears($datefrom, $dateto)
   {
      $nbyears = 0 ;
      if(date("Y", $dateto) == date("Y", $datefrom) )
      {
         $nbyears = 1 ;
      }else{
      	$nbmonth = $this->_countMonths($datefrom, $dateto) ;
      	$nbyears = date("Y", $dateto) - date("Y", $datefrom) ;
      	if($nbmonth > $nbyears*12)
      	{
      		$nbyears++;
      	}
      }
      return $nbyears ;
   }

    /**
    * Compte le nombre de mois entre deux date
    * Attention : les timestamp ne sont transform�s qu � partir de 01/01/1970
    * 
    * <p>_countYears</p>
    * 
    * @name HDate::_countMonths()
    * @param $datefrom (int)
    * @param $dateto (int)
    * @return int 
    */ 
   function _countMonths($datefrom, $dateto)
   {
      $months = 0 ;
      $nbyear = date("Y", $dateto) - date("Y", $datefrom) ;
      if($nbyear <= 2)
      {
		while($datefrom <= $dateto)
	    {
			$months++ ;
	        $datefrom = mktime(0,0,0, date("m", $datefrom)+1, date("d", $datefrom), date("Y", $datefrom) ) ;
	    }
      		
      }else{
      	$nextyear = mktime(0,0,0, 12, 31, date("Y", $datefrom));
	    while($datefrom <= $nextyear)
	    {
	    	$months++ ;
	        $datefrom = mktime(0,0,0, date("m", $datefrom)+1, date("d", $datefrom), date("Y", $datefrom) ) ;
	    }
	    $datefrom = mktime(0,0,0, 1, 31, date("Y", $dateto));
      	while($datefrom <= $dateto)
	    {
	    	$months++ ;
	        $datefrom = mktime(0,0,0, date("m", $datefrom)+1, date("d", $datefrom), date("Y", $datefrom) ) ;
	    }
      }
      return $months ;
   }

    /**
    * Compte le nombre de jours entre deux date
    * Attention : les timestamp ne sont transform�s qu � partir de 01/01/1970
    * 
    * <p>_countDays</p>
    * 
    * @name HDate::_countDays()
    * @param $datefrom (int)
    * @param $dateto (int)
    * @return int 
    */ 
   function _countDays($datefrom, $dateto)
   {
      $days = 0 ;
      if($datefrom==$dateto)
      {
      	$days = 1 ;
      }else{
	      while($datefrom <= $dateto)
		  {
			$days++ ;
		    $datefrom = mktime(0,0,0, date("m", $datefrom), date("d", $datefrom)+1, date("Y", $datefrom) ) ;
		  }
      }
      return $days ;
   }

    /**
    * Conversion de cha�ne de caract�re DMRF type jj:hh:mn:ss en secondes
    * Utilis�e dans la classe Traitement
    * 
    * <p>_dmrf2sec</p>
    * 
    * @name HDate::_dmrf2sec()
    * @param $$dmrf (string)
    * @return int 
    */ 
	public function _dmrf2sec($dmrf)
	{
		$dmrfnum=0;
		if ((substr($dmrf,2,1)==":")&&(substr($dmrf,5,1)==":")&&(substr($dmrf,8,1)==":"))
		{
			list($jour, $heure, $minute, $seconde)=explode(":", $dmrf);
			$dmrfnum=$seconde+60*$minute+3600*$heure+86400*$jour;
		}else{
			$dmrfnum=0;
		}
		return $dmrfnum;
	}
	
    /**
    * Conversion de seconde en cha�ne de caract�re DMRF type jj:hh:mn:ss
    * Utilis�e dans la classe Traitement
    * 
    * <p>_sec2dmrf</p>
    * 
    * @name HDate::_sec2dmrf()
    * @param $sec(int)
    * @return string 
    */ 
	public function _sec2dmrf($sec)
	{
		$dmrf=0;
		$jour=intval(abs($sec/86400));
		$sec=$sec - ($jour*86400);
		$heure=intval(abs($sec/3600));
		$sec=$sec - ($heure*3600);
		$minute=intval(abs($sec/60));
		$seconde=$sec - ($minute*60);
	
		$ar=array(sprintf("%02s",$jour),sprintf("%02s",$heure), sprintf("%02s",$minute), sprintf("%02s",$seconde));
		$dmrf=implode(":",$ar);
		return $dmrf;
	}

    /**
    * Conversion de cha�ne de caract�re DMRF type jj:hh:mn:ss en cha�ne de caract�re DMRF type 0 j 0 h 0 mn 0 s
    * Utilis�e dans la classe Traitement
    * 
    * <p>_dmrf2lib</p>
    * 
    * @name HDate::_dmrf2lib()
    * @param $dmrf(string)
    * @param $niv (string)
    * @return string 
    */ 
	function _dmrf2lib($dmrf,$niv)
	{
		$dmrflib="";
	
		$pos=strpos($dmrf,":");
		if (($dmrf=="") or ($pos==0) or (substr($dmrf,$pos+3,1)<>":")) {return $dmrf;}
	
		list($jour, $heure, $minute, $seconde)=explode(":", $dmrf);
	
		if ($jour>0) {$dmrflib .="$jour j ";}
		if (($niv=="D") or ($dmrflib==" "))
		{ if ($heure>0) {$dmrflib .="$heure h ";}}
		if (($niv=="D") or ($dmrflib==" "))
		{ if ($minute>0) {$dmrflib .="$minute mn ";}}
		if (($niv=="D") or ($dmrflib==" "))
		{ if ($seconde>0) {$dmrflib .="$seconde s ";}}
	
		if (substr($dmrflib,0,2)=="99") $dmrflib="99 j";
		return $dmrflib;
	}
	
	
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /*  2.1 m�thodes publiques */
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Destructeur
    * 
    * <p>Destruction de l'instance de classe</p>
    * 
    * @name HDate::__destruct()
    * @return void
    */
    public function __destruct() {
    
    }
 }
?>