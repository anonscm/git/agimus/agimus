<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/affiliation.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/affilIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/structure.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/application.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/srvIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/catSrv.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/dcplIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/discipline.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/diplomeIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/diplome.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/domEtuIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/domEtu.php') ;
//v1.5-PPR-#016 ajout nouvelles dimensions
require_once( DIR_WWW.ROOT_APPL.'/app/common/compIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/composante.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/regInscIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/regInsc.php') ;
/**
 *
 * <p>Dimension</p>
 *
 * @name  Dimension
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

class Dimension {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc Identifiant
	 */
	private $id;

	/**
	 * @var (String)
	 * @desc Libell� de la dimension
	 */
	private $lib;

	/**
	 * @var (String)
	 * @desc R�f�rence (objet) de la dimension
	 */
	private $refDim;

	/**
	 * @var (String)
	 * @desc id dimension dans l'agregat
	 */
	private $dimIdLib;

	

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Dimension::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		$this->id=0;
		$this->lib='';
		$this->refDim='';
		$this->dimIdLib='';
		

		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}

	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name Dimension::_getId()
	 * @return Int
	 */
	public function _getId() {
		return $this->id ;
	}

	/**
	 * @name Dimension::_getLib()
	 * @return String
	 */
	public function _getLib() {
		return $this->lib ;
	}

	/**
	 * @name Dimension::_getRefDim()
	 * @return String
	 */
	public function _getRefDim() {
		return $this->refDim ;
	}

	/**
	 * @name Dimension::_getDimIdLib()
	 * @return String
	 */
	public function _getDimIdLib() {
		return $this->dimIdLib ;
	}
	
	/**
	 * Accesseurs en �criture
	 */

	/**
	 * @name Dimension::_setId()
	 * @param $id (Int)
	 * @return void
	 */
	public function _setId($id) {
		$this->id  = $id ;
	}

	/**
	 * @name Dimension::_setLib()
	 * @param $lib (String)
	 * @return void
	 */
	public function _setLib($lib) {
		$this->lib  = $lib ;
	}

	/**
	 * @name Dimension::_setRefDim()
	 * @param $refDim (String)
	 * @return void
	 */
	public function _setRefDim($refDim) {
		$this->refDim  = $refDim ;
	}

	/**
	 * @name Dimension::_setDimIdLib()
	 * @param $dimIdLib (String)
	 * @return void
	 */
	public function _setDimIdLib($dimIdLib) {
		$this->dimIdLib  = $dimIdLib ;
	}
	

	/**
	 * Compte du nombre d'occurence de l'objet Dimension dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name Dimension::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_dim ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * renvoit l'id d'un stamp existant
	 *
	 * <p>_existsRef</p>
	 *
	 * @name Dimension::_existsRef()
	 * @param $lib string
	 * @return int
	 */
	public function _existsRef($lib)
	{
		$idRef = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT id ';
		$sql .= ' FROM t_dim ';
		$sql .= ' WHERE UPPER(lib) = \''.strtoupper($lib).'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$idRef  = $row['id'] ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		return $idRef;
	}

	/**
	 * Cr�ation d'un Dimension dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name Dimension::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM t_dim ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
			
		$sql  = 'INSERT INTO t_dim VALUES( ';
		$sql .= '\''.AddSlashes($this->id).'\', ';
		$sql .= '\''.AddSlashes($this->lib).'\', ';
		$sql .= '\''.AddSlashes($this->refDim).'\', ';
		$sql .= '\''.AddSlashes($this->dimIdLib).'\' ';
		
		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise � jour d'un Dimension dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name Dimension::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_dim SET ';
		$sql .= 'ID = \''.AddSlashes($this->id).'\', ';
		$sql .= 'LIB = \''.AddSlashes($this->lib).'\', ';
		$sql .= 'REF_DIM = \''.$this->refDim.'\', ';
		$sql .= 'DIM_ID_LIB = \''.$this->dimIdLib.'\' ';
		
		$sql .= 'WHERE id=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un Dimension dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name Dimension::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'DELETE FROM t_dim ';
		$sql .= 'WHERE id = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	
	/**
	 * Collecte de donn�es d'une dimension donn�e
	 *
	 * <p>_getDimensionValues</p>
	 *
	 * @name Dimension::_getDimensionValues()
	 * @param $refDim (int)
	 * @return array
	 */
	public function _getDimensionValues($refDim) {
		if (!$refDim>0) return array();

		$newDim=new Dimension($refDim);
		$objDim=$newDim->_getRefDim();
		
		//v1.3-PPR-24052011 correctif
		//eval('$newDimRech=new '.$objDim.'('.$value['ID_DIM'].');');
		eval('$newDimRech=new '.$objDim.'();');
		if (!isset($newDimRech))
		{
			throw new MsgException('_UNKNOWN_DIMENSION_')  ;
		}
		$arDummy=$newDimRech->_getList();
		return $newDimRech->_getComboList();
	}
	
	/**
	 * R�cup�ration de la liste des Dimensions
	 *
	 * <p>Liste des Dimension</p>
	 *
	 * @name Dimension::_getList()
	 * @return array
	 */
	public function _getList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_dim ' ;
		if ($this->id>0)
		$sql.= ' WHERE id = \''.$this->id.'\' ';
		$sql .= 'ORDER BY LIB ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['ID'] = StripSlashes($row['ID']) ;
				$listArray[$row['ID']]['LIB'] = StripSlashes($row['LIB']) ;
				$listArray[$row['ID']]['REF_DIM'] = StripSlashes($row['REF_DIM']) ;
				$listArray[$row['ID']]['DIM_ID_LIB'] = StripSlashes($row['DIM_ID_LIB']) ;
				
			}
		}
		return $listArray ;
	}

	/**
	 * R�cup�ration de la liste des objets
	 * pour alimenter des listes de s�lection
	 *
	 * <p>getComboList</p>
	 *
	 * @name Structure::_getComboList()
	 * @return array
	 */
	public function _getComboList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT ID, LIB FROM t_dim ' ;
		$sql  .= 'ORDER BY LIB ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		while($row = $maconnexion->_bddFetchAssoc($res))
		{
			$listArray[$row['ID']]= StripSlashes($row['LIB']) ;
		}
		return $listArray ;
	}

	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name Dimension::_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id==0) return false;

		$listArray=$this->_getList();
		$this->id = $listArray[$this->id]['ID'];
		$this->lib = $listArray[$this->id]['LIB'];
		$this->refDim = $listArray[$this->id]['REF_DIM'];
		$this->dimIdLib = $listArray[$this->id]['DIM_ID_LIB'];
		
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Dimension::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>