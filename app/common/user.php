<?php
  require_once( 'person.php') ;
  require_once( 'moduleRights.php') ;
  require_once('msgException.php') ;
/**
 * 
 * <p>User</p>
 * 
 * @name User
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright Capella Conseil 2009
 * @version 1.0.0
 * @package common
 */
 
 class User extends Person {
 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/

    /**
    * @var (String)
    * @desc statut de l'utilisateur : activ� ou d�sactiv� */
    private $statut;
    /**
    * @var $otherGroupArray (Array)
    * @desc Tableau de(s) identifiant(s) du/des groupe(s) secondaire(s) de l'utilisateur */    
    private $otherGroupArray ;
    
    /**
    * @var $mainGroup (Int)
    * @desc Identifiant du groupe principal de l'utilisateur */   
    private $mainGroup ;
    
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name Person::__construct()
    * @return void 
    */
    public function __construct() {
    	parent::__construct() ;
    	$this->login = '' ;
		$this->pwd = '' ;
		$this->statut = '0' ;
		$this->mainGroup = 0 ;
		$this->otherGroupArray = array() ;
    } 

    /**
    * Accesseurs en lecture
    */ 
    /** 
    * @name User::_getLogin()
    * @return String 
    */
    public function _getLogin()
    {
      return $this->login ;
    }
    /** 
    * @name User::_getPwd()
    * @return string 
    */
    public function _getPwd()
    {
      return $this->pwd ;
    }
      
    /** 
    * @name User::_getStatut()
    * @return string 
    */
    public function _getStatut()
    {
      return $this->statut  ; 
    }
     /** 
    * @name User::_getMainGroup()
    * @return string 
    */
    public function _getMainGroup()
    {
      return $this->mainGroup ; 
    }
    
    /** 
    * @name User::_getOtherGroupArray()
    * @return string 
    */
    public function _getOtherGroupArray()
    {
      return $this->otherGroupArray; 
    }
    /**
    * Accesseurs en �criture
    */ 
    /** 
    * @name User::_setLogin()
    * @param $login (String)
    * @return void 
    */
     public function _setLogin($login)
    {
      $this->login  = $login;
    }
    /** 
    * @name Person::_setPwd()
    * @param $pwd (string)
    * @return void 
    */
    public function _setPwd($pwd)
    {
      $this->pwd = $pwd ;
    }
      
    /** 
    * @name User::_setStatut()
    * @param $statut (string)
    * @return void 
    */
    public function _setStatut($statut)
    {
      $this->statut = $statut  ; 
    }
      /** 
    * @name User::_setMainGroup()
    * @return void 
    */
    public function _setMainGroup($main)
    {
      $this->mainGroup = $main ; 
    }
    
    /** 
    * @name User::_setOtherGroupArray()
    * @return void 
    */
    public function _setOtherGroupArray($otherArray)
    {
      $this->otherGroupArray = $otherArray; 
    }


    /**
    * Compte du nombre d'occurence d'utilisateurs dans la bdd
    * 
    * <p>_countUserRef</p>
    * 
    * @name User::_countUserRef()
    * @return int 
    */
    public function _countUserRef()
    {
      $nbr = 0 ;
      $maconnexion = MysqlDatabase::GetInstance() ;
      $sql = 'SELECT count(INT_COD) AS nbr ';
      $sql .= 'FROM t_aut ';
      $sql .= 'WHERE AUT_STA=\'1\' ';
      $sql .= 'AND INT_COD > 1 ';
    	try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
      $row = $maconnexion->_bddFetchAssoc($res);  // Recuperation de la requete SQL
      $nbr  = $row['nbr'] ;
      return $nbr;
    }
   
    /**
    * Cr�ation d'un intervenant dans la bdd
    * 
    * <p>_create</p>
    * 
    * @name User::_create()
    * @return void 
    */
    public function _create()
    {
    	$this->userYN = 1;
      	parent::_create() ;
      	$maconnexion = MysqlDatabase::GetInstance() ;
      	$right = new ModuleRights ;
      	if($this->mainGroup != 0)
	    {
	      	$sql = 'INSERT INTO `appl_user_group` (`GID`, `INT_COD`, `MAIN_GROUP_YN`) ';
			$sql .= 'VALUES('.$this->mainGroup.', \''.$this->intCod.'\', 1) ';
	      	$res = $maconnexion->_bddQuery($sql) ;
	      	
	      	$right->_createUserGroupRight($this->mainGroup, $this->intCod) ;
	    }
		if($this->otherGroupArray != NULL)
	    {
	      	foreach($this->otherGroupArray as $key=>$value)
	      	{
				$sql = 'INSERT INTO `appl_user_group` (`GID`, `INT_COD`, `MAIN_GROUP_YN`) ';
				$sql .= 'VALUES('.$value.', \''.$this->intCod.'\', 0) ';
	      		$res = $maconnexion->_bddQuery($sql) ;

	      		$right->_createUserGroupRight($value, $this->intCod) ;
	      	}
	    } 
    }
 
    /**
    * Mise � jour d'un intervenant dans la bdd
    * 
    * <p>_update</p>
    * 
    * @name User::_update()
    * @return void 
    */ 
    public function _update()
    {
      $maconnexion = MysqlDatabase::GetInstance() ; 
      try{     
	      	$sql1  = 'UPDATE t_int SET ';
	      	$sql1 .= 'INT_TRG = \''.$this->intTrg.'\', ';       
	      	$sql1 .= 'INT_NOM = \''.AddSlashes($this->nom).'\', ';
	      	$sql1 .= 'INT_PRE = \''.AddSlashes($this->prenom).'\', ';
	      	$sql1 .= 'INT_EMA = \''.$this->email.'\' ';
	      	$sql1 .= 'WHERE INT_COD = \''.$this->intCod.'\' ';
	      	$res1 = $maconnexion->_bddQuery($sql1) ;
	      	
	      	$sql2  = 'SELECT * FROM t_aut ';
	      	$sql2 .= 'WHERE INT_COD = \''.$this->intCod.'\' ';
	      	$res2 = $maconnexion->_bddQuery($sql2) ;
	      	if($maconnexion->_bddNumRows($res2) >0)
	      	{
	      		//maj t_aut
	      		$sql2 = 'UPDATE t_aut SET ';
				$sql2 .= 'AUT_USER_LOGIN = \''.AddSlashes($this->login).'\' ';
				if($this->pwd != '')
				{
					$sql2 .= ', AUT_PWD = \''.md5($this->pwd).'\', '; 
					$sql2 .= 'DATE_PWD = \''.date('Y-m-d').'\' '; 
				}
				$sql2 .= 'WHERE INT_COD = \''.$this->intCod.'\' ';
	      	}else{
	      		$sql2 = 'INSERT INTO t_aut SET ';
	      		$sql2 .= 'INT_COD = \''.$this->intCod.'\', ';
				$sql2 .= 'AUT_USER_LOGIN = \''.AddSlashes($this->login).'\', ';
				$sql2 .= 'AUT_PWD = \''.md5($this->pwd).'\', '; 
				$sql2 .= 'DATE_PWD = \''.date('Y-m-d').'\', ';
				$sql2 .= 'AUT_STA= \'1\' ';
	      	}
	    	$res2 = $maconnexion->_bddQuery($sql2) ;
	    	
	    	if($this->pwd != '')
			{
				$sql3 = 'INSERT INTO user_pwd_history SET ' ; 
				$sql3 .= 'INT_COD = \''.$this->intCod.'\', ';
	    		$sql3 .= 'AUT_PWD = \''.md5($this->pwd).'\', ';
	    		$sql3 .= 'DATE_PWD = \''.date('Y-m-d').'\' ';
	    		$res3 = $maconnexion->_bddQuery($sql3) ;
			}
			
			$sql = 'DELETE FROM `appl_user_group` WHERE INT_COD = '.$this->intCod.'';
	      	$res = $maconnexion->_bddQuery($sql) ;
	      	$right = new ModuleRights ;
	      	if($this->mainGroup != 0)
	      	{
	      		
	      		$sql = 'INSERT INTO `appl_user_group` (`GID`, `INT_COD`, `MAIN_GROUP_YN`) ';
				$sql .= 'VALUES('.$this->mainGroup.', \''.$this->intCod.'\', 1) ';
	      		$res = $maconnexion->_bddQuery($sql) ;
	      		
	      		$right->_updateUserGroupRight($this->mainGroup, $this->intCod) ;
	      	}
			if($this->otherGroupArray != NULL)
	      	{
	      		foreach($this->otherGroupArray as $key=>$value)
	      		{
					$sql = 'INSERT INTO `appl_user_group` (`GID`, `INT_COD`, `MAIN_GROUP_YN`) ';
					$sql .= 'VALUES('.$value.', \''.$this->intCod.'\', 0) ';
	      			$res = $maconnexion->_bddQuery($sql) ;	
	      			
					$right->_updateUserGroupRight($value, $this->intCod) ;
	      		}
	      	} 
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
    }
    
    /**
    * <p>R�cup�ration de la liste des utilisateeurs et des intervenants</p>
    * 
    * @name AdminUser::_getUserList()
    * @param $list (String)
    * @param $lang (String)
    * @return Array
    */     
    public function _getUserList($list='1', $lang='fr')
    {
    	try{
      		$listArray = array() ;
      		$maconnexion = MysqlDatabase::GetInstance() ;
			if($list=='inter')
			{
				$sql  = 'SELECT * FROM t_int as ti ' ;
			    $sql .= 'WHERE ti.INT_COD > 1 ' ;     
			    $sql .= 'AND ti.IND_SUPP = \'\' ' ;
			    $sql .= 'AND NOT EXISTS(SELECT * FROM t_aut as ta ' ;
				$sql .= 'WHERE ta.INT_COD = ti.INT_COD) ' ;
			    $sql .= 'ORDER BY ti.INT_NOM ' ;
			    try{
					$res = $maconnexion->_bddQuery($sql) ;
		      	}
		      	catch(MsgException $e){
		      		$msgString = $e ->_getError();
		      		throw new MsgException($msgString, 'database') ;
		      	}
    	   		while($row = $maconnexion->_bddFetchAssoc($res))
	      		{
	      			$othergroup = array() ;
			        $listArray[$row['INT_COD']]['INT_NOM'] = StripSlashes($row['INT_NOM']) ; 
			        $listArray[$row['INT_COD']]['INT_PRE'] = StripSlashes($row['INT_PRE']) ; 
			        $listArray[$row['INT_COD']]['INT_TRG'] = StripSlashes($row['INT_TRG']) ;  
			        $listArray[$row['INT_COD']]['INT_EMA'] = StripSlashes($row['INT_EMA']) ;			        
			        $listArray[$row['INT_COD']]['DATE_PWD'] = '-' ;
			        $listArray[$row['INT_COD']]['OTHER_GROUP'] = $othergroup ;
		      	}
			}else{
				$sql  = 'SELECT ti.INT_COD, ti.INT_TRG, ti.INT_NOM, ti.INT_PRE, ti.INT_EMA, ';
			    $sql .= 'ta.AUT_USER_LOGIN, ta.DATE_PWD, ta.AUT_PWD ';
			    $sql .= 'FROM t_int as ti, t_aut as ta ' ;
			    $sql .= 'WHERE ti.INT_COD = ta.INT_COD ' ;
			    $sql .= 'AND ti.INT_COD > 1 ';
				$sql .= 'AND ta.AUT_STA=\''.$list.'\' ' ;
			    $sql .= 'ORDER BY ti.INT_NOM  ' ;
    	
				try{
					$res = $maconnexion->_bddQuery($sql) ;
		      	}
		      	catch(MsgException $e){
		      		$msgString = $e ->_getError();
		      		throw new MsgException($msgString, 'database') ;
		      	}
    	   		while($row = $maconnexion->_bddFetchAssoc($res))
	      		{
			        $listArray[$row['INT_COD']]['INT_NOM'] = StripSlashes($row['INT_NOM']) ; 
			        $listArray[$row['INT_COD']]['INT_PRE'] = StripSlashes($row['INT_PRE']) ; 
			        $listArray[$row['INT_COD']]['INT_TRG'] = StripSlashes($row['INT_TRG']) ;  
			        $listArray[$row['INT_COD']]['INT_EMA'] = StripSlashes($row['INT_EMA']) ;
			        $listArray[$row['INT_COD']]['AUT_USER_LOGIN'] = StripSlashes($row['AUT_USER_LOGIN']) ;
			        $listArray[$row['INT_COD']]['DATE_PWD'] = $row['DATE_PWD'] ;
			        if($row['DATE_PWD'] == '0000-00-00')
			        {
			        	$listArray[$row['INT_COD']]['AUT_NEW'] = TRUE ;
			        }else{
			        	$listArray[$row['INT_COD']]['AUT_NEW'] = FALSE ;
			        }
					$maingroup = 0 ;
					$othergroup = array() ;
			        $sql1 = 'SELECT igl.GID, igl.LIBELLE, iug.MAIN_GROUP_YN ';
			        $sql1 .= 'FROM appl_group_libelle as igl, appl_user_group as iug ';
    				$sql1 .= 'WHERE iug.INT_COD = '.$row['INT_COD'].' ';
    	    		$sql1 .= 'AND LANG = \''.$lang.'\' ';
    	    		$sql1 .= 'AND igl.GID = iug.GID ';
	      			try{
					$res1 = $maconnexion->_bddQuery($sql1) ;
		      		}
		      		catch(MsgException $e){
		      			$msgString = $e ->_getError();
		      			throw new MsgException($msgString, 'database') ;
		      		}
					while($row1 = $maconnexion->_bddFetchAssoc($res1) )
					{
						if($row1['MAIN_GROUP_YN'] == 1)
						{
							$listArray[$row['INT_COD']]['MAIN_GROUP'] = $row1['GID'];
							$listArray[$row['INT_COD']]['GROUPE'] = StripSlashes($row1['LIBELLE']) ;
						}else{
							$othergroup[$row1['GID']] = $row1['GID'] ;
						}
					}
					$listArray[$row['INT_COD']]['OTHER_GROUP'] = $othergroup ;
		      	}
			}
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
      	return $listArray ;    
    }

     /**
    * <p>R�cup�ration de la liste des utilisateurs et des droits</p>
    * 
    * @name AdminUser::_getUserRightList()
    * @param $lang (String)
    * @return Array
    */     
    public function _getUserRightList($lang='fr')
    {
    	try{
      		$listArray = array() ;
      		$maconnexion = MysqlDatabase::GetInstance() ;

			$sql  = 'SELECT ti.INT_COD, ti.INT_TRG, ti.INT_NOM, ti.INT_PRE ';
			$sql .= 'FROM t_int as ti, t_aut as ta ' ;
			$sql .= 'WHERE ti.INT_COD = ta.INT_COD ' ;
			$sql .= 'AND ti.INT_COD > 1 ';
			$sql .= 'AND ta.AUT_STA=\'1\' ' ;
			$sql .= 'ORDER BY ti.INT_NOM  ' ;
			$res = $maconnexion->_bddQuery($sql) ;

    	   	while($row = $maconnexion->_bddFetchAssoc($res))
	      	{
				$listArray[$row['INT_COD']]['user'] = StripSlashes('['.$row['INT_TRG'].'] '.$row['INT_NOM'].' '.$row['INT_PRE']) ; 

				$group = array() ;
			    $sql1 = 'SELECT igl.GID, igl.LIBELLE, iug.MAIN_GROUP_YN ';
			    $sql1 .= 'FROM appl_group_libelle as igl, appl_user_group as iug ';
    			$sql1 .= 'WHERE iug.INT_COD = '.$row['INT_COD'].' ';
    	    	$sql1 .= 'AND LANG = \''.$lang.'\' ';
    	    	$sql1 .= 'AND igl.GID = iug.GID ';

				$res1 = $maconnexion->_bddQuery($sql1) ;

				while($row1 = $maconnexion->_bddFetchAssoc($res1) )
				{
					$group[$row1['GID']]['gid'] = $row1['GID'] ;
					$group[$row1['GID']]['libelle'] = StripSlashes($row1['LIBELLE']) ;

				}
				$listArray[$row['INT_COD']]['group'] = $group ;
			}
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
      	return $listArray ;    
    }
    
    /**
    * Mise � jour d'un utilisateur dans la bdd : d�sactivation
    * 
    * <p>_deactivate</p>
    * 
    * @name User::_deactivate()
    * @param $int_cod (int)
    * @return void
    */ 
    public function _deactivate($int_cod)
    {
    	if($int_cod == 1)
    	{
    	 	throw new MsgException('_ERROR_DELETE_SUPERADMIN_') ;	
    	}
		$maconnexion = MysqlDatabase::GetInstance() ;

		// Mise � jour dans la table T_AUT
		$sql  = 'UPDATE t_aut SET ';
		$sql .= 'AUT_STA =\'0\' ';     
		$sql .= 'WHERE int_cod = \''.$int_cod.'\' ' ;
        try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
    }
    
     /**
    * Mise � jour d'un intervenant dans la bdd : r�activation
    * 
    * <p>_restore</p>
    * 
    * @name User::_restore()
    * @param $int_cod (int)
    * @return void
    */ 
    public function _restore($int_cod)
    {
		$maconnexion = MysqlDatabase::GetInstance() ;
		 	
		// Mise � jour dans la table T_AUT
		$sql  = 'UPDATE t_aut SET ';
		$sql .= 'AUT_STA =\'1\' ';     
		$sql .= 'WHERE int_cod = \''.$int_cod.'\' ' ;
    	try{
			$res = $maconnexion->_bddQuery($sql) ;
      	}
      	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}
    }
    /**
    * Destructeur
    * 
    * <p>Destruction de l'instance de classe</p>
    * 
    * @name Person::__destruct()
    * @return void
    */
    public function __destruct() {
    }
 } 
?>