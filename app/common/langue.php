<?php
require_once( 'mysqlDatabase.php') ;
require_once( 'msgException.php') ;
/**
 * <p>Gestion des langues</p>
 * 
 * @name angue
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright Capella Conseil 2009
 * @version 1.0.0
 * @package common
 */

class Langue {
	/*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/

 	   
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name Message::__construct()
    * @return void 
    */
    public function __construct() {
    }
    
 
    
    /**
    * Recup�ration du tableau des langues support�es par l'application
    * 
    * <p>Obtenir un tableau des langues support�es</p>
    * 
    * @name Message::_getLang()
    * @return Array 
    */    
    public function _getLang()
    {
    	try{
    		$langArray = array() ;
			$maconnexion = MysqlDatabase::GetInstance() ;
			$sql = 'SELECT * FROM appl_languages ORDER BY LANG ';	
			$res = $maconnexion->_bddQuery($sql) ;
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$langArray[$row['LANG']] = StripSlashes($row['LIBELLE']) ;
			}
			return 	$langArray ;
    	}
          	catch(MsgException $e){
      		$msgString = $e ->_getError();
      		throw new MsgException($msgString, 'database') ;
      	}		
    }
    
    
}
?>