<?php
require_once('rights.php') ;
require_once('mysqlDatabase.php') ;
require_once('msgException.php') ;

/**
 *
 * <p>Intitul� de la classe</p>
 *
 * @name className
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package common
 */

class ModuleRights implements rights {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var (Singleton)
	 * @desc variable pour le singleton
	 */
	private static $instance;

	/**
	 * @var (String)
	 * @desc attributName
	 */
	private $value;
	/**
	 * @var (String)
	 * @desc attributName
	 */
	protected $value1;
	/**
	 * @var (String)
	 * @desc attributName
	 */
	public $value2;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name ModuleRights::__construct()
	 * @return void
	 */
	public function __construct() {

	}

	/**
	 * Singleton
	 *
	 * <p>cr�ation de l'instance de la classe si n'existe pas</p>
	 *
	 * @name ModuleRights::_GetInstance()
	 * @return ModuleRights
	 */
	public static function _GetInstance()
	{
		if(!isset(self::$instance))
		{
			self::$instance = new ModuleRights();
		}
		return self::$instance;
	}

	/**
	 * Mise � jour des droits pour les groupes et leurs utilisateurs
	 *
	 * <p>_createUserGroupRight</p>
	 *
	 * @name ModuleRights::_createUserGroupRight()
	 * @param $gid (Int)
	 * @param $uid (Int)
	 * @return void
	 */
	public function _createUserGroupRight($gid, $uid)
	{
		try{
			$maconnexion = MysqlDatabase::GetInstance() ;

			$sql  = 'SELECT * FROM appl_modules_acl ' ;
			$sql .= 'WHERE GID = '.$gid.' AND INT_COD = 0  ';
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0)
			{
				while($row = $maconnexion->_bddFetchAssoc($res))
				{
					$sql2  = 'INSERT INTO appl_modules_acl SET ';
					$sql2 .= 'MOD_ID = \''.$row['MOD_ID'].'\',  ';
					$sql2 .= 'RIGHT_ID = \''.$row['RIGHT_ID'].'\',  ';
					$sql2 .= 'INT_COD = \''.$uid.'\', ' ;
					$sql2 .= 'GID = \''.$gid.'\', ' ;
					$sql2 .= 'ACL = \''.$row['ACL'].'\' ' ;
					$res2 = $maconnexion->_bddQuery($sql2) ;
				}
			}
			$sql  = 'SELECT * ';
			$sql .= 'FROM appl_modules WHERE MOD_ENABLED = \'1\' ';
			$res = $maconnexion->_bddQuery($sql) ;
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$modname = $row['MOD_NAME'] ;
				 
				$sql2  = 'SELECT * FROM '.$modname.'_right_acl ';
				$sql2 .= 'WHERE GID = '.$gid.' AND INT_COD = 0  ';
				$res2 = $maconnexion->_bddQuery($sql2) ;
				if($maconnexion->_bddNumRows($res2) >0)
				{
					while($row2 = $maconnexion->_bddFetchAssoc($res2))
					{
						$sql3  = 'INSERT INTO '.$modname.'_right_acl SET ';
						$sql3 .= 'RIGHT_ID = \''.$row2['RIGHT_ID'].'\',  ';
						$sql3 .= 'INT_COD = \''.$uid.'\', ' ;
						$sql3 .= 'GID = \''.$gid.'\', ' ;
						$sql3 .= 'ACL = \''.$row2['ACL'].'\' ' ;
						$res3 = $maconnexion->_bddQuery($sql3) ;
					}
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise � jour des droits pour les groupes et leurs utilisateurs
	 *
	 * <p>_updateUserGroupRight</p>
	 *
	 * @name ModuleRights::_updateUserGroupRight()
	 * @param $gid (Int)
	 * @param $uid (Int)
	 * @return void
	 */
	public function _updateUserGroupRight($gid, $uid)
	{
		try{
			$maconnexion = MysqlDatabase::GetInstance() ;
			$sql  = 'DELETE FROM appl_modules_acl ' ;
			$sql .= 'WHERE GID = '.$gid.' AND INT_COD = \''.$uid.'\'  ';
			$res = $maconnexion->_bddQuery($sql) ;

			$sql  = 'SELECT * FROM appl_modules_acl ' ;
			$sql .= 'WHERE GID = '.$gid.' AND INT_COD = 0  ';
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0)
			{
				while($row = $maconnexion->_bddFetchAssoc($res))
				{
					$sql2  = 'INSERT INTO appl_modules_acl SET ';
					$sql2 .= 'MOD_ID = \''.$row['MOD_ID'].'\',  ';
					$sql2 .= 'RIGHT_ID = \''.$row['RIGHT_ID'].'\',  ';
					$sql2 .= 'INT_COD = \''.$uid.'\', ' ;
					$sql2 .= 'GID = \''.$gid.'\', ' ;
					$sql2 .= 'ACL = \''.$row['ACL'].'\' ' ;
					$res2 = $maconnexion->_bddQuery($sql2) ;
				}
			}
			$sql  = 'SELECT * ';
			$sql .= 'FROM appl_modules WHERE MOD_ENABLED = \'1\' ';
			$res = $maconnexion->_bddQuery($sql) ;
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$modname = $row['MOD_NAME'] ;
				$sql1  = 'DELETE FROM '.$modname.'_right_acl ';
				$sql1 .= 'WHERE GID = '.$gid.' AND INT_COD = \''.$uid.'\'  ';
				$res1 = $maconnexion->_bddQuery($sql1) ;
				 
				$sql2  = 'SELECT * FROM '.$modname.'_right_acl ';
				$sql2 .= 'WHERE GID = '.$gid.' AND INT_COD = 0  ';
				$res2 = $maconnexion->_bddQuery($sql2) ;
				if($maconnexion->_bddNumRows($res2) >0)
				{
					while($row2 = $maconnexion->_bddFetchAssoc($res2))
					{
						$sql3  = 'INSERT INTO '.$modname.'_right_acl SET ';
						$sql3 .= 'RIGHT_ID = \''.$row2['RIGHT_ID'].'\',  ';
						$sql3 .= 'INT_COD = \''.$uid.'\', ' ;
						$sql3 .= 'GID = \''.$gid.'\', ' ;
						$sql3 .= 'ACL = \''.$row2['ACL'].'\' ' ;
						$res3 = $maconnexion->_bddQuery($sql3) ;
					}
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise � jour des droits pour les groupes
	 *
	 * <p>_updateGroupRight</p>
	 *
	 * @name ModuleRights::_updateGroupRight()
	 * @param $modright (Array)
	 * @param $actArray (Array)
	 * @return void
	 */
	public function _updateGroupRight($modright=array(), $actArray=array())
	{
		try{
			$maconnexion = MysqlDatabase::GetInstance() ;
			$sql  = 'DELETE FROM appl_modules_acl WHERE GID > 1 ';
			$res = $maconnexion->_bddQuery($sql) ;

			if($modright != NULL)
			{
				foreach($modright as $key=>$value)
				{
					foreach($value as $right=>$value2)
					{
						foreach($value2 as $kid=>$gid)
						{
							$sql2  = 'INSERT INTO appl_modules_acl SET ';
							$sql2 .= 'MOD_ID = \''.$key.'\',  ';
							$sql2 .= 'RIGHT_ID = \''.$right.'\',  ';
							$sql2 .= 'INT_COD = 0, ' ;
							$sql2 .= 'GID = \''.$gid.'\', ' ;
							$sql2 .= 'ACL = \'1\' ' ;
							$res2 = $maconnexion->_bddQuery($sql2) ;
							 
							$sql1 = 'SELECT * FROM appl_user_group WHERE GID = \''.$gid.'\' ' ;
							$res1 = $maconnexion->_bddQuery($sql1) ;
							if($maconnexion->_bddNumRows($res1) >0)
							{
								while($row1 = $maconnexion->_bddFetchAssoc($res1))
								{
									$sql2  = 'INSERT INTO appl_modules_acl SET ';
									$sql2 .= 'MOD_ID = \''.$key.'\',  ';
									$sql2 .= 'RIGHT_ID = \''.$right.'\',  ';
									$sql2 .= 'INT_COD = \''.$row1['INT_COD'].'\', ' ;
									$sql2 .= 'GID = \''.$gid.'\', ' ;
									$sql2 .= 'ACL = \'1\' ' ;
									$res2 = $maconnexion->_bddQuery($sql2) ;
								}
							}
						}
					}
				}
			}

			if($actArray != NULL)
			{
				foreach($actArray as $key=>$value)
				{
					$sql  = 'SELECT * ';
					$sql .= 'FROM appl_modules ';
					$sql .= 'WHERE MOD_ID = '.$key;
					$res = $maconnexion->_bddQuery($sql) ;
					$row = $maconnexion->_bddFetchAssoc($res);
					$modname = $row['MOD_NAME'] ;
					$sql  = 'DELETE FROM '.$modname.'_right_acl  WHERE GID > 1 ';
					$res = $maconnexion->_bddQuery($sql) ;
					foreach($value as $right=>$value2)
					{
						foreach($value2 as $kid=>$gid)
						{
							$sql2  = 'INSERT INTO '.$modname.'_right_acl SET ';
							$sql2 .= 'RIGHT_ID = \''.$right.'\',  ';
							$sql2 .= 'INT_COD = 0, ' ;
							$sql2 .= 'GID = \''.$gid.'\', ' ;
							$sql2 .= 'ACL = \'1\' ' ;
							$res2 = $maconnexion->_bddQuery($sql2) ;
							 
							$sql1 = 'SELECT * FROM appl_user_group WHERE GID = \''.$gid.'\' ' ;
							$res1 = $maconnexion->_bddQuery($sql1) ;
							if($maconnexion->_bddNumRows($res1) >0)
							{
								while($row1 = $maconnexion->_bddFetchAssoc($res1))
								{
									$sql2  = 'INSERT INTO '.$modname.'_right_acl SET ';
									$sql2 .= 'RIGHT_ID = \''.$right.'\',  ';
									$sql2 .= 'INT_COD = \''.$row1['INT_COD'].'\', ' ;
									$sql2 .= 'GID = \''.$gid.'\', ' ;
									$sql2 .= 'ACL = \'1\' ' ;
									$res2 = $maconnexion->_bddQuery($sql2) ;
								}
							}
						}
					}
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise � jour des droits pour les utiliisateurs
	 *
	 * <p>_updateUserRight</p>
	 *
	 * @name ModuleRights::_updateUserRight()
	 * @param $userid (Int)
	 * @param $modright (Array)
	 * @param $actArray (Array)
	 * @return void
	 */
	public function _updateUserRight($userid, $modright=array(), $actArray=array())
	{
		try{
			$maconnexion = MysqlDatabase::GetInstance() ;
			$sql  = 'DELETE FROM appl_modules_acl WHERE INT_COD = '.$userid.' AND GID = 0 ';
			$res = $maconnexion->_bddQuery($sql) ;

			if($modright != NULL)
			{
				foreach($modright as $key=>$value)
				{
					foreach($value as $key2=>$right)
					{
						$sql2  = 'INSERT INTO appl_modules_acl SET ';
						$sql2 .= 'MOD_ID = \''.$key.'\',  ';
						$sql2 .= 'RIGHT_ID = \''.$right.'\',  ';
						$sql2 .= 'INT_COD = \''.$userid.'\', ' ;
						$sql2 .= 'GID = 0, ' ;
						$sql2 .= 'ACL = \'1\' ' ;
						$res2 = $maconnexion->_bddQuery($sql2) ;
					}
				}
			}

			if($actArray != NULL)
			{
				foreach($actArray as $key=>$value)
				{
					$sql  = 'SELECT * ';
					$sql .= 'FROM appl_modules ';
					$sql .= 'WHERE MOD_ID = '.$key;
					$res = $maconnexion->_bddQuery($sql) ;
					$row = $maconnexion->_bddFetchAssoc($res);
					$modname = $row['MOD_NAME'] ;
					$sql  = 'DELETE FROM '.$modname.'_right_acl  WHERE INT_COD = '.$userid.' AND GID = 0 ';
					$res = $maconnexion->_bddQuery($sql) ;
					foreach($value as $key2=>$right)
					{
						$sql2  = 'INSERT INTO '.$modname.'_right_acl SET ';
						$sql2 .= 'RIGHT_ID = \''.$right.'\',  ';
						$sql2 .= 'INT_COD = \''.$userid.'\', ' ;
						$sql2 .= 'GID = 0, ' ;
						$sql2 .= 'ACL = \'1\' ' ;
						$res2 = $maconnexion->_bddQuery($sql2) ;
					}
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}


	/**
	 * R�cup�ration des donn�es de droit d'acc�s des modules activ�s dans un tableau
	 *
	 * <p>_getModuleRights</p>
	 *
	 * @name ModuleRights::_getModuleRights()
	 * @param $lg (String)
	 * @return array
	 */
	public function _getModuleRights($lg = 'fr', $gid=0, $userid=0)
	{
		$moduleArray = array() ;
		try{
			$maconnexion = MysqlDatabase::GetInstance() ;
			$sql  = 'SELECT * ';
			$sql .= 'FROM appl_modules ';
			$sql .= 'WHERE MOD_ENABLED = 1 ';
			$sql .= 'ORDER BY MOD_TITLE ' ;
			$res = $maconnexion->_bddQuery($sql) ;
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$modright = array() ;
				$actionright = array() ;
				$moduleArray[$row['MOD_ID']]['name'] = StripSlashes($row['MOD_NAME']) ;
				$moduleArray[$row['MOD_ID']]['title'] = StripSlashes($row['MOD_TITLE']) ;

				$sql1  = 'SELECT * ';
				$sql1 .= 'FROM appl_modules_right_libelle ';
				$sql1 .= 'WHERE LANG = \''.$lg.'\' ' ;
				$sql1 .= 'ORDER BY LIBELLE ' ;
				$res1 = $maconnexion->_bddQuery($sql1) ;
				while($row1 = $maconnexion->_bddFetchAssoc($res1))
				{
					$group = array() ;
					$user = array() ;
					$modright[$row1['RIGHT_ID']]['name'] = StripSlashes($row1['LIBELLE']) ;
					$sql2  = 'SELECT * ';
					$sql2 .= 'FROM appl_modules_acl ';
					$sql2 .= 'WHERE MOD_ID = \''.$row['MOD_ID'].'\' ' ;
					$sql2 .= 'AND RIGHT_ID = \''.$row1['RIGHT_ID'].'\' ' ;
					if($gid != 0)
					{
						$sql2 .= 'AND GID = \''.$gid.'\' ' ;
					}
					if($userid != 0)
					{
						$sql2 .= 'AND INT_COD = \''.$userid.'\' ' ;
					}
					$res2 = $maconnexion->_bddQuery($sql2) ;
					while($row2 = $maconnexion->_bddFetchAssoc($res2))
					{
						$group[$row2['GID']] = $row2['GID'] ;
						$user[$row2['INT_COD']]['group'] = $row2['GID'] ;
						$user[$row2['INT_COD']]['userid'] = $row2['INT_COD'] ;
					}
					$modright[$row1['RIGHT_ID']]['group'] = $group ;
					$modright[$row1['RIGHT_ID']]['user'] = $user ;
				}
				$moduleArray[$row['MOD_ID']]['right'] = $modright ;
				 

				$sql3  = 'SELECT rl.RIGHT_ID, rl.LIBELLE ';
				$sql3 .= 'FROM '.$row['MOD_NAME'].'_right_libelle as rl, '.$row['MOD_NAME'].'_right as ri ';
				$sql3 .= 'WHERE LANG = \''.$lg.'\' ' ;
				$sql3 .= 'AND rl.RIGHT_ID=ri.RIGHT_ID ';
				$sql3 .= 'ORDER BY ri.RIGHT_CODE, rl.LIBELLE ' ;
				$res3 = $maconnexion->_bddQuery($sql3) ;
				while($row3 = $maconnexion->_bddFetchAssoc($res3))
				{
					$group = array() ;
					$user = array() ;
					$actionright[$row3['RIGHT_ID']]['name'] = StripSlashes($row3['LIBELLE']) ;
					$sql4  = 'SELECT * ';
					$sql4 .= 'FROM '.$row['MOD_NAME'].'_right_acl ';
					$sql4 .= 'WHERE RIGHT_ID = \''.$row3['RIGHT_ID'].'\' ' ;
					if($gid != 0)
					{
						$sql4 .= 'AND GID = \''.$gid.'\' ' ;
					}
					if($userid != 0)
					{
						$sql4 .= 'AND INT_COD = \''.$userid.'\' ' ;
					}
					$res4 = $maconnexion->_bddQuery($sql4) ;
					while($row4 = $maconnexion->_bddFetchAssoc($res4))
					{
						$group[$row4['GID']] = $row4['GID'] ;
						$user[$row4['INT_COD']]['group'] = $row4['GID'] ;
						$user[$row4['INT_COD']]['userid'] = $row4['INT_COD'] ;
					}
					$actionright[$row3['RIGHT_ID']]['group'] = $group ;
					$actionright[$row3['RIGHT_ID']]['user'] = $user ;
				}
				$moduleArray[$row['MOD_ID']]['action_right'] = $actionright ;

			}
			return $moduleArray ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * R�cup�ration des donn�es de droit d'action des modules activ�s dans un tableau
	 *
	 * <p>_getModActRights</p>
	 *
	 * @name ModuleRights::_getModActRights()
	 * @param $gid (Int)
	 * @param $lg (String)
	 * @return array
	 */
	public function _getModActRights($lg = 'fr', $modid=0)
	{
		$moduleArray = array() ;
		try{
			$maconnexion = MysqlDatabase::GetInstance() ;
			$sql  = 'SELECT * ';
			$sql .= 'FROM appl_modules ';
			$sql .= 'WHERE MOD_ENABLED = 1 ';
			if($modid != 0)
			{
				$sql .= 'AND MOD_ID = \''.$modid.'\' ';
			}
			$sql .= 'ORDER BY MOD_TITLE ' ;
			$res = $maconnexion->_bddQuery($sql) ;
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$modright = array() ;
				$moduleArray[$row['MOD_ID']]['name'] = StripSlashes($row['MOD_NAME']) ;
				$moduleArray[$row['MOD_ID']]['title'] = StripSlashes($row['MOD_TITLE']) ;

				$sql1  = 'SELECT * ';
				$sql1 .= 'FROM appl_modules_right_libelle ';
				$sql1 .= 'WHERE LANG = \''.$lg.'\' ' ;
				$sql1 .= 'ORDER BY LIBELLE ' ;
				$res1 = $maconnexion->_bddQuery($sql1) ;
				while($row1 = $maconnexion->_bddFetchAssoc($res1))
				{
					$group = array() ;
					$user = array() ;
					$modright[$row1['RIGHT_ID']]['name'] = StripSlashes($row1['LIBELLE']) ;
					$sql2  = 'SELECT * ';
					$sql2 .= 'FROM appl_modules_acl ';
					$sql2 .= 'WHERE MOD_ID = \''.$row['MOD_ID'].'\' ' ;
					$sql2 .= 'AND  RIGHT_ID = \''.$row1['RIGHT_ID'].'\' ' ;
					$res2 = $maconnexion->_bddQuery($sql2) ;
					while($row2 = $maconnexion->_bddFetchAssoc($res2))
					{
						$group[$row2['GID']] = $row2['GID'] ;
						$user[$row2['INT_COD']] = $row2['INT_COD'] ;
					}
					$modright[$row1['RIGHT_ID']]['group'] = $group ;
					$modright[$row1['RIGHT_ID']]['user'] = $user ;
				}
				$moduleArray[$row['MOD_ID']]['right'] = $modright ;

			}
			return $moduleArray ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * V�rification des droits d'acc�s � au moins un module d'un utilisateur
	 *
	 * <p>_isAllowed</p>
	 *
	 * @name ModuleRights::_isAllowed()
	 * @param $rightID::Int
	 * @param $ID::Int
	 * @return boolean
	 */
	public function _isAllowed($rightID, $uid)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * ';
		$sql .= 'FROM appl_modules_acl ';
		$sql .= 'WHERE right_id = \''.$rightID.'\' ' ;
		$sql .= 'AND int_cod = \''.$uid.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res) >= 1)
		{
			return TRUE ;
		}else{
			return FALSE ;
		}
	}
	/**
	 * V�rification des droits d'acc�s � un module d'un utilisateur
	 *
	 * <p>_isModuleAllowed</p>
	 *
	 * @name ModuleRights::_isModuleAllowed()
	 * @param $modID::Int
	 * @param $rightID::Int
	 * @param $ID::Int
	 * @return boolean
	 */
	public function _isModuleAllowed($modID, $rightID, $ID, $code='_MODULE_ACCESS_NOT_ALLOWED_')
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * ';
		$sql .= 'FROM appl_modules_acl ';
		$sql .= 'WHERE right_id = \''.$rightID.'\' ' ;
		$sql .= 'AND int_cod = \''.$ID.'\' ' ;
		$sql .= 'AND mod_id = \''.$modID.'\' ' ;

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res) >= 1)
		{
			return TRUE ;
		}else{
			if($code != '')
			{
				throw new MsgException($code) ;
			}else{
				return FALSE ;
			}
		}
	}

	/**
	 * Chargement des modules accessibles par un utilisateur
	 *
	 * <p>_loadModules</p>
	 *
	 * @name ModuleRights::_loadModules()
	 * @param $UID::Int
	 * @return array
	 */
	public function _loadModules($UID=0)
	{
		$moduleArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * ';
		$sql .= 'FROM appl_modules ';
		$sql .= 'WHERE MOD_ENABLED = 1 ';
		$sql .= 'ORDER BY MOD_ORDER ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		while($row = $maconnexion->_bddFetchAssoc($res))
		{
			//$moduleArray[$row['MOD_ID']]['name'] = StripSlashes($row['MOD_NAME']) ;
			//$moduleArray[$row['MOD_ID']]['title'] = StripSlashes($row['MOD_TITLE']) ;

			// par defaut, on revient sur l'accueil
			//$moduleArray[$row['MOD_ID']]['link'] = StripSlashes('../main/menu.php') ;

			if($UID != 0)
			{
					
				$sql2  = 'SELECT * ';
				$sql2 .= 'FROM appl_modules_acl ';
				$sql2 .= 'WHERE MOD_ID = \''.$row['MOD_ID'].'\' ' ;
				$sql2 .= 'AND INT_COD = \''.$UID.'\' ' ;
				try{
					$res2 = $maconnexion->_bddQuery($sql2) ;
				}
				catch(MsgException $e){
					$msgString = $e ->_getError();
					throw new MsgException($msgString, 'database') ;
				}
				if($maconnexion->_bddNumRows($res2) >= 1)
				{
				$moduleArray[$row['MOD_ID']]['name'] = StripSlashes($row['MOD_NAME']) ;
				$moduleArray[$row['MOD_ID']]['title'] = StripSlashes($row['MOD_TITLE']) ;
					$moduleArray[$row['MOD_ID']]['link'] = StripSlashes('../'.$row['MOD_NAME'].'/'.$row['MOD_NAME'].'Menu.php') ;
				}
			}
		}
		return $moduleArray ;
	}

	/**
	 * V�rification des droits d'un utilisateur pour une action dans un module
	 *
	 * <p>_isActionAllowed</p>
	 *
	 * @name ModuleRights::_isActionAllowed()
	 * @param $modName::String
	 * @param $rightID::Int
	 * @param $UID::Int
	 * @return boolean
	 */
	public function _isActionAllowed($modName, $rightID, $UID, $code='')
	{
		if($UID==1)
		{
			return TRUE ;
		}else{
			$maconnexion = MysqlDatabase::GetInstance() ;
			$sql  = 'SELECT ra.RIGHT_ID ';
			$sql .= 'FROM '.$modName.'_right_acl as ra, t_int as ti ';
			$sql .= 'WHERE ra.RIGHT_ID = \''.$rightID.'\' ' ;
			$sql .= 'AND ra.INT_COD = \''.$UID.'\' ' ;
			$sql .= 'AND ra.INT_COD = ti.INT_COD ' ;
			$sql .= 'AND ti.IND_SUPP = \'\' ' ;
			$sql .= 'AND ra.ACL = 1 ' ;
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			if($maconnexion->_bddNumRows($res)== 1)
			{
				return TRUE ;
			}else{
				if($code != '')
				{
					throw new MsgException($code) ;
				}else{
					return FALSE ;
				}
			}
		}
	}

	/*~*~*~*~*~*~*~*~*~*~*~*~*~*/
	/*  2.1 m�thodes publiques */
	/*~*~*~*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name className::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>