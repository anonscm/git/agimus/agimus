<?php
/**
 * 
 * <p>Form</p>
 * 
 * @name Form
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright Capella Conseil 2009
 * @version 1.0.0
 * @package common
 */
 
 class Form {
 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * url de la page de r�ception des informations du formulaire
    * @var (String)
    * @desc action
    */
    private $action;
    /**
    * m�thode d'envoi des informations : get ou post (recommand�)
    * @var (String)
    * @desc method
    */
    private $method;
    /**
    * type de fichier � envoyer, par d�faut : 'application/x-www-form-urlencoded'
    * 
    * Tableau de r�f�rence :
    * 
    * valeur => extension
    * application / octet-stream => .bin ou .exe
	* text / plain => .txt
	* text / richtext => .rxt
	* video / mpeg => .mpeg ou .mpg ou .mpe
	* video / quicktime => .qt ou .mov
	* video / x-msvideo => .avi
	* 
    * @var (String)
    * @desc enctype
    */
    private $enctype;
    
    /**
    * d�finition des CSS
    * @var (String)
    * @desc style
    */
    private $style;
    
    /**
    * identifiant de la balise form
    * @var (String)
    * @desc id
    */
    private $id;
    /**
    * Tableau des input du formulaire
    * @var (Array)
    * @desc inputArray
    */
    private $inputArray;
    
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name className::__construct()
    * @return void 
    */
    public function __construct($action, $method='post', $id='', $enctype='application/x-www-form-urlencoded') {
    
    	$this->action = $action ;
    	$this->method = $method ;
    	$this->enctype = $enctype ;
    	$this->id = $id ;
    	$this->inputArray = array() ;
    }
     
    /**
    * Construction d'une balise input
    * 
    * <p>_mkInput</p>
    * 
    * @name Form::_mkInput()
    * @param $type (string)
    * @param $name (string)
    * @param $value (String)
    * @param $attributs (String)
    * @return array 
    */    
    public function _mkInput($type, $name, $value='', $attributs='')
    {
    	$input='' ;
    	$input .= '<input type="'.$type.'" ' ;
    	$input .= 'id="'.$name.'" name="'.$name.'" ' ;
    	if($value != '')
    	{
    		$input .= 'value="'.$value.'" ' ;
    	}
        if($attributs != '')
    	{
    		$input .= $attributs ;
    	}
    	$input .= ' />' ;
    	return $input ;
    }
    
     public function _mkDateInput($name, $value='', $formid ='0', $disabled='')
    {
    	$input='' ;

		//v1.5-PPR-#006 intégration jquery date-picker
		if($disabled == '')
    	{	
	    	$input .= '<script type="text/javascript">';
			$input .= '$(function() { ';
			$input .= '	$("#'.$name.'").datepicker();';
			$input .= '}); ';
			$input .= '</script> ';			
    	}

    	$input .= '<input type="text" readonly ' ;
    	if($value != '')
    	{
			$input .= 'value="'.$value.'" ' ;
    	}
    	$input .= 'id="'.$name.'" name="'.$name.'" ' ;
    	$input .= ' />' ;    	
    	return $input ;
    }
    
    public function _mkTimeInput($name='', $jname='jour', $hname='heure', $mname='minute', $sname='seconde', $j=0, $h=0, $m=0, $s=0)
    {
    	
    	$input='<table class="tableinput"><tr>' ;
    	$input .= '<td>'.$jname.' : <input type="text" ' ;
    	$input .= 'id="'.$name.'_jour" name="'.$name.'_jour" ' ;
    	if($j != '')
    	{
    		$input .= 'value="'.$j.'" ' ;
    	}
    	$input .= ' /></td>' ;
    	$input .= '<td>'.$hname.' : <input type="text" ' ;
    	$input .= 'id="'.$name.'_heure" name="'.$name.'_heure" ' ;
    	if($h != '')
    	{
    		$input .= 'value="'.$h.'" ' ;
    	}
    	$input .= ' /></td>' ;
    	$input .= '<td>'.$mname.' : <input type="text" ' ;
    	$input .= 'id="'.$name.'_mn" name="'.$name.'_mn" ' ;
    	if($m != '')
    	{
    		$input .= 'value="'.$m.'" ' ;
    	}
    	$input .= ' /></td>' ;
    	$input .= '<td>'.$sname.' : <input type="text" ' ;
    	$input .= 'id="'.$name.'_scd" name="'.$name.'_scd" ' ;
    	if($s != '')
    	{
    		$input .= 'value="'.$s.'" ' ;
    	}
    	$input .= ' /></td>' ;
    	$input .= '</tr></table>' ;
    	return $input ;
    	
    }
    
    function _mkTextArea($name, $value='', $cols = 100, $rows = 4)
     {
    	$area='' ;
    	$area .= '<textarea id="'.$name.'" name="'.$name.'" cols ="'.$cols.'" rows="'.$rows.'">' ;
    	if($value != '')
    	{
			$area .= $value ;
    	}
    	$area .= '</textarea>' ;
    	return $area ;
    }
    
    function _mkCheckBox($name, $checked = '')
    {
    	$input = '<input type="checkbox" ' ;
    	$input .= 'id="'.$name.'" name="'.$name.'" ' ;
    	if($checked != '')
    	{
    		$input .= 'checked ="checked"' ;
    	}
    	$input .= '/>' ;
    	return $input ;
    }

    /**
    * Construction d'une table de cases � cocher
    * 
    * <p>_mkCheckBoxest</p>
    * 
    * @name Form::_mkCheckBoxes()
    * @param $name (string)
    * Nom de la balise
    * @param $valueArray (Array)
    *			Table des �l�ments constituant les cases � cocher : [id]=>value
    * @param $checkedArray (Array)
    *			Table des �l�ments coch�s
    * @param $cols (int)
    * 			Nombre des colonnes
    * @param $usedArray (Array)
    *			Table des �l�ments utiles pour ne pas s�lectionner d'�l�ments st�riles
    * @return array 
    */
    
    function _mkCheckBoxes($name, $valueArray, $checkedArray=array(), $cols=3, $usedArray = 'none')
    {
		if($cols>1)
		{
	      	$itemsNB = count($valueArray) ;
			$nbcols = (int)($itemsNB / $cols) ;
	      	if($itemsNB % $cols > 0)
	      	{
	         	$nbcols++ ;
	      	}
		}
		$i=0 ;
      	$j=0 ;
    	$input='<tr>' ;
		$colstr=(string)$nbcols ;
    	foreach($valueArray as $key=>$value)
    	{
			if(($i == 0)&&($j < $itemsNB))
         	{
            	$input .= '<td >';
         	}

    		$input .= '<input type="checkbox" ' ;
    		$input .= 'id="'.$name.$key.'" name="'.$name.'[]" ' ;
    		$input .= 'value="'.$key.'" ' ;
    		if(in_array($key, $checkedArray))
    		{
    			$input .= 'checked="checked"' ;
    		}
    		if((is_array($usedArray))&&(!in_array($key, $usedArray)))
    	    {
    			$input .= 'disabled="disabled"' ;
    		}
    		$input .= '/>' ;
    		$input .= '<label for='.$name.$key.'>'.$value.'</label>' ;
    		
    		$input .= '<br/>' ;
    		$i++ ;
    		$j++ ;
    		if(($i>= $nbcols)||($j >= $itemsNB))
         {
            $input .= '</td>';
            $i = 0 ;
         }
    	}
    	$input .= '</tr>' ;
    	return $input ;
    }
     /**
    * Construction d'une balise input
    * 
    * <p>_mkInput</p>
    * 
    * @name Form::_mkInput()
    * @param $type (string)
    * @param $name (string)
    * @param $value (String)
    * @param $attributs (String)
    * @return array 
    */    
    public function _mkRadioInput($name, $valueArray, $checked='', $colOrRow='row' )
    {
    	$input='' ;
    	foreach($valueArray as $key=>$value)
    	{
    		$input .= '<label for='.$name.$key.'>'.$value.'</label>' ;
    		$input .= '<input type="radio" ' ;
    		$input .= 'id="'.$name.$key.'" name="'.$name.'" ' ;
    		$input .= 'value="'.$value.'" ' ;
    		if($key == $checked)
    		{
    			$input .= 'checked="checked"' ;
    		}
    		$input .= '/>' ;
    		if($colOrRow=='col')
    		{
    			$input .= '<br/>' ;
    		}
    	}
    	return $input ;
    	//$this->inputArray[] = $input ;
    }
    
      /**
    * Construction d'une balise de type select
    * 
    * <p>_mkSelect</p>
    * 
    * @name Form::_mkSelect()
    * @param $name (string)
    * @param $valueArray (Array)
    * @param $selected (String)
    * @return array 
    */    
    public function _mkSelect($name, $valueArray, $selected='0', $attributs ='', $firstvalname='' )
    {
    	$select ='';

    	$select .= '<select name="'.$name.'" id="'.$name.'" ' ;
        if($attributs != '')
    	{
    		$select .= $attributs ;
    	}
    	$select .= '>' ;
    	$select .='<option value="0">'.$firstvalname.'</option>' ;
    	
    	if (count($valueArray)>0)
    	foreach($valueArray as $key=>$value)
    	{
    		$select .='<option value="'.$key.'" ' ;
    		if(($selected != '0')&&($key == $selected))
    		{
    			$select .='selected="selected" ' ;	
    		}
    		$select .='>'.$value.'</option>' ;
    	}
		$select .= '</select>' ;
    	return $select ;
    }
    
    
    public function _mkSubmit($name, $value='Valider')
    {
    	$input='<input type="submit" ' ;
    	$input .= 'name="'.$name.'" ' ;
    	$input .= 'id="'.$name.'" ' ;
    	$input .= 'value="'.$value.'">' ;
    	return $input ;    	
    }
    
    /**
    * Desc
    * 
    * <p>libelle</p>
    * 
    * @name className::_methodName()
    * @param $attributname
    * @return void 
    */
    public function _builForm()
    {
    }
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /*  2.1 m�thodes publiques */
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Destructeur
    * 
    * <p>Destruction de l'instance de classe</p>
    * 
    * @name className::__destruct()
    * @return void
    */
    public function __destruct() {
    
    }
 } 
?>