<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * <p>Composante Intra_etablissement</p>
 *
 * @name  CompIntra
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

class CompIntra {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc Identifiant
	 */
	private $id;

	/**
	 * @var (String)
	 * @desc Reference externe
	 */
	private $extnRef;

	/**
	 * @var (String)
	 * @desc Libelle
	 */
	private $lib;

	/**
	 * @var (String)
	 * @desc Decription
	 */
	private $dsc;

	/**
	 * @var (String)
	 * @desc Chaine Type (reperage)
	 */
	private $chaineType;

	/**
	 * @var (Int)
	 * @desc Identifiant Dimension mere
	 */
	private $idRegr;

	/**
	 * @var (Int)
	 * @desc ID Structure
	 */
	private $idStruct;

	/**
	 * @var (Array)
	 * @desc tableau identifiant=>libelle *
	 private $comboList;


	 /*~*~*~*~*~*~*~*~*~*~*/
	/*  2. methodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>creation de l'instance de la classe</p>
	 *
	 * @name CompIntra::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		$this->id=0;
		$this->extnRef='';
		$this->lib='';
		$this->dsc='';
		$this->chaineType='';
		$this->idRegr=0;
		$this->idStruct=0;

		$this->comboList = array() ;

		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}

	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name CompIntra::_getId()
	 * @return Int
	 */
	public function _getId() {
		return $this->id ;
	}

	/**
	 * @name CompIntra::_getExtnRef()
	 * @return string
	 */
	public function _getExtnRef() {
		return $this->extnRef ;
	}

	/**
	 * @name CompIntra::_getLib()
	 * @return String
	 */
	public function _getLib() {
		return $this->lib ;
	}

	/**
	 * @name CompIntra::_getDsc()
	 * @return String
	 */
	public function _getDsc() {
		return $this->dsc ;
	}

	/**
	 * @name CompIntra::_getChaineType()
	 * @return String
	 */
	public function _getChaineType() {
		return $this->chaineType ;
	}

	/**
	 * @name CompIntra::_getIdRegr()
	 * @return Int
	 */
	public function _getIdRegr() {
		return $this->idRegr ;
	}

	/**
	 * @name CompIntra::_getIdStruct()
	 * @return Int
	 */
	public function _getIdStruct() {
		return $this->idStruct ;
	}

	/**
	 * @name CatSrv::_getComboList()
	 * @return array
	 */
	public function _getComboList()
	{
		return $this->comboList ;
	}



	/**
	 * Accesseurs en ecriture
	 */

	/**
	 * @name CompIntra::_setId()
	 * @param $id (Int)
	 * @return void
	 */
	public function _setId($id) {
		$this->id  = $id ;
	}

	/**
	 * @name CompIntra::_setExtnRef()
	 * @param $extnRef (String)
	 * @return void
	 */
	public function _setExtnRef($extnRef) {
		$this->extnRef  = $extnRef ;
	}

	/**
	 * @name CompIntra::_setLib()
	 * @param $lib (String)
	 * @return void
	 */
	public function _setLib($lib) {
		$this->lib  = $lib ;
	}

	/**
	 * @name CompIntra::_setDsc()
	 * @param $dsc (String)
	 * @return void
	 */
	public function _setDsc($dsc) {
		$this->dsc  = $dsc ;
	}

	/**
	 * @name CompIntra::_setChaineType()
	 * @param $chaineType (String)
	 * @return void
	 */
	public function _setChaineType($chaineType) {
		$this->chaineType  = $chaineType ;
	}

	/**
	 * @name CompIntra::_setIdRegr()
	 * @param $idRegr (Int)
	 * @return void
	 */
	public function _setIdRegr($idRegr) {
		$this->idRegr  = $idRegr ;
	}


	/**
	 * @name CompIntra::_setIdStruct()
	 /* @param $idStruct (Int)
	 * @return void
	 */
	public function _setIdStruct($idStruct) {
		$this->idStruct  = $idStruct ;
	}

	/**
	 * Compte du nombre d'occurence de l'objet CompIntra dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name CompIntra::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_comp_intra ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * renvoit l'id d'une comp_intra sur le libelle
	 *
	 * <p>_existsRef</p>
	 *
	 * @name CompIntra::_existsRef()
	 * @param $stampStart string
	 * @return int
	 */

	public function _existsRef($lib,$strict=0)
	{
		$idRef = 0 ;
		$listArray=array();
		if (trim($lib)=="") return $listArray;

		$maconnexion = MysqlDatabase::GetInstance() ;

		if ($strict)
		{
			$sql = 'SELECT id ';
			$sql .= ' FROM t_comp_intra ';
			$sql .= ' WHERE UPPER(lib) = \''.strtoupper($lib).'\' ';

			try{
				$res = $maconnexion->_bddQuery($sql) ;
				$row = $maconnexion->_bddFetchAssoc($res);
				$idRef  = $row['id'] ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			return $idRef;
		}
		else
		{
			// recherche de concordance entre libelle et url_type
			$sql = 'SELECT * ';
			$sql .= ' FROM t_comp_intra ';
			$sql .= ' WHERE CHAINE_TYPE <>\'\' ';
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

			if($maconnexion->_bddNumRows($res))
			{
				while($row = $maconnexion->_bddFetchAssoc($res))
				{
					$row=keyToUpper($row);
					if ($debug) echo "\r\ncomparatif [$lib] et ['".$row['CHAINE_TYPE']."']";
					//if (strstr($lib,$row['CHAINE_TYPE']))
					if (preg_match($row['CHAINE_TYPE'],$lib))
					$listArray[]=$row['ID'];
				}
			}
			return $listArray;
		}
	}

	/**
	 * Creation d'un CompIntra dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name CompIntra::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM t_comp_intra ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}

		$sql  = 'INSERT INTO t_comp_intra VALUES( ';
		$sql .= '\''.AddSlashes($this->id).'\', ';
		$sql .= '\''.AddSlashes($this->extnRef).'\', ';
		$sql .= '\''.AddSlashes($this->lib).'\', ';
		$sql .= '\''.AddSlashes($this->dsc).'\', ';
		$sql .= '\''.AddSlashes($this->chaineType).'\', ';
		$sql .= '\''.AddSlashes($this->idRegr).'\', ';
		$sql .= '\''.AddSlashes($this->idStruct).'\' ';

		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise a jour d'un CompIntra dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name CompIntra::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_comp_intra SET ';
		$sql .= 'ID = \''.AddSlashes($this->id).'\', ';
		$sql .= 'EXTN_REF = \''.AddSlashes($this->extnRef).'\', ';
		$sql .= 'LIB = \''.AddSlashes($this->lib).'\', ';
		$sql .= 'DSC = \''.AddSlashes($this->dsc).'\', ';
		$sql .= 'CHAINE_TYPE = \''.AddSlashes($this->chaineType).'\', ';
		$sql .= 'ID_REGR = \''.AddSlashes($this->idRegr).'\', ';
		$sql .= 'ID_STRUCT = \''.AddSlashes($this->idStruct).'\' ';

		$sql .= 'WHERE id=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un CompIntra dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name CompIntra::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'DELETE FROM t_comp_intra ';
		$sql .= 'WHERE id = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Recuperation de la liste des CompIntras
	 *
	 * <p>Liste des CompIntra</p>
	 *
	 * @name CompIntra::_getList()
	 * @return array
	 */
	public function _getList($idRegr=0)
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_comp_intra WHERE 1 ' ;
		if ($this->id>0)
		$sql.= ' AND id = \''.$this->id.'\' ';
		if ($idRegr>0)
		$sql.= ' AND id_regr = \''.$idRegr.'\' ';
		$sql .= 'ORDER BY LIB ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['ID'] = StripSlashes($row['ID']) ;
				$listArray[$row['ID']]['EXTN_REF'] = StripSlashes($row['EXTN_REF']) ;
				$listArray[$row['ID']]['LIB'] = StripSlashes($row['LIB']) ;
				$listArray[$row['ID']]['DSC'] = StripSlashes($row['DSC']) ;
				$listArray[$row['ID']]['CHAINE_TYPE'] = StripSlashes($row['CHAINE_TYPE']) ;
				$listArray[$row['ID']]['ID_REGR'] = StripSlashes($row['ID_REGR']) ;
				$listArray[$row['ID']]['ID_STRUCT'] = StripSlashes($row['ID_STRUCT']) ;
				$this->comboList[$row['ID']] = StripSlashes($row['LIB']) ;

			}
		}
		return $listArray ;
	}

	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name CompIntra:_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id==0) return false;

		$listArray=$this->_getList();
		$this->_setExtnRef($listArray[$this->id]['EXTN_REF']);
		$this->_setLib($listArray[$this->id]['LIB']);
		$this->_setDsc($listArray[$this->id]['DSC']);
		$this->_setChaineType($listArray[$this->id]["CHAINE_TYPE"]);
		$this->_setIdRegr($listArray[$this->id]["ID_REGR"]);
		$this->_setIdStruct($listArray[$this->id]["ID_STRUCT"]);
	}

	/**
	 * Récupération de la liste des Alertes
	 *
	 * <p>Liste des Alertes</p>
	 *
	 * @name CompIntra::_getAlerts()
	 * @return array
	 */
	public function _getAlerts($typeAlert=1)
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_comp_intra WHERE 1 ' ;
		if ($typeAlert==1)
			$sql.= ' AND id_regr = 0 ';
		if ($typeAlert==2)
			$sql.= ' AND lib like  \'%~%\' ';
		$sql .= 'ORDER BY id desc ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]=$row['LIB'];				
			}
		}
		return $listArray ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name CompIntra::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>