<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * <p>Discipline Intra_etablissement</p>
 *
 * @name  DcplIntra
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

class DcplIntra {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc Identifiant
	 */
	private $id;

	/**
	 * @var (String)
	 * @desc R�f�rence externe
	 */
	private $extnRef;
	
	/**
	 * @var (String)
	 * @desc Libell�
	 */
	private $lib;

	/**
	 * @var (String)
	 * @desc Decription
	 */
	private $dsc;

	/**
	 * @var (String)
	 * @desc Chaine Type (reperage)
	 */
	private $chaineType;

	/**
	 * @var (Int)
	 * @desc Identifiant Dimension m�re
	 */
	private $idRegr;

	/**
	 * @var (Int)
	 * @desc ID Structure
	 */
	private $idStruct;

	/**
	 * @var (Array)
	 * @desc tableau identifiant=>libell� */
	private $comboList;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name DcplIntra::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		$this->id=0;
		$this->extnRef='';
		$this->lib='';
		$this->dsc='';
		$this->chaineType='';
		$this->idRegr=0;
		$this->idStruct=0;

		$this->comboList = array() ;
		
		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}

	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name DcplIntra::_getId()
	 * @return Int
	 */
	public function _getId() {
		return $this->id ;
	}

		/**
	 * @name DcplIntra::_getExtnRef()
	 * @return string
	 */
	public function _getExtnRef() {
		return $this->extnRef ;
	}
	
	/**
	 * @name DcplIntra::_getLib()
	 * @return String
	 */
	public function _getLib() {
		return $this->lib ;
	}

	/**
	 * @name DcplIntra::_getDsc()
	 * @return String
	 */
	public function _getDsc() {
		return $this->dsc ;
	}

	/**
	 * @name DcplIntra::_getChaineType()
	 * @return String
	 */
	public function _getChaineType() {
		return $this->chaineType ;
	}

	/**
	 * @name DcplIntra::_getIdRegr()
	 * @return Int
	 */
	public function _getIdRegr() {
		return $this->idRegr ;
	}

	/**
	 * @name DcplIntra::_getIdStruct()
	 * @return Int
	 */
	public function _getIdStruct() {
		return $this->idStruct ;
	}

	/**
	 * @name CatSrv::_getComboList()
	 * @return array
	 */
	public function _getComboList()
	{
		return $this->comboList ;
	}

	/**
	 * Accesseurs en �criture
	 */

	/**
	 * @name DcplIntra::_setId()
	 * @param $id (Int)
	 * @return void
	 */
	public function _setId($id) {
		$this->id  = $id ;
	}

	/**
	 * @name DcplIntra::_setExtnRef()
	 * @param $extnRef (String)
	 * @return void
	 */
	public function _setExtnRef($extnRef) {
		$this->extnRef  = $extnRef ;
	}
	
	/**
	 * @name DcplIntra::_setLib()
	 * @param $lib (String)
	 * @return void
	 */
	public function _setLib($lib) {
		$this->lib  = $lib ;
	}

	/**
	 * @name DcplIntra::_setDsc()
	 * @param $dsc (String)
	 * @return void
	 */
	public function _setDsc($dsc) {
		$this->dsc  = $dsc ;
	}

	/**
	 * @name DcplIntra::_setChaineType()
	 * @param $chaineType (String)
	 * @return void
	 */
	public function _setChaineType($chaineType) {
		$this->chaineType  = $chaineType ;
	}

	/**
	 * @name DcplIntra::_setIdRegr()
	 * @param $idRegr (Int)
	 * @return void
	 */
	public function _setIdRegr($idRegr) {
		$this->idRegr  = $idRegr ;
	}

	/**
	 * @name DcplIntra::_setIdStruct()
	 * @param $idStruct (Int)
	 * @return void
	 */
	public function _setIdStruct($idStruct) {
		$this->idStruct  = $idStruct ;
	}



	/**
	 * Compte du nombre d'occurence de l'objet DcplIntra dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name DcplIntra::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_dcpl_intra ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * renvoit l'id d'une dcpl_intra sur le libell�
	 *
	 * <p>_existsRef</p>
	 *
	 * @name DcplIntra::_existsRef()
	 * @param $stampStart string
	 * @return int
	 */

	public function _existsRef($lib,$strict=0)
	{
		$idRef = 0 ;
		$listArray=array();
		if (trim($lib)=="") return $listArray;

		$maconnexion = MysqlDatabase::GetInstance() ;

		if ($strict)
		{
			$sql = 'SELECT id ';
			$sql .= ' FROM t_dcpl_intra ';
			$sql .= ' WHERE UPPER(lib) = \''.strtoupper($lib).'\' ';

			try{
				$res = $maconnexion->_bddQuery($sql) ;
				$row = $maconnexion->_bddFetchAssoc($res);
				$idRef  = $row['id'] ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			return $idRef;
		}
		else
		{
			// recherche de concordance entre libell� et url_type
			$sql = 'SELECT * ';
			$sql .= ' FROM t_dcpl_intra ';
			$sql .= ' WHERE CHAINE_TYPE <>\'\' ';
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

			if($maconnexion->_bddNumRows($res))
			{
				while($row = $maconnexion->_bddFetchAssoc($res))
				{
					$row=keyToUpper($row);
					if ($debug) echo "\r\ncomparatif [$lib] et ['".$row['CHAINE_TYPE']."']";
					//if (strstr($lib,$row['CHAINE_TYPE']))
					if (preg_match($row['CHAINE_TYPE'],$lib))
					$listArray[]=$row['ID'];
				}
			}
			return $listArray;
		}
	}

	/**
	 * Cr�ation d'un DcplIntra dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name DcplIntra::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM t_dcpl_intra ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}

		$sql  = 'INSERT INTO t_dcpl_intra VALUES( ';
		$sql .= '\''.AddSlashes($this->id).'\', ';
		$sql .= '\''.AddSlashes($this->extnRef).'\', ';
		$sql .= '\''.AddSlashes($this->lib).'\', ';
		$sql .= '\''.AddSlashes($this->dsc).'\', ';
		$sql .= '\''.AddSlashes($this->chaineType).'\', ';
		$sql .= '\''.AddSlashes($this->idRegr).'\', ';
		$sql .= '\''.AddSlashes($this->idStruct).'\' ';

		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise � jour d'un DcplIntra dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name DcplIntra::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_dcpl_intra SET ';
		$sql .= 'ID = \''.AddSlashes($this->id).'\', ';
		$sql .= 'EXTN_REF = \''.AddSlashes($this->extnRef).'\', ';
		$sql .= 'LIB = \''.AddSlashes($this->lib).'\', ';
		$sql .= 'DSC = \''.AddSlashes($this->dsc).'\', ';
		$sql .= 'CHAINE_TYPE = \''.AddSlashes($this->chaineType).'\', ';
		$sql .= 'ID_REGR = \''.AddSlashes($this->idRegr).'\', ';
		$sql .= 'ID_STRUCT = \''.AddSlashes($this->idStruct).'\' ';

		$sql .= 'WHERE id=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un DcplIntra dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name DcplIntra::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'DELETE FROM t_dcpl_intra ';
		$sql .= 'WHERE id = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}
	/**
	 * R�cup�ration de la liste des DcplIntras
	 *
	 * <p>Liste des DcplIntra</p>
	 *
	 * @name DcplIntra::_getList()
	 * @return array
	 */
	public function _getList($idRegr=0)
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_dcpl_intra WHERE 1 ' ;
		//v1.5-PPR-#000 suppression message notice
		if ((isset($this->id)) and ($this->id>0))
		$sql.= ' and id = \''.$this->id.'\' ';
		if ($idRegr>0)
		$sql.= ' and id_regr = \''.$idRegr.'\' ';
		
		$sql .= 'ORDER BY LIB ' ;

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['ID'] = StripSlashes($row['ID']) ;
				$listArray[$row['ID']]['EXTN_REF'] = StripSlashes($row['EXTN_REF']) ;
				$listArray[$row['ID']]['LIB'] = StripSlashes($row['LIB']) ;
				$listArray[$row['ID']]['DSC'] = StripSlashes($row['DSC']) ;
				$listArray[$row['ID']]['CHAINE_TYPE'] = StripSlashes($row['CHAINE_TYPE']) ;
				$listArray[$row['ID']]['ID_REGR'] = StripSlashes($row['ID_REGR']) ;
				$listArray[$row['ID']]['ID_STRUCT'] = StripSlashes($row['ID_STRUCT']) ;
				$this->comboList[$row['ID']] = StripSlashes($row['LIB']) ;
				
			}
		}
		return $listArray ;
	}

	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name DcplIntra:_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id==0) return false;

		$listArray=$this->_getList();
		$this->_setExtnRef($listArray[$this->id]['EXTN_REF']);
		$this->_setLib($listArray[$this->id]['LIB']);
		$this->_setDsc($listArray[$this->id]['DSC']);
		$this->_setChaineType($listArray[$this->id]["CHAINE_TYPE"]);
		$this->_setIdRegr($listArray[$this->id]["ID_REGR"]);
		$this->_setIdStruct($listArray[$this->id]["ID_STRUCT"]);
	}

	/**
	 * Récupération de la liste des Alertes
	 *
	 * <p>Liste des Alertes</p>
	 *
	 * @name DcplIntra::_getAlerts()
	 * @return array
	 */
	public function _getAlerts($typeAlert=1)
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_dcpl_intra WHERE 1 ' ;
		if ($typeAlert==1)
			$sql.= ' AND id_regr = 0 ';
		if ($typeAlert==2)
			$sql.= ' AND lib like  \'%~%\' ';
		$sql .= 'ORDER BY id desc ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]=$row['LIB'];				
			}
		}
		return $listArray ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name DcplIntra::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>