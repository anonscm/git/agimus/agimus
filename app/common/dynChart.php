<?php

/**
 *
 * <p>Chart dynamique</p>
 *
 * @name dynChart
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class DynChart {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietes    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc id Chart
	 */
	public $dynChartId;
	/**
	 * @var (String)
	 * @desc titre Chart
	 */
	public $dynChartLib;
	/**
	 * @var (Int)
	 * @desc Type Chart
	 */
	public $dynChartType;

	/**
	 * @var (array)
	 * @desc Valeurs Chart
	 */
	public $dynChartValues;

	/**
	 * @var (String)
	 * @desc fichier cible Chart
	 */
	public $dynChartOutFile;

	/**
	 * @var (Int)
	 * @desc Largeur
	 */
	public $dynChartWidth;

	/**
	 * @var (Int)
	 * @desc Largeur
	 */
	public $dynChartHeight;

	/**
	 * @var (Int)
	 * @desc nombre de valeurs maximum avant agregation
	 */
	public $maxValuesBeforeAgregate;

	/**
	 * @var (Bool)
	 * @desc Border oui/non
	 */
	public $border;

	public $Colors;

	public $cumColors;

	public $axisLibs;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name dynChart:__construct()
	 * @return void
	 */
	public function __construct($dynChartId=0,$width=400,$height=300) {

		$this->dynChartId = $dynChartId ;
		$this->dynChartLib='';
		$this->dynChartType=1; // par defaut : courbe
		$this->dynChartValues=array();
		$this->dynChartOutFile=ROOT_OUT.'temp_0_chart.gif';
		$this->dynChartWidth=$width;
		$this->dynChartHeight=$height;
		$this->border=false;
		
		$this->axisLibs=array();
		
		//v1.3-PPR-24052011 paramètre réintégré dans param.php
	}

	/**
	 * Accesseurs en lecture
	 */
	/**
	 * @name dynChart::_getDynChartId()
	 * @return int
	 */
	public function _getDynChartId()
	{
		return $this->dynChartId ;
	}
	/**
	 * @name dynChart::_getDynChartLib()
	 * @return string
	 */
	public function _getDynChartLib()
	{
		return $this->dynChartLib ;
	}
	/**
	 * @name dynChart::_getDynChartType()
	 * @return int
	 */
	public function _getDynChartType()
	{
		return $this->dynChartType ;
	}
	/**
	 * @name dynChart::_getDynChartValues()
	 * @return array
	 */
	public function _getDynChartValues()
	{
		return $this->dynChartValues ;
	}
	/**
	 * @name dynChart::_getDynChartOutFile()
	 * @return string
	 */
	public function _getDynChartOutFile()
	{
		return $this->dynChartOutFile ;
	}

	/**
	 * Accesseurs en �criture
	 */

	/**
	 * @name dynChart::_setDynChartId()
	 * @param $dynChartId (int)
	 * @return void
	 */
	public function _setDynChartId($dynChartId)
	{
		$this->dynChartId = $dynChartId ;
	}
	/**
	 * @name dynChart::_setDynChartLib()
	 * @param $dynChartLib (string)
	 * @return void
	 */
	public function _setDynChartLib($dynChartLib)
	{
		$this->dynChartLib = $dynChartLib ;
	}
	/**
	 * @name dynChart::_setDynChartType()
	 * @param $dynChartType (int)
	 * @return void
	 */
	public function _setDynChartType($dynChartType)
	{
		$this->dynChartType = $dynChartType ;
	}
	/**
	 * @name dynChart::_setDynChartValues()
	 * @param $dynChartValues (array)
	 * @return void
	 */
	public function _setDynChartValues($dynChartValues)
	{
		$this->dynChartValues = $dynChartValues ;
	}
	/**
	 * @name dynChart::_setDynChartOutFile()
	 * @param $dynChartOutFile (string)
	 * @return void
	 */
	public function _setDynChartOutFile($dynChartOutFile)
	{
		$this->dynChartOutFile = $dynChartOutFile ;
	}
	/**
	 * @name dynChart::_setDynChartWidth()
	 * @param $dynChartWidth (int)
	 * @return void
	 */
	public function _setDynChartWidth($dynChartWidth)
	{
		$this->dynChartWidth = $dynChartWidth ;
	}

	/**
	 * @name dynChart::_setDynChartHeight()
	 * @param $dynChartHeight (int)
	 * @return void
	 */
	public function _setDynChartHeight($dynChartHeight)
	{
		$this->dynChartHeight = $dynChartHeight ;
	}
	/**
	 * @name dynChart::_setMaxValuesBeforeAgregate()
	 * @param $maxValues (int)
	 * @return void
	 */
	public function _setMaxValuesBeforeAgregate($maxValues)
	{
		$this->maxValuesBeforeAgregate = $maxValues ;
	}

	/**
	 * @name dynChart::_setBorder()
	 * @param $border (bool)
	 * @return void
	 */
	public function _setBorder($bool)
	{
		$this->border = $bool ;
	}

	/**
	 * @name dynChart::_setAxisLibs()
	 * @param $libs (array)
	 * @return void
	 */
	public function _setAxisLibs($libs)
	{
		$this->axisLibs = $libs ;
	}

	

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name DynChart::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>