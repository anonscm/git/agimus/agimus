<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * <p>Discipline</p>
 *
 * @name  Discipline
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

class Discipline {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc Identifiant
	 */
	private $id;

	/**
	 * @var (String)
	 * @desc Reference externe
	 */
	private $extnRef;
	
	/**
	 * @var (String)
	 * @desc Libelle
	 */
	private $lib;

	/**
	 * @var (String)
	 * @desc Decription
	 */
	private $dsc;

	/**
	 * @var (String)
	 * @desc Id Regroupement
	 */
	private $idRegr;

	/**
	 * @var (Array)
	 * @desc tableau identifiant=>libell� */
	private $comboList;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Discipline::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		$this->id=0;
		$this->extnRef='';
		$this->lib='';
		$this->dsc='';
		$this->idRegr='';

		$this->comboList = array() ;
		
		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}

	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name Discipline::_getId()
	 * @return Int
	 */
	public function _getId() {
		return $this->id ;
	}

		/**
	 * @name Discipline::_getExtnRef()
	 * @return string
	 */
	public function _getExtnRef() {
		return $this->extnRef ;
	}
	
	/**
	 * @name Discipline::_getLib()
	 * @return String
	 */
	public function _getLib() {
		return $this->lib ;
	}

	/**
	 * @name Discipline::_getDsc()
	 * @return String
	 */
	public function _getDsc() {
		return $this->dsc ;
	}

	/**
	 * @name Discipline::_getIdRegr()
	 * @return Int
	 */
	public function _getIdRegr() {
		return $this->idRegr ;
	}

	/**
	 * @name CatSrv::_getComboList()
	 * @return array
	 */
	public function _getComboList()
	{
		return $this->comboList ;
	}
	

	/**
	 * Accesseurs en �criture
	 */

	/**
	 * @name Discipline::_setId()
	 * @param $id (Int)
	 * @return void
	 */
	public function _setId($id) {
		$this->id  = $id ;
	}

	/**
	 * @name Discipline::_setExtnRef()
	 * @param $extnRef (String)
	 * @return void
	 */
	public function _setExtnRef($extnRef) {
		$this->extnRef  = $extnRef ;
	}
	
	/**
	 * @name Discipline::_setLib()
	 * @param $lib (String)
	 * @return void
	 */
	public function _setLib($lib) {
		$this->lib  = $lib ;
	}

	/**
	 * @name Discipline::_setDsc()
	 * @param $dsc (String)
	 * @return void
	 */
	public function _setDsc($dsc) {
		$this->dsc  = $dsc ;
	}

	/**
	 * @name Discipline::_setIdRegr()
	 * @param $idRegr (Int)
	 * @return void
	 */
	public function _setIdRegr($idRegr) {
		$this->idRegr  = $idRegr ;
	}



	/**
	 * Compte du nombre d'occurence de l'objet Discipline dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name Discipline::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_discipline ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * renvoit l'id d'un stamp existant
	 *
	 * <p>_existsRef</p>
	 *
	 * @name Discipline::_existsRef()
	 * @param $lib string
	 * @return int
	 */
	public function _existsRef($lib)
	{
		$idRef = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT id ';
		$sql .= ' FROM t_discipline ';
		$sql .= ' WHERE UPPER(lib) = \''.strtoupper($lib).'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$idRef  = $row['id'] ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		return $idRef;
	}

	/**
	 * Cr�ation d'un Discipline dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name Discipline::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM t_discipline ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
			
		$sql  = 'INSERT INTO t_discipline VALUES( ';
		$sql .= '\''.AddSlashes($this->id).'\', ';
		$sql .= '\''.AddSlashes($this->extnRef).'\', ';
		$sql .= '\''.AddSlashes($this->lib).'\', ';
		$sql .= '\''.AddSlashes($this->dsc).'\', ';
		$sql .= '\''.AddSlashes($this->idRegr).'\' ';
		
		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise � jour d'un Discipline dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name Discipline::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_discipline SET ';
		$sql .= 'ID = \''.AddSlashes($this->id).'\', ';
		$sql .= 'EXTN_REF = \''.AddSlashes($this->extnRef).'\', ';
		$sql .= 'LIB = \''.AddSlashes($this->lib).'\', ';
		$sql .= 'DSC = \''.AddSlashes($this->dsc).'\', ';
		$sql .= 'ID_REGR = \''.AddSlashes($this->idRegr).'\' ';

		$sql .= 'WHERE id=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un Discipline dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name Discipline::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'DELETE FROM t_discipline ';
		$sql .= 'WHERE id = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}
	/**
	 * R�cup�ration de la liste des Disciplines
	 *
	 * <p>Liste des Discipline</p>
	 *
	 * @name Discipline::_getList()
	 * @return array
	 */
	public function _getList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_discipline ' ;
		if ($this->id>0)
		$sql.= ' WHERE id = \''.$this->id.'\' ';
		$sql .= 'ORDER BY LIB ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['ID'] = StripSlashes($row['ID']) ;
				$listArray[$row['ID']]['EXTN_REF'] = StripSlashes($row['EXTN_REF']) ;
				$listArray[$row['ID']]['LIB'] = StripSlashes($row['LIB']) ;
				$listArray[$row['ID']]['DSC'] = StripSlashes($row['DSC']) ;
				$listArray[$row['ID']]['ID_REGR'] = StripSlashes($row['ID_REGR']) ;
				$this->comboList[$row['ID']] = StripSlashes($row['LIB']) ;
				
			}
		}
		return $listArray ;
	}


	
	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name Discipline::_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id==0) return false;

		$listArray=$this->_getList();
		$this->id = $listArray[$this->id]['ID'];
		$this->extnRef=$listArray[$this->id]['EXTN_REF'];
		$this->lib = $listArray[$this->id]['LIB'];
		$this->dsc = $listArray[$this->id]['DSC'];
		$this->idRegr = $listArray[$this->id]['ID_REGR'];

	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Discipline::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>