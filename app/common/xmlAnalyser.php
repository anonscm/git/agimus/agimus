<?php
/**
 * <p>Analyse des fichiers xml</p>
 * 
 * @name XmlAnalyser
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright Capella Conseil 2009
 * @version 1.0.0
 * @package common
 */

class XmlAnalyser{
	/*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/
 	/**
    * @var (Singleton)
    * @desc variable pour le singleton
    */
 	private static $instance;   
    /**
    * @var $parser (String)
    * @desc r�f�rence de l'analyseur xml
    */
 	private $parser ;
 	
 	   
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name XmlAnalyser::__construct()
    * @return void 
    */
    public function __construct(){
    	//$this->parser = xml_parser_create("");
		//xml_parser_set_option($this->parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
    }
    
    /**
    * Singleton
    * 
    * <p>cr�ation de l'instance de la classe si n'existe pas</p>
    * 
    * @name XmlAnalyser::_GetInstance()
    * @return XmlAnalyser
    */
	public static function _GetInstance()
	{
		if(!isset(self::$instance))
		{
			self::$instance = new XmlAnalyser();
		}
		return self::$instance;
	}  
    
    /**
    * R�cup�ration de donn�es stock�es dans un fichier xml
    * On r�cup�re et on lit le fichier xml
    * On cr�e le parseur xml et on parcourt le document pour le transformer en tableau de donn�es
    * 
    * <p>Obtenir les informations de configuration</p>
    * 
    * @name XmlAnalyser::_getDataXmlInArray()
    * @param $xmlfile (string)
    * @return array 
    */    
	public function _getDataXmlInArray($xmlfile)
    {
    	$dataArray = array() ;
		$xml_doc = $xmlfile;
		$handle = fopen ($xml_doc, "r");
		$datastring = fread ($handle, filesize($xml_doc));
		fclose ($handle);

		$xml_parser = xml_parser_create();
		xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING,0);
		xml_parse_into_struct($xml_parser, $datastring, $valueArray, $indexArray);
		xml_parser_free($xml_parser);

		for ($i=0; $i < count($valueArray); $i++){
			if (isset($valueArray[$i]["value"]))
			$dataArray[$valueArray[$i]["tag"]] = $valueArray[$i]["value"];
		}
		return $dataArray ;
    }
    
    /**
    * Destructeur
    * 
    * <p>Destruction de l'instance de classe</p>
    * 
    * @name XmlAnalyser::__destruct()
    * @return void
    */
    public function __destruct() {
    }
}
?>