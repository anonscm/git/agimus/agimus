<?php
/**
 * 
 * <p>rights</p>
 * 
 * @name rights
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */
 
 interface rights {
 

    /*~*~*~*~*~*~*~*~*~*~*/
    /*     m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
   // public function _openRight($modID, $rightID, $UID=0, $GID=0);
   // public function _closeRight($modID, $rightID, $UID=0, $GID=0) ;
    public function _isAllowed($rightID, $ID) ;
    
 } 
?>