<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/hdate.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/ldap.php') ;

/**
 *
 * <p>TraceLog</p>
 *
 * @name  TraceLog
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

// v1.5-PPR-#016 ajout dimensions 5 à 9 + SupannOrganisme

class TraceLog {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietes    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc Identifiant
	 */
	private $id;

	/**
	 * @var (String)
	 * @desc unique cookie id
	 */
	private $cookieId;

	/**
	 * @var (String)
	 * @desc unique user id
	 */
	private $uid;


	/**
	 * @var (String)
	 * @desc Stamp d'integration AGIMUS
	 */
	private $stampProcessed;

	/**
	 * @var (String)
	 * @desc Dimension 1 LDAP
	 */
	private $dim1;

	/**
	 * @var (String)
	 * @desc Dimension 2 LDAP
	 */
	private $dim2;

	/**
	 * @var (String)
	 * @desc Dimension 3 LDAP
	 */
	private $dim3;

	/**
	 * @var (String)
	 * @desc Dimension 4 LDAP
	 */
	private $dim4;

	/**
	 * @var (String)
	 * @desc Dimension 5 LDAP
	 */
	private $dim5;

	/**
	 * @var (String)
	 * @desc Dimension 6 LDAP
	 */
	private $dim6;

	/**
	 * @var (String)
	 * @desc Dimension 7 LDAP
	 */
	private $dim7;

	/**
	 * @var (String)
	 * @desc Dimension 8 LDAP
	 */
	private $dim8;

	/**
	 * @var (String)
	 * @desc Dimension 9 LDAP
	 */
	private $dim9;

	/**
	 * @var (String)
	 * @desc Supann Organisme
	 */
	private $organisme;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. methodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>creation de l'instance de la classe</p>
	 *
	 * @name traceLog::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		$this->id=0;
		$this->cookieId='';
		$this->uid='';
		$this->stampProcessed='';
		$this->dim1='';
		$this->dim2='';
		$this->dim3='';
		$this->dim4='';
		$this->dim5='';
		$this->dim6='';
		$this->dim7='';
		$this->dim8='';
		$this->dim9='';
		$this->organisme='';

		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}
	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name traceLog::_getId()
	 * @return Int
	 */
	public function _getId() {
		return $this->id ;
	}

	/**
	 * @name traceLog::_getCookieId()
	 * @return String
	 */
	public function _getCookieId() {
		return $this->cookieId ;
	}


	/**
	 * @name traceLog::_getUid()
	 * @return String
	 */
	public function _getUid() {
		return $this->uid ;
	}

	/**
	 * @name traceLog::_getStampProcessed()
	 * @return String
	 */
	public function _getStampProcessed() {
		return $this->stampProcessed ;
	}

	/**
	 * @name traceLog::_getDim1()
	 * @return String
	 */
	public function _getDim1() {
		return $this->dim1;
	}

	/**
	 * @name traceLog::_getDim2()
	 * @return String
	 */
	public function _getDim2() {
		return $this->dim2;
	}

	/**
	 * @name traceLog::_getDim3()
	 * @return String
	 */
	public function _getDim3() {
		return $this->dim3;
	}

	/**
	 * @name traceLog::_getDim4()
	 * @return String
	 */
	public function _getDim4() {
		return $this->dim4;
	}

	/**
	 * @name traceLog::_getDim5()
	 * @return String
	 */
	public function _getDim5() {
		return $this->dim5;
	}

	/**
	 * @name traceLog::_getDim6()
	 * @return String
	 */
	public function _getDim6() {
		return $this->dim6;
	}

	/**
	 * @name traceLog::_getDim7()
	 * @return String
	 */
	public function _getDim7() {
		return $this->dim7;
	}

	/**
	 * @name traceLog::_getDim8()
	 * @return String
	 */
	public function _getDim8() {
		return $this->dim8;
	}

	/**
	 * @name traceLog::_getDim9()
	 * @return String
	 */
	public function _getDim9() {
		return $this->dim9;
	}

	/**
	 * @name traceLog::_getOrganisme()
	 * @return String
	 */
	public function _getOrganisme() {
		return $this->organisme;
	}



	/**
	 * Accesseurs en ecriture
	 */

	/**
	 * @name traceLog::_setId()
	 * @param $id (Int)
	 * @return void
	 */
	public function _setId($id) {
		$this->id  = $id ;
	}

	/**
	 * @name traceLog::_setCookieId()
	 * @param $cookieId (String)
	 * @return void
	 */
	public function _setCookieId($cookieId) {
		$this->cookieId  = $cookieId ;
	}

	/**
	 * @name traceLog::_setUid()
	 * @param $uid (String)
	 * @return void
	 */
	public function _setUid($uid) {
		$this->uid  = $uid ;
	}

	/**
	 * @name traceLog::_setStampProcessed()
	 * @param $stampProcessed (String)
	 * @return void
	 */
	public function _setStampProcessed($stampProcessed) {
		$this->stampProcessed  = $stampProcessed ;
	}

	/**
	 * @name traceLog::_setDim1()
	 * @param $dim1 (String)
	 * @return void
	 */
	public function _setDim1($dim1) {
		$this->dim1  = $dim1 ;
	}

	/**
	 * @name traceLog::_setDim2()
	 * @param $dim2 (String)
	 * @return void
	 */
	public function _setDim2($dim2) {
		$this->dim2  = $dim2 ;
	}

	/**
	 * @name traceLog::_setDim3()
	 * @param $dim3 (String)
	 * @return void
	 */
	public function _setDim3($dim3) {
		$this->dim3  = $dim3 ;
	}

	/**
	 * @name traceLog::_setDim4()
	 * @param $dim4 (String)
	 * @return void
	 */
	public function _setDim4($dim4) {
		$this->dim4  = $dim4 ;
	}

	/**
	 * @name traceLog::_setDim5()
	 * @param $dim5 (String)
	 * @return void
	 */
	public function _setDim5($dim5) {
		$this->dim5 = $dim5 ;
	}

	/**
	 * @name traceLog::_setDim6()
	 * @param $dim6 (String)
	 * @return void
	 */
	public function _setDim6($dim6) {
		$this->dim6 = $dim6 ;
	}

	/**
	 * @name traceLog::_setDim7()
	 * @param $dim7 (String)
	 * @return void
	 */
	public function _setDim7($dim7) {
		$this->dim7  = $dim7 ;
	}

	/**
	 * @name traceLog::_setDim8()
	 * @param $dim8 (String)
	 * @return void
	 */
	public function _setDim8($dim8) {
		$this->dim8  = $dim8 ;
	}

	/**
	 * @name traceLog::_setDim9()
	 * @param $dim9 (String)
	 * @return void
	 */
	public function _setDim9($dim9) {
		$this->dim9  = $dim9 ;
	}

	/**
	 * @name traceLog::_setOrganisme()
	 * @param $organisme (String)
	 * @return void
	 */
	public function _setOrganisme($organisme) {
		$this->organisme  = $organisme ;
	}

	/**
	 * Compte du nombre d'occurence de l'objet traceLog dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name traceLog::_countRef()
	 * @param $stampStart string
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_tracelog ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * renvoit l'id d'un cookie existant
	 *
	 * <p>_existsRef</p>
	 *
	 * @name TraceLog::_existsRef()
	 * @param $stampStart string
	 * @return int
	 */
	public function _existsRef($cookieId)
	{
		$idRef = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT id ';
		$sql .= ' FROM t_tracelog ';
		$sql .= ' WHERE COOKIE_ID = \''.$cookieId.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$idRef  = $row['id'] ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		return $idRef;
	}

	/**
	 * Creation d'un traceLog dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name traceLog::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM t_tracelog ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
			
		$sql  = 'INSERT INTO t_tracelog VALUES( ';
		$sql .= '\''.$this->id.'\', ';
		$sql .= '\''.AddSlashes($this->cookieId).'\', ';
		$sql .= '\''.AddSlashes($this->uid).'\', ';
		$sql .= '\''.AddSlashes($this->stampProcessed).'\', ';
		$sql .= '\''.AddSlashes($this->dim1).'\', ';
		$sql .= '\''.AddSlashes($this->dim2).'\', ';
		$sql .= '\''.AddSlashes($this->dim3).'\', ';
		$sql .= '\''.AddSlashes($this->dim4).'\', ';
		$sql .= '\''.AddSlashes($this->dim5).'\', ';
		$sql .= '\''.AddSlashes($this->dim6).'\', ';
		$sql .= '\''.AddSlashes($this->dim7).'\', ';
		$sql .= '\''.AddSlashes($this->dim8).'\', ';
		$sql .= '\''.AddSlashes($this->dim9).'\', ';
		$sql .= '\''.AddSlashes($this->organisme).'\' ';
		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise a jour d'un traceLog dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name traceLog::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_tracelog SET ';
		$sql .= 'ID = \''.$this->id.'\', ';
		$sql .= 'COOKIE_ID = \''.AddSlashes($this->cookieId).'\', ';
		$sql .= 'UID = \''.AddSlashes($this->uid).'\', ';
		$sql .= 'STAMP_PROCESSED = \''.$this->stampProcessed.'\', ';
		$sql .= 'DIM_1 = \''.AddSlashes($this->dim1).'\', ';
		$sql .= 'DIM_2 = \''.AddSlashes($this->dim2).'\', ';
		$sql .= 'DIM_3 = \''.AddSlashes($this->dim3).'\', ';
		$sql .= 'DIM_4 = \''.AddSlashes($this->dim4).'\', ';
		$sql .= 'DIM_5 = \''.AddSlashes($this->dim5).'\', ';
		$sql .= 'DIM_6 = \''.AddSlashes($this->dim6).'\', ';
		$sql .= 'DIM_7 = \''.AddSlashes($this->dim7).'\', ';
		$sql .= 'DIM_8 = \''.AddSlashes($this->dim8).'\', ';
		$sql .= 'DIM_9 = \''.AddSlashes($this->dim9).'\', ';
		$sql .= 'ORGANISME = \''.AddSlashes($this->organisme).'\' ';

		$sql .= 'WHERE ID=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un traceLog dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name traceLog::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql  = 'DELETE FROM t_tracelog ';
		$sql .= 'WHERE ID = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression complete dans la bdd
	 *
	 * <p>_deleteAll</p>
	 *
	 * @name traceLog::_deleteAll()
	 * @return void
	 */
	public function _deleteAll()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql  = 'TRUNCATE TABLE t_tracelog ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Recuperation de la liste des traceLogs
	 *
	 * <p>Liste des traceLog</p>
	 *
	 * @name traceLog::_getList()
	 * @return array
	 */
	public function _getList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_tracelog ' ;
		if ($this->id>0)
		$sql.= ' WHERE ID = \''.$this->id.'\' ';
		$sql .= 'ORDER BY ID ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['ID'] = StripSlashes($row['ID']) ;
				$listArray[$row['ID']]['COOKIE_ID'] = StripSlashes($row['COOKIE_ID']) ;
				$listArray[$row['ID']]['UID'] = StripSlashes($row['UID']) ;
				$listArray[$row['ID']]['STAMP_PROCESSED'] = StripSlashes($row['STAMP_PROCESSED']) ;
				$listArray[$row['ID']]['DIM_1'] = StripSlashes($row['DIM_1']) ;
				$listArray[$row['ID']]['DIM_2'] = StripSlashes($row['DIM_2']) ;
				$listArray[$row['ID']]['DIM_3'] = StripSlashes($row['DIM_3']) ;
				$listArray[$row['ID']]['DIM_4'] = StripSlashes($row['DIM_4']) ;
				$listArray[$row['ID']]['DIM_5'] = StripSlashes($row['DIM_5']) ;
				$listArray[$row['ID']]['DIM_6'] = StripSlashes($row['DIM_6']) ;
				$listArray[$row['ID']]['DIM_7'] = StripSlashes($row['DIM_7']) ;
				$listArray[$row['ID']]['DIM_8'] = StripSlashes($row['DIM_8']) ;
				$listArray[$row['ID']]['DIM_9'] = StripSlashes($row['DIM_9']) ;
				$listArray[$row['ID']]['ORGANISME'] = StripSlashes($row['ORGANISME']) ;
			}
		}
		return $listArray ;
	}

	/**
	 * Analyse du repertoire sas
	 *
	 * <p>Analyse du repertoire sas</p>
	 *
	 * @name traceLog::_analyseLogDir()
	 * @param $templateString (string)
	 * @param $separator (string)
	 * @return array
	 */
	public function _analyseLogDir($templateString="trace",$separator=":",$logDir=ROOT_IN) {

		$arLogFile=array();

		if (($logDir=="") or (!is_dir($logDir)))
		return false;

		if ($handle = opendir($logDir)) {

			/* parcours du sas et lecture fichier par fichier */
			while (false !== ($file= readdir($handle))) {

				if ($file!=="." and $file !== ".." ) {

					// controle que le nom du fichier correspond a la recherche
					if (strtoupper(substr($file,0,strlen($templateString)))==strtoupper($templateString)) {

						//TODO : provoque des erreurs si chemin vers linux
						chmod($logDir."/".$file,0755);

						$arLines=file($logDir."/".$file);
						$arLog=array();

						/* lecture et nettoyage du fichier d'entree (suppression des espaces inutiles) */
						foreach($arLines as $curLine)
						{
							$arFields=explode($separator,trim($curLine));
							$i=0;
							$arLogLine=array();
							foreach($arFields as $curField)
							{
								if (trim($curField)!="")
								$arLogLine[]=$curField;
								$i+=1;
							}
							$arLog[]=$arLogLine;
						}
						$arLogFile[$file]=$arLog;
					}
				}
			}
		}
		return $arLogFile;
	}

	/**
	 * Analyse du repertoire sas
	 *
	 * v1.2-PPR-23032011 intégration de l'accès LDAP on-the-fly (Pb CNIL)
	 * <p>Analyse du repertoire sas</p>
	 *
	 * @name traceLog::_analyseLog()
	 * @param $templateString (string)
	 * @param $separator (string)
	 * @return array
	 */
	public function _analyseLog($arLogFile,$pattern="TRACE") {
		global $_matchTraceLogFields, $_arMatchLogFields,$_arMatchLogFieldsDoubleQuoted, $_matchMimeTypes,$arMonthConv,
		$_matchLdapAttributes,$_attribValueMandatory;

		$nbrLigLus=0;
		$nbrLigWrite=0;
		$debug=false;
		$mess="";

		$config=$_SESSION['config'];
		
		//v1.2-PPR-23032011 ajout mot de passe interne pour cryptage uid
		$mdpAgimus="kdibdreyhkroncanppro";
		if (isset($config['mdp_agimus']))
			$mdpAgimus=$config['mdp_agimus'];
	
		$codeRet=0;
		$arLdapLus=array();

		// tableau des valeurs d'attribut récupérées (pour log et enrichissement)
		$arLdapValuesCatched=array();

		// tentative de connexion au LDAP

		$ldap=new Ldap();
		$ldap->_setUname($config['ldap_uname']);
		$ldap->_setPwd($config['ldap_pwd']);
		$ldap->_setLdapURL($config['ldap_url']);
		$ldap->_setBaseDN($config['ldap_base_dn']);

		if (!$ldap->_connect())
		{$mess.="\r\nImpossible de se connecter au serveur LDAP !";$codeRet=20;}
	
		if (!$codeRet)
		{
		// Chargement en mémoire des infos LDAP nécessaires
		$ldap->_setBaseDN($config['ldap_search_base_dn']);
		$ldap->_setFilter("uid=*");
		$ldap->_setAttributeArray(array_values($_matchLdapAttributes));
		$arResult=$ldap->_search();
		$mess.="\r\n\r\nChargement LDAP : ".ldap_count_entries($ldap->_getLdapCnx(),$ldap->_getLdapSearchId())." entrees collectees !\r\n";
		
		$arLdap=array();

		foreach($arResult as $entryId=>$curEntry)
		{
			$arAttrib=array();
			if (myIsInt($entryId))
			{
				$arAttrib[$_matchLdapAttributes['affiliation']]=$curEntry[$_matchLdapAttributes['affiliation']][0];
				$arAttrib[$_matchLdapAttributes['discipline']]=$curEntry[$_matchLdapAttributes['discipline']][0];
				$arAttrib[$_matchLdapAttributes['diplome']]=$curEntry[$_matchLdapAttributes['diplome']][0];
				$arAttrib[$_matchLdapAttributes['dometu']]=$curEntry[$_matchLdapAttributes['dometu']][0];
				//v1.5-PPR-#016 ajout dimensions et supannOrganisme
				$arAttrib[$_matchLdapAttributes['composante']]=$curEntry[$_matchLdapAttributes['composante']][0];
				$arAttrib[$_matchLdapAttributes['reginsc']]=$curEntry[$_matchLdapAttributes['reginsc']][0];

				$arAttrib[$_matchLdapAttributes['organisme']]=$curEntry[$_matchLdapAttributes['organisme']][0];
				
				$arLdap[$curEntry['uid'][0]]=$arAttrib;
			}
		}


		if (count($arLogFile)>0)
		{
			foreach($arLogFile as $curFile=>$curLogFile)
			{

				$mess.="\r\nTraitement du fichier ".$curFile." [".count($curLogFile)."] enr.";
				
				if (count($curLogFile)>0)
				{
					foreach($curLogFile as $curLogLine)
					{

						$nbrLigLus+=1;
						$uid=$curLogLine[$_matchTraceLogFields["UID"]];

						//v1.2-PPR-23032011 recup infos LDAP
						if (isset($arLdap[$uid])) {
							$affiliation=$arLdap[$uid][$_matchLdapAttributes['affiliation']];
							$discipline=$arLdap[$uid][$_matchLdapAttributes['discipline']];
							$diplome=$arLdap[$uid][$_matchLdapAttributes['diplome']];
							$dometu=$arLdap[$uid][$_matchLdapAttributes['dometu']];
//v1.5-PPR-#016 ajout dimensions et supannOrganisme
							$composante=$arLdap[$uid][$_matchLdapAttributes['composante']];
							$reginsc=$arLdap[$uid][$_matchLdapAttributes['reginsc']];
							$organisme=$arLdap[$uid][$_matchLdapAttributes['organisme']];
						}
						else
						{
							$mess.= "\r\n ligne $nbrLigLus : UID non trouvé dans le tableau LDAP ! ";
							continue;
						}

						//MATCHING :
						//controle que le cookie n'a pas deja ete traite
						$cookieId=$curLogLine[$_matchTraceLogFields["COOKIE_ID"]];
						
						if ($debug) echo "\r\nligne $nbrLigLus : $uid [$cookieId] ";
						
						//contrôle si le pattern correspond
						$subCookie=substr($cookieId,0,strlen($pattern));

						if (($pattern=="") or (($pattern!="") and ($subCookie==$pattern)))
						{
							$idRef=$this->_existsRef($cookieId);
							//si le cookie existe deja dans la base : on supprime
							if ($idRef==0)
							{
								//v1.2-PPR-23032011 cryptage sha1 avec ajout mdp Agimus paramétré
								$uidCrypt=sha1(substr($mdpAgimus,0,5).$uid.substr($mdpAgimus,5,99));

								$this->_setCookieId($cookieId);
								$this->_setUid($uidCrypt);
								$this->_setDim1($affiliation);
								$this->_setDim2($discipline);
								$this->_setDim3($diplome);
								$this->_setDim4($dometu);
								$this->_setDim5($composante);
								$this->_setDim6($reginsc);
								//v1.5-PPR-#016 ajouter futures dimensions ci-dessous
								// ex : -this->_setDim7($dimension7)
								
								$this->_setOrganisme($organisme);
								$this->_create();
								$nbrLigWrite+=1;
							}
						}
					}
				}
			}
		}
		//v1.3-PPR alimentation du tracelog avec les données LDAP même si pas de tracelogs (esup)
		else
		{
			$this->_deleteAll();
			
			if ($arLdap != NULL) { 
				foreach($arLdap as $key=>$value) {

								
					$affiliation=$arLdap[$key][$_matchLdapAttributes['affiliation']];
					$discipline=$arLdap[$key][$_matchLdapAttributes['discipline']];
					$diplome=$arLdap[$key][$_matchLdapAttributes['diplome']];
					$dometu=$arLdap[$key][$_matchLdapAttributes['dometu']];
					$composante=$arLdap[$key][$_matchLdapAttributes['composante']];
					$reginsc=$arLdap[$key][$_matchLdapAttributes['reginsc']];
					$organisme=$arLdap[$key][$_matchLdapAttributes['organisme']];

					$this->_setCookieId($key);
					$this->_setUid($key);
					$this->_setDim1($affiliation);
					$this->_setDim2($discipline);
					$this->_setDim3($diplome);
					$this->_setDim4($dometu);
					$this->_setDim5($composante);
					$this->_setDim6($reginsc);
					//v1.5-PPR-#016 ajouter futures dimensions ci-dessous
					// ex : -this->_setDim7($dimension7)
					
					$this->_setOrganisme($organisme);
					$this->_create();
					$nbrLigWrite+=1;
				}	
			}		
			$mess.="\r\nAlimentation de la base par le LDAP effectuée ($nbrLigWrite enregistrements ecrits) !";	
		}
				// deconnection de l'annuaire LDAP
		$ldap->_disconnect();
	}
		
		return array("lu"=>$nbrLigLus,"ecrit"=>$nbrLigWrite,"mess"=>$mess);
}

	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name TraceLog::_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id==0) return false;

		$listArray=$this->_getList();
		$this->_setCookieId($listArray[$this->id]['COOKIE_ID']);
		$this->_setUid($listArray[$this->id]['UID']);
		$this->_setStampProcessed($listArray[$this->id]['STAMP_PROCESSED']);
		$this->_setDim1($listArray[$this->id]['DIM_1']);
		$this->_setDim2($listArray[$this->id]['DIM_2']);
		$this->_setDim3($listArray[$this->id]['DIM_3']);
		$this->_setDim4($listArray[$this->id]['DIM_4']);
		$this->_setDim5($listArray[$this->id]['DIM_5']);
		$this->_setDim6($listArray[$this->id]['DIM_6']);
		$this->_setDim7($listArray[$this->id]['DIM_7']);
		$this->_setDim8($listArray[$this->id]['DIM_8']);
		$this->_setDim9($listArray[$this->id]['DIM_9']);
		$this->_setOrganisme($listArray[$this->id]['ORGANISME']);
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Structure::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}

} ?>
