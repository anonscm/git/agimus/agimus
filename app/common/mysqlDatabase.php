<?php

require_once ('database.php') ;
require_once ('msgException.php') ;

/**
 * 
 * <p>Gestion de base de donn�es MySQL</p>
 * 
 * @name MysqlDatabase
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package common
 */
 
 class MysqlDatabase implements Database{
 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/

 	/**
    * @var (Singleton)
    * @desc variable pour le singleton
    */
 	private static $instance;
 	
    /**
    * @var (String)
    * @desc Nom/adresse du serveur
    */
    private $serverName;
    
        /**
    * @var (String)
    * @desc Nom de la base de donn�es
    */
    private $bddName;
    
    /**
    * @var (String)
    * @desc Nom de l'utilisateur mySQL
    */
    private $userName;
    
    /**
    * @var (String)
    * @desc Mot de passe de l'utilisateur mySQL
    */
    private $userPwd;
    
    /**
    * @var (String)
    * @desc identifiant de connexion MySQL
    */
    private $link;
    
    
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name MysqlDatabase::__construct()
    * @return void 
    */
    public function __construct() {
      $this->_getConnexionData();
      $this->link = '' ;
    } 
    #
/**
* SINGLETON
*/

public static function GetInstance()
{
	if(!isset(self::$instance))
	{
		self::$instance = new MysqlDatabase();
	}
	return self::$instance;
}
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /*  2.1 m�thodes priv�es   */
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Connecte � la base de donn�es mySQL
    * 
    * <p>Connexion � la base de donn�es mySQL</p>
    * 
    * @name MysqlDatabase::_bddConnect()
    * @param $serverName
    * @param $userName
    * @param $userPwd
    * @return void 
    */
    public function _bddConnect()
    {
      $link = @mysql_connect($this->serverName, $this->userName, $this->userPwd) ;
      if($link == FALSE)
      {
      	throw new Exception(mysql_error()) ;      	
      }else{
      	$this->link = $link ;
        $db = mysql_select_db($this->bddName, $this->link);       
        if (!$db) 
        {
        	$error = mysql_error($this->link) ;
         	throw new Exception($error) ;
        }
      }
    }
    
    /**
    * Ferme la connexion � la base de donn�es mySQL
    * 
    * <p>Fermeture de la connexion � la base de donn�es mySQL</p>
    * 
    * @name MysqlDatabase::_bddClose()
    * @param $link
    * @return void 
    */
    public function _bddClose()
    {
		mysql_close($this->link);
    }
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /*  2.1 m�thodes publiques */
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    
    public function _getBddName() 
    {
    	return $this->bddName;
    }
    public function _getLink() 
    {
    	return $this->link;
    }
    /**
    * Envoie une requ�te � la base de donn�es mySQL et r�cup�re le r�sultat dans un tableau
    * 
    * <p>Requ�te � la base de donn�es mySQL</p>
    * 
    * @name MysqlDatabase::_bddQuery()
    * @param $queryString
    * @return $result 
    */    
    public function _bddQuery($queryString)
    {
    	if($this->link == '')
    	{
    		try{
      			$this->_bddConnect() ; 
    		}
    		catch(Exception $e){
    			$msgString = $e ->getMessage() ;
    			throw new MsgException($msgString, 'database') ;
    		}
    	}
    	$result= mysql_query("SET NAMES UTF8;");
    	$result = mysql_query($queryString, $this->link) ;
	    if($result == FALSE){
	    //traitement du message d'erreur
	        throw new  MsgException($queryString.'<br>'.mysql_error($this->link), 'database')  ;
	    }else{
	        return $result ;
	    }
    } 

     /**
    * Envoie une requ�te d'insertion avec r�cup�ration du dernier identifiant pour les id auto_increment
    * 
    * <p>Requ�te � la base de donn�es mySQL</p>
    * 
    * @name MysqlDatabase::_bddQueryLastId()
    * @param $queryString
    * @return $result 
    */    
    public function _bddQueryLastId($queryString)
    {
        if($this->link == '')
        {
			try{
      			$this->_bddConnect() ; 
    		}
            catch(Exception $e){
    			$msgString = $e ->getMessage() ;
    			throw new MsgException($msgString, 'database') ;
    		}
        }   
    	$result= mysql_query("SET NAMES UTF8;");
    	$result=mysql_query($queryString, $this->link) ;
      	if($result == FALSE){
        	throw new  MsgException(mysql_error($this->link), 'database')  ;
      	}else{
        	$queryID = mysql_query('SELECT LAST_INSERT_ID()', $this->link) ;
        	$rowID = mysql_fetch_assoc($queryID) ;
        	$resultID = $rowID['LAST_INSERT_ID()'] ;        
        	return $resultID ;      
      	}
      	//$this->_bddClose() ;
    }
    /**
    * Retourne le r�sultat d'une requ�te sous forme de tableau associatif
    * 
    * <p>Requ�te � la base de donn�es mySQL</p>
    * 
    * @name MysqlDatabase::_bddFetchAssoc()
    * @param $result
    * @return $result 
    */
    public static function _bddFetchAssoc($result)
    {
      $row = mysql_fetch_assoc($result) ;
      return $row ;
    }
    
    /**
    * Retourne le nombre de ligne du r�sultat d'une requ�te de type select 
    * 
    * <p>Nombre de ligne du r�sultat d'une requ�te � la base de donn�es mySQL</p>
    * 
    * @name MysqlDatabase::_bddNumRows()
    * @param $result
    * @return $num_rows 
    */
    public static function _bddNumRows($result)
    {
      $num_rows = mysql_num_rows($result);
      return $num_rows ;
    }
    
    /**
    * Retourne si un champs existe ou non dans une table
    * 
    * <p>Existence d'un champs dans une table</p>
    * 
    * @name MysqlDatabase::_fieldExists()
    * @param $table (String)
    * @param $fieldname (String)
    * @return boolean 
    */
    public function _fieldExists($fieldname, $table)
    {
	    try{
	    	$field_array = array();	
	    	$result = $this->_bddQuery('SHOW COLUMNS FROM '.$table);
	    	while ($row = mysql_fetch_assoc($result)) {
	    		$field_array[] = $row['Field'] ;
	    	}
	    	if (!in_array($fieldname, $field_array))
			{
				return FALSE ;
			}else{
				return TRUE ;
			}
	    }
        catch(Exception $e){
    		$msgString = $e ->getMessage() ;
    		throw new MsgException($msgString, 'database') ;
    	}
    }
    
    private function _getConnexionData()
    { 
		$data = $_SESSION['config'] ;
    	$this->serverName = $data['db_host'] ;
      	$this->bddName = $data['db_name'] ;
      	$this->userName = $data['db_user'] ;
      	$this->userPwd = $data['db_pwd'] ;
    }

    /**
    * Test de requete
    * 
    * <p>test de requete</p>
    * 
    * @name className::_queryTest()
    * @param $queryString (String)
    * @return boolean 
    * 
    */
    public function _queryTest($queryString)
    {
    	if($this->link == '')
    	{
    		try{
      			$this->_bddConnect() ; 
    		}
    		catch(Exception $e){
    			$msgString = $e ->getMessage() ;
    			throw new MsgException($msgString, 'database') ;
    		}
    	}
      	$result = mysql_query($queryString, $this->link) ;
      	return $result ;
    	
    }
    
    /**
    * Destructeur
    * 
    * <p>Destruction de l'instance de classe</p>
    * 
    * @name className::__destruct()
    * @return void
    */
    public function __destruct() {
    	if($this->link != '')
    	{
    		$this->_bddClose() ;
    	}
    }
 } 
?>