<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/hdate.php') ;
/**
 *
 * <p>CnxLog</p>
 *
 * @name  CnxLog
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

class CnxLog {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc Identifiant
	 */
	private $id;

	/**
	 * @var (String)
	 * @desc unique user id
	 */
	private $uid;

	/**
	 * @var (String)
	 * @desc Stamp unix de debut de connexion
	 */
	private $stampStart;

	/**
	 * @var (Int)
	 * @desc Volume du flux
	 */
	private $flowSize;

	/**
	 * @var (float)
	 * @desc Dur�e du flux
	 */
	private $respTime;

	/**
	 * @var (String)
	 * @desc URL cible
	 */
	private $targetUrl;

	/**
	 * @var (String)
	 * @desc type Mime
	 */
	private $mimeType;

	/**
	 * @var (String)
	 * @desc Stamp d'int�gration AGIMUS
	 */
	private $stampProcessed;

	/**
	 * @var (String)
	 * @desc User Agent
	 */
	private $userAgent;


	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name cnxLog::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		$this->id=0;
		$this->uid='';
		$this->stampStart='';
		$this->flowSize=0;
		$this->respTime='';
		$this->targetUrl='';
		$this->mimeType='';
		$this->stampProcessed='';
		$this->userAgent='';

		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}
	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name cnxLog::_getId()
	 * @return Int
	 */
	public function _getId() {
		return $this->id ;
	}

	/**
	 * @name cnxLog::_getUid()
	 * @return String
	 */
	public function _getUid() {
		return $this->uid ;
	}

	/**
	 * @name cnxLog::_getStampStart()
	 * @return String
	 */
	public function _getStampStart() {
		return $this->stampStart ;
	}

	/**
	 * @name cnxLog::_getFlowSize()
	 * @return Int
	 */
	public function _getFlowSize() {
		return $this->flowSize ;
	}

	/**
	 * @name cnxLog::_getRespTime()
	 * @return float
	 */
	public function _getRespTime() {
		return $this->respTime ;
	}

	/**
	 * @name cnxLog::_getTargetUrl()
	 * @return String
	 */
	public function _getTargetUrl() {
		return $this->targetUrl ;
	}

	/**
	 * @name cnxLog::_getMimeType()
	 * @return String
	 */
	public function _getMimeType() {
		return $this->mimeType ;
	}

	/**
	 * @name cnxLog::_getStampProcessed()
	 * @return String
	 */
	public function _getStampProcessed() {
		return $this->stampProcessed ;
	}

	/**
	 * @name Agregat::_getUserAgent()
	 * @return string
	 */
	public function _getUserAgent() {
		return $this->userAgent ;
	}


	/**
	 * Accesseurs en �criture
	 */

	/**
	 * @name cnxLog::_setId()
	 * @param $id (Int)
	 * @return void
	 */
	public function _setId($id) {
		$this->id  = $id ;
	}

	/**
	 * @name cnxLog::_setUid()
	 * @param $uid (String)
	 * @return void
	 */
	public function _setUid($uid) {
		$this->uid  = $uid ;
	}

	/**
	 * @name cnxLog::_setStampStart()
	 * @param $stampStart (String)
	 * @return void
	 */
	public function _setStampStart($stampStart) {
		$this->stampStart  = $stampStart ;
	}

	/**
	 * @name cnxLog::_setFlowSize()
	 * @param $flowSize (Int)
	 * @return void
	 */
	public function _setFlowSize($flowSize) {
		$this->flowSize  = $flowSize ;
	}

	/**
	 * @name cnxLog::_setRespTime()
	 * @param $RespTime (float)
	 * @return void
	 */
	public function _setRespTime($RespTime) {
		$this->respTime  = $RespTime ;
	}

	/**
	 * @name cnxLog::_setTargetUrl()
	 * @param $targetUrl (String)
	 * @return void
	 */
	public function _setTargetUrl($targetUrl) {
		$this->targetUrl  = $targetUrl ;
	}

	/**
	 * @name cnxLog::_setMimeType()
	 * @param $mimeType (String)
	 * @return void
	 */
	public function _setMimeType($mimeType) {
		$this->mimeType  = $mimeType ;
	}

	/**
	 * @name cnxLog::_setStampProcessed()
	 * @param $stampProcessed (String)
	 * @return void
	 */
	public function _setStampProcessed($stampProcessed) {
		$this->stampProcessed  = $stampProcessed ;
	}

	/**
	 * @name Agregat::_setUserAgent()
	 * @param $userAgent (string)
	 * @return void
	 */
	public function _setUserAgent($userAgent) {
		$this->userAgent  = $userAgent ;
	}


	/**
	 * Compte du nombre d'occurence de l'objet cnxLog dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name cnxLog::_countRef()
	 * @param $stampStart string
	 * @return int
	 */
	public function _countRef($stampStart="")
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		if ($stampStart!="")
		$sql .= ' AND STAMP_START = \''.$stampStart.'\' ';
		$sql .= 'FROM t_cnxlog ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Compte du nombre d'occurence restant a traiter
	 *
	 * <p>getNbToProcess</p>
	 *
	 * @name cnxLog::_getNbToProcess()
	 * @return int
	 */
	public static function _getNbToProcess()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_cnxlog ';
		$sql .= ' WHERE STAMP_PROCESSED = \'0000-00-00 00:00:00\' ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * renvoit l'id d'un stamp existant
	 *
	 * <p>_existsRef</p>
	 *
	 * @name CnxLog::_existsRef()
	 * @param $stampStart string
	 * @return int
	 */
	public function _existsRef($uid,$stampStart)
	{
		$idRef = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT id ';
		$sql .= ' FROM t_cnxlog ';
		$sql .= ' WHERE 1 ';
		$sql .= ' AND UID = \''.$uid.'\' ';
		$sql .= ' AND STAMP_START = \''.$stampStart.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$idRef  = $row['id'] ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		return $idRef;
	}

	/**
	 * Création d'un cnxLog dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name cnxLog::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM t_cnxlog ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
			
		$sql  = 'INSERT INTO t_cnxlog VALUES( ';
		$sql .= '\''.$this->id.'\', ';
		$sql .= '\''.AddSlashes($this->uid).'\', ';
		$sql .= '\''.AddSlashes($this->stampStart).'\', ';
		$sql .= '\''.AddSlashes($this->flowSize).'\', ';
		$sql .= '\''.AddSlashes($this->respTime).'\', ';
		$sql .= '\''.AddSlashes($this->targetUrl).'\', ';
		$sql .= '\''.AddSlashes($this->mimeType).'\', ';
		$sql .= '\''.AddSlashes($this->stampProcessed).'\', ';
		$sql .= '\''.AddSlashes($this->userAgent).'\' ';

		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise à jour d'un cnxLog dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name cnxLog::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_cnxlog SET ';
		$sql .= 'ID = \''.$this->id.'\', ';
		$sql .= 'UID = \''.AddSlashes($this->uid).'\', ';
		$sql .= 'STAMP_START = \''.AddSlashes($this->stampStart).'\', ';
		$sql .= 'FLOW_SIZE = \''.AddSlashes($this->flowSize).'\', ';
		$sql .= 'RESP_TIME = \''.AddSlashes($this->respTime).'\', ';
		$sql .= 'TARGET_URL = \''.AddSlashes($this->targetUrl).'\', ';
		$sql .= 'MIME_TYPE = \''.AddSlashes($this->mimeType).'\', ';
		$sql .= 'STAMP_PROCESSED = \''.$this->stampProcessed.'\', ';
		$sql .= 'USER_AGENT = \''.$this->userAgent.'\' ';

		$sql .= 'WHERE id=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un cnxLog dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name cnxLog::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql  = 'DELETE FROM t_cnxlog ';
		$sql .= 'WHERE id = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}
	/**
	 * Récupération de la liste des cnxLogs
	 *
	 * <p>Liste des cnxLog</p>
	 *
	 * @name cnxLog::_getList()
	 * @return array
	 */
	public function _getList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_cnxlog ' ;
		if ($this->id>0)
		$sql.= ' WHERE id = \''.$this->id.'\' ';
		$sql .= 'ORDER BY ID ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['ID'] = StripSlashes($row['ID']) ;
				$listArray[$row['ID']]['UID'] = StripSlashes($row['UID']) ;
				$listArray[$row['ID']]['STAMP_START'] = StripSlashes($row['STAMP_START']) ;
				$listArray[$row['ID']]['FLOW_SIZE'] = StripSlashes($row['FLOW_SIZE']) ;
				$listArray[$row['ID']]['RESP_TIME'] = StripSlashes($row['RESP_TIME']) ;
				$listArray[$row['ID']]['TARGET_URL'] = StripSlashes($row['TARGET_URL']) ;
				$listArray[$row['ID']]['MIME_TYPE'] = StripSlashes($row['MIME_TYPE']) ;
				$listArray[$row['ID']]['STAMP_PROCESSED'] = StripSlashes($row['STAMP_PROCESSED']) ;
				$listArray[$row['ID']]['USER_AGENT'] = StripSlashes($row['USER_AGENT']) ;

			}
		}
		return $listArray ;
	}

	/**
	 * Analyse du répertoire sas
	 *
	 * <p>Analyse du répertoire sas</p>
	 *
	 * @name cnxLog::_analyseLogDir()
	 * @param $templateString (string)
	 * @param $separator (string)
	 * @return array
	 */
	public function _analyseLogDir($templateString="access",$separator=" ",$logDir=ROOT_IN) {

		$arLogFile=array();
		$debug=false;
		$fileOut="agimus_access_analysed.log";

		if (($logDir=="") or (!is_dir($logDir)))
		return false;

		if ($handle = opendir($logDir)) {

			// ouverture du fichie de sortie
			$fs = fopen($logDir."/".$fileOut,"w");

			/* parcours du sas et lecture fichier par fichier */
			while (false !== ($file= readdir($handle))) {

				if ($file!=="." and $file !== ".." ) {

					if ($debug) echo "\r\n Lecture du fichier $file";
					// contr�le que le nom du fichier correspond a la recherche
					if (strtoupper(substr($file,0,strlen($templateString)))==strtoupper($templateString)) {

						if ($debug) echo "--> accepté !";
						//TODO : provoque des erreurs si chemin vers linux
						chmod($logDir."/".$file,0755);

						// Ouverture du fichier
						$fp = fopen($logDir."/".$file, 'r');
						$i = 0;
						$arLog=array();

						while(!feof($fp)) {
							try {
								$curLine=fgets($fp);
								$arFields=explode($separator,trim($curLine));
								$j=0;
								$arLogLine=array();
								foreach($arFields as $curField)
								{
									if (trim($curField)!="")
									$arLogLine[]=$curField;
									$j+=1;
								}

								$newLine=implode($separator,$arLogLine)." \n";
								fwrite($fs,$newLine);
							} catch (Exception $e) {
								echo '\r\n Erreur ligne #'.$i."\n";
							}
							$i++;
						}
						fclose($fp);

					}
				}
			}
			fclose($fs);
		}
		return $fileOut;
	}

	/**
	 * Analyse du répertoire sas
	 *
	 * <p>Analyse du repertoire sas</p>
	 *
	 * @name cnxLog::_analyseLog()
	 * @param $templateString (string)
	 * @param $separator (string)
	 * @return array
	 */
	public function _analyseLog($arLogFile,$separator=" ") {
		global $_matchLogFields,$_matchLogFieldsDoubleQuoted, $_matchMimeTypes,$arMonthConv;

		$nbrLigLus=0;
		$nbrLigWrite=0;
		$nbRejets=0;
		$debug=false;

		if (file_exists($arLogFile))
		{
			// Ouverture du fichier
			$fp = fopen($arLogFile, 'r');
			$i = 0;
			$arLog=array();

			while(!feof($fp)) {
				try {
					$lineRed=fgets($fp);

					// recup classique des élements par separateur
					$curLogLine=explode($separator,trim($lineRed));
					// recup des elements entre quotes par expression rationnelle
					$logEntryPattern = "#[\"*]#";
					$arSplit=preg_split($logEntryPattern,$lineRed);

					$nbrLigLus+=1;

					// Formattage de l'ID unique
					$uid=$curLogLine[$_matchLogFields["UID"]];

					//MATCHING :
					//controle que le type mime est bien selectionne dans l'application (param.php)
					$mimeType=$curLogLine[$_matchLogFields["MIME_TYPE"]];

					if ((count($_matchMimeTypes[$templateString])==0) or (in_array($mimeType,$_matchMimeTypes[$templateString])))
					{
						// ejection des lignes sans stampstart
						if ($curLogLine[$_matchLogFields["UID"]]=="-")
						{
							$nbRejets+=1;
							continue;
						}

						// formattage eventuel du stamp au format unix
						$stampStart=$curLogLine[$_matchLogFields["STAMP_START"]];

						$arStamp=preg_split("#^\[*#",$stampStart);
						$arD=preg_split("#[/:]#",$arStamp[1]);

						//conversion mois alpha en nombre
						$arD[1]=$arMonthConv[$arD[1]];

						// controle si le stamp est réellement un stamp
						if (
						(myIsInt($arD[0])) and
						(myIsInt($arD[1])) and
						(myIsInt($arD[2])) and
						(myIsInt($arD[3])) and
						(myIsInt($arD[4])) and
						(myIsInt($arD[5]))
						)
						$stampStart=mktime($arD[3],$arD[4],$arD[5],$arD[1],$arD[0],$arD[2]);
						else
						{
							if ($debug) echo "\r\n le Stamp est incorrect [$stampStart] !";
							$nbRejets+=1;
							continue;
						}

						if (isset($_matchLogFields["STAMP_MILLI"]))
						$stampStart.=".".$curLogLine[$_matchLogFields["STAMP_MILLI"]];

						//controle que le stamp n'a pas deja ete traite
						$idRef=$this->_existsRef($_matchLogFields["UID"], $stampStart);
						//si le stamp existe deja dans la base : on supprime
						if ($idRef==0)
						{
							$this->_setUid($uid);
							$this->_setStampStart($stampStart);
							$this->_setFlowSize($curLogLine[$_matchLogFields["FLOW_SIZE"]]);
							$this->_setRespTime($curLogLine[$_matchLogFields["RESP_TIME"]]);

							// récupération de l'application	
							$targetUrl=$arSplit[$_matchLogFieldsDoubleQuoted["TARGET_URL"]];

							if (GET_SHORT_URL)
							$targetUrl=substr($targetUrl,0,strpos($targetUrl,"?")-1);
							IF ($targetUrl!="")
							$this->_setTargetUrl($targetUrl);
							else
							{
								$nbRejets+=1;
								continue;
							}

							// collecte des données entre doublequotes
							$userAgent=$this->_getBrowser($arSplit[$_matchLogFieldsDoubleQuoted["USER_AGENT"]]);
							$this->_setUserAgent($userAgent);

							$this->_setMimeType($mimeType);

							$this->_create();
							$nbrLigWrite+=1;
						}
						else
						{
							$nbRejets+=1;
							continue;

						}
					}
					else
					{
						$nbRejets+=1;
						continue;
					}
				} catch (Exception $e) {
					echo "\r\n Erreur ligne #".$i."\n";
				}
				$i++;
			}
			fclose($fp);
		}
		return array("lu"=>$nbrLigLus,"ecrit"=>$nbrLigWrite, "rejete"=>$nbRejets);
	}

	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name CnxLog::_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id==0) return false;

		$listArray=$this->_getList();
		$this->_setUid($listArray[$this->id]['UID']);
		$this->_setStampStart($listArray[$this->id]['STAMP_START']);
		$this->_setFlowSize($listArray[$this->id]["FLOW_SIZE"]);
		$this->_setRespTime($listArray[$this->id]["RESP_TIME"]);
		$this->_setTargetUrl($listArray[$this->id]["TARGET_URL"]);
		$this->_setMimeType($listArray[$this->id]["MIME_TYPE"]);
		$this->_setStampProcessed($listArray[$this->id]["STAMP_PROCESSED"]);
		$this->_setUserAgent($listArray[$this->id]["USER_AGENT"]);
	}

	/**
	 * recherche du browser
	 *
	 * <p>recherche du browser</p>
	 *
	 * @name CnxLog::_getBrowser()
	 * @return string
	 */
	public function _getBrowser($user_agent)
	{
		if ((strpos($user_agent, "Nav") !== FALSE) || (strpos($user_agent, "Gold") !== FALSE) ||
		(strpos($user_agent, "X11") !== FALSE) || (strpos($user_agent, "Mozilla") !== FALSE) ||
		(strpos($user_agent, "Netscape") !== FALSE)
		AND (!strpos($user_agent, "MSIE") !== FALSE)
		AND (!strpos($user_agent, "Konqueror") !== FALSE)
		AND (!strpos($user_agent, "Firefox") !== FALSE)
		AND (!strpos($user_agent, "Safari") !== FALSE))
		$browser = "Netscape";
		elseif (strpos($user_agent, "Opera") !== FALSE)
		$browser = "Opera";
		elseif (strpos($user_agent, "MSIE") !== FALSE)
		$browser = "MSIE";
		elseif (strpos($user_agent, "Lynx") !== FALSE)
		$browser = "Lynx";
		elseif (strpos($user_agent, "WebTV") !== FALSE)
		$browser = "WebTV";
		elseif (strpos($user_agent, "Konqueror") !== FALSE)
		$browser = "Konqueror";
		elseif (strpos($user_agent, "Safari") !== FALSE)
		$browser = "Safari";
		elseif (strpos($user_agent, "Firefox") !== FALSE)
		$browser = "Firefox";
		elseif (strpos($user_agent, "Chrome") !== FALSE)
		$browser = "Chrome";
		elseif ((stripos($user_agent, "bot") !== FALSE) || (strpos($user_agent, "Google") !== FALSE) ||
		(strpos($user_agent, "Slurp") !== FALSE) || (strpos($user_agent, "Scooter") !== FALSE) ||
		(stripos($user_agent, "Spider") !== FALSE) || (stripos($user_agent, "Infoseek") !== FALSE))
		$browser = "Bot";
		else
		$browser = "Autre";

		return $browser;
	}

	/**
	 * recherche de l'OS
	 *
	 * <p>recherche de l'OS</p>
	 *
	 * @name CnxLog::_getOs()
	 * @return string
	 */
	public function _getOs($user_agent)
	{

		if (strpos($user_agent, "Win") !== FALSE)
		$os = "Windows";
		elseif ((strpos($user_agent, "Mac") !== FALSE) || (strpos($user_agent, "PPC") !== FALSE))
		$os = "Mac";
		elseif (strpos($user_agent, "Linux") !== FALSE)
		$os = "Linux";
		elseif (strpos($user_agent, "FreeBSD") !== FALSE)
		$os = "FreeBSD";
		elseif (strpos($user_agent, "SunOS") !== FALSE)
		$os = "SunOS";
		elseif (strpos($user_agent, "IRIX") !== FALSE)
		$os = "IRIX";
		elseif (strpos($user_agent, "BeOS") !== FALSE)
		$os = "BeOS";
		elseif (strpos($user_agent, "OS/2") !== FALSE)
		$os = "OS/2";
		elseif (strpos($user_agent, "AIX") !== FALSE)
		$os = "AIX";
		else
		$os = "Autre";

		return $os;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Structure::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}

} ?>
