<?php
/**
 *
 * <p>XmlIO</p>
 *
 * @name format XML
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright Capella Conseil 2009
 * @version 1.0.0
 * @package name
 */
class XmlIO {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. propri�t�s    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (string)
	 * @desc parser XML
	 */
	private $parser;

	/**
	 * @var (string)
	 * @desc data XML
	 */
	private $data;

	/**
	 * @var (array)
	 * @desc valeurs XML
	 */
	public $values;

	/**
	 * @var (array)
	 * @desc tags XML
	 */
	private $tags;

	/**
	 * @var (string)
	 * @desc chaine de sortie XML
	 */
	public $outString;

	/**
	 * @var (string)
	 * @desc chaine de retour charriot
	 */
	private $strCR;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name xmlIO::__construct()
	 * @return void
	 */
	public function __construct() {
		$this->outString="";
		$this->strCR='\r\n';

		$this->parser = xml_parser_create();
		xml_parser_set_option($this->parser,XML_OPTION_CASE_FOLDING,0);
		xml_parser_set_option($this->parser,XML_OPTION_SKIP_WHITE,1);
	}

	public function _importXML() {

		if ($this->data=="") return false;
		xml_parse_into_struct($this->parser,$this->data,$this->values,$this->tags);
		xml_parser_free($this->parser);
	}

	public function _readXmlFile($filename) {
		if (!file_exists($filename)) return false;
		$this->data = implode("",file($filename));
	}

	public function _exportXml($filename) {
		$fileHandler=fopen($filename,'w');
		fwrite($fileHandler,$this->outString);
		fclose($fileHandler);
	}
	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name xmlIO::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}

?>