<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * <p>Traitement</p>
 *
 * @name  Traitement
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

class Traitement {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. propri&eacute;t&eacute;s    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc Identifiant
	 */
	private $id;

	/**
	 * @var (String)
	 * @desc Code traitement
	 */
	private $trtCod;

	/**
	 * @var (Date)
	 * @desc Date de debut du traitement
	 */
	private $trtDateDeb;

	/**
	 * @var (Date)
	 * @desc Date de fin du traitement
	 */
	private $trtDateFin;

	/**
	 * @var (string)
	 * @desc Param�tres du traitement
	 */
	private $trtParams;

	/**
	 * @var (string)
	 * @desc Statut du traitement
	 */
	private $trtStatus;

	/**
	 * @var (String)
	 * @desc Log du traitement
	 */
	private $trtLog;



	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m&eacute;thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr&eacute;ation de l'instance de la classe</p>
	 *
	 * @name Traitement::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		$this->id='';
		$this->trtCod='';
		$this->trtDateDeb='';
		$this->trtDateFin='';
		$this->trtParams='';
		$this->trtStatus='';
		$this->trtLog='';


		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}

	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name Traitement::_getId()
	 * @return Int
	 */
	public function _getId() {
		return $this->id ;
	}

	/**
	 * @name Traitement::_getTrtCod()
	 * @return String
	 */
	public function _getTrtCod() {
		return $this->trtCod ;
	}

	/**
	 * @name Traitement::_getTrtDateDeb()
	 * @return Date
	 */
	public function _getTrtDateDeb() {
		return $this->trtDateDeb ;
	}

	/**
	 * @name Traitement::_getTrtDateFin()
	 * @return Date
	 */
	public function _getTrtDateFin() {
		return $this->trtDateFin ;
	}

	/**
	 * @name Traitement::_getTrtParams()
	 * @return string
	 */
	public function _getTrtParams() {
		return $this->trtParams ;
	}

	/**
	 * @name Traitement::_getTrtStatus()
	 * @return string
	 */
	public function _getTrtStatus() {
		return $this->trtStatus ;
	}

	/**
	 * @name Traitement::_getTrtLog()
	 * @return String
	 */
	public function _getTrtLog() {
		return $this->trtLog ;
	}



	/**
	 * Accesseurs en &eacute;criture
	 */

	/**
	 * @name Traitement::_setId()
	 * @param $id (Int)
	 * @return void
	 */
	public function _setId($id) {
		$this->id  = $id ;
	}

	/**
	 * @name Traitement::_setTrtCod()
	 * @param $trtCod (String)
	 * @return void
	 */
	public function _setTrtCod($trtCod) {
		$this->trtCod  = $trtCod ;
	}

	/**
	 * @name Traitement::_setTrtDateDeb()
	 * @param $trtDateDeb (Date)
	 * @return void
	 */
	public function _setTrtDateDeb($trtDateDeb) {
		$this->trtDateDeb  = $trtDateDeb ;
	}

	/**
	 * @name Traitement::_setTrtDateFin()
	 * @param $trtDateFin (Date)
	 * @return void
	 */
	public function _setTrtDateFin($trtDateFin) {
		$this->trtDateFin  = $trtDateFin ;
	}

	/**
	 * @name Traitement::_setTrtParams()
	 * @param $trtParams (string)
	 * @return void
	 */
	public function _setTrtParams($trtParams) {
		$this->trtParams  = $trtParams ;
	}

	/**
	 * @name Traitement::_setTrtStatus()
	 * @param $trtStatus (string)
	 * @return void
	 */
	public function _setTrtStatus($trtStatus) {
		$this->trtStatus  = $trtStatus ;
	}

	/**
	 * @name Traitement::_setTrtLog()
	 * @param $trtLog (String)
	 * @return void
	 */
	public function _setTrtLog($trtLog) {
		$this->trtLog  = $trtLog ;
	}



	/**
	 * Compte du nombre d'occurence de l'objet Traitement dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name Traitement::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_traitement ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}


	/**
	 * Cr&eacute;ation d'un Traitement dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name Traitement::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM t_traitement ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
			
		$sql  = 'INSERT INTO t_traitement VALUES( ';
		$sql .= '\''.AddSlashes($this->id).'\', ';
		$sql .= '\''.AddSlashes($this->trtCod).'\', ';
		$sql .= '\''.AddSlashes($this->trtDateDeb).'\', ';
		$sql .= '\''.AddSlashes($this->trtDateFin).'\', ';
		$sql .= '\''.AddSlashes($this->trtParams).'\', ';
		$sql .= '\''.AddSlashes($this->trtStatus).'\', ';
		$sql .= '\''.AddSlashes($this->trtLog).'\' ';

		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise &eacute; jour d'un Traitement dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name Traitement::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_traitement SET ';
		$sql .= 'ID = \''.AddSlashes($this->id).'\', ';
		$sql .= 'TRT_COD = \''.AddSlashes($this->trtCod).'\', ';
		$sql .= 'TRT_DATE_DEB = \''.AddSlashes($this->trtDateDeb).'\', ';
		$sql .= 'TRT_DATE_FIN = \''.AddSlashes($this->trtDateFin).'\', ';
		$sql .= 'TRT_PARAMS = \''.AddSlashes($this->trtParams).'\', ';
		$sql .= 'TRT_STATUS = \''.AddSlashes($this->trtStatus).'\', ';
		$sql .= 'TRT_LOG = \''.AddSlashes($this->trtLog).'\' ';

		$sql .= 'WHERE id=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un Traitement dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name Traitement::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'DELETE FROM t_traitement ';
		$sql .= 'WHERE id = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}
	/**
	 * R&eacute;cup&eacute;ration de la liste des Traitements
	 *
	 * <p>Liste des Traitement</p>
	 *
	 * @name Traitement::_getList()
	 * @return array
	 */
	public function _getList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_traitement ' ;
		if ($this->id>0)
		$sql.= ' WHERE id = \''.$this->id.'\' ';
		$sql .= 'ORDER BY TRT_DATE_DEB DESC ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['ID'] = StripSlashes($row['ID']) ;
				$listArray[$row['ID']]['TRT_COD'] = StripSlashes($row['TRT_COD']) ;
				$listArray[$row['ID']]['TRT_DATE_DEB'] = StripSlashes($row['TRT_DATE_DEB']) ;
				$listArray[$row['ID']]['TRT_DATE_FIN'] = StripSlashes($row['TRT_DATE_FIN']) ;
				$listArray[$row['ID']]['TRT_PARAMS'] = StripSlashes($row['TRT_PARAMS']) ;
				$listArray[$row['ID']]['TRT_STATUS'] = StripSlashes($row['TRT_STATUS']) ;
				$listArray[$row['ID']]['TRT_LOG'] = StripSlashes($row['TRT_LOG']) ;

			}
		}
		return $listArray ;
	}

	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name Traitement::_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id==0) return false;

		$listArray=$this->_getList();
		$this->id = $listArray[$this->id]['ID'];
		$this->trtCod = $listArray[$this->id]['TRT_COD'];
		$this->trtDateDeb = $listArray[$this->id]['TRT_DATE_DEB'];
		$this->trtDateFin = $listArray[$this->id]['TRT_DATE_FIN'];
		$this->trtParams = $listArray[$this->id]['TRT_PARAMS'];
		$this->trtStatus = $listArray[$this->id]['TRT_STATUS'];
		$this->trtLog = $listArray[$this->id]['TRT_LOG'];

	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Traitement::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>