<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * <p>Indicateur</p>
 *
 * @name  Indicateur
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright CAPELLA Conseil 2009
 * @version 1.0.0
 * @package
 */

class Indicateur {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc Identifiant
	 */
	private $id;

	/**
	 * @var (Int)
	 * @desc Groupe Proprietaire
	 */
	private $owner;

	/**
	 * @var (String)
	 * @desc Libell�
	 */
	private $lib;

	/**
	 * @var (String)
	 * @desc Description
	 */
	private $dsc;

	/**
	 * @var (String)
	 * @desc Type qual/quant
	 */
	private $type;

	/**
	 * @var (String)
	 * @desc Unit� (duree, volume, ...)
	 */
	private $unite;

	/**
	 * @var (Int)
	 * @desc Restitution par d�faut (cf. table des restitutions)
	 */
	private $defaultOutput;

	/**
	 * @var (int)
	 * @desc Fr�quence de rafraichissement
	 */
	private $updateFreq;

	/**
	 * @var (String)
	 * @desc Declencheur
	 */
	private $trig;

	/**
	 * @var (String)
	 * @desc Requete SQL type
	 */
	private $sqlString;

	/**
	 * @var (string)
	 * @desc completions de valeurs
	 */
	private $completionValues;


	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Indicateur::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		$this->id=0;
		$this->owner=0;
		$this->lib='';
		$this->dsc='';
		$this->type='';
		$this->unite='';
		$this->defaultOutput=1;
		$this->updateFreq=1;
		$this->trig='';
		$this->sqlString='';
		$this->completionValues="";

		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}

	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name Indicateur::_getId()
	 * @return Int
	 */
	public function _getId() {
		return $this->id ;
	}

	/**
	 * @name Indicateur::_getOwner()
	 * @return Int
	 */
	public function _getOwner() {
		return $this->owner ;
	}

	/**
	 * @name Indicateur::_getLib()
	 * @return String
	 */
	public function _getLib() {
		return $this->lib ;
	}

	/**
	 * @name Indicateur::_getDsc()
	 * @return String
	 */
	public function _getDsc() {
		return $this->dsc ;
	}

	/**
	 * @name Indicateur::_getType()
	 * @return String
	 */
	public function _getType() {
		return $this->type ;
	}

	/**
	 * @name Indicateur::_getUnite()
	 * @return String
	 */
	public function _getUnite() {
		return $this->unite ;
	}

	/**
	 * @name Indicateur::_getDefaultOutput()
	 * @return Int
	 */
	public function _getDefaultOutput() {
		return $this->defaultOutput ;
	}

	/**
	 * @name Indicateur::_getUpdateFreq()
	 * @return int
	 */
	public function _getUpdateFreq() {
		return $this->updateFreq ;
	}

	/**
	 * @name Indicateur::_getTrig()
	 * @return String
	 */
	public function _getTrig() {
		return $this->trig ;
	}

	/**
	 * @name Indicateur::_getSqlString()
	 * @return String
	 */
	public function _getSqlString() {
		return $this->sqlString ;
	}

	/**
	 * @name Indicateur::_getCompletingFlag()
	 * @return String
	 */
	public function _getCompletionValues() {
		return $this->completionValues ;
	}


	/**
	 * Accesseurs en �criture
	 */

	/**
	 * @name Indicateur::_setId()
	 * @param $id (Int)
	 * @return void
	 */
	public function _setId($id) {
		$this->id  = $id ;
	}

	/**
	 * @name Indicateur::_setOwner()
	 * @param $owner (Int)
	 * @return void
	 */
	public function _setOwner($owner) {
		$this->owner  = $owner ;
	}

	/**
	 * @name Indicateur::_setLib()
	 * @param $lib (String)
	 * @return void
	 */
	public function _setLib($lib) {
		$this->lib  = $lib ;
	}

	/**
	 * @name Indicateur::_setDsc()
	 * @param $dsc (String)
	 * @return void
	 */
	public function _setDsc($dsc) {
		$this->dsc  = $dsc ;
	}

	/**
	 * @name Indicateur::_setType()
	 * @param $type (String)
	 * @return void
	 */
	public function _setType($type) {
		$this->type  = $type ;
	}

	/**
	 * @name Indicateur::_setUnite()
	 * @param $unite (String)
	 * @return void
	 */
	public function _setUnite($unite) {
		$this->unite  = $unite ;
	}

	/**
	 * @name Indicateur::_setDefaultOutput()
	 * @param $defaultOutput (Int)
	 * @return void
	 */
	public function _setDefaultOutput($defaultOutput) {
		$this->defaultOutput  = $defaultOutput ;
	}

	/**
	 * @name Indicateur::_setUpdateFreq()
	 * @param $updateFreq (int)
	 * @return void
	 */
	public function _setUpdateFreq($updateFreq) {
		$this->updateFreq  = $updateFreq ;
	}

	/**
	 * @name Indicateur::_setTrig()
	 * @param $trig (String)
	 * @return void
	 */
	public function _setTrig($trig) {
		$this->trig  = $trig ;
	}

	/**
	 * @name Indicateur::_setSqlString()
	 * @param $sqlString (String)
	 * @return void
	 */
	public function _setSqlString($sqlString) {
		$this->sqlString  = $sqlString ;
	}

	/**
	 * @name Indicateur::_setCompletionValues()
	 * @param $values (array)
	 * @return void
	 */
	public function _setCompletionValues($values) {
		$this->completionValues  = $values ;
	}

	/**
	 * Compte du nombre d'occurence de l'objet Indicateur dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name Indicateur::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_indic ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * renvoit l'id d'un stamp existant
	 *
	 * <p>_existsRef</p>
	 *
	 * @name Indicateur::_existsRef()
	 * @param $lib string
	 * @return int
	 */
	public function _existsRef($lib)
	{
		$idRef = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT id ';
		$sql .= ' FROM t_indic ';
		$sql .= ' WHERE UPPER(lib) = \''.strtoupper($lib).'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$idRef  = $row['id'] ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		return $idRef;
	}

	/**
	 * Cr�ation d'un Indicateur dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name Indicateur::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM t_indic ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
			
		$sql  = 'INSERT INTO t_indic VALUES( ';
		$sql .= '\''.AddSlashes($this->id).'\', ';
		$sql .= '\''.AddSlashes($this->owner).'\', ';
		$sql .= '\''.AddSlashes($this->lib).'\', ';
		$sql .= '\''.AddSlashes($this->dsc).'\', ';
		$sql .= '\''.AddSlashes($this->type).'\', ';
		$sql .= '\''.AddSlashes($this->unite).'\', ';
		$sql .= '\''.AddSlashes($this->defaultOutput).'\', ';
		$sql .= '\''.AddSlashes($this->updateFreq).'\', ';
		$sql .= '\''.AddSlashes($this->trig).'\', ';
		$sql .= '\''.AddSlashes($this->sqlString).'\', ';
		$sql .= '\''.AddSlashes($this->completionValues).'\' ';

		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise � jour d'un Indicateur dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name Indicateur::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_indic SET ';
		$sql .= 'ID = \''.AddSlashes($this->id).'\', ';
		$sql .= 'OWNER = \''.AddSlashes($this->owner).'\', ';
		$sql .= 'LIB = \''.AddSlashes($this->lib).'\', ';
		$sql .= 'DSC = \''.AddSlashes($this->dsc).'\', ';
		$sql .= 'TYPE = \''.AddSlashes($this->type).'\', ';
		$sql .= 'UNITE = \''.AddSlashes($this->unite).'\', ';
		$sql .= 'DEFAULT_OUTPUT = \''.AddSlashes($this->defaultOutput).'\', ';
		$sql .= 'UPDATE_FREQ = \''.AddSlashes($this->updateFreq).'\', ';
		$sql .= 'TRIG = \''.AddSlashes($this->trig).'\', ';
		$sql .= 'SQL_STRING = \''.AddSlashes($this->sqlString).'\', ';
		$sql .= 'COMPLETION_VALUES = \''.AddSlashes($this->completionValues).'\' ';

		$sql .= 'WHERE id=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un Indicateur dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name Indicateur::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'DELETE FROM t_indic ';
		$sql .= 'WHERE id = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}
	
	/**
	 * Recuperation de la liste des Indicateurs
	 *
	 * <p>Liste des Indicateur</p>
	 *
	 * @name Indicateur::_getList()
	 * @return array
	 */
	public function _getList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_indic ' ;
		if ($this->id>0)
		$sql.= ' WHERE id = \''.$this->id.'\' ';
		$sql .= 'ORDER BY LIB ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['ID'] = StripSlashes($row['ID']) ;
				$listArray[$row['ID']]['OWNER'] = StripSlashes($row['OWNER']) ;
				$listArray[$row['ID']]['LIB'] = StripSlashes($row['LIB']) ;
				$listArray[$row['ID']]['DSC'] = StripSlashes($row['DSC']) ;
				$listArray[$row['ID']]['TYPE'] = StripSlashes($row['TYPE']) ;
				$listArray[$row['ID']]['UNITE'] = StripSlashes($row['UNITE']) ;
				$listArray[$row['ID']]['DEFAULT_OUTPUT'] = StripSlashes($row['DEFAULT_OUTPUT']) ;
				$listArray[$row['ID']]['UPDATE_FREQ'] = StripSlashes($row['UPDATE_FREQ']) ;
				$listArray[$row['ID']]['TRIG'] = StripSlashes($row['TRIG']) ;
				$listArray[$row['ID']]['SQL_STRING'] = StripSlashes($row['SQL_STRING']) ;
				$listArray[$row['ID']]['COMPLETION_VALUES'] = StripSlashes($row['COMPLETION_VALUES']) ;

			}
		}
		return $listArray ;
	}

	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name Indicateur::_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id==0) return false;

		$listArray=$this->_getList();
		$this->id = $listArray[$this->id]['ID'];
		$this->owner = $listArray[$this->id]['OWNER'];
		$this->lib = $listArray[$this->id]['LIB'];
		$this->dsc = $listArray[$this->id]['DSC'];
		$this->type = $listArray[$this->id]['TYPE'];
		$this->unite = $listArray[$this->id]['UNITE'];
		$this->defaultOutput = $listArray[$this->id]['DEFAULT_OUTPUT'];
		$this->updateFreq = $listArray[$this->id]['UPDATE_FREQ'];
		$this->trig = $listArray[$this->id]['TRIG'];
		$this->sqlString = $listArray[$this->id]['SQL_STRING'];
		$this->completionValues = $listArray[$this->id]['COMPLETION_VALUES'];

	}

	/**
	 * alimentation requete sur structures
	 *
	 * <p>_formatStructSql</p>
	 *
	 * @name refIndicateur::_formatStructSql()
	 * @param $structArray(Array)
	 * @return void
	 */
	public function _formatStructSql($structArray,$tableTemplate="t_agregat")
	{
		//v1.3-PPR-24052011 corretif notice
		$sqlString='';
		if (!is_array($structArray)) return $sqlString;

		$cpt=0;
		foreach($structArray as $parent=>$tree)
		{
			$sqlString.=" $tableTemplate.ID_STRUCT = \"".$parent."\" ";
			if ($cpt<count($structArray)-1)
			$sqlString.=" OR ";
			//v1.3-PPR-24052011 correctif undefined index
			if ((isset($tree['tree'][$parent])) and (is_array($tree['tree'][$parent])))
			$sqlString.=" OR ".$this->_formatStructSql($tree['tree'][$parent],$tableTemplate);
			$cpt+=1;
				
		}
		return $sqlString;
	}

	/**
	 * Collecte des agregats
	 *
	 * <p>libelle</p>
	 *
	 * @name Indicateur::_getResult()
	 * @return array
	 */
	public function _getResult()
	{
		//if ($this->id==0) return false;

		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql=$this->_getSqlString();

		$res = $maconnexion->_bddQuery($sql) ;
		if ($res>0)
		{
			if($maconnexion->_bddNumRows($res))
			{
				while($row = $maconnexion->_bddFetchAssoc($res))
				{
					$listArray[] = $row ;
				}
			}
		}
		return $listArray ;

	}


	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Indicateur::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>