<?php
/**
 *
 * <p>Parser</p>
 *
 * @name  Parser
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

class Parser {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (String)
	 * @desc Nom du fichier d'entr�e
	 */
	private $inFileName;

	/**
	 * @var (String)
	 * @desc Nom du fichier de sortie
	 */
	private $outFileName;

	/**
	 * @var (array)
	 * @desc Contenu Fichier entr�e
	 */
	private $arFileContent;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Parser::__construct()
	 * @return void
	 */
	public function __construct() {
		$this->inFileName='';
		$this->outFileName='';
	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name Parser::_getInFileName()
	 * @return String
	 */
	public function _getInFileName() {
		return $this->inFileName ;
	}

	/**
	 * @name Parser::_getOutFileName()
	 * @return String
	 */
	public function _getOutFileName() {
		return $this->outFileName ;
	}



	/**
	 * Accesseurs en �criture
	 */

	/**
	 * @name Parser::_setInFileName()
	 * @param $inFileName (String)
	 * @return void
	 */
	public function _setInFileName($inFileName) {
		$this->inFileName  = $inFileName ;
	}

	/**
	 * @name Parser::_setOutFileName()
	 * @param $outFileName (String)
	 * @return void
	 */
	public function _setOutFileName($outFileName) {
		$this->outFileName  = $outFileName ;
	}

	/**
	 * loadFile
	 *
	 * <p>Chargement du fichier d'entr�e</p>
	 *
	 * @name Parser::__loadFile()
	 * @return void
	 */
	public function __loadFile() {

		$log=new Log();
		
		if (!file_exists($this->inFileName)) 
		{
			$log->write("parser : fichier d'entr�e \"$this->inFileName\" non trouv� !");
			return false;
		}
		
		
	
	}

	
	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Parser::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>