<?php

require_once ('dynChart.php');
require_once ('../jpgraph/src/jpgraph.php');
require_once ('../jpgraph/src/jpgraph_line.php');
require_once ('../jpgraph/src/jpgraph_scatter.php');
require_once ('../jpgraph/src/jpgraph_regstat.php');
require_once ('../jpgraph/src/jpgraph_pie.php');
require_once ('../jpgraph/src/jpgraph_pie3d.php');
require_once ('../jpgraph/src/jpgraph_bar.php');
require_once ('../jpgraph/src/jpgraph_radar.php');
require_once ('../jpgraph/src/jpgraph_gantt.php');


/**
 *
 * <p>Chart dynamique JPGraph</p>
 *
 * @name dynChart
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */


class DynChartJPGraph extends DynChart {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. propriet�s    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Object)
	 * @desc current Chart
	 */
	public $graph;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name dynChart:__construct()
	 * @return void
	 */
	public function __construct($dynChartId=0,$width=600,$height=400) {
		parent::__construct($dynChartId,$width,$height);
		$this->cumColors=array(
		"0"=>array('AntiqueWhite2','AntiqueWhite4:0.8'),
		"1"=>array('olivedrab1','olivedrab4:0.8'),
		"2"=>array('cadetblue1','cadetblue4:0.8'),
		"3"=>array('chocolate1','chocolate4:0.8')
		);
	}

	/**
	 * @name dynChart::_initGraph()
	 * @return void
	 */
	public function _initGraph($scale='textlin')
	{
		// Create the graph. These two calls are always required
		$this->graph->title->Set($this->dynChartLib);
		$this->graph->title->SetFont(FF_ARIAL,FS_BOLD,10);
		$this->graph->SetScale($scale);
	
		$this->graph->SetMarginColor('white');
		$this->graph->img->SetMargin(20,20,20,20);

		$this->graph->SetFrame(0);
		if ($this->border)
			$this->graph->SetFrame(1,"blue@0.9",2);
		
		//$this->graph->img->SetAntiAliasing(1);
	}

	/**
	 * @name dynChart::_drawLineChart()
	 * @return void
	 */
	public function _drawLineChart()
	{

		/* contr�le des donn�es d'entr�e */

		if (count($this->dynChartValues)<2)
		return false;
			
		$this->graph = new Graph($this->dynChartWidth,$this->dynChartHeight,"auto");
		$this->_initGraph('textlin');
		
		$xdata = array_keys($this->dynChartValues);
		$ydata = array_values($this->dynChartValues);

		$lineplot=new LinePlot($ydata);
		$this->graph->Add($lineplot);
		$this->graph->xaxis->SetTickLabels($xdata);

		$xint=round((count($this->dynChartValues)/20),0)+1;
		$this->graph->xaxis->SetTextLabelInterval($xint);
		
		$lineplot->SetColor("darkblue");
		$lineplot->SetWeight(1.5);

		// Display the graph
		$this->graph->img->SetAntiAliasing();
		$this->graph->Stroke($this->dynChartOutFile);

		return $this->dynChartOutFile;
	}

	/**
	 * @name dynChart::_drawCumHistoChart()
	 * @param $dynChartValues (array)
	 * @return void
	 */
	public function _drawCumLineChart()
	{
		/* contr�le des donn�es d'entr�e */
		if (count($this->dynChartValues)==0)
		return false;

		foreach($this->dynChartValues as $pivot=>$values)
		{
			if (!is_array($values)) return false;
			if (count($values)<2) return false;
		}

		$this->graph = new Graph($this->dynChartWidth,$this->dynChartHeight,"auto");
		$this->_initGraph('textlin');
		$theme_class = new SoftyTheme;
		$this->graph->SetTheme($theme_class);
		
		$arPivot = array_keys($this->dynChartValues);

		// Create the linear plot
		$arCat=array();

		$idxColor=0;
		
		//v1.4.1
		$maxNbValues=0;
		
		foreach($this->dynChartValues as $pivot=>$values)
		{

			$newLinePlot[$idxColor]=new LinePlot(array_values($values));
			$newLinePlot[$idxColor]->SetColor($this->colors[$idxColor]);

			$newLinePlot[$idxColor]->SetLegend($pivot);
			$newLinePlot[$idxColor]->SetWeight(2);
			//$newLinePlot[$idxColor]->SetStepStyle();
			$this->graph->Add($newLinePlot[$idxColor]);
			$xdata=array_keys($values);
			$idxColor+=1;
			
			//v1.4.1
			$maxNbValues=max($maxNbValues,count($values));
		}


		$this->graph->xaxis->SetTickLabels($xdata);

		//v1.4.1
		$xint=round(($maxNbValues/20),0)+1;
		$this->graph->xaxis->SetTextLabelInterval($xint);

		// Display the graph
		$this->graph->SetMargin(50,10,70,10);
		$this->graph->Stroke($this->dynChartOutFile);

		return $this->dynChartOutFile;
	}
	/**
	 * @name dynChart::_drawPieChart()
	 * @param void
	 * @return void
	 */

	public function _drawPieChart() {

		/* contr�le des donn�es d'entr�e */
		if (count($this->dynChartValues)<1)
		return false;

		foreach($this->dynChartValues as $pivot=>$values)
		{
			if (is_array($values)) return false;
		}

		$this->graph = new PieGraph($this->dynChartWidth,$this->dynChartHeight,"auto");
		$this->_initGraph('linlin');
		$theme_class = new SoftyTheme;
		$this->graph->SetTheme($theme_class);
		
		// agregation automatique
		$tabValPie=$this->dynChartValues;

		if ($this->maxValuesBeforeAgregate>0)
		{
			$i=0;
			$tabValPie=array();
			foreach($this->dynChartValues as $objRef=>$objVal)
			{
				if ($i<$this->maxValuesBeforeAgregate)
				$tabValPie[$objRef]=$objVal;
				else
				$tabValPie["Autres"]+=$objVal;
				$i+=1;
			}
		}

		$data  = array_values($tabValPie);
		$datalib  = array_keys($tabValPie);

		// limitation de la longueur du texte de l�gende
		foreach($datalib as $curlib)
		{
			if (strlen($curlib)>DYNCHART_LEGEND_MAX_LENGTH)
			$datalibtronq[]=substr($curlib,0,DYNCHART_LEGEND_MAX_LENGTH)."...";
			else
			$datalibtronq[]=$curlib;
		}
		$datalib=$datalibtronq;

		$p1 = new PiePlot3D($data);
		$p1->SetAngle(80);
		$p1->SetTheme('earth');
		$p1->SetSize(0.25);
		$p1->SetCenter(0.5);
		$p1->SetLegends($datalib);
		$p1->SetColor("darkgray");
		$p1->SetShadow("gray",2);
		$this->graph->legend->SetPos(0.5,0.99,"center","bottom");
		$this->graph->legend->SetColumns(3);

		$this->graph->Add($p1);
		$this->graph->Stroke($this->dynChartOutFile);

		return $this->dynChartOutFile;

	}

	/**
	 * @name dynChart::_drawHistoChart()
	 * @param $dynChartValues (array)
	 * @return void
	 */
	public function _drawHistoChart()
	{
		/* contr�le des donn�es d'entr�e */
		if (count($this->dynChartValues)<1)
		return false;

		foreach($this->dynChartValues as $pivot=>$values)
		{
			if (is_array($values)) return false;
		}

		$this->graph = new Graph($this->dynChartWidth,$this->dynChartHeight,"auto");
		$this->_initGraph('textlin');
		$theme_class = new SoftyTheme;
		$this->graph->SetTheme($theme_class);
		$this->graph->SetMargin(50,10,70,10);
		
		$xdata = array_keys($this->dynChartValues);
		$ydata = array_values($this->dynChartValues);

		$barplot=new BarPlot($ydata);
		$this->graph->Add($barplot);

		$this->graph->xaxis->SetTickLabels($xdata);
		$xint=round((count($this->dynChartValues)/20),0)+1;
		$this->graph->xaxis->SetTextLabelInterval($xint);

		$barplot->SetColor("darkblue");
		$barplot->SetWeight(1.5);

		// Display the graph
		$this->graph->img->SetAntiAliasing();
		$this->graph->Stroke($this->dynChartOutFile);

		return $this->dynChartOutFile;
	}

	/**
	 * @name dynChart::_drawCumHistoChart()
	 * @param $dynChartValues (array)
	 * @return void
	 */
	public function _drawCumHistoChart()
	{
		/* controle des donnees d'entree */
		if (count($this->dynChartValues)==0)
		return false;

		foreach($this->dynChartValues as $pivot=>$values)
		{
			if (!is_array($values)) return false;
		}

		$this->graph = new Graph($this->dynChartWidth,$this->dynChartHeight,"auto");
		$this->_initGraph('textlin');
		$theme_class = new SoftyTheme;
		$this->graph->SetTheme($theme_class);
		$this->graph->SetMargin(50,10,70,10);

		$this->graph->xaxis->title->Set($this->axisLibs['xaxis']);
		$this->graph->xaxis->title->SetFont(FF_ARIAL,FS_NORMAL);
		$this->graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,8);
		$this->graph->xaxis->SetLabelMargin(10);

		$this->graph->yaxis->title->Set($this->axisLibs['yaxis']);
		$this->graph->yaxis->title->SetFont(FF_ARIAL,FS_NORMAL);
		$this->graph->yaxis->SetFont(FF_ARIAL,FS_NORMAL,8);
		$this->graph->yaxis->title->SetMargin(10);

		//$this->graph->ygrid->SetFill(true,'#EFEFEF@0.5','#BBCCFF@0.5');
		//$this->graph->xgrid->Show();

		$this->graph->legend->SetLayout(LEGEND_HOR);
		$this->graph->legend->Pos(0.5,0.1,"center","bottom");
		
		$arPivot = array_keys($this->dynChartValues);

		// Create the linear plot
		$arCat=array();

		$idxColor=0;
		//v1.4.1
		$nbMaxValues=0;
		foreach($this->dynChartValues as $pivot=>$values)
		{
			$newBarPlot=new BarPlot(array_values($values));

			$newBarPlot->value->SetFont(FF_ARIAL,FS_NORMAL,8);
			$newBarPlot->SetValuePos('center');
			$newBarPlot->value->SetFormat('%01.1f');
			$newBarPlot->SetWidth(0.9);
			$newBarPlot->value->Show();
			$newBarPlot->SetLegend($pivot);

			$arBarPlots[]=$newBarPlot;
			$xdata=array_keys($values);
			$idxColor+=1;
			
			//v1.4.1
			$nbMaxValues=max($nbMaxValues,count($values));
		}

		$gbplot = new GroupBarPlot($arBarPlots);
		$gbplot->SetWidth(0.7);
		$gbplot->SetShadow('gray');
		//$gbplot->value->SetFormat('%01.0f');
		//$gbplot->value->SetFont(FF_ARIAL,FS_BOLD,8);
		//$gbplot->value->Show();

		$this->graph->Add($gbplot);

		$this->graph->xaxis->SetTickLabels($xdata);
		//v1.4.1
		$this->graph->xaxis->SetLabelAngle(40);

		//v1.4.1
		$xint=round(($nbMaxValues/20),0)+1;
		$this->graph->xaxis->SetTextLabelInterval($xint);

		$gbplot->SetColor("darkblue");
		$gbplot->SetWeight(1.0);

		// Display the graph
		$this->graph->Stroke($this->dynChartOutFile);

		return $this->dynChartOutFile;
	}

	public function _drawRadarChart()
	{
		/* contr�le des donn�es d'entr�e */
		if (count($this->dynChartValues)<3)
		return false;

		foreach($this->dynChartValues as $pivot=>$values)
		{
			if (is_array($values)) return false;
		}

		$titles = array_keys($this->dynChartValues);
		$data = array_values($this->dynChartValues);

		$this->graph = new RadarGraph($this->dynChartWidth,$this->dynChartHeight);
		$this->_initGraph('lin');
		
		$this->graph->SetTitles($titles);
		$this->graph->SetCenter(0.5,0.55);
		$this->graph->HideTickMarks();
		$this->graph->SetColor('white');
		$this->graph->axis->SetColor('darkgray');
		$this->graph->grid->SetColor('darkgray');
		$this->graph->grid->Show();

		$this->graph->axis->title->SetFont(FF_ARIAL,FS_NORMAL,9);
		$this->graph->axis->title->SetMargin(5);
		$this->graph->SetGridDepth(DEPTH_BACK);
		$this->graph->SetSize(0.7);

		$plot = new RadarPlot($data);
		$plot->SetColor('blue@0.2');
		$plot->SetLineWeight(1);
		$plot->SetFillColor('blue@0.9');

		$plot->mark->SetType(MARK_IMG_SBALL,'red');

		$this->graph->Add( $plot);
		$this->graph->Stroke($this->dynChartOutFile);

		return $this->dynChartOutFile;
	}

	public function _drawGanttJPG() {

		/* contr�le des donn�es d'entr�e */
		if (count($this->dynChartValues)<1)
		return false;

		define("LINE_HEIGHT",0.8);
		//  A new activity on row '0'

		$nbAct=0;
		$this->graph = new GanttGraph (0,0, "auto");
		$this->_initGraph('');

		// Explicitely set the date range
		// (Autoscaling will of course also work)
	
		$this->graph->SetVMarginFactor(0.7);
		$this->graph->SetMargin(0,35,10,20);
	
		// Setup some nonstandard colors
		$this->graph->SetMarginColor('#FFFFFF');
		//$graph->SetBox(true,'yellow:0.6',1);
		//$graph->SetFrame(true,'darkblue',2);
	
		$this->graph->scale->divider->SetColor('blue:0.6');
		$this->graph->scale->dividerh->SetColor('blue:0.6');
	
		// Display month and year scale with the gridlines
		$this->graph->ShowHeaders(GANTT_HMONTH | GANTT_HYEAR);
		$this->graph->scale->month->SetFont( FF_ARIAL, FS_NORMAL,8);
		$this->graph->scale->month->SetStyle(MONTHSTYLE_FIRSTLETTER);
		$this->graph->scale->month->grid->SetColor('gray');
		$this->graph->scale->month->SetBackgroundColor('#F8F8F8');
		$this->graph->scale->month->grid->Show(true);
		$this->graph->scale->year->SetFont( FF_ARIAL, FS_BOLD,10);
		$this->graph->scale->year->grid->SetColor('gray');
		$this->graph->scale->year->SetBackgroundColor('#F8F8F8');
		$this->graph->scale->year->grid->Show(true);
	
		// For the titles we also add a minimum width of 100 pixels for the Task name column
		//$graph->scale->actinfo->SetColTitles(
		//    array('C','Task','Duration','Start','Finish'),array(30,100));
		$this->graph->scale->actinfo ->SetColTitles (
		array('Type' ,'Phase'));
		$this->graph->scale->actinfo->SetBackgroundColor('#F8F8F8');
		$this->graph->scale->actinfo->SetFont(FF_ARIAL,FS_NORMAL,8);
		$this->graph->scale->actinfo->vgrid->SetStyle('solid');
		$this->graph->scale->actinfo->vgrid->SetColor('gray');
	
		// Ajout de lignes d'affichage alternatives
		$this->graph->hgrid-> Show();
		$this->graph->hgrid-> SetRowFillColor( 'gray@0.9');

		//echo Session::_affPr($this->dynChartValues);		

		foreach ($this->dynChartValues as $key=>$value) {

			$iconopen = new  IconImage( '../../images/icons/'.$value['icone'], 1);
			$bgcolor='black';
			if (isset($value['color']))
				$bgcolor=$value['color'];

			$activity = new GanttBar ($nbAct,array($iconopen,$value['libelle']), $value['start_date'], $value['end_date'],$value['prog'],LINE_HEIGHT);
			$activity->title->SetFont( FF_ARIAL, FS_NORMAL,8);
			if (isset($value['color']))
				$activity->SetPattern(BAND_LDIAG,$bgcolor);
			$activity->SetColor("black");
			$activity->SetFillColor("white");
			$activity->SetShadow();
	
			// affichage de la ligne de progression
			if ($value['avc']>0)
			{
				$activity->progress-> SetPattern( BAND_RDIAG, $bgcolor);
				$activity->progress-> SetHeight(0.4);
				$activity->progress->Set($value['avc']);
			}
	
	
			//     $activity->SetFillColor("red");
			$this->graph->Add($activity);
			$nbAct+=1;
		}
		$this->graph->Stroke($this->dynChartOutFile);

		return $this->dynChartOutFile;

	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name DynChart::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>