<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * <p>UniteTemps</p>
 *
 * @name  UniteTemps
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

class UniteTemps {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc Identifiant
	 */
	private $id;

	/**
	 * @var (int)
	 * @desc annee
	 */
	private $annee;

	/**
	 * @var (int)
	 * @desc Mois
	 */
	private $mois;

	/**
	 * @var (int)
	 * @desc Semaine
	 */
	private $semaine;

	/**
	 * @var (int)
	 * @desc Jour
	 */
	private $jour;

	/**
	 * @var (int)
	 * @desc Heure
	 */
	private $heure;



	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name UniteTemps::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		$this->id=0;
		$this->annee=0;
		$this->mois=0;
		$this->semaine=0;
		$this->jour=0;
		$this->heure=0;


		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}

	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name UniteTemps::_getId()
	 * @return Int
	 */
	public function _getId() {
		return $this->id ;
	}

	/**
	 * @name UniteTemps::_getAnnee()
	 * @return int
	 */
	public function _getAnnee() {
		return $this->annee ;
	}

	/**
	 * @name UniteTemps::_getMois()
	 * @return int
	 */
	public function _getMois() {
		return $this->mois ;
	}

	/**
	 * @name UniteTemps::_getSemaine()
	 * @return int
	 */
	public function _getSemaine() {
		return $this->semaine ;
	}

	/**
	 * @name UniteTemps::_getJour()
	 * @return int
	 */
	public function _getJour() {
		return $this->jour ;
	}

	/**
	 * @name UniteTemps::_getHeure()
	 * @return int
	 */
	public function _getHeure() {
		return $this->heure ;
	}



	/**
	 * Accesseurs en �criture
	 */

	/**
	 * @name UniteTemps::_setId()
	 * @param $id (Int)
	 * @return void
	 */
	public function _setId($id) {
		$this->id  = $id ;
	}

	/**
	 * @name UniteTemps::_setAnnee()
	 * @param $annee (int)
	 * @return void
	 */
	public function _setAnnee($annee) {
		$this->annee  = $annee ;
	}

	/**
	 * @name UniteTemps::_setMois()
	 * @param $mois (int)
	 * @return void
	 */
	public function _setMois($mois) {
		$this->mois  = $mois ;
	}

	/**
	 * @name UniteTemps::_setSemaine()
	 * @param $semaine (int)
	 * @return void
	 */
	public function _setSemaine($semaine) {
		$this->semaine  = $semaine ;
	}

	/**
	 * @name UniteTemps::_setJour()
	 * @param $jour (int)
	 * @return void
	 */
	public function _setJour($jour) {
		$this->jour  = $jour ;
	}

	/**
	 * @name UniteTemps::_setHeure()
	 * @param $heure (int)
	 * @return void
	 */
	public function _setHeure($heure) {
		$this->heure  = $heure ;
	}



	/**
	 * Compte du nombre d'occurence de l'objet UniteTemps dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name UniteTemps::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_ut ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * renvoit l'id d'un stamp existant
	 *
	 * <p>_existsRef</p>
	 *
	 * @name UnitTemps::_existsRef()
	 * @param $arDate $array
	 * @return int
	 */
	public function _existsRef($arDate)
	{
		$idRef = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT id ';
		$sql .= ' FROM t_ut ';
		$sql .= ' WHERE 1 ';
		$sql .= ' AND ANNEE = \''.$arDate[0].'\' ';
		$sql .= ' AND MOIS = \''.$arDate[1].'\' ';
		$sql .= ' AND SEMAINE = \''.$arDate[2].'\' ';
		$sql .= ' AND JOUR = \''.$arDate[3].'\' ';
		$sql .= ' AND HEURE = \''.$arDate[4].'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$idRef  = $row['id'] ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		return $idRef;
	}


	/**
	 * Cr�ation d'un UniteTemps dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name UniteTemps::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;

		// L'ID est un timestamp calcul� � partir des donn�es de base
		// de l'unit� de temps
		
		$this->id=mktime($this->heure,0,0,$this->mois,$this->jour,$this->annee);
		
		$sql  = 'INSERT INTO t_ut VALUES( ';
		$sql .= '\''.AddSlashes($this->id).'\', ';
		$sql .= '\''.AddSlashes($this->annee).'\', ';
		$sql .= '\''.AddSlashes($this->mois).'\', ';
		$sql .= '\''.AddSlashes($this->semaine).'\', ';
		$sql .= '\''.AddSlashes($this->jour).'\', ';
		$sql .= '\''.AddSlashes($this->heure).'\' ';

		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise � jour d'un UniteTemps dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name UniteTemps::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_ut SET ';
		$sql .= 'ID = \''.AddSlashes($this->id).'\', ';
		$sql .= 'ANNEE = \''.AddSlashes($this->annee).'\', ';
		$sql .= 'MOIS = \''.AddSlashes($this->mois).'\', ';
		$sql .= 'SEMAINE = \''.AddSlashes($this->semaine).'\', ';
		$sql .= 'JOUR = \''.AddSlashes($this->jour).'\', ';
		$sql .= 'HEURE = \''.AddSlashes($this->heure).'\' ';

		$sql .= 'WHERE id=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un UniteTemps dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name UniteTemps::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$sql  = 'DELETE FROM t_ut ';
		$sql .= 'WHERE id = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}
	/**
	 * R�cup�ration de la liste des UniteTempss
	 *
	 * <p>Liste des UniteTemps</p>
	 *
	 * @name UniteTemps::_getList()
	 * @return array
	 */
	public function _getList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_ut ' ;
		if ($this->id>0)
		$sql.= ' WHERE id = \''.$this->id.'\' ';
		$sql .= 'ORDER BY LIB ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['ID'] = StripSlashes($row['ID']) ;
				$listArray[$row['ID']]['ANNEE'] = StripSlashes($row['ANNEE']) ;
				$listArray[$row['ID']]['MOIS'] = StripSlashes($row['MOIS']) ;
				$listArray[$row['ID']]['SEMAINE'] = StripSlashes($row['SEMAINE']) ;
				$listArray[$row['ID']]['JOUR'] = StripSlashes($row['JOUR']) ;
				$listArray[$row['ID']]['HEURE'] = StripSlashes($row['HEURE']) ;

			}
		}
		return $listArray ;
	}

	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name UniteTemps::_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id==0) return false;

		$listArray=$this->_getList();
		$this->id = $listArray[$this->id]['ID'];
		$this->annee = $listArray[$this->id]['ANNEE'];
		$this->mois = $listArray[$this->id]['MOIS'];
		$this->semaine = $listArray[$this->id]['SEMAINE'];
		$this->jour = $listArray[$this->id]['JOUR'];
		$this->heure = $listArray[$this->id]['HEURE'];

	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name UniteTemps::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>