<?php
/* ====================================== */
/* Librairie applicative                  */
/* ====================================== */
require_once("../../config/param.php");

//creation des repertoires
createDir("/in");

//v1.5-PPR-#017 ajout repertoire in/archives
createDir("/in/archives");

createDir("/out");
createDir("/logs");
createDir("/app/restit/media");

//==========================================================
// Creation des repertoires applicatifs
//==========================================================
function createDir($wh) {
	if (!is_dir(DIR_WWW.ROOT_APPL.$wh))
	{
		if (!mkdir(DIR_WWW.ROOT_APPL.$wh)) dieAppli("ARRET DU TRAITEMENT : impossible de creer le repertoire ".DIR_WWW.ROOT_APPL."$wh");
	}
	if (!chmod(DIR_WWW.ROOT_APPL.$wh,511)) // equivalent 777 octal
	dieAppli("ARRET DU TRAITEMENT : impossible de modifier les droits du répertoire ".DIR_WWW.ROOT_APPL."$wh");
}

//==========================================================
//v1.9.6-PPR-#35 migration from mainlib.php
//v1.9.4-PPR-#747 passage en majuscule des champs d'une table
//==========================================================
function keyToUpper($ar) {
	$arkeys=array_keys($ar);
	$arkeys=array_map("strtoupper",$arkeys);
	$ar=array_combine($arkeys,$ar);
	return $ar;
}

// ========================================================
//  generation du timestamp en microsecondes
// ========================================================
function microtimeFloat()
{
	list($usec, $sec) = explode(" ", microtime());
	return ((float)$usec + (float)$sec);
}

// ===========================================
// Affichage d'erreur bloquante
// ===========================================
function dieAppli($texte)
{
	$html ="<html>";
	$html.="<head>";
	$html.="</head>";
	$html.="<body>";
	$html.="<link rel='stylesheet' href='".ROOT_APPL."/style/appl.css'  type='text/css' title='standard'>";
	$html.="<p align=center>";
	$html.="<div style='background-color:white;border:2px dashed #777;width:30%;padding:20px;margin-left:35%;margin-top:80px;'>";
	$html.="<b>Erreur dans ".TITRE_APPLI." ".VER_APPLI."</b><hr><font color=red>$texte</font><br><br><i>Contactez votre administrateur.</i><hr>";
	$html.="</div>";
	$html.="</p>";
	$html.="</body>";
	$html.="</html>";

	die($html);
}

// ===========================================
// Affichage tableau en mode debug
// ===========================================
function aff_pr($tableau,$print=false) {
	$html="<hr>";
	$html.= "<pre>";
	$html.=print_r($tableau,$print);
	$html.= "</pre>";
	if ($print)
	return $html;
	echo $html;
}

// ===========================================
// Import d'un fichier XML
// ===========================================
function _importXmlFile($fileName)
{
	// lit la base de donn�es xml
	if (!file_exists($fileName)) return false;
	$file=fopen($fileName,"r");

	$data = implode("",file($fileName));
	$parser = xml_parser_create();
	xml_parser_set_option($parser,XML_OPTION_TARGET_ENCODING, "ISO-8859-1").
	xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
	xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,1);
	xml_parse_into_struct($parser,$data,$values,$tags);
	xml_parser_free($parser);

	return array($data,$values,$tags);
}

// ===========================================
// Analyse d'une structure XML et sortie en tableau
// ===========================================
function _analyseXmlStruct($xmlStruct) {

	$debug=false;

	// contr�le d'entr�e
	$data=$xmlStruct[0];
	if (!($data!="")) return false;
	$values=$xmlStruct[1];
	if (!is_array($values)) return false;
	$tags=$xmlStruct[2];
	if (!is_array($tags)) return false;


	$arClass=array();
	$classHeader=array();
	$classAttributes=array();

	// boucle � travers les structures

	foreach ($values as $key=>$val) {

		if ($debug) aff_pr($val);

		if ($val["type"]=="open")
		{
			$typLoop=$val[tag];
			$arStructLoop=array();
			$loop=true;
			if ($debug) echo "<br>DEBUT LOOP : $typLoop";
			continue;
		}

		if ($val["type"]=="close")
		{
			if ($debug) echo "<br>FIN LOOP : $typLoop";
			if ($typLoop==$val[tag])
			$arDocStruct[$typLoop][]=$arStructLoop;
			$loop=false;
		}

		if ($val["type"]!="close") // tag d'entr�e

		if ($loop)
		$arStructLoop[$val[tag]]=$val[value];
	}
	if ($debug) aff_pr($arDocStruct);
	return $arDocStruct;
}

// ===========================================
// Analyse d'un XML de Classe
// ===========================================
function _generateClass($xmlStruct,$scriptName) {

	$debug=false;

	if (!is_array($xmlStruct)) return false;
	if ($scriptName=="") $scriptName="testClass.php";

	$arClass=_analyseXmlStruct($xmlStruct) or dieAppli("Impossible d'analyser la structure XML !");
	if ($debug) aff_pr($arClass);

	//ouvre le script en ecriture
	if (file_exists($scriptName))
	dieAppli("Le fichier \"$scriptName\" existe : creation impossible !");
	$script=fopen($scriptName,"w");

	// ------------------- modification headers -----------------------------
	//controle des parametres
	if (!defined("TPL_MAIN")) dieAppli("Parametre TPL_MAIN non defini");

	// lit le template de base
	if (!file_exists(TPL_MAIN)) return false;
	$data=file_get_contents(TPL_MAIN);

	$className="???";
	$initString="";
	$idName="";

	// remplacement des valeurs du header de classe
	foreach($arClass[header][0] as $param=>$value)
	{
		$data=str_replace("{{$param}}","$value",$data);
		//Memorisation du nom de classe
		if ($param=="name")
		$className=$value;
		if ($param=="idName")
		$idName=$value;
	}

	// ------------------- generation des attributs -----------------------------
	//controle des parametres
	if (!defined("TPL_ATTRIBUTE")) dieAppli("Parametre TPL_ATTRIBUTE non defini");

	// lit le template
	if (!file_exists(TPL_ATTRIBUTE)) return false;
	$dataTpl=file_get_contents(TPL_ATTRIBUTE);

	$finalString="";

	// remplacement des valeurs du header de classe
	foreach($arClass[attribute] as $seq=>$arAttribute)
	{
		// initialise la chaine de template
		$curDataAttrib=$dataTpl;
		foreach($arAttribute as $param=>$value)
		{
			$curDataAttrib=str_replace("{{$param}}","$value",$curDataAttrib);
			//Generation de l'init string
			if ($param=="name") $attribName=$value;
			if (($param=="var") and ($attribName!=""))
			{
				if (strtoupper($value)=="INT") {
					$initString.='$this->'.$attribName.'=\'\';';
				}
				else {
					$initString.='$this->'.$attribName.'=\'\';';
				}
				$initString.="\r\n";
			}
		}
		$finalString.=$curDataAttrib."\r\n";
	}

	$param="attrib";
	$data=str_replace("{{$param}}","$finalString",$data);

	// ------------------- generation des Accesseurs en lecture -----------------------------
	//controle des parametres
	if (!defined("TPL_READ_FONC")) dieAppli("Parametre TPL_READ_FONC non defini");

	// lit le template
	if (!file_exists(TPL_READ_FONC)) return false;
	$dataTpl=file_get_contents(TPL_READ_FONC);

	$finalString="";
	// remplacement des valeurs du header de classe
	foreach($arClass[attribute] as $seq=>$arAttribute)
	{
		// initialise la chaine de template
		$attribName="";
		$curDataAttrib=$dataTpl;
		foreach($arAttribute as $param=>$value)
		{
			$curDataAttrib=str_replace("{{$param}}","$value",$curDataAttrib);
			//memorisation nom d'attribut
			if ($param=="name")
			$attribName=$value;
		}
		$uValue=ucfirst($attribName);
		$attrib="Uname";
		$curDataAttrib=str_replace("{{$attrib}}","$uValue",$curDataAttrib);
		$attrib="Cname";
		$curDataAttrib=str_replace("{{$attrib}}","$className",$curDataAttrib);
		$finalString.=$curDataAttrib."\r\n";
	}

	$param="readfonc";
	$data=str_replace("{{$param}}","$finalString",$data);

	// ------------------- generation des Accesseurs en ecriture -----------------------------
	//controle des parametres
	if (!defined("TPL_WRITE_FONC")) dieAppli("Parametre TPL_WRITE_FONC non defini");

	// lit le template
	if (!file_exists(TPL_WRITE_FONC)) return false;
	$dataTpl=file_get_contents(TPL_WRITE_FONC);

	$finalString="";
	// remplacement des valeurs du header de classe
	foreach($arClass[attribute] as $seq=>$arAttribute)
	{
		// initialise la chaine de template
		$attribName="";
		$curDataAttrib=$dataTpl;
		foreach($arAttribute as $param=>$value)
		{
			$curDataAttrib=str_replace("{{$param}}","$value",$curDataAttrib);
			//memorisation nom d'attribut
			if ($param=="name")
			$attribName=$value;
		}
		$uValue=ucfirst($attribName);
		$attrib="Uname";
		$curDataAttrib=str_replace("{{$attrib}}","$uValue",$curDataAttrib);
		$attrib="Cname";
		$curDataAttrib=str_replace("{{$attrib}}","$className",$curDataAttrib);
		$finalString.=$curDataAttrib."\r\n";
	}

	$createString="";
	// remplacement des valeurs de la fonction create
	$i=0;
	foreach($arClass[attribute] as $seq=>$arAttribute)
	{
		$virgule="";
		if ($i<(count($arClass[attribute])-1))
		$virgule=',';
		foreach($arAttribute as $param=>$value)
		{

			//Generation de la string
			if ($param=="name") $attribName=$value;
			if (($param=="var") and ($attribName!=""))
			{
				//if (strtoupper($value)!="STRING") {
				$createString.=' $sql .= \'\\\'\'.AddSlashes($this->'.$attribName.').\'\\\''.$virgule.' \';  ';
				//}
				//else {
				//	$createString.=' $sql .= \'\\\'\'.$this->'.$attribName.'.\'\\\''.$virgule.' \';  ';
				//}
				$createString.="\r\n";
			}
		}
		$i+=1;
	}

	$updateString="";
	// remplacement des valeurs de la fonction update
	$i=0;
	foreach($arClass[attribute] as $seq=>$arAttribute)
	{
		$virgule="";
		if ($i<(count($arClass[attribute])-1))
		$virgule=',';
		foreach($arAttribute as $param=>$value)
		{

			//Generation de la string
			if ($param=="name") $attribName=$value;
			if (($param=="var") and ($attribName!=""))
			{
				//if (strtoupper($value)!="STRING") {
				$updateString.=' $sql .= \''.strtoupper($attribName).' = \\\'\'.AddSlashes($this->'.$attribName.').\'\\\''.$virgule.' \';  ';
				//}
				//else {
				//	$updateString.=' $sql .= \''.strtoupper($attribName).' = \\\'\'.$this->'.$attribName.'.\'\\\''.$virgule.' \';  ';
				//	}
				$updateString.="\r\n";
			}
		}
		$i+=1;
	}

	$listString="";
	// remplacement des valeurs de la fonction list
	$i=0;
	foreach($arClass[attribute] as $seq=>$arAttribute)
	{
		$virgule="";
		if ($i<(count($arClass[attribute])-1))
		$virgule=',';
		foreach($arAttribute as $param=>$value)
		{

			//Generation de la string
			if ($param=="name") $attribName=$value;
			if (($param=="var") and ($attribName!=""))
			{
				$listString.=' $listArray[$row[\''.strtoupper($idName).'\']][\''.strtoupper($attribName).'\'] = StripSlashes($row[\''.strtoupper($attribName).'\']) ;  ';
				$listString.="\r\n";
			}
		}
		$i+=1;
	}

	$fillString="";
	// remplacement des valeurs de la fonction fill
	$i=0;
	foreach($arClass[attribute] as $seq=>$arAttribute)
	{
		$virgule="";
		if ($i<(count($arClass[attribute])-1))
		$virgule=',';
		foreach($arAttribute as $param=>$value)
		{

			//G�n�ration de la string
			if ($param=="name") $attribName=$value;
			if (($param=="var") and ($attribName!=""))
			{
				$fillString.=' $this->'.$attribName.' = $listArray[$this->'.$idName.'][\''.strtoupper($attribName).'\']; ';
				$fillString.="\r\n";
			}
		}
		$i+=1;
	}

	$param="writefonc";
	$data=str_replace("{{$param}}","$finalString",$data);

	$param="init";
	$data=str_replace("{{$param}}","$initString",$data);

	$param="createfonc";
	$data=str_replace("{{$param}}","$createString",$data);

	$param="updatefonc";
	$data=str_replace("{{$param}}","$updateString",$data);

	$param="listfonc";
	$data=str_replace("{{$param}}","$listString",$data);

	$param="fillfonc";
	$data=str_replace("{{$param}}","$fillString",$data);

	// ecriture du fichier final
	fwrite($script,$data);
	fclose($script);

	return true;
}

// ===========================================
// Liste le contenu d'un repertoire
// ===========================================
function ls_a($wh,$ext)
{
	global $flog;

	$files="";
	if ($handle = opendir($wh))
	{
		while (false !== ($file = readdir($handle)))
		{
			if ($file !== "." && $file !== ".." )
			{
				if (
				(($ext=="") or (($ext!="") and (strstr($file,".$ext"))))
				and (!strstr($file,".old"))
				)
				{
					if(!$files) $files=$file;
					else $files = $file."\r\n".$files;
				}
			}
		}
		closedir($handle);
	}
	$arr=explode("\r\n", $files);
	return $arr;
}

// ========================================================
// Suppression des fichiers d'un repertoire selon template
// ========================================================
function filesDirDelete($dir_path,$ext,$fileTpl="")
{
	$dir_list = ls_a($dir_path,$ext);
	if(sizeof($dir_list)>0)
	{
		if($dir_list[0]!= "")
		{
			for($i=0;$i<sizeof($dir_list);$i++)
			{
				if ($fileTpl=="")
				{
					if (file_exists($dir_path."/".$dir_list[$i])) unlink($dir_path."/".$dir_list[$i]);
				}
				else
				{
					if ($fileTpl==substr($dir_list[$i],0,strlen($fileTpl))) unlink($dir_path."/".$dir_list[$i]);
				}

			}
		}
	}
}

/**
 * Analyse du r�pertoire sas
 *
 * <p>Analyse du r�pertoire sas</p>
 *
 * @name library::_analyseLogDir()
 * @param $templateString (string)
 * @param $separator (string)
 * @return array
 */
function _analyseDir($fileDir,$templateString="") {

	$arFiles=array();

	if ($handle = opendir($fileDir)) {

		/* parcours du sas et lecture fichier par fichier */
		while (false !== ($file= readdir($handle))) {

			if ($file!=="." and $file !== ".." ) {

				// filtrage sur les fichiers sql uniquement
				if (strrchr($file,'.')=='.sql')
				{
					// contr�le que le nom du fichier correspond � la recherche
					if (($templateString=="") or (strtoupper(substr($file,0,strlen($templateString)))==strtoupper($templateString))) {
						$arFiles[]=$file;
					}
				}
			}
		}
	}
	return $arFiles;
}


function myIsInt($x) {
	return(is_numeric($x) ? intval(0+$x) == $x : false);
}

?>