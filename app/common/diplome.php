<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * <p>Diplome</p>
 *
 * @name  Diplome
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

class Diplome {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc Identifiant
	 */
	private $id;

	/**
	 * @var (String)
	 * @desc R�f�rence externe
	 */
	private $extnRef;
	
	/**
	 * @var (String)
	 * @desc Libell�
	 */
	private $lib;

	/**
	 * @var (String)
	 * @desc Decription
	 */
	private $dsc;

	/**
	 * @var (Int)
	 * @desc Id Regroupement
	 */
	private $idRegr;

	/**
	 * @var (Int)
	 * @desc Nombre annees formation
	 */
	private $nbrAnForm;

	/**
	 * @var (Array)
	 * @desc tableau identifiant=>libell� */
	private $comboList;
	

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Diplome::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		$this->id=0;
		$this->extnRef='';
		$this->lib='';
		$this->dsc='';
		$this->idRegr=0;
		$this->nbrAnForm=0;

		$this->comboList = array() ;
		
		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}

	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name Diplome::_getId()
	 * @return Int
	 */
	public function _getId() {
		return $this->id ;
	}

		/**
	 * @name Diplome::_getExtnRef()
	 * @return string
	 */
	public function _getExtnRef() {
		return $this->extnRef ;
	}
	
	/**
	 * @name Diplome::_getLib()
	 * @return String
	 */
	public function _getLib() {
		return $this->lib ;
	}

	/**
	 * @name Diplome::_getDsc()
	 * @return String
	 */
	public function _getDsc() {
		return $this->dsc ;
	}

	/**
	 * @name Diplome::_getIdRegr()
	 * @return Int
	 */
	public function _getIdRegr() {
		return $this->idRegr ;
	}

	/**
	 * @name Diplome::_getNbrAnForm()
	 * @return Int
	 */
	public function _getNbrAnForm() {
		return $this->nbrAnForm ;
	}

	/**
	 * @name CatSrv::_getComboList()
	 * @return array
	 */
	public function _getComboList()
	{
		return $this->comboList ;
	}
	
	

	/**
	 * Accesseurs en �criture
	 */

	/**
	 * @name Diplome::_setId()
	 * @param $id (Int)
	 * @return void
	 */
	public function _setId($id) {
		$this->id  = $id ;
	}

	/**
	 * @name Diplome::_setExtnRef()
	 * @param $extnRef (String)
	 * @return void
	 */
	public function _setExtnRef($extnRef) {
		$this->extnRef  = $extnRef ;
	}
	
	/**
	 * @name Diplome::_setLib()
	 * @param $lib (String)
	 * @return void
	 */
	public function _setLib($lib) {
		$this->lib  = $lib ;
	}

	/**
	 * @name Diplome::_setDsc()
	 * @param $dsc (String)
	 * @return void
	 */
	public function _setDsc($dsc) {
		$this->dsc  = $dsc ;
	}

	/**
	 * @name Diplome::_setIdRegr()
	 * @param $idRegr (Int)
	 * @return void
	 */
	public function _setIdRegr($idRegr) {
		$this->idRegr  = $idRegr ;
	}

	/**
	 * @name Diplome::_setNbrAnForm()
	 * @param $nbrAnForm (Int)
	 * @return void
	 */
	public function _setNbrAnForm($nbrAnForm) {
		$this->nbrAnForm  = $nbrAnForm ;
	}



	/**
	 * Compte du nombre d'occurence de l'objet Diplome dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name Diplome::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_diplome ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * renvoit l'id d'un stamp existant
	 *
	 * <p>_existsRef</p>
	 *
	 * @name Diplome::_existsRef()
	 * @param $lib string
	 * @return int
	 */
	public function _existsRef($lib)
	{
		$idRef = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT id ';
		$sql .= ' FROM t_diplome ';
		$sql .= " WHERE UPPER(lib) = \"".strtoupper($lib)."\" ";

		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$idRef  = $row['id'] ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		return $idRef;
	}

	/**
	 * Cr�ation d'un Diplome dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name Diplome::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM t_diplome ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
			
		$sql  = 'INSERT INTO t_diplome VALUES( ';
		$sql .= '\''.AddSlashes($this->id).'\', ';
		$sql .= '\''.AddSlashes($this->extnRef).'\', ';
		$sql .= '\''.AddSlashes($this->lib).'\', ';
		$sql .= '\''.AddSlashes($this->dsc).'\', ';
		$sql .= '\''.AddSlashes($this->idRegr).'\', ';
		$sql .= '\''.AddSlashes($this->nbrAnForm).'\' ';

		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise � jour d'un Diplome dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name Diplome::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_diplome SET ';
		$sql .= 'ID = \''.AddSlashes($this->id).'\', ';
		$sql .= 'EXTN_REF = \''.AddSlashes($this->extnRef).'\', ';
		$sql .= 'LIB = \''.AddSlashes($this->lib).'\', ';
		$sql .= 'DSC = \''.AddSlashes($this->dsc).'\', ';
		$sql .= 'ID_REGR = \''.AddSlashes($this->idRegr).'\', ';
		$sql .= 'NBR_AN_FORM = \''.AddSlashes($this->nbrAnForm).'\' ';

		$sql .= 'WHERE id=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un Diplome dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name Diplome::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'DELETE FROM t_diplome ';
		$sql .= 'WHERE id = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}
	/**
	 * R�cup�ration de la liste des Diplomes
	 *
	 * <p>Liste des Diplome</p>
	 *
	 * @name Diplome::_getList()
	 * @return array
	 */
	public function _getList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_diplome ' ;
		if ($this->id>0)
		$sql.= ' WHERE id = \''.$this->id.'\' ';
		$sql .= 'ORDER BY LIB ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['ID'] = StripSlashes($row['ID']) ;
				$listArray[$row['ID']]['EXTN_REF'] = StripSlashes($row['EXTN_REF']) ;
				$listArray[$row['ID']]['LIB'] = StripSlashes($row['LIB']) ;
				$listArray[$row['ID']]['DSC'] = StripSlashes($row['DSC']) ;
				$listArray[$row['ID']]['ID_REGR'] = StripSlashes($row['ID_REGR']) ;
				$listArray[$row['ID']]['NBR_AN_FORM'] = StripSlashes($row['NBR_AN_FORM']) ;
				$this->comboList[$row['ID']] = StripSlashes($row['LIB']) ;
			}
		}
		return $listArray ;
	}

	
	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name Diplome::_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id==0) return false;

		$listArray=$this->_getList();
		$this->id = $listArray[$this->id]['ID'];
		$this->extnRef=$listArray[$this->id]['EXTN_REF'];
		$this->lib = $listArray[$this->id]['LIB'];
		$this->dsc = $listArray[$this->id]['DSC'];
		$this->idRegr = $listArray[$this->id]['ID_REGR'];
		$this->nbrAnForm = $listArray[$this->id]['NBR_AN_FORM'];

	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Diplome::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>