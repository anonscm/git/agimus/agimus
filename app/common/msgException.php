<?php
require_once( 'mysqlDatabase.php') ;
/**
 * <p>Gestion des messages</p>
 * 
 * @name MsgException
 * @author  AGIMUS <agimus.technique@education.gouv.fr>  
 * 			Pascal Procope <procope@capella-conseil.com> 
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright Capella Conseil 2009
 * @version 1.0.0
 * @package common
 */

class MsgException extends Exception{
	/*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/
 	
 	/**
    * @var $runSession(Session)
    * @desc Session ouverte � la connexion
    */
 	private $runSession;
 	
 	/**
    * @var $dbMessage(boolean)
    * @desc indique si le message est une erreur renvoy�e par la base de donn�es
    */ 	
 	private $dbMessage;
 	   
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name Message::__construct()
    * @return void 
    */
    public function __construct($message, $type='') {
    	parent::__construct($message);
    	if($type=='database')
    	{
    		$this->dbMessage = TRUE ;
    	}else{
    		$this->dbMessage = FALSE ;
    	}
    }
   
    
    /**
    * Recup�ration du message � afficher
    * 
    * <p>Obtenir un message</p>
    * 
    * @name Message::_getMessage()
    * @param $code (string)
    * @return string 
    */    
    public function _getError($maSession=NULL)
    {
    	if($this->dbMessage == TRUE )
    	{
    		$msgString = $this->getMessage() ;
    	}else{
    		$lang = $maSession->_getLang() ;
			$maconnexion = MysqlDatabase::GetInstance() ;
			$sql = 'SELECT im.MSG_ID, iml.LIBELLE, im.MSG_TYPE ';
			$sql .= 'FROM appl_msg as im, appl_msg_libelle as iml  ';
			$sql .= 'WHERE im.MSG_CODE = \''.$this->getMessage().'\' ';
			$sql .= 'AND iml.LANG = \''.$lang.'\' ';
			$sql .= 'AND im.MSG_ID = iml.MSG_ID ' ;		
			$res = $maconnexion->_bddQuery($sql) ;
			
			if($maconnexion->_bddNumRows($res) == 0) {
				$msgString=$this->getMessage();
            }else{
				$row = $maconnexion->_bddFetchAssoc($res) ;
				//$msgString =htmlentities( StripSlashes($row['LIBELLE']) );
				$msgString =StripSlashes($row['LIBELLE']);
				$msgType = $row['MSG_TYPE'] ;
				if($msgType == 'error')
				{
					//logue de l'erreur
					$sql1 = 'INSERT INTO appl_log_error SET ';
					$sql1 .= 'MSG_ID = \''.$row['MSG_ID'].'\', ';
					$sql1 .= 'MOD_ID = \''.$maSession->_getModid().'\', ';			
					$sql1 .= 'INT_COD = \''.$maSession->_getUid().'\', ';
					$sql1 .= 'ERROR_DATE = \''.date('Y-m-d H:i:s').'\' ';
					$res1 = $maconnexion->_bddQuery($sql1) ;			
				}
            }
    	}
    	return 	$msgString ;	
    }
}
?>