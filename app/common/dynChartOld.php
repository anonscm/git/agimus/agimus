<?php

/**
 *
 * <p>Chart dynamique</p>
 *
 * @name dynChart
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

require_once (DIR_WWW.ROOT_APPL.'/jpgraph/src/jpgraph.php');
require_once (DIR_WWW.ROOT_APPL.'/jpgraph/src/jpgraph_line.php');
require_once (DIR_WWW.ROOT_APPL.'/jpgraph/src/jpgraph_scatter.php');
require_once (DIR_WWW.ROOT_APPL.'/jpgraph/src/jpgraph_regstat.php');
require_once (DIR_WWW.ROOT_APPL.'/jpgraph/src/jpgraph_pie.php');
require_once (DIR_WWW.ROOT_APPL.'/jpgraph/src/jpgraph_pie3d.php');
require_once (DIR_WWW.ROOT_APPL.'/jpgraph/src/jpgraph_bar.php');
require_once (DIR_WWW.ROOT_APPL.'/jpgraph/src/jpgraph_radar.php');

class DynChart {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. propriet�s  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc id Chart
	 */
	private $dynChartId;
	/**
	 * @var (String)
	 * @desc titre Chart
	 */
	private $dynChartLib;
	/**
	 * @var (Int)
	 * @desc Type Chart
	 */
	private $dynChartType;

	/**
	 * @var (array)
	 * @desc Valeurs Chart
	 */
	private $dynChartValues;

	/**
	 * @var (String)
	 * @desc fichier cible Chart
	 */
	private $dynChartOutFile;

	/**
	 * @var (Int)
	 * @desc Largeur
	 */
	private $dynChartWidth;

	/**
	 * @var (Int)
	 * @desc Largeur
	 */
	private $dynChartHeight;

	/**
	 * @var (Int)
	 * @desc nombre de valeurs maximum avant agregation
	 */
	private $maxValuesBeforeAgregate;

	/**
	 * @var (Bool)
	 * @desc Border oui/non
	 */
	private $border;

	public $Colors;

	public $cumColors;

	public $axisLibs;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name dynChart:__construct()
	 * @return void
	 */
	public function __construct($dynChartId=0) {

		$this->dynChartId = $dynChartId ;
		$this->dynChartLib='';
		$this->dynChartType=1; // par d�faut : courbe
		$this->dynChartValues=array();
		$this->dynChartOutFile=DIR_WWW.ROOT_APPL.'/out/temp_0_chart.gif';
		$this->dynChartWidth=400;
		$this->dynChartHeight=300;
		$this->border=false;

		$this->colors=array(
		"0"=>'red',
		"1"=>'blue',
		"2"=>'green',
		"3"=>'magenta',
		"4"=>'chocolate'
		);
		$this->cumColors=array(
		"0"=>array('AntiqueWhite2','AntiqueWhite4:0.8'),
		"1"=>array('olivedrab1','olivedrab4:0.8'),
		"2"=>array('cadetblue1','cadetblue4:0.8'),
		"3"=>array('chocolate1','chocolate4:0.8')
		);

		$this->axisLibs=array();
	}

	/**
	 * Accesseurs en lecture
	 */
	/**
	 * @name dynChart::_getDynChartId()
	 * @return int
	 */
	public function _getDynChartId()
	{
		return $this->dynChartId ;
	}
	/**
	 * @name dynChart::_getDynChartLib()
	 * @return string
	 */
	public function _getDynChartLib()
	{
		return $this->dynChartLib ;
	}
	/**
	 * @name dynChart::_getDynChartType()
	 * @return int
	 */
	public function _getDynChartType()
	{
		return $this->dynChartType ;
	}
	/**
	 * @name dynChart::_getDynChartValues()
	 * @return array
	 */
	public function _getDynChartValues()
	{
		return $this->dynChartValues ;
	}
	/**
	 * @name dynChart::_getDynChartOutFile()
	 * @return string
	 */
	public function _getDynChartOutFile()
	{
		return $this->dynChartOutFile ;
	}

	/**
	 * Accesseurs en �criture
	 */

	/**
	 * @name dynChart::_setDynChartId()
	 * @param $dynChartId (int)
	 * @return void
	 */
	public function _setDynChartId($dynChartId)
	{
		$this->dynChartId = $dynChartId ;
	}
	/**
	 * @name dynChart::_setDynChartLib()
	 * @param $dynChartLib (string)
	 * @return void
	 */
	public function _setDynChartLib($dynChartLib)
	{
		$this->dynChartLib = $dynChartLib ;
	}
	/**
	 * @name dynChart::_setDynChartType()
	 * @param $dynChartType (int)
	 * @return void
	 */
	public function _setDynChartType($dynChartType)
	{
		$this->dynChartType = $dynChartType ;
	}
	/**
	 * @name dynChart::_setDynChartValues()
	 * @param $dynChartValues (array)
	 * @return void
	 */
	public function _setDynChartValues($dynChartValues)
	{
		$this->dynChartValues = $dynChartValues ;
	}
	/**
	 * @name dynChart::_setDynChartOutFile()
	 * @param $dynChartOutFile (string)
	 * @return void
	 */
	public function _setDynChartOutFile($dynChartOutFile)
	{
		$this->dynChartOutFile = $dynChartOutFile ;
	}
	/**
	 * @name dynChart::_setDynChartWidth()
	 * @param $dynChartWidth (int)
	 * @return void
	 */
	public function _setDynChartWidth($dynChartWidth)
	{
		$this->dynChartWidth = $dynChartWidth ;
	}

	/**
	 * @name dynChart::_setDynChartHeight()
	 * @param $dynChartHeight (int)
	 * @return void
	 */
	public function _setDynChartHeight($dynChartHeight)
	{
		$this->dynChartHeight = $dynChartHeight ;
	}
	/**
	 * @name dynChart::_setMaxValuesBeforeAgregate()
	 * @param $maxValues (int)
	 * @return void
	 */
	public function _setMaxValuesBeforeAgregate($maxValues)
	{
		$this->maxValuesBeforeAgregate = $maxValues ;
	}

	/**
	 * @name dynChart::_setBorder()
	 * @param $border (bool)
	 * @return void
	 */
	public function _setBorder($bool)
	{
		$this->border = $bool ;
	}

	/**
	 * @name dynChart::_setAxisLibs()
	 * @param $libs (array)
	 * @return void
	 */
	public function _setAxisLibs($libs)
	{
		$this->axisLibs = $libs ;
	}


	/**
	 * @name dynChart::_drawLineChart()
	 * @param $dynChartValues (array)
	 * @return void
	 */
	public function _drawLineChart()
	{

		/* contr�le des donn�es d'entr�e */
		if (count($this->dynChartValues)<2)
		return false;
		if ($this->dynChartType==0)
		return false;

		//ksort($this->dynChartValues);
			
		$xdata = array_keys($this->dynChartValues);
		$ydata = array_values($this->dynChartValues);

		$titre=$this->dynChartLib;

		$gJpgBrandTiming=true;

		// Create the graph. These two calls are always required
		$graph = new Graph(600,400,"auto");
		$graph->SetMarginColor('white');
		$graph->SetScale("textlin");
		$graph->SetFrame(0);
		if ($this->border)
		{
			$graph->SetFrame(1,"blue@0.9",3);
		}

		// Create the linear plot
		$lineplot=new LinePlot($ydata);
		//$lineplot2=new LinePlot($ydata2);

		// Add the plot to the graph
		$graph->Add($lineplot);
		//$graph->Add($lineplot2);

		$graph->img->SetMargin(60,60,20,90);
		//v1.9.6.2-PPR-#000 correctif libell� "evolution"
		$graph->title->Set($this->dynChartLib);
		$graph->xaxis->SetTickLabels($xdata);
		$graph->xaxis->SetLabelMargin(10);
		$graph->xaxis->SetLabelAngle(60);

		$graph->xaxis->title->Set($this->axisLibs['xaxis']);
		$graph->yaxis->title->Set($this->axisLibs['yaxis']);

		$xint=round((count($this->dynChartValues)/80),0)+1;
		$graph->xaxis->SetTextLabelInterval($xint);
		$graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,8);
		$graph->yaxis->SetFont(FF_ARIAL,FS_NORMAL,8);

		$graph->ygrid->SetFill(true,'#EFEFEF@0.5','#BBCCFF@0.5');
		$graph->xgrid->Show();

		$graph->title->SetFont(FF_ARIAL,FS_BOLD,12);

		$graph->yaxis->title->SetFont(FF_ARIAL,FS_NORMAL);
		$graph->xaxis->title->SetFont(FF_ARIAL,FS_NORMAL);

		$lineplot->SetColor("darkblue");
		$lineplot->SetWeight(1.5);

		// Display the graph
		$graph->img->SetAntiAliasing();
		$graph->Stroke($this->dynChartOutFile);

		return $this->dynChartOutFile;
	}

	/**
	 * @name dynChart::_drawCumHistoChart()
	 * @param $dynChartValues (array)
	 * @return void
	 */
	public function _drawCumLineChart()
	{
		/* contr�le des donn�es d'entr�e */
		if (count($this->dynChartValues)==0)
		return false;

		foreach($this->dynChartValues as $pivot=>$values)
		{
			if (!is_array($values)) return false;
			if (count($values)<2)
			return false;
		}
		if ($this->dynChartType==0)
		return false;

		// Create the graph. These two calls are always required
		$graph = new Graph($this->dynChartWidth,$this->dynChartHeight,"auto");
		$graph->SetMarginColor('white');
		$graph->SetScale("textlin");
		$graph->SetFrame(0);
		if ($this->border)
		{
			$graph->SetFrame(1,"blue@0.9",3);
		}
		$titre=$this->dynChartLib;
		$gJpgBrandTiming=true;

		$arPivot = array_keys($this->dynChartValues);

		// Create the linear plot
		$arCat=array();


		$idxColor=0;
		foreach($this->dynChartValues as $pivot=>$values)
		{

			$newLinePlot[$idxColor]=new LinePlot(array_values($values));
			$newLinePlot[$idxColor]->SetColor($this->colors[$idxColor]);

			$newLinePlot[$idxColor]->SetLegend($pivot);
			$newLinePlot[$idxColor]->SetWeight(2);
			//$newLinePlot[$idxColor]->SetStepStyle();
			$graph->Add($newLinePlot[$idxColor]);
			$xdata=array_keys($values);
			$idxColor+=1;
		}


		$graph->img->SetMargin(60,60,20,90);
		$graph->title->Set($this->dynChartLib);
		$graph->xaxis->SetTickLabels($xdata);
		$graph->xaxis->SetLabelMargin(10);
		$graph->xaxis->SetLabelAngle(90);

		$graph->xaxis->title->Set($this->axisLibs['xaxis']);
		$graph->yaxis->title->Set($this->axisLibs['yaxis']);

		$xint=round((count($this->dynChartValues)/80),0)+1;
		$graph->xaxis->SetTextLabelInterval($xint);
		$graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,8);
		$graph->yaxis->SetFont(FF_ARIAL,FS_NORMAL,8);

		$graph->ygrid->SetFill(true,'#EFEFEF@0.5','#BBCCFF@0.5');
		$graph->xgrid->Show();

		$graph->title->SetFont(FF_ARIAL,FS_BOLD,10);

		$graph->yaxis->title->SetFont(FF_ARIAL,FS_NORMAL);
		$graph->xaxis->title->SetFont(FF_ARIAL,FS_NORMAL);

		$graph->legend->SetLayout(LEGEND_HOR);
		$graph->legend->Pos(0.5,0.95,"right","bottom");


		// Display the graph
		$graph->img->SetAntiAliasing();
		$graph->Stroke($this->dynChartOutFile);

		return $this->dynChartOutFile;
	}
	/**
	 * @name dynChart::_drawPieChart()
	 * @param void
	 * @return void
	 */

	public function _drawPieChart() {

		/* contr�le des donn�es d'entr�e */
		if (count($this->dynChartValues)<1)
		return false;
		if ($this->dynChartType==0)
		return false;
		foreach($this->dynChartValues as $pivot=>$values)
		{
			if (is_array($values)) return false;
		}

		//asort($this->dynChartValues);

		//v1.9.6-PPR-#000 agregation automatique
		$tabValPie=$this->dynChartValues;

		if ($this->maxValuesBeforeAgregate>0)
		{
			$i=0;
			$tabValPie=array();
			foreach($this->dynChartValues as $objRef=>$objVal)
			{
				if ($i<$this->maxValuesBeforeAgregate)
				$tabValPie[$objRef]=$objVal;
				else
				$tabValPie["Autres"]+=$objVal;
				$i+=1;
			}
		}

		$data  = array_values($tabValPie);
		$datalib  = array_keys($tabValPie);

		// limitation de la longueur du texte de l�gende
		foreach($datalib as $curlib)
		{
			if (strlen($curlib)>DYNCHART_LEGEND_MAX_LENGTH)
			$datalibtronq[]=substr($curlib,0,DYNCHART_LEGEND_MAX_LENGTH)."...";
			else
			$datalibtronq[]=$curlib;
		}
		$datalib=$datalibtronq;

		$graph = new PieGraph($this->dynChartWidth,$this->dynChartHeight,"auto");
		$graph->SetFrame(0);
		if ($this->border)
		{
			$graph->SetFrame(1,"blue@0.9",3);
		}

		$graph->title->Set($this->dynChartLib);
		$graph->title->SetFont(FF_ARIAL,FS_BOLD);

		$p1 = new PiePlot($data);
		$p1->SetTheme(PIE_THEME);
		$p1->SetSize(0.3);
		$p1->SetCenter(0.30);
		$p1->SetLegends($datalib);
		$p1->SetColor("darkgray");
		$p1->SetShadow("gray",2);
		$graph->legend->SetPos(0.05,0.5,"right","center");

		//$p1->ExplodeAll();

		$graph->Add($p1);

		$graph->img->SetMargin(0,0,0,0);
		$graph->img->SetAntiAliasing();
		$graph->Stroke($this->dynChartOutFile);

		return $this->dynChartOutFile;

	}

	/**
	 * @name dynChart::_drawHistoChart()
	 * @param $dynChartValues (array)
	 * @return void
	 */
	public function _drawHistoChart()
	{

		/* contr�le des donn�es d'entr�e */
		//if (count($this->dynChartValues)<2)
		//return false;
		if ($this->dynChartType==0)
		return false;
		foreach($this->dynChartValues as $pivot=>$values)
		{
			if (is_array($values)) return false;
		}

		$xdata = array_keys($this->dynChartValues);
		$ydata = array_values($this->dynChartValues);

		$titre=$this->dynChartLib;

		$gJpgBrandTiming=true;

		// Create the graph. These two calls are always required
		$graph = new Graph($this->dynChartWidth,$this->dynChartHeight,"auto");
		$graph->SetMarginColor('white');
		$graph->SetScale("textlin");
		$graph->SetFrame(0);
		if ($this->border)
		{
			$graph->SetFrame(1,"blue@0.9",3);
		}

		// Create the linear plot
		$barplot=new BarPlot($ydata);

		//$barplot->value->Show();
		// Add the plot to the graph
		$graph->Add($barplot);
		//$graph->Add($lineplot2);

		$graph->img->SetMargin(60,60,20,90);
		$graph->title->Set($this->dynChartLib);
		$graph->xaxis->SetTickLabels($xdata);
		$graph->xaxis->SetLabelMargin(10);
		$graph->xaxis->SetLabelAngle(90);

		$graph->xaxis->title->Set($this->axisLibs['xaxis']);
		$graph->yaxis->title->Set($this->axisLibs['yaxis']);

		$xint=round((count($this->dynChartValues)/80),0)+1;
		$graph->xaxis->SetTextLabelInterval($xint);
		$graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,8);
		$graph->yaxis->SetFont(FF_ARIAL,FS_NORMAL,8);

		$graph->ygrid->SetFill(true,'#EFEFEF@0.5','#BBCCFF@0.5');
		$graph->xgrid->Show();

		$graph->title->SetFont(FF_ARIAL,FS_BOLD,10);

		$graph->yaxis->title->SetFont(FF_ARIAL,FS_NORMAL);
		$graph->xaxis->title->SetFont(FF_ARIAL,FS_NORMAL);

		$graph->yaxis->SetLabelMargin(0);
		$graph->SetMargin(130,140,50,130);
		$graph->legend->Pos(0.05,0.5,"right","top");
		
		$graph->yaxis->scale->SetGrace(20);

		$barplot->SetColor("darkblue");
		$barplot->SetWeight(1.5);

		// Display the graph
		$graph->img->SetAntiAliasing();
		$graph->Stroke($this->dynChartOutFile);

		return $this->dynChartOutFile;
	}

	/**
	 * @name dynChart::_drawCumHistoChart()
	 * @param $dynChartValues (array)
	 * @return void
	 */
	public function _drawCumHistoChart()
	{
		/* contr��le des donn�es d'entr�e */
		if (count($this->dynChartValues)==0)
		return false;

		foreach($this->dynChartValues as $pivot=>$values)
		{
			if (!is_array($values)) return false;
		}
		if ($this->dynChartType==0)
		return false;

		// Create the graph. These two calls are always required
		//$graph = new Graph($this->dynChartWidth,$this->dynChartHeight,"auto");
		$graph = new Graph($this->dynChartWidth,$this->dynChartHeight,"auto");
		$graph->SetMarginColor('white');
		$graph->SetScale("textint");
		$graph->SetFrame(0);
		if ($this->border)
		{
			$graph->SetFrame(1,"blue@0.9",3);
		}
		$titre=$this->dynChartLib;
		$gJpgBrandTiming=true;

		$arPivot = array_keys($this->dynChartValues);

		$graph->xaxis->title->Set($this->axisLibs['xaxis']);
		$graph->yaxis->title->Set($this->axisLibs['yaxis']);

		// Create the linear plot
		$arCat=array();

		$idxColor=0;
		foreach($this->dynChartValues as $pivot=>$values)
		{
			$newBarPlot=new BarPlot(array_values($values));
			//$newBarPlot->value->Show();
			//$newBarPlot->SetFillGradient($this->cumColors[$idxColor][0],$this->cumColors[$idxColor][1],GRAD_VERT);

			$newBarPlot->SetFillColor($this->colors[$idxColor]);
			$newBarPlot->SetLegend($pivot);
			$arBarPlots[]=$newBarPlot;
			$xdata=array_keys($values);
			$idxColor+=1;
		}

		// Add the plot to the graph
		//$graph->Add($lineplot2);

		// Create the grouped bar plot
		$gbplot = new GroupBarPlot($arBarPlots);

		// ...and add it to the graPH
		$graph->Add($gbplot);

		$graph->title->Set($this->dynChartLib);
		$graph->xaxis->SetTickLabels($xdata);
		$graph->xaxis->SetLabelAngle(45);

		$xint=round((count($this->dynChartValues)/80),0)+1;
		$graph->xaxis->SetTextLabelInterval($xint);
		$graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,8);
		$graph->yaxis->SetFont(FF_ARIAL,FS_NORMAL,8);

		$graph->ygrid->SetFill(true,'#EFEFEF@0.5','#BBCCFF@0.5');
		$graph->xgrid->Show();

		$graph->title->SetFont(FF_ARIAL,FS_BOLD,10);

		$graph->yaxis->title->SetFont(FF_ARIAL,FS_NORMAL);
		$graph->xaxis->title->SetFont(FF_ARIAL,FS_NORMAL);
		
		$graph->yaxis->SetLabelMargin(0);
		$graph->SetMargin(130,140,50,130);
		$graph->legend->Pos(0.05,0.5,"right","top");
				
		$gbplot->SetColor("darkblue");
		$gbplot->SetWeight(1.5);

		$graph->yaxis->scale->SetGrace(20);

		// Display the graph
		
		$graph->img->SetAntiAliasing();
		$graph->Stroke($this->dynChartOutFile);

		return $this->dynChartOutFile;
	}

	public function _drawRadarChart()
	{

		/* contr�le des donn�es d'entr�e */
		if (count($this->dynChartValues)<3)
		return false;
		if ($this->dynChartType==0)
		return false;
		foreach($this->dynChartValues as $pivot=>$values)
		{
			if (is_array($values)) return false;
		}

		$titles = array_keys($this->dynChartValues);
		$data = array_values($this->dynChartValues);

		$titre=$this->dynChartLib;

		$graph = new RadarGraph ($this->dynChartWidth,$this->dynChartHeight,"auto");

		$graph->title->Set($titre);
		$graph->title->SetFont(FF_ARIAL,FS_BOLD,10);


		$graph->SetTitles($titles);
		$graph->SetCenter(0.5,0.55);
		$graph->HideTickMarks();
		$graph->SetColor('white');
		$graph->axis->SetColor('darkgray');
		$graph->grid->SetColor('darkgray');
		$graph->grid->Show();

		$graph->axis->title->SetFont(FF_ARIAL,FS_NORMAL,9);
		$graph->axis->title->SetMargin(5);
		$graph->SetGridDepth(DEPTH_BACK);
		$graph->SetSize(0.7);

		$plot = new RadarPlot($data);
		$plot->SetColor('red@0.2');
		$plot->SetLineWeight(1);
		$plot->SetFillColor('red@0.7');

		$plot->mark->SetType(MARK_IMG_SBALL,'red');

		$graph->Add( $plot);
		$graph->img->SetAntiAliasing();
		$graph->Stroke($this->dynChartOutFile);

		return $this->dynChartOutFile;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name DynChart::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>