<?php
require_once( 'xmlAnalyser.php') ;
require_once( 'library.php') ;
/**
 * Gestion des sessions
 * <p>session</p>
 *
 * @name Session
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package common
 */

class Session {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. attributs     */
	/*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var (Singleton)
	 * @desc variable pour le singleton
	 */
	private static $instance;

	/**
	 * @var $uid (int)
	 * @desc identifiant de l'utilisateur si connexion
	 */
	private $uid ;

	/**
	 * @var $username (string)
	 * @desc nom de l'utilisateur si connexion
	 */
	private $username ;

	/**
	 * @var  $gid (int)
	 * @desc identifiant du groupe principal de l'utilisateur si connexion
	 */
	private $gid ;

	/**
	 * @var $modid (int)
	 * @desc identifiant du module utilis�
	 */
	private $modid ;

	/**
	 * @var $xtpl_file (string)
	 * @desc fichier du template
	 */
	private $xtpl_file ;

	/**
	 * @var $xtpl_path (string)
	 * @desc chemin du template
	 */
	private $xtpl_path ;

	/**
	 * @var  $sessid (string)
	 * @desc identifiant de la session g�n�r� par le serveur
	 */
	private $sessid ;

	/**
	 * @var $logID (int)
	 * @desc identifiant de la session dans la bdd
	 */
	private $logID ;

	/**
	 * @var $pageCount (int)
	 * @desc compteur de page
	 */
	private $pageCount ;

	/**
	 * @var $moduleArray (array)
	 * @desc tableau des modules disponibles
	 */
	private $moduleArray ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Session::__construct()
	 * @return void
	 */
	public function __construct() {
		$this->_startSession() ;
		$this->config_file=	'config.xml';
		$this->xtpl_file = 'main.xtpl' ;
		$this->xtpl_path = $this->lang.'/main/' ;
		
		$this->modid=0;
		$this->page_count=0;
	}
	/**
	 * Singleton
	 *
	 * <p>cr�ation de l'instance de la classe si n'existe pas</p>
	 *
	 * @name Session::_GetInstance()
	 * @return Session
	 */
	public static function _GetInstance()
	{
		if(!isset(self::$instance))
		{
			self::$instance = new Session();
		}
		return self::$instance;
	}
	/*~*~*~*~*~*~*~*~*~*~*~*~*~*/
	/*  2.1 m�thodes priv�es   */
	/*~*~*~*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * Initialise la session
	 *
	 * <p>initialisation de la session</p>
	 *
	 * @name session::_startSession()
	 * @return void
	 */
	private function _startSession()
	{
		if (session_id()==NULL)
		session_start() ;
		$this->sessid = session_id() ;
		$this->uid = $_SESSION['uid'] ;
		$this->username = $_SESSION['name'] ;
		$this->lang = $_SESSION['lang'] ;
		if (isset($_SESSION['modid']))
		$this->modid = $_SESSION['modid'] ;
		if (isset($_SESSION['page_count']))
		$this->pageCount = $_SESSION['page_count'] ;
		$this->logID = $_SESSION['logID'] ;
		// d�placement pour correctif access denied
		//$this->moduleArray = $this->_getModules() ;
		 
		if(($this->uid == 0)||($this->username=='')||($this->lang == ''))
		{
			$this->_sessionDestroy();
			header('Location: '.ROOT_APPL.'/index.html') ;
		}
		// nouvel emplacement du getModules
		$this->moduleArray = $this->_getModules() ;

		setcookie ("uid", $this->uid, time() + 3600);

	}
	/*~*~*~*~*~*~*~*~*~*~*~*~*~*/
	/*  2.1 m�thodes publiques */
	/*~*~*~*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Accesseur en lecture
	 *
	 * <p>Acc�s � la variable username</p>
	 * @name Session::_getUsername()
	 * @return string
	 */
	public function _getUsername()
	{
		return $this->username ;
	}
	/**
	 * Accesseur en lecture
	 *
	 * <p>Acc�s � la variable uid</p>
	 * @name Session::_getUid()
	 * @return int
	 */
	public function _getUid()
	{
		return $this->uid ;
	}
	/**
	 * Accesseur en lecture
	 *
	 * <p>Acc�s � la variable lang</p>
	 * @name Session::_getLang()
	 * @return string
	 */
	public function _getLang()
	{
		return $this->lang ;
	}

	/**
	 * Accesseur en lecture
	 *
	 * <p>Acc�s � la variable modid</p>
	 * @name Session::_getModid()
	 * @return int
	 */
	public function _getModid()
	{
		return $this->modid ;
	}
	/**
	 * Accesseur en �criture
	 */

	/**
	 * <p>Mise � jour de la variable modid</p>
	 * @name Session::_setModid()
	 * @return void
	 */
	public function _setModid($newmodid)
	{
		$_SESSION['modid'] = $newmodid ;
		$this->modid = $newmodid ;
	}

	public function _setPageCount()
	{
		//v1.5-PPR-#000 suppression message notice
		if (!isset($_SESSION['page_count']))
			$_SESSION['page_count'] =0 ;
		$_SESSION['page_count'] += 1 ;
		$this->pageCount = $_SESSION['page_count'] ;
	}
	/**
	 * service
	 *
	 * <p>cr�ation d'un tableau des modules accessibles par l'utilisateur</p>
	 *
	 * @name Session::__getModules()
	 * @return array
	 */
	public function _getModules()
	{
		$myrights = new moduleRights() ;
		$moduleArray = $myrights->_loadModules($this->uid) ;
		return $moduleArray ;
	}



	/**
	 * <p>Enregistrement des sessions dans la bdd</p>
	 *
	 * @name Session::_sessionLog()
	 * @return void
	 */
	public function _sessionLog()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'INSERT INTO appl_session_log SET ';
		$sql .= 'SESSID  = \''.$this->sessid.'\', ';
		$sql .= 'INT_COD = \''.$this->uid.'\', ';
		$sql .= 'LANG = \''.$this->lang.'\', ';
		$sql .= 'DATE_START = \''.date('Y-m-d H:i:s').'\', ';
		$sql .= 'ADDRESS_IP = \''.$_SERVER['REMOTE_ADDR'].'\' ' ;
		$res = $maconnexion->_bddQueryLastId($sql) ;
		$_SESSION['logID'] = $res ;
		$this->logID = $res ;
	}

	/**
	 * <p>Enregistrement de la navigation dans la bdd</p>
	 *
	 * @name Session::_sessionLogAction()
	 * @return void
	 */
	public function _sessionLogAction($modid='0', $action='afficher')
	{
		$this->_setPageCount();
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'INSERT INTO appl_session_log_action SET ';
		$sql .= 'SESSLOG_ID  = \''.$this->logID.'\', ';
		$sql .= 'INT_COD = \''.$this->uid.'\', ';
		$sql .= 'MOD_ID = \''.$modid.'\', ';
		$sql .= 'TIME_PAGE = \''.time().'\', ';
		$sql .= 'URL_PAGE = \''.$_SERVER['PHP_SELF'].'\', ';
		$sql .= 'PAGE_ORDER = \''.$this->pageCount.'\', ' ;
		$sql .= 'ACTION = \''.$action.'\' ' ;
		$res = $maconnexion->_bddQuery($sql) ;
	}


	/**
	 * Suppression de la session et de ses variables
	 *
	 * <p>Suppression session</p>
	 *
	 * @name Session::_sessionDestroy()
	 * @return void
	 */
	private function _sessionDestroy()
	{
		if ($this->sessid != "" || isset($_COOKIE[session_name()]))
		{
			setcookie (session_name(), "", time() - 3600);
			$_SESSION = array();
			session_destroy();
		}

	}

	/**
	 * <p>Construction de la page principale</p>
	 *
	 * @name Session::_makeMainPage()
	 * @param $content (String)
	 * @return void
	 */
	public function _makeMainPage($content='', $menuleft='')
	{
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('ROOT_APPL', ROOT_APPL);
		$xtpl->assign('USER_NAME', $this->username);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		 
		foreach($this->moduleArray as $key=>$value)
		{
			if($this->modid == $key)
			{
				$xtpl->parse('main.module.actif') ;
			}
			//$xtpl->assign('ITEM_TITLE', strtoupper($this->moduleArray[$key]['title'])) ;
			$xtpl->assign('ITEM_TITLE', $this->moduleArray[$key]['title']) ;
			$xtpl->assign('ITEM_LINK', $this->moduleArray[$key]['link']) ;
			$xtpl->parse('main.module') ;
		}
		if($menuleft != '')
		{
			$xtpl->assign('MENU_LEFT', $menuleft);
			$xtpl->parse('main.menuleft') ;
			$xtpl->assign('SCREEN_CONTENT', 'main_content');
		}else{
			$xtpl->assign('SCREEN_CONTENT', 'full_content');
		}
		$xtpl->assign('MAIN_CONTENT', $content);
		$xtpl->assign('APP_NAME', NOM_APPL) ;
		$xtpl->assign('VERSION', VERSION_APPL);
		$xtpl->assign('YEAR', YEAR_APPL) ;
		$xtpl->parse('main');
		$xtpl->out('main');
	}

	/**
	 * <p>Construction de la page d'erreur</p>
	 *
	 * @name Session::_makeMainPage()
	 * @param $errorString (String)
	 * @return void
	 */
	public function _makeErrorPage($errorString)
	{
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('ROOT_APPL', ROOT_APPL);
		$xtpl->assign('USER_NAME', $this->username);

		foreach($this->moduleArray as $key=>$value)
		{
			$xtpl->assign('ITEM_TITLE', $this->moduleArray[$key]['title']) ;
			$xtpl->assign('ITEM_LINK', $this->moduleArray[$key]['link']) ;
			$xtpl->parse('main.module') ;
		}
		$xtpl->assign('SCREEN_CONTENT', 'full_content');
		 
		$xtpl->assign('ERROR_STRING', $errorString);
		$xtpl->parse('main.error');
		$xtpl->assign('APP_NAME', NOM_APPL) ;
		$xtpl->assign('VERSION', VERSION_APPL);
		$xtpl->assign('YEAR', YEAR_APPL) ;
		$xtpl->parse('main');
		$xtpl->out('main');
	}

	/**
	 * Enregistrement d'une variable dans la variable $_SESSION
	 *
	 * @name Session::_setSessionValue()
	 * @param $strid (string)
	 * @param $val ()
	 * @return void
	 */
	public function _setSessionValue($name, $value)
	{
		$_SESSION[$name] = $value ;
	}

	/**
	 * Recup�ration d'une variable dans la variable $_SESSION
	 *
	 * @name Session::_getSessionValue()
	 * @param name (string)
	 * @return $value
	 */
	public function _getSessionValue($name)
	{
		if($_SESSION[$name] != NULL)
		{
			return $_SESSION[$name];
		}
	}

	public function _getConfigValue($name)
	{
		if ($_SESSION['config']!=NULL)
		{
			$data = $_SESSION['config'] ;
		}else{
			$x = new XmlAnalyser() ;
			$data = $x->_getDataXmlInArray(ROOT_CONFIG.$this->config_file) ;
			$_SESSION['config'] = $data ;
		}
		return $data[$name] ;
	}

   /* OutilsDev : affichage d'un tableau formatté
    * <p>OutilsDev : affichage d'un tableau formatté</p>
    * 
    * @name className::_aff_pr()
    * @param $ar (Array)
    * @return $str (String)
    */
    public function _affPr($ar) {
    		$str="<hr>";
			$str.="<pre>";
			$str.=print_r($ar,true);
			$str.="</pre>";
			return $str;
    }

	public function _trimUltime($chaine){
	$chaine = trim($chaine);
	$chaine = str_replace("\t", " ", $chaine);
	return $chaine;
	}

 
	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name className::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>