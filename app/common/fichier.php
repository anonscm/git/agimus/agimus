<?php

require_once('../common/msgException.php'); 
  
/**
 * 
 * <p>Manipulation de fichiers</p>
 * 
 * @name Document
 * @author Marie Lawani <mlawani@capella-conseil.com> 
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright Capella Conseil 2009
 * @version 1.0.0
 * @package common
 */

 class Fichier {
 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. propri�t�s    */
    /*~*~*~*~*~*~*~*~*~*~*/
    /**
    * @var maxFileSize(int)
    * @desc Taille maximum des fichiers
    */
    private $maxFileSize;
    
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name Fichier::__construct()
    * @return void 
    */
    public function __construct() {
		$this->_getFileConfig();
    }

    public function _getMaxFileSize()
    {
    	return $this->maxFileSize;
    }
    
    /**
    * Upload d'un fichier
    * Enregistre le fichier dans le r�pertoire cible par�s les �ventuelles corrections de caract�res accentu�s
    * 
    * <p>Uploader un fichier</p>
    * 
    * @name Fichier::_uploadFile()
    * @param $target_dir(string)
    * 	Chemin du r�pertoire cible
    * @return string
    * 	renvoie le chemin complet du fichier (ex: /www/filedirectory/myfile.txt)
    */    
     public function _uploadFile($target_dir, $formfilenm='userfile')
    {
		$FileTMP = $_FILES[$formfilenm]['tmp_name'];
      	if ( !file_exists($FileTMP) )
      	{
      		throw new msgException('_FILE_UPLOAD_FILETMP_NOT_FOUND_') ;
      	}elseif((filesize($FileTMP)) > $this->maxFileSize){
			throw new msgException('_FILE_UPLOAD_TOO_BIG_') ;
      	}else{
        	// copie du nouveau fichier
        	$fileName = $this->_removeAccent($_FILES[$formfilenm]['name']);
			$newPath = $target_dir.basename($fileName) ;
			//On verifie ici que le newpath n'existe pas d�j�
			if(is_file($newPath))
			{
	      		throw new msgException('_FILE_UPLOAD_ALREADY_EXISTS_');			
			}elseif (! move_uploaded_file($FileTMP, $newPath))
         	{
	      		throw new msgException('_FILE_UPLOAD_FAILED_');
         	}else{
            	chmod($newPath, 0777);
            	return $newPath ;
         	}
      	}
    }
 
    
     /**
    * Suppression d'un fichier
    * 
    * <p>Supprimer un fichier</p>
    * 
    * @name Fichier::_deleteFile()
    * @param $directory (string)
    * 	Chemin physique du fichier sur le serveur (ex: /www/filedirectory/)
    * @param $fileName
    * 	Nom du fichier (ex: myfile.txt)
    * @return void
    */    
     public function _deleteFile($directory, $file)
    {
        $filePath = $directory.basename($file) ;
        if((is_file($filePath))&&(!unlink($filePath)))
        {
        	throw new msgException('_FILE_DELETE_FAILED_') ;
        }
    }

    /**
    * T�l�chargement d'un fichier
    * 
    * <p>T�l�charger un fichier un fichier</p>
    * 
    * @name Fichier::_downloadFile()
    * @param $directory (string)
    * 	Chemin physique du fichier sur le serveur (ex: /www/filedirectory/)
    * @param $fileName
    * 	Nom du fichier (ex: myfile.txt)
    * @return void
    */ 
     public function _downloadFile($directory, $fileName)
    {
        $filePath = $directory.basename($fileName) ;

       	if(file_exists($filePath))
       	{
       		$path_parts = pathinfo($filePath);
       		$extension = $path_parts['extension'] ;	     
      		$mimeArray = $this->_buildMimeArray() ;

		    header("Pragma: public\n");
		    header("Cache-Control: max-age=0\n");//permet d'ouvrir directement le fichier download�
		    header("Content-type: " .$mimeArray[$extension]. "\n");
		    header("Content-disposition: attachment; filename=" .$fileName. "\n");
		    header("Content-transfer-encoding: binary\n");
		    header("Content-length: " . filesize($filePath) . "\n");
		
		    $fp=fopen($filePath, "r");
		    fpassthru($fp);
       	}else{
       		throw new msgException('_FILE_DOWNLOAD_ERROR_NOT_EXISTS_ON_SERVER_') ;
       	}
    }
    
    
    /**
    * Remplacement des caract�res accentu�s du nom d'un document
    * 
    * <p>Remplacer les caract�res d'un document</p>
    * 
    * @name Fichier::_removeAccent()
    * @param $filename (string)
    * @return string
    */     
	public function _removeAccent($filename)
	{
		$newfilename = strtr(trim($filename), "����������������������������������������������������� ", "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn_" ) ;
	 	return $newfilename ;
	}
	
 

	/**
    * Construction d'un tableau des types MIME
    * 
    * <p>Construire un tableau des types MIME</p>
    * 
    * @name Fichier::_buildMimeArray()
    * @return array
    */
	public function _buildMimeArray()
   {
      $mimeArray = array(  ".avi" => "video/avi",
                                        ".bmp" => "image/bmp",
                                        ".class" => "java/*",
                                        ".crl" => "application/pkix-crl",
                                        ".crt" => "application/x-x509-ca-cert",
                                        ".css" => "text/css",
                                        ".dib" => "image/bmp",
                                        ".doc" => "application/msword",
                                        ".dot" => "application/msword",
                                        ".eml" => "message/rfc822",
                                        ".eps" => "application/postscript",
                                        ".exe" => "application/x-msdownload",
                                        ".gif" => "image/gif",
                                        ".gz" => "application/x-compressed",
                                        ".htm" => "text/html",
                                        ".html" => "text/html",
                                        ".ico" => "image/x-icon",
                                        ".jpe" => "image/jpeg",
                                        ".jpeg" => "image/jpeg",
                                        ".jpg" => "image/jpeg",
                                        ".JS" => "application/x-javascript",
                                        ".latex" => "application/x-latex",
                                        ".m1v" => "video/mpeg",
                                        ".m3u" => "audio/x-mpegurl",
                                        ".mdb" => "application/msaccess",
                                        ".mht" => "message/rfc822",
                                        ".mhtml" => "message/rfc822",
                                        ".mid" => "audio/mid",
                                        ".midi" => "audio/mid",
                                        ".mix" => "image/x-xbitmap",
                                        ".movie" => "video/x-sgi-movie",
                                        ".mp2" => "video/mpeg",
                                        ".mp2v" => "video/mpeg",
                                        ".mp3" => "audio/mpeg",
                                        ".mpa" => "video/mpeg",
                                        ".mpe" => "video/mpeg",
                                        ".mpeg" => "video/mpeg",
                                        ".mpg" => "video/mpeg",
                                        ".mpv2" => "video/mpeg",
                                        ".pdf" => "application/pdf",
                                        ".png" => "image/png",
                                        ".ps" => "application/postscript",
                                        ".rar" => "application/x-rar-compressed",
                                        ".rmi" => "audio/mid",
                                        ".rtf" => "application/msword",
                                        ".swf" => "application/x-shockwave-flash",
                                        ".tar" => "application/x-compressed",
                                        ".tgz" => "application/x-compressed",
                                        ".tif" => "image/tiff",
                                        ".tiff" => "image/tiff",
                                        ".txt" => "text/plain",
                                        ".wav" => "audio/wav",
                                        ".wiz" => "application/msword",
                                        ".wm" => "video/x-ms-wm",
                                        ".wmv" => "video/x-ms-wmv",
                                        ".xls" => "application/vnd.ms-excel",
                                        ".xml" => "text/xml",
                                        ".xsl" => "text/xml",
                                        ".zip" => "application/x-zip-compressed",
                                        ".asx" => "video/x-ms-asf",
                                        ".avi" => "video/x-msvideo",
                                        ".bmp" => "image/x-bmp",
                                        ".crt" => "application/pkix-cert",
                                        ".dib" => "image/x-bmp",
                                        ".dif" => "video/x-dv",
                                        ".gz" => "application/x-gzip",
                                        ".java" => "text/java",
                                        ".m3u" => "audio/mpegurl",
                                        ".mac" => "image/x-macpaint",
                                        ".mov" => "video/quicktime",
                                        ".mpga" => "audio/mpeg",
                                        ".pct" => "image/pict",
                                        ".pic" => "image/pict",
                                        ".pict" => "image/pict",
                                        ".pntg" => "image/x-macpaint",
                                        ".POT" => "application/vnd.ms-powerpoint",
                                        ".ppa" => "application/vnd.ms-powerpoint",
                                        ".pps" => "application/vnd.ms-powerpoint",
                                        ".ppt" => "application/x-mspowerpoint",
                                        ".pwz" => "application/vnd.ms-powerpoint",
                                        ".qt" => "video/quicktime",
                                        ".qti" => "image/x-quicktime",
                                        ".qtif" => "image/x-quicktime",
                                        ".qtl" => "application/x-quicktimeplayer",
                                        ".ra" => "audio/vnd.rn-realaudio",
                                        ".ram" => "audio/x-pn-realaudio",
                                        ".rm" => "application/vnd.rn-realmedia",
                                        ".smi" => "application/smil",
                                        ".smil" => "application/smil",
                                        ".svg" => "image/svg-xml",
                                        ".svgz" => "image/svg-xml",
                                        ".tar" => "application/x-tar",
                                        ".tga" => "image/x-targa",
                                        ".vfw" => "video/x-msvideo",
                                        ".xls" => "application/x-msexcel",
                                        ".xpl" => "audio/mpegurl");
      return $mimeArray ;
   }
	
   public function _getMimeFile($filename)
   {
   	$newfname = explode('.', $filename);
   	$extension = $newfname[sizeof($newfname)-1] ;
   	return $extension ;
   }
   private function _getFileConfig()
    { 
    	$data = $_SESSION['config'] ;
    	$this->maxFileSize = $data['max_file_size'] ;

    }
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /*  2.1 m�thodes publiques */
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Destructeur
    * 
    * <p>Destruction de l'instance de classe</p>
    * 
    * @name className::__destruct()
    * @return void
    */
    public function __destruct() {
    
    }
 } 
?>