<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * <p>Acces LDAP</p>
 *
 * @name  ldap
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

class ldap {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (String)
	 * @desc Identifiant
	 */
	private $uname;

	/**
	 * @var (String)
	 * @desc Mot de passe
	 */
	private $pwd;

	/**
	 * @var (String)
	 * @desc URL du serveur LDAP
	 */
	private $ldapURL;

	/**
	 * @var (Int)
	 * @desc Connexion LDAP
	 */
	private $ldapCnx;

	/**
	 * @var (String)
	 * @desc LDAP DN
	 */
	private $baseDN;

	/**
	 * @var (string)
	 * @desc Filtre de selection
	 */
	private $filter;

	/**
	 * @var (array)
	 * @desc Liste des attributs � r�cup�rer
	 */
	private $attributeArray;

	/**
	 * @var (Int)
	 * @desc Connexion id recherche
	 */
	private $ldapSearchId;
	

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name ldap::__construct()
	 * @return void
	 */
	public function __construct() {
		$this->uname='';
		$this->pwd='';
		$this->ldapURL='';
		$this->ldapCnx=0;
		$this->baseDN='';
		$this->AttributeArray=array();
	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name ldap::_getUname()
	 * @return String
	 */
	public function _getUname() {
		return $this->uname ;
	}

	/**
	 * @name ldap::_getPwd()
	 * @return String
	 */
	public function _getPwd() {
		return $this->pwd ;
	}

	/**
	 * @name ldap::_getLdapURL()
	 * @return String
	 */
	public function _getLdapURL() {
		return $this->ldapURL ;
	}

	/**
	 * @name ldap::_getLdapCnx()
	 * @return Int
	 */
	public function _getLdapCnx() {
		return $this->ldapCnx ;
	}

	/**
	 * @name ldap::_getBaseDN()
	 * @return String
	 */
	public function _getBaseDN() {
		return $this->baseDN ;
	}

	/**
	 * @name ldap::_getFilter()
	 * @return string
	 */
	public function _getFilter() {
		return $this->filter ;
	}

	/**
	 * @name ldap::_getAttributeArray()
	 * @return array
	 */
	public function _getAttributeArray() {
		return $this->attributeArray ;
	}

	/**
	 * @name ldap::_getLdapSearchId()
	 * @return Int
	 */
	public function _getLdapSearchId() {
		return $this->ldapSearchId ;
	}
	

	/**
	 * Accesseurs en �criture
	 */

	/**
	 * @name ldap::_setUname()
	 * @param $uname (String)
	 * @return void
	 */
	public function _setUname($uname) {
		$this->uname  = $uname ;
	}

	/**
	 * @name ldap::_setPwd()
	 * @param $pwd (String)
	 * @return void
	 */
	public function _setPwd($pwd) {
		$this->pwd  = $pwd ;
	}

	/**
	 * @name ldap::_setLdapURL()
	 * @param $ldapURL (String)
	 * @return void
	 */
	public function _setLdapURL($ldapURL) {
		$this->ldapURL  = $ldapURL ;
	}

	/**
	 * @name ldap::_setLdapCnx()
	 * @param $ldapCnx (Int)
	 * @return void
	 */
	public function _setLdapCnx($ldapCnx) {
		$this->ldapCnx  = $ldapCnx ;
	}

	/**
	 * @name ldap::_setBaseDN()
	 * @param $baseDN (String)
	 * @return void
	 */
	public function _setBaseDN($baseDN) {
		$this->baseDN  = $baseDN ;
	}

	/**
	 * @name ldap::_setFilter()
	 * @param $filter (string)
	 * @return void
	 */
	public function _setFilter($filter) {
		$this->filter  = $filter ;
	}

	/**
	 * @name ldap::_setAttributeArray()
	 * @param $attributeArray (array)
	 * @return void
	 */
	public function _setAttributeArray($attributeArray) {
		$this->attributeArray  = $attributeArray ;
	}



	/**
	 * Compte du nombre d'occurence de l'objet ldap dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name ldap::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_ldap ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Cr�ation d'un ldap dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name ldap::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM t_ldap ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
			
		$sql  = 'INSERT INTO t_ldap VALUES( ';
		$sql .= '\''.$this->uname.'\', ';
		$sql .= '\''.$this->pwd.'\', ';
		$sql .= '\''.$this->ldapURL.'\', ';
		$sql .= '\''.AddSlashes($this->ldapCnx).'\', ';
		$sql .= '\''.$this->baseDN.'\', ';
		$sql .= '\''.AddSlashes($this->AttributeArray).'\' ';

		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise � jour d'un ldap dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name ldap::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_ldap SET ';
		$sql .= 'UNAME = \''.$this->uname.'\', ';
		$sql .= 'PWD = \''.$this->pwd.'\', ';
		$sql .= 'LDAPURL = \''.$this->ldapURL.'\', ';
		$sql .= 'LDAPBIND = \''.AddSlashes($this->ldapCnx).'\', ';
		$sql .= 'BASEDN = \''.$this->baseDN.'\', ';
		$sql .= 'ATTRIBUTEARRAY = \''.AddSlashes($this->AttributeArray).'\' ';

		$sql .= 'WHERE id=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un ldap dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name ldap::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$sql  = 'DELETE FROM t_ldap ';
		$sql .= 'WHERE id = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}
	/**
	 * R�cup�ration de la liste des ldaps
	 *
	 * <p>Liste des ldap</p>
	 *
	 * @name ldap::_getList()
	 * @return array
	 */
	public function _getList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_ldap ' ;
		$sql .= 'ORDER BY LIB ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['UNAME'] = StripSlashes($row['UNAME']) ;
				$listArray[$row['ID']]['PWD'] = StripSlashes($row['PWD']) ;
				$listArray[$row['ID']]['LDAPURL'] = StripSlashes($row['LDAPURL']) ;
				$listArray[$row['ID']]['LDAPBIND'] = StripSlashes($row['LDAPBIND']) ;
				$listArray[$row['ID']]['BASEDN'] = StripSlashes($row['BASEDN']) ;
				$listArray[$row['ID']]['ATTRIBUTEARRAY'] = StripSlashes($row['ATTRIBUTEARRAY']) ;

			}
		}
		return $listArray ;
	}

	/**
	 * Connexion au serveur LDAP
	 *
	 * <p>Connexion au serveur LDAP</p>
	 *
	 * @name ldap::_connect()
	 * @return void
	 */
	public function _connect() {

		$debug=false;
		// connect to ldap server
		if ($debug) ldap_set_option(NULL, LDAP_OPT_DEBUG_LEVEL, 7);
		$this->ldapCnx = ldap_connect($this->ldapURL,389) or die("Could not connect to LDAP server.");
		//positionne les options
		ldap_set_option($this->ldapCnx, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_set_option($this->ldapCnx, LDAP_OPT_REFERRALS, 0);
		return true;
	}

	/**
	 * Deconnexion du serveur LDAP
	 *
	 * <p>Deconnexion di serveur LDAP</p>
	 *
	 * @name ldap::_disconnect()
	 * @return void
	 */
	public function _disconnect() {

		if (isset($this->ldapCnx))
		ldap_close($this->ldapCnx);
	}

	/**
	 * Recherche dans l'annuaire LDAP
	 *
	 * <p>Recherche dans l'annuaire LDAP</p>
	 *
	 * @name ldap::_search()
	 * @return array
	 */
	public function _search() {

		$debug=false;
		
		// using ldap bind
		$ldaprdn  = $this->uname;     // ldap rdn or dn
		$ldappass = $this->pwd;  // associated password

		$resultArray=array();

		if ($debug)
		echo "\r\nldapCnx :$this->ldapCnx $this->uname $this->pwd";

		if ($this->ldapCnx) {
			// binding to ldap server
			$bind = ldap_bind($this->ldapCnx,$ldaprdn,$ldappass);
			if (!$bind)
			{
				$mess.="LDAP-Errno: " . ldap_errno($this->ldapCnx) . "\n";
				$mess.="LDAP-Error: " . ldap_error($this->ldapCnx) . "\n";
				echo "\r\n$mess";
				die();
			}
			else
			{
				$this->ldapSearchId=ldap_search($this->ldapCnx, $this->baseDN, $this->filter,$this->attributeArray);
				if ($debug)	echo "\r\nLDAP:Recherche effectuee sur [".$this->filter."] : ".$this->ldapSearchId;
				$resultArray=ldap_get_entries($this->ldapCnx,$this->ldapSearchId);
			}
		}
		return $resultArray;
	}


	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name ldap::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>