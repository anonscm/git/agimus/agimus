<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * <p>Denombrement</p>
 *
 * @name  Denombrement
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package
 */

class Denombrement {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc Identifiant
	 */
	private $id;

	/**
	 * @var (String)
	 * @desc R�f�rence de la dimension
	 */
	private $refDim;

	/**
	 * @var (Int)
	 * @desc Identifiant de la dimension
	 */
	private $idDim;

	/**
	 * @var (Int)
	 * @desc Nombre d'occurences
	 */
	private $nbrOcc;

	/**
	 * @var (Date)
	 * @desc Date de l'occurence
	 */
	private $dateOcc;

	/**
	 * @var (String)
	 * @desc ID Structure
	 */
	private $idStruct;

	/**
	 * @var (String)
	 * @desc ID Structure Parente
	 */
	private $idStructPere;


	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Denombrement::__construct()
	 * @return void
	 */
	public function __construct($idRef=0,$idStructRef='') {
		$this->id=0;
		$this->refDim='';
		$this->idDim=0;
		$this->nbrOcc=0;
		$this->dateOcc='';
		$this->idStruct='';
		$this->idStructPere='';


		if (($idRef>0) and ($idStructRef!=''))
		{
			$this->id=$idRef;
			$this->idStruct=$idStructRef;
			$this->_fill();
		}

	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name Denombrement::_getId()
	 * @return Int
	 */
	public function _getId() {
		return $this->id ;
	}

	/**
	 * @name Denombrement::_getRefDim()
	 * @return String
	 */
	public function _getRefDim() {
		return $this->refDim ;
	}

	/**
	 * @name Denombrement::_getIdDim()
	 * @return Int
	 */
	public function _getIdDim() {
		return $this->idDim ;
	}

	/**
	 * @name Denombrement::_getNbrOcc()
	 * @return Int
	 */
	public function _getNbrOcc() {
		return $this->nbrOcc ;
	}

	/**
	 * @name Denombrement::_getDateOcc()
	 * @return Date
	 */
	public function _getDateOcc() {
		return $this->dateOcc ;
	}

	/**
	 * @name Denombrement::_getIdStruct()
	 * @return String
	 */
	public function _getIdStruct() {
		return $this->idStruct ;
	}

	/**
	 * @name Denombrement::_getIdStructPere()
	 * @return String
	 */
	public function _getIdStructPere() {
		return $this->idStructPere ;
	}


	/**
	 * Accesseurs en ecriture
	 */
	/**

	*
	* @name Denombrement::_setId()
	* @param $id (Int)
	* @return void
	*/
	public function _setId($id) {
		$this->id  = $id ;
	}

	/**
	 * @name Denombrement::_setRefDim()
	 * @param $refDim (String)
	 * @return void
	 */
	public function _setRefDim($refDim) {
		$this->refDim  = $refDim ;
	}

	/**
	 * @name Denombrement::_setIdDim()
	 * @param $idDim (Int)
	 * @return void
	 */
	public function _setIdDim($idDim) {
		$this->idDim  = $idDim ;
	}

	/**
	 * @name Denombrement::_setNbrOcc()
	 * @param $nbrOcc (Int)
	 * @return void
	 */
	public function _setNbrOcc($nbrOcc) {
		$this->nbrOcc  = $nbrOcc ;
	}

	/**
	 * @name Denombrement::_setDateOcc()
	 * @param $dateOcc (Date)
	 * @return void
	 */
	public function _setDateOcc($dateOcc) {
		$this->dateOcc  = $dateOcc ;
	}

	/**
	 * @name Denombrement::_setIdStruct()
	 * @param $idStruct (String)
	 * @return void
	 */
	public function _setIdStruct($idStruct) {
		$this->idStruct  = $idStruct ;
	}

	/**
	 * @name Denombrement::_setIdStructPere()
	 * @param $idStructPere (String)
	 * @return void
	 */
	public function _setIdStructPere($idStructPere) {
		$this->idStructPere  = $idStructPere ;
	}

	/**
	 * Compte du nombre d'occurence de l'objet Denombrement dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name Denombrement::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM r_denombrement ';
		if ($this->idStruct!='')
		$sql .= " WHERE id_struct = '$this->idStruct' ";		
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	public function _existsRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM r_denombrement ';
		$sql .= 'WHERE 1 ';
		$sql .= 'AND ID_DIM=\''.$this->idDim.'\' ';
		$sql .= 'AND ID_STRUCT=\''.$this->idStruct.'\' ';
		$sql .= 'AND REF_DIM=\''.$this->refDim.'\' ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
				
			if ($nbr>0)
			return true;
			else
			return false;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}


	/**
	 * Cr�ation d'un Denombrement dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name Denombrement::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM r_denombrement ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}

		// alimentation de la structure mère
		if ($this->idStructPere=='') {
			$newStruct=new Structure($this->idStruct);
			$this->idStructPere=$newStruct->_getParent();
		}

		$sql  = 'INSERT INTO r_denombrement VALUES( ';
		$sql .= '\''.AddSlashes($this->id).'\', ';
		$sql .= '\''.AddSlashes($this->refDim).'\', ';
		$sql .= '\''.AddSlashes($this->idDim).'\', ';
		$sql .= '\''.AddSlashes($this->nbrOcc).'\', ';
		$sql .= '\''.AddSlashes($this->dateOcc).'\', ';
		$sql .= '\''.AddSlashes($this->idStruct).'\', ';
		$sql .= '\''.AddSlashes($this->idStructPere).'\' ';

		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise � jour d'un Denombrement dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name Denombrement::_update()
	 * @return void
	 */
	public function _update()
	{
		// alimentation de la structure mère
		if ($this->idStructPere=='') {
			$newStruct=new Structure($this->idStruct);
			$this->idStructPere=$newStruct->_getParent();
		}

		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE r_denombrement SET ';
		$sql .= 'ID = \''.AddSlashes($this->id).'\', ';
		$sql .= 'REF_DIM = \''.AddSlashes($this->refDim).'\', ';
		$sql .= 'ID_DIM = \''.AddSlashes($this->idDim).'\', ';
		$sql .= 'NBR_OCC = \''.AddSlashes($this->nbrOcc).'\', ';
		$sql .= 'DATE_OCC = \''.AddSlashes($this->dateOcc).'\', ';
		$sql .= 'ID_STRUCT = \''.AddSlashes($this->idStruct).'\', ';
		$sql .= 'ID_STRUCT_PERE = \''.AddSlashes($this->idStructPere).'\' ';

		$sql .= 'WHERE id=\''.$this->id.'\' and id_struct=\''.$this->idStruct.'\'';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un Denombrement dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name Denombrement::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef,$idStruct)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'DELETE FROM r_denombrement ';
		$sql .= 'WHERE id = \''.$idRef.'\' and id_struct=\''.$idStruct.'\'';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}
	/**
	 * Récupération de la liste des Denombrements
	 *
	 * <p>Liste des Denombrement</p>
	 *
	 * @name Denombrement::_getList()
	 * @return array
	 */
	public function _getList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM r_denombrement ' ;
		$sql .= 'WHERE 1 ';
		if ($this->id>0)
		$sql.= ' AND id = \''.$this->id.'\' ';
		if ($this->idStruct!='')
		$sql.= ' AND id_struct = \''.$this->idStruct.'\' ';
		$sql .= 'ORDER BY REF_DIM,ID_DIM ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$id=$row['ID'];
				$struct=$row['ID_STRUCT'];
				$listArray[$struct][$id]['REF_DIM'] = StripSlashes($row['REF_DIM']) ;
				$listArray[$struct][$id]['ID_DIM'] = StripSlashes($row['ID_DIM']) ;
				$listArray[$struct][$id]['NBR_OCC'] = StripSlashes($row['NBR_OCC']) ;
				$listArray[$struct][$id]['DATE_OCC'] = strtotime(StripSlashes($row['DATE_OCC'])) ;
				$listArray[$struct][$id]['ID_STRUCT_PERE'] = StripSlashes($row['ID_STRUCT_PERE']) ;
			}
		}
		return $listArray ;
	}

	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name Denombrement::_fill()
	 * @return void
	 */
	public function _fill()
	{
		if (($this->id==0) or ($this->idStruct=='')) return false;

		$listArray=$this->_getList();
		$this->id = $listArray[$this->idStruct][$this->id]['ID'];
		$this->refDim = $listArray[$this->idStruct][$this->id]['REF_DIM'];
		$this->idDim = $listArray[$this->idStruct][$this->id]['ID_DIM'];
		$this->nbrOcc = $listArray[$this->idStruct][$this->id]['NBR_OCC'];
		$this->dateOcc = $listArray[$this->idStruct][$this->id]['DATE_OCC'];
		$this->idStruct = $listArray[$this->idStruct][$this->id]['ID_STRUCT'];
		$this->idStructPere = $listArray[$this->idStruct][$this->id]['ID_STRUCT_PERE'];

	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Denombrement::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>