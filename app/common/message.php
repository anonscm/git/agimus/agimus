<?php
require_once( 'mysqlDatabase.php') ;
/**
 * <p>Gestion des messages</p>
 * 
 * @name Message
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package common
 */

class Message extends Exception{
	/*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/
 	/**
    * @var (Singleton)
    * @desc variable pour le singleton
    */
 	private static $instance;   
    /**
    * @var $msgString (String)
    * @desc message
    */
 	private $msgString ;
 	
 	/**
    * @var $runSession (Session)
    * @desc session ouverte par l'utilisateur
    */
 	private $runSession ;
 	   
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name Message::__construct()
    * @return void 
    */
    public function __construct($message='', $maSession) {
    	
    	parent::__construct($message);
    	$this->msgString = $message ;
    	$this->runSession = $maSession ;
    }
    
    /**
    * Singleton
    * 
    * <p>cr�ation de l'instance de la classe si n'existe pas</p>
    * 
    * @name Message::_GetInstance()
    * @return Message
    */
	public static function _GetInstance($message='', $maSession)
	{
		if(!isset(self::$instance))
		{
			self::$instance = new Message($message='', $maSession);
		}
		return self::$instance;
	}  
    
    /**
    * Recup�ration du message � afficher
    * 
    * <p>Obtenir un message</p>
    * 
    * @name Message::_getMessage()
    * @param $code (string)
    * @return string 
    */    
    public function _getMessage($code)
    {
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT im.MSG_ID, iml.LIBELLE, im.MSG_TYPE ';
		$sql .= 'FROM appl_msg as im, appl_msg_libelle as iml  ';
		$sql .= 'WHERE im.MSG_CODE = \''.$code.'\' ';
		$sql .= 'AND iml.LANG = \''.$this->runSession->_getLang().'\' ';
		$sql .= 'AND im.MSG_ID = iml.MSG_ID ' ;		
		$res = $maconnexion->_bddQuery($sql) ;
		$row = $maconnexion->_bddFetchAssoc($res) ;
		$this->msgString = StripSlashes($row['LIBELLE']) ;
		$msgType = $row['MSG_TYPE'] ;
		if($msgType == 'error')
		{
			//logue de l'erreur
			$sql1 = 'INSERT INTO appl_log_error SET ';
			$sql1 .= 'MSG_ID = \''.$row['MSG_ID'].'\', ';
			$sql1 .= 'MOD_ID = \''.$this->runSession->_getModid().'\', ';			
			$sql1 .= 'INT_COD = \''.$this->runSession->_getUid().'\', ';
			$sql1 .= 'ERROR_DATE = \''.date('Y-m-d H:i:s').'\' ';
			$res1 = $maconnexion->_bddQuery($sql1) ;			
		}
    	return 	$this->msgString ;	
    }
    
    
}
?>