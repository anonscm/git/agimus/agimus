<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * <p>Usage</p>
 *
 * @name  Usage
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright CAPELLA Conseil 2011
 * @version 1.0.0
 * @package 
 */

class Usage {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietes    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
 * @var (Int)
 * @desc Identifiant
 */
private $id;

/**
 * @var (string)
 * @desc uid crypt� SHA1
 */
private $uid;

/**
 * @var (int)
 * @desc R�f�rence de la dimension
 */
private $refDim;

/**
 * @var (int)
 * @desc Identifiant de la dimension
 */
private $idDim;

/**
 * @var (datetime)
 * @desc Date de cr�ation du tuple
 */
private $dateAcces;



	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. methodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>creation de l'instance de la classe</p>
	 *
	 * @name Usage::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		$this->id=0;
		$this->uid='';
		$this->refDim=0;
		$this->idDim=0;
		$this->dateAcces='';

		
		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}
		
	}

	/**
	 * Accesseurs en lecture
	 */

	/**
* @name Usage::_getId()
* @return Int
*/
public function _getId() {
		return $this->id ;
}

/**
* @name Usage::_getUid()
* @return string
*/
public function _getUid() {
		return $this->uid ;
}

/**
* @name Usage::_getRefDim()
* @return int
*/
public function _getRefDim() {
		return $this->refDim ;
}

/**
* @name Usage::_getIdDim()
* @return int
*/
public function _getIdDim() {
		return $this->idDim ;
}

/**
* @name Usage::_getDateAcces()
* @return datetime
*/
public function _getDateAcces() {
		return $this->dateAcces ;
}



	/**
	 * Accesseurs en ecriture
	 */

	/**
 * @name Usage::_setId()
 * @param $id (Int)
 * @return void
*/
public function _setId($id) {
	$this->id  = $id ;
}

/**
 * @name Usage::_setUid()
 * @param $uid (string)
 * @return void
*/
public function _setUid($uid) {
	$this->uid  = $uid ;
}

/**
 * @name Usage::_setRefDim()
 * @param $refDim (int)
 * @return void
*/
public function _setRefDim($refDim) {
	$this->refDim  = $refDim ;
}

/**
 * @name Usage::_setIdDim()
 * @param $idDim (int)
 * @return void
*/
public function _setIdDim($idDim) {
	$this->idDim  = $idDim ;
}

/**
 * @name Usage::_setDateAcces()
 * @param $dateAcces (datetime)
 * @return void
*/
public function _setDateAcces($dateAcces) {
	$this->dateAcces  = $dateAcces ;
}



	/**
	 * Compte du nombre d'occurence de l'objet Usage dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name Usage::_countRef()
	 * @return int
	 */
	public function _countRef($uid='',$refDim='',$idDim='',$dateAcces='')
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_usage ';
		$sql.= 'WHERE 1 ';
		if ($uid<>'')
			$sql.= 'AND UID = \''.$uid.'\' ';
		if ($refDim>0)
			$sql.= 'AND REF_DIM = \''.$refDim.'\' ';
		if ($idDim>0)
			$sql.= 'AND ID_DIM = \''.$idDim.'\' ';
		if ($dateAcces<>'')
			$sql.= 'AND DATE_ACCES = \''.$dateAcces.'\' ';
		
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

		/**
	 * renvoit l'id d'un stamp existant
	 *
	 * <p>_existsRef</p>
	 *
	 * @name Usage::_existsRef()
	 * @param $lib string
	 * @return int
	 */
	public function _existsRef($lib)
	{
		$idRef = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT id ';
		$sql .= ' FROM t_usage ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$idRef  = $row['id'] ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		return $idRef;
	}
	
	/**
	 * Creation d'un Usage dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name Usage::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM t_usage ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		 
		$sql  = 'INSERT INTO t_usage VALUES( ';
		$sql .= '\''.AddSlashes($this->id).'\', ';  
 		$sql .= '\''.AddSlashes($this->uid).'\', ';  
 		$sql .= '\''.AddSlashes($this->refDim).'\', ';  
 		$sql .= '\''.AddSlashes($this->idDim).'\', ';  
 		$sql .= '\''.AddSlashes($this->dateAcces).'\' ';  

		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise e jour d'un Usage dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name Usage::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_usage SET ';
		$sql .= 'ID = \''.AddSlashes($this->id).'\', ';  
 		$sql .= 'UID = \''.AddSlashes($this->uid).'\', ';  
 		$sql .= 'REF_DIM = \''.AddSlashes($this->refDim).'\', ';  
 		$sql .= 'ID_DIM = \''.AddSlashes($this->idDim).'\', ';  
 		$sql .= 'DATE_ACCES = \''.AddSlashes($this->dateAcces).'\' ';  

		$sql .= 'WHERE id=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un Usage dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name Usage::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'DELETE FROM t_usage ';
		$sql .= 'WHERE id = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}
	/**
	 * Recuperation de la liste des Usages
	 *
	 * <p>Liste des Usage</p>
	 *
	 * @name Usage::_getList()
	 * @return array
	 */
	public function _getList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_usage ' ;
		if ($this->id>0)
		$sql.= ' WHERE id = \''.$this->id.'\' ';
		$sql .= 'ORDER BY REF_DIM, ID_DIM, DATE_ACCES ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				 $listArray[$row['ID']]['ID'] = StripSlashes($row['ID']) ;  
				 $listArray[$row['ID']]['UID'] = StripSlashes($row['UID']) ;  
				 $listArray[$row['ID']]['REF_DIM'] = StripSlashes($row['REF_DIM']) ;  
				 $listArray[$row['ID']]['ID_DIM'] = StripSlashes($row['ID_DIM']) ;  
				 $listArray[$row['ID']]['DATE_ACCES'] = StripSlashes($row['DATE_ACCES']) ;  				
			}
		}
		return $listArray ;
	}

	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name Usage::_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id==0) return false;

		$listArray=$this->_getList();
		$this->id = $listArray[$this->id]['ID']; 
		$this->uid = $listArray[$this->id]['UID']; 
 		$this->refDim = $listArray[$this->id]['REF_DIM']; 
 		$this->idDim = $listArray[$this->id]['ID_DIM']; 
 		$this->dateAcces = $listArray[$this->id]['DATE_ACCES']; 

	}
	
	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Usage::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>