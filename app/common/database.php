<?php
/**
 * 
 * <p>Interface our la gestion des bases de donn�es</p>
 * 
 * @name Database
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package common
 */
 
 interface Database {
	public static function GetInstance() ;
    public function _bddConnect() ;
    public function _bddClose() ;  
    public function _bddQuery($queryString) ;
    public function _bddQueryLastId($queryString) ;
    public static function _bddFetchAssoc($result) ;
    /**
    * Retourne le nombre de ligne du r�sultat d'une requ�te de type select 
    * 
    * <p>Nombre de ligne du r�sultat d'une requ�te � la base de donn�es mySQL</p>
    * 
    * @name MysqlDatabase::_bddNumRows()
    * @param $result
    * @return $num_rows 
    */
    public static function _bddNumRows($result) ;
 } 
?>