<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * <p>CorrDiplome</p>
 *
 * @name  CorrDiplome
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright PROCOPE 2011
 * @version 1.0.0
 * @package 
 */

class CorrDiplome {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietes    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
 * @var (Int)
 * @desc Identifiant
 */
private $id;

/**
 * @var (string)
 * @desc Code composante
 */
private $composanteCode;

/**
 * @var (string)
 * @desc Libelle Composante
 */
private $composanteLib;

/**
 * @var (string)
 * @desc Code Diplome
 */
private $diplomeCode;

/**
 * @var (string)
 * @desc libelle Diplome
 */
private $diplomeLib;

/**
 * @var (string)
 * @desc Code Etape
 */
private $etapeCode;

/**
 * @var (string)
 * @desc Libelle Etape
 */
private $etapeLib;

/**
 * @var (string)
 * @desc Code Discipline
 */
private $disciplineCode;

/**
 * @var (string)
 * @desc Libelle Discipline
 */
private $disciplineLib;

/**
 * @var (string)
 * @desc Code Type Diplome
 */
private $typeDiplomeCode;

/**
 * @var (string)
 * @desc Libelle Type Diplome
 */
private $typeDiplomeLib;



	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. methodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>creation de l'instance de la classe</p>
	 *
	 * @name CorrDiplome::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		$this->id='';
$this->composanteCode='';
$this->composanteLib='';
$this->diplomeCode='';
$this->diplomeLib='';
$this->etapeCode='';
$this->etapeLib='';
$this->disciplineCode='';
$this->disciplineLib='';
$this->typeDiplomeCode='';
$this->typeDiplomeLib='';

		
		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}
		
	}

	/**
	 * Accesseurs en lecture
	 */

	/**
* @name CorrDiplome::_getId()
* @return Int
*/
public function _getId() {
		return $this->id ;
}

/**
* @name CorrDiplome::_getComposanteCode()
* @return string
*/
public function _getComposanteCode() {
		return $this->composanteCode ;
}

/**
* @name CorrDiplome::_getComposanteLib()
* @return string
*/
public function _getComposanteLib() {
		return $this->composanteLib ;
}

/**
* @name CorrDiplome::_getDiplomeCode()
* @return string
*/
public function _getDiplomeCode() {
		return $this->diplomeCode ;
}

/**
* @name CorrDiplome::_getDiplomeLib()
* @return string
*/
public function _getDiplomeLib() {
		return $this->diplomeLib ;
}

/**
* @name CorrDiplome::_getEtapeCode()
* @return string
*/
public function _getEtapeCode() {
		return $this->etapeCode ;
}

/**
* @name CorrDiplome::_getEtapeLib()
* @return string
*/
public function _getEtapeLib() {
		return $this->etapeLib ;
}

/**
* @name CorrDiplome::_getDisciplineCode()
* @return string
*/
public function _getDisciplineCode() {
		return $this->disciplineCode ;
}

/**
* @name CorrDiplome::_getDisciplineLib()
* @return string
*/
public function _getDisciplineLib() {
		return $this->disciplineLib ;
}

/**
* @name CorrDiplome::_getTypeDiplomeCode()
* @return string
*/
public function _getTypeDiplomeCode() {
		return $this->typeDiplomeCode ;
}

/**
* @name CorrDiplome::_getTypeDiplomeLib()
* @return string
*/
public function _getTypeDiplomeLib() {
		return $this->typeDiplomeLib ;
}



	/**
	 * Accesseurs en ecriture
	 */

	/**
 * @name CorrDiplome::_setId()
 * @param $id (Int)
 * @return void
*/
public function _setId($id) {
	$this->id  = $id ;
}

/**
 * @name CorrDiplome::_setComposanteCode()
 * @param $composanteCode (string)
 * @return void
*/
public function _setComposanteCode($composanteCode) {
	$this->composanteCode  = $composanteCode ;
}

/**
 * @name CorrDiplome::_setComposanteLib()
 * @param $composanteLib (string)
 * @return void
*/
public function _setComposanteLib($composanteLib) {
	$this->composanteLib  = $composanteLib ;
}

/**
 * @name CorrDiplome::_setDiplomeCode()
 * @param $diplomeCode (string)
 * @return void
*/
public function _setDiplomeCode($diplomeCode) {
	$this->diplomeCode  = $diplomeCode ;
}

/**
 * @name CorrDiplome::_setDiplomeLib()
 * @param $diplomeLib (string)
 * @return void
*/
public function _setDiplomeLib($diplomeLib) {
	$this->diplomeLib  = $diplomeLib ;
}

/**
 * @name CorrDiplome::_setEtapeCode()
 * @param $etapeCode (string)
 * @return void
*/
public function _setEtapeCode($etapeCode) {
	$this->etapeCode  = $etapeCode ;
}

/**
 * @name CorrDiplome::_setEtapeLib()
 * @param $etapeLib (string)
 * @return void
*/
public function _setEtapeLib($etapeLib) {
	$this->etapeLib  = $etapeLib ;
}

/**
 * @name CorrDiplome::_setDisciplineCode()
 * @param $disciplineCode (string)
 * @return void
*/
public function _setDisciplineCode($disciplineCode) {
	$this->disciplineCode  = $disciplineCode ;
}

/**
 * @name CorrDiplome::_setDisciplineLib()
 * @param $disciplineLib (string)
 * @return void
*/
public function _setDisciplineLib($disciplineLib) {
	$this->disciplineLib  = $disciplineLib ;
}

/**
 * @name CorrDiplome::_setTypeDiplomeCode()
 * @param $typeDiplomeCode (string)
 * @return void
*/
public function _setTypeDiplomeCode($typeDiplomeCode) {
	$this->typeDiplomeCode  = $typeDiplomeCode ;
}

/**
 * @name CorrDiplome::_setTypeDiplomeLib()
 * @param $typeDiplomeLib (string)
 * @return void
*/
public function _setTypeDiplomeLib($typeDiplomeLib) {
	$this->typeDiplomeLib  = $typeDiplomeLib ;
}



	/**
	 * Compte du nombre d'occurence de l'objet CorrDiplome dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name CorrDiplome::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_corr_diplome ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

		/**
	 * renvoit l'id d'un stamp existant
	 *
	 * <p>_existsRef</p>
	 *
	 * @name CorrDiplome::_existsRef()
	 * @param $lib string
	 * @return int
	 */
	public function _existsRef($lib)
	{
		$idRef = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT id ';
		$sql .= ' FROM t_corr_diplome ';
		$sql .= ' WHERE UPPER(ETAPECODE) = "'.strtoupper($lib).'" ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$idRef  = $row['id'] ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		return $idRef;
	}
	
	/**
	 * Creation d'un CorrDiplome dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name CorrDiplome::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM t_corr_diplome ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		 
		$sql  = 'INSERT INTO t_corr_diplome VALUES( ';
		 $sql .= '\''.AddSlashes($this->id).'\', ';  
 $sql .= '\''.AddSlashes($this->composanteCode).'\', ';  
 $sql .= '\''.AddSlashes($this->composanteLib).'\', ';  
 $sql .= '\''.AddSlashes($this->diplomeCode).'\', ';  
 $sql .= '\''.AddSlashes($this->diplomeLib).'\', ';  
 $sql .= '\''.AddSlashes($this->etapeCode).'\', ';  
 $sql .= '\''.AddSlashes($this->etapeLib).'\', ';  
 $sql .= '\''.AddSlashes($this->disciplineCode).'\', ';  
 $sql .= '\''.AddSlashes($this->disciplineLib).'\', ';  
 $sql .= '\''.AddSlashes($this->typeDiplomeCode).'\', ';  
 $sql .= '\''.AddSlashes($this->typeDiplomeLib).'\' ';  

		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise e jour d'un CorrDiplome dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name CorrDiplome::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_corr_diplome SET ';
		 $sql .= 'ID = \''.AddSlashes($this->id).'\', ';  
 $sql .= 'COMPOSANTECODE = \''.AddSlashes($this->composanteCode).'\', ';  
 $sql .= 'COMPOSANTELIB = \''.AddSlashes($this->composanteLib).'\', ';  
 $sql .= 'DIPLOMECODE = \''.AddSlashes($this->diplomeCode).'\', ';  
 $sql .= 'DIPLOMELIB = \''.AddSlashes($this->diplomeLib).'\', ';  
 $sql .= 'ETAPECODE = \''.AddSlashes($this->etapeCode).'\', ';  
 $sql .= 'ETAPELIB = \''.AddSlashes($this->etapeLib).'\', ';  
 $sql .= 'DISCIPLINECODE = \''.AddSlashes($this->disciplineCode).'\', ';  
 $sql .= 'DISCIPLINELIB = \''.AddSlashes($this->disciplineLib).'\', ';  
 $sql .= 'TYPEDIPLOMECODE = \''.AddSlashes($this->typeDiplomeCode).'\', ';  
 $sql .= 'TYPEDIPLOMELIB = \''.AddSlashes($this->typeDiplomeLib).'\' ';  

		$sql .= 'WHERE id=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'un CorrDiplome dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name CorrDiplome::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'DELETE FROM t_corr_diplome ';
		$sql .= 'WHERE id = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}
	/**
	 * Recuperation de la liste des CorrDiplomes
	 *
	 * <p>Liste des CorrDiplome</p>
	 *
	 * @name CorrDiplome::_getList()
	 * @return array
	 */
	public function _getList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_corr_diplome ' ;
		if ($this->id>0)
		$sql.= ' WHERE id = \''.$this->id.'\' ';
		$sql .= 'ORDER BY COMPOSANTELIB ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				 $listArray[$row['ID']]['ID'] = StripSlashes($row['ID']) ;  
				 $listArray[$row['ID']]['COMPOSANTECODE'] = StripSlashes($row['COMPOSANTECODE']) ;  
				 $listArray[$row['ID']]['COMPOSANTELIB'] = StripSlashes($row['COMPOSANTELIB']) ;  
				 $listArray[$row['ID']]['DIPLOMECODE'] = StripSlashes($row['DIPLOMECODE']) ;  
				 $listArray[$row['ID']]['DIPLOMELIB'] = StripSlashes($row['DIPLOMELIB']) ;  
				 $listArray[$row['ID']]['ETAPECODE'] = StripSlashes($row['ETAPECODE']) ;  
				 $listArray[$row['ID']]['ETAPELIB'] = StripSlashes($row['ETAPELIB']) ;  
				 $listArray[$row['ID']]['DISCIPLINECODE'] = StripSlashes($row['DISCIPLINECODE']) ;  
				 $listArray[$row['ID']]['DISCIPLINELIB'] = StripSlashes($row['DISCIPLINELIB']) ;  
				 $listArray[$row['ID']]['TYPEDIPLOMECODE'] = StripSlashes($row['TYPEDIPLOMECODE']) ;  
				 $listArray[$row['ID']]['TYPEDIPLOMELIB'] = StripSlashes($row['TYPEDIPLOMELIB']) ;  
				
			}
		}
		return $listArray ;
	}

	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name CorrDiplome::_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id==0) return false;

		$listArray=$this->_getList();
		 $this->id = $listArray[$this->id]['ID']; 
 $this->composanteCode = $listArray[$this->id]['COMPOSANTECODE']; 
 $this->composanteLib = $listArray[$this->id]['COMPOSANTELIB']; 
 $this->diplomeCode = $listArray[$this->id]['DIPLOMECODE']; 
 $this->diplomeLib = $listArray[$this->id]['DIPLOMELIB']; 
 $this->etapeCode = $listArray[$this->id]['ETAPECODE']; 
 $this->etapeLib = $listArray[$this->id]['ETAPELIB']; 
 $this->disciplineCode = $listArray[$this->id]['DISCIPLINECODE']; 
 $this->disciplineLib = $listArray[$this->id]['DISCIPLINELIB']; 
 $this->typeDiplomeCode = $listArray[$this->id]['TYPEDIPLOMECODE']; 
 $this->typeDiplomeLib = $listArray[$this->id]['TYPEDIPLOMELIB']; 

	}
	
	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name CorrDiplome::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>