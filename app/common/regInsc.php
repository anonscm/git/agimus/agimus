<?php
require_once (DIR_WWW.ROOT_APPL.'/app/common/mysqlDatabase.php') ;
require_once (DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
/**
 *
 * <p>Regime d'Inscription</p>
 *
 * @name  RegInsc
 * @author Pascal PROCOPE
 * @licence Cecill v2 (http://www.cecill.info)
 * @version 1.0.0
 * @package
 */

class RegInsc {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (Int)
	 * @desc Identifiant
	 */
	private $id;

	/**
	 * @var (String)
	 * @desc Reference externe
	 */
	private $extnRef;
	
	/**
	 * @var (String)
	 * @desc Libelle
	 */
	private $lib;

	/**
	 * @var (String)
	 * @desc Decription
	 */
	private $dsc;

	/**
	 * @var (Int)
	 * @desc Id Regroupement
	 */
	private $idRegr;

	/**
	 * @var (Array)
	 * @desc tableau identifiant=>libelle */
	private $comboList;
	

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. methodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>creation de l'instance de la classe</p>
	 *
	 * @name RegInsc::__construct()
	 * @return void
	 */
	public function __construct($idRef=0) {
		$this->id=0;
		$this->extnRef='';
		$this->lib='';
		$this->dsc='';
		$this->idRegr=0;

		$this->comboList = array() ;
		
		if ($idRef>0)
		{
			$this->id=$idRef;
			$this->_fill();
		}

	}

	/**
	 * Accesseurs en lecture
	 */

	/**
	 * @name RegInsc::_getId()
	 * @return Int
	 */
	public function _getId() {
		return $this->id ;
	}

		/**
	 * @name RegInsc::_getExtnRef()
	 * @return string
	 */
	public function _getExtnRef() {
		return $this->extnRef ;
	}
	
	/**
	 * @name RegInsc::_getLib()
	 * @return String
	 */
	public function _getLib() {
		return $this->lib ;
	}

	/**
	 * @name RegInsc::_getDsc()
	 * @return String
	 */
	public function _getDsc() {
		return $this->dsc ;
	}

	/**
	 * @name RegInsc::_getIdRegr()
	 * @return Int
	 */
	public function _getIdRegr() {
		return $this->idRegr ;
	}

	/**
	 * @name CatSrv::_getComboList()
	 * @return array
	 */
	public function _getComboList()
	{
		return $this->comboList ;
	}
	
	

	/**
	 * Accesseurs en ecriture
	 */

	/**
	 * @name RegInsc::_setId()
	 * @param $id (Int)
	 * @return void
	 */
	public function _setId($id) {
		$this->id  = $id ;
	}

	/**
	 * @name RegInsc::_setExtnRef()
	 * @param $extnRef (String)
	 * @return void
	 */
	public function _setExtnRef($extnRef) {
		$this->extnRef  = $extnRef ;
	}
	
	/**
	 * @name RegInsc::_setLib()
	 * @param $lib (String)
	 * @return void
	 */
	public function _setLib($lib) {
		$this->lib  = $lib ;
	}

	/**
	 * @name RegInsc::_setDsc()
	 * @param $dsc (String)
	 * @return void
	 */
	public function _setDsc($dsc) {
		$this->dsc  = $dsc ;
	}

	/**
	 * @name RegInsc::_setIdRegr()
	 * @param $idRegr (Int)
	 * @return void
	 */
	public function _setIdRegr($idRegr) {
		$this->idRegr  = $idRegr ;
	}


	/**
	 * Compte du nombre d'occurence de l'objet RegInsc dans la bdd
	 *
	 * <p>countRef</p>
	 *
	 * @name RegInsc::_countRef()
	 * @return int
	 */
	public function _countRef()
	{
		$nbr = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT count(id) AS nbr ';
		$sql .= 'FROM t_reg_insc ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$nbr  = $row['nbr'] ;
			return $nbr;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * renvoit l'id d'un stamp existant
	 *
	 * <p>_existsRef</p>
	 *
	 * @name RegInsc::_existsRef()
	 * @param $lib string
	 * @return int
	 */
	public function _existsRef($lib)
	{
		$idRef = 0 ;
		$maconnexion = MysqlDatabase::GetInstance() ;

		$sql = 'SELECT id ';
		$sql .= ' FROM t_reg_insc ';
		$sql .= " WHERE UPPER(lib) = \"".strtoupper($lib)."\" ";

		try{
			$res = $maconnexion->_bddQuery($sql) ;
			$row = $maconnexion->_bddFetchAssoc($res);
			$idRef  = $row['id'] ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		return $idRef;
	}

	/**
	 * Creation d'un RegInsc dans la bdd
	 *
	 * <p>_create</p>
	 *
	 * @name RegInsc::_create()
	 * @return void
	 */
	public function _create()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql = 'SELECT MAX(id) AS idmax FROM t_reg_insc ';
		try
		{
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) >0){
				$row = $maconnexion->_bddFetchAssoc($res) ;
				$this->id = $row['idmax']+1 ;
			}else{
				$this->id = 1 ;
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
			
		$sql  = 'INSERT INTO t_reg_insc VALUES( ';
		$sql .= '\''.AddSlashes($this->id).'\', ';
		$sql .= '\''.AddSlashes($this->extnRef).'\', ';
		$sql .= '\''.AddSlashes($this->lib).'\', ';
		$sql .= '\''.AddSlashes($this->dsc).'\', ';
		$sql .= '\''.AddSlashes($this->idRegr).'\' ';

		$sql .= ' ) ';
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Mise a jour d'une RegInsc dans la bdd
	 *
	 * <p>_update</p>
	 *
	 * @name RegInsc::_update()
	 * @return void
	 */
	public function _update()
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'UPDATE t_reg_insc SET ';
		$sql .= 'ID = \''.AddSlashes($this->id).'\', ';
		$sql .= 'EXTN_REF = \''.AddSlashes($this->extnRef).'\', ';
		$sql .= 'LIB = \''.AddSlashes($this->lib).'\', ';
		$sql .= 'DSC = \''.AddSlashes($this->dsc).'\', ';
		$sql .= 'ID_REGR = \''.AddSlashes($this->idRegr).'\' ';

		$sql .= 'WHERE id=\''.$this->id.'\' ';

		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}

	/**
	 * Suppression d'une RegInsc dans la bdd
	 *
	 * <p>_delete</p>
	 *
	 * @name RegInsc::_delete()
	 * @param $idRef(int)
	 * @return void
	 */
	public function _delete($idRef)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'DELETE FROM t_reg_insc ';
		$sql .= 'WHERE id = \''.$idRef.'\' ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
	}
	/**
	 * Recuperation de la liste des RegInscs
	 *
	 * <p>Liste des RegInscs</p>
	 *
	 * @name RegInsc::_getList()
	 * @return array
	 */
	public function _getList()
	{
		$listArray = array() ;
		$maconnexion = MysqlDatabase::GetInstance() ;
		$sql  = 'SELECT * FROM t_reg_insc ' ;
		if ($this->id>0)
		$sql.= ' WHERE id = \''.$this->id.'\' ';
		$sql .= 'ORDER BY LIB ' ;
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = $maconnexion->_bddFetchAssoc($res))
			{
				$listArray[$row['ID']]['ID'] = StripSlashes($row['ID']) ;
				$listArray[$row['ID']]['EXTN_REF'] = StripSlashes($row['EXTN_REF']) ;
				$listArray[$row['ID']]['LIB'] = StripSlashes($row['LIB']) ;
				$listArray[$row['ID']]['DSC'] = StripSlashes($row['DSC']) ;
				$listArray[$row['ID']]['ID_REGR'] = StripSlashes($row['ID_REGR']) ;
				$this->comboList[$row['ID']] = StripSlashes($row['LIB']) ;
			}
		}
		return $listArray ;
	}

	
	/**
	 * initialisation de l'occurrence
	 *
	 * <p>libelle</p>
	 *
	 * @name RegInsc::_fill()
	 * @return void
	 */
	public function _fill()
	{
		if ($this->id==0) return false;

		$listArray=$this->_getList();
		$this->id = $listArray[$this->id]['ID'];
		$this->extnRef=$listArray[$this->id]['EXTN_REF'];
		$this->lib = $listArray[$this->id]['LIB'];
		$this->dsc = $listArray[$this->id]['DSC'];
		$this->idRegr = $listArray[$this->id]['ID_REGR'];

	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name RegInsc::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>