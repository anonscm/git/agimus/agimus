<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/session.php');
/**
 *
 * <p>Import / Export donnees SQL</p>
 *
 * @name UpgradeVersion
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package common
 */

class UpgradeVersion extends Session {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. méthodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>creation de l'instance de la classe</p>
	 *
	 * @name MysqlDatabase::__construct()
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Upgrade automatique
	 *
	 * <p>Upgrade automatique</p>
	 *
	 * @name UpgradeVersion::_upgrade()
	 * @return void
	 */
	public function _upgrade() {

		$maconnexion = MysqlDatabase::GetInstance() ;

		// v1.0 Ajout ligne de droits GESTFLUX 21

		$alreadyDone=false;
		$sql="select * from gestflux_right where RIGHT_ID = 21";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		$alreadyDone=true;
		else
		{
			$sql = "INSERT INTO `gestflux_right` VALUES ";
			$sql.= "('21','IMPORT_SQL')";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `gestflux_right_acl` VALUES (";
			$sql .= "21, 1, 1, 1";
			$sql .= ")";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `gestflux_right_libelle` VALUES ";
			$sql .= "(21, 'fr', 'Importer les flux SQL'), ";
			$sql .= "(21, 'en', 'Import SQL Files') ";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

		// v1.0 Ajout ligne de droits GESTFLUX 22

		$alreadyDone=false;
		$sql="select * from gestflux_right where RIGHT_ID = 22";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		$alreadyDone=true;
		else
		{
			$sql = "INSERT INTO `gestflux_right` VALUES ";
			$sql.= "('22','EXPORT_SQL')";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `gestflux_right_acl` VALUES (";
			$sql .= "22, 1, 1, 1";
			$sql .= ")";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `gestflux_right_libelle` VALUES ";
			$sql .= "(22, 'fr', 'Exporter les flux SQL'), ";
			$sql .= "(22, 'en', 'Export SQL Files') ";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

		// v1.0 Ajout table trace log
		$alreadyDone=false;
		$sql="show tables";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_row($res))
			{
				if ($row[0]=='t_tracelog')
				$trouve=true;
			}
		}
		if (!$trouve)
		{
			$sql="
			CREATE TABLE IF NOT EXISTS `t_tracelog` (
			  `ID` int(11) NOT NULL default '0',
			  `COOKIE_ID` varchar(255) collate utf8_unicode_ci NOT NULL default '',
			  `UID` varchar(255) collate utf8_unicode_ci NOT NULL default '',
			  `STAMP_PROCESSED` datetime NOT NULL default '0000-00-00 00:00:00',
			  PRIMARY KEY  (`ID`),
			  KEY `COOKIE_ID` (`COOKIE_ID`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

		}

		// v1.0 Ajout dimensions d'agregats user_agent + 2 dimensions optionnelles
		$alreadyDone=false;
		$sql="show fields from t_agregat_calc ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['Field']=='USER_AGENT')
				$trouve=true;
			}
		}
		if (!$trouve)
		{
			$sql="ALTER TABLE `t_agregat_calc`
			ADD `USER_AGENT` varchar(50) NOT NULL default '',
			ADD `USER_DIM_1` varchar(50) NOT NULL default '',
			ADD `USER_DIM_2` varchar(50) NOT NULL default '',
			ADD `USER_DIM_3` varchar(50) NOT NULL default ''
			;";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

		}

		$alreadyDone=false;
		$sql="show fields from t_agregat ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['Field']=='USER_AGENT')
				$trouve=true;
			}
		}
		if (!$trouve)
		{
			$sql="ALTER TABLE `t_agregat`
			ADD `USER_AGENT` varchar(50) NOT NULL default '',
			ADD `USER_DIM_1` varchar(50) NOT NULL default '',
			ADD `USER_DIM_2` varchar(50) NOT NULL default '',
			ADD `USER_DIM_3` varchar(50) NOT NULL default ''
			;";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

		}

		$alreadyDone=false;
		$sql="show fields from t_cnx ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['Field']=='USER_AGENT')
				$trouve=true;
			}
		}
		if (!$trouve)
		{
			$sql="ALTER TABLE `t_cnx`
			ADD `USER_AGENT` varchar(50) NOT NULL default '',
			ADD `USER_DIM_1` varchar(50) NOT NULL default '',
			ADD `USER_DIM_2` varchar(50) NOT NULL default '',
			ADD `USER_DIM_3` varchar(50) NOT NULL default ''
			;";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

		}

		$alreadyDone=false;
		$sql="show fields from t_cnxlog ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['Field']=='USER_AGENT')
				$trouve=true;
			}
		}
		if (!$trouve)
		{
			$sql="ALTER TABLE `t_cnxlog`
			ADD `USER_AGENT` varchar(50) NOT NULL default '';";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

		}

		// Modif id_struct de dénombrement
		$alreadyDone=false;
		$sql="show fields from r_denombrement ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['Field']=='ID_STRUCT')
				{
					if (strstr($row['Type'],'int'))
					{
						$sql = "ALTER TABLE `r_denombrement` CHANGE `ID_STRUCT` `ID_STRUCT` VARCHAR(50) NOT NULL DEFAULT '0'";
						try{
							$res2 = $maconnexion->_bddQuery($sql) ;
						}
						catch(MsgException $e){
							$msgString = $e ->_getError();
							throw new MsgException($msgString, 'database') ;
						}
					}
				}
			}
		}

		// ajout de la structure mère au dénombrement

		$alreadyDone=false;
		$sql="show fields from r_denombrement ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['Field']=='ID_STRUCT_PERE')
				$trouve=true;
			}
		}
		if (!$trouve)
		{
			$sql="ALTER TABLE `r_denombrement`
			ADD `ID_STRUCT_PERE` varchar(50) NOT NULL default '';";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}


		// v1.0 Ajout table t_traitement
		$alreadyDone=false;
		$sql="show tables";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_row($res))
			{
				if ($row[0]=='t_traitement')
				$trouve=true;
			}
		}
		if (!$trouve)
		{
			$sql="
			CREATE TABLE IF NOT EXISTS `t_traitement` (
 			`ID` int(11) NOT NULL,
  			`TRT_COD` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  			`TRT_DATE_DEB` datetime NOT NULL,
  			`TRT_DATE_FIN` datetime NOT NULL,
  			`TRT_PARAMS` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  			`TRT_STATUS` tinyint(4) NOT NULL,
  			`TRT_LOG` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  			PRIMARY KEY (`ID`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

		}


		// v1.0 Ajout ligne de droits GESTFLUX 23

		$alreadyDone=false;
		$sql="select * from gestflux_right where RIGHT_ID = 23";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		$alreadyDone=true;
		else
		{
			$sql = "INSERT INTO `gestflux_right` VALUES ";
			$sql.= "('23','LIST_JOBS')";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `gestflux_right_acl` VALUES (";
			$sql .= "23, 1, 1, 1";
			$sql .= ")";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `gestflux_right_libelle` VALUES ";
			$sql .= "(23, 'fr', 'Lister les traitements'), ";
			$sql .= "(23, 'en', 'List jobs') ";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

		// v1.0 Ajout ligne de droits GESTFLUX 24

		$alreadyDone=false;
		$sql="select * from gestflux_right where RIGHT_ID = 24";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		$alreadyDone=true;
		else
		{
			$sql = "INSERT INTO `gestflux_right` VALUES ";
			$sql.= "('24','DELETE_JOB')";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `gestflux_right_acl` VALUES (";
			$sql .= "24, 1, 1, 1";
			$sql .= ")";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `gestflux_right_libelle` VALUES ";
			$sql .= "(24, 'fr', 'Supprimer un traitement'), ";
			$sql .= "(24, 'en', 'delete a job') ";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

		// v1.0 Ajout ligne de droits GESTFLUX 25

		$alreadyDone=false;
		$sql="select * from gestflux_right where RIGHT_ID = 25";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		$alreadyDone=true;
		else
		{
			$sql = "INSERT INTO `gestflux_right` VALUES ";
			$sql.= "('25','UPDATE_JOB')";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `gestflux_right_acl` VALUES (";
			$sql .= "25, 1, 1, 1";
			$sql .= ")";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `gestflux_right_libelle` VALUES ";
			$sql .= "(25, 'fr', 'Mettre a jour un traitement'), ";
			$sql .= "(25, 'en', 'update a job') ";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}
		// v1.0 Ajout ligne de droits GESTFLUX 26

		$alreadyDone=false;
		$sql="select * from gestflux_right where RIGHT_ID = 26";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		$alreadyDone=true;
		else
		{
			$sql = "INSERT INTO `gestflux_right` VALUES ";
			$sql.= "('26','CREATE_JOB')";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `gestflux_right_acl` VALUES (";
			$sql .= "26, 1, 1, 1";
			$sql .= ")";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `gestflux_right_libelle` VALUES ";
			$sql .= "(26, 'fr', 'Créer un traitement'), ";
			$sql .= "(26, 'en', 'create a job') ";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}
		// v1.0 Ajout ligne de droits GESTFLUX 27

		$alreadyDone=false;
		$sql="select * from gestflux_right where RIGHT_ID = 27";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		$alreadyDone=true;
		else
		{
			$sql = "INSERT INTO `gestflux_right` VALUES ";
			$sql.= "('27','RESTART_JOB')";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `gestflux_right_acl` VALUES (";
			$sql .= "27, 1, 1, 1";
			$sql .= ")";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `gestflux_right_libelle` VALUES ";
			$sql .= "(27, 'fr', 'Relancer un traitement'), ";
			$sql .= "(27, 'en', 'restart a job') ";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

		// v1.0 Ajout ligne de droits IMPORT INDIC

		$alreadyDone=false;
		$sql="select * from ref_right where RIGHT_ID = 76";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res))
		$alreadyDone=true;
		else
		{
			$sql = "INSERT INTO `ref_right` VALUES ";
			$sql.= "('76','_INDIC_IMPORT_')";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `ref_right_acl` VALUES (";
			$sql .= "76, 1, 1, 1";
			$sql .= ")";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			$sql  = "INSERT INTO `ref_right_libelle` VALUES ";
			$sql .= "(76, 'fr', 'Importer un indicateur'), ";
			$sql .= "(76, 'en', 'import an indicator') ";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

		$alreadyDone=false;
		$sql="show index from t_cnxlog ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row["Key_name"]=='processed_id')
				$trouve1=true;
				if ($row["Key_name"]=='uid_stamp_start')
				$trouve2=true;
			}
		}

		if (!$trouve1)
		{
			// ajout index
			$sql  = "ALTER TABLE `t_cnxlog` ADD INDEX `processed_id` (`STAMP_PROCESSED`,`ID`)";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

		}
		if (!$trouve2)
		{
			// ajout index
			$sql  = "ALTER TABLE `t_cnxlog` ADD INDEX `uid_stamp_start` (`UID`,`STAMP_START`)";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

		$alreadyDone=false;
		$sql="show index from t_cnx ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row["Key_name"]=='processed_id')
				$trouve=true;
			}
		}

		if (!$trouve)
		{
			// ajout index
			$sql  = "ALTER TABLE `t_cnx` ADD INDEX `processed_id` (`STAMP_PROCESSED`,`ID`)";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

		}

		// v1.1 Ajout table t_usage
		
		$alreadyDone=false;
		$sql="show tables";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_row($res))
			{
				if ($row[0]=='t_usage')
				$trouve=true;
			}
		}
		if (!$trouve)
		{
			$sql="
			CREATE TABLE IF NOT EXISTS `t_usage` (
			  `ID` int(11) NOT NULL default '0',
			  `UID` varchar(255) NOT NULL default '',
			  `REF_DIM` int(11) NOT NULL default '0',
			  `ID_DIM` int(11) NOT NULL default '0',
			  `DATE_ACCES` date NOT NULL default '0000-00-00',
			  PRIMARY KEY  (`ID`),
			  KEY `ACCES` (`UID`,`REF_DIM`,`ID_DIM`,`DATE_ACCES`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

	// -------------------------- mises à jour v1.2 mars 2011 ---------------------
			// memorisation on-the-fly des paramètres LDAP

		$alreadyDone=false;
		$sql="show fields from t_tracelog ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['Field']=='DIM_1')
				$trouve=true;
			}
		}

		if (!$trouve)
		{
			$sql="ALTER TABLE `t_tracelog`
			ADD `DIM_1` varchar(100) NOT NULL default '' ,
			ADD `DIM_2` varchar(100) NOT NULL default '' ,
			ADD `DIM_3` varchar(100) NOT NULL default '' ,
			ADD `DIM_4` varchar(100) NOT NULL default ''
			;";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}
	
			// ajout du code interne structure pour mise à jour facilitée

		$alreadyDone=false;
		$sql="show fields from t_struct ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['Field']=='ID_INTRA')
				$trouve=true;
			}
		}

		if (!$trouve)
		{
			$sql="ALTER TABLE `t_struct`
			ADD `ID_INTRA` varchar(50) NOT NULL default ''
			;";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

			// on en profite pour ajouter l'index sur id_appli
			$sql = "ALTER TABLE `t_cnx` ADD INDEX(`ID_APPLI`);";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			
			$sql = "ALTER TABLE `t_cnx` ADD INDEX `idx_agregate` (`UID`,`STAMP_START`,`ID_APPLI`);";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

	//v1.5-PPR-#016 -------------------------- mises à jour v1.5 juin 2012 ---------------------
	
			// Ajout nouvelles dimensions sur TraceLog

		$alreadyDone=false;
		$sql="show fields from t_tracelog ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['Field']=='DIM_5')
				$trouve=true;
			}
		}

		if (!$trouve)
		{
			$sql="ALTER TABLE `t_tracelog`
			ADD `DIM_5` varchar(100) NOT NULL default '' ,
			ADD `DIM_6` varchar(100) NOT NULL default '' ,
			ADD `DIM_7` varchar(100) NOT NULL default '' ,
			ADD `DIM_8` varchar(100) NOT NULL default '' ,
			ADD `DIM_9` varchar(100) NOT NULL default '' ,
			ADD `ORGANISME` varchar(50) NOT NULL default ''
			;";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

			// Ajout nouvelles dimensions sur t_cnx

		$alreadyDone=false;
		$sql="show fields from t_cnx ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['Field']=='ID_COMPOSANTE')
				$trouve=true;
			}
		}

		if (!$trouve)
		{
			$sql="ALTER TABLE `t_cnx`
			ADD `ID_COMPOSANTE` INT(11) NOT NULL AFTER `ID_DOM_ETU`,
			ADD `ID_REG_INSC` INT(11) NOT NULL AFTER `ID_COMPOSANTE`
			;";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

			// Ajout nouvelles dimensions sur t_agregat_calc

		$alreadyDone=false;
		$sql="show fields from t_agregat_calc ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['Field']=='ID_COMPOSANTE')
				$trouve=true;
			}
		}

		if (!$trouve)
		{
			$sql="ALTER TABLE `t_agregat_calc`
			ADD `ID_COMP_INTRA` INT(11) NOT NULL AFTER `ID_DIPLOME`,
			ADD `ID_COMPOSANTE` INT(11) NOT NULL AFTER `ID_COMP_INTRA`,
			ADD `ID_REG_INSC_INTRA` INT(11) NOT NULL AFTER `ID_COMPOSANTE`,
			ADD `ID_REG_INSC` INT(11) NOT NULL AFTER `ID_REG_INSC_INTRA`,
			ADD `USER_DIM_4` INT(11) NOT NULL AFTER `USER_DIM_3`,
			ADD `USER_DIM_5` INT(11) NOT NULL AFTER `USER_DIM_4`,
			ADD `USER_DIM_6` INT(11) NOT NULL AFTER `USER_DIM_5`
			;";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

		}

			// Ajout nouvelles dimensions sur t_agregat

		$alreadyDone=false;
		$sql="show fields from t_agregat ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['Field']=='ID_COMPOSANTE')
				$trouve=true;
			}
		}

		if (!$trouve)
		{
			$sql="ALTER TABLE `t_agregat`
			ADD `ID_COMP_INTRA` INT(11) NOT NULL AFTER `ID_DIPLOME`,
			ADD `ID_COMPOSANTE` INT(11) NOT NULL AFTER `ID_COMP_INTRA`,
			ADD `ID_REG_INSC_INTRA` INT(11) NOT NULL AFTER `ID_COMPOSANTE`,
			ADD `ID_REG_INSC` INT(11) NOT NULL AFTER `ID_REG_INSC_INTRA`,
			ADD `USER_DIM_4` INT(11) NOT NULL AFTER `USER_DIM_3`,
			ADD `USER_DIM_5` INT(11) NOT NULL AFTER `USER_DIM_4`,
			ADD `USER_DIM_6` INT(11) NOT NULL AFTER `USER_DIM_5`
			;";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

		}
		
		//v1.5-PPR-#016 création table composante intra
		
		$sql="
			CREATE TABLE IF NOT EXISTS `t_comp_intra` (
			  `ID` smallint(6) NOT NULL DEFAULT '0',
			  `EXTN_REF` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
			  `LIB` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `DSC` text COLLATE utf8_unicode_ci NOT NULL,
			  `CHAINE_TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
			  `ID_REGR` int(11) NOT NULL,
			  `ID_STRUCT` smallint(6) NOT NULL,
			  PRIMARY KEY (`ID`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

		//v1.5-PPR-#016 création table composante intra
		
		$sql="
			CREATE TABLE IF NOT EXISTS `t_reg_insc_intra` (
			  `ID` smallint(6) NOT NULL DEFAULT '0',
			  `EXTN_REF` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
			  `LIB` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `DSC` text COLLATE utf8_unicode_ci NOT NULL,
			  `CHAINE_TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
			  `ID_REGR` int(11) NOT NULL,
			  `ID_STRUCT` smallint(6) NOT NULL,
			  PRIMARY KEY (`ID`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

		//v1.5-PPR-#016 création table composante
		
		$sql="
			CREATE TABLE IF NOT EXISTS `t_composante` (
			  `ID` smallint(6) NOT NULL DEFAULT '0',
			  `EXTN_REF` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
			  `LIB` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `DSC` text COLLATE utf8_unicode_ci NOT NULL,
			  `ID_REGR` int(11) NOT NULL,
			  PRIMARY KEY (`ID`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

		//v1.5-PPR-#016 création table regimes inscription
		
		$sql="
			CREATE TABLE IF NOT EXISTS `t_reg_insc` (
			  `ID` smallint(6) NOT NULL DEFAULT '0',
			  `EXTN_REF` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
			  `LIB` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `DSC` text COLLATE utf8_unicode_ci NOT NULL,
			  `ID_REGR` int(11) NOT NULL,
			  PRIMARY KEY (`ID`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		";
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}

			//v1.5-PPR-#016 ajout nouvelles dimensions

		$sql="select count(id) as nbDim from t_dim where id=12 ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['nbDim']==1)
				$trouve=true;
			}
		}

		if (!$trouve)
		{

		$sql ="INSERT INTO `t_dim` (`ID`, `LIB`, `REF_DIM`, `DIM_ID_LIB`) VALUES ";
		$sql.="(12, 'Composante interne', 'CompIntra', 'id_comp_intra'), ";
		$sql.="(13, 'Composante', 'Composante', 'id_composante'), ";
		$sql.="(14, 'Regime Inscription interne', 'RegInscIntra', 'id_reg_insc_intra'), ";
		$sql.="(15, 'Regime Inscription', 'RegInsc', 'id_reg_insc') ";

			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}


		$sql="select count(right_id) as nbr from ref_right where right_id=77 ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['nbr']>0)
				$trouve=true;
			}
		}

		if (!$trouve)
		{

		$sql=" INSERT INTO `ref_right` (`RIGHT_ID`, `RIGHT_CODE`) VALUES ";
		$sql.="
			(77, '_COMPOSANTE_LIST_ACCESS_'),
			(78, '_COMPOSANTE_CREATE_'),
			(79, '_COMPOSANTE_DELETE_'),
			(80, '_COMPOSANTE_UPDATE_'),
			(81, '_COMP_INTRA_LIST_ACCESS_'),
			(82, '_COMP_INTRA_CREATE_'),
			(83, '_COMP_INTRA_DELETE_'),
			(84, '_COMP_INTRA_UPDATE_'),
			(85, '_REG_INSC_LIST_ACCESS_'),
			(86, '_REG_INSC_CREATE_'),
			(87, '_REG_INSC_DELETE_'),
			(88, '_REG_INSC_UPDATE_'),
			(89, '_REG_INSC_INTRA_LIST_ACCESS_'),
			(90, '_REG_INSC_INTRA_CREATE_'),
			(91, '_REG_INSC_INTRA_DELETE_'),
			(92, '_REG_INSC_INTRA_UPDATE_');
		";

			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

		$sql="select count(right_id) as nbr from ref_right_acl where right_id=77 ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['nbr']>0)
				$trouve=true;
			}
		}

		if (!$trouve)
		{

		$sql=" INSERT INTO `ref_right_acl` (`RIGHT_ID`, `INT_COD`, `GID`, `ACL`) VALUES ";
		$sql.="
			(77, 0, 0, 1),
			(78, 0, 0, 1),
			(79, 0, 0, 1),
			(80, 0, 0, 1),
			(81, 0, 0, 1),
			(82, 0, 0, 1),
			(83, 0, 0, 1),
			(84, 0, 0, 1),
			(85, 0, 0, 1),
			(86, 0, 0, 1),
			(87, 0, 0, 1),
			(88, 0, 0, 1),
			(89, 0, 0, 1),
			(90, 0, 0, 1),
			(91, 0, 0, 1),
			(92, 0, 0, 1);
		";

			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

		$sql="select count(right_id) as nbr from ref_right_libelle where right_id=77 ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['nbr']>0)
				$trouve=true;
			}
		}

		if (!$trouve)
		{

		$sql=" INSERT INTO `ref_right_libelle` (`RIGHT_ID`, `LANG`, `LIBELLE`) VALUES ";
		$sql.="
			(77, 'fr', 'Accès à la liste des composantes intra-etablissement'),
			(77, 'en', 'Access to the list of internal composantes'),
			(78, 'fr', 'Créer une composante intra-etablissement'),
			(78, 'en', 'Create an internal composante '),
			(79, 'fr', 'Supprimer une consosante intra-etablissement'),
			(79, 'en', 'Delete an internal composante '),
			(80, 'fr', 'Modifier une composante intra-etablissement'),
			(80, 'en', 'Update an internal composante '),
			(81, 'fr', 'Accès à la liste des composantes inter-etablissement'),
			(81, 'en', 'Access to the list of external composantes'),
			(82, 'fr', 'Créer une composante inter-etablissement'),
			(82, 'en', 'Create an external composante '),
			(83, 'fr', 'Supprimer une consosante inter-etablissement'),
			(83, 'en', 'Delete an external composante '),
			(84, 'fr', 'Modifier une composante inter-etablissement'),
			(84, 'en', 'Update an external composante '),
			(85, 'fr', 'Accès à la liste des regimes intra-etablissement'),
			(85, 'en', 'Access to the list of internal inscription rules'),
			(86, 'fr', 'Créer un regime intra-etablissement'),
			(86, 'en', 'Create an internal inscription rule '),
			(87, 'fr', 'Supprimer un regime intra-etablissement'),
			(87, 'en', 'Delete an internal inscription rules '),
			(88, 'fr', 'Modifier un regime intra-etablissement'),
			(88, 'en', 'Update an internal inscription rule '),
			(89, 'fr', 'Accès à la liste des regimes inter-etablissement'),
			(89, 'en', 'Access to the list of external inscription rules'),
			(90, 'fr', 'Créer un regime inter-etablissement'),
			(90, 'en', 'Create an external inscription rule '),
			(91, 'fr', 'Supprimer un regime inter-etablissement'),
			(91, 'en', 'Delete an external inscription rule '),
			(92, 'fr', 'Modifier un regime inter-etablissement'),
			(92, 'en', 'Update an external inscription rule ');
		";

			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

		// Insertion valeurs "Autres..." dans les nouvelles dimensions

		$sql="select count(id) as nbr from t_reg_insc where id=1 ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['nbr']>0)
				$trouve=true;
			}
		}

		if (!$trouve)
		{

		$sql=" INSERT INTO `t_reg_insc` (`ID`, `EXTN_REF`, `LIB`, `DSC`, `ID_REGR`) VALUES 
		(1, '', 'Autres régimes', '', 0);
		";

			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

		$sql="select count(id) as nbr from t_composante where id=1 ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		$trouve=false;
		if($maconnexion->_bddNumRows($res))
		{
			while($row = mysql_fetch_assoc($res))
			{
				if ($row['nbr']>0)
				$trouve=true;
			}
		}

		if (!$trouve)
		{

		$sql=" INSERT INTO `t_composante` (`ID`, `EXTN_REF`, `LIB`, `DSC`, `ID_REGR`) VALUES
		(1, '', 'Autres Composantes', '', 0);
		";

			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
		}

	} // fin function _upgrade

	/**
	 * Initialisation de l'appliciation
	 *
	 * <p>Initialisation automatique</p>
	 *
	 * @name UpgradeVersion::_initAppli()
	 * @return void
	 */
	public function _initAppli() {

		$maconnexion = MysqlDatabase::GetInstance() ;

		//v1.5-PPR-#010 création auto Structure

		$alreadyDone=false;
		$sql="select count(id) as nbStruct from t_struct ";
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res)) {
			$row = mysql_fetch_assoc($res);
			if ($row['nbStruct']==0)					
			{
				$sql = "INSERT INTO `t_struct` (`ID`, `LIB`, `DSC`, `PARENT`, `ID_CAT_STRUCT`, `URL_EXPORT`, `USER`, `PWD`, `ID_INTRA`) VALUES ";
				$sql.= "('{EES}ETB', 'ETB', '', '{EES}UNR', 3, 'http://url_unr/agimus', '', '', '{EES}ETB'),";
				$sql.= "('{EES}UNR', 'UNR', '', '', 2, 'http://url_ministere/agimus', '', '', '{EES}UNR'); ";
				try{
					$res = $maconnexion->_bddQuery($sql) ;
				}
				catch(MsgException $e){
					$msgString = $e ->_getError();
					throw new MsgException($msgString, 'database') ;
				}
			}
		}
	}
			
	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name className::__destruct()
	 * @return void
	 */
	public function __destruct() {
	}
}
?>