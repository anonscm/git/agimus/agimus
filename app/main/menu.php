<?php
/**
 * 
 * <p>Affichage du menu</p>
 * 
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package main
 */
 
require_once('../../app/common/required.php');
 
  	//Recuperation de la session
 	$session = Session::_GetInstance() ;
 	$session->_setModid(0);
 	$session->_sessionLogAction() ;
 	
 	// creation des répertoires obligatoires
 	createDir("/temp");
 	
 	// upgrade automatique
 	require_once( 'upgradeVersion.php');
 	$version=new UpgradeVersion();
 	$version->_upgrade();
 	//v1.5-PPR-#010 création auto d'une structure
 	$version->_initAppli();
 	
  	require_once( 'mainMenu.php') ; 
	$page = new MainMenu($session) ;
  	$page->_makePage($session) ;

	// test uniquement
	// contrôle d'analyse de regex
  
	//$lineRed='77.207.149.12 - -  [14/Jan/2011:00:00:42 +0100] "GET /main/img/students.gif HTTP/1.1" 200 1230 "https://portail.univ-pau.fr/render.userLayoutRootNode.uP" "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10" xtvrn=$362667$; __utmz=153204158.1294868505.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utma=153204158.1545699953.1294868505.1294868505.1294868505.1; CASTRACEME=TRACE-638414-eeqv1kzBKjaiacsoiwQgrhzUwHUajlM7utBsjlTVZA0tg9bizA-sso.univ-pau.fr';  	
	//$lineRed='186.123.59.129 - - [14/Jan/2011:00:00:02 +0100] "POST /horde/dimp/imp.php/PollFolders HTTP/1.1" 200 290 "https://webmail.univ-pau.fr/horde/dimp/" "Mozilla/5.0 (Windows; U; Windows NT 5.1; es-ES; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 ( .NET CLR 3.5.30729)" TRACE-638402-6zPQACxEyLevZ0apjCdCyxGmNhkGJdA6OBzD99npFN6dkqz4Jb-sso.univ-pau.fr';
	//$lineRed='TRACE-638402-6zPQACxEyLevZ0apjCdCyxGmNhkGJdA6OBzD99npFN6dkqz4Jb-sso.univ-pau.fr 186.123.59.129 - - 217599 [14/Jan/2011:00:00:02 +0100] "POST /horde/dimp/imp.php/PollFolders HTTP/1.1" 200 290 "https://webmail.univ-pau.fr/horde/dimp/" "Mozilla/5.0 (Windows; U; Windows NT 5.1; es-ES; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 ( .NET CLR 3.5.30729)"';
    //$logEntryPattern="/^(\S+) (\S+) (\S+) \[(\S+) ([^\]]+)\] \"(\S+) (.*?) (\S+)\" (\S+) (\S+) \"(.*?)\" (\".*?\") (\S+)$/";
	//$logEntryPattern="/^(\S+) (\S+) (\S+) (\S+) (\S+) \[(\S+) ([^\]]+)\] \"(\S+) (.*?) (\S+)\" (\S+) (\S+) \"(.*?)\" (\".*?\")$/";
	//if (!preg_match($logEntryPattern,$lineRed,$arSplit))
	//	echo "<br>Erreur regex : ".preg_last_error();
	//	aff_pr($arSplit);
	
	//foreach($arSplit as $i=>$curVal)
	//echo "<br>$i. $curVal";
  	
  	//aff_pr($_SERVER);
	//echo phpinfo();
  	
?>