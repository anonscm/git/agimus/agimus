<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/xmlAnalyser.php') ;

/**
 *
 * <p>Acces a l'application</p>
 *
 * @name login
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */


class Access
{
	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * @var (string)
	 * @desc Nom de connexion de l'utilisateur
	 */
	private $login ;
	/**
	 * @var (string)
	 * @desc Mot de passe
	 */
	private $pwd ;

	/**
	 * @var (string)
	 * @desc identifiant de l'utilisateur si connexion
	 */
	private $uid ;

	/**
	 * @var (string)
	 * @desc language s�lectionn� pour la dur�e de la session
	 */
	private $lang ;

	/**
	 * @var (string)
	 * @desc nom de l'utilisateur si connexion
	 */
	private $username;

	/**
	 * @var $config (array)
	 * @desc tableau des donn�es de configuration r�cup�r�es dans un fichier xml
	 */
	private $config ;

	/**
	 * @var $configFile (string)
	 * @desc Nom du fichier xml de configuration
	 */
	private $configFile ;
	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes      */
	/*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Access::__construct()
	 * @return void
	 */
	public function __construct($lgn, $pwd, $lg) {
		$this->config =array() ;
		$this->configFile='config.xml' ;
		$this->login = $lgn ;
		$this->password = $pwd ;
		$this->lang = $lg ;
	}
	/**
	 * Accesseur en lecture
	 *
	 * <p>Acc�s � la variable login</p>
	 * @name Access::_getlogin()
	 * @return string
	 */
	public function _getLogin()
	{
		return $this->login ;
	}
	/**
	 * Accesseur en lecture
	 *
	 * <p>Acc�s � la variable username</p>
	 * @name Access::_getUsername()
	 * @return string
	 */
	public function _getUsername()
	{
		return $this->username ;
	}
	/**
	 * Accesseur en lecture
	 *
	 * <p>Acc�s � la variable uid</p>
	 * @name Access::_getUid()
	 * @return int
	 */
	public function _getUid()
	{
		return $this->uid ;
	}
	/**
	 * Accesseur en lecture
	 *
	 * <p>Acc�s � la variable lang</p>
	 * @name Access::_getLang()
	 * @return String
	 */
	public function _getLang()
	{
		return $this->lang ;
	}
	/**
	 * Accesseur en �criture
	 */

	/**
	 * <p>Mise � jour du login</p>
	 * @name Access::_setlogin()
	 * @return void
	 */
	public function _setLogin($login)
	{
		$this->login = $login ;
	}
	/**
	 * <p>M�j du mot de passe</p>
	 * @name Access::_setPassword()
	 * @return void
	 */
	public function _setPassword($pwd)
	{
		$this->password = $pwd ;
	}

	/**
	 * <p>M�j de langue</p>
	 * @name Access::_setLang()
	 * @return void
	 */
	public function _setLang($lng)
	{
		$this->lang = $lng ;
	}
	/**
	 * V�rifie que le login est bien enregistr� et que l'acc�s est autoris�, que le mot de passe est ok
	 *
	 * <p>V�rifiaction de l'acc�s � l'application</p>
	 *
	 * @name Access::_verifAccess()
	 * @return $boolean
	 */
	public function _verifAccess()
	{

		$valid = FALSE ;

		if($this->login != '')
		{
				
			$whoisthere = explode(' ',$this->login);
			if(isset($whoisthere[1]) && $whoisthere[1] != NULL)
			{
				$login = $whoisthere[1];
				$userLogin = $whoisthere[0];
			}else{
				$login = $whoisthere[0];
				$userLogin = $login ;
			}

			$x = new XmlAnalyser() ;
			$this->config = $x->_getDataXmlInArray(ROOT_CONFIG.$this->configFile) ;
			$_SESSION['config'] = $this->config ;

			$maconnexion = MysqlDatabase::GetInstance() ;
			$sql  = 'SELECT * ';
			$sql .= 'FROM t_aut ';
			$sql .= 'WHERE AUT_USER_LOGIN = \''.$login.'\' ' ;
			if($userLogin != $login)
			{
				$sql .= 'AND INT_COD = \'1\' ' ;
			}
			$sql .= 'AND AUT_STA = \'1\' ' ;
			$res = $maconnexion->_bddQuery($sql) ;
			if($maconnexion->_bddNumRows($res) == 1)
			{
				$row = $maconnexion->_bddFetchAssoc($res);
				if( $row['AUT_PWD'] == md5($this->password) )
				{
					if($userLogin != $login)
					{
						$sql  = 'SELECT * ';
						$sql .= 'FROM t_aut ';
						$sql .= 'WHERE AUT_USER_LOGIN = \''.$userLogin.'\' ' ;
						$sql .= 'AND AUT_STA = \'1\' ' ;
						$res = $maconnexion->_bddQuery($sql) ;
						$row = $maconnexion->_bddFetchAssoc($res);
					}

					$this->uid=$row['INT_COD'] ;

					$sql1  = 'SELECT * ';
					$sql1 .= 'FROM t_int ';
					$sql1 .= 'WHERE INT_COD= \''.$this->uid.'\' ' ;
					$res1 = $maconnexion->_bddQuery($sql1) ;
					$row1 = $maconnexion->_bddFetchAssoc($res1);
					$this->username=$row1['INT_NOM'] ;
					$valid = TRUE;
				}
			}
		}
		return $valid ;
	}


	/**
	 * Fonction d'initialisation de la session avec enregistrement des variables dans la table $_SESSION
	 * Pr�alablement on supprime la session existante pour la connexion
	 * pour �viter la conservation des param�tres dans le cas d'une nouvelle connexion
	 *
	 * <p>Initialisation des variables de session</p>
	 *
	 * @name Access::_sessionInit()
	 *
	 * identifiant de l'utilisateur
	 * @param $uid (int)
	 * nom de l'utilisateur
	 * @param $username (string)
	 * langue utilis�e pour l'affichage
	 * @param $lang (string)
	 * @return void
	 */
	public function _sessionInit($uid, $username, $lang)
	{
		//$this->_sessionDestroy();
		session_set_cookie_params(0,ROOT_APPL);
		session_start();
		session_regenerate_id() ;

		$_SESSION['uid'] = $uid ;
		$_SESSION['name'] = $username ;
		$_SESSION['lang'] = $lang ;
		$_SESSION['config'] = $this->config ;
			
	}

	/**
	 * Suppression de la session et de ses variables
	 *
	 * <p>Suppression session</p>
	 *
	 * @name Access::_sessionDestroy()
	 * @return void
	 */
	private function _sessionDestroy()
	{
		if (session_id() != "" || isset($_COOKIE[session_name()]))
		{
			setcookie (session_name(), "", time() - 3600);
			$_SESSION = array();
			session_destroy();
		}
	}
}

?>