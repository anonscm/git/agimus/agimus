<?php
/**
 * 
 * <p>Acc�s � l'application</p>
 * 
 * @name login
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package main
 */
require_once("../../config/param.php");

if (CAS_ACTIVE)
{
// ------------------ CAS-ification application AGIMUS --------*/
// localisation du serveur CAS

  // propre URL
  $service = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

  /** Cette simple fonction réalise l’authentification CAS.
   * @return  le login de l’utilisateur authentifié, ou FALSE.
   */
  function authenticate() {
      global $service ;

      // récupération du ticket (retour du serveur CAS)
      if (!isset($_GET['ticket'])) {
          // pas de ticket : on redirige le navigateur web vers le serveur CAS
          header('Location: ' . CAS_BASE . '/login?service='  . $service);
          exit() ;
      }
      // un ticket a été transmis, on essaie de le valider auprès du serveur CAS
      $fpage = fopen (CAS_BASE . '/serviceValidate?service='
                               . preg_replace('/&/','%26',$service) . '&ticket=' . $ticket,  'r');
      if ($fpage) {
          while (!feof ($fpage)) { $page .= fgets ($fpage, 1024); }
          // analyse de la réponse du serveur CAS
          if (preg_match('|<cas:authenticationSuccess>.*</cas:authenticationSuccess>|mis',$page)) {
              if(preg_match('|<cas:user>(.*)</cas:user>|',$page,$match)){
                  return($match[1]);
              }
          }
      }
      // problème de validation
      return FALSE;
  }


  $login = authenticate();

  if ($login === FALSE ) {
      echo 'Requête non authentifiée (<a href="'.$service.'"><b>Recommencer</b></a>).';
      exit() ;
  }


  // à ce point, l’utilisateur est authentifié
  echo 'Utilisateur connecté : ' . $login . '(<a href="' . CAS_BASE . '/logout"><b>déconnexion</b></a>)';
}

// -------------------------------------------------------------------- //


if(($_POST['login'] != '')&&($_POST['passwd'] != '') &&($_POST['lang'] != '')
  )
  {
    require_once( 'access.php') ; 
    $access = new Access($_POST['login'],$_POST['passwd'], $_POST['lang'] ) ;
    
    if($access->_verifAccess()==TRUE)
    {
    	$username = $access->_getUsername() ;
    	$uid = $access->_getUid() ;
    	$lang = $access->_getLang() ;
      	$access->_sessionInit($uid, $username, $lang) ;
    	$session = new Session() ;
      	$session->_sessionLog();
      	header('Location: '.ROOT_APPL.'/app/main/menu.php');
    }else{
      	//echo "<br>Acces interdit !";
    	header('Location: '.ROOT_APPL.'/index.html') ;
    }     
  }else{
  	//echo "<br>Renseignez tous les champs, s'il vous plait !";
  	header('Location: '.ROOT_APPL.'/index.html') ;
  }
  
?>