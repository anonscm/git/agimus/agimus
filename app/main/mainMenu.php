<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/application.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/affilIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/dcplIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/diplomeIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/domEtuIntra.php') ;

/**
 * 
 * <p>Menu principal de l'application</p>
 * 
 * @name MainMenu
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package main
 */
 
 
 class MainMenu
 { 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    
    /**
    * @var (Array)
    * @desc tableau des modules accessibles
    */
    protected $moduleArray ;
    
    /**
    * @var (String)
    * @desc fichier du template
    */
    private $xtpl_file ;
    
    /**
    * @var (String)
    * @desc chemin du template
    */
    private $xtpl_path ;
    
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name MainMenu::__construct()
    * @return void 
    */    
    public function __construct($session) {
      $this->moduleArray = $session->_getModules() ;
      $this->xtpl_file = 'menu.xtpl' ;
      $this->xtpl_path =$session->_getLang().'/main/' ;

    }
    /**
    * Charge le template associ� et affecte les donn�es
    * 
    * <p>Construction de la page � afficher</p>
    * 
    * @name MainMenu::_makePage()
    * @return string 
    */ 
    
    public function _makePage($session) 
    {
    	global $_arModuleIcons;
    	
      $content = '' ;
      $xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
	  $xtpl->assign('ROOT_APPL', ROOT_APPL) ;
      $xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;

      		foreach($this->moduleArray as $key=>$value)
		{
			$xtpl->assign('ITEM_ICON',$_arModuleIcons[$key]) ;
			$xtpl->assign('ITEM_TITLE', $this->moduleArray[$key]['title']) ;
			$xtpl->assign('ITEM_LINK', $this->moduleArray[$key]['link']) ;
			$xtpl->parse('main.icons') ;		
		}
		
	//v1.2-PPR-28032011 ajout alertes traitement et référentiels
	$arAlertes=array();

	$arAlertes['Application'][2]=Application::_getAlerts(2); // nouvelle valeur détectée	
	$arAlertes['Application'][1]=Application::_getAlerts(1); //référent inconnu	
	
	$arAlertes['Profil'][2]=AffilIntra::_getAlerts(2); // nouvelle valeur détectée	
	$arAlertes['Profil'][1]=AffilIntra::_getAlerts(1); //référent inconnu	
	
	$arAlertes['Filiere'][2]=DcplIntra::_getAlerts(2); // nouvelle valeur détectée	
	$arAlertes['Filiere'][1]=DcplIntra::_getAlerts(1); //référent inconnu	

	$arAlertes['Diplome'][2]=DiplomeIntra::_getAlerts(2); // nouvelle valeur détectée	
	$arAlertes['Diplome'][1]=DiplomeIntra::_getAlerts(1); //référent inconnu	

	$arAlertes['Domaine'][2]=DomEtuIntra::_getAlerts(2); // nouvelle valeur détectée	
	$arAlertes['Domaine'][1]=DomEtuIntra::_getAlerts(1); //référent inconnu	

	$nbAlerts=0;	
	if (count($arAlertes)>0)
	{
		foreach($arAlertes as $alertDim=>$alertDimVals)
		{
			foreach($alertDimVals as $typeAlert=>$dimIdVal)
			{
				foreach($dimIdVal as $id=>$alertLine) 
				{
					$nbAlerts+=1;
					$alertLibDim="";$alertLib="";
					if ($typeAlert==1)
					$alertLibDim.="Absence R&eacute;f&eacute;rent ".$alertDim." :";
					if ($typeAlert==2)
					$alertLibDim.=$alertDim." mis &agrave; jour : ";
					
					$alertLib.=$alertLine." (ref. #".$id.")";
					$xtpl->assign('ALERT_DIM', $alertLibDim) ;
					$xtpl->assign('ALERT_LINE', $alertLib) ;
					$xtpl->parse('main.alertes') ;
				}	
			}
		}		
	}

	if ($nbAlerts==0)
	{
					$xtpl->assign('ALERT_LINE', "Aucune alerte.") ;
					$xtpl->parse('main.alertes') ;
	}

      $xtpl->parse('main');
      $content = $xtpl->text('main') ;      
      $session->_makeMainPage($content) ;
    }
 }
 
?>