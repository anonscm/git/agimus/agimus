<?php
require("../common/library.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><?php echo NOM_APPL." ".VERSION_APPL;?></title>
</head>
<body>
<?php

// connexion a l'application
// templates de classe
define("TPL_MAIN","templates/classgen/tpl_main.php");
define("TPL_ATTRIBUTE","templates/classgen/tpl_attribute.php");
define("TPL_READ_FONC","templates/classgen/tpl_read_fonc.php");
define("TPL_WRITE_FONC","templates/classgen/tpl_write_fonc.php");

$clsToCreate="";
if (isset($_POST["createcls"]))
$clsToCreate=$_POST["createcls"];

$occToCreate="";
if (isset($_POST["createocc"]))
$occToCreate=$_POST["createocc"];

/* =========================================================== */

if ($clsToCreate!="")
{
	$xmlFileName=ROOT_XML."/".$clsToCreate.".xml";
	$xmlStruct=_importXmlFile($xmlFileName);
	if (!is_array($xmlStruct)) dieAppli("Impossible de determiner la structure XML du fichier\"".$xmlFileName."\" !");
	$phpScriptName=ROOT_CLASS."/".$clsToCreate.".php";
	if (!_generateClass($xmlStruct,$phpScriptName)) dieAppli("Impossible de generer le fichier \"$phpScriptName\" !");
	else
	echo "<br>Classe $phpScriptName creee avec succes ! ";
}

echo "<p align=left>Generation de la classe</p><br />";

echo "<form method=post action='createCls.php'>";
echo "Nom de la classe a creer : <input name=createcls id=createcls value=\"$clsToCreate\">";
echo "<input type=submit name='action' value='Creer'>";
echo "</form>";

echo "<hr>";

?>
</body>
</html>

