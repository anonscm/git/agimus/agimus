<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//R�cup�ration de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 56, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['discipline_id'])){
			$disciplineId = $_GET['discipline_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['discipline_id'])){
			$disciplineId = $_POST['discipline_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$disciplineId = 0 ;
			$action = 'create' ;
		}


		require_once( 'refDiscipline.php') ;
		$discipline = new RefDiscipline($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 58, $uid, '_DISCIPLINE_DELETE_NOT_ALLOWED_'))
				{
					$discipline->_delete($disciplineId) ;
					$session->_sessionLogAction(1, '_DISCIPLINE_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_discipline.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 59, $uid, '_DISCIPLINE_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donn�es dans le formulaire
					$discipline->_makeForm($disciplineId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($disciplineId == '')&&($rights->_isActionAllowed('ref', 57, $uid, '_DISCIPLINE_CREATE_NOT_ALLOWED_')))
				{
					$discipline->_checkFormValues($_POST,$action) ;
					$discipline->_setFormValues($_POST) ;
					$discipline->_create() ;
					$session->_sessionLogAction(1, '_DISCIPLINE_CREATE_') ;
				}
				if(($disciplineId != '')&&($rights->_isActionAllowed('ref', 59, $uid, '_DISCIPLINE_UPDATE_NOT_ALLOWED_')))
				{
					$discipline->_checkFormValues($_POST,$action) ;
					$discipline->_setFormValues($_POST) ;
					$discipline->_update() ;
					$session->_sessionLogAction(1, '_DISCIPLINE_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_discipline.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 57, $uid))
				{
					$discipline->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$disciplineId='' ;
				if($rights->_isActionAllowed('ref', 58, $uid))
				{
					$discipline->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$discipline->_makeForm($disciplineId, $valueArray, $docref, $docVal) ;
			}
		}

		$discipline->_makePage($disciplineId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
