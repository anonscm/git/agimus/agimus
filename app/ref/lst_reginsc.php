<?php
//v1.5-PPR-#016 ajout nouvelle dimension
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//Recuperation de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 89, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['reginsc_id'])){
			$reginscId = $_GET['reginsc_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['reginsc_id'])){
			$reginscId = $_POST['reginsc_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$reginscId = 0 ;
			$action = 'create' ;
		}


		require_once( 'refRegInsc.php') ;
		$reginsc = new RefRegInsc($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 91, $uid, '_REG_INSC_DELETE_NOT_ALLOWED_'))
				{
					$reginsc->_delete($reginscId) ;
					$session->_sessionLogAction(1, '_REG_INSC_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_reginsc.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 92, $uid, '_REG_INSC_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donn�es dans le formulaire
					$reginsc->_makeForm($reginscId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($reginscId == '')&&($rights->_isActionAllowed('ref', 90, $uid, '_REG_INSC_CREATE_NOT_ALLOWED_')))
				{
					$reginsc->_checkFormValues($_POST,$action) ;
					$reginsc->_setFormValues($_POST) ;
					$reginsc->_create() ;
					$session->_sessionLogAction(1, '_REG_INSC_CREATE_') ;
				}
				if(($reginscId != '')&&($rights->_isActionAllowed('ref', 92, $uid, '_REG_INSC_UPDATE_NOT_ALLOWED_')))
				{
					$reginsc->_checkFormValues($_POST,$action) ;
					$reginsc->_setFormValues($_POST) ;
					$reginsc->_update() ;
					$session->_sessionLogAction(1, '_REG_INSC_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_reginsc.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 90, $uid))
				{
					$reginsc->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$reginscId='' ;
				if($rights->_isActionAllowed('ref', 91, $uid))
				{
					$reginsc->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$reginsc->_makeForm($reginscId, $valueArray, $docref, $docVal) ;
			}
		}

		$reginsc->_makePage($reginscId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
