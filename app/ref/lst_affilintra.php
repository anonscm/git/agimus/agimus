<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//R�cup�ration de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 72, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['affilintra_id'])){
			$affilintraId = $_GET['affilintra_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['affilintra_id'])){
			$affilintraId = $_POST['affilintra_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$affilintraId = 0 ;
			$action = 'create' ;
		}


		require_once( 'refAffilIntra.php') ;
		$affilintra = new RefAffilIntra($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 74, $uid, '_AFFIL_INTRA_DELETE_NOT_ALLOWED_'))
				{
					$affilintra->_delete($affilintraId) ;
					$session->_sessionLogAction(1, '_AFFIL_INTRA_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_affilintra.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 75, $uid, '_AFFIL_INTRA_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donn�es dans le formulaire
					$affilintra->_makeForm($affilintraId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($affilintraId == '')&&($rights->_isActionAllowed('ref', 73, $uid, '_AFFIL_INTRA_CREATE_NOT_ALLOWED_')))
				{
					$affilintra->_checkFormValues($_POST,$action) ;
					$affilintra->_setFormValues($_POST) ;
					$affilintra->_create() ;
					$session->_sessionLogAction(1, '_AFFIL_INTRA_CREATE_') ;
				}
				if(($affilintraId != '')&&($rights->_isActionAllowed('ref', 75, $uid, '_AFFIL_INTRA_UPDATE_NOT_ALLOWED_')))
				{
					$affilintra->_checkFormValues($_POST,$action) ;
					$affilintra->_setFormValues($_POST) ;
					$affilintra->_update() ;
					$session->_sessionLogAction(1, '_AFFIL_INTRA_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_affilintra.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 73, $uid))
				{
					$affilintra->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$affilintraId='' ;
				if($rights->_isActionAllowed('ref', 74, $uid))
				{
					$affilintra->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$affilintra->_makeForm($affilintraId, $valueArray, $docref, $docVal) ;
			}
		}

		$affilintra->_makePage($affilintraId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
