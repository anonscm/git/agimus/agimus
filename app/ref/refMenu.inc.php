<?php
  require_once( DIR_WWW.ROOT_APPL.'/main/mainMenu.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/structure.php') ;
  require_once( DIR_WWW.ROOT_APPL.'/app/common/person.php') ;
  
/**
 * 
 * <p>Acc�s � l'application</p>
 * 
 * @name menu
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */
 
 
 class refMenu extends MainMenu
 { 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. proprietés    */
    /*~*~*~*~*~*~*~*~*~*~*/
    /**
    * @var (String)
    * @desc fichier du template
    */
    private $xtpl_file ;
    
    /**
    * @var (String)
    * @desc chemin du template
    */
    private $xtpl_path ;
    
        
    /**
    * @var (String)
    * @desc module
    */
    private $module ;
    
        /**
    * @var (Array)
    * @desc tableau des objets du module Ref
    */
    private $refArray  ;
    
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name MysqlDatabase::__construct()
    * @return void 
    */    
    public function __construct($session, $rights) {

      parent::__construct($session, $rights) ;
      $this->module = 'ref' ;
      $this->xtpl_file = 'refmenu.xtpl' ;
      $this->xtpl_path =$session->lang.'/ref/'  ;      
      
    }
    /**
    * V�rifie que le login est bien enregistr� et que l'acc�s est autoris�, que le mot de passe est ok
    * 
    * <p>V�rifiaction de l'acc�s � l'application</p>
    * 
    * @name login::_verifAccess()
    * @return $boolean 
    */     
    private function _countReferences()
    {
       $docu = new Document() ;
	  $this->refArray['document'] = $docu->_countRef() ;     
    }
    
    public function _makePage($session, $rights) 
    {
    	$menuleft = $session->_makeMenuLeft($rights) ;
      $xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
      $xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
      foreach($this->moduleArray as $key=>$value)
      {
        $xtpl->assign('item_value', $value['title']);
        $xtpl->assign('item_link', $value['link']);

        $xtpl->parse('main.table.row.cell');
      }
      $xtpl->parse('main.table.row');
      //Construction du menu en fonction des droits de l'utilisateur
      
      $xtpl->parse('main.table');
      $xtpl->assign('NBR_DOC',$this->refArray['document']) ;
      if($rights->_isActionAllowed($this->module, 14, $session->_getUid()))
      {
            $xtpl->parse('main.docopen');
            $xtpl->parse('main.docclose');
      }
      $xtpl->parse('main');
      $content = $xtpl->text('main') ;
      $session->_makeMainPage($content, $menuleft) ;
    }
 }
 
?>