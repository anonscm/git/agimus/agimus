<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/diplome.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/diplomeIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;

/**
 *
 * <p>Diplome</p>
 *
 * @name Diplome
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class RefDiplome extends Diplome {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    
	 /*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste arborecente des srvIntras
	 */
	private $DiplomeTreeArray ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des regroupements
	 */
	private $RegrComboArray ;

	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Diplome::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'diplomelist.xtpl' ;
		$this->form_file = 'diplomeform.xtpl' ;
		$this->xtpl_tree = 'diplometree.xtpl' ;
		$this->xtpl_path = $this->lang.'/ref/'  ;
		$this->DiplomeTreeArray = $this->_getList() ;

	}

	/**
	 * V�rification des variables re�ues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refDiplome::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray,$action='')
	{
		if($valueArray['diplome_lib'] == '')
		{
			throw new MsgException('_ERROR_LIBELLE_MISSING_')  ;
		}

		if($valueArray['diplome_nbr_an_form'] == '')
		{
			throw new MsgException('_ERROR_VALUE_MISSING_')  ;
		}

		$maconnexion = MysqlDatabase::GetInstance() ;

		//V�rification que le libell� n'est pas d�j� utilis�

		$sql = 'SELECT * FROM t_diplome ';
		$sql .= 'WHERE LIB=  \''.AddSlashes($valueArray['diplome_lib']).'\' ';
		if($valueArray['diplome_id']!=0)
		{
			$sql .= 'AND ID != \''.$valueArray['diplome_id'].'\' ';
		}
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res) >0)
		{
			throw new msgException('_ERROR_LIBELLE_ALREADY_EXISTS_')  ;
		}


	}

	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refDiplome::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
		Diplome::_setId($valueArray['diplome_id']) ;
		Diplome::_setExtnRef($valueArray['diplome_extn_ref']) ;
		Diplome::_setLib($valueArray['diplome_lib']) ;
		Diplome::_setDsc($valueArray['diplome_dsc']) ;
		Diplome::_setIdRegr($valueArray['id_regr']) ;
		Diplome::_setNbrAnForm($valueArray['diplome_nbr_an_form']) ;
	}

	/**
	 * Insertion des donn�es dans le mod�le de pag
	 *
	 * <p>_makePage</p>
	 *
	 * @name Diplome::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($diplomeId=0, $msg='')
	{
		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
			$xtpl->assign('MSG', $msg) ;
			$xtpl->parse('main.msg');
		}


		foreach($this->DiplomeTreeArray as $key=>$value)
		{
			$xtpl->assign('DIPLOME_ID', $key);
			$xtpl->assign('EXTN_REF', $value['EXTN_REF']);
			$xtpl->assign('LIB', $value['LIB']);
			$xtpl->assign('DSC', $value['DSC']);
			$xtpl->assign('REGR_ID', $value['ID_REGR']);
			$xtpl->assign('NBR_AN_FORM', $value['NBR_AN_FORM']);

			//Recupération des fils intra
			$arFils=DiplomeIntra::_getList($key);
			$nbFils=count($arFils);
			
			if(($key != $diplomeId)&&($this->rights->_isActionAllowed('ref', 63, $this->uid)))
			{
				$xtpl->parse('main.list.row.modif');
			}

			//V1.5-PPR-#000 suppression message notice
			if ((isset($value['tree'])) and ($value['tree'] != NULL))
			{
				$treeArray = $value['tree'] ;
				$treeString = $this->_buildTree($treeArray, $maxlevel, 1) ;
				$xtpl->assign('DIPLOME_TREE', $treeString );
				$xtpl->parse('main.list.row.tree');
			}else{
				//v1.2-PPR-28032011 restriction à la suppression des id > 1 (1 : autres)
				//if(($nbFils==0) && ($key != $diplomeId)&&($this->rights->_isActionAllowed('ref', 34, $this->uid)))
				if(($key!=1) && ($nbFils==0) && ($key != $diplomeId)&&($this->rights->_isActionAllowed('ref', 34, $this->uid)))
				{
					$xtpl->parse('main.list.row.delete');
				}
			}
			$xtpl->parse('main.list.row');
		}
		$xtpl->parse('main.list');
		//Construction du menu en fonction des droits de l'utilisateur

		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}
		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}

	// non utilis�e pour l'instant
	private function _buildTree($subtreeArray, $maxlevel = 0, $treelevel=0)
	{
		$levelmax = $maxlevel ;
		$treeArray = $subtreeArray ;
		$level=$treelevel+1 ;
		$treeString = '' ;
		$xtpl = new XTemplate($this->xtpl_tree, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		foreach($treeArray as $key=>$value)
		{
			foreach($value as $key2=>$val2)
			{
				$xtpl->assign('LEVEL_PERE', $level) ;
				$xtpl->parse('main.row.level');

				$xtpl->assign('LEVEL',1 ) ;
				if($maxlevel-$level>0)
				{
					$xtpl->assign('LEVEL_SON',$maxlevel-$level) ;
					$xtpl->parse('main.row.levelson');
				}
				$xtpl->assign('DIPLOME_LIB', $val2['libelle']);
				$xtpl->assign('DIPLOME_ID', $key2);
				if($this->rights->_isActionAllowed('ref', 23, $this->uid))
				{
					$xtpl->parse('main.row.modif');
				}

				if($val2['tree'] != NULL)
				{
					$subtree = $this->_buildTree($val2['tree'], $levelmax, $level) ;
					$xtpl->assign('DIPLOME_TREE', $subtree);
					$xtpl->parse('main.row.tree');
				}else{
					if($this->rights->_isActionAllowed('ref', 22, $this->uid))
					{
						$xtpl->parse('main.row.delete');
					}

				}
				$xtpl->parse('main.row');
			}
		}
		$xtpl->parse('main');
		$treeString = $xtpl->text('main') ;
		return $treeString ;

	}

	public function _makeForm($diplome_id=0, $valueArray=array(), $doc_ref=0, $docVal = array())
	{
		$form = new Form('../lst_diplome.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);
		$config=$_SESSION['config'];

		if(($valueArray == NULL)&&($diplome_id==0))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}
		if($valueArray != NULL)
		{
			$diplome_extn_ref = $valueArray['diplome_extn_ref'] ;
			$diplome_lib = $valueArray['diplome_lib'] ;
			$diplome_id = $valueArray['diplome_id'] ;
			$diplome_dsc = $valueArray['diplome_dsc'] ;
			$id_regr = $valueArray['id_regr'];
			$diplome_nbr_an_form = $valueArray['diplome_nbr_an_form'];

		}else{
			if($diplome_id!=0)
			{
				$diplome_extn_ref =  $this->DiplomeTreeArray[$diplome_id]['EXTN_REF'] ;
				$diplome_lib =  $this->DiplomeTreeArray[$diplome_id]['LIB'] ;
				$diplome_dsc =  $this->DiplomeTreeArray[$diplome_id]['DSC'] ;
				$id_regr =  $this->DiplomeTreeArray[$diplome_id]['ID_REGR'] ;
				$diplome_nbr_an_form = $this->DiplomeTreeArray[$diplome_id]['NBR_AN_FORM'] ;
			}else{
				$diplome_extn_ref = '' ;
				$diplome_lib = '' ;
				$diplome_dsc= '' ;
				$id_regr = 0;
				$diplome_nbr_an_form = 0 ;
			}
		}

		$xtplform->assign('DIPLOME_ID', $form->_mkInput('hidden', 'diplome_id', $diplome_id)) ;
		$xtplform->assign('EXTN_REF', $form->_mkInput('text', 'diplome_extn_ref', $diplome_extn_ref)) ;
		$xtplform->assign('LIB', $form->_mkInput('text', 'diplome_lib',$diplome_lib)) ;
		$xtplform->assign('DSC', $form->_mkInput('text', 'diplome_dsc',$diplome_dsc)) ;
		$xtplform->assign('NBR_AN_FORM', $form->_mkInput('text', 'diplome_nbr_an_form',$diplome_nbr_an_form)) ;
		$xtplform->assign('REGR_ID', $form->_mkInput('hidden','id_regr', $id_regr,'')) ;

		$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('submit', 'Enregistrer' ) );

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	public function _makeView($rights=NULL, $diplome_id=0)
	{
		$form = new Form('../lst_diplome.php') ;
		$xtplform = new XTemplate($this->view_file, $this->xtpl_path);

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Diplome::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>