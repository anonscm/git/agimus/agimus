<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/diplomeIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/diplome.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
//v1.4-PPR-#000 correspondance Diplome Tours
require_once( '../../app/common/corrDiplome.php') ;

/**
 *
 * <p>Service Intra</p>
 *
 * @name DiplomeIntra
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class RefDiplomeIntra extends DiplomeIntra {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés 
	 /*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste arborecente des diplomeintras
	 */
	private $DiplomeIntraTreeArray ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des regroupements
	 */
	private $RegrComboArray ;

	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name DiplomeIntra::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'diplomeintralist.xtpl' ;
		$this->form_file = 'diplomeintraform.xtpl' ;
		$this->xtpl_tree = 'diplomeintratree.xtpl' ;
		$this->xtpl_path = $this->lang.'/ref/'  ;
		$this->DiplomeIntraTreeArray = $this->_getList() ;

		//v1.4-PPR-#000 conversion des libellés sur table de correspondance Tours
	//v1.4-PPR-#004 paramétrage conversion diplomes pour Tours
		if (CONV_DIPLOME_TOURS)
			$this->_convertLib();
		$this->DiplomeIntraTreeArray = array();
		$this->DiplomeIntraTreeArray = $this->_getList() ;
		
		$newDiplome = new Diplome();
		$dummyDiplome=$newDiplome->_getList() ;
		$this->RegrComboArray = $newDiplome->_getComboList() ;
	}

	/**
	 * Conversion des libellés sur base table de correspondance t_corr_diplome
	 * avec mise à jour dynamique de la table t_diplomeintra
	 * <p>_convertLib</p>
	 *
	 * @name refDiplomeIntra::_convertLib()
	 * @param void
	 * @return void
	 */
	private function _convertLib() {
		if ($this->DiplomeIntraTreeArray == NULL)
		return false;
		
		foreach($this->DiplomeIntraTreeArray as $id=>$value)
		{
			$libParam=trim($value["LIB"]);
			$idCorr=CorrDiplome::_existsRef($libParam);
			if ($idCorr>0) {
				$newCorr=new CorrDiplome($idCorr);
				$newDip=new DiplomeIntra($id);
				$newDip->_setLib($newCorr->_getEtapeLib());
				$newDip->_setDsc($newCorr->_getEtapeLib());
				$newDip->_update();
			}
		} 		
	}

	/**
	 * V�rification des variables re�ues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refDiplomeIntra::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray,$action='')
	{
		if($valueArray['diplomeintra_lib'] == '')
		{
			throw new MsgException('_ERROR_LIBELLE_MISSING_')  ;
		}

		if($valueArray['diplomeintra_chaine_type'] == '')
		{
			throw new MsgException('_ERROR_CHAINE_TYPE_MISSING_')  ;
		}

		if($valueArray['id_regr'] == '')
		{
			throw new MsgException('_ERROR_ID_MISSING_')  ;
		}

		if($valueArray['struct_id'] == '')
		{
			throw new MsgException('_ERROR_ID_MISSING_')  ;
		}

		$maconnexion = MysqlDatabase::GetInstance() ;

		//Verification que le libelle n'est pas deja utilise

		$sql = 'SELECT * FROM t_diplome_intra ';
		$sql .= 'WHERE LIB=  \''.AddSlashes($valueArray['diplomeintra_lib']).'\' ';
		if($valueArray['diplomeintra_id']!=0)
		{
			$sql .= 'AND ID != \''.$valueArray['diplomeintra_id'].'\' ';
		}
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res) >0)
		{
			throw new msgException('_ERROR_LIBELLE_ALREADY_EXISTS_')  ;
		}


	}

	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refDiplomeIntra::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
		DiplomeIntra::_setId($valueArray['diplomeintra_id']) ;
		DiplomeIntra::_setExtnRef($valueArray['diplomeintra_extn_ref']) ;
		DiplomeIntra::_setLib($valueArray['diplomeintra_lib']) ;
		DiplomeIntra::_setDsc($valueArray['diplomeintra_dsc']) ;
		DiplomeIntra::_setChaineType($valueArray['diplomeintra_chaine_type']) ;
		DiplomeIntra::_setIdRegr($valueArray['id_regr']) ;
		DiplomeIntra::_setIdStruct($valueArray['struct_id']) ;
	}

	/**
	 * Insertion des donn�es dans le mod�le de pag
	 *
	 * <p>_makePage</p>
	 *
	 * @name DiplomeIntra::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($diplomeintraId=0, $msg='')
	{
		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
			$xtpl->assign('MSG', $msg) ;
			$xtpl->parse('main.msg');
		}


		foreach($this->DiplomeIntraTreeArray as $key=>$value)
		{
			$xtpl->assign('DIPLOME_INTRA_ID', $key);
			$xtpl->assign('EXTN_REF', $value['EXTN_REF']);
			$xtpl->assign('LIB', $value['LIB']);
			$xtpl->assign('DSC', $value['DSC']);
			$xtpl->assign('CHAINE_TYPE', $value['CHAINE_TYPE']);
			$xtpl->assign('REGR_LIB', $this->RegrComboArray[$value['ID_REGR']]);
			if(($key != $diplomeintraId)&&($this->rights->_isActionAllowed('ref', 35, $this->uid)))
			{
				$xtpl->parse('main.list.row.modif');
			}

			if((isset($value['tree'])) and ($value['tree'] != NULL))
			{
				$treeArray = $value['tree'] ;
				$treeString = $this->_buildTree($treeArray, $maxlevel, 1) ;
				$xtpl->assign('DIPLOME_INTRA_TREE', $treeString );
				$xtpl->parse('main.list.row.tree');
			}else{
				//v1.2-PPR-28032011 restriction à la suppression des id > 1 (1 : autres)
				if(($key!=1) && ($key != $diplomeintraId)&&($this->rights->_isActionAllowed('ref', 34, $this->uid)))
				{
					$xtpl->parse('main.list.row.delete');
				}
			}
			$xtpl->parse('main.list.row');
		}
		$xtpl->parse('main.list');
		//Construction du menu en fonction des droits de l'utilisateur

		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}

		// TODO : reprise de données Apogee (sur base diplome LDAP Pau uppacodeetape)
		IF (REPRISE_DONNEES_APOGEE)
		{
			foreach($this->DiplomeIntraTreeArray as $dipIntraId=>$curDip)
			{
				$arLib=$curDip["DSC"];						
					$libDip="";
					$idRegr=1;

					if (strlen($arLib)==6) {
					switch ($arLib[1]) {
						case "A" : $libDip.="DALF/DELF ";break;
						case "C" : $libDip.="Capacité ";break;
						case "E" : $libDip.="DAEU";break;
						case "F" : $libDip.="Preparation Etudes Comptables";break;
						case "H" : $libDip.="HDR";break;
						case "L" : $libDip.="Licence";break;
						case "P" : $libDip.="Preparation Concours Administratifs";break;
						case "R" : $libDip.="Diplome Ingénieur";break;
						case "T" : $libDip.="DUT";break;
						case "U" : $libDip.="Diplome d'Université";break;
						case "W" : $libDip.="Doctorat";break;
						case "Z" : $libDip.="Master";break;
					}

					$diplomeIntraLib=$libDip." ".$arLib[5];
					$idDiplome=Diplome::_existsRef($diplomeIntraLib);
					if ($idDiplome>1) // une seule occurence trouvee
					$idRegr=$idDiplome;

					$diplomeIntraLib.=" ".$arLib[3].$arLib[4];
						
				}
				else
				{
					$diplomeIntraLib="Diplome ".$arLib;
					$idRegr=1;
				}
					// mise à jour finale
					$newDipIntra=new DiplomeIntra($dipIntraId);
					$newDipIntra->_setLib($diplomeIntraLib);
					$newDipIntra->_setIdRegr($idRegr);
					$newDipIntra->_update();
				
			}
		}

		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}

	// non utilis�e pour l'instant
	private function _buildTree($subtreeArray, $maxlevel = 0, $treelevel=0)
	{
		$levelmax = $maxlevel ;
		$treeArray = $subtreeArray ;
		$level=$treelevel+1 ;
		$treeString = '' ;
		$xtpl = new XTemplate($this->xtpl_tree, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		foreach($treeArray as $key=>$value)
		{
			foreach($value as $key2=>$val2)
			{
				$xtpl->assign('LEVEL_PERE', $level) ;
				$xtpl->parse('main.row.level');

				$xtpl->assign('LEVEL',1 ) ;
				if($maxlevel-$level>0)
				{
					$xtpl->assign('LEVEL_SON',$maxlevel-$level) ;
					$xtpl->parse('main.row.levelson');
				}
				$xtpl->assign('DIPLOME_INTRA_LIB', $val2['libelle']);
				$xtpl->assign('DIPLOME_INTRA_ID', $key2);
				if($this->rights->_isActionAllowed('ref', 23, $this->uid))
				{
					$xtpl->parse('main.row.modif');
				}

				if($val2['tree'] != NULL)
				{
					$subtree = $this->_buildTree($val2['tree'], $levelmax, $level) ;
					$xtpl->assign('DIPLOME_INTRA_TREE', $subtree);
					$xtpl->parse('main.row.tree');
				}else{
					if($this->rights->_isActionAllowed('ref', 22, $this->uid))
					{
						$xtpl->parse('main.row.delete');
					}

				}
				$xtpl->parse('main.row');
			}
		}
		$xtpl->parse('main');
		$treeString = $xtpl->text('main') ;
		return $treeString ;

	}

	public function _makeForm($diplomeintra_id=0, $valueArray=array(), $doc_ref=0, $docVal = array())
	{
		$form = new Form('../lst_diplomeintra.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);
		$config=$_SESSION['config'];

		if(($valueArray == NULL)&&($diplomeintra_id==0))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}
		if($valueArray != NULL)
		{
			$diplomeintra_extn_ref = $valueArray['diplomeintra_extn_ref'] ;
			$diplomeintra_lib = $valueArray['diplomeintra_lib'] ;
			$diplomeintra_id = $valueArray['diplomeintra_id'] ;
			$diplomeintra_dsc = $valueArray['diplomeintra_dsc'] ;
			$diplomeintra_chaine_type = $valueArray['diplomeintra_chaine_type'];
			$id_regr = $valueArray['id_regr'];
			$struct_id = $valueArray['struct_id'];

		}else{
			if($diplomeintra_id!=0)
			{
				$diplomeintra_extn_ref =  $this->DiplomeIntraTreeArray[$diplomeintra_id]['EXTN_REF'] ;
				$diplomeintra_lib =  $this->DiplomeIntraTreeArray[$diplomeintra_id]['LIB'] ;
				$diplomeintra_dsc =  $this->DiplomeIntraTreeArray[$diplomeintra_id]['DSC'] ;
				$diplomeintra_chaine_type =  $this->DiplomeIntraTreeArray[$diplomeintra_id]['CHAINE_TYPE'] ;
				$id_regr =  $this->DiplomeIntraTreeArray[$diplomeintra_id]['ID_REGR'] ;
				$struct_id =  $this->DiplomeIntraTreeArray[$diplomeintra_id]['ID_STRUCT'] ;
			}else{
				$diplomeintra_extn_ref = '' ;
				$diplomeintra_lib = '' ;
				$diplomeintra_dsc= '' ;
				$diplomeintra_chaine_type = '';
				$id_regr = 0;
				$struct_id = $config["id_struct"];
			}
		}

		$xtplform->assign('STRUCT_ID', $form->_mkInput('hidden', 'struct_id', $struct_id)) ;
		$xtplform->assign('DIPLOME_INTRA_ID', $form->_mkInput('hidden', 'diplomeintra_id', $diplomeintra_id)) ;
		$xtplform->assign('EXTN_REF', $form->_mkInput('text', 'diplomeintra_extn_ref', $diplomeintra_extn_ref)) ;
		$xtplform->assign('LIB', $form->_mkInput('text', 'diplomeintra_lib',$diplomeintra_lib)) ;
		$xtplform->assign('DSC', $form->_mkInput('text', 'diplomeintra_dsc',$diplomeintra_dsc)) ;
		$xtplform->assign('CHAINE_TYPE', $form->_mkInput('text', 'diplomeintra_chaine_type',$diplomeintra_chaine_type)) ;
		$xtplform->assign('REGR_LIB', $form->_mkSelect('id_regr', $this->RegrComboArray, $id_regr,'')) ;

		$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('submit', 'Enregistrer' ) );

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	public function _makeView($rights=NULL, $diplomeintra_id=0)
	{
		$form = new Form('../lst_diplomeintra.php') ;
		$xtplform = new XTemplate($this->view_file, $this->xtpl_path);

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name DiplomeIntra::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>
