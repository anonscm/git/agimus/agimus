<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//R�cup�ration de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 40, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['dcplintra_id'])){
			$dcplintraId = $_GET['dcplintra_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['dcplintra_id'])){
			$dcplintraId = $_POST['dcplintra_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$dcplintraId = 0 ;
			$action = 'create' ;
		}


		require_once( 'refDcplIntra.php') ;
		$dcplintra = new RefDcplIntra($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 44, $uid, '_DCPL_INTRA_DELETE_NOT_ALLOWED_'))
				{
					$dcplintra->_delete($dcplintraId) ;
					$session->_sessionLogAction(1, '_DCPL_INTRA_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_dcplintra.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 43, $uid, '_DCPL_INTRA_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donn�es dans le formulaire
					$dcplintra->_makeForm($dcplintraId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($dcplintraId == '')&&($rights->_isActionAllowed('ref', 41, $uid, '_DCPL_INTRA_CREATE_NOT_ALLOWED_')))
				{
					$dcplintra->_checkFormValues($_POST,$action) ;
					$dcplintra->_setFormValues($_POST) ;
					$dcplintra->_create() ;
					$session->_sessionLogAction(1, '_DCPL_INTRA_CREATE_') ;
				}
				if(($dcplintraId != '')&&($rights->_isActionAllowed('ref', 43, $uid, '_DCPL_INTRA_UPDATE_NOT_ALLOWED_')))
				{
					$dcplintra->_checkFormValues($_POST,$action) ;
					$dcplintra->_setFormValues($_POST) ;
					$dcplintra->_update() ;
					$session->_sessionLogAction(1, '_DCPL_INTRA_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_dcplintra.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 41, $uid))
				{
					$dcplintra->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$dcplintraId='' ;
				if($rights->_isActionAllowed('ref', 42, $uid))
				{
					$dcplintra->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$dcplintra->_makeForm($dcplintraId, $valueArray, $docref, $docVal) ;
			}
		}

		$dcplintra->_makePage($dcplintraId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
