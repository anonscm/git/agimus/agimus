<?php
//v1.5-PPR-#016 ajout nouvelle dimension
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//Recuperation de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 77, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['compintra_id'])){
			$compintraId = $_GET['compintra_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['compintra_id'])){
			$compintraId = $_POST['compintra_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$compintraId = 0 ;
			$action = 'create' ;
		}


		require_once( 'refCompIntra.php') ;
		$compintra = new RefCompIntra($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 79, $uid, '_COMP_INTRA_DELETE_NOT_ALLOWED_'))
				{
					$compintra->_delete($compintraId) ;
					$session->_sessionLogAction(1, '_COMP_INTRA_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_compintra.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 80, $uid, '_COMP_INTRA_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donnees dans le formulaire
					$compintra->_makeForm($compintraId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($compintraId == '')&&($rights->_isActionAllowed('ref', 78, $uid, '_COMP_INTRA_CREATE_NOT_ALLOWED_')))
				{
					$compintra->_checkFormValues($_POST,$action) ;
					$compintra->_setFormValues($_POST) ;
					$compintra->_create() ;
					$session->_sessionLogAction(1, '_COMP_INTRA_CREATE_') ;
				}
				if(($compintraId != '')&&($rights->_isActionAllowed('ref', 80, $uid, '_COMP_INTRA_UPDATE_NOT_ALLOWED_')))
				{
					$compintra->_checkFormValues($_POST,$action) ;
					$compintra->_setFormValues($_POST) ;
					$compintra->_update() ;
					$session->_sessionLogAction(1, '_COMP_INTRA_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_compintra.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 78, $uid))
				{
					$compintra->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$compintraId='' ;
				if($rights->_isActionAllowed('ref', 79, $uid))
				{
					$compintra->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$compintra->_makeForm($compintraId, $valueArray, $docref, $docVal) ;
			}
		}

		$compintra->_makePage($compintraId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
