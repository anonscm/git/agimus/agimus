<?php
//v1.5-PPR-#016 ajout nouvelle dimension
require_once( DIR_WWW.ROOT_APPL.'/app/common/regInscIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/regInsc.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;

/**
 *
 * <p>Regime d'Inscription Intra</p>
 *
 * @name RegInscIntra
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class RefRegInscIntra extends RegInscIntra {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés 
	 /*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste arborecente des reginscintras
	 */
	private $RegInscIntraTreeArray ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des regroupements
	 */
	private $RegrComboArray ;

	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name RegInscIntra::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'reginscintralist.xtpl' ;
		$this->form_file = 'reginscintraform.xtpl' ;
		$this->xtpl_tree = 'reginscintratree.xtpl' ;
		$this->xtpl_path = $this->lang.'/ref/'  ;
		$this->RegInscIntraTreeArray = $this->_getList() ;
		$newRegInsc = new RegInsc();
		$dummyRegInsc=$newRegInsc->_getList() ;
		$this->RegrComboArray = $newRegInsc->_getComboList() ;
	}

	/**
	 * V�rification des variables re�ues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refRegInscIntra::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray,$action='')
	{
		if($valueArray['reginscintra_lib'] == '')
		{
			throw new MsgException('_ERROR_LIBELLE_MISSING_')  ;
		}

		if($valueArray['reginscintra_chaine_type'] == '')
		{
			throw new MsgException('_ERROR_CHAINE_TYPE_MISSING_')  ;
		}

		if($valueArray['id_regr'] == '')
		{
			throw new MsgException('_ERROR_ID_MISSING_')  ;
		}

		if($valueArray['struct_id'] == '')
		{
			throw new MsgException('_ERROR_ID_MISSING_')  ;
		}

		$maconnexion = MysqlDatabase::GetInstance() ;

		//Verification que le libelle n'est pas deja utilise

		$sql = 'SELECT * FROM t_reg_insc_intra ';
		$sql .= 'WHERE LIB=  \''.AddSlashes($valueArray['reginscintra_lib']).'\' ';
		if($valueArray['reginscintra_id']!=0)
		{
			$sql .= 'AND ID != \''.$valueArray['reginscintra_id'].'\' ';
		}
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res) >0)
		{
			throw new msgException('_ERROR_LIBELLE_ALREADY_EXISTS_')  ;
		}


	}

	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refRegInscIntra::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
		RegInscIntra::_setId($valueArray['reginscintra_id']) ;
		RegInscIntra::_setExtnRef($valueArray['reginscintra_extn_ref']) ;
		RegInscIntra::_setLib($valueArray['reginscintra_lib']) ;
		RegInscIntra::_setDsc($valueArray['reginscintra_dsc']) ;
		RegInscIntra::_setChaineType($valueArray['reginscintra_chaine_type']) ;
		RegInscIntra::_setIdRegr($valueArray['id_regr']) ;
		RegInscIntra::_setIdStruct($valueArray['struct_id']) ;
	}

	/**
	 * Insertion des donn�es dans le mod�le de pag
	 *
	 * <p>_makePage</p>
	 *
	 * @name RegInscIntra::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($reginscintraId=0, $msg='')
	{
		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
			$xtpl->assign('MSG', $msg) ;
			$xtpl->parse('main.msg');
		}


		foreach($this->RegInscIntraTreeArray as $key=>$value)
		{
			$xtpl->assign('REG_INSC_INTRA_ID', $key);
			$xtpl->assign('EXTN_REF', $value['EXTN_REF']);
			$xtpl->assign('LIB', $value['LIB']);
			$xtpl->assign('DSC', $value['DSC']);
			$xtpl->assign('CHAINE_TYPE', $value['CHAINE_TYPE']);
			$xtpl->assign('REGR_LIB', $this->RegrComboArray[$value['ID_REGR']]);
			if(($key != $reginscintraId)&&($this->rights->_isActionAllowed('ref', 35, $this->uid)))
			{
				$xtpl->parse('main.list.row.modif');
			}

			if($value['tree'] != NULL)
			{
				$treeArray = $value['tree'] ;
				$treeString = $this->_buildTree($treeArray, $maxlevel, 1) ;
				$xtpl->assign('REG_INSC_INTRA_TREE', $treeString );
				$xtpl->parse('main.list.row.tree');
			}else{
				//v1.2-PPR-28032011 restriction à la suppression des id > 1 (1 : autres)
				if(($key!=1) && ($key != $reginscintraId)&&($this->rights->_isActionAllowed('ref', 34, $this->uid)))
				{
					$xtpl->parse('main.list.row.delete');
				}
			}
			$xtpl->parse('main.list.row');
		}
		$xtpl->parse('main.list');

		// TODO : reprise de données Apogee (sur base diplome LDAP Pau uppacodeetape)
		IF (REPRISE_DONNEES_APOGEE)
		{
			foreach($this->RegInscIntraTreeArray as $domIntraId=>$curDom)
			{
				if (substr($curDom["LIB"],0,1)=="~") // on ne traite que les nouveaux detectés
				{
					$arLib=$curDom["DSC"];
					$libDom="";
					$idRegr=0;

					if (strlen($arLib)==6) {
						$domaineIntraLib="Domaine ".$arLib[3].$arLib[4];
					}
					else
					{
						$domaineIntraLib="Domaine ".$arLib;
					}
					// mise à jour finale
					$newDomIntra=new RegInscIntra($domIntraId);
					$newDomIntra->_setLib($domaineIntraLib);
					$newDomIntra->_setIdRegr($idRegr);
					$newDomIntra->_update();
				}
			}
		}

		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}
		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}

	// non utilis�e pour l'instant
	private function _buildTree($subtreeArray, $maxlevel = 0, $treelevel=0)
	{
		$levelmax = $maxlevel ;
		$treeArray = $subtreeArray ;
		$level=$treelevel+1 ;
		$treeString = '' ;
		$xtpl = new XTemplate($this->xtpl_tree, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		foreach($treeArray as $key=>$value)
		{
			foreach($value as $key2=>$val2)
			{
				$xtpl->assign('LEVEL_PERE', $level) ;
				$xtpl->parse('main.row.level');

				$xtpl->assign('LEVEL',1 ) ;
				if($maxlevel-$level>0)
				{
					$xtpl->assign('LEVEL_SON',$maxlevel-$level) ;
					$xtpl->parse('main.row.levelson');
				}
				$xtpl->assign('REG_INSC_INTRA_LIB', $val2['libelle']);
				$xtpl->assign('REG_INSC_INTRA_ID', $key2);
				if($this->rights->_isActionAllowed('ref', 23, $this->uid))
				{
					$xtpl->parse('main.row.modif');
				}

				if($val2['tree'] != NULL)
				{
					$subtree = $this->_buildTree($val2['tree'], $levelmax, $level) ;
					$xtpl->assign('REG_INSC_INTRA_TREE', $subtree);
					$xtpl->parse('main.row.tree');
				}else{
					if($this->rights->_isActionAllowed('ref', 22, $this->uid))
					{
						$xtpl->parse('main.row.delete');
					}

				}
				$xtpl->parse('main.row');
			}
		}
		$xtpl->parse('main');
		$treeString = $xtpl->text('main') ;
		return $treeString ;

	}

	public function _makeForm($reginscintra_id=0, $valueArray=array(), $doc_ref=0, $docVal = array())
	{
		$form = new Form('../lst_reginscintra.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);
		$config=$_SESSION['config'];

		if(($valueArray == NULL)&&($reginscintra_id==0))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}
		if($valueArray != NULL)
		{
			$reginscintra_extn_ref = $valueArray['reginscintra_extn_ref'] ;
			$reginscintra_lib = $valueArray['reginscintra_lib'] ;
			$reginscintra_id = $valueArray['reginscintra_id'] ;
			$reginscintra_dsc = $valueArray['reginscintra_dsc'] ;
			$reginscintra_chaine_type = $valueArray['reginscintra_chaine_type'];
			$id_regr = $valueArray['id_regr'];
			$struct_id = $valueArray['struct_id'];

		}else{
			if($reginscintra_id!=0)
			{
				$reginscintra_extn_ref =  $this->RegInscIntraTreeArray[$reginscintra_id]['EXTN_REF'] ;
				$reginscintra_lib =  $this->RegInscIntraTreeArray[$reginscintra_id]['LIB'] ;
				$reginscintra_dsc =  $this->RegInscIntraTreeArray[$reginscintra_id]['DSC'] ;
				$reginscintra_chaine_type =  $this->RegInscIntraTreeArray[$reginscintra_id]['CHAINE_TYPE'] ;
				$id_regr =  $this->RegInscIntraTreeArray[$reginscintra_id]['ID_REGR'] ;
				$struct_id =  $this->RegInscIntraTreeArray[$reginscintra_id]['ID_STRUCT'] ;
			}else{
				$reginscintra_extn_ref = '' ;
				$reginscintra_lib = '' ;
				$reginscintra_dsc= '' ;
				$reginscintra_chaine_type = '';
				$id_regr = 0;
				$struct_id = $config["id_struct"];
			}
		}

		$xtplform->assign('STRUCT_ID', $form->_mkInput('hidden', 'struct_id', $struct_id)) ;
		$xtplform->assign('REG_INSC_INTRA_ID', $form->_mkInput('hidden', 'reginscintra_id', $reginscintra_id)) ;
		$xtplform->assign('EXTN_REF', $form->_mkInput('text', 'reginscintra_extn_ref', $reginscintra_extn_ref)) ;
		$xtplform->assign('LIB', $form->_mkInput('text', 'reginscintra_lib',$reginscintra_lib)) ;
		$xtplform->assign('DSC', $form->_mkInput('text', 'reginscintra_dsc',$reginscintra_dsc)) ;
		$xtplform->assign('CHAINE_TYPE', $form->_mkInput('text', 'reginscintra_chaine_type',$reginscintra_chaine_type)) ;
		$xtplform->assign('REGR_LIB', $form->_mkSelect('id_regr', $this->RegrComboArray, $id_regr,'')) ;

		$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('submit', 'Enregistrer' ) );

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	public function _makeView($rights=NULL, $reginscintra_id=0)
	{
		$form = new Form('../lst_reginscintra.php') ;
		$xtplform = new XTemplate($this->view_file, $this->xtpl_path);

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name RegInscIntra::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>