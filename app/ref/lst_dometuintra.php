<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//Recuperation de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 40, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['dometuintra_id'])){
			$dometuintraId = $_GET['dometuintra_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['dometuintra_id'])){
			$dometuintraId = $_POST['dometuintra_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$dometuintraId = 0 ;
			$action = 'create' ;
		}


		require_once( 'refDomEtuIntra.php') ;
		$dometuintra = new RefDomEtuIntra($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 44, $uid, '_DOMETU_INTRA_DELETE_NOT_ALLOWED_'))
				{
					$dometuintra->_delete($dometuintraId) ;
					$session->_sessionLogAction(1, '_DOMETU_INTRA_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_dometuintra.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 43, $uid, '_DOMETU_INTRA_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donn�es dans le formulaire
					$dometuintra->_makeForm($dometuintraId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($dometuintraId == '')&&($rights->_isActionAllowed('ref', 41, $uid, '_DOMETU_INTRA_CREATE_NOT_ALLOWED_')))
				{
					$dometuintra->_checkFormValues($_POST,$action) ;
					$dometuintra->_setFormValues($_POST) ;
					$dometuintra->_create() ;
					$session->_sessionLogAction(1, '_DOMETU_INTRA_CREATE_') ;
				}
				if(($dometuintraId != '')&&($rights->_isActionAllowed('ref', 43, $uid, '_DOMETU_INTRA_UPDATE_NOT_ALLOWED_')))
				{
					$dometuintra->_checkFormValues($_POST,$action) ;
					$dometuintra->_setFormValues($_POST) ;
					$dometuintra->_update() ;
					$session->_sessionLogAction(1, '_DOMETU_INTRA_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_dometuintra.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 41, $uid))
				{
					$dometuintra->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$dometuintraId='' ;
				if($rights->_isActionAllowed('ref', 42, $uid))
				{
					$dometuintra->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$dometuintra->_makeForm($dometuintraId, $valueArray, $docref, $docVal) ;
			}
		}

		$dometuintra->_makePage($dometuintraId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
