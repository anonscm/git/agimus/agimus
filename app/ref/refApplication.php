<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/srvIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/application.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;

/**
 *
 * <p>Application</p>
 *
 * @name Application
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class RefApplication extends Application {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    
	 /*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste arborecente des applications
	 */
	private $AppliTreeArray ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des services intra
	 */
	private $SrvIntraComboArray ;

	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Application::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'applilist.xtpl' ;
		$this->form_file = 'appliform.xtpl' ;
		$this->xtpl_tree = 'applitree.xtpl' ;
		$this->xtpl_path = $this->lang.'/ref/'  ;
		$this->AppliTreeArray = $this->_getList() ;
		$newSrvIntra = new SrvIntra();
		$dummySrv=$newSrvIntra->_getList() ;
		$this->SrvIntraComboArray = $newSrvIntra->_getComboList() ;

	}

	/**
	 * V�rification des variables re�ues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refApplication::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray,$action='')
	{
		if($valueArray['appli_lib'] == '')
		{
			throw new MsgException('_ERROR_LIBELLE_MISSING_')  ;
		}

		if($valueArray['appli_url_type'] == '')
		{
			throw new MsgException('_ERROR_URL_TYPE_MISSING_')  ;
		}

		if($valueArray['id_srv_intra'] == '')
		{
			throw new MsgException('_ERROR_ID_MISSING_')  ;
		}

		if($valueArray['struct_id'] == '')
		{
			throw new MsgException('_ERROR_ID_MISSING_')  ;
		}

		$maconnexion = MysqlDatabase::GetInstance() ;

		//V�rification que le libell� n'est pas d�j� utilis�

		$sql = 'SELECT * FROM t_application ';
		$sql .= 'WHERE LIB=  \''.AddSlashes($valueArray['appli_lib']).'\' ';
		if($valueArray['appli_id']!=0)
		{
			$sql .= 'AND ID != \''.$valueArray['appli_id'].'\' ';
		}
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res) >0)
		{
			throw new msgException('_ERROR_LIBELLE_ALREADY_EXISTS_')  ;
		}

		//V�rification que l'URL-Type n'est pas d�j� utilis�

		$sql = 'SELECT * FROM t_application ';
		$sql .= 'WHERE URL_TYPE=  \''.AddSlashes($valueArray['appli_url_type']).'\' ';
		if($valueArray['appli_id']!=0)
		{
			$sql .= 'AND ID != \''.$valueArray['appli_id'].'\' ';
		}
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res) >0)
		{
			throw new msgException('_ERROR_URL_TYPE_ALREADY_EXISTS_')  ;
		}

	}

	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refApplication::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
		Application::_setId($valueArray['appli_id']) ;
		Application::_setLib($valueArray['appli_lib']) ;
		Application::_setDsc($valueArray['appli_dsc']) ;
		Application::_setUrlType($valueArray['appli_url_type']) ;
		Application::_setIdSrvIntra($valueArray['id_srv_intra']) ;
		Application::_setIdStruct($valueArray['struct_id']) ;
	}

	/**
	 * Insertion des donn�es dans le mod�le de pag
	 *
	 * <p>_makePage</p>
	 *
	 * @name Application::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($appliId=0, $msg='')
	{
		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
			$xtpl->assign('MSG', $msg) ;
			$xtpl->parse('main.msg');
		}


		foreach($this->AppliTreeArray as $key=>$value)
		{
			$xtpl->assign('APPLI_ID', $key);
			$xtpl->assign('LIB', $value['LIB']);
			$xtpl->assign('DSC', $value['DSC']);
			$xtpl->assign('URL_TYPE', $value['URL_TYPE']);
			$xtpl->assign('SRV_INTRA_LIB', $this->SrvIntraComboArray[$value['ID_SRV_INTRA']]);
			if(($key != $appliId)&&($this->rights->_isActionAllowed('ref', 35, $this->uid)))
			{
				$xtpl->parse('main.list.row.modif');
			}

			//v1.3-PPR-24052011 correctif undefined index
			if((isset($value['tree'])) and ($value['tree'] != NULL))
			{
				$treeArray = $value['tree'] ;
				$maxlevel=1;
				$treeString = $this->_buildTree($treeArray, $maxlevel, 1) ;
				$xtpl->assign('APPLI_TREE', $treeString );
				$xtpl->parse('main.list.row.tree');
			}else{

				require_once (DIR_WWW.ROOT_APPL.'/app/common/cnx.php') ;
				//contrôle préalable appli non rattachée
				$nbCnx=Cnx::_countRef($key);

				//v1.2-PPR-28032011 restriction à la suppression des id > 1 (1 : autres)
				if(($nbCnx==0) && ($key!=1) && ($key != $appliId)&&($this->rights->_isActionAllowed('ref', 34, $this->uid)))
				{
					$xtpl->parse('main.list.row.delete');
				}
			}
			$xtpl->parse('main.list.row');
		}
		$xtpl->parse('main.list');
		//Construction du menu en fonction des droits de l'utilisateur

		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}
		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}

	// non utilis�e pour l'instant
	private function _buildTree($subtreeArray, $maxlevel = 0, $treelevel=0)
	{
		$levelmax = $maxlevel ;
		$treeArray = $subtreeArray ;
		$level=$treelevel+1 ;
		$treeString = '' ;
		$xtpl = new XTemplate($this->xtpl_tree, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		foreach($treeArray as $key=>$value)
		{
			foreach($value as $key2=>$val2)
			{
				$xtpl->assign('LEVEL_PERE', $level) ;
				$xtpl->parse('main.row.level');

				$xtpl->assign('LEVEL',1 ) ;
				if($maxlevel-$level>0)
				{
					$xtpl->assign('LEVEL_SON',$maxlevel-$level) ;
					$xtpl->parse('main.row.levelson');
				}
				$xtpl->assign('APPLI_LIB', $val2['libelle']);
				$xtpl->assign('APPLI_ID', $key2);
				if($this->rights->_isActionAllowed('ref', 23, $this->uid))
				{
					$xtpl->parse('main.row.modif');
				}

				if($val2['tree'] != NULL)
				{
					$subtree = $this->_buildTree($val2['tree'], $levelmax, $level) ;
					$xtpl->assign('APPLI_TREE', $subtree);
					$xtpl->parse('main.row.tree');
				}else{
					if($this->rights->_isActionAllowed('ref', 22, $this->uid))
					{
						$xtpl->parse('main.row.delete');
					}

				}
				$xtpl->parse('main.row');
			}
		}
		$xtpl->parse('main');
		$treeString = $xtpl->text('main') ;
		return $treeString ;

	}

	public function _makeForm($appli_id=0, $valueArray=array(), $doc_ref=0, $docVal = array())
	{
		$form = new Form('../lst_appli.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);
		$config=$_SESSION['config'];

		if(($valueArray == NULL)&&($appli_id==0))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}
		if($valueArray != NULL)
		{
			$appli_lib = $valueArray['appli_lib'] ;
			$appli_id = $valueArray['appli_id'] ;
			$appli_dsc = $valueArray['appli_dsc'] ;
			$appli_url_type = $valueArray['appli_url_type'];
			$id_srv_intra = $valueArray['id_srv_intra'];
			$struct_id = $valueArray['struct_id'];

		}else{
			if($appli_id!=0)
			{
				$appli_lib =  $this->AppliTreeArray[$appli_id]['LIB'] ;
				$appli_dsc =  $this->AppliTreeArray[$appli_id]['DSC'] ;
				$appli_url_type =  $this->AppliTreeArray[$appli_id]['URL_TYPE'] ;
				$id_srv_intra =  $this->AppliTreeArray[$appli_id]['ID_SRV_INTRA'] ;
				$struct_id =  $this->AppliTreeArray[$appli_id]['ID_STRUCT'] ;
			}else{
				$appli_lib = '' ;
				$appli_dsc= '' ;
				$appli_url_type = '';
				$id_srv_intra = '';
				$struct_id = $config["id_struct"];
			}
		}

		$xtplform->assign('STRUCT_ID', $form->_mkInput('hidden', 'struct_id', $struct_id)) ;
		$xtplform->assign('APPLI_ID', $form->_mkInput('hidden', 'appli_id', $appli_id)) ;
		$xtplform->assign('LIB', $form->_mkInput('text', 'appli_lib',$appli_lib)) ;
		$xtplform->assign('DSC', $form->_mkInput('text', 'appli_dsc',$appli_dsc)) ;
		$xtplform->assign('URL_TYPE', $form->_mkInput('text', 'appli_url_type',$appli_url_type)) ;
		$xtplform->assign('SRV_INTRA_LIB', $form->_mkSelect('id_srv_intra', $this->SrvIntraComboArray, $id_srv_intra,'')) ;
		$xtplform->assign('INDIC_REGR', $form->_mkCheckBox('indic_regr'));

		$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('submit', 'Enregistrer' ) );

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	public function _makeView($rights=NULL, $appli_id=0)
	{
		$form = new Form('../lst_appli.php') ;
		$xtplform = new XTemplate($this->view_file, $this->xtpl_path);

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Application::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>