<?php
//v1.5-PPR-#016 ajout nouvelle dimension
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//Recuperation de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 85, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['reginscintra_id'])){
			$reginscintraId = $_GET['reginscintra_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['reginscintra_id'])){
			$reginscintraId = $_POST['reginscintra_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$reginscintraId = 0 ;
			$action = 'create' ;
		}


		require_once( 'refRegInscIntra.php') ;
		$reginscintra = new RefRegInscIntra($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 87, $uid, '_REG_INSC_INTRA_DELETE_NOT_ALLOWED_'))
				{
					$reginscintra->_delete($reginscintraId) ;
					$session->_sessionLogAction(1, '_REG_INSC_INTRA_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_reginscintra.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 88, $uid, '_REG_INSC_INTRA_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donnees dans le formulaire
					$reginscintra->_makeForm($reginscintraId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($reginscintraId == '')&&($rights->_isActionAllowed('ref', 86, $uid, '_REG_INSC_INTRA_CREATE_NOT_ALLOWED_')))
				{
					$reginscintra->_checkFormValues($_POST,$action) ;
					$reginscintra->_setFormValues($_POST) ;
					$reginscintra->_create() ;
					$session->_sessionLogAction(1, '_REG_INSC_INTRA_CREATE_') ;
				}
				if(($reginscintraId != '')&&($rights->_isActionAllowed('ref', 88, $uid, '_REG_INSC_INTRA_UPDATE_NOT_ALLOWED_')))
				{
					$reginscintra->_checkFormValues($_POST,$action) ;
					$reginscintra->_setFormValues($_POST) ;
					$reginscintra->_update() ;
					$session->_sessionLogAction(1, '_REG_INSC_INTRA_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_reginscintra.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 86, $uid))
				{
					$reginscintra->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$reginscintraId='' ;
				if($rights->_isActionAllowed('ref', 87, $uid))
				{
					$reginscintra->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$reginscintra->_makeForm($reginscintraId, $valueArray, $docref, $docVal) ;
			}
		}

		$reginscintra->_makePage($reginscintraId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
