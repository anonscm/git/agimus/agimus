<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//R�cup�ration de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 64, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['dometu_id'])){
			$dometuId = $_GET['dometu_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['dometu_id'])){
			$dometuId = $_POST['dometu_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$dometuId = 0 ;
			$action = 'create' ;
		}


		require_once( 'refDomEtu.php') ;
		$dometu = new RefDomEtu($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 68, $uid, '_DOMETU_DELETE_NOT_ALLOWED_'))
				{
					$dometu->_delete($dometuId) ;
					$session->_sessionLogAction(1, '_DOMETU_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_dometu.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 67, $uid, '_DOMETU_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donn�es dans le formulaire
					$dometu->_makeForm($dometuId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($dometuId == '')&&($rights->_isActionAllowed('ref', 65, $uid, '_DOMETU_CREATE_NOT_ALLOWED_')))
				{
					$dometu->_checkFormValues($_POST,$action) ;
					$dometu->_setFormValues($_POST) ;
					$dometu->_create() ;
					$session->_sessionLogAction(1, '_DOMETU_CREATE_') ;
				}
				if(($dometuId != '')&&($rights->_isActionAllowed('ref', 67, $uid, '_DOMETU_UPDATE_NOT_ALLOWED_')))
				{
					$dometu->_checkFormValues($_POST,$action) ;
					$dometu->_setFormValues($_POST) ;
					$dometu->_update() ;
					$session->_sessionLogAction(1, '_DOMETU_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_dometu.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 65, $uid))
				{
					$dometu->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$dometuId='' ;
				if($rights->_isActionAllowed('ref', 66, $uid))
				{
					$dometu->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$dometu->_makeForm($dometuId, $valueArray, $docref, $docVal) ;
			}
		}

		$dometu->_makePage($dometuId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
