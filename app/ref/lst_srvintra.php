<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//R�cup�ration de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 36, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['srvintra_id'])){
			$srvintraId = $_GET['srvintra_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['srvintra_id'])){
			$srvintraId = $_POST['srvintra_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$srvintraId = 0 ;
			$action = 'create' ;
		}


		require_once( 'refSrvIntra.php') ;
		$srvintra = new RefSrvIntra($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 40, $uid, '_SRV_INTRA_DELETE_NOT_ALLOWED_'))
				{
					$srvintra->_delete($srvintraId) ;
					$session->_sessionLogAction(1, '_SRV_INTRA_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_srvintra.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 39, $uid, '_SRV_INTRA_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donn�es dans le formulaire
					$srvintra->_makeForm($srvintraId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($srvintraId == '')&&($rights->_isActionAllowed('ref', 37, $uid, '_SRV_INTRA_CREATE_NOT_ALLOWED_')))
				{
					$srvintra->_checkFormValues($_POST,$action) ;
					$srvintra->_setFormValues($_POST) ;
					$srvintra->_create() ;
					$session->_sessionLogAction(1, '_SRV_INTRA_CREATE_') ;
				}
				if(($srvintraId != '')&&($rights->_isActionAllowed('ref', 39, $uid, '_SRV_INTRA_UPDATE_NOT_ALLOWED_')))
				{
					$srvintra->_checkFormValues($_POST,$action) ;
					$srvintra->_setFormValues($_POST) ;
					$srvintra->_update() ;
					$session->_sessionLogAction(1, '_SRV_INTRA_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_srvintra.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 37, $uid))
				{
					$srvintra->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$srvintraId='' ;
				if($rights->_isActionAllowed('ref', 38, $uid))
				{
					$srvintra->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$srvintra->_makeForm($srvintraId, $valueArray, $docref, $docVal) ;
			}
		}

		$srvintra->_makePage($srvintraId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
