<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//R�cup�ration de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;


$docref=0;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 24, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;
		$denombrId=0;
		$structId='';

		if (isset($_GET['generate']))
		{
			$action = 'generate';
		}
		else
		{
			if(isset($_GET['denombr_id'])){
				$denombrId = $_GET['denombr_id'] ;
				$structId = $_GET['id_struct'] ;
				if(isset($_GET['del'])){
					$action = 'delete' ;
				}else{
					$action = 'update' ;
				}
			}elseif(isset($_POST['denombr_id'])){
				$denombrId = $_POST['denombr_id'] ;
				$structId = $_POST['id_struct'] ;
				$valueArray = $_POST ;
				$action = 'submit' ;
			}else{
				$denombrId = 0 ;
				$action = 'create' ;
				if (isset($_SESSION['denombr_ref_dim']))
				{
					if ($_SESSION['denombr_ref_dim']>0)
					$valueArray['REF_DIM']=$_SESSION['denombr_ref_dim'];
					$_SESSION['denombr_ref_dim']=NULL;
					unset($_SESSION['denombr_ref_dim']);
				}
			}
		}


		require_once( 'refDenombrement.php') ;
		$denombr = new RefDenombrement($session, $rights) ;
		try{
			// generation automatique des dénombrements (obsolete)	
			if ($action == 'generate'){
				if($rights->_isActionAllowed('ref', 25, $uid, '_DENOMBR_DELETE_NOT_ALLOWED_'))
				{
					$denombr->_autoGenerate();
					$session->_sessionLogAction(1, '_DENOMBR_GENERATE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_denombr.php');
					exit() ;
				}
			}
			elseif($action == 'delete'){
				if($rights->_isActionAllowed('ref', 26, $uid, '_DENOMBR_DELETE_NOT_ALLOWED_'))
				{
					$denombr->_delete($denombrId,$structId) ;
					$session->_sessionLogAction(1, '_DENOMBR_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_denombr.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 27, $uid, '_DENOMBR_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donnees dans le formulaire
					$denombr->_makeForm($denombrId, $structId,$valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($denombrId == 0)&&($rights->_isActionAllowed('ref', 25, $uid, '_DENOMBR_CREATE_NOT_ALLOWED_')))
				{
					$ctr=$denombr->_checkFormValues($_POST) ; //$ctr=false si ref_dim et id_dim ne sont pas tous les deux renseign�s
					$denombr->_setFormValues($_POST) ;
					if ($ctr)
					{
						$denombr->_create() ;
						$session->_sessionLogAction(1, '_DENOMBR_CREATE_') ;
						$_SESSION['denombr_ref_dim']='';
					}
					else
					{
						$_SESSION['denombr_ref_dim']=$_POST['ref_dim'];
					}
				}
				if(($denombrId != 0)&&($rights->_isActionAllowed('ref', 27, $uid, '_DENOMBR_UPDATE_NOT_ALLOWED_')))
				{
					$denombr->_checkFormValues($_POST) ;
					$denombr->_setFormValues($_POST) ;
					$denombr->_update() ;
					$session->_sessionLogAction(1, '_DENOMBR_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_denombr.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 25, $uid))
				{
					//$denombr->_makeForm() ;
					$denombr->_makeForm($denombrId, $structId,$valueArray, $docref, $docVal) ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$denombrId=0 ;
				if($rights->_isActionAllowed('ref', 26, $uid))
				{
					$denombr->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$denombr->_makeForm($denombrId,$structId, $valueArray, $docref, $docVal) ;
			}
		}

		$denombr->_makePage($denombrId, $structId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
