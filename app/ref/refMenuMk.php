<?php
require_once( '../../app/common/required.php') ;

 //R�cup�ration de la session
 $session = Session::_GetInstance() ;
 //v�rification du droit d'acc�s
 $rights = new ModuleRights() ;
 //(1=>mod_id, 1=> right_id, uid) => int->int->int=>boolean

 if($rights->_isModuleAllowed(3, 1, $session->_getUid()))
 {
    require_once( 'classes/refMenu.inc.php') ; 
    $page = new RefMenu($session, $rights) ;
    $page->_makePage($session, $rights) ;
 }else{
   // $error = new Error() ;
   // $error->_obtainMsg('ref', _ACCESS_DENIED_ ) ;
    echo('You\'re not allowed to access this menu') ;
 }
 

?>