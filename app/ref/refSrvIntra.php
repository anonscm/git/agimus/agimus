<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/application.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/srvIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/catSrv.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;

/**
 *
 * <p>Service Intra</p>
 *
 * @name SrvIntra
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class RefSrvIntra extends SrvIntra {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    
	 /*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste arborecente des srvIntras
	 */
	private $SrvIntraTreeArray ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des regroupements
	 */
	private $RegrComboArray ;

	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name SrvIntra::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'srvintralist.xtpl' ;
		$this->form_file = 'srvintraform.xtpl' ;
		$this->xtpl_tree = 'srvintratree.xtpl' ;
		$this->xtpl_path = $this->lang.'/ref/'  ;
		$this->SrvIntraTreeArray = $this->_getList() ;
		$newCatSrv = new CatSrv();
		$dummySrv=$newCatSrv->_getList() ;
		$this->RegrComboArray = $newCatSrv->_getComboList() ;

	}

	/**
	 * V�rification des variables re�ues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refSrvIntra::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray,$action='')
	{
		if($valueArray['srvintra_lib'] == '')
		{
			throw new MsgException('_ERROR_LIBELLE_MISSING_')  ;
		}

		if($valueArray['srvintra_chaine_type'] == '')
		{
			//v1.0-03.11.2010 la chaine type est utilisée au niveau application
			//throw new MsgException('_ERROR_CHAINE_TYPE_MISSING_')  ;
		}

		if($valueArray['id_regr'] == '')
		{
			throw new MsgException('_ERROR_ID_MISSING_')  ;
		}

		if($valueArray['struct_id'] == '')
		{
			throw new MsgException('_ERROR_ID_MISSING_')  ;
		}

		$maconnexion = MysqlDatabase::GetInstance() ;

		//Verification que le libelle n'est pas deja utilise

		$sql = 'SELECT * FROM t_srv_intra ';
		$sql .= 'WHERE LIB=  \''.AddSlashes($valueArray['srvintra_lib']).'\' ';
		if($valueArray['srvintra_id']!=0)
		{
			$sql .= 'AND ID != \''.$valueArray['srvintra_id'].'\' ';
		}
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res) >0)
		{
			throw new msgException('_ERROR_LIBELLE_ALREADY_EXISTS_')  ;
		}


	}

	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refSrvIntra::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
		SrvIntra::_setId($valueArray['srvintra_id']) ;
		SrvIntra::_setExtnRef($valueArray['srvintra_extn_ref']) ;
		SrvIntra::_setLib($valueArray['srvintra_lib']) ;
		SrvIntra::_setDsc($valueArray['srvintra_dsc']) ;
		SrvIntra::_setChaineType($valueArray['srvintra_chaine_type']) ;
		SrvIntra::_setIdRegr($valueArray['id_regr']) ;
		SrvIntra::_setIdStruct($valueArray['struct_id']) ;
	}

	/**
	 * Insertion des donn�es dans le mod�le de pag
	 *
	 * <p>_makePage</p>
	 *
	 * @name SrvIntra::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($srvintraId=0, $msg='')
	{
		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
			$xtpl->assign('MSG', $msg) ;
			$xtpl->parse('main.msg');
		}


		foreach($this->SrvIntraTreeArray as $key=>$value)
		{
			$xtpl->assign('SRV_INTRA_ID', $key);
			$xtpl->assign('EXTN_REF', $value['EXTN_REF']);
			$xtpl->assign('LIB', $value['LIB']);
			$xtpl->assign('DSC', $value['DSC']);
			$xtpl->assign('CHAINE_TYPE', $value['CHAINE_TYPE']);
			$xtpl->assign('REGR_LIB', $this->RegrComboArray[$value['ID_REGR']]);
			
			//Recupération des fils intra
			$arFils=Application::_getList($key);
			$nbFils=count($arFils);
			
			if(($key != $srvintraId)&&($this->rights->_isActionAllowed('ref', 35, $this->uid)))
			{
				$xtpl->parse('main.list.row.modif');
			}
	
			//v1.5-PPR-#000 suppression message notice
			if ((isset($value['tree'])) and ($value['tree'] != NULL))
			{
				$treeArray = $value['tree'] ;
				$treeString = $this->_buildTree($treeArray, $maxlevel, 1) ;
				$xtpl->assign('SRV_INTRA_TREE', $treeString );
				$xtpl->parse('main.list.row.tree');
			}else{
				if(($nbFils==0) &&($key != $srvintraId)&&($this->rights->_isActionAllowed('ref', 34, $this->uid)))
				{
					$xtpl->parse('main.list.row.delete');
				}
			}
			$xtpl->parse('main.list.row');
		}
		$xtpl->parse('main.list');
		//Construction du menu en fonction des droits de l'utilisateur

		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}
		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}

	// non utilis�e pour l'instant
	private function _buildTree($subtreeArray, $maxlevel = 0, $treelevel=0)
	{
		$levelmax = $maxlevel ;
		$treeArray = $subtreeArray ;
		$level=$treelevel+1 ;
		$treeString = '' ;
		$xtpl = new XTemplate($this->xtpl_tree, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		foreach($treeArray as $key=>$value)
		{
			foreach($value as $key2=>$val2)
			{
				$xtpl->assign('LEVEL_PERE', $level) ;
				$xtpl->parse('main.row.level');

				$xtpl->assign('LEVEL',1 ) ;
				if($maxlevel-$level>0)
				{
					$xtpl->assign('LEVEL_SON',$maxlevel-$level) ;
					$xtpl->parse('main.row.levelson');
				}
				$xtpl->assign('SRV_INTRA_LIB', $val2['libelle']);
				$xtpl->assign('SRV_INTRA_ID', $key2);
				if($this->rights->_isActionAllowed('ref', 23, $this->uid))
				{
					$xtpl->parse('main.row.modif');
				}

				if($val2['tree'] != NULL)
				{
					$subtree = $this->_buildTree($val2['tree'], $levelmax, $level) ;
					$xtpl->assign('SRV_INTRA_TREE', $subtree);
					$xtpl->parse('main.row.tree');
				}else{
					if($this->rights->_isActionAllowed('ref', 22, $this->uid))
					{
						$xtpl->parse('main.row.delete');
					}

				}
				$xtpl->parse('main.row');
			}
		}
		$xtpl->parse('main');
		$treeString = $xtpl->text('main') ;
		return $treeString ;

	}

	public function _makeForm($srvintra_id=0, $valueArray=array(), $doc_ref=0, $docVal = array())
	{
		$form = new Form('../lst_srvintra.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);
		$config=$_SESSION['config'];

		if(($valueArray == NULL)&&($srvintra_id==0))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}
		if($valueArray != NULL)
		{
			$srvintra_extn_ref = $valueArray['srvintra_extn_ref'] ;
			$srvintra_lib = $valueArray['srvintra_lib'] ;
			$srvintra_id = $valueArray['srvintra_id'] ;
			$srvintra_dsc = $valueArray['srvintra_dsc'] ;
			$srvintra_chaine_type = $valueArray['srvintra_chaine_type'];
			$id_regr = $valueArray['id_regr'];
			$struct_id = $valueArray['struct_id'];

		}else{
			if($srvintra_id!=0)
			{
				$srvintra_extn_ref =  $this->SrvIntraTreeArray[$srvintra_id]['EXTN_REF'] ;
				$srvintra_lib =  $this->SrvIntraTreeArray[$srvintra_id]['LIB'] ;
				$srvintra_dsc =  $this->SrvIntraTreeArray[$srvintra_id]['DSC'] ;
				$srvintra_chaine_type =  $this->SrvIntraTreeArray[$srvintra_id]['CHAINE_TYPE'] ;
				$id_regr =  $this->SrvIntraTreeArray[$srvintra_id]['ID_REGR'] ;
				$struct_id =  $this->SrvIntraTreeArray[$srvintra_id]['ID_STRUCT'] ;
			}else{
				$srvintra_extn_ref = '' ;
				$srvintra_lib = '' ;
				$srvintra_dsc= '' ;
				$srvintra_chaine_type = '';
				$id_regr = 0;
				$struct_id = $config["id_struct"];
			}
		}

		$xtplform->assign('STRUCT_ID', $form->_mkInput('hidden', 'struct_id', $struct_id)) ;
		$xtplform->assign('SRV_INTRA_ID', $form->_mkInput('hidden', 'srvintra_id', $srvintra_id)) ;
		$xtplform->assign('EXTN_REF', $form->_mkInput('text', 'srvintra_extn_ref', $srvintra_extn_ref)) ;
		$xtplform->assign('LIB', $form->_mkInput('text', 'srvintra_lib',$srvintra_lib)) ;
		$xtplform->assign('DSC', $form->_mkInput('text', 'srvintra_dsc',$srvintra_dsc)) ;
		$xtplform->assign('CHAINE_TYPE', $form->_mkInput('text', 'srvintra_chaine_type',$srvintra_chaine_type)) ;
		$xtplform->assign('REGR_LIB', $form->_mkSelect('id_regr', $this->RegrComboArray, $id_regr,'')) ;

		$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('submit', 'Enregistrer' ) );

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	public function _makeView($rights=NULL, $srvintra_id=0)
	{
		$form = new Form('../lst_srvintra.php') ;
		$xtplform = new XTemplate($this->view_file, $this->xtpl_path);

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name SrvIntra::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>