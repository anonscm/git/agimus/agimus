<?php
//v1.5-PPR-#016 ajout nouvelle dimension
require_once( DIR_WWW.ROOT_APPL.'/app/common/compIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/composante.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;

/**
 *
 * <p>Composante Intra</p>
 *
 * @name CompIntra
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class RefCompIntra extends CompIntra {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés 
	 /*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste arborecente des compintras
	 */
	private $CompIntraTreeArray ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des regroupements
	 */
	private $RegrComboArray ;

	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name CompIntra::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'compintralist.xtpl' ;
		$this->form_file = 'compintraform.xtpl' ;
		$this->xtpl_tree = 'compintratree.xtpl' ;
		$this->xtpl_path = $this->lang.'/ref/'  ;
		$this->CompIntraTreeArray = $this->_getList() ;

		$this->CompIntraTreeArray = array();
		$this->CompIntraTreeArray = $this->_getList() ;
		
		$newComposante = new Composante();
		$dummyComposante=$newComposante->_getList() ;
		$this->RegrComboArray = $newComposante->_getComboList() ;
	}


	/**
	 * Verification des variables recues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refCompIntra::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray,$action='')
	{
		if($valueArray['compintra_lib'] == '')
		{
			throw new MsgException('_ERROR_LIBELLE_MISSING_')  ;
		}

		if($valueArray['compintra_chaine_type'] == '')
		{
			throw new MsgException('_ERROR_CHAINE_TYPE_MISSING_')  ;
		}

		if($valueArray['id_regr'] == '')
		{
			throw new MsgException('_ERROR_ID_MISSING_')  ;
		}

		if($valueArray['struct_id'] == '')
		{
			throw new MsgException('_ERROR_ID_MISSING_')  ;
		}

		$maconnexion = MysqlDatabase::GetInstance() ;

		//Verification que le libelle n'est pas deja utilise

		$sql = 'SELECT * FROM t_comp_intra ';
		$sql .= 'WHERE LIB=  \''.AddSlashes($valueArray['compintra_lib']).'\' ';
		if($valueArray['compintra_id']!=0)
		{
			$sql .= 'AND ID != \''.$valueArray['compintra_id'].'\' ';
		}
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res) >0)
		{
			throw new msgException('_ERROR_LIBELLE_ALREADY_EXISTS_')  ;
		}


	}

	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refCompIntra::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
		CompIntra::_setId($valueArray['compintra_id']) ;
		CompIntra::_setExtnRef($valueArray['compintra_extn_ref']) ;
		CompIntra::_setLib($valueArray['compintra_lib']) ;
		CompIntra::_setDsc($valueArray['compintra_dsc']) ;
		CompIntra::_setChaineType($valueArray['compintra_chaine_type']) ;
		CompIntra::_setIdRegr($valueArray['id_regr']) ;
		CompIntra::_setIdStruct($valueArray['struct_id']) ;
	}

	/**
	 * Insertion des donn�es dans le mod�le de pag
	 *
	 * <p>_makePage</p>
	 *
	 * @name CompIntra::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($compintraId=0, $msg='')
	{
		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
			$xtpl->assign('MSG', $msg) ;
			$xtpl->parse('main.msg');
		}


		foreach($this->CompIntraTreeArray as $key=>$value)
		{
			$xtpl->assign('COMP_INTRA_ID', $key);
			$xtpl->assign('EXTN_REF', $value['EXTN_REF']);
			$xtpl->assign('LIB', $value['LIB']);
			$xtpl->assign('DSC', $value['DSC']);
			$xtpl->assign('CHAINE_TYPE', $value['CHAINE_TYPE']);
			$xtpl->assign('REGR_LIB', $this->RegrComboArray[$value['ID_REGR']]);
			if(($key != $compintraId)&&($this->rights->_isActionAllowed('ref', 35, $this->uid)))
			{
				$xtpl->parse('main.list.row.modif');
			}

			if((isset($value['tree'])) and ($value['tree'] != NULL))
			{
				$treeArray = $value['tree'] ;
				$treeString = $this->_buildTree($treeArray, $maxlevel, 1) ;
				$xtpl->assign('COMP_INTRA_TREE', $treeString );
				$xtpl->parse('main.list.row.tree');
			}else{
				//v1.2-PPR-28032011 restriction à la suppression des id > 1 (1 : autres)
				if(($key!=1) && ($key != $compintraId)&&($this->rights->_isActionAllowed('ref', 34, $this->uid)))
				{
					$xtpl->parse('main.list.row.delete');
				}
			}
			$xtpl->parse('main.list.row');
		}
		$xtpl->parse('main.list');
		//Construction du menu en fonction des droits de l'utilisateur

		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}

		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}

	// non utilisee pour l'instant
	private function _buildTree($subtreeArray, $maxlevel = 0, $treelevel=0)
	{
		$levelmax = $maxlevel ;
		$treeArray = $subtreeArray ;
		$level=$treelevel+1 ;
		$treeString = '' ;
		$xtpl = new XTemplate($this->xtpl_tree, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		foreach($treeArray as $key=>$value)
		{
			foreach($value as $key2=>$val2)
			{
				$xtpl->assign('LEVEL_PERE', $level) ;
				$xtpl->parse('main.row.level');

				$xtpl->assign('LEVEL',1 ) ;
				if($maxlevel-$level>0)
				{
					$xtpl->assign('LEVEL_SON',$maxlevel-$level) ;
					$xtpl->parse('main.row.levelson');
				}
				$xtpl->assign('COMP_INTRA_LIB', $val2['libelle']);
				$xtpl->assign('COMP_INTRA_ID', $key2);
				if($this->rights->_isActionAllowed('ref', 23, $this->uid))
				{
					$xtpl->parse('main.row.modif');
				}

				if($val2['tree'] != NULL)
				{
					$subtree = $this->_buildTree($val2['tree'], $levelmax, $level) ;
					$xtpl->assign('COMP_INTRA_TREE', $subtree);
					$xtpl->parse('main.row.tree');
				}else{
					if($this->rights->_isActionAllowed('ref', 22, $this->uid))
					{
						$xtpl->parse('main.row.delete');
					}

				}
				$xtpl->parse('main.row');
			}
		}
		$xtpl->parse('main');
		$treeString = $xtpl->text('main') ;
		return $treeString ;

	}

	public function _makeForm($compintra_id=0, $valueArray=array(), $doc_ref=0, $docVal = array())
	{
		$form = new Form('../lst_compintra.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);
		$config=$_SESSION['config'];

		if(($valueArray == NULL)&&($compintra_id==0))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}
		if($valueArray != NULL)
		{
			$compintra_extn_ref = $valueArray['compintra_extn_ref'] ;
			$compintra_lib = $valueArray['compintra_lib'] ;
			$compintra_id = $valueArray['compintra_id'] ;
			$compintra_dsc = $valueArray['compintra_dsc'] ;
			$compintra_chaine_type = $valueArray['compintra_chaine_type'];
			$id_regr = $valueArray['id_regr'];
			$struct_id = $valueArray['struct_id'];

		}else{
			if($compintra_id!=0)
			{
				$compintra_extn_ref =  $this->CompIntraTreeArray[$compintra_id]['EXTN_REF'] ;
				$compintra_lib =  $this->CompIntraTreeArray[$compintra_id]['LIB'] ;
				$compintra_dsc =  $this->CompIntraTreeArray[$compintra_id]['DSC'] ;
				$compintra_chaine_type =  $this->CompIntraTreeArray[$compintra_id]['CHAINE_TYPE'] ;
				$id_regr =  $this->CompIntraTreeArray[$compintra_id]['ID_REGR'] ;
				$struct_id =  $this->CompIntraTreeArray[$compintra_id]['ID_STRUCT'] ;
			}else{
				$compintra_extn_ref = '' ;
				$compintra_lib = '' ;
				$compintra_dsc= '' ;
				$compintra_chaine_type = '';
				$id_regr = 0;
				$struct_id = $config["id_struct"];
			}
		}

		$xtplform->assign('STRUCT_ID', $form->_mkInput('hidden', 'struct_id', $struct_id)) ;
		$xtplform->assign('COMP_INTRA_ID', $form->_mkInput('hidden', 'compintra_id', $compintra_id)) ;
		$xtplform->assign('EXTN_REF', $form->_mkInput('text', 'compintra_extn_ref', $compintra_extn_ref)) ;
		$xtplform->assign('LIB', $form->_mkInput('text', 'compintra_lib',$compintra_lib)) ;
		$xtplform->assign('DSC', $form->_mkInput('text', 'compintra_dsc',$compintra_dsc)) ;
		$xtplform->assign('CHAINE_TYPE', $form->_mkInput('text', 'compintra_chaine_type',$compintra_chaine_type)) ;
		$xtplform->assign('REGR_LIB', $form->_mkSelect('id_regr', $this->RegrComboArray, $id_regr,'')) ;

		$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('submit', 'Enregistrer' ) );

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	public function _makeView($rights=NULL, $compintra_id=0)
	{
		$form = new Form('../lst_compintra.php') ;
		$xtplform = new XTemplate($this->view_file, $this->xtpl_path);

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name CompIntra::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>
