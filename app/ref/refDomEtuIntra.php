<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/domEtuIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/domEtu.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;

/**
 *
 * <p>Domaine d'Etude Intra</p>
 *
 * @name DomEtuIntra
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class RefDomEtuIntra extends DomEtuIntra {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés 
	 /*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste arborecente des dometuintras
	 */
	private $DomEtuIntraTreeArray ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des regroupements
	 */
	private $RegrComboArray ;

	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name DomEtuIntra::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'dometuintralist.xtpl' ;
		$this->form_file = 'dometuintraform.xtpl' ;
		$this->xtpl_tree = 'dometuintratree.xtpl' ;
		$this->xtpl_path = $this->lang.'/ref/'  ;
		$this->DomEtuIntraTreeArray = $this->_getList() ;
		$newDomEtu = new DomEtu();
		$dummyDomEtu=$newDomEtu->_getList() ;
		$this->RegrComboArray = $newDomEtu->_getComboList() ;
	}

	/**
	 * V�rification des variables re�ues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refDomEtuIntra::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray,$action='')
	{
		if($valueArray['dometuintra_lib'] == '')
		{
			throw new MsgException('_ERROR_LIBELLE_MISSING_')  ;
		}

		if($valueArray['dometuintra_chaine_type'] == '')
		{
			throw new MsgException('_ERROR_CHAINE_TYPE_MISSING_')  ;
		}

		if($valueArray['id_regr'] == '')
		{
			throw new MsgException('_ERROR_ID_MISSING_')  ;
		}

		if($valueArray['struct_id'] == '')
		{
			throw new MsgException('_ERROR_ID_MISSING_')  ;
		}

		$maconnexion = MysqlDatabase::GetInstance() ;

		//V�rification que le libell� n'est pas d�j� utilis�

		$sql = 'SELECT * FROM t_dometu_intra ';
		$sql .= 'WHERE LIB=  \''.AddSlashes($valueArray['dometuintra_lib']).'\' ';
		if($valueArray['dometuintra_id']!=0)
		{
			$sql .= 'AND ID != \''.$valueArray['dometuintra_id'].'\' ';
		}
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res) >0)
		{
			throw new msgException('_ERROR_LIBELLE_ALREADY_EXISTS_')  ;
		}


	}

	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refDomEtuIntra::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
		DomEtuIntra::_setId($valueArray['dometuintra_id']) ;
		DomEtuIntra::_setExtnRef($valueArray['dometuintra_extn_ref']) ;
		DomEtuIntra::_setLib($valueArray['dometuintra_lib']) ;
		DomEtuIntra::_setDsc($valueArray['dometuintra_dsc']) ;
		DomEtuIntra::_setChaineType($valueArray['dometuintra_chaine_type']) ;
		DomEtuIntra::_setIdRegr($valueArray['id_regr']) ;
		DomEtuIntra::_setIdStruct($valueArray['struct_id']) ;
	}

	/**
	 * Insertion des donn�es dans le mod�le de pag
	 *
	 * <p>_makePage</p>
	 *
	 * @name DomEtuIntra::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($dometuintraId=0, $msg='')
	{
		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
			$xtpl->assign('MSG', $msg) ;
			$xtpl->parse('main.msg');
		}


		foreach($this->DomEtuIntraTreeArray as $key=>$value)
		{
			$xtpl->assign('DOMETU_INTRA_ID', $key);
			$xtpl->assign('EXTN_REF', $value['EXTN_REF']);
			$xtpl->assign('LIB', $value['LIB']);
			$xtpl->assign('DSC', $value['DSC']);
			$xtpl->assign('CHAINE_TYPE', $value['CHAINE_TYPE']);
			$xtpl->assign('REGR_LIB', $this->RegrComboArray[$value['ID_REGR']]);
			if(($key != $dometuintraId)&&($this->rights->_isActionAllowed('ref', 35, $this->uid)))
			{
				$xtpl->parse('main.list.row.modif');
			}

			if($value['tree'] != NULL)
			{
				$treeArray = $value['tree'] ;
				$treeString = $this->_buildTree($treeArray, $maxlevel, 1) ;
				$xtpl->assign('DOMETU_INTRA_TREE', $treeString );
				$xtpl->parse('main.list.row.tree');
			}else{
				//v1.2-PPR-28032011 restriction à la suppression des id > 1 (1 : autres)
				if(($key!=1) && ($key != $dometuintraId)&&($this->rights->_isActionAllowed('ref', 34, $this->uid)))
				{
					$xtpl->parse('main.list.row.delete');
				}
			}
			$xtpl->parse('main.list.row');
		}
		$xtpl->parse('main.list');

		// TODO : reprise de données Apogee (sur base diplome LDAP Pau uppacodeetape)
		IF (REPRISE_DONNEES_APOGEE)
		{
			foreach($this->DomEtuIntraTreeArray as $domIntraId=>$curDom)
			{
				if (substr($curDom["LIB"],0,1)=="~") // on ne traite que les nouveaux detectés
				{
					$arLib=$curDom["DSC"];
					$libDom="";
					$idRegr=0;

					if (strlen($arLib)==6) {
						$domaineIntraLib="Domaine ".$arLib[3].$arLib[4];
					}
					else
					{
						$domaineIntraLib="Domaine ".$arLib;
					}
					// mise à jour finale
					$newDomIntra=new DomEtuIntra($domIntraId);
					$newDomIntra->_setLib($domaineIntraLib);
					$newDomIntra->_setIdRegr($idRegr);
					$newDomIntra->_update();
				}
			}
		}

		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}
		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}

	// non utilis�e pour l'instant
	private function _buildTree($subtreeArray, $maxlevel = 0, $treelevel=0)
	{
		$levelmax = $maxlevel ;
		$treeArray = $subtreeArray ;
		$level=$treelevel+1 ;
		$treeString = '' ;
		$xtpl = new XTemplate($this->xtpl_tree, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		foreach($treeArray as $key=>$value)
		{
			foreach($value as $key2=>$val2)
			{
				$xtpl->assign('LEVEL_PERE', $level) ;
				$xtpl->parse('main.row.level');

				$xtpl->assign('LEVEL',1 ) ;
				if($maxlevel-$level>0)
				{
					$xtpl->assign('LEVEL_SON',$maxlevel-$level) ;
					$xtpl->parse('main.row.levelson');
				}
				$xtpl->assign('DOMETU_INTRA_LIB', $val2['libelle']);
				$xtpl->assign('DOMETU_INTRA_ID', $key2);
				if($this->rights->_isActionAllowed('ref', 23, $this->uid))
				{
					$xtpl->parse('main.row.modif');
				}

				if($val2['tree'] != NULL)
				{
					$subtree = $this->_buildTree($val2['tree'], $levelmax, $level) ;
					$xtpl->assign('DOMETU_INTRA_TREE', $subtree);
					$xtpl->parse('main.row.tree');
				}else{
					if($this->rights->_isActionAllowed('ref', 22, $this->uid))
					{
						$xtpl->parse('main.row.delete');
					}

				}
				$xtpl->parse('main.row');
			}
		}
		$xtpl->parse('main');
		$treeString = $xtpl->text('main') ;
		return $treeString ;

	}

	public function _makeForm($dometuintra_id=0, $valueArray=array(), $doc_ref=0, $docVal = array())
	{
		$form = new Form('../lst_dometuintra.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);
		$config=$_SESSION['config'];

		if(($valueArray == NULL)&&($dometuintra_id==0))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}
		if($valueArray != NULL)
		{
			$dometuintra_extn_ref = $valueArray['dometuintra_extn_ref'] ;
			$dometuintra_lib = $valueArray['dometuintra_lib'] ;
			$dometuintra_id = $valueArray['dometuintra_id'] ;
			$dometuintra_dsc = $valueArray['dometuintra_dsc'] ;
			$dometuintra_chaine_type = $valueArray['dometuintra_chaine_type'];
			$id_regr = $valueArray['id_regr'];
			$struct_id = $valueArray['struct_id'];

		}else{
			if($dometuintra_id!=0)
			{
				$dometuintra_extn_ref =  $this->DomEtuIntraTreeArray[$dometuintra_id]['EXTN_REF'] ;
				$dometuintra_lib =  $this->DomEtuIntraTreeArray[$dometuintra_id]['LIB'] ;
				$dometuintra_dsc =  $this->DomEtuIntraTreeArray[$dometuintra_id]['DSC'] ;
				$dometuintra_chaine_type =  $this->DomEtuIntraTreeArray[$dometuintra_id]['CHAINE_TYPE'] ;
				$id_regr =  $this->DomEtuIntraTreeArray[$dometuintra_id]['ID_REGR'] ;
				$struct_id =  $this->DomEtuIntraTreeArray[$dometuintra_id]['ID_STRUCT'] ;
			}else{
				$dometuintra_extn_ref = '' ;
				$dometuintra_lib = '' ;
				$dometuintra_dsc= '' ;
				$dometuintra_chaine_type = '';
				$id_regr = 0;
				$struct_id = $config["id_struct"];
			}
		}

		$xtplform->assign('STRUCT_ID', $form->_mkInput('hidden', 'struct_id', $struct_id)) ;
		$xtplform->assign('DOMETU_INTRA_ID', $form->_mkInput('hidden', 'dometuintra_id', $dometuintra_id)) ;
		$xtplform->assign('EXTN_REF', $form->_mkInput('text', 'dometuintra_extn_ref', $dometuintra_extn_ref)) ;
		$xtplform->assign('LIB', $form->_mkInput('text', 'dometuintra_lib',$dometuintra_lib)) ;
		$xtplform->assign('DSC', $form->_mkInput('text', 'dometuintra_dsc',$dometuintra_dsc)) ;
		$xtplform->assign('CHAINE_TYPE', $form->_mkInput('text', 'dometuintra_chaine_type',$dometuintra_chaine_type)) ;
		$xtplform->assign('REGR_LIB', $form->_mkSelect('id_regr', $this->RegrComboArray, $id_regr,'')) ;

		$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('submit', 'Enregistrer' ) );

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	public function _makeView($rights=NULL, $dometuintra_id=0)
	{
		$form = new Form('../lst_dometuintra.php') ;
		$xtplform = new XTemplate($this->view_file, $this->xtpl_path);

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name DomEtuIntra::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>