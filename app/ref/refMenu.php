<?php
require_once( '../../app/common/required.php') ;
require_once( 'refSession.php') ;

//Recuperation de la session
$session = RefSession::_GetInstance() ;
//verification du droit d'acces
$rights = new ModuleRights() ;
//(1=>mod_id, 1=> right_id, uid) => int->int->int=>boolean

if($rights->_isModuleAllowed(3, 1, $session->_getUid()))
{

	header('Location: '.ROOT_APPL.'/app/ref/lst_indic.php');

	//require_once( 'refMenu.inc.php') ;
	//$session->_setModid(2);
	//$page = new RefMenu($session, $rights) ;
	//$page->_makePage($session, $rights) ;


	$session->_sessionLogAction(1, '_ACCESS_') ;

}else{
	echo('You\'re not allowed to access this menu') ;
}


?>