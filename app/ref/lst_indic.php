<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//Recuperation de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

//v1.3-PPR-24052011 correctif notice
$docref=0;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 28, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		
		if(isset($_GET['indic_id'])){
			$indicId = $_GET['indic_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['indic_id'])){
			$indicId = $_POST['indic_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}elseif(isset($_POST['xml_indic_id'])){
			$indicId = $_POST['xml_indic_id'] ;
			$valueArray = $_POST ;
			$action = 'import' ;
		}else{
			$indicId = 0 ;
			$action = 'create' ;
		}



		require_once( 'refIndicateur.php') ;
		$indic = new RefIndicateur($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 30, $uid, '_INDIC_DELETE_NOT_ALLOWED_'))
				{
					$indic->_delete($indicId) ;
					$session->_sessionLogAction(1, '_INDIC_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_indic.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 31, $uid, '_INDIC_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donn�es dans le formulaire
					$indic->_makeForm($indicId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($indicId == 0)&&($rights->_isActionAllowed('ref', 29, $uid, '_INDIC_CREATE_NOT_ALLOWED_')))
				{
					$indic->_checkFormValues($_POST) ;
					$indic->_setFormValues($_POST) ;
					$indic->_create() ;
					$session->_sessionLogAction(1, '_INDIC_CREATE_') ;
				}
				if(($indicId != 0)&&($rights->_isActionAllowed('ref', 31, $uid, '_INDIC_UPDATE_NOT_ALLOWED_')))
				{
					$indic->_checkFormValues($_POST) ;
					$indic->_setFormValues($_POST) ;
					$indic->_update() ;
					$session->_sessionLogAction(1, '_INDIC_UPDATE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_indic.php?indic_id='.$indicId);
					exit() ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_indic.php');
				exit() ;

			}elseif($action == 'import'){
				if(($indicId == 0)&&($rights->_isActionAllowed('ref', 76, $uid, '_INDIC_IMPORT_NOT_ALLOWED_')))
				{
					$indic->_checkXmlFormValues($_FILES['userfile']) ;
					$indic->_import() ;
					$session->_sessionLogAction(1, '_INDIC_IMPORT_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_indic.php');
				exit() ;
			}else{
				if($rights->_isActionAllowed('ref', 29, $uid))
				{
					$indic->_makeForm() ;
				}
				if($rights->_isActionAllowed('ref', 76, $uid))
				{
					$indic->_makeFormXml($valueArray) ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$indicId=0 ;
				if($rights->_isActionAllowed('ref', 30, $uid))
				{
					$indic->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$indic->_makeForm($indicId, $valueArray, $docref, $docVal) ;
				$indic->_makeFormXml($valueArray) ;
			}
		}

		$indic->_makePage($indicId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
