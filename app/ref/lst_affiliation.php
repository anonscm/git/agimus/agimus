<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//R�cup�ration de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 68, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['affiliation_id'])){
			$affiliationId = $_GET['affiliation_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['affiliation_id'])){
			$affiliationId = $_POST['affiliation_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$affiliationId = 0 ;
			$action = 'create' ;
		}


		require_once( 'refAffiliation.php') ;
		$affiliation = new RefAffiliation($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 70, $uid, '_AFFILIATION_DELETE_NOT_ALLOWED_'))
				{
					$affiliation->_delete($affiliationId) ;
					$session->_sessionLogAction(1, '_AFFILIATION_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_affiliation.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 71, $uid, '_AFFILIATION_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donn�es dans le formulaire
					$affiliation->_makeForm($affiliationId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($affiliationId == '')&&($rights->_isActionAllowed('ref', 69, $uid, '_AFFILIATION_CREATE_NOT_ALLOWED_')))
				{
					$affiliation->_checkFormValues($_POST,$action) ;
					$affiliation->_setFormValues($_POST) ;
					$affiliation->_create() ;
					$session->_sessionLogAction(1, '_AFFILIATION_CREATE_') ;
				}
				if(($affiliationId != '')&&($rights->_isActionAllowed('ref', 71, $uid, '_AFFILIATION_UPDATE_NOT_ALLOWED_')))
				{
					$affiliation->_checkFormValues($_POST,$action) ;
					$affiliation->_setFormValues($_POST) ;
					$affiliation->_update() ;
					$session->_sessionLogAction(1, '_AFFILIATION_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_affiliation.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 69, $uid))
				{
					$affiliation->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$affiliationId='' ;
				if($rights->_isActionAllowed('ref', 70, $uid))
				{
					$affiliation->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$affiliation->_makeForm($affiliationId, $valueArray, $docref, $docVal) ;
			}
		}

		$affiliation->_makePage($affiliationId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
