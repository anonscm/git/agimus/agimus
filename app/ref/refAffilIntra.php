<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/affilIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/affiliation.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;

/**
 *
 * <p>Affiliation Intra</p>
 *
 * @name AffilIntra
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class RefAffilIntra extends AffilIntra {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    
	 /*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste arborecente des affilintras
	 */
	private $AffilIntraTreeArray ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des regroupements
	 */
	private $RegrComboArray ;

	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name AffilIntra::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'affilintralist.xtpl' ;
		$this->form_file = 'affilintraform.xtpl' ;
		$this->xtpl_tree = 'affilintratree.xtpl' ;
		$this->xtpl_path = $this->lang.'/ref/'  ;
		$this->AffilIntraTreeArray = $this->_getList() ;
		$newAffiliation = new Affiliation();
		$dummyAffil=$newAffiliation->_getList() ;
		$this->RegrComboArray = $newAffiliation->_getComboList() ;
	}

	/**
	 * V�rification des variables re�ues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refAffilIntra::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray,$action='')
	{
		if($valueArray['affilintra_lib'] == '')
		{
			throw new MsgException('_ERROR_LIBELLE_MISSING_')  ;
		}

		if($valueArray['affilintra_chaine_type'] == '')
		{
			throw new MsgException('_ERROR_CHAINE_TYPE_MISSING_')  ;
		}

		if($valueArray['id_regr'] == '')
		{
			throw new MsgException('_ERROR_ID_MISSING_')  ;
		}

		if($valueArray['struct_id'] == '')
		{
			throw new MsgException('_ERROR_ID_MISSING_')  ;
		}

		$maconnexion = MysqlDatabase::GetInstance() ;

		//V�rification que le libell� n'est pas d�j� utilis�

		$sql = 'SELECT * FROM t_affil_intra ';
		$sql .= 'WHERE LIB=  \''.AddSlashes($valueArray['affilintra_lib']).'\' ';
		if($valueArray['affilintra_id']!=0)
		{
			$sql .= 'AND ID != \''.$valueArray['affilintra_id'].'\' ';
		}
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res) >0)
		{
			throw new msgException('_ERROR_LIBELLE_ALREADY_EXISTS_')  ;
		}


	}

	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refAffilIntra::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
		AffilIntra::_setId($valueArray['affilintra_id']) ;
		AffilIntra::_setExtnRef($valueArray['affilintra_extn_ref']) ;
		AffilIntra::_setLib($valueArray['affilintra_lib']) ;
		AffilIntra::_setDsc($valueArray['affilintra_dsc']) ;
		AffilIntra::_setChaineType($valueArray['affilintra_chaine_type']) ;
		AffilIntra::_setIdRegr($valueArray['id_regr']) ;
		AffilIntra::_setIdStruct($valueArray['struct_id']) ;
	}

	/**
	 * Insertion des donn�es dans le mod�le de pag
	 *
	 * <p>_makePage</p>
	 *
	 * @name AffilIntra::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($affilintraId=0, $msg='')
	{
		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
			$xtpl->assign('MSG', $msg) ;
			$xtpl->parse('main.msg');
		}


		foreach($this->AffilIntraTreeArray as $key=>$value)
		{
			$xtpl->assign('AFFIL_INTRA_ID', $key);
			$xtpl->assign('EXTN_REF', $value['EXTN_REF']);
			$xtpl->assign('LIB', $value['LIB']);
			$xtpl->assign('DSC', $value['DSC']);
			$xtpl->assign('CHAINE_TYPE', $value['CHAINE_TYPE']);
			$xtpl->assign('REGR_LIB', $this->RegrComboArray[$value['ID_REGR']]);
			if(($key != $affilintraId)&&($this->rights->_isActionAllowed('ref', 75, $this->uid)))
			{
				$xtpl->parse('main.list.row.modif');
			}

			//V1.5-PPR-#000 suppression message notice
			if ((isset($value['tree'])) and ($value['tree'] != NULL))
			{
				$treeArray = $value['tree'] ;
				$treeString = $this->_buildTree($treeArray, $maxlevel, 1) ;
				$xtpl->assign('_TREE', $treeString );
				$xtpl->parse('main.list.row.tree');
			}else{
				//v1.2-PPR-28032011 restriction à la suppression des id > 1 (1 : autres)
				if(($key!=1) && ($key != $affilintraId)&&($this->rights->_isActionAllowed('ref', 74, $this->uid)))
				{
					$xtpl->parse('main.list.row.delete');
				}
			}
			$xtpl->parse('main.list.row');
		}
		$xtpl->parse('main.list');
		//Construction du menu en fonction des droits de l'utilisateur

		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}
		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}

	// non utilis�e pour l'instant
	private function _buildTree($subtreeArray, $maxlevel = 0, $treelevel=0)
	{
		$levelmax = $maxlevel ;
		$treeArray = $subtreeArray ;
		$level=$treelevel+1 ;
		$treeString = '' ;
		$xtpl = new XTemplate($this->xtpl_tree, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		foreach($treeArray as $key=>$value)
		{
			foreach($value as $key2=>$val2)
			{
				$xtpl->assign('LEVEL_PERE', $level) ;
				$xtpl->parse('main.row.level');

				$xtpl->assign('LEVEL',1 ) ;
				if($maxlevel-$level>0)
				{
					$xtpl->assign('LEVEL_SON',$maxlevel-$level) ;
					$xtpl->parse('main.row.levelson');
				}
				$xtpl->assign('_LIB', $val2['libelle']);
				$xtpl->assign('AFFIL_INTRA_ID', $key2);
				if($this->rights->_isActionAllowed('ref', 75, $this->uid))
				{
					$xtpl->parse('main.row.modif');
				}

				if($val2['tree'] != NULL)
				{
					$subtree = $this->_buildTree($val2['tree'], $levelmax, $level) ;
					$xtpl->assign('_TREE', $subtree);
					$xtpl->parse('main.row.tree');
				}else{
					if($this->rights->_isActionAllowed('ref', 74, $this->uid))
					{
						$xtpl->parse('main.row.delete');
					}

				}
				$xtpl->parse('main.row');
			}
		}
		$xtpl->parse('main');
		$treeString = $xtpl->text('main') ;
		return $treeString ;

	}

	public function _makeForm($affilintra_id=0, $valueArray=array(), $doc_ref=0, $docVal = array())
	{
		$form = new Form('../lst_affilintra.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);
		$config=$_SESSION['config'];

		if(($valueArray == NULL)&&($affilintra_id==0))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}
		if($valueArray != NULL)
		{
			$affilintra_extn_ref = $valueArray['affilintra_extn_ref'] ;
			$affilintra_lib = $valueArray['affilintra_lib'] ;
			$affilintra_id = $valueArray['affilintra_id'] ;
			$affilintra_dsc = $valueArray['affilintra_dsc'] ;
			$affilintra_chaine_type = $valueArray['affilintra_chaine_type'];
			$id_regr = $valueArray['id_regr'];
			$struct_id = $valueArray['struct_id'];

		}else{
			if($affilintra_id!=0)
			{
				$affilintra_extn_ref =  $this->AffilIntraTreeArray[$affilintra_id]['EXTN_REF'] ;
				$affilintra_lib =  $this->AffilIntraTreeArray[$affilintra_id]['LIB'] ;
				$affilintra_dsc =  $this->AffilIntraTreeArray[$affilintra_id]['DSC'] ;
				$affilintra_chaine_type =  $this->AffilIntraTreeArray[$affilintra_id]['CHAINE_TYPE'] ;
				$id_regr =  $this->AffilIntraTreeArray[$affilintra_id]['ID_REGR'] ;
				$struct_id =  $this->AffilIntraTreeArray[$affilintra_id]['ID_STRUCT'] ;
			}else{
				$affilintra_extn_ref = '' ;
				$affilintra_lib = '' ;
				$affilintra_dsc= '' ;
				$affilintra_chaine_type = '';
				$id_regr = 0;
				$struct_id = $config["id_struct"];
			}
		}

		$xtplform->assign('STRUCT_ID', $form->_mkInput('hidden', 'struct_id', $struct_id)) ;
		$xtplform->assign('AFFIL_INTRA_ID', $form->_mkInput('hidden', 'affilintra_id', $affilintra_id)) ;
		$xtplform->assign('EXTN_REF', $form->_mkInput('text', 'affilintra_extn_ref', $affilintra_extn_ref)) ;
		$xtplform->assign('LIB', $form->_mkInput('text', 'affilintra_lib',$affilintra_lib)) ;
		$xtplform->assign('DSC', $form->_mkInput('text', 'affilintra_dsc',$affilintra_dsc)) ;
		$xtplform->assign('CHAINE_TYPE', $form->_mkInput('text', 'affilintra_chaine_type',$affilintra_chaine_type)) ;
		$xtplform->assign('REGR_LIB', $form->_mkSelect('id_regr', $this->RegrComboArray, $id_regr,'')) ;

		$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('submit', 'Enregistrer' ) );

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	public function _makeView($rights=NULL, $affilintra_id=0)
	{
		$form = new Form('../lst_affilintra.php') ;
		$xtplform = new XTemplate($this->view_file, $this->xtpl_path);

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name AffilIntra::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>