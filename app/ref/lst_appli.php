<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//R�cup�ration de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 32, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['appli_id'])){
			$appliId = $_GET['appli_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['appli_id'])){
			$appliId = $_POST['appli_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$appliId = 0 ;
			$action = 'create' ;
		}

		//v1.2-PPR-28032011 ajout regroupement sur applications
		$regroupeAppli=false;
		if (isset($_POST['indic_regr'])) {
			if (strstr(strtoupper($_POST['indic_regr']),"ON"))
				$regroupeAppli=true;
		}

		require_once( 'refApplication.php') ;
		$appli = new RefApplication($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 34, $uid, '_APPLI_DELETE_NOT_ALLOWED_'))
				{
					$appli->_delete($appliId) ;
					$session->_sessionLogAction(1, '_APPLI_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_appli.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 35, $uid, '_APPLI_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donn�es dans le formulaire
					$appli->_makeForm($appliId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($appliId == '')&&($rights->_isActionAllowed('ref', 33, $uid, '_APPLI_CREATE_NOT_ALLOWED_')))
				{
					$appli->_checkFormValues($_POST,$action) ;
					$appli->_setFormValues($_POST) ;
					$appli->_create() ;
					$session->_sessionLogAction(1, '_APPLI_CREATE_') ;
				}
				if(($appliId != '')&&($rights->_isActionAllowed('ref', 35, $uid, '_APPLI_UPDATE_NOT_ALLOWED_')))
				{
					$appli->_checkFormValues($_POST,$action) ;
					$appli->_setFormValues($_POST) ;
					$appli->_update() ;
					//v1.2-PPR-28032011 ajout regroupement automatique sur expression régulière
					if ($regroupeAppli)
						$appli->_regroupe();
					$session->_sessionLogAction(1, '_APPLI_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_appli.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 33, $uid))
				{
					$appli->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$appliId='' ;
				if($rights->_isActionAllowed('ref', 34, $uid))
				{
					$appli->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$appli->_makeForm($appliId, $valueArray, $docref, $docVal) ;
			}
		}

		$appli->_makePage($appliId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
