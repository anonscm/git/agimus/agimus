<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/structure.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/denombrement.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/dimension.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/affiliation.php');
require_once( DIR_WWW.ROOT_APPL.'/app/common/affilIntra.php');
require_once( DIR_WWW.ROOT_APPL.'/app/common/discipline.php');
require_once( DIR_WWW.ROOT_APPL.'/app/common/dcplIntra.php');
require_once( DIR_WWW.ROOT_APPL.'/app/common/diplome.php');
require_once( DIR_WWW.ROOT_APPL.'/app/common/diplomeIntra.php');
require_once( DIR_WWW.ROOT_APPL.'/app/common/domEtu.php');
require_once( DIR_WWW.ROOT_APPL.'/app/common/domEtuIntra.php');
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/hdate.php') ;

/**
 *
 * <p>D�nombrement</p>
 *
 * @name D�nombrement
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class RefDenombrement extends Denombrement {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés 
	 /*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des denombrements
	 */
	private $DenombrementArray ;

	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des structures
	 */
	private $StructComboArray ;

	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des dimensions
	 */
	private $DimensionComboArray ;

	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Denombrement::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'denombrlist.xtpl' ;
		$this->form_file = 'denombrform.xtpl' ;
		$this->xtpl_path = $this->lang.'/ref/'  ;
		$this->DenombrementArray = $this->_getList() ;

		$newStruct=new Structure();
		$arStruct = $newStruct->_getList() ;
		$this->StructComboArray = $newStruct->_getComboList() ;

		$this->DimensionComboArray = Dimension::_getComboList() ;
	}


	/**
	 * Création automatique des dénombrements
	 *
	 * <p>_autoGenerate</p>
	 *
	 * @name refDenombrement::_autoGenerate()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _autoGenerate()
	{
		$config=$_SESSION['config'];
		$date=new HDate();
		$date_occ=$date->_dateStampLgFormat(time(),"en");

		$arDims = Dimension::_getComboList() ;
		foreach($arDims as $refDim=>$libDim)
		{
			$newDen=new Denombrement();
			$newDen->_setRefDim($refDim);
			$arDimValues=Dimension::_getDimensionValues($refDim);
			if (count($arDimValues)>0)
			{
				foreach($arDimValues as $idDim=>$libValueDim)
				{
					$newDen->_setIdDim($idDim);
					$newDen->_setIdStruct($config['id_struct']);
					if (!$newDen->_existsRef())
					{
						$newDen->_setNbrOcc(1);
						$newDen->_setDateOcc($date_occ);
						$newDen->_create();
					}
				}
			}

		}

	}

	/**
	 * V�rification des variables re�ues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refDenombrement::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray)
	{
		$date=new HDate();

		// contr�le dimension
		$error=false;
		if(!$valueArray['ref_dim']>0)
		{
			$error=true;
		}
		// contr�le nombre d'occurences
		if(!$valueArray['nbr_occ']>0)
		{
			$error=true;
		}
		// contr�le structure
		if(!$valueArray['id_struct']>0)
		{
			$error=true;
		}
		if(!$date->_verifDateStr($valueArray['date_occ']))
		{
			throw new msgException('_ERROR_DATE_EXISTS_')  ;
		}

		// Vérification si la combinaison existe déjà (en maj)

		if ($valueArray['denombr_id']==0)
		{
			$ctrDen=new Denombrement();
			$ctrDen->_setRefDim($valueArray['ref_dim']);
			$ctrDen->_setIdDim($valueArray['id_dim']);
			$ctrDen->_setIdStruct($valueArray['id_struct']);
			if ($ctrDen->_existsRef())
			{
				throw new msgException('_ERROR_COMBINATION_ALREADY_EXISTS_')  ;
			}
		}

		if(($valueArray['ref_dim']>0) and ($valueArray['id_dim']>0))
		{
			if ($error)
			throw new MsgException('_ERROR_VALUE_MISSING_')  ;
			return true;
		}
		else
		return false;
	}

	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refDenombrement::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
		$date=new HDate();
		Denombrement::_setId($valueArray['denombr_id']) ;
		Denombrement::_setRefDim($valueArray['ref_dim']) ;
		Denombrement::_setIdDim($valueArray['id_dim']) ;
		Denombrement::_setNbrOcc($valueArray['nbr_occ']) ;
		Denombrement::_setDateOcc($date->_dateToMySQLFormat(trim($valueArray['date_occ']))) ;
		Denombrement::_setIdStruct($valueArray['id_struct']) ;
	}

	/**
	 * Insertion des donn�es dans le mod�le de pag
	 *
	 * <p>_makePage</p>
	 *
	 * @name Denombrement::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($denombrId=0, $structId='', $msg='')
	{
		global $_matchLdapAttributes;

		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
	 	$xtpl->assign('MSG', $msg) ;
	 	$xtpl->parse('main.msg');
		}

		// formattage des dates
		$date=new HDate();

		foreach($this->DenombrementArray as $struct=>$arValue)
		{
			foreach($arValue as $key=>$value) {
				// recherche des donnees de la dimension
				$newDim=new Dimension($value['REF_DIM']);
				$objDim=$newDim->_getRefDim();
				eval('$newDimRech=new '.$objDim.'('.$value['ID_DIM'].');');
				if (!isset($newDimRech))
				{
					throw new MsgException('_UNKNOWN_DIMENSION_')  ;
				}

				//recherche libelle de la structure
				$newStruct=new Structure($struct);

				$xtpl->assign('DENOMBR_ID', $key);
				$xtpl->assign('LIB_REF_DIM', $newDim->_getLib());
				$xtpl->assign('LIB_ID_DIM', $newDimRech->_getLib());
				$xtpl->assign('NBR_OCC', $value['NBR_OCC']);
				$xtpl->assign('DATE_OCC', $date->_dateStampLgFormat($value['DATE_OCC'], $this->lang));
				$xtpl->assign('ID_STRUCT', $newStruct->_getId());
				$xtpl->assign('LIB_STRUCT', $newStruct->_getLib());

				if((($key != $denombrId)or($struct != $structId))&&($this->rights->_isActionAllowed('ref', 27, $this->uid)))
				{
					$xtpl->parse('main.list.row.modif');
				}

				if((($key != $denombrId)or($struct != $structId))&&($this->rights->_isActionAllowed('ref', 26, $this->uid)))
				{
					$xtpl->parse('main.list.row.delete');
				}
				$xtpl->parse('main.list.row');
			}
		}
		$xtpl->parse('main.list');
		//Condenombrion du menu en fonction des droits de l'utilisateur

		//TODO : dénombrements ==========================================================

		$getLdapDenombr=false;
		if ($getLdapDenombr)
		{
			// tentative de connexion au LDAP

			require_once (DIR_WWW.ROOT_APPL.'/app/common/ldap.php') ;
			$config=$_SESSION['config'];
			$mess="";
			$ldap=new Ldap();
			$ldap->_setUname($config['ldap_uname']);
			$ldap->_setPwd($config['ldap_pwd']);
			$ldap->_setLdapURL($config['ldap_url']);
			$ldap->_setBaseDN($config['ldap_base_dn']);

			if (!$ldap->_connect())
			{$mess.="<br>Impossible de se connecter au serveur LDAP !";}

			// Recherche du uid dans l'annuaire

			$uidToFind="uid=*";
			$ldap->_setBaseDN($config['ldap_search_base_dn']);
			$ldap->_setFilter($uidToFind);
			$ldap->_setAttributeArray(array_values($_matchLdapAttributes));
			$arResult=$ldap->_search();
			$mess.="<br> Nombre d'entrées : ".ldap_count_entries($ldap->_getLdapCnx(),$ldap->_getLdapSearchId());

			$mess.=aff_pr($arResult,1);
			foreach($arResult as $entryId=>$curEntry)
			{
				if ($entryId!="count")
				{
					$arAttrib[$_matchLdapAttributes['affiliation']]=$curEntry[$_matchLdapAttributes['affiliation']][0];
					$arAttrib[$_matchLdapAttributes['discipline']]=$curEntry[$_matchLdapAttributes['discipline']][0];
					$arAttrib[$_matchLdapAttributes['diplome']]=$curEntry[$_matchLdapAttributes['diplome']][0];
					$arAttrib[$_matchLdapAttributes['dometu']]=$curEntry[$_matchLdapAttributes['dometu']][0];
					$arLdap[$curEntry['uid'][0]]=$arAttrib;
				}
			}
			$mess.=aff_pr($arLdap,1);
			//echo $mess;
		}

		// TODO : dénombrements ================================================


		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}
		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}


	/**
	 * G�n�ration du formulaire de saisie
	 *
	 * <p>_makeForm</p>
	 *
	 * @name Denombrement::_makeForm()
	 * @param $denombr_id (int)
	 * @param $valueArray (array)
	 * @param $doc_ref (Int)
	 * @param $docVal (array)
	 * @return string
	 */
	public function _makeForm($denombr_id=0,  $structId='', $valueArray=array(), $doc_ref=0, $docVal = array())
	{

		$config=$_SESSION['config'];

		$form = new Form('../lst_denombr.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);

		if(($valueArray == NULL)&&($denombr_id==0))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}

		// recherche des donn�es de la dimension
		$arDimValComboList=array();

		if ($denombr_id!=0)
		{
			if ($this->DenombrementArray[$structId][$denombr_id]['REF_DIM']!="")
			$arDimValComboList=Dimension::_getDimensionValues($this->DenombrementArray[$structId][$denombr_id]['REF_DIM']);
		}

		if($valueArray != NULL)
		{
			if (($valueArray['REF_DIM']>0)
			and (!$valueArray['ID_DIM']>0)) // Alimentation des valeurs de la dimension
			$arDimValComboList=Dimension::_getDimensionValues($valueArray['REF_DIM']);

			$denombr_id = $valueArray['ID'] ;
			$ref_dim = $valueArray['REF_DIM'] ;
			$id_dim = $valueArray['ID_DIM'] ;
			$nbr_occ = $valueArray['NBR_OCC'] ;
			$date_occ = $valueArray['DATE_OCC'] ;
			if ($structId!='')
			$id_struct = $structId;
			else
			$id_struct = $config['id_struct'] ;

		}else{
			if($denombr_id!=0)
			{
				$ref_dim = $this->DenombrementArray[$structId][$denombr_id]['REF_DIM'] ;
				$id_dim = $this->DenombrementArray[$structId][$denombr_id]['ID_DIM'] ;
				$nbr_occ = $this->DenombrementArray[$structId][$denombr_id]['NBR_OCC'] ;
				$date_occ = $this->DenombrementArray[$structId][$denombr_id]['DATE_OCC'] ;
				$id_struct = $structId ;

			}else{
				$denombr_id = 0 ;
				$ref_dim = '' ;
				$id_dim = 0 ;
				$nbr_occ = '';
				$date_occ = '' ;
				$id_struct = $config['id_struct'] ;
			}
		}

		if ($date_occ=='')
		$date_occ=time();

		$date=new HDate();
		$xtplform->assign('DENOMBR_ID', $form->_mkInput('hidden', 'denombr_id', $denombr_id)) ;
		$xtplform->assign('REF_DIM', $form->_mkSelect('ref_dim', $this->DimensionComboArray, $ref_dim,'onchange=submit()')) ;
		$xtplform->assign('ID_DIM', $form->_mkSelect('id_dim', $arDimValComboList, $id_dim,'')) ;
		$xtplform->assign('NBR_OCC', $form->_mkInput('text', 'nbr_occ',$nbr_occ)) ;
		$xtplform->assign('DATE_OCC', $form->_mkDateInput('date_occ',$date->_dateStampLgFormat($date_occ, $this->lang))) ;
		$xtplform->assign('STRUCT_PARENT', $form->_mkSelect('id_struct', $this->StructComboArray, $id_struct,'')) ;

		$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('validate', 'Enregistrer' ) );
			
		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Denombrement::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>