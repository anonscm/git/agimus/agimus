<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//R�cup�ration de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 52, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['catsrv_id'])){
			$catsrvId = $_GET['catsrv_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['catsrv_id'])){
			$catsrvId = $_POST['catsrv_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$catsrvId = 0 ;
			$action = 'create' ;
		}


		require_once( 'refCatSrv.php') ;
		$catsrv = new RefCatSrv($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 54, $uid, '_CAT_SRV_DELETE_NOT_ALLOWED_'))
				{
					$catsrv->_delete($catsrvId) ;
					$session->_sessionLogAction(1, '_CAT_SRV_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_catsrv.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 55, $uid, '_CAT_SRV_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donn�es dans le formulaire
					$catsrv->_makeForm($catsrvId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($catsrvId == '')&&($rights->_isActionAllowed('ref', 53, $uid, '_CAT_SRV_CREATE_NOT_ALLOWED_')))
				{
					$catsrv->_checkFormValues($_POST,$action) ;
					$catsrv->_setFormValues($_POST) ;
					$catsrv->_create() ;
					$session->_sessionLogAction(1, '_CAT_SRV_CREATE_') ;
				}
				if(($catsrvId != '')&&($rights->_isActionAllowed('ref', 55, $uid, '_CAT_SRV_UPDATE_NOT_ALLOWED_')))
				{
					$catsrv->_checkFormValues($_POST,$action) ;
					$catsrv->_setFormValues($_POST) ;
					$catsrv->_update() ;
					$session->_sessionLogAction(1, '_CAT_SRV_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_catsrv.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 55, $uid))
				{
					$catsrv->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$catsrvId='' ;
				if($rights->_isActionAllowed('ref', 54, $uid))
				{
					$catsrv->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$catsrv->_makeForm($catsrvId, $valueArray, $docref, $docVal) ;
			}
		}

		$catsrv->_makePage($catsrvId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
