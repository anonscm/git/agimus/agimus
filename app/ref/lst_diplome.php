<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//R�cup�ration de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 60, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['diplome_id'])){
			$diplomeId = $_GET['diplome_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['diplome_id'])){
			$diplomeId = $_POST['diplome_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$diplomeId = 0 ;
			$action = 'create' ;
		}


		require_once( 'refDiplome.php') ;
		$diplome = new RefDiplome($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 62, $uid, '_DIPLOME_DELETE_NOT_ALLOWED_'))
				{
					$diplome->_delete($diplomeId) ;
					$session->_sessionLogAction(1, '_DIPLOME_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_diplome.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 63, $uid, '_DIPLOME_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donn�es dans le formulaire
					$diplome->_makeForm($diplomeId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($diplomeId == '')&&($rights->_isActionAllowed('ref', 61, $uid, '_DIPLOME_CREATE_NOT_ALLOWED_')))
				{
					$diplome->_checkFormValues($_POST,$action) ;
					$diplome->_setFormValues($_POST) ;
					$diplome->_create() ;
					$session->_sessionLogAction(1, '_DIPLOME_CREATE_') ;
				}
				if(($diplomeId != '')&&($rights->_isActionAllowed('ref', 63, $uid, '_DIPLOME_UPDATE_NOT_ALLOWED_')))
				{
					$diplome->_checkFormValues($_POST,$action) ;
					$diplome->_setFormValues($_POST) ;
					$diplome->_update() ;
					$session->_sessionLogAction(1, '_DIPLOME_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_diplome.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 61, $uid))
				{
					$diplome->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$diplomeId='' ;
				if($rights->_isActionAllowed('ref', 62, $uid))
				{
					$diplome->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$diplome->_makeForm($diplomeId, $valueArray, $docref, $docVal) ;
			}
		}

		$diplome->_makePage($diplomeId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
