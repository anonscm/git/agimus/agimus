<?php
//v1.5-PPR-#016 ajout nouvelle dimension
require_once( DIR_WWW.ROOT_APPL.'/app/common/regInsc.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/regInscIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;

/**
 *
 * <p>Regime d'inscription</p>
 *
 * @name RegInsc
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class RefRegInsc extends RegInsc {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    
	 /*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste arborecente des srvIntras
	 */
	private $RegInscTreeArray ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des regroupements
	 */
	private $RegrComboArray ;

	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name RegInsc::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'reginsclist.xtpl' ;
		$this->form_file = 'reginscform.xtpl' ;
		$this->xtpl_tree = 'reginsctree.xtpl' ;
		$this->xtpl_path = $this->lang.'/ref/'  ;
		$this->RegInscTreeArray = $this->_getList() ;

	}

	/**
	 * V�rification des variables re�ues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refRegInsc::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray,$action='')
	{
		if($valueArray['reginsc_lib'] == '')
		{
			throw new MsgException('_ERROR_LIBELLE_MISSING_')  ;
		}

		$maconnexion = MysqlDatabase::GetInstance() ;

		//Verification que le libelle n'est pas deja utilise

		$sql = 'SELECT * FROM t_reg_insc ';
		$sql .= 'WHERE LIB=  \''.AddSlashes($valueArray['reginsc_lib']).'\' ';
		if($valueArray['reginsc_id']!=0)
		{
			$sql .= 'AND ID != \''.$valueArray['reginsc_id'].'\' ';
		}
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res) >0)
		{
			throw new msgException('_ERROR_LIBELLE_ALREADY_EXISTS_')  ;
		}


	}

	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refRegInsc::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
		RegInsc::_setId($valueArray['reginsc_id']) ;
		RegInsc::_setExtnRef($valueArray['reginsc_extn_ref']) ;
		RegInsc::_setLib($valueArray['reginsc_lib']) ;
		RegInsc::_setDsc($valueArray['reginsc_dsc']) ;
		RegInsc::_setIdRegr($valueArray['id_regr']) ;
	}

	/**
	 * Insertion des donn�es dans le mod�le de pag
	 *
	 * <p>_makePage</p>
	 *
	 * @name RegInsc::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($reginscId=0, $msg='')
	{
		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
			$xtpl->assign('MSG', $msg) ;
			$xtpl->parse('main.msg');
		}


		foreach($this->RegInscTreeArray as $key=>$value)
		{
			$xtpl->assign('REG_INSC_ID', $key);
			$xtpl->assign('EXTN_REF', $value['EXTN_REF']);
			$xtpl->assign('LIB', $value['LIB']);
			$xtpl->assign('DSC', $value['DSC']);
			$xtpl->assign('REGR_ID', $value['ID_REGR']);
			
			//Recupération des fils intra
			$arFils=RegInscIntra::_getList($key);
			$nbFils=count($arFils);
			
			if(($key != $reginscId)&&($this->rights->_isActionAllowed('ref', 67, $this->uid)))
			{
				$xtpl->parse('main.list.row.modif');
			}

			//V1.5-PPR-#000 suppression message notice
			if ((isset($value['tree'])) and ($value['tree'] != NULL))
			{
				$treeArray = $value['tree'] ;
				$treeString = $this->_buildTree($treeArray, $maxlevel, 1) ;
				$xtpl->assign('REG_INSC_TREE', $treeString );
				$xtpl->parse('main.list.row.tree');
			}else{
				//v1.2-PPR-28032011 restriction à la suppression des id > 1 (1 : autres)
				//if(($nbFils=0) && ($key != $reginscId)&&($this->rights->_isActionAllowed('ref', 34, $this->uid)))
				if(($key!=1) && ($nbFils==0) && ($key != $reginscId)&&($this->rights->_isActionAllowed('ref', 66, $this->uid)))
				{
					$xtpl->parse('main.list.row.delete');
				}
			}
			$xtpl->parse('main.list.row');
		}
		$xtpl->parse('main.list');
		//Construction du menu en fonction des droits de l'utilisateur

		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}
		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}

	// non utilis�e pour l'instant
	private function _buildTree($subtreeArray, $maxlevel = 0, $treelevel=0)
	{
		$levelmax = $maxlevel ;
		$treeArray = $subtreeArray ;
		$level=$treelevel+1 ;
		$treeString = '' ;
		$xtpl = new XTemplate($this->xtpl_tree, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		foreach($treeArray as $key=>$value)
		{
			foreach($value as $key2=>$val2)
			{
				$xtpl->assign('LEVEL_PERE', $level) ;
				$xtpl->parse('main.row.level');

				$xtpl->assign('LEVEL',1 ) ;
				if($maxlevel-$level>0)
				{
					$xtpl->assign('LEVEL_SON',$maxlevel-$level) ;
					$xtpl->parse('main.row.levelson');
				}
				$xtpl->assign('REG_INSC_LIB', $val2['libelle']);
				$xtpl->assign('REG_INSC_ID', $key2);
				if($this->rights->_isActionAllowed('ref', 67, $this->uid))
				{
					$xtpl->parse('main.row.modif');
				}

				if($val2['tree'] != NULL)
				{
					$subtree = $this->_buildTree($val2['tree'], $levelmax, $level) ;
					$xtpl->assign('REG_INSC_TREE', $subtree);
					$xtpl->parse('main.row.tree');
				}else{
					if($this->rights->_isActionAllowed('ref', 66, $this->uid))
					{
						$xtpl->parse('main.row.delete');
					}

				}
				$xtpl->parse('main.row');
			}
		}
		$xtpl->parse('main');
		$treeString = $xtpl->text('main') ;
		return $treeString ;

	}

	public function _makeForm($reginsc_id=0, $valueArray=array(), $doc_ref=0, $docVal = array())
	{
		$form = new Form('../lst_reginsc.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);
		$config=$_SESSION['config'];

		if(($valueArray == NULL)&&($reginsc_id==0))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}
		if($valueArray != NULL)
		{
			$reginsc_extn_ref = $valueArray['reginsc_extn_ref'] ;
			$reginsc_lib = $valueArray['reginsc_lib'] ;
			$reginsc_id = $valueArray['reginsc_id'] ;
			$reginsc_dsc = $valueArray['reginsc_dsc'] ;
			$id_regr = $valueArray['id_regr'];

		}else{
			if($reginsc_id!=0)
			{
				$reginsc_extn_ref =  $this->RegInscTreeArray[$reginsc_id]['EXTN_REF'] ;
				$reginsc_lib =  $this->RegInscTreeArray[$reginsc_id]['LIB'] ;
				$reginsc_dsc =  $this->RegInscTreeArray[$reginsc_id]['DSC'] ;
				$id_regr =  $this->RegInscTreeArray[$reginsc_id]['ID_REGR'] ;
			}else{
				$reginsc_extn_ref = '' ;
				$reginsc_lib = '' ;
				$reginsc_dsc= '' ;
				$id_regr = 0;
			}
		}

		$xtplform->assign('REG_INSC_ID', $form->_mkInput('hidden', 'reginsc_id', $reginsc_id)) ;
		$xtplform->assign('EXTN_REF', $form->_mkInput('text', 'reginsc_extn_ref', $reginsc_extn_ref)) ;
		$xtplform->assign('LIB', $form->_mkInput('text', 'reginsc_lib',$reginsc_lib)) ;
		$xtplform->assign('DSC', $form->_mkInput('text', 'reginsc_dsc',$reginsc_dsc)) ;
		$xtplform->assign('REGR_ID', $form->_mkInput('hidden','id_regr', $id_regr,'')) ;

		$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('submit', 'Enregistrer' ) );

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	public function _makeView($rights=NULL, $reginsc_id=0)
	{
		$form = new Form('../lst_reginsc.php') ;
		$xtplform = new XTemplate($this->view_file, $this->xtpl_path);

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name RegInsc::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>