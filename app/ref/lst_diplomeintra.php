<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//R�cup�ration de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 40, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['diplomeintra_id'])){
			$diplomeintraId = $_GET['diplomeintra_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['diplomeintra_id'])){
			$diplomeintraId = $_POST['diplomeintra_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$diplomeintraId = 0 ;
			$action = 'create' ;
		}


		require_once( 'refDiplomeIntra.php') ;
		$diplomeintra = new RefDiplomeIntra($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 44, $uid, '_DIPLOME_INTRA_DELETE_NOT_ALLOWED_'))
				{
					$diplomeintra->_delete($diplomeintraId) ;
					$session->_sessionLogAction(1, '_DIPLOME_INTRA_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_diplomeintra.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 43, $uid, '_DIPLOME_INTRA_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donn�es dans le formulaire
					$diplomeintra->_makeForm($diplomeintraId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($diplomeintraId == '')&&($rights->_isActionAllowed('ref', 41, $uid, '_DIPLOME_INTRA_CREATE_NOT_ALLOWED_')))
				{
					$diplomeintra->_checkFormValues($_POST,$action) ;
					$diplomeintra->_setFormValues($_POST) ;
					$diplomeintra->_create() ;
					$session->_sessionLogAction(1, '_DIPLOME_INTRA_CREATE_') ;
				}
				if(($diplomeintraId != '')&&($rights->_isActionAllowed('ref', 43, $uid, '_DIPLOME_INTRA_UPDATE_NOT_ALLOWED_')))
				{
					$diplomeintra->_checkFormValues($_POST,$action) ;
					$diplomeintra->_setFormValues($_POST) ;
					$diplomeintra->_update() ;
					$session->_sessionLogAction(1, '_DIPLOME_INTRA_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_diplomeintra.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 41, $uid))
				{
					$diplomeintra->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$diplomeintraId='' ;
				if($rights->_isActionAllowed('ref', 42, $uid))
				{
					$diplomeintra->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$diplomeintra->_makeForm($diplomeintraId, $valueArray, $docref, $docVal) ;
			}
		}

		$diplomeintra->_makePage($diplomeintraId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
