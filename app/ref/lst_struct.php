<?php
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//R�cup�ration de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 20, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['struct_id'])){
			$structId = $_GET['struct_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['struct_id_int'])){
			$structIdInt = $_POST['struct_id_int'] ;
			$structId = $_POST['struct_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$structId = '' ;
			$structIdInt=0;
			$action = 'create' ;
		}


		require_once( 'refStruct.php') ;
		$struct = new RefStruct($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 22, $uid, '_STRUCT_DELETE_NOT_ALLOWED_'))
				{
					$struct->_delete($structId) ;
					$session->_sessionLogAction(1, '_STRUCT_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_struct.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 23, $uid, '_STRUCT_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donn�es dans le formulaire
					$struct->_makeForm($structId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($structIdInt == '')&&($rights->_isActionAllowed('ref', 21, $uid, '_STRUCT_CREATE_NOT_ALLOWED_')))
				{
					$struct->_checkFormValues($_POST,$action) ;
					$struct->_setFormValues($_POST) ;
					$struct->_create() ;
					$session->_sessionLogAction(1, '_STRUCT_CREATE_') ;
				}
				if(($structIdInt != '')&&($rights->_isActionAllowed('ref', 23, $uid, '_STRUCT_UPDATE_NOT_ALLOWED_')))
				{
					$struct->_checkFormValues($_POST,$action) ;
					$struct->_setFormValues($_POST) ;
					$struct->_update() ;
					$session->_sessionLogAction(1, '_STRUCT_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_struct.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 21, $uid))
				{
					$struct->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$structId='' ;
				if($rights->_isActionAllowed('ref', 22, $uid))
				{
					$struct->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$struct->_makeForm($structId, $valueArray, $docref, $docVal) ;
			}
		}

		$struct->_makePage($structId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
