<?php
//v1.5-PPR-#016 ajout nouvelle dimension
require_once( '../../app/common/required.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/library.php') ;
require_once( '../ref/refSession.php') ;

//Recuperation de la session
$session = RefSession::_GetInstance() ;
$uid = $session->_getUid() ;
$rights = ModuleRights::_GetInstance() ;

try{
	if(($rights->_isModuleAllowed(3, 1, $uid))&&($rights->_isActionAllowed('ref', 81, $uid, '_FONCTION_NOT_ALLOWED_')))
	{
		$msgString = '' ;
		$valueArray = array() ;
		$docVal = array() ;

		if(isset($_GET['composante_id'])){
			$composanteId = $_GET['composante_id'] ;
			if(isset($_GET['del'])){
				$action = 'delete' ;
			}else{
				$action = 'update' ;
			}
		}elseif(isset($_POST['composante_id'])){
			$composanteId = $_POST['composante_id'] ;
			$valueArray = $_POST ;
			$action = 'submit' ;
		}else{
			$composanteId = 0 ;
			$action = 'create' ;
		}


		require_once( 'refComposante.php') ;
		$composante = new RefComposante($session, $rights) ;
		try{
			if($action == 'delete'){
				if($rights->_isActionAllowed('ref', 83, $uid, '_COMPOSANTE_DELETE_NOT_ALLOWED_'))
				{
					$composante->_delete($composanteId) ;
					$session->_sessionLogAction(1, '_COMPOSANTE_DELETE_') ;
					header('Location: '.ROOT_APPL.'/app/ref/lst_composante.php');
					exit() ;
				}

			}elseif($action == 'update'){
				if($rights->_isActionAllowed('ref', 84, $uid, '_COMPOSANTE_UPDATE_NOT_ALLOWED_'))
				{
					//affichage des donnees dans le formulaire
					$composante->_makeForm($composanteId, $valueArray, $docref, $docVal) ;
				}
			}elseif($action == 'submit'){
				if(($composanteId == '')&&($rights->_isActionAllowed('ref', 82, $uid, '_COMPOSANTE_CREATE_NOT_ALLOWED_')))
				{
					$composante->_checkFormValues($_POST,$action) ;
					$composante->_setFormValues($_POST) ;
					$composante->_create() ;
					$session->_sessionLogAction(1, '_COMPOSANTE_CREATE_') ;
				}
				if(($composanteId != '')&&($rights->_isActionAllowed('ref', 84, $uid, '_COMPOSANTE_UPDATE_NOT_ALLOWED_')))
				{
					$composante->_checkFormValues($_POST,$action) ;
					$composante->_setFormValues($_POST) ;
					$composante->_update() ;
					$session->_sessionLogAction(1, '_COMPOSANTE_UPDATE_') ;
				}
				header('Location: '.ROOT_APPL.'/app/ref/lst_composante.php');
				exit() ;

			}else{
				if($rights->_isActionAllowed('ref', 82, $uid))
				{
					$composante->_makeForm() ;
				}
			}
		}
		catch(MsgException $e){
			$msgString = $e ->_getError($session) ;
			if($action == 'delete'){
				$composanteId='' ;
				if($rights->_isActionAllowed('ref', 83, $uid))
				{
					$composante->_makeForm() ;
				}
			}
			if($action == 'submit')
			{
				$composante->_makeForm($composanteId, $valueArray, $docref, $docVal) ;
			}
		}

		$composante->_makePage($composanteId, $msgString) ;
	}
}
catch(MsgException $e){
	$msgString = $e ->_getError($session) ;
	$session->_makeErrorPage($msgString) ;
}
