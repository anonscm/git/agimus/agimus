<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/session.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/affiliation.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/affilIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/structure.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/application.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/srvIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/catSrv.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/dcplIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/discipline.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/diplomeIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/diplome.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/domEtuIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/domEtu.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/denombrement.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/indicateur.php') ;
//v1.5-PPR-#016 ajout nouvelles dimensions
require_once( DIR_WWW.ROOT_APPL.'/app/common/compIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/composante.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/regInscIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/regInsc.php') ;

/**
 * Gestion des sessions dans le module Ref
 * <p>RefSession</p>
 * 
 * @name RefSession
 * @author AGIMUS <agimus.technique@education.gouv.fr>  
 * @licence Cecill v2 (http://www.cecill.info) 
 * @copyright Capella Conseil 2009
 * @version 1.0.0
 * @package ref
 */
 
 class RefSession extends Session {
 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. attributs     */
    /*~*~*~*~*~*~*~*~*~*~*/
 	/**
    * @var (Singleton)
    * @desc variable pour le singleton
    */
 	private static $instance;
 	    
    /**
    * @var $menu_file (String)
    * @desc fichier template du menu vertical
    */
    private $menu_file ;
    
    /**
    * @var $xtpl_path (String)
    * @desc chemin des templates
    */   
    private $xtpl_path ;

    /**
    * @var $module (String)
    * @desc module
    */  
    private $module ;
    
    /**
    * @var (Array)
    * @desc tableau des objets du module ref
    */
    private $refArray  ;
        
    
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>creation de l'instance de la classe</p>
    * 
    * @name RefSession::__construct()
    * @return void 
    */
    public function __construct() {
    	parent::__construct();
    	$this->module = 'ref' ;
    	$this->menu_file = 'refmenuleft.xtpl' ; 
    	$this->xtpl_path = Session::_getLang().'/ref/'  ;
    	$this->uid = Session::_getUid() ;
    	$this->_countReferences() ;
    }
    /**
    * Singleton
    * 
    * <p>creation de l'instance de la classe si n'existe pas</p>
    * 
    * @name Session::_GetInstance()
    * @return Session
    */
	public static function _GetInstance()
	{
		if(!isset(self::$instance))
		{
			self::$instance = new RefSession();
		}
		return self::$instance;
	}  

	public function _getXtplPath()
	{
		return $this->xtpl_path ;
	}
	
	/**
    * Initialisation des variables de session pour l'affichage
    * 
    * @name RefSession::_initSessionValue()
    * @param $refArray (Array)
    * 			Tableau des identifiants avec $key=>identifiant (int) et $value=>identifiant (int)
    * @return void 
    */ 
    public function _initSessionValue($classCi=1)
    {
    	$_SESSION['class_ci'] = $classCi ;
    	$cirefArray = array() ;
    	$_SESSION['ci_refarray'] = $cirefArray ;
    	$_SESSION['ci_tab'] = 'cont' ;
    	$_SESSION['ci_page'] = 'list' ;
    	$_SESSION['ci_view'] = 'smtree';
    	$_SESSION['father_ref'] = 0;
    }
	    
    /**
    * @name RefSession::_setDocTypeArray()
    * @param $typeArray (Array)
    * @return void 
    */ 
    public function _setDocTypeArray($typeArray)
    {
    	$_SESSION['doc_type'] = $typeArray ;
    }

    /**
    * @name RefSession::_setDocObjArray()
    * @param $ObjArray (Array)
    * @return void 
    */ 
    public function _setDocObjArray($ObjArray)
    {
    	$_SESSION['doc_objet'] = $ObjArray ;
    }

    /**
    * @name RefSession::_setDocintArray()
    * @param $IntArray (Array)
    * @return void 
    */ 
    public function _setDocIntArray($intArray)
    {
    	$_SESSION['doc_user'] = $intArray ;
    }
    
     /**
    * @name RefSession::_setDocTriArray()
    * @param $TriArray (Array)
    * @return void 
    */ 
    public function _setDocTriArray($triArray)
    {
    	$_SESSION['doc_tri'] = $triArray ;
    }
    
    /**
    * @name RefSession::_getDocTypeArray()
    * @return Array 
    */ 
    public function _getDocTypeArray()
    {
    	if($_SESSION['doc_type'] != NULL)
    	{
    		return $_SESSION['doc_type'];
    	}else{
    		$typeArray = array() ;
    		return $typeArray;
    	}
    }
    /**
    * @name Session::_getDocObjArray()
    * @return Array 
    */ 
    public function _getDocObjArray()
    {
     	if($_SESSION['doc_objet'] != NULL)
    	{
    		return $_SESSION['doc_objet'];
    	}else{
    		$ObjArray = array() ;
    		return $ObjArray ;
    	}
    }    

    /**
    * @name Session::_getDocIntArray()
    * @return Array 
    */ 
    public function _getDocIntArray()
    {
     	if($_SESSION['doc_user'] != NULL)
    	{
    		return $_SESSION['doc_user'];
    	}else{
    		$intArray = array() ;
    		return $intArray ;
    	}
    }
    
     /**
    * @name Session::_getDocTriArray()
    * @return Array 
    */ 
    public function _getDocTriArray()
    {
     	if($_SESSION['doc_tri'] != NULL)
    	{
    		return $_SESSION['doc_tri'];
    	}else{
    		$triArray = array() ;
    		return $triArray ;
    	}
    }
    
     private function _countReferences()
    {
      $this->refArray = array() ;

      $struct = new Structure();
      $this->refArray['struct'] = $struct->_countRef() ;
      $appli = new Application();
      $this->refArray['appli'] = $appli->_countRef() ;
      $srvintra = new SrvIntra();
      $this->refArray['srvintra'] = $srvintra->_countRef() ;
      $dcplintra = new DcplIntra();
      $this->refArray['dcplintra'] = $dcplintra->_countRef() ;
      $diplomeintra = new DiplomeIntra();
      $this->refArray['diplomeintra'] = $diplomeintra->_countRef() ;
      $dometuintra = new DomEtuIntra();
      $this->refArray['dometuintra'] = $dometuintra->_countRef() ;
      $affilintra = new AffilIntra();
      $this->refArray['affilintra'] = $affilintra->_countRef() ;
      $catsrv = new CatSrv();
      $this->refArray['catsrv'] = $catsrv->_countRef() ;
      $discipline = new Discipline();
      $this->refArray['discipline'] = $discipline->_countRef() ;
      $diplome = new Diplome();
      $this->refArray['diplome'] = $diplome->_countRef() ;
      $dometu = new DomEtu();
      $this->refArray['dometu'] = $dometu->_countRef() ;
      $affiliation = new Affiliation();
      $this->refArray['affiliation'] = $affiliation->_countRef() ;
      $denombr = new Denombrement();
      $this->refArray['denombr'] = $denombr->_countRef() ;
      $indic = new Indicateur();
      $this->refArray['indic'] = $indic->_countRef() ;

      //v1.5-PPR-#016 ajout nouvelles dimensions
	  $compintra=new compIntra();
	  $this->refArray['compintra'] = $compintra->_countRef() ;
	  $composante=new Composante();
	  $this->refArray['composante'] = $composante->_countRef() ;
	  $reginscintra=new RegInscIntra();
	  $this->refArray['reginscintra'] = $reginscintra->_countRef() ;
	  $reginsc=new RegInsc();
	  $this->refArray['reginsc'] = $reginsc->_countRef() ;
      
    }
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /*  2.1 m�thodes publiques */
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /**
    * <p>Construction de la page principale</p>
    * 
    * @name Session::_makeMainPage()
    * @param $content (String)
    * @return void 
    */   

    public function _makeMenuLeft($rights)
    {
		$xtplm = new XTemplate($this->menu_file, $this->xtpl_path);
		$xtplm->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtplm->assign('NBR_STRUCT', $this->refArray['struct']);
		$xtplm->assign('NBR_APPLI', $this->refArray['appli']);
		$xtplm->assign('NBR_SRV_INTRA', $this->refArray['srvintra']);
		$xtplm->assign('NBR_DCPL_INTRA', $this->refArray['dcplintra']);
		$xtplm->assign('NBR_DIPLOME_INTRA', $this->refArray['diplomeintra']);
		$xtplm->assign('NBR_DOMETU_INTRA', $this->refArray['dometuintra']);
		$xtplm->assign('NBR_AFFIL_INTRA', $this->refArray['affilintra']);
		$xtplm->assign('NBR_CAT_SRV', $this->refArray['catsrv']);
		$xtplm->assign('NBR_DISCIPLINE', $this->refArray['discipline']);
		$xtplm->assign('NBR_DIPLOME', $this->refArray['diplome']);
		$xtplm->assign('NBR_DOMETU', $this->refArray['dometu']);
		$xtplm->assign('NBR_DENOMBR', $this->refArray['denombr']);
		$xtplm->assign('NBR_INDIC', $this->refArray['indic']);
		$xtplm->assign('NBR_AFFILIATION', $this->refArray['affiliation']);
		//v1.5-PPR-#016 ajout nouvelles dimensions
		$xtplm->assign('NBR_COMP_INTRA', $this->refArray['compintra']);
		$xtplm->assign('NBR_COMPOSANTE', $this->refArray['composante']);
		$xtplm->assign('NBR_REG_INSC_INTRA', $this->refArray['reginscintra']);
		$xtplm->assign('NBR_REG_INSC', $this->refArray['reginsc']);
		
        if($rights->_isActionAllowed($this->module, 20, $this->uid))
      	{
            $xtplm->parse('main.structopen'); 
      	}else{
      		$xtplm->parse('main.structclose');
      	}

        if($rights->_isActionAllowed($this->module, 32, $this->uid))
      	{
            $xtplm->parse('main.appliopen'); 
      	}else{
      		$xtplm->parse('main.appliclose');
      	}
      	
        if($rights->_isActionAllowed($this->module, 36, $this->uid))
      	{
            $xtplm->parse('main.srvintraopen'); 
      	}else{
      		$xtplm->parse('main.srvintraclose');
      	}

        if($rights->_isActionAllowed($this->module, 52, $this->uid))
      	{
            $xtplm->parse('main.catsrvopen'); 
      	}else{
      		$xtplm->parse('main.catsrvclose');
      	}

        if($rights->_isActionAllowed($this->module, 40, $this->uid))
      	{
            $xtplm->parse('main.dcplintraopen'); 
      	}else{
      		$xtplm->parse('main.dcplintraclose');
      	}

        if($rights->_isActionAllowed($this->module, 56, $this->uid))
      	{
            $xtplm->parse('main.disciplineopen'); 
      	}else{
      		$xtplm->parse('main.disciplineclose');
      	}
      	
        if($rights->_isActionAllowed($this->module, 44, $this->uid))
      	{
            $xtplm->parse('main.diplomeintraopen'); 
      	}else{
      		$xtplm->parse('main.diplomeintraclose');
      	}

        if($rights->_isActionAllowed($this->module, 60, $this->uid))
      	{
            $xtplm->parse('main.diplomeopen'); 
      	}else{
      		$xtplm->parse('main.diplomeclose');
      	}

       if($rights->_isActionAllowed($this->module, 48, $this->uid))
      	{
            $xtplm->parse('main.dometuintraopen'); 
      	}else{
      		$xtplm->parse('main.dometuintraclose');
      	}

        if($rights->_isActionAllowed($this->module, 64, $this->uid))
      	{
            $xtplm->parse('main.dometuopen'); 
      	}else{
      		$xtplm->parse('main.dometuclose');
      	}
      	
      	if($rights->_isActionAllowed($this->module, 24, $this->uid))
      	{
            $xtplm->parse('main.denombropen'); 
      	}else{
      		$xtplm->parse('main.denombrclose');
      	}

      	if($rights->_isActionAllowed($this->module, 28, $this->uid))
      	{
            $xtplm->parse('main.indicopen'); 
      	}else{
      		$xtplm->parse('main.indicclose');
      	}
      	
      	if($rights->_isActionAllowed($this->module, 72, $this->uid))
      	{
            $xtplm->parse('main.affilintraopen'); 
      	}else{
      		$xtplm->parse('main.affilintraclose');
      	}
      	
      	if($rights->_isActionAllowed($this->module, 68, $this->uid))
      	{
            $xtplm->parse('main.affiliationopen'); 
      	}else{
      		$xtplm->parse('main.affiliationclose');
      	}
 
 		//v1.5-PPR-#016 ajout nouvelles dimensions
 
     	if($rights->_isActionAllowed($this->module, 81, $this->uid))
      	{
            $xtplm->parse('main.compintraopen'); 
      	}else{
      		$xtplm->parse('main.compintraclose');
      	}
      	
      	if($rights->_isActionAllowed($this->module, 77, $this->uid))
      	{
            $xtplm->parse('main.composanteopen'); 
      	}else{
      		$xtplm->parse('main.composanteclose');
      	}
 
     	if($rights->_isActionAllowed($this->module, 89, $this->uid))
      	{
            $xtplm->parse('main.reginscintraopen'); 
      	}else{
      		$xtplm->parse('main.reginscintraclose');
      	}
      	
      	if($rights->_isActionAllowed($this->module, 85, $this->uid))
      	{
            $xtplm->parse('main.reginscopen'); 
      	}else{
      		$xtplm->parse('main.reginscclose');
      	}
 


      	
      	$xtplm->parse('main');
      	$menuString = $xtplm->text('main') ;
      	return $menuString;
    }
 } 
?>