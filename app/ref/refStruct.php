<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/catStruct.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/structure.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;

/**
 *
 * <p>Societe</p>
 *
 * @name Societe
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class RefStruct extends Structure {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    
	 /*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste arborecente des structures
	 */
	private $StructTreeArray ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des structures
	 */
	private $StructArray ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des categories de structures
	 */
	private $CatStructArray ;

	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Structure::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'structlist.xtpl' ;
		$this->form_file = 'structform.xtpl' ;
		$this->xtpl_tree = 'structtree.xtpl' ;
		$this->xtpl_path = $this->lang.'/ref/'  ;
		$this->StructTreeArray = $this->_getTreeList() ;
		$this->StructArray = $this->_getList() ;
		$this->StructComboArray = Structure::_getComboList() ;

		$catStruct=new CatStruct();
		$dummyCat=$catStruct->_getList();
		$this->CatStructArray = $catStruct->_getComboList() ;
	}

	/**
	 * V�rification des variables re�ues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refStruct::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray,$action='')
	{
		if($valueArray['struct_id'] == '')
		{
			throw new MsgException('_ERROR_ID_MISSING_')  ;
		}

		if($valueArray['struct_lib'] == '')
		{
			throw new MsgException('_ERROR_LIBELLE_MISSING_')  ;
		}

		if(!$valueArray['cat_struct'] >0)
		{
			throw new MsgException('_ERROR_CAT_STRUCT_MISSING_')  ;
		}

		// si on n'est pas � la racine et qu'il n'y a pas de parent : erreur
		//if(($valueArray['struct_id']!=ROOT_EXTN_REF) && (!$valueArray['struct_parent'] >0))
		//{
		//	throw new MsgException('_ERROR_PARENT_MISSING_')  ;
		//}

		$maconnexion = MysqlDatabase::GetInstance() ;

		//V�rification que le libell� n'est pas d�j� utilis�
		//v1.2-PPR-28032011 annulation du contrôle car impossible alors de modifier l'ID...

//		$sql = 'SELECT * FROM t_struct ';
//		$sql .= 'WHERE LIB=  \''.AddSlashes($valueArray['struct_lib']).'\' ';
//		if($valueArray['struct_id']!='')
//		{
//			$sql .= 'AND ID != \''.$valueArray['struct_id'].'\' ';
//		}
//		try{
//			$res = $maconnexion->_bddQuery($sql) ;
//		}
//		catch(MsgException $e){
//			$msgString = $e ->_getError();
//			throw new MsgException($msgString, 'database') ;
//		}
//		if($maconnexion->_bddNumRows($res) >0)
//		{
//			throw new msgException('_ERROR_LIBELLE_ALREADY_EXISTS_')  ;
//		}

		//V�rification que l'id n'est pas d�j� utilis�

		if ($action=='create') // creation
		{
			$sql = 'SELECT * FROM t_struct ';
			$sql .= 'WHERE ID = \''.$valueArray['struct_id'].'\' ';
			try{
				$res = $maconnexion->_bddQuery($sql) ;
			}
			catch(MsgException $e){
				$msgString = $e ->_getError();
				throw new MsgException($msgString, 'database') ;
			}
			if($maconnexion->_bddNumRows($res) >0)
			{
				throw new msgException('_ERROR_ID_ALREADY_EXISTS_')  ;
			}
		}
	}

	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refStruct::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
		//v1.2-PPR reinit du parent à "" si 0
		if ($valueArray['struct_parent']=="0")
			$valueArray['struct_parent']="";
		
		Structure::_setId($valueArray['struct_id']) ;
		Structure::_setLib($valueArray['struct_lib']) ;
		Structure::_setDsc($valueArray['struct_dsc']) ;
		Structure::_setParent($valueArray['struct_parent']) ;
		Structure::_setCatStruct($valueArray['cat_struct']) ;
		Structure::_setUrlExport($valueArray['struct_url_export']) ;
		Structure::_setUser($valueArray['struct_user']) ;
		Structure::_setPwd($valueArray['struct_pwd']) ;
		Structure::_setIdIntra($valueArray['struct_id_int']) ;
	}

	/**
	 * Insertion des donn�es dans le mod�le de pag
	 *
	 * <p>_makePage</p>
	 *
	 * @name Structure::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($structId='', $msg='')
	{
		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
			$xtpl->assign('MSG', $msg) ;
			$xtpl->parse('main.msg');
		}
		$maxlevel = $this->_getLevel() ;
		$xtpl->assign('LEVEL_MAX', $maxlevel+1) ;
		$xtpl->assign('LEVEL', 1) ;
		$xtpl->assign('LEVEL_SON', $maxlevel) ;
		foreach($this->StructTreeArray as $key=>$value)
		{
			$xtpl->assign('STRUCT_ID', $key);
			$xtpl->assign('STRUCT_LIB', $value['libelle']);
			$xtpl->assign('CAT_STRUCT_LIB', $this->CatStructArray[$value['cat_struct']]);
			if(($key != $structId)&&($this->rights->_isActionAllowed('ref', 23, $this->uid)))
			{
				$xtpl->parse('main.list.row.modif');
			}

			if($value['tree'] != NULL)
			{
				$treeArray = $value['tree'] ;
				$treeString = $this->_buildTree($treeArray, $maxlevel, 1) ;
				$xtpl->assign('STRUCT_TREE', $treeString );
				$xtpl->parse('main.list.row.tree');
			}else{
				if(($key != $structId)&&($this->rights->_isActionAllowed('ref', 22, $this->uid)))
				{
					$xtpl->parse('main.list.row.delete');
				}
			}
			$xtpl->parse('main.list.row');
		}
		$xtpl->parse('main.list');
		//Construction du menu en fonction des droits de l'utilisateur

		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}
		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}

	private function _buildTree($subtreeArray, $maxlevel = 0, $treelevel=0)
	{
		$levelmax = $maxlevel ;
		$treeArray = $subtreeArray ;
		$level=$treelevel+1 ;
		$treeString = '' ;
		$xtpl = new XTemplate($this->xtpl_tree, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		foreach($treeArray as $key=>$value)
		{
			foreach($value as $key2=>$val2)
			{
				$xtpl->assign('LEVEL_PERE', $level) ;
				$xtpl->parse('main.row.level');

				$xtpl->assign('LEVEL',1 ) ;
				if($maxlevel-$level>0)
				{
					$xtpl->assign('LEVEL_SON',$maxlevel-$level) ;
					$xtpl->parse('main.row.levelson');
				}
				$xtpl->assign('STRUCT_LIB', $val2['libelle']);
				$xtpl->assign('CAT_STRUCT_LIB', $this->CatStructArray[$val2['cat_struct']]);
				$xtpl->assign('STRUCT_ID', $key2);
				if($this->rights->_isActionAllowed('ref', 23, $this->uid))
				{
					$xtpl->parse('main.row.modif');
				}

				//v1.5-PPR-#000 suppression message notice
				if((isset($val2['tree'])) and ($val2['tree'] != NULL))
				{
					$subtree = $this->_buildTree($val2['tree'], $levelmax, $level) ;
					$xtpl->assign('STRUCT_TREE', $subtree);
					$xtpl->parse('main.row.tree');
				}else{
					if($this->rights->_isActionAllowed('ref', 22, $this->uid))
					{
						$xtpl->parse('main.row.delete');
					}

				}
				$xtpl->parse('main.row');
			}
		}
		$xtpl->parse('main');
		$treeString = $xtpl->text('main') ;
		return $treeString ;

	}

	public function _makeForm($struct_id='', $valueArray=array(), $doc_ref=0, $docVal = array())
	{
		$form = new Form('../lst_struct.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);

		if(($valueArray == NULL)&&($struct_id==''))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}
		if($valueArray != NULL)
		{
			$struct_lib = $valueArray['struct_lib'] ;
			$struct_id = $valueArray['struct_id'] ;
			$struct_dsc = $valueArray['struct_dsc'] ;
			$struct_parent = $valueArray['struct_parent'] ;
			$cat_struct = $valueArray['cat_struct'];
			$struct_url_export = $valueArray['struct_url_export'];
			$struct_user = $valueArray['struct_user'];
			$struct_pwd = $valueArray['struct_pwd'];
			$struct_id_int = $struct_id;

		}else{
			if($struct_id!='')
			{
				$struct_lib = $this->StructArray[$struct_id]['LIB'] ;
				$struct_dsc = $this->StructArray[$struct_id]['DSC'] ;
				$struct_parent = $this->StructArray[$struct_id]['PARENT'] ;
				$cat_struct = $this->StructArray[$struct_id]['ID_CAT_STRUCT'];
				$struct_url_export = $this->StructArray[$struct_id]['URL_EXPORT'];
				$struct_user = $this->StructArray[$struct_id]['USER'];
				$struct_pwd = $this->StructArray[$struct_id]['PWD'];
				$struct_id_int = $struct_id;
			}else{
				$struct_lib = '' ;
				$struct_dsc= '' ;
				$struct_parent = '' ;
				$cat_struct = 0;
				$struct_url_export = '';
				$struct_user = '';
				$struct_pwd = '';
				$struct_id_int = 0;

			}
		}

		$xtplform->assign('STRUCT_ID_INT', $form->_mkInput('hidden', 'struct_id_int', $struct_id_int)) ;
		$xtplform->assign('STRUCT_ID', $form->_mkInput('text', 'struct_id', $struct_id)) ;
		$xtplform->assign('STRUCT_LIB', $form->_mkInput('text', 'struct_lib',$struct_lib)) ;
		$xtplform->assign('STRUCT_DSC', $form->_mkInput('text', 'struct_dsc',$struct_dsc)) ;
		$xtplform->assign('STRUCT_PARENT', $form->_mkSelect('struct_parent', $this->StructComboArray, $struct_parent,'')) ;
		$xtplform->assign('CAT_STRUCT', $form->_mkSelect('cat_struct', $this->CatStructArray, $cat_struct,'')) ;

		//TODO placer le droit admin
		if ($this->uid=='1')
		{
			$xtplform->assign('STRUCT_URL_EXPORT', $form->_mkInput('text', 'struct_url_export',$struct_url_export)) ;
			$xtplform->assign('STRUCT_USER', $form->_mkInput('text', 'struct_user',$struct_user)) ;
			$xtplform->assign('STRUCT_PWD', $form->_mkInput('text', 'struct_pwd',$struct_pwd)) ;
			$xtplform->parse('main.exportform');
		}
		$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('submit', 'Enregistrer' ) );

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	public function _makeView($rights=NULL, $struct_id='')
	{
		$form = new Form('../lst_struct.php') ;
		$xtplform = new XTemplate($this->view_file, $this->xtpl_path);

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Structure::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>