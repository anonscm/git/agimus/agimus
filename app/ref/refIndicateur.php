<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/indicateur.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/hdate.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/indicXmlIO.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/xmlAnalyser.php') ;


/**
 *
 * <p>Indicateur</p>
 *
 * @name Indicateur
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class RefIndicateur extends Indicateur {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés    */
	/*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des indicateurs
	 */
	private $IndicateurArray ;

	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des groupes utilisateurs
	 */
	private $OwnerArray ;

	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des valeurs de completude
	 */
	private $completionValuesArray ;

	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $formString ;

	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $impxml_form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apr�s traitement
	 */
	private $impXmlFormString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/**
	 * @var $realReqString(String)
	 * @desc chaine sql completee par les valeurs de filtre
	 */
	private $realReqString ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. m�thodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>cr�ation de l'instance de la classe</p>
	 *
	 * @name Indicateur::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'indiclist.xtpl' ;
		$this->form_file = 'indicform.xtpl' ;
		$this->impxml_form_file = 'impxmlform.xtpl' ;
		$this->result_file = 'indicresult.xtpl' ;
		$this->xtpl_path = $this->lang.'/ref/'  ;
		$this->IndicateurArray = $this->_getList() ;

		//TODO : remplacer par acces classe
		$this->TypeArray=array("1"=>"Suivi","2"=>"Gouvernance", "Exploitation");
		$this->OwnerArray=array("1"=>"Administrateurs");
		$this->CompletionValuesArray=array(
		"1"=>"Heures"
		);
	}

	/**
	 * V�rification des variables re�ues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refIndicateur::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray)
	{
		$maconnexion = MysqlDatabase::GetInstance() ;

		// contr�le libell�
		$error=false;
		if(trim($valueArray['lib'])=='')
		{
			$error=true;
		}
		// contr�le unit�
		if(!$valueArray['unite']>0)
		{
			$error=true;
		}

		if ($error)
		throw new MsgException('_ERROR_VALUE_MISSING_')  ;

		// contrôle sécuritaire des requetes
		if (trim($valueArray['sql_string'])!='')
		{
			if (
			(strstr(strtoupper($valueArray['sql_string']),"DELETE")) or
			(strstr(strtoupper($valueArray['sql_string']),"UPDATE")) or
			(strstr(strtoupper($valueArray['sql_string']),"CREATE")) or
			(strstr(strtoupper($valueArray['sql_string']),"DROP")) or
			(strstr(strtoupper($valueArray['sql_string']),"TRUNCATE"))
			)
			throw new MsgException('_ERROR_INSECURE_SQL_STRING_')  ;
		}
	}

	/**
	 * V�rification des variables re�ues via le formulaire import XML
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refIndicateur::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkXmlFormValues($valueArray)
	{
		// controle libelle
		$error=false;
		if(trim($valueArray['name'])=='')
		{
			$error=true;
		}

		if ($error)
		throw new MsgException('_ERROR_VALUE_MISSING_')  ;

	}

	/**
	 * Mise � jour des variables re�ues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refIndicateur::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
		Indicateur::_setId($valueArray['indic_id']) ;
		Indicateur::_setOwner($valueArray['owner']) ;
		Indicateur::_setLib($valueArray['lib']) ;
		Indicateur::_setDsc($valueArray['dsc']) ;
		Indicateur::_setType($valueArray['type']) ;
		Indicateur::_setUnite($valueArray['unite']) ;
		Indicateur::_setDefaultOutput($valueArray['default_output']) ;
		Indicateur::_setUpdateFreq($valueArray['update_freq']) ;
		Indicateur::_setTrig($valueArray['trig']) ;
		Indicateur::_setSqlString($valueArray['sql_string']) ;
		Indicateur::_setCompletionValues($valueArray['completion_values']) ;
	}

	/**
	 * Import des données xml de l'indicateur
	 *
	 * <p>_import</p>
	 *
	 * @name refIndicateur::_import()
	 * @param $file(string)
	 * @return void
	 */
	public function _import()
	{
		$FileTMP = $_FILES['userfile']['tmp_name'];
		if ( !file_exists($FileTMP) )
		{
			throw new msgException('_FILE_UPLOAD_FILETMP_NOT_FOUND_') ;
		}else{

			$x = new XmlAnalyser() ;
			$data = $x->_getDataXmlInArray($FileTMP) ;
			$data = array_map("utf8_decode",$data);
			if (is_array($data))
			{
				$newIndic=new Indicateur();
				$newIndic->_setOwner($data['OWNER']);
				$newIndic->_setLib($data['LIB']);
				$newIndic->_setDsc($data['DSC']);
				$newIndic->_setType($data['TYPE']);
				$newIndic->_setUnite($data['UNITE']);
				$newIndic->_setDefaultOutput($data['DEFAULT_OUTPUT']);
				$newIndic->_setUpdateFreq($data['UPDATE_FREQ']);
				$newIndic->_setTrig($data['TRIG']);
				$newIndic->_setSqlString($data['SQL_STRING']);
				$newIndic->_setCompletionValues($data['COMPLETION_VALUES']);
				$newIndic->_create();
			}
				
		}
	}


	/**
	 * Insertion des donn�es dans le mod�le de pag
	 *
	 * <p>_makePage</p>
	 *
	 * @name Indicateur::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($indicId=0, $msg='')
	{
		global $_indicUnits,$_defaultOutputs,$_updateFreqs,$_arGraphIcons;

		$config=$_SESSION['config'];

		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
	 	$xtpl->assign('MSG', $msg) ;
	 	$xtpl->parse('main.msg');
		}

		foreach($this->IndicateurArray as $key=>$value)
		{
			// Annulé cause perfs

			//if (!($this->_ctrSqlQuery($value['SQL_STRING'])))
			//	$xtpl->parse('main.list.row.sqlfailure');

			$xtpl->assign('INDIC_ID', $key);
			$xtpl->assign('LIB', $value['LIB']);
			$xtpl->assign('DSC', $value['DSC']);
			if ($value['TYPE']>0)
			$xtpl->assign('TYPE', $this->TypeArray[$value['TYPE']]);
			$xtpl->assign('UNITE', $_indicUnits[$value['UNITE']]);

			//TODO : droits d'accès au fichier XML
			//if($this->rights->_isActionAllowed('ref', 99, $this->uid))
			{
				$newIndicXml=new IndicXmlIO();
				$xmlFileName="indic_".$key."_".$config['id_struct'].".xml";
				$newIndicXml->outFile=ROOT_XML."/".$xmlFileName;
				$newIndicXml->_indicToXml($key,$value['LIB']);
				if (file_exists($newIndicXml->outFile))
				$xtpl->assign('XML_FILE', ROOT_APPL."/xml/".$xmlFileName);
				$xtpl->parse('main.list.row.xml');
			}

			if (isset($value['DEFAULT_OUTPUT']))
			$icon=$_arGraphIcons[$value['DEFAULT_OUTPUT']];
			else
			$icon=$_arGraphIcons[0];

			$xtpl->assign('DEFAULT_OUTPUT', $icon);

			if(($key != $indicId)&&($this->rights->_isActionAllowed('ref', 27, $this->uid)))
			{
				$xtpl->parse('main.list.row.modif');
			}

			if(($key != $indicId)&&($this->rights->_isActionAllowed('ref', 26, $this->uid)))
			{
				$xtpl->parse('main.list.row.delete');
			}
			$xtpl->parse('main.list.row');
		}
		$xtpl->parse('main.list');

		// formulaire de saisie d'indicateur
		if ($this->rights->_isActionAllowed('ref', 29, $this->uid))
		{
			if($this->formString != '')
			{
				$xtpl->assign('FORM', $this->formString);
				$xtpl->parse('main.form');
			}
		}

		// formulaire d'import d'indicateur
		if ($this->rights->_isActionAllowed('ref', 76, $this->uid))
		{
			if($this->impXmlFormString != '')
			{
				$xtpl->assign('IMPXMLFORM', $this->impXmlFormString);
				$xtpl->parse('main.impxmlform');
			}
		}

		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}


	private function _genericSqlString($reqString)
	{
		$config=$_SESSION['config'];
		//remplacement des paramètres de requete

		// recherche d'un pere eventuel
		$newStruct=new Structure();
		$arStructFils=$newStruct->_getTree('');
		if (count($arStructFils[''])>0)
		{
			$newStructSqlString=" AND (" . $this->_formatStructSql($arStructFils['']) . " ) ";
			$newDenStructSqlString=" AND (" . $this->_formatStructSql($arStructFils[''],'r_denombrement') . " ) ";
		}
		else
		{
			$newStructSqlString =" AND t_agregat.ID_STRUCT = \"".$config['id_struct']."\" ";
			$newDenStructSqlString =" AND r_denombrement.ID_STRUCT = \"".$config['id_struct']."\" ";
		}
		$reqString=str_replace('%STRUCTSEL%',$newStructSqlString,$reqString);
		$reqString=str_replace('%DENSTRUCTSEL%',$newDenStructSqlString,$reqString);
		$reqString=str_replace('%DIMSEL%','',$reqString);
		$reqString=str_replace('%DATEDEB%','',$reqString);
		$reqString=str_replace('%DATEFIN%','',$reqString);

		return $reqString;

	}

	/**
	 * Contrôle requete SQL
	 *
	 * <p>Contrôle requete SQL</p>
	 *
	 * @name Indicateur::_ctrSqlQuery($reqString)
	 * @return boolean
	 */
	public function _ctrSqlQuery($reqString)
	{
		if ($reqString=="")
		return false;

		$this->realReqString=$this->_genericSqlString($reqString);
		$maconnexion = MysqlDatabase::GetInstance() ;
		$res = $maconnexion->_queryTest($this->realReqString);
		return $res;
	}

	/**
	 * Formattage du tableau de valeurs
	 *
	 * <p>libelle</p>
	 *
	 * @name Indicateur::_getHtmlResult()
	 * @return array
	 */
	public function _getHtmlResult($reqString="",$maxRows=10)
	{
		global $_arReqColLibs;
		$xtplResult = new XTemplate($this->result_file, $this->xtpl_path);

		if ($reqString!="")
		{

			if (!$this->_ctrSqlQuery($reqString))
			{
				$xtplResult->assign('SQL_FAILURE',"Requete SQL incorrecte : $this->realReqString");
				$xtplResult->parse('main.message');
				$xtplResult->parse('main');
			}
			else
			{
				$reqString=$this->_genericSqlString($reqString);
				$reqString=$reqString." LIMIT ".$maxRows;


				$this->_setSqlString($reqString);
				$arResult=$this->_getResult();
					
				$row=0;
				if (count($arResult)>0)
				{
					foreach($arResult as $val=>$values)
					{
						if ($row==0)
						{
							foreach(array_keys($values) as $lib)
							{
								$lib=(isset($_arReqColLibs[$lib])?$_arReqColLibs[$lib]:$lib);
								$xtplResult->assign('HEADER',$lib);
								$xtplResult->parse('main.header');
							}
						}

						foreach($values as $val=>$lib)
						{
							$xtplResult->assign('VALEUR',$lib);
							$xtplResult->parse('main.row.cell');
						}
						$xtplResult->parse('main.row');
						$row+=1;
					}
					$xtplResult->parse('main');
				}
			}
		}

		return $xtplResult->text('main');

	}

	/**
	 * G�n�ration du formulaire de saisie
	 *
	 * <p>_makeForm</p>
	 *
	 * @name Indicateur::_makeForm()
	 * @param $indic_id (int)
	 * @param $valueArray (array)
	 * @param $doc_ref (Int)
	 * @param $docVal (array)
	 * @return string
	 */
	public function _makeForm($indic_id=0, $valueArray=array(), $doc_ref=0, $docVal = array())
	{
		global $_indicUnits,$_defaultOutputs,$_updateFreqs;

		$form = new Form('../lst_indic.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);

		if(($valueArray == NULL)&&($indic_id==0))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}

		if($valueArray != NULL)
		$valueArray=keyToUpper($valueArray);

		if($valueArray != NULL)
		{
			$indic_id = $valueArray['ID'] ;
			$owner = $valueArray['OWNER'] ;
			$lib = $valueArray['LIB'] ;
			$dsc = $valueArray['DSC'] ;
			$type = $valueArray['TYPE'] ;
			$unite = $valueArray['UNITE'] ;
			$default_output = $valueArray['DEFAULT_OUTPUT'] ;
			$update_freq = $valueArray['UPDATE_FREQ'] ;
			$trig = $valueArray['TRIG'] ;
			$sql_string = $valueArray['SQL_STRING'] ;
			$completion_values = $valueArray['COMPLETION_VALUES'] ;

		}else{
			if($indic_id!=0)
			{
				$indic_id = $this->IndicateurArray[$indic_id]['ID'] ;
				$owner = $this->IndicateurArray[$indic_id]['OWNER'] ;
				$lib = $this->IndicateurArray[$indic_id]['LIB'] ;
				$dsc = $this->IndicateurArray[$indic_id]['DSC'] ;
				$type = $this->IndicateurArray[$indic_id]['TYPE'] ;
				$unite = $this->IndicateurArray[$indic_id]['UNITE'] ;
				$default_output = $this->IndicateurArray[$indic_id]['DEFAULT_OUTPUT'] ;
				$update_freq = $this->IndicateurArray[$indic_id]['UPDATE_FREQ'] ;
				$trig = $this->IndicateurArray[$indic_id]['TRIG'] ;
				$sql_string = $this->IndicateurArray[$indic_id]['SQL_STRING'] ;
				$completion_values = $this->IndicateurArray[$indic_id]['COMPLETION_VALUES'] ;

			}else{
				$indic_id = 0 ;
				$owner = 0 ;
				$lib = '' ;
				$dsc = '' ;
				$type = '' ;
				$unite = '' ;
				$default_output = 1 ;
				$update_freq = 1 ;
				$trig = '' ;
				$sql_string = '' ;
				$completion_values = '';
			}
		}

		// affichage des n premières valeurs pour contrôle
		$htmlValues="";
		if ($sql_string!="")
		{
			$htmlValues=$this->_getHtmlResult($sql_string);
			$xtplform->assign('VALUE_TABLE',$htmlValues);
		}

		$date=new HDate();
		$xtplform->assign('INDIC_ID', $form->_mkInput('hidden', 'indic_id', $indic_id)) ;
		$xtplform->assign('OWNER', $form->_mkSelect('owner', $this->OwnerArray, $owner)) ;
		$xtplform->assign('LIB', $form->_mkInput('text', 'lib',$lib)) ;
		$xtplform->assign('DSC', $form->_mkTextArea('dsc',$dsc)) ;
		$xtplform->assign('TYPE', $form->_mkSelect('type',$this->TypeArray,$type)) ;
		$xtplform->assign('UNITE', $form->_mkSelect('unite', $_indicUnits, $unite)) ;
		$xtplform->assign('DEFAULT_OUTPUT', $form->_mkSelect('default_output', $_defaultOutputs, $default_output)) ;
		$xtplform->assign('UPDATE_FREQ', $form->_mkSelect('update_freq', $_updateFreqs, $update_freq)) ;
		$xtplform->assign('TRIG', $form->_mkInput('text', 'trig',$trig)) ;

		$xtplform->assign('SQL_STRING', $form->_mkTextArea('sql_string',$sql_string,100,10)) ;

		$xtplform->assign('COMPLETION_VALUES', $form->_mkSelect('completion_values',$this->CompletionValuesArray,$completion_values)) ;

		$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('validate', 'Enregistrer' ) );

		$xtplform->parse('main.link_emptyform');

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	/**
	 * Generation du formulaire d'import XML
	 *
	 * <p>_makeImpXmlForm</p>
	 *
	 * @name Indicateur::_makeImpXmlForm()
	 * @return string
	 */
	public function _makeFormXml($valueArray=NULL)
	{
		$form = new Form('../lst_indic.php') ;
		$xtplform = new XTemplate($this->impxml_form_file, $this->xtpl_path);

		if(($valueArray == NULL))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}

		$date=new HDate();
		$xtplform->assign('INDIC_ID', $form->_mkInput('hidden', 'xml_indic_id', 0)) ;
		$xtplform->assign('LIB', $form->_mkInput('file', 'userfile')) ;

		$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('import', 'Importer' ) );
		$xtplform->parse('main.link_emptyform');

		$xtplform->parse('main');
		$this->impXmlFormString = $xtplform->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Indicateur::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>