<?php
require_once( DIR_WWW.ROOT_APPL.'/app/common/discipline.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/dcplIntra.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/form.php') ;
require_once( DIR_WWW.ROOT_APPL.'/app/common/msgException.php') ;

/**
 *
 * <p>Discipline</p>
 *
 * @name SrvIntra
 * @author AGIMUS <agimus.technique@education.gouv.fr> 
 * @licence Cecill v2 (http://www.cecill.info)
 * @copyright AGIMUS 2008-2012
 * @version 1.0.0
 * @package name
 */

class RefDiscipline extends Discipline {

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  1. proprietés 
	 /*~*~*~*~*~*~*~*~*~*~*/
	/**
	 * @var $session(Object)
	 * @desc Session en cours
	 */
	private $session ;
	/**
	 * @var $rights(Object)
	 * @desc Droits
	 */
	private $rights ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste arborecente des srvIntras
	 */
	private $DisciplineTreeArray ;
	/**
	 * @var (Array)
	 * @desc tableau contenant la liste des regroupements
	 */
	private $RegrComboArray ;

	/**
	 * @var $xtpl_file(String)
	 * @desc fichier template de la page
	 */
	private $xtpl_file ;
	/**
	 * @var $form_file(String)
	 * @desc fichier template du formulaire
	 */
	private $form_file ;

	/**
	 * @var $formString(String)
	 * @desc fichier du formulaire apres traitement
	 */
	private $formString ;

	/**
	 * @var $uid(String)
	 * @desc identifiant de l'utilisateur
	 */
	private $uid ;

	/**
	 * @var $lang(String)
	 * @desc langue pour la session
	 */
	private $lang ;

	/*~*~*~*~*~*~*~*~*~*~*/
	/*  2. methodes  
	 /*~*~*~*~*~*~*~*~*~*~*/

	/**
	 * Constructeur
	 *
	 * <p>creation de l'instance de la classe</p>
	 *
	 * @name Discipline::__construct()
	 * @return void
	 */
	public function __construct($session, $rights) {
		$this->session = $session ;
		$this->rights = $rights ;
		$this->uid=$session->_getUid();
		$this->lang = $session->_getLang() ;
		$this->xtpl_file = 'disciplinelist.xtpl' ;
		$this->form_file = 'disciplineform.xtpl' ;
		$this->xtpl_tree = 'disciplinetree.xtpl' ;
		$this->xtpl_path = $this->lang.'/ref/'  ;
		$this->DisciplineTreeArray = $this->_getList() ;
	}

	/**
	 * Verification des variables recues via le formulaire
	 *
	 * <p>_checkFormValues</p>
	 *
	 * @name refDiscipline::_checkFormValues()
	 * @param $valueArray(Array)
	 * @return void
	 */
	public function _checkFormValues($valueArray,$action='')
	{
		if($valueArray['discipline_lib'] == '')
		{
			throw new MsgException('_ERROR_LIBELLE_MISSING_')  ;
		}

		$maconnexion = MysqlDatabase::GetInstance() ;

		//Verification que le libelle n'est pas deja utilise
		// désactivé : les filières ont des libellés identiques mais des références différentes...
		$sql = 'SELECT * FROM t_discipline ';
		$sql .= 'WHERE LIB=  \''.AddSlashes($valueArray['discipline_lib']).'\' ';
		if($valueArray['discipline_id']!=0)
		{
			$sql .= 'AND ID != \''.$valueArray['discipline_id'].'\' ';
		}
		try{
			$res = $maconnexion->_bddQuery($sql) ;
		}
		catch(MsgException $e){
			$msgString = $e ->_getError();
			throw new MsgException($msgString, 'database') ;
		}
		if($maconnexion->_bddNumRows($res) >0)
		{
			throw new msgException('_ERROR_LIBELLE_ALREADY_EXISTS_')  ;
		}

	}

	/**
	 * Mise a jour des variables recues via le formulaire
	 *
	 * <p>_setFormValues</p>
	 *
	 * @name refDiscipline::_setFormValues()
	 * @param $valuArray(Array)
	 * @return void
	 */
	public function _setFormValues($valueArray)
	{
		Discipline::_setId($valueArray['discipline_id']) ;
		Discipline::_setExtnRef($valueArray['discipline_extn_ref']) ;
		Discipline::_setLib($valueArray['discipline_lib']) ;
		Discipline::_setDsc($valueArray['discipline_dsc']) ;
		Discipline::_setIdRegr($valueArray['id_regr']) ;
	}

	/**
	 * Insertion des donnees dans le modele de pag
	 *
	 * <p>_makePage</p>
	 *
	 * @name Discipline::_makePage()
	 * @param session (class)
	 * @param rights (class)
	 * @return array
	 */
	public function _makePage($disciplineId=0, $msg='')
	{
		$menuleft = $this->session->_makeMenuLeft($this->rights) ;
		$xtpl = new XTemplate($this->xtpl_file, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		if($msg != '')
		{
			$xtpl->assign('MSG', $msg) ;
			$xtpl->parse('main.msg');
		}


		foreach($this->DisciplineTreeArray as $key=>$value)
		{
			$xtpl->assign('DISCIPLINE_ID', $key);
			$xtpl->assign('EXTN_REF', $value['EXTN_REF']);
			$xtpl->assign('LIB', $value['LIB']);
			$xtpl->assign('DSC', $value['DSC']);
			$xtpl->assign('REGR_ID', $value['ID_REGR']);

			//Recupération des fils intra
			$arFils=DcplIntra::_getList($key);
			$nbFils=count($arFils);

			if(($key != $disciplineId)&&($this->rights->_isActionAllowed('ref', 59, $this->uid)))
			{
				$xtpl->parse('main.list.row.modif');
			}

			//V1.5-PPR-#000 suppression message notice
			if ((isset($value['tree'])) and ($value['tree'] != NULL))
			{
				$treeArray = $value['tree'] ;
				$treeString = $this->_buildTree($treeArray, $maxlevel, 1) ;
				$xtpl->assign('DISCIPLINE_TREE', $treeString );
				$xtpl->parse('main.list.row.tree');
			}else{
				//v1.2-PPR-28032011 restriction à la suppression des id > 1 (1 : autres)
				//if(($nbFils==0) && ($key != $disciplineId)&&($this->rights->_isActionAllowed('ref', 58, $this->uid)))
				if(($key!=1) && ($nbFils==0) && ($key != $disciplineId)&&($this->rights->_isActionAllowed('ref', 58, $this->uid)))
				{
					$xtpl->parse('main.list.row.delete');
				}
			}
			$xtpl->parse('main.list.row');
		}
		$xtpl->parse('main.list');
		//Construction du menu en fonction des droits de l'utilisateur

		if($this->formString != '')
		{
			$xtpl->assign('FORM', $this->formString);
			$xtpl->parse('main.form');
		}
		$xtpl->parse('main');
		$content = $xtpl->text('main') ;
		$this->session->_makeMainPage($content, $menuleft) ;
	}

	// non utilisee pour l'instant
	private function _buildTree($subtreeArray, $maxlevel = 0, $treelevel=0)
	{
		$levelmax = $maxlevel ;
		$treeArray = $subtreeArray ;
		$level=$treelevel+1 ;
		$treeString = '' ;
		$xtpl = new XTemplate($this->xtpl_tree, $this->xtpl_path);
		$xtpl->assign('IMAGES_PATH', ROOT_IMAGES ) ;
		$xtpl->assign('LANG', $this->lang ) ;
		foreach($treeArray as $key=>$value)
		{
			foreach($value as $key2=>$val2)
			{
				$xtpl->assign('LEVEL_PERE', $level) ;
				$xtpl->parse('main.row.level');

				$xtpl->assign('LEVEL',1 ) ;
				if($maxlevel-$level>0)
				{
					$xtpl->assign('LEVEL_SON',$maxlevel-$level) ;
					$xtpl->parse('main.row.levelson');
				}
				$xtpl->assign('DISCIPLINE_LIB', $val2['libelle']);
				$xtpl->assign('DISCIPLINE_ID', $key2);
				if($this->rights->_isActionAllowed('ref', 59, $this->uid))
				{
					$xtpl->parse('main.row.modif');
				}

				if($val2['tree'] != NULL)
				{
					$subtree = $this->_buildTree($val2['tree'], $levelmax, $level) ;
					$xtpl->assign('DISCIPLINE_TREE', $subtree);
					$xtpl->parse('main.row.tree');
				}else{
					if($this->rights->_isActionAllowed('ref', 58, $this->uid))
					{
						$xtpl->parse('main.row.delete');
					}

				}
				$xtpl->parse('main.row');
			}
		}
		$xtpl->parse('main');
		$treeString = $xtpl->text('main') ;
		return $treeString ;

	}

	public function _makeForm($discipline_id=0, $valueArray=array(), $doc_ref=0, $docVal = array())
	{
		$form = new Form('../lst_discipline.php') ;
		$xtplform = new XTemplate($this->form_file, $this->xtpl_path);
		$config=$_SESSION['config'];

		if(($valueArray == NULL)&&($discipline_id==0))
		{
			$xtplform->assign('CACHEDIV', 'cachediv') ;
		}
		if($valueArray != NULL)
		{
			$discipline_extn_ref = $valueArray['discipline_extn_ref'] ;
			$discipline_lib = $valueArray['discipline_lib'] ;
			$discipline_id = $valueArray['discipline_id'] ;
			$discipline_dsc = $valueArray['discipline_dsc'] ;
			$id_regr = $valueArray['id_regr'];

		}else{
			if($discipline_id!=0)
			{
				$discipline_extn_ref =  $this->DisciplineTreeArray[$discipline_id]['EXTN_REF'] ;
				$discipline_lib =  $this->DisciplineTreeArray[$discipline_id]['LIB'] ;
				$discipline_dsc =  $this->DisciplineTreeArray[$discipline_id]['DSC'] ;
				$id_regr =  $this->DisciplineTreeArray[$discipline_id]['ID_REGR'] ;
			}else{
				$discipline_extn_ref = '' ;
				$discipline_lib = '' ;
				$discipline_dsc= '' ;
				$id_regr = 0;
			}
		}

		$xtplform->assign('DISCIPLINE_ID', $form->_mkInput('hidden', 'discipline_id', $discipline_id)) ;
		$xtplform->assign('EXTN_REF', $form->_mkInput('text', 'discipline_extn_ref', $discipline_extn_ref)) ;
		$xtplform->assign('LIB', $form->_mkInput('text', 'discipline_lib',$discipline_lib)) ;
		$xtplform->assign('DSC', $form->_mkInput('text', 'discipline_dsc',$discipline_dsc)) ;
		$xtplform->assign('REGR_ID', $form->_mkInput('hidden','id_regr', $id_regr,'')) ;

		$xtplform->assign('SUBMIT_BUTTON', $form->_mkSubmit('submit', 'Enregistrer' ) );

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	public function _makeView($rights=NULL, $discipline_id=0)
	{
		$form = new Form('../lst_discipline.php') ;
		$xtplform = new XTemplate($this->view_file, $this->xtpl_path);

		$xtplform->parse('main');
		$this->formString = $xtplform->text('main') ;
	}

	/**
	 * Destructeur
	 *
	 * <p>Destruction de l'instance de classe</p>
	 *
	 * @name Discipline::__destruct()
	 * @return void
	 */
	public function __destruct() {

	}
}
?>