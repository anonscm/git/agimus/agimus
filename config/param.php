<?php
// =========================================================
// AGIMUS - 2010/2011 - CAPELLA CONSEIL
// =========================================================

// Parametres d'application

define("NOM_APPL","AGIMUS Univ 1");
define("VERSION_APPL","v1.5.beta");
define("DEFAULT_LANG_APPL","fr");

// type de structure deployee :
// ETB : etablissement,
// UNR : U.N.R.
// MNS : Ministere
define("SITE_TYPE",'ETB'); 

// Serveur CAS
define('CAS_ACTIVE','0');
define('CAS_BASE','https://auth.univ-???.fr');

define("ROOT_EXTN_REF","MESR"); // ministere de l'enseignement et de la recherche


//annee copyright
define("YEAR_APPL",date('Y'));

//racine Web
if ($_SERVER['DOCUMENT_ROOT']!="")
define("DIR_WWW",trim($_SERVER['DOCUMENT_ROOT']));
else
{
	if (isset($_SERVER['DIR_WWW']))
	define("DIR_WWW",trim($_SERVER['DIR_WWW']));
	else
	die("Impossible de determiner la racine du site !");
}

//racine de l'application
define("ROOT_APPL","/agimus_v1.5");
//Path config
define("ROOT_CONFIG",DIR_WWW.ROOT_APPL."/config/");
//Path des xml
define("ROOT_XML",DIR_WWW.ROOT_APPL."/xml/");
//path des xtpl
define("ROOT_XTPL",DIR_WWW.ROOT_APPL."/templates/");
//path des images pour affichage
define("ROOT_IMAGES",ROOT_APPL."/images/");
//path d'upload des images
define("ROOT_IMAGES_UPLOAD",DIR_WWW.ROOT_APPL."/images/");
//path d'upload des fichiers
define("ROOT_FILES", DIR_WWW.ROOT_APPL."/file/") ;
//path du sas d'entree
define("ROOT_IN", DIR_WWW.ROOT_APPL."/in/") ;
//path des archives d'entree
define("ROOT_IN_ARCHIVES", DIR_WWW.ROOT_APPL."/in/archives") ;
//path du sas de sortie
define("ROOT_OUT", DIR_WWW.ROOT_APPL."/out/") ;
//path des logs
define("PATH_LOGS", ROOT_APPL."/logs/") ;
define("ROOT_LOGS", DIR_WWW.PATH_LOGS) ;
//path des graphes
define("ROOT_MEDIA", "media/") ;
//path des classes generees
define("ROOT_CLASS",DIR_WWW.ROOT_APPL."/app/common");
// templates de classe
define("TPL_MAIN",DIR_WWW.ROOT_APPL."/templates/classgen/tpl_main.php");
define("TPL_ATTRIBUTE",DIR_WWW.ROOT_APPL."/templates/classgen/tpl_attribute.php");
define("TPL_READ_FONC",DIR_WWW.ROOT_APPL."/templates/classgen/tpl_read_fonc.php");
define("TPL_WRITE_FONC",DIR_WWW.ROOT_APPL."/templates/classgen/tpl_write_fonc.php");

// tableau de matching des champs de log de connexion
// champ --> index dans la ligne de connexion
// v1.1-PPR-03012011 ajout du template de log en cle primaire + Pattern regex
$_arMatchLogFields=array(
"access"=>array(
"PATTERN"=>"/^(\S+) (\S+) (\S+) (\S+) (\S+) \[(\S+) ([^\]]+)\] \"(\S+) (.*?) (\S+)\" (\S+) (\S+) \"(.*?)\" (\".*?\")$/",
"UID"=>1,
"STAMP_START"=>6,
"FLOW_SIZE"=>5,
"RESP_TIME"=>12,
"MIME_TYPE"=>99,
"TARGET_URL"=>9,
"USER_AGENT"=>14
),
"light"=>array(
"PATTERN"=>"/^(\S+)\s+(\S+)\s+(\S+)\s+\[(\S+) ([^\]]+)\]\s+\"(\S+) (.*?) (\S+)\"\s+(\S+)\s+(\S+)\s+\"(.*?)\" (\".*?\") .*CASTRACEME=([a-zA-Z0-9\.-]+).*/",
"UID"=>13,
"STAMP_START"=>4,
"FLOW_SIZE"=>99,
"RESP_TIME"=>10,
"MIME_TYPE"=>99,
"TARGET_URL"=>7,
"USER_AGENT"=>12
),
"esup"=>array(
"PATTERN"=>"/^(.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?)$/",
"UID"=>7,
"STAMP_DATE"=>1,
"STAMP_HOUR"=>2,
"FLOW_SIZE"=>6,
"RESP_TIME"=>99,
"MIME_TYPE"=>4,
"TARGET_URL"=>8,
"USER_AGENT"=>99,
"FUNCTION"=>4
),
"zimap"=>array(
"PATTERN"=>"/^(.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?) (.*?)$/",
"UID"=>7,
"STAMP_DATE"=>1,
"STAMP_HOUR"=>2,
"FLOW_SIZE"=>6,
"RESP_TIME"=>99,
"MIME_TYPE"=>4,
"TARGET_URL"=>8,
"USER_AGENT"=>99,
"FUNCTION"=>4
)
);

//NEW PATTERN : /^(\S+)\s+(\S+)\s+(\S+)\s+\[(\S+) ([^\]]+)\]\s+\"(\S+) (.*?) (\S+)\"\s+(\S+)\s+(\S+)\s+\"(.*?)\" (\".*?\") .*CASTRACEME=([a-zA-Z0-9\.-]+).*/
//OLD PATTERN : /^(\S+) (\S+) (\S+) \[(\S+) ([^\]]+)\] \"(\S+) (.*?) (\S+)\" (\S+) (\S+) \"(.*?)\" (\S+) (\S+) (\S+) (\S+)$/

// tableau de matching des champs du trace log
// champ --> index dans la ligne de connexion
$_matchTraceLogFields=array(
"COOKIE_ID"=>0,
"UID"=>1
);

// trace log => separateur entre le cookie et le uid
$_traceLogSeparator=":";

// tableau de matching des type mime
//$_matchMimeTypes=array("text/html");
//$_matchMimeTypes=array("\"POST");
//v1.3-PPR-25042011 fonction du template
$_matchMimeTypes=array(
"access"=>array(),
"lighttpd"=>array(),
"esup"=>array("SSTART","CCALL_EXT"),
"zimap"=>array());

// tableau de matching des attributs LDAP
// attention : les attibuts ldap ne sont pas en notation hongroise
// ex : employeeType -> employeetype
//v1.5-PPR-#016 ajout nouvelles dimensions
$_matchLdapAttributes=array(
"uid"=>"uid",
"affiliation"=>"edupersonprimaryaffiliation",
"discipline"=>"supannetudiplome",
"diplome"=>"supannetucursusannee",
"dometu"=>"supannetusecteurdisciplinaire",
"composante"=>"supannentiteaffectationprincipale",
"reginsc"=>"supannregimeinscription",
"organisme"=>"supannorganisme"
);

// caractere obligatoire ou facultatif de la valeur d'attribut
// si 1. obligatoire => rejet de la ligne
// si 0. facultatif => deverse en id=1 (autre)
//v1.5-PPR-#016 ajout nouvelles dimensions
$_attribValueMandatory=array(
"application"=>"0",
"affiliation"=>"1",
"discipline"=>"0",
"diplome"=>"0",
"dometu"=>"0",
"composante"=>"0",
"reginsc"=>"0"
);

// algorithme de hashage utilise pour l'uid
// 0 : aucun, 1 sha1
define("UID_OBFUSCATE",1);
// specifie si l'uid de l'access.log est un cookie de trace ou un uid ldap
define("UID_IS_COOKIE_TRACE",0);

// tableau des restitutions par defaut
$_defaultOutputs=array(
"1"=>"table",
"2"=>"line",
"3"=>"pie",
"4"=>"histo",
"5"=>"radar"
);

//tableau des unites d'indicateur
$_indicUnits=array(
"1"=>"Duree (s)",
"2"=>"Volume (o)",
"3"=>"Taux (%)",
"4"=>"Nombre"
);

//tableau des frequences d'update
$_updateFreqs=array(
"1"=>"Heure",
"2"=>"Jour",
"3"=>"Semaine",
"4"=>"Mois",
"5"=>"Annee"
);

//Duree arbitraire en secondes d'une session applicative
$_AppliSessionDuration=900;

// Format du champ de rupture temporelle pour l'agregation
$_TimeRupt="Y-m-W-d";

define("DYNCHART_LEGEND_MAX_LENGTH",30);
define("PIE_THEME",'earth'); // earth, pastel, sand, water

// Couleur des graphes cumules

$_arJPGColors=array(
		"0"=>'aquamarine',
		"1"=>'burlywood',
		"2"=>'cadetblue',
		"3"=>'coral',
		"4"=>'darkkhaki',
		"5"=>'darkolivegreen',
		"6"=>'darkorange',
		"7"=>'darksalmon',
		"8"=>'darkstateblue',
		"9"=>'goldenrod',
		"10"=>'greenyellow',
		"11"=>'lightgreen',
		"12"=>'mediumorchid'
);

$_arJPGCumColors=array(
		"0"=>array('AntiqueWhite2','AntiqueWhite4:0.8'),
		"1"=>array('olivedrab1','olivedrab4:0.8'),
		"2"=>array('cadetblue1','cadetblue4:0.8'),
		"3"=>array('chocolate1','chocolate4:0.8'),
		"4"=>array('darkorchid1','darkorchid4:0.8')
);

// Liste des valeurs horaires
$_arHours=array(
"0"=>"0","1"=>"0","2"=>"0","3"=>"0","4"=>"0","5"=>"0","6"=>"0","7"=>"0","8"=>"0","9"=>"0",
"10"=>"0","11"=>"0","12"=>"0","13"=>"0","14"=>"0","15"=>"0","16"=>"0","17"=>"0","18"=>"0","19"=>"0",
"20"=>"0","21"=>"0","22"=>"0","23"=>"0"
);

// Icones des modules
$_arModuleIcons=array(
      "1"=>"settings.png",
	  "2"=>"transferts.png",
      "3"=>"referentiels.png",
      "4"=>"charts.png"
      );

// icones des graphes
$_arGraphIcons=array(
"0"=>"charts.png",
"1"=>"table.png",
"2"=>"line.png",
"3"=>"pie.png",
"4"=>"bar.png",
"5"=>"radar.png"
);

//nombre de colonnes dans le menu icone de tableaux de bord
define("NBR_COL_MENU_TDB",5);

// Libelle des colonnes de controle de requete (referentiel->Indicateur)
$_arReqColLibs=array(
"pivotid"=>"Ref. Pivot",
"pivotlib"=>"Libell&eacute; Pivot",
"pivotval"=>"Valeur de Pilot",
"lib"=>"Libell&eacute;",
"val"=>"Valeur",
"datemin"=>"Au plus t&ocirc;t",
"datemax"=>"Au plus tard"
);

// Template des ficheirs d'entree import
define("IMPORT_TEMPLATE_FILE",""); // valeur exemple : "agregat"

// Liste de matching des user agents 
$_userAgentList=array(
"Netscape",
"Opera",
"MSIE",
"Lynx",
"WebTV",
"Konqueror",
"Safari",
"Firefox",
"Chrome",
"Bot"
);
// dimensions de denombrements a exporter (cf. t_dim)
$arDimsToExport=array("1");
// dimensions a mettre a zero dans l'export
//v1.5-PPR-#016
$arDimsToRaz=array(
"ID_SRV_INTRA",
"ID_DCPL_INTRA",
"ID_AFFIL_INTRA",
"ID_DOM_ETU_INTRA",
"ID_DIPLOME_INTRA",
"ID_COMP_INTRA",
"ID_REG_INSC_INTRA"
);

// conversion mois en nombre
$arMonthConv=array(
"Jan"=>"01",
"Feb"=>"02",
"Mar"=>"03",
"Apr"=>"04",
"May"=>"05",
"Jun"=>"06",
"Jul"=>"07",
"Aug"=>"08",
"Sep"=>"09",
"Oct"=>"10",
"Nov"=>"11",
"Dec"=>"12"
);

// filtrage de l'URL : true pour recuperer uniquement l'url sans les parametres
define("GET_SHORT_URL","true");

// Seuil de panel insuffisant
define("SEUIL_PANEL_INSUFFISANT",0);

// Creation automatique de valeur de dimension (lors de l'enrichissement des logs de cnx par le LDAP)
define("CREATE_UNKNOWN_DIM",TRUE); //MODIF NC UPPA
//define("CREATE_UNKNOWN_DIM",FALSE);

// libelle des statuts de traitement
$arTrtStatus=array(
"10"=>"En cours",
"20"=>"Termin&eacute; en erreur",
"30"=>"Termin&eacute; avec Succ&egrave;s"
);

// reprise de donnees
// V1.2 Attention laisser a FALSE pour que les valeurs de dimension
// nouvellement creees restent prefixees par un "~"
// Si a True : l'affichage de la dimension dans "Referentiel" lance automatiquement
// la mise a jour du libelle en le prefixant par le nom de dimension ("Domaine", "Filiere", ...)
define("REPRISE_DONNEES_APOGEE", true);


// Suppression des lignes de logs traitees (final_agregate)
define("ERASE_CNXLOG_PROCESSED", true);
define("ERASE_CNX_PROCESSED", true);

//v1.1 reference de la dimension d'usage (taux de penetration)
define("REF_DIM_USAGE", 7); 

//v1.2 pre-filtrage des lignes sur chaine CAS
// ajouter la chaine voulue par template pour filter uniquement ces lignes
$_arPreFilter=array(
"access"=>"",
"lighttpd"=>"CASTRACEME",
"esup"=>""
);

//v1.5-PPR-#004 paramétrage de la reprise de données diplomes intra de Tours
define("CONV_DIPLOME_TOURS",false);

?>
